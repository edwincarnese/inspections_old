<?php
/*
 * Created on Dec 22, 2005
 *
 * Author: Jason Rankin
 */
?>
<script language=javascript >
function openerHref() {
	document.update.popup.value = opener.location.href;
}

</script>
<?php 
$iid = $_POST['iid'] or $iid = $_GET['iid'] or $iid = 0;
$popup = $_GET['isPopup'];

//require_once'session.php';
require_once'connect.php';

if (!$popup) {
	$title = "Schedule Inspector";
	require_once'header.php';
} else {
		?>
		<head>
	<link rel="stylesheet" type="text/css" href="style.css" />
	</head>
	<div id="content" ><?//style="width:700px; height:480px;">?>
	<?
}

?>
<p>Choose the inspectors to assign to this inspection.</p>

<form name="update" action="inspection-inspector-assign.php" method="post">
<input type="hidden" name="iid" value="<?php print $iid;?>" />

<table>
<tr><th>Lead</th><th>Name</th></tr>
<tr><td><input type="radio" name="lead" value="None" /></td>
<td>None</td></tr>
<?php
$query = "SELECT type FROM inspection where iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_result($result, 0) == 'Conformance' || mysql_result($result, 0) == 'Conformance - Clearance') {
	$query = "SELECT inspector.tid, lastname, firstname, job, eli FROM inspector LEFT JOIN insp_assigned ON (inspector.tid=insp_assigned.tid AND insp_assigned.iid=$iid) WHERE (eli IS NOT NULL AND eli LIKE '%_%') OR elt_lead='yes'  ORDER BY lastname, firstname";
} else {
	$query = "SELECT inspector.tid, lastname, firstname, job, eli FROM inspector LEFT JOIN insp_assigned ON (inspector.tid=insp_assigned.tid AND insp_assigned.iid=$iid) WHERE (eli IS NOT NULL AND eli LIKE '%_%') ORDER BY lastname, firstname";
}

	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while ($row = mysql_fetch_assoc($result)) {
	print '<tr><td>';
	if ($row['job'] == 'Lead') {
		print "<input type=\"radio\" name=\"lead\" value=\"$row[tid]\" checked=\"checked\" />";
	} else {
		print "<input type=\"radio\" name=\"lead\" value=\"$row[tid]\" />";
	}
	print "</td><td>$row[lastname], $row[firstname]</td></tr>\n";
}
?>
</table>
<input type="hidden" name="popup"/>
<p>
<input type="submit" name="submit" value="Save" onClick="javascript:openerHref();"/><input type="reset" value="Reset" /><input type="submit" name="submit" value="Cancel" onclick="javascript:self.close();"/>
</p>

<p><a href="index.php">Main Menu</a></p>
<?php
if (!$popup){
	require_once'footer.php';
} else {
?></div>
<?}?>