<?php




require_once'session.php';
require_once'connect.php';


$crid = $_GET['crid'] or $crid = 0;

$query = "SELECT *, units.number AS unitnumber FROM units INNER JOIN comprehensive_rooms USING (cuid) WHERE crid=$crid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}



$room = mysql_fetch_assoc($result);
$unitnumber = $room['unitnumber']; //units is second table and will overwrite room #
$iid = $room['iid'];
$number = $room['number'];
$cuid = $room['cuid'];

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - $room[unitdesc] - $room[name] (Room $room[number])";
require_once'header.php';
?>

<p><a href="inspection-comprehensive-room-editname.php?crid=<?php print $crid; ?>">Edit Room Name</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="inspection-comprehensive-interior-paintchips.php?crid=<?php print $crid; ?>">Paint Chip Samples</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="inspection-comprehensive-wipe.php?crid=<?php print $room['crid']; ?>">Dust Wipe</a>&nbsp;&nbsp;|&nbsp;&nbsp;Doors: <?php print $room['doors']; ?> <a href="inspection-comprehensive-room-savedoorwindow.php?crid=<?php print $crid; ?>&amp;item=doors&amp;action=dec"><b>-</b></a> <a href="inspection-comprehensive-room-savedoorwindow.php?crid=<?php print $crid; ?>&amp;item=doors&amp;action=inc"><b>+</b></a>&nbsp;&nbsp;&nbsp;Windows: <?php print $room['windows']; ?> <a href="inspection-comprehensive-room-savedoorwindow.php?crid=<?php print $crid; ?>&amp;item=windows&amp;action=dec"><b>-</b></a> <a href="inspection-comprehensive-room-savedoorwindow.php?crid=<?php print $crid; ?>&amp;item=windows&amp;action=inc"><b>+</b></a>&nbsp;&nbsp;&nbsp;<a href="inspection-comprehensive-room-doorwindow.php?crid=<?php print $crid; ?>">Change</a></p>
<hr />
<!--
<p><a href="inspection-comprehensive-room-addcomponent1.php?crid=<?php print $crid; ?>">Add Standard Component</a>&nbsp;&nbsp;
|&nbsp;&nbsp;<a href="inspection-comprehensive-room-addcustom1.php?crid=<?php print $crid; ?>">Add Custom Component</a>
</p>
-->
<table>
<tr><td style="vertical-align: top">
<?php



$query = "SELECT ccid, comprehensive_components.name, side, abbreviation, xrf, hazards, spottest, hazardassessment FROM comprehensive_components LEFT JOIN substrates USING (sid) WHERE crid=$crid ORDER BY comprehensive_components.displayorder, comprehensive_components.ccid";


$result = mysql_query($query);



if (mysql_num_rows($result)) {
?>
<table class="info">
<tr><th>Component</th><th><a href="inspection-comprehensive-room-component-sides.php?crid=<?php print $crid; ?>">Side</a></th><th>Substrate</th><th>Condition</th><th>XRF</th><th>Actions</th><th>Assessment</th><th>Delete</th></tr>
<?php
	$condmap = array('Intact' => 'I', 'Intact Where Visible' => 'IWV', 'Damaged Touchup' => 'DT', 'Damaged' => 'D', 'Assumed Damaged' => 'D', 'No Paint' => 'N', 'Post \'77' => '78', 'Damaged - Friction' => 'DF', 'Covered' => 'CV', 'Intact - Factory Finish' => 'IFF', 'Door Missing' => 'DM');

$spotTestActionArray = array('Component does not exist' => '\\', 'Capped' => 'CAP', 'Enclosed' => 'COV', 'Encapsulated' => 'ENC', 'Paint Stabilization' => 'PS', 'Prepared for ENC' => 'PRE', 'Vinyl/Metal Rep Window' => 'VR/MR', 'Storm Frame Removed' => 'SFR', 'Scraped' => 'SCR', 'Dipped' => 'DIP', 'Removed' => 'REM', 'Replaced' => 'REP', 'Reversed INT Intact' => 'REV');

// checks if components are not in the array; change color to determin

	while ($row = mysql_fetch_assoc($result)) {
		$condition = $row['hazards'];
                $spottest = $row['spottest']; 
                
$query1 = "SELECT * FROM room_item_list WHERE itemname='$row[name]'";
$result1 = mysql_query($query1) or sql_crapout($query1.'<br />'.mysql_error());

if($row1 = mysql_fetch_assoc($result1)) 
{
	print "<tr><td><a href=\"inspection-comprehensive-room-component.php?ccid=$row[ccid]\">$row[name]</a></td><td>$row[side]</td><td>$row[abbreviation]</td><td>$condmap[$condition]</td><td>$row[xrf]</td><td>$spotTestActionArray[$spottest]</td><td>$row[hazardassessment]</td><td><a href=\"inspection-comprehensive-room-component-delete.php?ccid=$row[ccid]&amp;crid=$crid\" class=\"red\">Delete</a></td></tr>\n";
}
else
{
        print "<tr><td><a href=\"inspection-comprehensive-room-component.php?ccid=$row[ccid]\" style='color:#F0059D; font-family:Calibri;'>$row[name]</a></td><td>$row[side]</td><td>$row[abbreviation]</td><td>$condmap[$condition]</td><td>$row[xrf]</td><td>$spotTestActionArray[$spottest]</td><td>$row[hazardassessment]</td><td><a href=\"inspection-comprehensive-room-component-delete.php?ccid=$row[ccid]&amp;crid=$crid\" class=\"red\">Delete</a></td></tr>\n";
}

		
	}
?>
</table>
<p>
<table width='550px'>
	<tr>
		<td width="225px" align="center">
			<?php
			}
			
			$query = "SELECT crid, name FROM comprehensive_rooms WHERE cuid=$cuid AND number=($number - 1)";
			$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			
			if (mysql_num_rows($result)) {
				list($prevcrid, $name) = mysql_fetch_row($result);
			?>
			
			<a href="inspection-comprehensive-room.php?crid=<?php print $prevcrid; ?>">&lt;&lt; <?php print "$name"; ?></a>
		</td>
			<?php
			}
			
			$query = "SELECT crid, name FROM comprehensive_rooms WHERE cuid=$cuid AND number=($number + 1)";
			$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			if (mysql_num_rows($result)) {
				list($nextcrid, $name) = mysql_fetch_row($result);
				//if ($prevcrid) {
				//	print ' <b>|</b> ';
				//} 
				
			?>
		<td width="225px" align=center>
			<a href="inspection-comprehensive-room.php?crid=<?php print $nextcrid; ?>"><?php print "$name"; ?> &gt;&gt;</a>
		
		<?php
		}
		?>
		</td>
	</tr>
</table>
</p>

<p><a href="inspection-comprehensive-interior-addroom.php?cuid=<?php print $cuid; ?>">Add new room</a></p>

<p><a href="inspection-comprehensive-xrfsummary.php?cuid=<?php print $room['cuid']; ?>">XRF summary / unit comments</a></p>

<p><a href="inspection-comprehensive-unit.php?cuid=<?php print $room['cuid']; ?>"><?php print $room['unitdesc']; ?> Main Menu</a></p>

<p><a href="inspection-view.php?iid=<?php print $room['iid']; ?>">Inspection Main Menu</a></p>
</td><td style="vertical-align: top; padding-left: 1em"><b>Add component:</b>
<?
$query = "SELECT * FROM room_item_list WHERE type='Interior'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$itemname = '$%%^^&';
while ($row = mysql_fetch_assoc($result)) {
	if (strpos($row['itemname'], $itemname) === 0) {
		print " <b>|</b> <a href=\"inspection-comprehensive-room-addcomponent.php?crid=$crid&amp;itemid=$row[itemid]\">".substr($row['itemname'], strlen($itemname))."</a>\n";
	} else {
		print "<br /><a href=\"inspection-comprehensive-room-addcomponent.php?crid=$crid&amp;itemid=$row[itemid]\">$row[itemname]</a>\n";
		$itemname = $row['itemname'];
	}
}
?>
<br /><a href="inspection-comprehensive-room-addcustom1.php?crid=<?php print $crid; ?>">Custom Component</a>
</td></tr>
</table>

<?php
require_once'footer.php';
?>