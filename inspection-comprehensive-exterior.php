<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

$query = "SELECT * FROM units  WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}

$unit = mysql_fetch_assoc($result);
$iid = $unit['iid'];
$unitnumber = $unit['number'];
$diagrams = $unit['diagrams'];

$query = "UPDATE units SET starttime=NOW() WHERE cuid=$cuid AND (starttime < '1000-01-01' OR starttime IS NULL)";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - $unit[unitdesc]";
require_once'header.php';
?>
<p><a href="inspection-comprehensive-soil.php?cuid=<?php print $cuid; ?>">Soil Inspection</a></p>

<?php
$query = "SELECT * FROM comprehensive_ext WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) { //add default entry if none exists
	$query = "INSERT INTO comprehensive_ext (cuid) VALUES ($cuid)";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$query = "SELECT * FROM comprehensive_ext WHERE cuid=$cuid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
}
$row = mysql_fetch_assoc($result);
?>

<p>Basement Windows: <?php print $row['basementwindows']; ?>&nbsp;&nbsp;&nbsp;Attic Windows: <?php print $row['atticwindows']; ?>&nbsp;&nbsp;&nbsp;Garage Windows: <?php print $row['garagewindows']; ?>&nbsp;&nbsp;&nbsp;<a href="inspection-comprehensive-ext-window.php?cuid=<?php print $cuid; ?>">Change</a></p>

<p>Diagrams:&nbsp;&nbsp;&nbsp;<?php print $diagrams; ?>&nbsp;&nbsp;&nbsp;<a href="inspection-comprehensive-unit-diagrams.php?cuid=<?php print $cuid; ?>">Edit</a></p>

<hr />
<!--
<p><a href="inspection-comprehensive-ext-addcomponent1.php?cuid=<?php print $cuid; ?>">Add Standard Component</a>&nbsp;&nbsp;
|&nbsp;&nbsp;<a href="inspection-comprehensive-ext-addcustom1.php?cuid=<?php print $cuid; ?>">Add Custom Component</a></p>
-->
<table>
<tr><td style="vertical-align: top">
<?php

$query = "SELECT ccid, comprehensive_ext_components.name, side, abbreviation, xrf, hazards, hazardassessment FROM comprehensive_ext_components LEFT JOIN substrates USING (sid) WHERE cuid=$cuid ORDER BY comprehensive_ext_components.displayorder, comprehensive_ext_components.ccid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result)) {
?>
<table class="info">
<tr><th>Component</th><th><a href="inspection-comprehensive-ext-component-sides.php?cuid=<?php print $cuid;?>">Side</a></th><th>Substrate</th><th>Condition</th><th>XRF</th><th>Assessment</th><th>Delete</th></tr>
<?php
	$condmap = array('Intact' => 'I', 'Intact Where Visible' => 'IWV', 'Damaged Touchup' => 'DT', 'Damaged' => 'D', 'Assumed Damaged' => 'D', 'No Paint' => 'N', 'Post \'77' => '78', 'Damaged - Friction' => 'DF', 'Covered' => 'CV', 'Intact - Factory Finish' => 'IFF', 'Door Missing' => 'DM');
	while ($row = mysql_fetch_assoc($result)) {
		$condition = $row['hazards'];
		print "<tr><td><a href=\"inspection-comprehensive-ext-component.php?ccid=$row[ccid]\">$row[name]</a></td><td>$row[side]</td><td>$row[abbreviation]</td><td>$condmap[$condition]</td><td>$row[xrf]</td><td>$row[hazardassessment]</td><td><a href=\"inspection-comprehensive-ext-component-delete.php?ccid=$row[ccid]&amp;cuid=$cuid\" class=\"red\">Delete</a></td></tr>\n";
	}
?>
</table>
<?php
}

?>
<p><a href="inspection-comprehensive-ext-paintchips.php?cuid=<?php print $cuid; ?>">Paint Chip Samples</a></p>

<p><a href="inspection-comprehensive-xrfcalibration.php?cuid=<?php print $cuid; ?>">XRF Calibration</a><br />
<a href="inspection-comprehensive-xrfsummary.php?cuid=<?php print $cuid;?>">XRF Summary</a></p>

<!--
<p><a href="inspection-comprehensive-water.php?cuid=<?php print $cuid; ?>">Water Inspection</a></p>
-->

<p><a href="inspection-comprehensive-ext-samplesummary.php?cuid=<?php print $cuid;?>">Sample Summary</a></p>

<p><a href="inspection-comprehensive-unit-notes.php?cuid=<?php print $cuid; ?>">Private Unit Inspection Notes</a></p>

<p><a href="inspection-main.php?iid=<?php print $unit[iid]; ?>">Inspection Main Menu</a></p>

</td><td style="vertical-align: top; padding-left: 1em"><b>Add component:</b>
<?
$query = "SELECT * FROM room_item_list WHERE type='Exterior'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while ($row = mysql_fetch_assoc($result)) {
	print "<br /><a href=\"inspection-comprehensive-ext-addcomponent.php?cuid=$cuid&amp;itemid=$row[itemid]\">$row[itemname]</a>\n";
}
?>
<br /><a href="inspection-comprehensive-ext-addcustom1.php?crid=<?php print $cuid; ?>">Custom Component</a>
</td></tr>
</table>

<?php
require_once'footer.php';
?>