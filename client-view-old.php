<?php 
$title = "View client";

require_once'session.php';
require_once'connect.php';

$cid = $_POST['cid'] or $cid = $_GET['cid'];

$cid += 0; // (force numbers)

$query = "SELECT * FROM client WHERE cid=$cid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/client-list.php");
	exit();
}

$row = mysql_fetch_assoc($result);
/*
print "<!--\n";
print_r($row);
print "\n-->";
*/

require_once'header.php';
?>

<p><a href="client-view?cid=<?php print $cid;?>">New version</a></p>

<p><a href="client-edit.php?cid=<?php print $cid; ?>">Edit</a></p>
<?php
print "<p>$row[firstname] $row[lastname]<br />\n";
if ($row['company']) {print "$row[company]<br />\n";}
print "$row[streetnum] $row[address] $row[suffix]<br />\n";
if ($row['address2']) {print "$row[address2]<br />\n";}
print "$row[city], $row[state] $row[zip]<br />
$row[email]<br />\n";

$query = "SELECT * FROM client_phone WHERE cid=$cid";
$result2 = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while ($row2 = mysql_fetch_assoc($result2) ) {
	print "($row2[type]) $row2[number]&nbsp;&nbsp;&nbsp;";
	if ($row2['ext']) { print "Ext: $row2[ext]"; }
	print "<br />\n";
}
print "</p>\n";

print "<p>Class completed: $row[class_comp]";
if ($row['class_comp'] == 'Yes') {
	print "<br />\nClass taken with: $row[class_with]";
}
print "</p>\n";

if ($row['comments']) {
	print "\n<p>Comments:<br />$row[comments]</p>";
}
?>
<hr />
<p>Add another phone number to this client:</p>
<form action="client-addnumber.php" method="post">
<p>
<input type="hidden" name="cid" value="<?php print $cid; ?>" />
Number: <input type="text" name="phone1" maxlength="3" size="4" />-<input type="text" name="phone2" maxlength="3" size="4" />-<input type="text" name="phone3" maxlength="4" size="5" /> Ext: <input type="text" name="phoneext" maxlength="5" size="6" /> Type: <select name="phonetype">
<option value="Work">Work</option>
<option value="Home">Home</option>
<option value="Cell">Cell</option>
<option value="Fax">Fax</option>
</select><br />
<input type="submit" value="Add" />
</p>
</form>

<p><a href="building-add.php?cid=<?php print $cid; ?>">Add a NEW building to this client (as owner)</a></p>

<?php
$query = "SELECT building.bid, streetnum, address, suffix, address2, city, state, zip FROM building INNER JOIN owners USING (bid) WHERE cid=$cid AND startdate <= DATE(NOW()) AND enddate > DATE(NOW())";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result)) {
	print "<hr /><p><b>This client owns:</b><br />\n";
	while ($row = mysql_fetch_assoc($result)) {
		print "<a href=\"building-view.php?bid=$row[bid]\">$row[streetnum] $row[address] $row[suffix], $row[address2], $row[city], $row[state], $row[zip]</a><br />";
	}
	print "</p><hr />\n";
}
?>
<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>