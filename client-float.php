<?php
/*
 * Created on Dec 29, 2005
 *
 * Author: Jason Rankin
 */
 require_once'session.php';
 require_once'connect.php';
 
?>
<head>
<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body style="background: #D5DFF5;"/>
<SCRIPT LANGUAGE="JavaScript">

function popUp(URL) {
day = new Date();
id = day.getTime();
eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=825,height=530,top=575');");
}
//document.searchForm.firstname.value='blah';
</script>
<?php



$cid = isset($_POST['cid']) ? $_POST['cid'] : (isset($_GET['cid']) ? $_GET['cid'] : 0);

$showall = isset($_POST['showall']) ? $_POST['showall'] : (isset($_GET['showall']) ? $_GET['showall'] : 0);

if ($cid==0) {
	?>
	
	
	<form action="client-searchresults.php" method="post" name="searchForm" target="_parent">
	<input type="hidden" name="float" value="true"/>
	<table>
	<tr><td>First name:</td><td><input type="text" name="firstname" maxlength="30" value="<?php echo isset($_POST['firstname']);?>"/></td></tr>
	<tr><td>Last name:</td><td><input type="text" name="lastname" maxlength="30" /></td></tr>
	<tr><td>Company:</td><td><input type="text" name="company" maxlength="75" /></td></tr>
	<tr><td>City:</td><td><input type="text" name="city" maxlength="30" /></td></tr>
	<tr><td>State:</td><td><input type="text" name="state" maxlength="2" size="3" /></td></tr>
	<tr><td>Zip:</td><td><input type="text" name="zip" maxlength="5" size="6" /></td></tr>
	<tr><td>Phone:</td><td><input type="text" name="phone1" maxlength="3" size="4" />-<input type="text" name="phone2" maxlength="3" size="4" />-<input type="text" name="phone3" maxlength="4" size="5" /></td></tr>
	</table>
	<p><input type="submit" value="Search" /> <input type="reset" value="Reset" /></p>
	</form>
	<?php
}
else {
	$query = "SELECT * FROM client WHERE cid=$cid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$row = mysql_fetch_assoc($result);
	$cid = $row['cid'];



	$mailingstreetnum = isset($row['mailingstreetnum']) ? $row['mailingstreetnum'] : "";
	$mailingcity = isset($row['mailingcity']) ? $row['mailingcity'] : "";
	$mailingaddress = isset($row['mailingaddress']) ? $row['mailingaddress'] : "";
	$mailingstate = isset($row['mailingstate']) ? $row['mailingstate'] : "";
	$mailingsuffix = isset($row['mailingsuffix']) ? $row['mailingsuffix'] : "";
	$mailingaddress2 = isset($row['mailingaddress2']) ? $row['mailingaddress2'] : "";
	$mailingzip = isset($row['mailingzip']) ? $row['mailingzip'] : "";

	$title = isset($row['title']) ? $row['title'] : "";
	$firstname = isset($row['firstname']) ? $row['firstname'] : "";
	$mi = isset($row['mi']) ? $row['mi'] : "";
	$lastname = isset($row['lastname']) ? $row['lastname'] : "";
	$streetnum = isset($row['streetnum']) ? $row['streetnum'] : "";
	$address = isset($row['address']) ? $row['address'] : "";
	$suffix = isset($row['suffix']) ? $row['suffix'] : "";
	$address2 = isset($row['address2']) ? $row['address2'] : "";
	$city = isset($row['city']) ? $row['city'] : "";
	$state = isset($row['state']) ? $row['state'] : "";
	$zip = isset($row['zip']) ? $row['zip'] : "";
	$company = isset($row['company']) ? $row['company'] : "";
	$companytitle = isset($row['companytitle']) ? $row['companytitle'] : "";




	?>
	<table>
		<tr>
			<td>
				<table frame="border" border="2" rules="none" cellspacing="1" cellpadding="1" width="360px"" >
				<tr><th class="box" align="left" colspan="3">Client Information <font size="-1"><a href="javascript:popUp('client-view.php?cid=<?php print $row['cid'];?>&isPopup=true')" class="box2" style="border:solid gray 1px">click to view</font></a></th></tr>
				<tr>
					<td class="box2"><?php print $title." ".$firstname." ".$mi." ".$lastname; ?><br>
					<?php print $streetnum." ".$address." ".$suffix." ".$address2; ?><br>
					<?php print $city.", ".$state." ".$zip; ?></td>
				
					<td class="box2" colspan="2" valign="top">Company: <?php print $company; ?><br>
					Title: <?php print $companytitle; ?></td>
				</tr>
					<td class="box2" colspan="3">Mailing Address:<br>
					<?php print $mailingstreetnum." ".$mailingaddress." ".$mailingsuffix." ".$mailingaddress2; ?><br>
					<?php print $mailingcity.", ".$mailingstate." ".$mailingzip; ?></td>
				</tr>
				
				<tr>
				<?php
				
				$query = "SELECT * FROM client_phone WHERE cid=$row[cid]";
				$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
				
				$phonecount = 0;
				$phonerow = mysql_fetch_assoc($result);
				$phonerow[1] = isset($phonerow['number']) ? substr($phonerow['number'], 0, 3) : "";
				$phonerow[2] = isset($phonerow['number']) ? substr($phonerow['number'], 4, 3) : "";
				$phonerow[3] = isset($phonerow['number']) ? substr($phonerow['number'], 8) : "";
				
				?>
					<td class="box2">Phone (1): <?php print "$phonerow[1]-$phonerow[2]-$phonerow[3]"; ?></td>
					<td class="box2">Ext: <?php print isset($phonerow['ext']) ? $phonerow['ext'] : '' ?></td>
					<td class="box2">Type: <?php print isset($phonerow['type']) ? $phonerow['type'] : '' ?></td>
				</tr>
				<tr>
				<?php
				$phonerow = mysql_fetch_assoc($result);
				$phonerow[1] = isset($phonerow['number']) ? substr($phonerow['number'], 0, 3) : "";
				$phonerow[2] = isset($phonerow['number']) ? substr($phonerow['number'], 4, 3) : "";
				$phonerow[3] = isset($phonerow['number']) ? substr($phonerow['number'], 8) : "";
				?>
					<td class="box2">Phone (2): <?php print "$phonerow[1]-$phonerow[2]-$phonerow[3]"; ?></td>
					<td class="box2">Ext: <?php print isset($phonerow['ext']) ? $phonerow['ext'] : '' ?></td>
					<td class="box2">Type: <?php print isset($phonerow['type']) ? $phonerow['type'] : '' ?></td>
				</tr>
				<tr>
				<?php
				$phonerow = mysql_fetch_assoc($result);
				$phonerow[1] = isset($phonerow['number']) ? substr($phonerow['number'], 0, 3) : "";
				$phonerow[2] = isset($phonerow['number']) ? substr($phonerow['number'], 4, 3) : "";
				$phonerow[3] = isset($phonerow['number']) ? substr($phonerow['number'], 8) : "";
				?>
					<td class="box2">Phone (3): <?php print "$phonerow[1]-$phonerow[2]-$phonerow[3]"; ?></td>
					<td class="box2">Ext: <?php print isset($phonerow['ext']) ? $phonerow['ext'] : "" ?></td>
					<td class="box2">Type: <?php print isset($phonerow['type']) ? $phonerow['type'] : "" ?></td>
				</tr>
				<tr>
					<td class="box2" colspan="2">E-mail: <?php print $row['email']; ?></td>
					<td class="box2">Class completed: <?php print $row['class_comp']; ?></td> 
				</tr>
				<tr>
					<td class="box2">Class taken with: <?php print $row['class_with']; ?></td>
					<td class="box2" colspan="2">Relationship: </php print $ros['relationship']; ?></td>
				</tr>
				</table>
				<p> </p>
			</td>
		</tr>
	</table>

<?php
}
//if (mysql_num_rows($result) == 0) {
	//header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/client-list.php");
	//exit();
//}
?>
<script type="text/javascript">
  //window.parent.fill_form()
</script>


