<?php

$pdf->AddPage('P');
$pdf->SetFont('Times','',10);
$pdf->SetLineWidth(0.014);

$x=0;
$y=0;
$ml=0.625;
$w=7.25;
$ff=65;
$fontsize=10;
require_once'include-form-letterhead.php';

$query = "SELECT building.bid, streetnum, address, suffix, city, state, zip, numunits,designation, yearbuilt, plat, lot, inspection.iid, unitdesc, reason, number, units.cuid FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);
$iid = $row['iid'];
$bid = $row['bid'];
$cuid = $row['cuid'];
$reason = $row['reason'];

$builaddress = "$row[streetnum] $row[address] $row[suffix]";
$buildingcitystzip = "$row[city],', ', $row[state],'  ', $row[zip]";

/////
$yd=$y;
$lh = 0.4;
$pdf->SetXY($ml+$w-1.5, $yd);
$pdf->Cell(0,0, "Orig Date");
$yd=$yd+$fontsize/$ff;
$pdf->SetXY($ml+$w-1.5, $yd);
$pdf->Cell(0,0, "Inspection $row[iid]-$row[number]");




///add colon separator if both
//OWNER
$query = "SELECT client.cid, firstname, lastname, company, streetnum, address, suffix, city, state, zip FROM client INNER JOIN owners USING (cid) WHERE bid=$bid AND startdate <= DATE(NOW()) AND enddate >= DATE(NOW())";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) {
	exit("Building requires owner. Form blows up without owner.");
}
$row = mysql_fetch_assoc($result);
formfix($row);
$cid = $row['cid'];

$basex = $ml; 
$basey = $y;

$pdf->SetXY($ml, $y);
if ($row['firstname']=='' or $row['lastname']=='') {
	$pdf->Cell(0,0,"$row[company]");
}
else {
	$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
}
$y=$y+$fontsize/$ff;
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0, "$row[streetnum] $row[address] $row[suffix]");
$y=$y+$fontsize/$ff;
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0, "$row[city], $row[state],  $row[zip]");
$y=$y+$fontsize/$ff;
$y=$y+$fontsize/$ff;
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0, "$row[title] were is title $row[lastname] ,");
$y=$y+$fontsize/$ff;
$y=$y+$fontsize/$ff;
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0, "Start letter");

/*
$query = "SELECT type, number, ext FROM client_phone WHERE cid=$cid GROUP BY type";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while ($row = mysql_fetch_assoc($result)) {
	formfix($row);
	if ($row['ext']) {
		$nums[$row['type']] = "$row[number] ext. $row[ext]";
	} else {
		$nums[$row['type']] = "$row[number]";
	}
}

$pdf->SetXY($basex, $basey + 0.39);
if ($nums['Home']) {
	$pdf->Cell(0,0, $nums['Home']);
} else if ($nums['Cell']) {
	$pdf->Cell(0,0, $nums['Cell'] . ' (Cell)');
	$cellused = 1;
}
$pdf->SetXY($basex + 3.5, $basey + 0.39);
if ($nums['Work']) {
	$pdf->Cell(0,0, $nums['Work']);
} else if ($nums['Cell'] && $cellused < 1) {
	$pdf->Cell(0,0, $nums['Cell'] . ' (Cell)');
}

//TENANT
$query = "SELECT tenant, tenantyears, under6 FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

$pdf->SetXY(1.25, 4.46);
$pdf->Cell(0,0, $row['tenant']);
$pdf->SetXY(2.75, 4.80);
$pdf->Cell(0,0, $row['tenantyears']);
if ($row['under6']) {
	$pdf->SetXY(5.90, 4.46);
} else {
	$pdf->SetXY(6.90, 4.46);
}
$pdf->Cell(0,0, 'X');

//INSPECTORS
$query = "SELECT firstname, lastname, elt, eli FROM inspector INNER JOIN insp_assigned USING (tid) INNER JOIN unit_insp_assigned USING (assig_id) WHERE iid=$iid AND unit_insp_assigned.cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$x = $mody = 0;
$basex = 3.05; $basey = 5.52;
while (($row = mysql_fetch_assoc($result)) && $x < 2) {
	formfix($row);
	if ($row['eli']) {
		$pdf->SetXY($basex, $basey + $mody);
		$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
		$pdf->SetXY($basex + 2.88, $basey+$mody);
		$pdf->Cell(0,0, "ELI-$row[eli]");
	} else {
		if ($x == 0) {
			$mody += 0.7;
			$x++;
		}
		$pdf->SetXY($basex, $basey + $mody);
		$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
		$pdf->SetXY($basex + 2.88, $basey+$mody);
		$pdf->Cell(0,0, "ELT-$row[elt]");
	}
	$mody += 0.7;
	$x++;
}

while ($x < 2) {
	$mody += 0.7; //bump down if no second inspector for time
	$x++;
}

$mody -= 0.07; //whatever
$query = "SELECT DATE_FORMAT(starttime, '%c/%e/%Y') AS inspdate, DATE_FORMAT(starttime, '%l:%i %p') AS insptime FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
$pdf->SetXY($basex, $basey + $mody);
$pdf->Cell(0,0, "$row[inspdate]");
$pdf->SetXY($basex + 2.90, $basey + $mody);
$pdf->Cell(0,0, "$row[insptime]");

switch ($reason) {
	case 'Independent Clearance Inspection':
		$pdf->SetXY(0.62, 7.68);
		break;
	case 'Tenant Complaint':
		$pdf->SetXY(0.62, 7.96);
		break;
	case 'Presumptive Compliance':
		$pdf->SetXY(0.62, 8.21);
		break;
	case 'Code Enforcement':
		$pdf->SetXY(0.62, 8.46);
		break;
	case 'Visual Inspection':
		$pdf->SetXY(3.68, 7.68);
		break;
	case 'Private Client - Property Transfer':
		$pdf->SetXY(3.68, 7.96);
		break;
	case 'Private Client':
		$pdf->SetXY(3.68, 8.21);
		break;
	default:
		$pdf->SetXY(4.54, 8.49);
		$pdf->Cell(0,0, $row['reason']);
		$pdf->SetXY(3.68, 8.46);
		break;
}
$pdf->Cell(0,0, 'X');

//PAGE NUMBERS
$y=10.5;
$pdf->SetFont('Times','','10');
$pdf->SetXY(6.5, $y);
$pdf->Cell(0,0, 'Page _____ of _____');
$pdf->SetXY(6.9, $y);
$pdf->Cell(0, 0, $pdf->PageNo());
$pdf->SetXY(7.4, $y);
$pdf->Cell(0,0, '{totalpages}');
*/
?>