change log 4/29/2006
5 pages
1 table to be added to the database


inspector-list.php
	-added lead capable column

inspector-view.php
	-line 62: added print "<br> Lead Capable: $row[lead_capable]";

inspector-edit.php
	-line 9:  added 'lead_capable' to the select statement
	-line 83:  added code to list the lead capable status of the inspector

inspection-unitinfo.php
	-line 37 added onUnload event

inpsection-unitupdate.php
	-line 24: changed true to false
		if under6null is true,  no matter what is entered into the under6 field, a null value is inserted	

table:
CREATE TABLE `building_associated_people` (
`bid` INT( 4 ) NOT NULL COMMENT 'Building ID',
`apid` INT( 4 ) NOT NULL COMMENT 'Associated Person ID',
`name` VARCHAR( 100 ) NOT NULL COMMENT 'Name of Associated Person',
`title` VARCHAR( 50 ) NOT NULL ,
`report_expectations` VARCHAR( 100 ) NOT NULL ,
`building_relationship` VARCHAR( 100 ) NOT NULL ,
`start_date` DATETIME NOT NULL ,
`end_date` DATETIME NOT NULL ,
`comments` TEXT NOT NULL ,
`how_sent` VARCHAR( 100 ) NOT NULL ,
`authorized` VARCHAR( 50 ) NOT NULL 
) ENGINE = MYISAM COMMENT = 'tracks individuals associated with a specific building';

