<?php


require_once'session.php';
require_once'connect.php';

/*
print_r($_POST);
exit();
*/



//shortcut - all fields are text
foreach ($_POST as $field => $value) {
	$fieldlist[] = $field;
	$valuelist[] = htmlspecialchars($value);
}

/*
Because htmlspecialchars() is being performed on the data, it is possible that some data may be truncated. This can happen if, for example, the data contains '&', which becomes '&amp;', and the resulting string is greater than the allowed column width. However, the column widths are rather generous, and it only affects a few characters, so as such, it was deemed not worth checking the various lengths of each field to see if they would actually be truncated. It should be noted that if no substitutions are performed, the 'maxlength' attribute on the form elements will prevent an entry from being truncated.
*/


$query = "INSERT INTO productlist (".implode(',', $fieldlist).") VALUES ('".implode("','", $valuelist)."')";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$query = "SELECT LAST_INSERT_ID()";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$itemID = mysql_result($result, 0);


header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/product-view.php?itemID=$itemID");
?>