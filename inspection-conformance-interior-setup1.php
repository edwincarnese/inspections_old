<?php

require_once'session.php';
require_once'connect.php';

$query = "SELECT iid, unitdesc, number FROM units WHERE cuid=$_GET[cuid]";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
list($iid, $unitdesc, $unitnumber) = mysql_fetch_row($result);

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - $unitdesc - Interior Setup";
require_once'header.php';
?>
<form action="inspection-conformance-interior-setup2.php" method="post">
<input type="hidden" name="cuid" value="<?php print $_GET['cuid']; ?>" />
<p>How many rooms are in this unit?</p>
<p>
<input type="radio" name="digit1" value="0" />0&nbsp;&nbsp;<input type="radio" name="digit2" value="0" />0<br />
<input type="radio" name="digit1" value="1" />1&nbsp;&nbsp;<input type="radio" name="digit2" value="1" checked="checked" />1<br />
<input type="radio" name="digit1" value="2" />2&nbsp;&nbsp;<input type="radio" name="digit2" value="2" />2<br />
<input type="radio" name="digit1" value="3" />3&nbsp;&nbsp;<input type="radio" name="digit2" value="3" />3<br />
<input type="radio" name="digit1" value="4" />4&nbsp;&nbsp;<input type="radio" name="digit2" value="4" />4<br />
<input type="radio" name="digit1" value="5" />5&nbsp;&nbsp;<input type="radio" name="digit2" value="5" />5<br />
<input type="radio" name="digit1" value="6" />6&nbsp;&nbsp;<input type="radio" name="digit2" value="6" />6<br />
<input type="radio" name="digit1" value="7" />7&nbsp;&nbsp;<input type="radio" name="digit2" value="7" />7<br />
<input type="radio" name="digit1" value="8" />8&nbsp;&nbsp;<input type="radio" name="digit2" value="8" />8<br />
<input type="radio" name="digit1" value="9" />9&nbsp;&nbsp;<input type="radio" name="digit2" value="9" />9<br />
<input type="submit" value="Next" />
</p>
</form>
<p><a href="inspection-view.php?iid=<?php print $iid; ?>">Back</a></p>
<?php
require_once'footer.php';
?>