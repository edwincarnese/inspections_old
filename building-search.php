<?php 

require_once'session.php';

$title = "Search Buildings";
require_once'header.php';
?>

<p>Enter as much information as is known.</p>

<form action="building-searchresults.php" method="post">
<table>
<tr><td>Address:</td><td><input type="text" name="streetnum" maxlength="8" size="5" /><input type="text" name="address" maxlength="30" /><input type="text" name="suffix" maxlength="10" size="7" /></td></tr>
<tr><td>Address 2:</td><td><input type="text" name="address2" maxlength="30" /></td></tr>
<tr><td>City:</td><td><input type="text" name="city" maxlength="30" /></td></tr>
<tr><td>State:</td><td><input type="text" name="state" maxlength="2" size="3" /></td></tr>
<tr><td>Zip:</td><td><input type="text" name="zip" maxlength="5" size="6" /></td></tr>
<tr><td>Year Built:</td><td><input type="text" name="yearbuilt" maxlength="6" size="7" /></td></tr>
</table>
<p><input type="submit" value="Search" /> <input type="reset" value="Reset" /></p>
</form>
<p><a href="building-add.php">Add New Building</a></p>
<p><a href="building-list.php">View Building List</a></p>
<p><a href="index.php">Main Menu</a></p>

<?php
require_once'footer.php';
?>