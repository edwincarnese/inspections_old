<?php

$x = 0;
$y = 0;
$ml=.625;
$w=7.25;
$pdf->SetFont('Times','','12');
$pdf->SetLineWidth(0.014);
//$pdf->SetLineWidth(0.04);
//Start Banner box package
$y=$y+0.5; 		//Increment Y from last input.
$ys=$y-0.2;  	//Save Y at start.
$ff=65;			//Chang in $y = $fontsize/$ff
////
$Y=$Y+1;
$ys=$y; // This adjusts the boxes height
$fontsize=10;
$ff=65;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'INTERIOR DUST CLEARANCE:');
$y=$y+$fontsize/$ff;
$pdf->Rect($ml, $ys-0.08, $w, $y-$ys);
$y=$y+0.15;
//Unit and address
$x=$ml;
$y=$y+0.06;
$pdf->SetXY($ml, $y);
$pdf->Cell(0, 0, 'City/Street:');
$pdf->SetXY($ml+0.75, $y);
$pdf->Cell(0,0, "$row[streetnum] $row[address] $row[suffix], $row[city]");
$pdf->Line($ml+0.74, $y+0.05, $ml+3.6, $y+0.05);
$pdf->SetXY(4.3, $y);
$pdf->Cell(0, 0, 'Floor/Unit Number:');
$pdf->Line($ml+5, $y+0.05, $ml+$w, $y+0.05);


//Sampling method.
$x=$ml;
$y=$y+.3;
$pdf->SetXY($x, $y);
$pdf->Cell($x, 0, 'Sampling Method: (circle one)');
$pdf->SetFont('Times','B','10');
$pdf->SetXY($x+2.33, $y);
$pdf->Cell($x, 0, 'Wet Wipe       Vacuum');
// Rectangle around "Wet Wipe"
// Rectangle around "Vacuum"
//$pdf->Rect($x+2.7, $y-0.09, 0.6, 0.17);
$pdf->SetFont('Times','','10');

//Sampling Date line
$x=$ml;
$y=$y+.35;
$pdf->SetXY($x, $y);
$pdf->Cell($x, 0, 'Sampling Date:');
$pdf->Line($ml+0.95, $y+0.05, $ml+2.375, $y+0.05);
$x=$x+2.865;
$pdf->SetXY($x, $y);
$pdf->Cell(0, 0, 'Laboratory Utilized:');
$pdf->Line($x+1.25, $y+0.05, $ml+7.27, $y+0.05);

//Sample Grid
$y=$y+.25;
$x=$ml;
$h=3.02;
$pdf->SetLineWidth(0.01);
$pdf->Line($ml, $y, $ml, $y+$h);
$x=$ml+1.5;
$pdf->Line($x, $y, $x, $y+$h);
$x=$ml+2;
$pdf->Line($x, $y, $x, $y+$h);
$x=$ml+2.875;
$pdf->Line($x, $y, $x, $y+$h);
$x=$ml+3.5;
$pdf->Line($x, $y, $x, $y+$h);
$x=$ml+4.375;
$pdf->Line($x, $y, $x, $y+$h);
$x=$ml+5.125;
$pdf->Line($x, $y, $x, $y+$h);
$x=$ml+6;
$pdf->Line($x, $y, $x, $y+$h);
$x=$ml+6.5;
$pdf->Line($x, $y, $x, $y+$h);
$x=$ml+7.25;
$pdf->Line($x, $y, $x, $y+$h);

$pdf->Line($ml, $y, $ml+7.25, $y);

//Grid Headings
$pdf->SetXY($ml+.2, $y+.1);
$pdf->Cell(0,0, 'Testing Location');
$pdf->SetXY($ml+.3, $y+.25);
$pdf->SetFont('Times','','8');
$pdf->Cell(0,0, '(Room / Side)');
$pdf->SetXY($ml+.2, $y+.4);
$pdf->SetFont('Times','','10');
$pdf->Cell(0,0, '');

$pdf->SetXY($ml+1.55, $y+.1);
$pdf->Cell(0,0, 'Paint');
$pdf->SetXY($ml+1.5, $y+.25);
$pdf->Cell(0,0, '(Chips)');
$pdf->SetXY($ml+1.55, $y+.4);
$pdf->Cell(0,0, 'Y / N');

$pdf->SetXY($ml+2.125, $y+.1);
$pdf->Cell(0,0, 'Sampled');
$pdf->SetXY($ml+2.225, $y+.25);
$pdf->Cell(0,0, 'Area');
$pdf->SetXY($ml+2.05, $y+.4);
$pdf->Cell(0,0, 'Dimensions');

$pdf->SetXY($ml+3.03, $y+.1);
$pdf->Cell(0,0, 'Lab');
$pdf->SetXY($ml+2.93, $y+.25);
$pdf->Cell(0,0, 'Sample');
$pdf->SetXY($ml+2.9, $y+.4);
$pdf->Cell(0,0, 'Number');

$pdf->SetXY($ml+3.55, $y+.1);
$pdf->Cell(0,0, 'Micrograms/');
$pdf->SetXY($ml+3.75, $y+.25);
$pdf->Cell(0,0, 'Wipe');
$pdf->SetXY($ml+3.7, $y+.4);
$pdf->Cell(0,0, 'Result');

$pdf->SetXY($ml+4.4, $y+.1);
$pdf->Cell(0,0, 'Conversion');
$pdf->SetXY($ml+4.55, $y+.25);
$pdf->Cell(0,0, 'Factor');
$pdf->SetXY($ml+.2, $y+.4);
$pdf->Cell(0,0, '');

$pdf->SetXY($ml+5.18, $y+.1);
$pdf->Cell(0,0, 'Micrograms/');
$pdf->SetXY($ml+5.22, $y+.25);
$pdf->Cell(0,0, 'square foot');
$pdf->SetXY($ml+5.35, $y+.4);
$pdf->Cell(0,0, 'Result');

$pdf->SetXY($ml+6.1, $y+.1);
$pdf->Cell(0,0, 'Spot');
$pdf->SetXY($ml+6.1, $y+.25);
$pdf->Cell(0,0, 'Test');
$pdf->SetXY($ml+6.05, $y+.4);
$pdf->Cell(0,0, '(+ / -)');

$pdf->SetXY($ml+6.625, $y+.1);
$pdf->Cell(0,0, 'Hazard');
$pdf->SetXY($ml+6.5, $y+.25);
$pdf->Cell(0,0, 'Assessment');
$pdf->SetXY($ml+6.6, $y+.4);
$pdf->Cell(0,0, 'F / S / H');

$y=$y+.47;
$c=0;
$n=0;
while ($n < 9) { 
	$pdf->Line($ml, $y+$c, $ml+7.25, $y+$c);
	$c=$c+.32;
	$n=$n+1;
}

$y=$y+$h-0.55;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0, 0, 'Field Blank');

//nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn
$y=$y+0.5;
$ys=$y; // This adjusts the boxes height
$fontsize=10;
$ff=65;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'Key to Headings');
$y=$y+$fontsize/$ff;
$pdf->Rect($ml, $ys-0.08, $w, $y-$ys);
$y=$y+0.2;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0, 0, 'Hazard Assessment:    F = Lead-Free    S = Lead-Safe    H = Lead Hazard');

// Comments
$y=$y+0.3;
$yi=0.16;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0, 0, 'Comments:');
$y=$y+0.23;
$count=8;
$pdf->SetLineWidth(0.01);
$c=1;

while ($c <= $count) { 
	$pdf->Line($ml, $y, $ml+$w, $y);
	$y=$y+$yi;
	$c=$c+1;
}

$y=$y+0.2;
$pdf->SetXY($ml, $y);
$pdf->Cell(0, 0, 'Note to Property Owner: A complete report must include a copy of the laboratory results from the Laboratory Company');
$y=$y+$fontsize/$ff;
$pdf->SetXY($ml, $y);
$pdf->Cell(0, 0, 'identified above.');

$y=9.94;
$pdf->SetXY($ml+1, $y);
$pdf->Cell(0,0,'Initials');
$pdf->SetXY($ml+2.9, $y);
$pdf->Cell(0,0,'Date:');
$pdf->SetXY($ml+5, $y);
$pdf->Cell(0,0,'Time:');
$y=$y+0.055;
$pdf->Line($ml+1.5, $y, $ml+2.5, $y);
$pdf->Line($ml+3.4, $y, $ml+4.5, $y);
$pdf->Line($ml+5.5, $y, $ml+6.5, $y);

////////////////
$y=10.5;
$pdf->SetFont('Times','B', 9);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'HRC LHM FORM -2 (4/04)');


?>