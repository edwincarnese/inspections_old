<?php 

require_once'session.php';

$title = "Add a client";
require_once'header.php';
?>

<form action="client-save.php" method="post">
<table>
<tr><td>First name:</td><td><input type="text" name="firstname" maxlength="30" /></td></tr>
<tr><td>Last name:</td><td><input type="text" name="lastname" maxlength="30" /></td></tr>
<tr><td>Company:</td><td><input type="text" name="company" maxlength="75" /></td></tr>
<tr><td>Address:</td><td><input type="text" name="streetnum" maxlength="8" size="5" /><input type="text" name="address" maxlength="30" /><input type="text" name="suffix" maxlength="10" size="7" /></td></tr>
<tr><td>Address2:</td><td><input type="text" name="address2" maxlength="30" /></td></tr>
<tr><td>City:</td><td><input type="text" name="city" maxlength="30" /></td></tr>
<tr><td>State:</td><td><input type="text" name="state" maxlength="2" size="3" /></td></tr>
<tr><td>Zip:</td><td><input type="text" name="zip" maxlength="5" size="6" /></td></tr>
<tr><td>Phone (1):</td><td><input type="text" name="phones[1][1]" maxlength="3" size="4" />-<input type="text" name="phones[1][2]" maxlength="3" size="4" />-<input type="text" name="phones[1][3]" maxlength="4" size="5" /> Ext: <input type="text" name="phones[1][ext]" maxlength="5" size="6" /> Type: <select name="phones[1][type]">
<option value="Work">Work</option>
<option value="Home">Home</option>
<option value="Cell">Cell</option>
<option value="Fax">Fax</option>
</select></td></tr>
<tr><td>Phone (2):</td><td><input type="text" name="phones[2][1]" maxlength="3" size="4" />-<input type="text" name="phones[2][2]" maxlength="3" size="4" />-<input type="text" name="phones[2][3]" maxlength="4" size="5" /> Ext: <input type="text" name="phones[2][ext]" maxlength="5" size="6" /> Type: <select name="phones[2][type]">
<option value="Work">Work</option>
<option value="Home">Home</option>
<option value="Cell">Cell</option>
<option value="Fax">Fax</option>
</select></td></tr>
<tr><td>Phone (3):</td><td><input type="text" name="phones[3][1]" maxlength="3" size="4" />-<input type="text" name="phones[3][2]" maxlength="3" size="4" />-<input type="text" name="phones[3][3]" maxlength="4" size="5" /> Ext: <input type="text" name="phones[3][ext]" maxlength="5" size="6" /> Type: <select name="phones[3][type]">
<option value="Work">Work</option>
<option value="Home">Home</option>
<option value="Cell">Cell</option>
<option value="Fax">Fax</option>
</select></td></tr>
<tr><td>E-mail:</td><td><input type="text" name="email" maxlength="50" /></td></tr>
<tr><td>Class completed: </td><td> <input type="radio" name="class_comp" value="Yes" /> Yes&nbsp;&nbsp;&nbsp;<input type="radio" name="class_comp" value="No" /> No</td></tr>
<tr><td>Class taken with:</td><td><input type="text" name="class_with" maxlength="50" /></td></tr>
<tr><td>Comments:</td><td><textarea cols="40" rows="5" name="comments"></textarea></td></tr>
</table>
<p><input type="submit" value="Save" /> <input type="reset" value="Reset" /></p>
</form>

<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>