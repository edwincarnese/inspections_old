<?php
 
$pdf->AddPage('P');
 
$x = 0;
$y = 0;
$ml=0.5;
$ff=65;
$w=7.5; 
$pdf->SetFont('Arial','',14);
$pdf->SetLineWidth(0.01);

 
//PAGE NUMBERS

$query = "SELECT building.bid, streetnum, address, suffix, city, state, zip, numunits, yearbuilt, plat, lot, inspection.iid, unitdesc, section8, publichousing, starttime, scheddate, number, designation, type, units.owneroccupied as unitowneroccupied, inspection.owneroccupied as inspowneroccupied FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";

$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

$inspowneroccupied=$row['inspowneroccupied'];
$unitowneroccupied=$row['unitowneroccupied'];
$section8=$row['section8'];
$publichousing=$row['publichousing'];
$insptype=$row['type'];
$iid = $row['iid'];

//require_once'debugview.php';

$pdf->SetFont('Times','','12');
//$pdf->SetXY(7.12, 0.36);
$x=1.4;
$y=$y + 0.3 ;
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, "Inspection $row[iid]-$row[number]");

$pdf->SetXY(7,$y);
$pdf->Cell(0,0, 'Page '. $pdf->PageNo().' of  '. '{totalpages}');

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','B','16');
$pdf->SetXY($ml, $y);
$pdf->Cell( $w, 0.3,'Environmental Lead Inspection Report', 1, 0, 'C');
//$pdf->SetXY($x, $y+.1);
//$pdf->Cell(0,0,'Environmental Lead Inspection Report','B','c');
$pdf->SetFont('Times','B','13');
$x=$x+1;
$y=$y+.5;
$pdf->SetXY($x, $y);
$pdf->Cell(0,0,'Type of Inspection');
$pdf->SetXY($x, $y);
$pdf->Cell(0,0,'________________');
 
//ADDRESS


$lh = 0.4;
$y=$y+0.4;
//MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
//SQL statement to identify type of inspection.
/*
$query1 = "select * from units where cuid=$cuid";
$result1 = mysql_query($query1) or sql_crapout($query1.'<br />'.mysql_error());
$row1 = mysql_fetch_assoc($result1);
formfix($row1);
$insptype=$row1['insptype'];
*/
//MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM

$pdf->SetFont('Times','','10');     
$x=0.6;     
if ($insptype=='Comprehensive') {       
$pdf->SetXY($x,  $y);       
$pdf->Cell(0,0, 'X');       
        }
$pdf->SetXY($x+0.3,  $y);       
$pdf->Cell(0,0, 'Comprehensive');       
$pdf->SetXY($x-0.12,  $y);      
$pdf->Cell(0,0, '_____');       
        
$x=2.0;     
//$insptype='Limited'       
if ($insptype=='Limited') {             
$pdf->SetXY($x,  $y);       
$pdf->Cell(0,0, 'X');       
        }
$pdf->SetXY($x+0.3,  $y);       
$pdf->Cell(0,0, 'Limited');     
$pdf->SetXY($x-0.12,  $y);      
$pdf->Cell(0,0, '_____');       

$x=3.0;     
if ($insptype=='Annual Inspection') {               
$pdf->SetXY($x,  $y);       
$pdf->Cell(0,0, 'X');       
        }
$pdf->SetXY($x+0.3,  $y);       
$pdf->Cell(0,0, 'Annual Inspection');       
$pdf->SetXY($x-0.12,  $y);      
$pdf->Cell(0,0, '_____');       

$x=4.6;     
if ($insptype=='Comprehensive - Clearance')  {                  
$pdf->SetXY($x,  $y);       
$pdf->Cell(0,0, 'X');       
        }

if ($insptype=='Conformance Clearance')  {                  
$pdf->SetXY($x,  $y);       
$pdf->Cell(0,0, 'X');       
        }               
        $pdf->SetXY($x+0.3,  $y);
$pdf->Cell(0,0, 'Clearance Inspection');        
$pdf->SetXY($x-0.12,  $y);      
$pdf->Cell(0,0, '_____');       

$x=6.3;     
if ($insptype=='Lead Assessment') {                 
$pdf->SetXY($x,  $y);       
$pdf->Cell(0,0, 'X');       
        }       
        $pdf->SetXY($x+0.3,  $y);
$pdf->Cell(0,0, 'Lead Assessment');
$pdf->SetXY($x-0.12,  $y);
$pdf->Cell(0,0, '_____');


$y=$y+0.1;
$pdf->SetFont('Times','','11');
$pdf->SetXY(0.4, $y);
$pdf->line($ml, $y, $ml+$w, $y);
 
$iid = $row['iid'];
$bid = $row['bid'];
$scheddate = date("m/d/Y",strtotime($row['scheddate']));
$starttime = date("m/d/Y",strtotime($row['starttime']));

$pdf->SetFont('Times','B','13');
$x=3.5 ;
$y=$y+0.2;
$pdf->SetXY($x, $y);
$pdf->Cell(0,0,'Address Inspected');
$pdf->SetXY($x, $y);
$pdf->Cell(0,0,'________________');
$pdf->SetFont('Times','','12');

$pdf->Cell(0, 0, $row['unitdesc']);
$x=1.0;
$y=$y+.35;
$pdf->SetXY($ml, $y);
$pdf->Cell(3, 0, $row['streetnum'].'  '. $row['address'].'  '. $row['suffix'], 0, 0, 'C');
$x=3.75;
$pdf->SetXY($x, $y);
$pdf->Cell(0.75, 0, $row['designation'], 0, 0, 'C');
$x=$x+1.1;
$pdf->SetXY($x, $y);
$pdf->Cell(($w-$x),0, $row['city'].'    '.$row['zip'], 0, 0, 'C' );
/*
$pdf->Cell(0,0, $row['city']. ', '.$row['state'] $row['zip']);
*/
$pdf->SetXY(0.4,$y);
$pdf->Cell(0,0,'___________________________________');
$pdf->SetXY(3.67,$y);
$pdf->Cell(0,0,'__________');
$pdf->SetXY(4.75,$y);
$pdf->Cell(0,0,'______________________________________');
$y=$y+.17;
$pdf->SetXY(1.0,$y);
$pdf->Cell(0,0,'(No & Street Address)');
$pdf->SetXY(3.67,$y);
$pdf->Cell(0,0,'(Apt. / Floor)');
$pdf->SetXY(5.5,$y);
$pdf->Cell(0,0,'(City / Town & Zip Code)');
 
$query = "SELECT COUNT(*) FROM comprehensive_rooms WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$rooms = mysql_result($result, 0);
 
$x=0.5;
$y=$y+.32;
$pdf->SetXY($x+0.05, $y);
$pdf->Cell(0.625,0, $row['numunits'], 0, 0, 'C');
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, '__________');
$pdf->SetXY($x+.15, $y+.2);
$pdf->Cell(0,0, '(Units)');
 
$x=$x+1.1;
$pdf->SetXY($x+0.05, $y);
$pdf->Cell(0.625,0, $rooms, 0, 0, 'C' );
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, '__________');
$pdf->SetXY($x+.05, $y+.2);
$pdf->Cell(0,0, '(# Rooms) ');
 
$x=$x+1.1;
$pdf->SetXY($x+0.05, $y);
$pdf->Cell(0.625,0, $row['yearbuilt'], 0, 0, 'C');
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, '___________');
$pdf->SetXY($x+.05, $y+.2);
$pdf->Cell(0,0, '(Year Built)');
 
$x=$x+1.15;
$pdf->SetXY($x+0.05, $y);
$pdf->Cell(0.625,0, $row['plat'], 0, 0, 'C');
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, '__________');
$pdf->SetXY($x+.1, $y+.2);
$pdf->Cell(0,0, '  (Plat)  ');


$x=$x+1.1;
$pdf->SetXY($x+0.05, $y);
$pdf->Cell(0.625,0, $row['lot'], 0, 0, 'C');
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, '__________');
$pdf->SetXY($x+.15, $y+.2);
$pdf->Cell(0,0, '  (Lot)   ');

//check to see if buiding is owneroccupied
$boo = '';
$query = "SELECT * FROM units INNER JOIN inspection ON inspection.iid = units.iid INNER JOIN building ON building.bid = inspection.bid WHERE units.iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$under6 = mysql_result($result, 0, 'under6');
 while ($row = mysql_fetch_row($result)) {
    $isbuildingowneroccupied = issetBlank($row, 'owneroccupied');
    if ($isbuildingowneroccupied == 'Yes'){
    $boo = 'Yes';
    }
}
 
 
$x=$x+1.1;
$pdf->SetXY($x+0.05, $y);
$pdf->Cell(2.0,0, $under6, 0, 0, 'C');
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, '____________________');
$pdf->SetXY($x+.1, $y+.2);
$pdf->Cell(0,0, '(# Children Under 6)');
 
$pdf->SetFont('Times','','10');
$y=$y+0.65;
$owneroccupied = mysql_result($result, 0, 'owneroccupied');
$pdf->SetXY(0.4,$y);
$pdf->Cell(0,0,'Inspected Unit Occupied by Owner: Y/N        Premise Occupied by Owner: Y/N       Section 8: Y/N       Public Housing: Y/N');
$pdf->SetXY(0.4,$y+.05);
$pdf->line($ml, $y+0.15, $ml+$w, $y+0.15);
$yshift=-0.067;
$pdf->SetLineWidth(0.014);

//  Need to identify if building owner occupied

if($unitowneroccupied == 'Yes') {
$pdf->Rect(2.46, $y+$yshift, 0.14, 0.14);
$inspowneroccupied = $unitowneroccupied;
} else { // No
$pdf->Rect(2.61, $y+$yshift, 0.13, 0.14);
}       
/*
if($boo == 'Yes'){      
        $pdf->Rect(4.65, $y+$yshift, 0.13, 0.14);
} else {$pdf->Rect(4.8, $y+$yshift, 0.13, 0.14);}       
*/
//if($row['$owneroccupied'] == 'Yes') {     

if($inspowneroccupied == 'Yes') {
$pdf->Rect(4.65, $y+$yshift, 0.14, 0.14);       
} else  {       
$pdf->Rect(4.8, $y+$yshift, 0.14, 0.14);        
}

//require_once'debugview.php';

if($section8 == 'Yes') {
$pdf->Rect(5.73, $y+$yshift, 0.13, 0.14);
} else { // No
$pdf->Rect(5.87, $y+$yshift, 0.13, 0.14);
}
if($publichousing == 'Yes') {
$pdf->Rect(7.15, $y+$yshift, 0.13, 0.14);
} else { // No
$pdf->Rect(7.29, $y+$yshift, 0.13, 0.14);
}
/*
if($row['section8'] == 'Yes') {
$pdf->Rect(5.73, $y+$yshift, 0.13, 0.14);
} else { // No
$pdf->Rect(5.87, $y+$yshift, 0.13, 0.14);
}
if($row['publichousing'] == 'Yes') {
$pdf->Rect(7.15, $y+$yshift, 0.13, 0.14);
} else { // No
$pdf->Rect(7.29, $y+$yshift, 0.13, 0.14);
}
*/ 
$y=$y+.25;
$pdf->SetFont('Times','B','13');
$pdf->SetXY(3.6, $y);
$pdf->Cell(0,0,'Owner Information');        
$pdf->SetXY(3.6, $y);       
$pdf->Cell(0,0,'_________________');        
        
//OWNER     

$pdf->SetFont('Times','','12');     
$query = "SELECT client.cid, firstname, lastname, streetnum, address, suffix, city, state, zip,startdate FROM client INNER JOIN owners USING (cid) WHERE bid=$bid AND startdate <= DATE(NOW()) AND enddate >= DATE(NOW()) LIMIT 1";     
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());        
if ($row = mysql_fetch_assoc($result)) {        
            formfix($row);
            $clearanceDate = $row['startdate'];     
            $cid = $row['cid'];     
        $startdate=$row['startdate'];
            $basex = 0.5; $basey = 3.9;     
        
$pdf->SetXY($basex, $basey);        
$pdf->Cell(3.5,0, "$row[firstname] $row[lastname]", 0, 0, 'C');
$pdf->SetXY($basex + 4.0, $basey);
$pdf->Cell(3.5,0, "$row[streetnum] $row[address] $row[suffix]", 0, 0, 'C');
$pdf->SetXY($basex, $basey + 0.48);
$pdf->Cell(3.5,0, "$row[city], $row[state] $row[zip]", 0, 0, 'C');
 
            $query = "SELECT type, number, ext FROM client_phone WHERE cid=$cid GROUP BY type";
            $result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                        formfix($row);
                        if ($row['ext']) {
                                    $nums[$row['type']] = "$row[number] ext. $row[ext]";
                        } else {
                                    $nums[$row['type']] = "$row[number]";
                        }
            }

 

$pdf->SetXY($basex + 3.7, $basey + 0.48);
            if (isset($nums['Home'])) {
$pdf->Cell(0,0, $nums['Home']);
            } else if ($nums['Cell']) {
$pdf->Cell(2.0,0, $nums['Cell'] . ' (Cell)', 0, 0, 'C');

                        $cellused = 1;

            }

$pdf->SetXY($basex + 5.75, $basey + 0.48);

            if (isset($nums['Work'])) {

$pdf->Cell(2.0,0, $nums['Work'], 0, 0, 'C');

            } else if ($nums['Cell'] && $cellused < 1) {

$pdf->Cell(2.0,0, $nums['Cell'] . ' (Cell)', 0, 0, 'C');

            }

}

 

$y=$y+.45;

$pdf->SetFont('Times','','12');

$pdf->SetXY(0.4, $y+.03);

$pdf->Cell(0,0,' ________________________________________         _____________________________________________');

$y=$y+.2;

$pdf->SetXY(1.6, $y);

$pdf->Cell(0,0,'(Name)');

$pdf->SetXY(5.0, $y);

$pdf->Cell(0,0,'(No. & Street Address)');

 

$y=$y+.26;

$x=.4;

$yshift=0.04;

$pdf->SetXY($x, $y+$yshift);

$pdf->Cell(0,0,'_________________________________________');

$pdf->SetXY($x+.55, $y+.2);

$pdf->Cell(0,0,'(City / Town, State & Zip)');

 

$pdf->SetXY($x+3.6, $y+$yshift);

$pdf->Cell(0,0,'______________________');

$pdf->SetXY($x+4.0, $y+.2);

$pdf->Cell(0,0,'(Home Phone)');

 

$pdf->SetXY($x+5.6, $y+$yshift);

$pdf->Cell(0,0,'______________________');

$pdf->SetXY($x+6.0, $y+.2);

$pdf->Cell(0,0,'(Work Phone)');

 

$y=$y+.3;

$pdf->SetXY(0.4,$y);

$pdf->Line($ml, $y, $ml+$w, $y);

 

//Inspector and Company

 

$y=$y+.2;

$pdf->SetFont('Times','B','13');

$pdf->SetXY(3.25, $y);

$pdf->Cell(0,0,'Inspector/Assessor Information');

$pdf->SetXY(3.25, $y);

$pdf->Cell(0,0,'___________________________');

 

$x = 0.6;

$y=$y+.3;
$ydrop=0;
$pdf->SetFont('Times','','12');
//print $query;

//$startdate = mysql_result($result, 0, 'startdate'); //used in inspector section

//INSPECTORS

$query = "SELECT firstname, lastname, elt, eli, units.starttime, elistart AS useeli FROM inspector INNER JOIN insp_assigned USING ( tid ) INNER JOIN units USING ( iid ) WHERE cuid =$cuid ORDER BY eli DESC  LIMIT 0 , 30 ";

$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
//$row1= mysql_fetch_assoc($result);

$datequery = "SELECT DATE_FORMAT(starttime, '%c/%e/%Y') AS startdate, DATE_FORMAT(starttime, '%l:%i %p') AS starttime FROM units WHERE cuid=$cuid";
$dateresult = mysql_query($datequery) or sql_crapout($datequery.'<br />'.mysql_error());
$daterow = mysql_fetch_row($dateresult);

$starttime = $daterow[0];
$pdf->SetXY($basex + 5.75, $y + $ydrop);
$pdf->Cell(0,0, $starttime); //retreived in address section"

/*    Need to skip ELI row if no ELI
        if ($row['eli'] && $row['useeli']) {
                $ydrop=$ydrop+0.48;
                        } 
//*/
while (($row = mysql_fetch_assoc($result)) && $x < 4) {
            formfix($row);
                $pdf->SetXY($x, $y + $ydrop);
                $pdf->Cell(0,0, "$row[firstname] $row[lastname]");
                $pdf->SetXY($x + 3.61, $y + $ydrop);
                            if ($row['eli'] && $row['useeli']) {
                $pdf->Cell(0,0, "ELI- $row[eli]");
                            } else {
                $pdf->Cell(0,0, "ELT- $row[elt]");

            }


        $ydrop=$ydrop+0.48;

//          $x++;

}
 
$y=$y+0.05;
$pdf->SetFont('Times','','10');
$pdf->SetXY(0.4, $y);
$pdf->Cell(0,0,'___________________________________________________   _______________                         ____________________');
$pdf->SetXY(0.6, $y+0.15);
$pdf->Cell(0,0,'(Inspector/Assessor)                         (Signature)                             (Cert. No.)                                     (Date Inspected)');
$pdf->SetXY(0.4, $y+0.48);
$pdf->Cell(0,0,'___________________________________________________   _______________');
$pdf->SetXY(0.6, $y+0.48+0.15);
$pdf->Cell(0,0,'(Inspector Technician)                      (Signature)                             (Cert. No.)');
$pdf->SetXY(0.4, $y+0.96);
$pdf->Cell(0,0,'___________________________________________________   _______________');
$pdf->SetXY(0.6, $y+0.96+0.15);
$pdf->Cell(0,0,'(Inspector Technician)                      (Signature)                             (Cert. No.)');
$pdf->SetXY(0.4, $y+1.44);
$pdf->Cell(0,0,'___________________________________________________   _______________ ');
$pdf->SetXY(0.6, $y+1.44+0.15);
$pdf->Cell(0,0,'(Inspector Technician)                      (Signature)                            (Cert. No.)');$pdf->SetLineWidth(0.014);
$pdf->Rect(5.3, $y+.25, 2.7, 1.6);
//$pdf->Image('images/fixed/comprehensive-page1v2.png', 0, 0, 8.5, 11.0, 'PNG');

//$pdf->Image('anchor_new2.png', 5.3, $y+.8,.7,.7, 'PNG');

//$pdf->SetLineWidth(0.014);//$pdf->Rect(5.4, $y+.75, 0.6, 0.7);
 
$pdf->SetXY(6.15, $y+.5);
$pdf->SetFont('Times','B','12');
$pdf->Cell(0,0,'Inspecting Firm');
$pdf->SetXY(6.05, $y+.8);
$pdf->SetFont('Times','','11');
$pdf->Cell(0,0,'Rhode Island');
$pdf->SetXY(6.10, $y+1.0);
$pdf->Cell(0,0,'Lead Technicians, Inc');
$pdf->SetXY(6.15, $y+1.2);
$pdf->Cell(0,0,'120 Amaral Street');
$pdf->SetXY(6.20, $y+1.4);
$pdf->Cell(0,0,'East Providence, RI 02915');
 
//Reason
$y=$y+.25+1.6;
$pdf->SetXY(0.4,$y);
$pdf->Cell(0,0,'___________________________________________________________________________________________________');
$query = "SELECT reason, otherreason FROM inspection WHERE iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());list($reason, $otherreason) = mysql_fetch_row($result);
$pdf->SetFont('Times','',12);
 
switch ($reason) {
            case 'Department of Health Initiated':
$pdf->SetXY(0.60, $y+.6);
                        break;
            case 'Licensed Child Care Facility':
$pdf->SetXY(0.60, $y+.75);
                        break;
            case 'School':
$pdf->SetXY(0.60, $y+0.9);
                        break;
            case 'Code Enforcement':
$pdf->SetXY(0.60, $y+1.05);
                        break;
            case 'Investigation of Improper Lead Hazard Reduction/Illegal Abatement':
$pdf->SetXY(3.71, $y+0.6);
                        break;
            case 'Private Client - Property Transfer':
$pdf->SetXY(3.71, $y+0.75);
                        break;
            case 'Dept. of Human Services Initiated':
$pdf->SetXY(3.71, $y+1.05);
                        break;
            default:
$pdf->SetXY(3.71, $y+0.9);
                       if ($reason == 'Other') {
                                   $other = $otherreason;
                       } else { //conformance reason
                                   $other = $reason;
                       }
                       break;
}
$pdf->Cell(0,0, 'X');

if ($other) {
$pdf->SetXY(4.43, $y+0.9);
$pdf->Cell(0,0, $other);
}
 
//
$y=$y+0.25;
$pdf->SetFont('Times','B','13');
$pdf->SetXY(3.4,$y);
$pdf->Cell(0,0,'Reason for Inspection');
$pdf->SetXY(3.4,$y);
$pdf->Cell(0,0,'___________________');
$pdf->SetFont('Times','','10');
$y=$y+0.2;
$pdf->SetXY(3.4,$y);
$pdf->SetFont('Times','','10');
$pdf->SetXY(0.4,$y+.15);
$pdf->Cell(0,0,'______ Dept. of Health Initiated');
$pdf->SetXY(3.5,$y+.15);
$pdf->Cell(0,0,'_______ Investigation of Improper Lead Hazard Reduction / Illegal Abatement');
$pdf->SetXY(0.4,$y+.3);
$pdf->Cell(0,0,'______ Licensed Child Care Facility ');
$pdf->SetXY(3.5,$y+.3);
$pdf->Cell(0,0,'_______ Private Client ? Property Transfer');
$pdf->SetXY(0.4,$y+.45);
$pdf->Cell(0,0,'______ School');
$pdf->SetXY(3.5,$y+.45);
$pdf->Cell(0,0,'_______ Other ___________________________________________________');
$pdf->SetXY(0.4,$y+.6);
$pdf->Cell(0,0,'______ Code Enforcement');
$pdf->SetXY(3.5,$y+.6);
$pdf->Cell(0,0,'_______ Dept. of Human Services Initiated ___________________________');
$y=$y+.7;
$pdf->SetXY(0.4,$y);
$pdf->Cell(0,0,'____________________________________________________________________________________________________________');
 
//Media
$y=$y+.2;
$pdf->SetFont('Times','B','13');
$pdf->SetXY(3.4, $y);
$pdf->Cell(0,0,'Media Tested Includes');
$pdf->SetXY(3.4, $y);
$pdf->Cell(0,0,'___________________');
$pdf->SetFont('Times','','10');
$y=$y+.35;

//PAINT
$query = "SELECT COUNT(*) FROM comprehensive_rooms INNER JOIN comprehensive_components USING (crid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$paint = mysql_result($result, 0);          
$query = "SELECT COUNT(*) FROM comprehensive_ext_components WHERE cuid=$cuid";          
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$paint += mysql_result($result, 0);         
$pdf->SetFont('Times','','12');         
$x=1.4;         
if ($paint) {           
                        
$pdf->SetXY($x,  $y);           
$pdf->Cell(0,0, 'X');           
            }
$pdf->SetXY($x+0.3,  $y);           
$pdf->Cell(0,0, 'Paint');           
$pdf->SetXY($x-0.12,  $y);          
$pdf->Cell(0,0, '_____');

 
//SOIL
//*****uncommented these lines to handle soil samples
//*****added timetaken to the where clause in the 2nd sql query
//*****Question: Why is there data automatically being inserted on comprehensive_sides table?
 
$query = "SELECT u2.cuid FROM units AS u1 INNER JOIN units AS u2 USING (iid) WHERE u1.cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$extcuid = mysql_result($result, 0);
$query = "SELECT COUNT(*) FROM comprehensive_sides WHERE cuid=$extcuid AND inaccessible='No' AND cover='None' and timetaken <> '0000-00-00 00:00:00'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$soils = mysql_result($result, 0);
$query = "SELECT COUNT(*) FROM comprehensive_soil_debris WHERE cuid=$extcuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$soils += mysql_result($result, 0);         
$query = "SELECT COUNT(*) FROM comprehensive_soil_object WHERE cuid=$extcuid";          
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$soils += mysql_result($result, 0);         
            
//$soils = 1;           
$x=3.0;         
if ($soils) {           
                        
$pdf->SetXY($x,  $y);           
$pdf->Cell(0,0, 'X');           
            }
$pdf->SetXY($x+0.3,  $y);           
$pdf->Cell(0,0, 'Soil');            
$pdf->SetXY($x-0.12,  $y);          
$pdf->Cell(0,0, '_____');           


//WATER         
$query = "SELECT COUNT(*) FROM waters WHERE cuid=$cuid AND iid=$iid"; //iid unnecessary?"           
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$waters = mysql_result($result, 0);         
$x=4.56;            
if ($waters) {          
                        
$pdf->SetXY($x,  $y);           
$pdf->Cell(0,0, 'X');           
            }
$pdf->SetXY($x+0.3,  $y);           
$pdf->Cell(0,0, 'Water');           
$pdf->SetXY($x-0.12,  $y);          
$pdf->Cell(0,0, '_____');           


//DUST          
$query = "SELECT COUNT(*) FROM comprehensive_wipes INNER JOIN comprehensive_rooms USING(crid) WHERE cuid=$cuid AND iid=$iid";   
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$dust = mysql_result($result, 0);           
$x=6.38;            
if ($dust) {            
                        
$pdf->SetXY($x,  $y);           
$pdf->Cell(0,0, 'X');           
            }
$pdf->SetXY($x+0.3,  $y);           
$pdf->Cell(0,0, 'Dust');            
$pdf->SetXY($x-0.12,  $y);          
$pdf->Cell(0,0, '_____');           

 
$y=$y+.25;
$pdf->SetXY(0.4,$y);
$pdf->Cell(0,0,'__________________________________________________________________________________________');
 
$y=$y+.25;
$pdf->SetFont('Times','B','12');
$pdf->SetXY(0.4, $y);
$pdf->Cell(0,0,'Notice to Owners:');
$pdf->SetFont('Times','','9');
$pdf->SetXY(1.8,$y);
$pdf->Cell(0,0,'R.I. lead regulations allow certified lead inspectors to assume that any material contains lead without testing, based on');
$yinc=0.12;
$y=$y+$yinc;
$pdf->SetXY(1.8,$y);

$pdf->Cell(0,0,'professional expertise. This assumption may save the owner inspection costs but may result in additional abatement cost.');
$y=$y+$yinc+.03;
$pdf->SetFont('Times','B','12');
$pdf->SetXY(1.8,$y);
$pdf->Cell(0,0,'You are advised to discuss this issue with your inspector in detail prior to inspection.');
 
$y=$y+0.4;
$pdf->SetFont('Times','B','16');
$pdf->SetXY(1.8,$y);
$pdf->Cell(0,0,'THIS REPORT SERVES AS A NOTICE TO ABATE');

$y=$y+0.25;
$pdf->SetXY(1.4,$y);
$pdf->Cell(0,0,'WHEN SIGNIFICANT LEAD HAZARDS ARE IDENTIFIED');
$pdf->SetFont('Times','','10');
$y=$y+0.3;
$pdf->SetXY(0.4,$y);
$pdf->Cell(0,0,'____________________________________________________________________________________________________________');

$y=$y+0.3;
$pdf->SetXY(0.4,$y);
$pdf->Cell(0,0,'FORM PBLC-23-1 (7/05) Replaces FORM PBLC-23-1 (1/03) p.1, which is obsolete.');


?>
