<?php 

require_once'session.php';
require_once'connect.php';

$ctid = $_SESSION['ctid'] or $_POST['ctid'] or $ctid = $_GET['ctid'] or $ctid = 0;
$part = $_SESSION['part'] or $_POST['part'] or $part = $_GET['part'] or $part = 1;
$date = $_POST['date'] or $date = $_GET['date'] or $date = 1;

$query = "SELECT * FROM class_types WHERE ctid=$ctid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/class-schedule.php");
}
$row = mysql_fetch_assoc($result);

$title = "Schedule Class Part";
require_once'header.php';
?>
<?php
print "<p>Schedule part $part of $row[parts] for class $row[name].<br />This part is ".($row['hours'] / $row['parts'])." hours. (Do not start a class at midnight.)</p>";
?>

<form action="class-schedule-savepart.php" method="post">
<input type="hidden" name="date" value="<?php print $date; ?>" />
<table>
<tr><td>Scheduled Start:</td><td><input type="text" name="starthour" maxlength="2" size="3" />:<input type="text" name="startmin" maxlength="2" size="3" /> <input type="radio" name="startampm" value="AM" />AM&nbsp;<input type="radio" name="startampm" value="PM" />PM</td></tr>
<tr><td>Minimum Students</td><td><input type="text" name="minstudents" value="<?php print $row['defaultmin']; ?>" maxlength="2" size="3" /></td></tr>
<tr><td>Recommended Students</td><td><input type="text" name="recstudents" value="<?php print $row['defaultrec']; ?>" maxlength="2" size="3" /></td></tr>
<tr><td>Maximum Students</td><td><input type="text" name="maxstudents" value="<?php print $row['defaultmax']; ?>" maxlength="2" size="3" /></td></tr>
<tr><td>Location</td><td><input type="text" name="location" value="<?php print $row['defaultlocation']; ?>" maxlength="50" /></td></tr>
</table>
<p><input type="submit" name="submit" value="Save" /> <input type="reset" value="Reset" /> <input type="submit" name="submit" value="Cancel" /></p>
</form>

<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>