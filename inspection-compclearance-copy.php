<?php

require_once'session.php';
require_once'connect.php';

$iid = $_POST['iid'] or $iid = $_GET['iid'] or $iid = 0;
$old = $_POST['old'] or $old = $_GET['old'] or $old = 0;

foreach ($_POST['oldunits'] as $cuid) {
	copyunit($cuid);
}


function copyunit($cuid) {
	global $iid;
	$linkupdates = array();
	$query = "SELECT * FROM units WHERE cuid=$cuid order by designation";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		$cuid = array_shift($row); //get rid of cuid off array for implodes
		unset ($row['endtime']);
		unset ($row['starttime']);
		$row['iid'] = $iid;
		$query = "INSERT INTO units (".implode(',', array_keys($row)).") VALUES ('".implode("','", $row)."')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		$query = "SELECT LAST_INSERT_ID()"; //get new cuid
		$resultid = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		$newid = mysql_result($resultid, 0);
		//unit itself copied; just go down the list
		//unit links done below, after all units completed
		copy_comprehensive_ext($cuid, $newid);
		copy_comprehensive_ext_components($cuid, $newid);
		copy_comprehensive_sides($cuid, $newid);
		copy_comprehensive_soil_debris($cuid, $newid);
		copy_comprehensive_soil_object($cuid, $newid);
		copy_comprehensive_rooms($cuid, $newid);
		copy_conformance_comments($cuid, $newid);
		copy_conformance_sides($cuid, $newid);
		copy_conformance_rooms($cuid, $newid);
		$unitmap["$cuid"] = $newid;
		$linkupdates[] = $cuid;
	}

/*
	//unit links; can't be a function; need to know whole unit map for non-commons
	foreach ($linkupdates as $common) {
		//deleting is done while updating the unit
		$newcommon = $unitmap[$common];
		$query = "SELECT unit FROM unit_links WHERE common=$common";
		$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		while ($row = mysql_fetch_assoc($result)) {
			$newunit = $unitmap[$row['unit']];
			$query = "INSERT IGNORE INTO unit_links (common, unit) VALUES ($newcommon, $newunit)";
			mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		}
		//flip half, unit could be non-common
		//deleting is done while updating the unit
		$newunit = $unitmap[$common];
		$query = "SELECT common FROM unit_links WHERE unit=$common";
		$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		while ($row = mysql_fetch_assoc($result)) {
			$newcommon = $unitmap[$row['common']];
			$query = "INSERT IGNORE INTO unit_links (common, unit) VALUES ($newcommon, $newunit)"; //ignore errors
			mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		}
	}
*/
}

function copy_comprehensive_ext($oldcuid, $newcuid) {
	global $iid;
	$query = "SELECT * FROM comprehensive_ext WHERE cuid=$oldcuid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		$query = "INSERT INTO comprehensive_ext (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
}

function copy_comprehensive_ext_components($oldcuid, $newcuid) {
	global $iid;
	$query = "SELECT * FROM comprehensive_ext_components WHERE cuid=$oldcuid AND (hazardassessment != 'Lead-free' AND hazardassessment != 'Lead-safe')";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		$oldccid = $row['ccid'];
		unset($row['cuid']); //get rid of cuid off array for implodes
		unset($row['ccid']); //get rid of ccid off array for implodes
		$query = "INSERT INTO comprehensive_ext_components (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		// PAINT CHIPS! Woo hoo!
		$query = "SELECT LAST_INSERT_ID()";
		$resultid = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
}

function copy_comprehensive_sides($oldcuid, $newcuid) {
	global $iid;
	$query = "SELECT * FROM comprehensive_sides WHERE cuid=$oldcuid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		unset($row['csid']); //auto-increment
		$query = "INSERT INTO comprehensive_sides (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
}

function copy_comprehensive_soil_debris($oldcuid, $newcuid) {
	global $iid;
	$query = "SELECT * FROM comprehensive_soil_debris WHERE cuid=$oldcuid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		unset($row['csid']); //auto-increment
		$query = "INSERT INTO comprehensive_soil_debris (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
}

function copy_comprehensive_soil_object($oldcuid, $newcuid) {
	global $iid;
	$query = "SELECT * FROM comprehensive_soil_object WHERE cuid=$oldcuid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		unset($row['csid']); //auto-increment
		$query = "INSERT INTO comprehensive_soil_object (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
}


function copy_comprehensive_rooms($oldcuid, $newcuid) {
	global $iid;
	$query = "SELECT * FROM comprehensive_rooms WHERE cuid=$oldcuid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		$oldcrid = $row['crid'];
		unset($row['crid']); //get rid of crid off array for implodes
		$query = "INSERT INTO comprehensive_rooms (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		$query = "SELECT LAST_INSERT_ID()";
		$resultid = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		$newcrid = mysql_result($resultid, 0);
		copy_comprehensive_components($oldcrid, $newcrid);
	}
}

function copy_comprehensive_components($oldcrid, $newcrid) {
	global $iid;
	$query = "SELECT * FROM comprehensive_components WHERE crid=$oldcrid AND (hazardassessment != 'Lead-free' AND hazardassessment != 'Lead-safe')";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['crid']); //get rid of crid off array for implodes
		$oldccid = $row['ccid'];
		unset($row['ccid']); //get rid of ccid off array for implodes
		$query = "INSERT INTO comprehensive_components (crid, ".implode(',', array_keys($row)).") VALUES ('$newcrid','".implode("','", $row)."')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
}

function copy_conformance_comments($oldcuid, $newcuid) {
	global $iid;
	$query = "SELECT * FROM conformance_comments WHERE cuid=$oldcuid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		$query = "INSERT INTO conformance_comments (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
}

function copy_conformance_sides($oldcuid, $newcuid) {
	global $iid;
	$query = "SELECT * FROM conformance_sides WHERE cuid=$oldcuid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		unset($row['csid']);
		$query = "INSERT INTO conformance_sides (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
}

function copy_conformance_rooms($oldcuid, $newcuid) {
	global $iid;
	$query = "SELECT * FROM conformance_rooms WHERE cuid=$oldcuid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		$oldcrid = $row['crid'];
		unset($row['crid']); //get rid of crid off array for implodes
		$query = "INSERT INTO conformance_rooms (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
}

//**********************************************************************************//
//******************************* NON-COPY FUNCTIONS *******************************//
//**********************************************************************************//

function slashadd(&$item) {
	$item = addslashes($item);
}



$title = "Copy Inspection";
require_once'header.php';
?>
<?php
require_once'footer.php';
?>