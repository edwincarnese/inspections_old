<input type="hidden" name="cuid" value="<?php print $cuid;?>" />
<table>
<tr><td>Unit Description:</td><td><input type="text" name="unitdesc" value="<?php print $row['unitdesc']; ?>" maxlength="25" /></td></tr>
<tr><td>Unit Type:</td><td>
<?php
$values = array( 'Interior', 'Interior Common');
foreach ($values as $value) {
	if ($row['unittype'] == $value) {
		print "<input type=\"radio\" name=\"unittype\" value=\"$value\" checked=\"checked\" />$value&nbsp;";
	} else {
		print "<input type=\"radio\" name=\"unittype\" value=\"$value\" />$value&nbsp;";
	}
}
?>
</td></tr>
<tr><td colspan="2"><br /><b>The below fields should be skipped for common areas.</b></td></tr>
<tr><td>Tenant:</td><td><input type="text" name="tenant" value="<?php print $row['tenant']; ?>" maxlength="30" /></td></tr>
<tr><td>Years resided at unit:</td><td><input type="text" name="tenantyears" value="<?php print $row['tenantyears']; ?>" maxlength="150" /></td></tr>
<tr><td>Children Under 6:</td><td><input type="text" size="4" maxlength="2" value="<?php print $row['under6']; ?>" /></td></tr>

</table>
<p><input type="submit" name="submit" value="Save" /> <input type="reset" value="Reset" /> <input type="submit" name="submit" value="Cancel" /></p>
