<?
include_once 'session.php';?>
<script language=javascript>
function openerHref() {
	document.update.popup.value = opener.location.href;
}
</script>
<?php 

include_once 'connect.php';

$iid = $_POST['iid'] or $iid = $_GET['iid'] or $iid = 0;

$query = "SELECT * FROM inspection WHERE iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (!($row = mysql_fetch_assoc($result))) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/building-list.php");
	exit();
}

$title = "Edit Inspection";
if (!$_GET['isPopup']) {
	require_once'header.php';
}
else if ($_GET['isPopup']) {
	echo "<body bgcolor='#dddddd'>";
}
?>

<form name="update" action="inspection-schedule-update.php" method="post">
<input type="hidden" name="iid" value="<?php print $iid;?>" />
<table>
<tr><td style="vertical-align:top">Inspection type:</td><td>
<?php
// 10/18/6, added presumptive compliance
$types = array('Conformance','Comprehensive','Conformance - Clearance','Comprehensive - Clearance','Pre-Inspection Walkthrough','Post-Inspection Walkthrough','Limited','Presumptive Compliance');
$x = 0;
foreach ($types as $type) {
	if ($row['type'] == $type) {
		print "<input type=\"radio\" name=\"type\" value=\"$type\" checked=\"checked\" /> $type";
	} else {
		print "<input type=\"radio\" name=\"type\" value=\"$type\" /> $type";
	}
	if ($x % 2) {
		print "<br />\n";
	}
	$x++;
}
?></td></tr>
<tr><td style="vertical-align:top">Reason:</td><td>
<!-- Reasons -->
<table>
<?php
// $reasons = array('Independent Clearance Inspection', 'Owner Request', 'Code Enforcement', 'Visual Inspection', 'Private Client', 'Private Client - Property Transfer', 'Tenant Complaint', 'Presumptive Compliance', 'School', 'Licensed Child Care Facility', 'Department of Health Initiated', 'Dept. of Human Services Initiated');

$reasons = array('Independent Clearance Inspection', 'Owner Request', 'Code Enforcement', 'Visual Inspection', 'Private Client', 'Private Client - Property Transfer', 'Tenant Complaint', 'Presumptive Compliance', 'School', 'Licensed Child Care Facility', 'Department of Health Initiated', 'Dept. of Human Services Initiated', 'Under Order', 'DHHS', 'Insurance', 'RRP', 'Informational');

$x = 0;
foreach ($reasons as $reason) {
	if ($x % 2 == 0) {
		print '<tr>';
	}
	if ($row['reason'] == $reason) {
		print "<td><input type=\"radio\" name=\"reason\" value=\"$reason\" checked=\"checked\" /> $reason</td>";
	} else {
		print "<td><input type=\"radio\" name=\"reason\" value=\"$reason\" /> $reason</td>";
	}
	if ($x % 2) {
		print "</tr>\n";
	}
	$x++;
}
if ($reason == 'Investigation of Improper Lead Hazard Reduction/Illegal Abatement') {
	print "<tr><td colspan=\"2\"><input type=\"radio\" name=\"reason\" value=\"Investigation of Improper Lead Hazard Reduction/Illegal Abatement\" checked=\"checked\" /> Investigation of Improper Lead Hazard Reduction/Illegal Abatement</td></tr>\n";
} else {
	print "<tr><td colspan=\"2\"><input type=\"radio\" name=\"reason\" value=\"Investigation of Improper Lead Hazard Reduction/Illegal Abatement\" /> Investigation of Improper Lead Hazard Reduction/Illegal Abatement</td></tr>\n";
}
?>
<tr>
<td colspan="2"><input type="radio" name="reason" value="Other" <?php if ($row['otherreason']) { print 'checked="checked"'; } ?> /> Other: <input type="text" name="otherreason" size="60" maxlength="60" /></td>
</tr>
</table>
</td></tr>
<tr><td colspan="2">Is Building Owner Occupied?
<?php
$values = array('Yes','No');
foreach ($values as $value) {
	if ($row['owneroccupied'] == $value) {
		print "<input type=\"radio\" name=\"owneroccupied\" value=\"$value\" checked=\"checked\" />$value&nbsp;";
	} else {
		print "<input type=\"radio\" name=\"owneroccupied\" value=\"$value\" />$value&nbsp;";
	}
}
?>
</td></tr>
<?php
list ($year, $month, $day) = explode('-', $row['scheddate']);
?>
<tr><td>Scheduled Date:</td><td><input type="text" name="month" maxlength="2" size="3" value="<?php print $month; ?>" /> / <input type="text" name="day" maxlength="2" size="3" value="<?php print $day; ?>" /> / <input type="text" name="year" maxlength="4" size="5" value="<?php print $year; ?>" /></td></tr>
<?php
list ($hour, $minute, $ampm) = explode(':', $row['schedstart']);
switch (true) {
	case ($hour == 0):
		$hour = 12; $ampm = 'AM'; break;
	case ($hour < 12):
		$ampm = 'AM'; break;
	case ($hour > 12):
		$hour -= 12;
	default:
		$ampm = 'PM';
}
?>
<tr><td>Scheduled Start:</td><td><input type="text" name="starthour" maxlength="2" size="3" value="<?php print $hour;?>" />:<input type="text" name="startmin" maxlength="2" size="3" value="<?php print $minute;?>" /> <input type="radio" name="startampm" value="AM" <?php if ($ampm == 'AM') { print 'checked="checked"'; } ?> />AM&nbsp;<input type="radio" name="startampm" value="PM" <?php if ($ampm == 'PM') { print 'checked="checked"'; } ?>/>PM</td></tr>
<?php
list ($hour, $minute, $ampm) = explode(':', $row['schedend']);
switch (true) {
	case ($hour == 0):
		$hour = 12; $ampm = 'AM'; break;
	case ($hour < 12):
		$ampm = 'AM'; break;
	case ($hour > 12):
		$hour -= 12;
	default:
		$ampm = 'PM';
}
?>
<tr><td>Scheduled End:</td><td><input type="text" name="endhour" maxlength="2" size="3" value="<?php print $hour;?>" />:<input type="text" name="endmin" maxlength="2" size="3" value="<?php print $minute;?>" /> <input type="radio" name="endampm" value="AM" <?php if ($ampm == 'AM') { print 'checked="checked"'; } ?> />AM&nbsp;<input type="radio" name="endampm" value="PM" <?php if ($ampm == 'PM') { print 'checked="checked"'; } ?> />PM</td></tr>
</table>
<input type="hidden" name="popup"/>
<p><input type="submit" value="Save" onClick="javascript:openerHref();"/> <input type="reset" value="Reset" /><input type="submit" value="Cancel" onClick="javascript:self.close();"</p>
</form>

<?php
if (!$_GET['isPopup']) {
	require_once'footer.php';
}
else if ($_GET['isPopup']) {
	echo "</body>";
}
?>