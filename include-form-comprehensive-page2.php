<?php

$pdf->AddPage('P');
$pdf->Image('images/fixed/comprehensive-page2.png', 0, 0, 8.5, 11.0, 'PNG');

$query = "SELECT iid, number FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

$iid = $row['iid'];

$lh = 0.4;
$pdf->SetFont('Arial','','10');

$pdf->SetXY(1.75, 0.15);
$pdf->Cell(0,0, "Inspection $row[iid]-$row[number]");

//ADDRESS
$query = "SELECT streetnum, address, suffix, city, unitdesc FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

$pdf->SetXY(1.75, 0.30);
$pdf->Cell(0,0, "$row[unitdesc], $row[streetnum] $row[address] $row[suffix], $row[city]");

//PAGE NUMBERS
$pdf->SetFont('Arial','','12');
//$pdf->SetXY(7.12, 0.18);
//$pdf->Cell(0,0, $pdf->PageNo());
$pdf->SetXY(7.63, 0.46);
$pdf->Cell(0,0, '{totalpages}');

$query = "SELECT firstname, lastname, elt, eli FROM inspector INNER JOIN insp_assigned USING (tid) WHERE iid=$iid AND job='Lead'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if ($row = mysql_fetch_assoc($result) ) {
	formfix($row);
	$pdf->SetXY(3.30, 9.70);
	$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
	$pdf->SetXY(4.50, 10.14);
	$pdf->Cell(0,0, "ELI- $row[eli]");
}


//date/time at bottom
$query = "SELECT DATE_FORMAT(starttime, '%c/%e/%Y') AS startdate, DATE_FORMAT(starttime, '%l:%i %p') AS starttime FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_row($result);
$pdf->SetXY(0.81, 10.15);
$pdf->Cell(0,0, $row[0]);

?>