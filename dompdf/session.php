<?php
session_start();
session_cache_limiter('private');

if (isset($_GET['logout'])) {
	if (isset($_COOKIE[session_name()])) {
   		setcookie(session_name(), '', time()-42000, '/');
	}
	session_destroy();
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/login.php");

}

if (!$sid = $_SESSION['sid']) { // session id, current tech's ID
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/login.php");
}

if (isset($_SESSION['skipXRF'])) {
	$skipXRF = $_SESSION['skipXRF'];
}

?>
