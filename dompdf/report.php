<?php
require_once 'autoload.inc.php';
ini_set('xdebug.max_nesting_level', 1000);


// <div style='page-break-after:always;'></div> 
//<img src='images/signature.png' height='50px' style='margin-right:200px;'>
//<img src='images/signature.png' height='30px' style='border-bottom: 1px solid #000; margin:0px; padding:0px;'>
//<img src='images/signature.png' height='30px'>
// reference the Dompdf namespace
use Dompdf\Dompdf;

if(isset($_POST["submit"]))
{
	// lead inspection risk assessment
	$logo = $_FILES["logo"];
	$lira = $_POST["lira"];
        $building_address = $_POST["building_address"];

	// owner info
	$owner_name = $_POST["owner_name"];
	$owner_address = $_POST["owner_address"];
	$owner_contact = $_POST["owner_contact"];
	$property_type = $_POST["property_type"];

	// inspector
	$inspector_name = $_POST["inspector_name"];
	$inspector_address = $_POST["inspector_address"];
	$inspector_contact = $_POST["inspector_contact"];
	$inspector_email = $_POST["inspector_email"];
	$inspector_license = $_POST["inspector_license"];
	$inspector_ra = $_POST["inspector_ra"];
        
        $date_performed = $_POST["date_performed"];





$target_dir = "images/";
$target_file = $target_dir . basename($_FILES["logo"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["logo"]["tmp_name"]);
    if($check !== false) {
        //echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        //echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    //echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
//if ($_FILES["fileToUpload"]["size"] > 500000) {
    //echo "Sorry, your file is too large.";
//    $uploadOk = 0;
//}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    //echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["logo"]["tmp_name"], $target_file)) {
        //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
    } else {
        //echo "Sorry, there was an error uploading your file.";
    }
}






}


// instantiate and use the dompdf class
$dompdf = new Dompdf();

$dompdf->loadHtml("
<html>
<style>
p{line-height: 150%;}
@page { margin: 50px 50px; }
#footer { position: fixed; left: 0px; bottom: -180px; right: 0px; height: 180px; }
#footer .page:after { content: counter(page); }</style>
<body style='text-align:justify'>

<div id='footer'>
     <p class='page' style='text-align:center;'></p>
</div>


<table border='1' align='center'>

<tr>
<td style='text-align:center;'><img src='images/logo.png'></td>
</tr>

</table>
<br/>

<p style='text-align:center; font-weight:bold; font-size:35px;'>". $lira ."</p>

<p style='text-align:center; font-weight:bold; font-size:30px;'>". $building_address ."</p>

<p style='text-align:center; font-weight:bold; font-size:30px;'><img src='". $target_file ."' height='400' width='550'></p>

<p style='float:left; margin:0px; padding:0px;'>Prepared by:</p>
<p style='text-align:right; margin:0px; padding:0px;'>Prepared for:
</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>". $inspector_name ."</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>" .$owner_name. "</p>

<p style='float:left;; margin:0px; padding:0px;'>". $inspector_license ."</p>
<p style='text-align:right; margin:0px; padding:0px;'>". $owner_address ."
</p>

<p style='float:left;; margin:0px; padding:0px;'>". $inspector_ra ."</p>
<p style='text-align:right; margin:0px; padding:0px;'>". $owner_contact ."
</p>
<br/>
<br/>
<br/>

Contact Information:
<br/>
". $inspector_address ."
<br/>
". $inspector_contact ."
<br/>
". $inspector_email ."

<div style='page-break-after:always;'></div> 

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:40px;'> Table of Contents </p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>Introduction and Executive Summary</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>.......................................................................................................... _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>What is the difference between an inspection and risk assessment?
</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>.......................................................... _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>EPA §745.107 Disclosure Requirements for Sellers and Lessors</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>............................................................... _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>VISUAL ASSESSMENT SUMMARY</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>.......................................................................................................... _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>XRF Testing Results and Lead-Based Paint Hazards</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>................................................................................. _</p>

<p style='margin-left:15px; float:left; font-weight:bold; margin:0px; padding:0px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;New Hampshire defines a lead exposure hazard as:
</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>.............................................................................. _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>Explanation of Lead Inspection Risk Assessment Report Form Columns</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>............................................... _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>Conditions and Limitations</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>........................................................................................................................... _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>New Hampshire Lead-Safe Requirements</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>................................................................................................... _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;He-P 1609.02 Abatement Methods</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>........................................................................................................... _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>PART He-P 1610 STANDARDS FOR INTERIM CONTROLS</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>................................................................... _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;He-P 1610.01 Interim Control Standards</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>.............................................................................................. _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;He-P 1609.03 Encapsulant Products and Their Use
</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>............................................................................. _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>Testing Protocols</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>........................................................................................................................................... _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Calibration</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>................................................................................................................................................ _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Testing</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>....................................................................................................................................................... _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Testing Combination</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>............................................................................................................................... _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Walls</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>.......................................................................................................................................................... _</p> 

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Window Systems</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>...................................................................................................................................... _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>Performance Characteristics</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>....................................................................................................................... _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Field Operation Guidance</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>....................................................................................................................... _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Background Information</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>........................................................................................................................ _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Testing Times</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>........................................................................................................................................... _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>Cover Sheet</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>................................................................................................................................................... _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>Field Notes</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>..................................................................................................................................................... _</p>

<p style='float:left; font-weight:bold; margin:0px; padding:0px;'>Photo Exibit A</p>
<p style='text-align:right; font-weight:bold; margin:0px; padding:0px;'>............................................................................................................................................... _</p>



<div style='page-break-after:always;'></div> 




<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:40px;'> Introduction and Executive Summary </p>
<p style='text-indent:50px;'> This is a ". $lira ." on a ". $property_type ." located at ". $owner_address .". The inspection was conducted at the request of the owners ". $owner_name ." and was provided by ". $inspector_license .", ". $inspector_name .".
</p>
<p style='text-indent:50px;'>
The field work was performed on ". $date_performed ." The inspection was performed
within the current acceptable industry guidelines including: the US Environmental Protection
Agency's (EPA) title X, the Housing and Urban Development (HUD) lead safe housing rule, and
the State of New Hampshire's RSA 130-A & Chapter He-P 1600, lead poisoning prevention and
control rules (see table of contents)
</p>
<p style='text-align:center; font-weight:bold; font-size:20px; margin-top:40px; margin-bottom:40px;'> What is the difference between an inspection and risk assessment? </p>
<p style='text-indent:50px;'> An inspection is a surface-by-surface investigation to determine whether there is lead-based paint in a home or child-occupied facility, and where it is located. Inspections can be legally
performed only by certified inspectors or risk assessors. Lead-based paint inspections determine
the presence of lead-based paint. It is particularly helpful in determining whether lead-based
paint is present prior to purchasing, renting, or renovating a home, and identifying potential
sources of lead exposure at any time. </p>
<p style='text-indent:50px; align:justify;'> A risk assessment is an on-site investigation to determine the presence, type, severity,
and location of lead-based paint hazards (including lead hazards in paint, dust, and soil) and
provides suggested ways to control them. Risk assessments can be legally performed only by
certified risk assessors. Lead-based paint risk assessments are particularly helpful in determining
sources of current exposure and in designing possible solutions. </p>
<p style='text-indent:50px;'> You can also have a combined inspection and risk assessment. </p>

<div style='page-break-after:always;'></div> 

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:40px;'> EPA §745.107 Disclosure Requirements for Sellers and Lessors </p>

<p>1) The following activities shall be completed before the purchaser or lessee is obligated under
any contract to purchase or lease target housing this is not otherwise an exempt transaction
pursuant to §745.107. Nothing in this section implies a positive obligation on the seller or lessor
to conduct any evaluation or reduction activity. </p>

<p style='margin-left:60px; margin-bottom:20px; margin-top:20px;'>a) The seller or lessor shall provide the purchaser or lessee with an EPA approved lead
hazard information pamphlet. Such pamphlets include the EPA document entitled
&quot;Protect Your Family from Lead in Your Home&quot; (EPA #747- K-94-001) or an equivalent
pamphlet that has been approved for use in that State by the EPA.</p>

<p style='margin-left:60px; margin-bottom:20px; margin-top:20px;'>b) The seller or lessor shall also disclose to the purchaser or lessee the presence of any
known lead-based paint and/or lead-based paint hazards in the target housing being sold
or leased. The seller or lessor shall also disclose any additional information available
concerning the known lead-based paint and/or lead-based paint hazards, such as the
basis for the determination that lead-based paint and/or lead-based paint hazards exist,
the location of the lead-based paint and/or lead-based paint hazards, and the condition
of the painted surfaces.</p>

<p style='margin-left:60px; margin-bottom:20px; margin-top:20px;'>c) The seller or lessor shall disclose to each agent the presence of any known lead- based
paint and/or lead based paint hazards in the target housing being sold or leased and the
existence of any available records or reports pertaining to lead-based paint and or lead-
based paint hazards. The seller or lessor shall also disclose any additional information
available concerning the known lead-based paint and/or lead based paint hazards, such
as the basis for the determination that lead-based paint and/or lead-based paint hazards
exist, the location of the lead-based paint and or lead-based paint hazards, and the
condition of the painted surfaces.</p>

<p style='margin-left:60px; margin-bottom:20px; margin-top:20px;'>d) The seller or lessor shall provide the purchaser or lessee with any records or reports
available to the seller or lessor pertaining to lead-based paint and or lead-based paint
hazards in the target housing being sold or leased. This requirement includes records or
reports regarding common areas. This requirement also includes records or reports
regarding other residential dwellings in multifamily target housing, if such information is
part of an evaluation or reduction of lead-based paint and/or lead based paint hazards in
the target housing.</p>

<p>2) If any of the disclosure activities identified in paragraph (1) of this section occurs after the
purchaser or lessee has provided an offer to purchase or lease the housing, the seller or lessor
shall complete the required disclosure activities prior to accepting the purchaser's or lessee's
offer and allow the purchaser or lessee an opportunity to review the information and possibly
amend the offer. </p>

<div style='page-break-after:always;'></div> 

<table border='1'>
<tr><td colspan='6'> <p style='font-weight:bold; font-size:20px; text-align:center; margin-bottom:0px;'>VISUAL ASSESSMENT SUMMARY</p> </td>
</tr>
<tr>
<td></td>
<td style='text-align:center; padding: 10px;'> Deteriorated Paint </td>
<td style='text-align:center; padding: 10px;'> Visible Dust Accumulation </td>
<td style='text-align:center; padding: 10px;'> Bare<br/>Soil </td>
<td style='text-align:center; padding: 10px;'> Friction/Impact Points </td>
<td style='text-align:center; padding: 10px;'> Chew Surfaces </td>
</tr>
<tr>
<td style='text-align:center; padding: 10px;'>Interior</td>
<td style='text-align:center; padding: 10px;'> _______ </td>
<td style='text-align:center; padding: 10px;'> _______ </td>
<td style='text-align:center; padding: 10px;'> _______ </td>
<td style='text-align:center; padding: 10px;'> _______ </td>
<td style='text-align:center; padding: 10px;'> _______ </td>
</tr>
<tr>
<td style='text-align:center; padding: 10px;'>Exterior</td>
<td style='text-align:center; padding: 10px;'> _______ </td>
<td style='text-align:center; padding: 10px;'> _______ </td>
<td style='text-align:center; padding: 10px;'> _______ </td>
<td style='text-align:center; padding: 10px;'> _______ </td>
<td style='text-align:center; padding: 10px;'> _______ </td>
</tr>
</table>

<br/>
<br/>
<br/>

<div style='text-align:center;'>

</div>
<hr style='margin:2px;'/>

<em style='margin-right:170px;'>Name of Inspector</em> Date 

<br/>
<br/>
<br/>

<p style='text-indent:50px;'>
Accessible building components and repersentative surfaces from selected areas of the
building were tested to determine the presence of lead based paint, utilizing XRF testing
technology. The XRF Pb-Test is an energy dispersive x-ray fluorescence (EDXRF) spectrometer
that uses a sealed, highly purified Cobalt-57 radioisotope source (&lt;12mCi) to excite a test
sample's constituent elements. The Pb-Test utilizes the recently developed Cadmium Telluride
(CdTe) Schottky diode detectors. In this inspection, a Thermo-Niton, XL—303 XRF, Serial number
8988 was used, which is a complete lead paint analysis system that quickly, accurately, and non-
destructively measures the concentration of lead-based paint (LBP) on surfaces. The XRF is used
in accordance with the following Performance Characteristic Sheet; Niton XLp 300, 9/24/2004,
ed.1. Please request additional information if you require specific sampling
methodologies associated with the XRF. Appendix D provides a glossary of terms common to
Lead-Based Paint.</p>

<p style='text-indent:50px;'>
*Please note: Accessible building components were tested to determine the presence of
lead based paint. Each surface identified to be a potential lead exposure hazard by the visual
inspection and having a distinct paint history, was tested for the presence of lead. Please refer to





the property specific cover sheet for an explanation of the abbreviations used in your inspection
and the property field notes for the actual testing results for the inspection.
</p>

<div style='page-break-after:always;'></div> 

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:40px;'> XRF Testing Results and Lead-Based Paint Hazards </p>

<p style='text-indent:50px;'>XRF readings of 1.0mg/cm² or greater are considered to be lead based paint in accordance
with the U.S. Environmental Protection Agency (EPA), Department of Housing and Urban
Development (HUD) and the State of New Hampshire Department of Health and Human Services.
These are indicated in <span style='background:yellow; font-weight:bold; color:black;'>yellow</span> on this report.</p>

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px; margin-top:40px;'> New Hampshire defines a lead exposure hazard as: </p>

<p style='margin-left:50px;'>(a) The presence of lead base substances on chewable, accessible, horizontal surfaces
that protrude more than ½ inch and are located more than 6 inches but less than 4
feet from the floor or ground;</p>

<p style='margin-left:50px;'>(b) Lead based substances which are peeling, chipping, chalking or cracking or any paint
located on an interior or exterior surface or fixture that is damaged or deteriorated
and is likely to become accessible to a child;</p>

<p style='margin-left:50px;'>(c) Lead based substances on interior or exterior surfaces that are subject to abrasion or
friction or subject to damage by repeated impact; or</p>

<p style='margin-left:50px;'>(d) Bare soil in play areas or the rest of the yard that contains lead in concentration equal
to or greater than the limits defined in RSA130-A:1, These hazards will be indicated in
<span style='background:red; font-weight:bold; color:black;'>red</span> on this report.</p>

<br/>

<p>NOTE: The report presented in the property specific field notes lists the results for all surfaces
identified as part of the lead inspection. This includes surfaces that are not lead paint, as well as
surfaces that contain lead paint, but are not hazards at the time of the inspection. All surfaces
which contain a dangerous level of lead in paint and are not lead hazards at the time of the lead
inspection, may become a hazard if their condition or use changes. These surfaces should be
checked periodically to ensure that they do not become lead hazards (yellow).</p







<p style='text-indent:50px;'>There may be surfaces omitted from the lead inspection due to not being visible, or
accessible to the lead inspector risk assessor. Any surface not listed on the lead inspection report should be assumed to contain lead, and considered a lead hazard, as applicable, for hazard
purposes.</p>

<p style='text-indent:50px;'>Intact surfaces (under the hazard column) which become loose, and then identified by a
lead inspector during a re-inspection or clearance inspection, must be fully abated. This
requirement exists even if the surface was not a hazard during the initial lead inspection.
Therefore, make sure these surfaces have not become loose prior to the clearance inspection.</p>

<p style='text-indent:50px;'>When ceramic tile is present, it is tested for informational coatings as any lead content
would be limited to the glazing, or paint under the glazing. This is not considered a coating by
most regulations. If lead is present, avoid breaking up the tiles as this could release lead.</p>

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px; margin-top:40px;'> Explanation of Lead Inspection Risk Assessment Report Form Columns </p>

<p style='text-indent:50px;'>This section provides general information needed to understand the lead inspection risk
assessment report. However, you should speak to your lead inspector risk assessor before you
start to do any work on your home.</p>

<p><span style='font-weight:bold;'>SIDE</span> - Refers to A, B, C, or D side of the building or room. See the diagram on the cover sheet.
The &quot;A&quot; Side of the building or room is the side facing the street that faces the property its address
(usually known as the front of the building). Keeping your back to this street from the &quot;A&quot; side
move clockwise to the &quot;B&quot; side on your left, the &quot;C&quot; side opposite you, and the &quot;D&quot; side to the
right.</p>

<p><span style='font-weight:bold;'>LOCATION/SURFACE</span> - Refers to the building component(s) being tested. Some surfaces may be
made up of more than one part. For example, &quot;Baseboard&quot; may refer to four separate pieces of
wood (one on each wall), but it is still considered one surface.</p>












<p><span style='font-weight:bold;'>LEAD</span> - The actual lead result. Each surface tested must have a result recorded in the &quot;Lead&quot;
column. A number shows that the surface was tested with an XRF analyzer. A number (or average
number) equal to or greater than 1.0mg/cm2, or are &quot;COV&quot; means the surface is currently
covered or enclosed by material such as carpet, metal, or paneling. &quot;NA&quot; means that the surface
was not available for testing due to an obstruction. Not all lead paint must be abated. This column
does not tell you why a surface needs abating. The abatement standards below may not apply
for interim controls. Speak to your risk assessor for more information.</p>

<p><span style='font-weight:bold;'>TYPE OF HAZARD</span> - &quot;F/I&quot; Circled means that the surface is an abrasion/friction/impact surface
and must be abated. &quot;CH&quot; Circled means that the surface is &quot;Chewable Accessible Horizontal&quot;
and must be abated to a minimum of four feet high, two inches in from the edge or corner. &quot;D&quot;
Circled means that the surface is damaged or deteriorated and must, at minimum, be stabilized.
If more than one choice is highlighted, the rules for abatement may vary depending upon what
method of abatement you choose. Speak to the lead inspector risk assessor for more information.</p>

<p><span style='font-weight:bold;'>SUBSTRATE</span> - This column is only completed during a risk assessment for LERP or work scope
information. The designation in this column records the substrate: W=Wood, P=Plaster,
MET=Metal, MAS=Masonry, O=Other (will be identified in the comments box)</p>

<p><span style='font-weight:bold;'>LEAD EXPOSURE HAZARD REDUCTION PLAN (LEHRP) RECOMMENDATION</span> - This is the recommendation for lead exposure hazard reduction.</p>

<p><span style='font-weight:bold;'>LEAD EXPOSURE HAZARD REDUCTION PLAN (LEHRP) ACTUAL</span> - This is the method used by the lead abatement contractor to remediate the lead hazard.</p>

<p><span style='font-weight:bold;'>ABATE DATE</span> - The date that the lead inspector or risk assessor re-inspects the surface and finds
that it has been successfully abated for full compliance or lead safe. Full compliance is defined as
no lead paint hazards remain in the home, it has been deleaded. Lead safe means that any lead
based paint remaining in the home has been properly covered and does not pose a threat to the
occupants provided it remains covered, intact and properly maintained.</p>
















<p><span style='font-weight:bold;'>ABATEMENT METHOD</span> - The abatement method used to bring a surface into full compliance or lead safe.</p>

<p><span style='font-weight:bold;'>SOIL AND DUST TESTING</span> -  If your property undergoes a risk assessment, soil and dust testing
may be required. There is also a space for the lead inspector risk assessor to indicate the amount
and location of dust and bare soil, laboratory results, method of remediation, and the date of
remediation. For properties under DHHS Order of Lead Hazard Reduction, when weather
conditions (example: snow on the ground) prohibit sampling of soil at the time of inspection, soil
sampling must be performed within 14 calendar days after soil conditions allow for such testing.</p>


<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px; margin-top:40px;'> Conditions and Limitations </p>

<p style='text-indent:50px;'>
K. Kirkwood Consulting, LLC (KKC) has performed the tasks described in this report
requested by the Client in a thorough and professional manner consistent with commonly
accepted standard industry practices, using state of the art practices and best available known
technology, as of the date of the assessment. KKC cannot guarantee and does not warrant that
this Assessment has identified all adverse environmental factors and/or conditions affecting the
subject property on the date of the Assessment. KKC cannot and will not warrant that the
Assessment that was requested by the client will satisfy the dictates of, or provide a legal defense
in connection with, any environmental laws or regulations. It is the responsibility of the client to
know and abide by all applicable laws, regulations, and standards, including EPA’s Renovation,
Repair and Painting regulation. This certification is available through KKC division Lead-Edu, for
more information contact Lead-Edu at 603-781-4304 or visit <a href='http://www.lead-edu.info'> www.lead-edu.info.</a>
</p>

<p style='text-indent:50px;'>
The results reported and conclusions reached by KKC are solely for the benefit of the
client. The results and opinions in this report, based solely upon the conditions found on the
property as of the date of the Assessment, will be valid only as of the date of the Assessment.
KKC assumes no obligation to advise the client of any changes in any real or potential lead hazards
at this residence that may or may not be later brought to our attention. However, KKC is always available for and interested in providing any additional guidance and/or services required
following this inspection.
</p>















<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px; margin-top:40px;'> New Hampshire Lead-Safe Requirements </p>

<p style='text-indent:50px'>Lead exposure hazards may be controlled either by Lead Abatement or Interim Control.
Please refer to New Hampshire Code of Administrative Rules Chapter He-P 1600 Lead Poisoning
Prevention and Control Rules provided below for specific information and requirements.</p>

<p style='margin-left:30px; font-weight:bold;'>He-P 1609.02 Abatement Methods</p>

<p style='margin-left:50px;'>(a) One or more of the following lead hazard reduction techniques shall be used on lead-
based painted surfaces to meet abatement standards:</p>

<p style='margin-left:70px;'>(1) Removal of lead-based paint by:</p>

<p style='margin-left:100px;'>a. Removal and replacement of any component with a component that is free of lead-based substances; or</p>

<p style='margin-left:100px;'>b. Removal of the surface coating down to the substrate by:</p>

<p style='margin-left:120px;'>1. Wet sanding;</p>
<p style='margin-left:120px;'>2. Utilizing of non-flammable chemical strippers, which do not contain methylene chloride;</p>
<p style='margin-left:120px;'>3. Removing the lead containing component for off-site stripping and then reinstalling;</p>
<p style='margin-left:120px;'>4. Scraping with the aid of a caustic paint remover;</p>
<p style='margin-left:120px;'>5. Misting the surface with water and wet scraping;</p>
<p style='margin-left:120px;'>6. Controlled low-level heating element, which produces a temperature no greater than 700 degrees Fahrenheit;</p>
<p style='margin-left:120px;'>7. Machine sanding using a sander equipped with a HEPA local vacuum
exhaust sized to match the tool to feather edges and prepare substrate for
repainting or sealing;</p>










<p style='margin-left:120px;'>8. Machine planing using a planing tool equipped with a HEPA local vacuum exhaust sized to match the tool;</p>
<p style='margin-left:120px;'>9. Abrasive blasting using a HEPA local vacuum exhaust sized to match the tool;</p>
<p style='margin-left:120px;'>10. Dry scraping within 6 inches of an area that would present an electrical hazard if other methods were used; or</p>
<p style='margin-left:120px;'>11. Any other method approved by the department through a variance in
accordance with He-P 1605.03;</p>

<p style='margin-left:70px;'>(2) Application of an encapsulant product that is:</p>
<p style='margin-left:100px;'>a. Approved in accordance with He-P 1609.03(a);</p>
<p style='margin-left:100px;'>b. Used only on those surfaces specified by the manufacturer; and</p>
<p style='margin-left:100px;'>c. Applied in accordance with the manufacturer’s instructions;</p>


<p style='margin-left:70px;'>(3) Enclosure of the surface so that no lead containing surface remains by:</p>

<p style='margin-left:100px;'>a. First labeling the surface to be enclosed with a warning, “Danger: Lead-Based
Paint” written in permanent ink with lettering no less than one inch high
horizontally and vertically approximately every 16 square feet on large
components, such as walls and floors, and every 4 linear feet on small
components, such as baseboards;</p>

<p style='margin-left:100px;'>b. Securely fastening and affixing all junctions of floors, walls, ceilings, and other
joined surfaces by fastening with nails, screws or an adhesive recommended by
the manufacturer for the covering so that the covering remains in place and the
physical integrity of the covering remains intact to prevent removal, and then
caulking and sealing all seams;</p>

<p style='margin-left:100px;'>c. Covering floor surfaces with wall to wall carpeting, vinyl flooring, ceramic tile,
wood or stone, or similar durable material intended for use as flooring;</p>

<p style='margin-left:100px;'>d. Covering all other interior surfaces with wood, vinyl, aluminum, plastic or
similar durable materials, except that vinyl wallpaper and plastic sheeting shall
not be allowed;</p>

<p style='margin-left:100px;'>e. Covering walls or ceiling surfaces with gypsum board, fiberglass mats, vinyl
wall coverings, formica, tile, paneling or other material that does not tear, chip
or peel;</p>

<p style='margin-left:100px;'>f. Enclosing exterior surfaces with aluminum, vinyl siding, wood, concrete or
similar durable material after covering with breathable building wrap; and</p>

<p style='margin-left:100px;'>g. Enclosing exterior trim with aluminum or vinyl coil stock;</p>

<p style='margin-left:70px;'>(4) Reversal of all component parts of a woodwork surface such that:</p>

<p style='margin-left:100px;'>a. No surface containing a lead exposure hazard remains exposed; and</p>

<p style='margin-left:100px;'>b. All seams are caulked and sealed; and</p>

<p style='margin-left:70px;'>(5) Permanent fastening of window sashes to eliminate friction surfaces if not
otherwise prohibited by any state laws, rules or local ordinances for health, building
and fire safety.</p>

<p style='margin-left:50px;'>(b) The materials used in (a)(3) above shall:</p>

<p style='margin-left:70px;'>(1) Comply with all state laws, rules or local ordinances for health, building and fire
safety; and</p>

<p style='margin-left:70px;'>(2) Only be used in places that the manufacturer intended them to be used.</p>

<p style='margin-left:50px;'>(c) The following methods shall be prohibited when performing lead-based substance abatement:</p>	

<p style='margin-left:70px;'>(1) Dry scraping or sanding except as allowed by (a) (1) b.11. above;</p>

<p style='margin-left:70px;'>(2) Dry sweeping of lead contaminated areas or surfaces;</p>

<p style='margin-left:70px;'>(3) Dry abrasive blasting using sand, grit or any other particulate without a HEPA local vacuum exhaust tool;</p>

<p style='margin-left:70px;'>(4) Utilizing mechanical sanding, planning, grinding or other removal equipment without a HEPA local vacuum exhaust tool;</p>

<p style='margin-left:70px;'>(5) Torch or open-flame burning;</p>

<p style='margin-left:70px;'>(6) Propane-fueled heat grids;</p>

<p style='margin-left:70px;'>(7) Heating elements operating above 700 degrees Fahrenheit;</p>

<p style='margin-left:70px;'>(8) Uncontained hydroblasting or high-pressure wash;</p>

<p style='margin-left:70px;'>(9) Use of methylene chloride or solutions containing methylene chloride in interior work areas; and</p>

<p style='margin-left:70px;'>(10) Encapsulants that have not been approved under He-P 1609.03(a).</p>

<p style='margin-left:50px;'>(d) The following precautions shall be used when conducting lead-based substance removal on properties listed in or determined eligible for the National Register of Historic
Places:</p>

<p style='margin-left:70px;'>(1) When an orbital sander with a HEPA local vacuum exhaust sized to match the tool is used, such device shall be used only as a finishing or smoothing tool;</p>

<p style='margin-left:70px;'>(2) When a belt sander with a HEPA local vacuum exhaust sized to match the tool is used, such device shall be used only on flat surfaces; and</p>

<p style='margin-left:70px;'>(3) When abrasive blasting with a HEPA local vacuum exhaust sized to match the tool is performed, such method shall only be used on cast and wrought iron, steel or
concrete substrates under the supervision of a professionally qualified art or
architectural conservator.</p>

<p style='margin-left:50px;'>(e) When soil lead levels are equal to or greater than 5,000 parts per million (ppm), soil abatement shall occur by one of the following:</p>

<p style='margin-left:70px;'>(1) The contaminated soil shall be completely excavated to a depth of at least 6 inches and replaced with soil containing less than 200 ppm lead;</p>

<p style='margin-left:70px;'>(2) When the soil below 2 inches from the surface has been found to contain lead
below 1,500 ppm, the contaminated soil shall be excavated to a depth of at least 2
inches, the remaining rototilled, and the excavated soil replaced with soil containing
less than 200 ppm lead; or</p>

<p style='margin-left:70px;'>(3) The contaminated soil shall be completely enclosed with asphalt or concrete.</p>

<p style='margin-left:50px;'>(f) During soil abatement:</p>

<p style='margin-left:70px;'>(1) Surface run-off and the windblown spread of lead-contaminated soil shall be prevented by either:</p>

<p style='margin-left:100px;'>a. Keeping bare soil wet during the entire period of abatement; or</p>

<p style='margin-left:100px;'>b. Temporarily covering exposed sites with polyethylene sheeting with the covering secured in place at all edges and seams;</p>

<p style='margin-left:70px;'>(2) Soil removal activities shall not be conducted when:</p>

<p style='margin-left:100px;'>a. The constant wind speed exceeds 20 miles per hour; or</p>

<p style='margin-left:100px;'>b It is raining in such a manner as to create surface run-off of contaminated soil; and</p>

<p style='margin-left:70px;'>(3) All contaminated soil shall be disposed of in accordance with He-P 1608.11(e) and (f)</p>

<p style='margin-left:250px;'><a href='http://www.gencourt.state.nh.us/rules/state_agencies/sourcehe-p.html'>Source.</a> #5920, eff 1-1-95; amd by #6096, eff 9-22-
95; ss by #7181, eff 12-24-99; ss by #7495, eff 5-23-
01; ss by #8039, eff 2-13-04; ss by #8932, eff 7-6-07;
ss by #9986, eff 9-1-11 (from He-P 1605.08)</p>

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px; margin-top:40px;'> PART He-P 1610 STANDARDS FOR INTERIM CONTROLS </p>

<p style='font-weight:bold;'>He-P 1610.01 Interim Control Standards<</p>

<p style='margin-left:50px;'>(a) He-P 1610 shall apply to any owner or person who has been issued an order of lead
hazard reduction and who has chosen to utilize interim controls as a means of
temporarily reducing lead exposure hazards on a dwelling, dwelling unit or child care
facility.</p>

<p style='margin-left:50px;'>(b) All interim control activities shall be conducted in accordance with RSA 130-A and He-P 1600.</p>

<p style='margin-left:50px;'>(c) An owner, owner-contractor, licensed lead abatement contractor, licensed lead
inspector or licensed risk assessor shall request permission in writing from the
commissioner to use interim controls as an alternative to abatement by submitting a
“Request for Use of Interim Controls” form (January 2011 edition) and a copy of the
LEHRP as described He-P 1608.05 to the department at least 5 business days prior to
the anticipated start date of lead hazard reduction work.</p>

<p style='margin-left:50px;'>(d) Interim control work shall not be conducted until written approval is received from the department.</p>

<p style='margin-left:50px;'>(e) The commissioner shall deny a request to use interim controls when it is found that:</p>

<p style='margin-left:70px;'>(1) The person making the request has been found in violation of one or more provisions of RSA 130-A and He-P 1600; or</p>

<p style='margin-left:70px;'>(2) The requested plan does not satisfy the intent of the rules as an alternative to complying with the rules.</p>

<p style='margin-left:50px;'>(f) The commissioner shall revoke permission for the use of interim controls and order the owner to abate all lead exposure hazards when it is found that:</p>

<p style='margin-left:70px;'>(1) The owner has failed to maintain a current certificate of compliance – interim controls; or</p>

<p style='margin-left:70px;'>(2) When a compliance inspection conducted in accordance with He-P 1605.04
reveals that the property with a certificate of compliance – interim controls no longer
meets the requirements of He-P 1600.</p>

<p style='margin-left:250px;'><a href='http://www.gencourt.state.nh.us/rules/state_agencies/sourcehe-p.html'>Source.</a> #5920, eff 1-1-95; ss by #7181, eff 12-24-99; ss by
#7495, eff 5-23-01; ss by #8039, eff 2-13-04; ss by #8932,
eff 7-6-07; ss by #9986, eff 9-1-11 (from He-P 1606.02)</p>

<p style='font-weight:bold;'>
He-P 1609.03 Encapsulant Products and Their Use
</p>

<p style='margin-left:50px;'>(a) Encapsulant products shall be approved in accordance with RSA 130-A:1, VII, or (p) and (q) below prior to their use.</p>

<p style='margin-left:50px;'>(b) Except for a licensed lead abatement contractor or owner-contractor, any person who
wishes to use an encapsulant product shall request permission from the department in
writing prior to initiating the work or activity and include a copy of the LEHRP describing
the components that encapsulant products are requested to be used on.</p>

<p style='margin-left:50px;'>(c) Encapsulant products shall be applied:</p>

<p style='margin-left:80px;'>(1) After passing substrate assessment testing using the “Pull-Off Tape Test for
Adhesion” or the “Assessment of Painted Surfaces for Adhesion” (ASTM E 1796-
03), for each architectural system, element or building component where an
encapsulant product is to be used;</p>

<p style='margin-left:80px;'>(2) Only after all surface preparation, and any other phases of lead hazard reduction work, including painting, component removal or both, is complete;</p>

<p style='margin-left:80px;'>(3) In accordance with the manufacturer’s criteria; and</p>

<p style='margin-left:80px;'>(4) In accordance with ASTM E 1796-03 Standard Guide for Selection and Use of Liquid Coating Encapsulation Products for Leaded Paint in Buildings.</p>

<p style='margin-left:50px;'>(d) Encapsulant products shall not be used on any surface(s) that:</p>

<p style='margin-left:80px;'>(1) Fails the substrate assessments tests such as the “Pull-Off Tape Test for Adhesion” or the “Assessment of Painted Surfaces for Adhesion” (ASTM E 1796-03); or</p>

<p style='margin-left:80px;'>(2) Is not recommended for encapsulation or restricted by the product
manufacturer.</p>

<p style='margin-left:50px;'>(e) Surface preparation as described in (c)(2) shall include:</p>

<p style='margin-left:80px;'>(1) Cleaning and deglossing with a strong detergent or similar deglossing agent or by wet sanding, if necessary;</p>

<p style='margin-left:80px;'>(2) Making minor repairs such as filling holes with plaster or spackling; and</p>

<p style='margin-left:80px;'>(3) Paint stabilization of the interior, exterior or both, as described in He-P 1610.02, as required.</p>

<p style='margin-left:50px;'>(f) All encapsulant debris generated through the application process and any unused
encapsulant not suitable for application shall be disposed of in accordance with the
encapsulant manufacturer’s instructions.</p>

<p style='margin-left:50px;'>(g) When encapsulant products have been used and the dwelling, dwelling unit or child
care facility has no documentation of passing the substrate assessment or the substrate
assessment test has been failed for any architectural system, element or building
component, the risk assessor shall not issue a certificate of compliance – abatement.</p>

<p style='margin-left:50px;'>(h) The owner shall perform a visual inspection of the encapsulated surfaces as recommended by the manufacturer and as follows:</p>

<p style='margin-left:80px;'>(1) 30 days after application;</p>

<p style='margin-left:80px;'>(2) 6 months after application;</p>

<p style='margin-left:80px;'>(3) Annually thereafter; and</p>

<p style='margin-left:80px;'>(4) Whenever there is a change in tenant occupancy.</p>

<p style='margin-left:50px;'>(i) The visual inspection required by (h) above shall determine whether the encapsulant has maintained its integrity and is not:</p>

<p style='margin-left:80px;'>(1) Cracked;</p>

<p style='margin-left:80px;'>(2) Peeling;</p>

<p style='margin-left:80px;'>(3) Sagging;</p>

<p style='margin-left:80px;'>(4) Bubbling;</p>

<p style='margin-left:80px;'>(5) Water damaged or evidencing other moisture related problems;</p>

<p style='margin-left:80px;'>(6) Blistering;</p>

<p style='margin-left:80px;'>(7) Open to the environment in a manner that could damage the encapsulated area; or</p>

<p style='margin-left:80px;'>(8) Otherwise altered in a manner which jeopardizes its protective qualities.</p>

<p style='margin-left:50px;'>(j) If signs of wear or deterioration, as described in (i) above, are found during the visual
inspection, the owner shall visually inspect the encapsulated surfaces at least every 3
months for the next 6 months, then annually thereafter.</p>

<p style='margin-left:50px;'>(k) If the encapsulation fails to maintain its integrity or if repairs are needed and the
affected area involves less than 6 square feet of surface, the repair shall be considered in-
place management and shall be remedied in accordance with the encapsulant
manufacturer’s recommendations, He-P 1608, and He-P 1610.02 through He-P 1610.05.</p>

<p style='margin-left:50px;'>(l) When repairing a surface as described in (k) above, a property owner shall not engage in any practice prohibited under He-P 1609.02(c).</p>

<p style='margin-left:50px;'>(m) When a repair of the affected area involves more than 6 square feet of surface area,
the property owner shall remedy in accordance with He-P 1608 and either He-P 1609 or
He-P 1610, including the requirement for a clearance inspection with dust wipes for the
area where work occurred.</p>

<p style='margin-left:50px;'>(n) In addition to the record keeping requirements of He-P 1608.15, the owner shall maintain the following records for the life of the encapsulant product:</p>

<p style='margin-left:80px;'>(1) Documentation of:</p>

<p style='margin-left:120px;'>a. The name of the encapsulant product applied;</p>

<p style='margin-left:120px;'>b. The results of the “Pull-Off Tape Test for Adhesion” or the “Assessment of Painted Surfaces for Adhesion” test (ASTM E 1796-03);</p>

<p style='margin-left:120px;'>c. The location of the encapsulant application; and</p>

<p style='margin-left:120px;'>d. The date of encapsulant application; and</p>

<p style='margin-left:80px;'>(2) Written documentation of the visual inspections required by (h) through (j) above.</p>

<p style='margin-left:50px;'>(o) The owner shall make all records required by (n) above available to:</p>

<p style='margin-left:80px;'>(1) The commissioner upon request; and</p>

<p style='margin-left:80px;'>(2) An owner or entity upon the sale, lease, rental or transfer of interest in the dwelling, dwelling unit or child care facility.</p>

<p style='margin-left:50px;'>(p) The commissioner shall approve encapsulant products for lead hazard reduction work that have been tested and meet or exceed:</p>

<p style='margin-left:80px;'>(1) ASTM E 1795-04, Standard Specification for Non-Reinforced Liquid Coating Encapsulation Products for Leaded Paint in Buildings; or</p>

<p style='margin-left:80px;'>(2) ASTM E 1797-04, Standard Specification for Reinforced Liquid Coating Encapsulation Products for Leaded Paint in Buildings.</p>

<p style='margin-left:50px;'>(q) Manufacturers shall submit the following documentation to the commissioner prior to the encapsulation product being approved:</p>

<p style='margin-left:80px;'>(1) Documentation in the form of a performance testing report showing:</p>

<p style='margin-left:120px;'>a. Compliance with the applicable ASTM standard;</p>

<p style='margin-left:120px;'>b. That all testing was conducted by an independent and National
Voluntary Laboratory Accreditation Program (NVLAP) certified testing laboratory; and</p>

<p style='margin-left:120px;'>c. The minimum dry film thickness at which the lead encapsulant product meets or exceeds the requirements of the applicable ASTM standard in (p) above for interior and/or exterior use; and</p>

<p style='margin-left:80px;'>(2) Documentation showing that the encapsulation product:</p>

<p style='margin-left:120px;'>a. Is warranteed by the product manufacturer to perform for at least 20 years as a durable barrier between the lead-based paint and the environment in locations or conditions similar to those of the planned application; and</p>

<p style='margin-left:120px;'>b. Is formulated with an FDA-approved anti-ingestant ingredient which
deters oral contact with the cured film and which discourages ingestion of
delaminated coatings.</p>

<p style='margin-left:250px;'><a href='http://www.gencourt.state.nh.us/rules/state_agencies/sourcehe-p.html'>Source.</a> #5920, eff 1-1-95; ss by #7181, eff 12-24-99;
ss by #7495, eff 5-23-01; ss by #8039, eff 2-13-04; ss
by #8932, eff 7-6-07; ss by #9986, eff 9-1-11 (from
He-P 1605.09</p>

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px; margin-top:40px;'> Testing Protocols </p>

<p style='margin-left:80px;'>The following steps are taken by a licensed lead inspector to conduct an accurate lead inspection:</p>

<p style='margin-left:50px;'>1. Draw a floor plan for the property.</p>

<p style='margin-left:50px;'>2. Calibrate the XRF</p>

<p style='margin-left:50px;'>3. List all painted testing combinations by room equivalent including
the color and substrates (metal, sheetrock, concrete, plaster,
wood, vinyl).</p>

<p style='margin-left:50px;'>4. Select testing combinations, location and surface of where to test with XRF.</p>

<p style='margin-left:50px;'>5. Test surfaces with the XRF </p>

<p style='margin-left:65px;'>a. Collect and analyze paint chip samples if necessary.</p>

<p style='margin-left:50px;'>6. Classify the condition of the lead surfaces on the report pages</p>

<p style='margin-left:50px;'>7. Post Calibrate the XRF</p>

<p style='margin-left:50px;'>8. Evaluate the work and results to ensure the quality of the inspection.</p>

<p style='margin-left:50px;'>9. Document all findings in the report.</p>

<p style='margin-left:50px;'>10. Submit the completed report within 10 days of the inspection.</p>

<p style='font-weight:bold; font-size:20px;'> Calibration </p>

<p style='text-indent:50px;'>Calibrate the XRF prior to and at the completion of lead paint testing and at least every
four (4) hours. To calibrate the NITON XLP 300, put the XRF in K&L mode on the 1.02 block. Each
of the three readings must be taken for 20 nominal seconds, as shown on the XRF screen. When
performing the lead inspection, switch to Standard Mode.</p>

<p style='font-weight:bold; font-size:20px;'> Testing </p>

<p style='text-indent:50px;'>When properties are under order, full lead inspections must be performed on the interior
of the unit and all interior and exterior common areas. For multi-family housing, all interior
common areas accessible to occupants must be included in the inspection (i.e. for 3 story
buildings, all interior stairways must be inspected for any unit in the property).</p>

<p style='text-indent:50px;'>It is the inspector's job to test a surface that is representative of each type of painted (or otherwise coated) testing combination in every room equivalent.</p>

<p style='text-indent:50px;'>When testing for lead paint using an XRF, certain adjacent building components can be
grouped together if they have the same painting history. A testing combination is made up of the
room equivalent, component and substrate. If they are all similar they can be grouped together
and tested once.</p>

<p style='font-weight:bold; font-size:20px;'> Testing Combination </p>

<p style='text-indent:50px;'>A testing combination is a unique combination of room equivalent, building component
type, and substrate. The following may be grouped together as a single testing combination as
long as they have the same painting histories:</p>

<p style='font-weight:bold; font-size:20px;'> Walls </p>

<p style='text-indent:50px;'>All four walls should be tested separately. Floor, radiator, and ceilings should each be
tested once</p>

<p style='font-weight:bold; font-size:20px;'>Window Systems </p>

<p style='text-indent:50px; margin:20px;'>Testing combination 1: aprons, casing, interior stops, and jambs</p>
<p style='text-indent:50px;  margin:20px;'>Testing combination 2: interior window muntins and window sashes</p>
<p style='text-indent:50px;  margin:20px;'>Testing combination 3: exterior window muntins and window sashes</p>
<p style='text-indent:50px;  margin:20px;'>Testing combination 4: Interior window sill</p>

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px;'> Performance Characteristic Sheet </p>

<p style='font-weight:bold;'>EFFECTIVE DATE: <span style='margin-left:25px;'>September 24, 2004 </span> <span style='margin-left:250px;'>EDITION NO.: 1</span></p>

<p style='font-weight:bold; margin-bottom:5px;'>MANUFACTURER AND MODEL:</p>

<span style='margin-left:50px;'>Make:</span> 
<span style='margin-left:89px;'>Niton LLC</span><br/>

<span style='margin-left:50px;'>Tested Model:</span> 
<span style='margin-left:28;'>XLp 300</span> <br/>

<span style='margin-left:50px;'>Source:</span> 
<span style='margin-left:80px;'>109 Cd</span> <br/>

<span style='margin-left:50px;'>Note:</span> 
<p style='margin-left:183px; margin-top:-20px;'>This PCS is also applicable to the equivalent model variations indicated below, for the Lead-in-Paint K+L variable reading time mode, in the XLi and XLp series:</p>

<span style='margin-left:210px; margin-bottom:5px;'>XLi 300A, XLi 301A, XLi 302A and XLi 303A.</span> <br/>
<span style='margin-left:210px; margin-bottom:5px;'>XLp 300A, XLp 301A, XLp 302A and XLp 303A.</span> <br/>
<span style='margin-left:210px; margin-bottom:5px;'>XLi 700A, XLi 701A, XLi 702A and XLi 703A.</span> <br/>
<span style='margin-left:210px; margin-bottom:5px;'>XLp 700A, XLp 701A, XLp 702A, and XLp 703A.</span> <br/> <br/> 

<span>Note:</span>
<p style='margin-left:45px; margin-top:-22px;'>The XLi and XLp versions refer to the shape of the handle part of the instrument. The
differences in the model numbers reflect other modes available, in addition to Lead-in-
Paint modes. The manufacturer states that specifications for these instruments are
identical for the source, detector, and detector electronics relative to the Lead-in-Paint
mode.</p>

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px;'> FIELD OPERATION GUIDANCE </p>

<p style='font-weight:bold; margin-bottom:5px;'>OPERATING PARAMETERS:</p>
<p style='margin-top:-10px;'>Lead-in-Paint K+L variable reading time mode.</p>

<p style='font-weight:bold; margin-bottom:5px;'>XRF CALIBRATION CHECK LIMITS:</p>
<table border='1' style='margin-top:-10px; margin-bottom:15px; width: 100%;'>
	<tr>
		<td style='padding:10px;'>____________ (inclusive)</td>
	</tr>
</table>

<p>The calibration of the XRF instrument should be checked using the paint film nearest 1.0 mg/cm 2 in the NIST Standard Reference Material (SRM) used (e.g., for NIST SRM 2579, use the 1.02 mg/cm 2 film).</p>

<p>If readings are outside the acceptable calibration check range, follow the manufacturer's instructions to bring the instruments into control before XRF testing proceeds.</p>

<p style='font-weight:bold; margin-bottom:5px;'>SUBSTRATE CORRECTION:</p>

<p style='margin-top:-10px;'>For XRF results using Lead-in-Paint K+L variable reading time mode, substrate correction is not needed for:</p>

<p style='text-indent:50px;'>Brick, Concrete, Drywall, Metal, Plaster, and Wood</p>

<p style='font-weight:bold; margin-bottom:5px;'>INCONCLUSIVE RANGE OR THRESHOLD:</p>

<table border='1' style='width: 100%; margin-bottom:30px;'>
	<tr>
		<td style='text-align:center; font-weight:bold;'>K+L MODE <br/> READING DESCRIPTION</td>
		<td style='text-align:center; font-weight:bold;'>SUBSTRATE</td>
		<td style='text-align:center; font-weight:bold;'>THRESHOLD <br/> (mg/cm²)</td>
	</tr>
	<tr>
		<td style='text-align:center;'>____________</td>
		<td style='text-align:center;'>
			____________ <br/>
			____________ <br/>
			____________ <br/>
			____________ <br/>
			____________ <br/>
			____________ <br/>
		</td>
		<td style='text-align:center;'>
			____________ <br/>
			____________ <br/>
			____________ <br/>
			____________ <br/>
			____________ <br/>
			____________ <br/>
		</td>
	</tr>
</table>

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px;'> BACKGROUND INFORMATION </p>

<p style='font-weight:bold;'>EVALUATION DATA SOURCE AND DATE:</p>

<p>This sheet is supplemental information to be used in conjunction with Chapter 7 of the HUD Guidelines for
the Evaluation and Control of Lead-Based Paint Hazards in Housing &quot;HUD Guidelines&quot;. Performance
parameters shown on this sheet are calculated from the EPA/HUD evaluation using archived building
components. Testing was conducted in August 2004 on 133 testing combinations. The instruments that
were used to perform the testing had new sources; one instrument’s was installed in November 2003 with 40 mCi initial strength, and the other’s was installed June 2004 with 40 mCi initial strength.</p>

<p style='font-weight:bold;'>OPERATING PARAMETERS:</p>

<p>Performance parameters shown in this sheet are applicable only when properly operating the instrument susing the manufacturer's instructions and procedures described in Chapter 7 of the HUD Guidelines.</p>

<p style='font-weight:bold;'>SUBSTRATE CORRECTION VALUE COMPUTATION:</p>

<p>Substrate correction is not needed for brick, concrete, drywall, metal, plaster or wood when using Lead-in- Paint K+L variable reading time mode, the normal operating mode for these instruments. If substrate correction is desired, refer to Chapter 7 of the HUD Guidelines for guidance on correcting XRF results for substrate bias.</p>

<p style='font-weight:bold;'>EVALUATING THE QUALITY OF XRF TESTING:</p>

<p>Randomly select ten testing combinations for retesting from each house or from two randomly selected units in multifamily housing. Use the K+L variable time mode readings.</p>

<p>Conduct XRF retesting at the ten testing combinations selected for retesting.</p>

<p>Determine if the XRF testing in the units or house passed or failed the test by applying the steps below.</p>

<p style='margin-left:50px;'>Compute the Retest Tolerance Limit by the following steps:</p>

<p style='margin-left:80px;'>Determine XRF results for the original and retest XRF readings. Do not correct the
original or retest results for substrate bias. In single-family housing a result is defined as
the average of three readings. In multifamily housing, a result is a single reading.
Therefore, there will be ten original and ten retest XRF results for each house or for the
two selected units.</p>

<p style='margin-left:120px;'>Calculate the average of the original XRF result and retest XRF result for each testing combination.</p>

<p style='margin-left:120px;'>Square the average for each testing combination.</p>

<p style='margin-left:120px;'>Add the ten squared averages together. Call this quantity C.</p>

<p style='margin-left:120px;'>Multiply the number C by 0.0072. Call this quantity D.</p>

<p style='margin-left:120px;'>Add the number 0.032 to D. Call this quantity E.</p>

<p style='margin-left:120px;'>Take the square root of E. Call this quantity F.</p>

<p style='margin-left:50px;'>Multiply F by 1.645. The result is the Retest Tolerance Limit.</p>

<p style='margin-left:50px;'>Compute the average of all ten original XRF results.</p>

<p style='margin-left:50px;'>Compute the average of all ten re-test XRF results.</p>

<p style='margin-left:50px;'>Find the absolute difference of the two averages.</p>

<p style='margin-left:50px;'>If the difference is less than the Retest Tolerance Limit, the inspection has passed the retest. If
the difference of the overall averages equals or exceeds the Retest Tolerance Limit, this
procedure should be repeated with ten new testing combinations. If the difference of the overall
averages is equal to or greater than the Retest Tolerance Limit a second time, then the
inspection should be considered deficient.</p>

<p>Use of this procedure is estimated to produce a spurious result approximately 1% of the time. That is, results of this procedure will call for further examination when no examination is warranted in approximately 1 out of 100 dwelling units tested.</p>

<p style='font-weight:bold;'>TESTING TIMES:</p>

<p>For the Lead-in-Paint K+L variable reading time mode, the instrument continues to read until it is moved
away from the testing surface, terminated by the user, or the instrument software indicates the reading is
complete. The following table provides testing time information for this testing mode. The times have
been adjusted for source decay, normalized to the initial source strengths as noted above. Source
strength and type of substrate will affect actual testing times. At the time of testing, the instruments had
source strengths of 26.6 and 36.6 mCi.</p>












<div style='page-break-after:always;'></div> 

<table border='1' style='width:100%; margin-bottom:20px;'>

<tr>
	<td style='text-align:center; font-weight:bold; padding:10px;' colspan='7'>
		Testing Times Using K+L Reading Mode (Seconds)
	</td>
</tr>

<tr>
	<td style='text-align:center;'>
		
	</td>
	<td style='text-align:center;' colspan='3'>
		All Data
	</td>
	<td style='text-align:center;' colspan='3'>
		Median for laboratory-measured lead levels (mg/cm²)
	</td>
</tr>

<tr>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________ 
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
</tr>

<tr>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
</tr>

<tr>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
</tr>

<tr>
	<td style='text-align:center;'>
		____________ <br/> ____________ <br/> ____________ <br/> ____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
</tr>

</table>

<p style='font-weight:bold;'>CLASSIFICATION RESULTS:</p>
<p>XRF results are classified as positive if they are greater than or equal to the threshold, and negative if they are less than the threshold.</p>

<p style='font-weight:bold;'>DOCUMENTATION:</p>
<p>A document titled Methodology for XRF Performance Characteristic Sheets provides an explanation of
the statistical methodology used to construct the data in the sheets, and provides empirical results from
using the recommended inconclusive ranges or thresholds for specific XRF instruments. For a copy of
this document call the National Lead Information Center Clearinghouse at 1-800-424-LEAD.</p>

<table border='1'>
<tr>
<td style='padding:5px;'>This XRF Performance Characteristic Sheet was developed by the Midwest Research Institute (MRI)
and QuanTech, Inc., under a contract between MRI and the XRF manufacturer. HUD has determined
that the information provided here is acceptable when used as guidance in conjunction with Chapter 7,
Lead-Based Paint Inspection, of HUD’s Guidelines for the Evaluation and Control of Lead-Based Paint
Hazards in Housing.</td>
</tr>
</table>

<div style='page-break-after:always;'></div> 

<table style='width:100%;'>

<tr>

<td style='text-align:left;'>
25 Lowell St. <br/>
Manchester,  <br/>
NH 03101 </td>

<td style='text-align:center;'><img src='images/Untitled.png' style='border-style: solid; border-color:green; margin-bottom:5px;'/> <br/>
<a href='http://www.kkirkwood.com'>www.kkirkwood.com</a> <br/>
Lead Inspection / Risk Assessment Report
</td>

<td style='text-align:right;'>
Phone: 603-781-4304 <br/>
Email: <br/>
Kate@kkirkwood.com</td>

</tr>

</table>

<p style='text-align:right;'>X-Ray Fluorescence Niton Xlp 303 AW: Serial number 8988</p>

Property Address:

<table style='width:100%; border-bottom:25px; border-collapse: collapse;'>
<tr>

<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>

</tr>

<tr>

<td style='text-align:center;'>Street Number</td>
<td style='text-align:center;'>Address</td>
<td style='text-align:center;'>Apartment #</td>
<td style='text-align:center;'>City</td>
<td style='text-align:center;'>State</td>
<td style='text-align:center;'>Zip</td>

</tr>
</table>

<br/>

Owner Information:

<table style='width:100%; border-bottom:25px; border-collapse: collapse;'>
<tr>

<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>

</tr>

<tr>

<td style='text-align:center;'>First Name</td>
<td style='text-align:center;'>Last Name</td>
<td style='text-align:center;'>Address</td>
<td style='text-align:center;'>City</td>
<td style='text-align:center;'>State</td>
<td style='text-align:center;'>Zip</td>

</tr>
</table>

<br/>

<table style='border-bottom:25px; border-collapse: collapse; width:100%'>
<tr>

<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>

</tr>

<tr>

<td style='text-align:center;'>Phone</td>
<td style='text-align:center;'>Email</td>

</tr>

</table>

<br/>

Client Information (if different from owner):

<table style='border-bottom:25px; border-collapse: collapse; width:100%'>

<tr>

<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>

</tr>

<tr>

<td style='text-align:center;'>Address</td>
<td style='text-align:center;'>City</td>
<td style='text-align:center;'>State</td>
<td style='text-align:center;'>Zip</td>

</tr>

</table>

<br/>
<br/>
<span>
<table border='1' style='margin-left:20px;' cellspacing='0' cellpadding='0'>
	<tr>
		<td style='padding: 10px;'>Depth Indicator:</td> 
		<td style='text-align:center; padding: 10px;'>1st</td>
		<td style='text-align:center; padding: 10px;'>2nd</td>
	</tr>
	<tr>
		<td style='padding: 10px;'>RES</td> 
		<td colspan='2' style='padding: 10px; text-align:center;'>__</td>		
	</tr>
	<tr>
		<td colspan='3' style='padding: 10px;'>Calibration:</td>
	</tr>
	<tr>
		<td style='padding: 10px;'>Start: 1st</td> 
		<td style='padding: 10px;'>Start: __</td> 
		<td style='padding: 10px;'>Start: __</td>
	</tr>
	<tr>
		<td style='padding: 10px;'>2nd</td> 
		<td style='padding: 10px;'>Start: __</td> 
		<td style='padding: 10px;'>Start: __</td>
	</tr>
	<tr>
		<td style='padding: 10px;'>3nd</td> 
		<td style='padding: 10px;'>Start: __</td> 
		<td style='padding: 10px;'>Start: __</td>
	</tr>
	<tr>
		<td colspan='3' style='padding: 10px;'>Calibration:</td>
	</tr>
	<tr>
		<td style='padding: 10px;'>End: 1st</td> 
		<td style='padding: 10px;'>Start: __</td> 
		<td style='padding: 10px;'>Start: __</td>
	</tr>
	<tr>
		<td style='padding: 10px;'>2nd</td> 
		<td style='padding: 10px;'>Start: __</td> 
		<td style='padding: 10px;'>Start: __</td>
	</tr>
	<tr>
		<td style='padding: 10px;'>3nd</td> 
		<td style='padding: 10px;'>Start: __</td> 
		<td style='padding: 10px;'>Start: __</td>
	</tr>
</table>


<table border='1' style='margin-top:-405px; margin-left:270px;' cellspacing='0' cellpadding='0'>

<tr>
<td style='padding: 10px;'>Number of Rooms in Unit:</td>
<td style='text-align:center; padding:10px;'>__</td>
</tr>

<tr>
<td style='padding: 10px;'>Property Type:</td>
<td style='text-align:center; padding:10px;'>__________</td>
</tr>

<tr>
<td style='padding: 10px;'>Condominium # of Units:</td>
<td style='text-align:center; padding:10px;'>__</td>
</tr>

<tr>
<td style='padding: 10px;'>Year of Construction:</td>
<td style='text-align:center; padding:10px;'>____</td>
</tr>

</table>

</span>

<div style='page-break-after:always;'></div> 

<table border='1' style='width:100%'>

<tr>
<td style='font-weight:bold; text-align:center;'>Key:</td>
<td style='font-weight:bold; text-align:center;'>Lead Column</td>
<td style='font-weight:bold; text-align:center;'>Key:</td>
<td style='font-weight:bold; text-align:center;'>Abatement/I.C. Method Column</td>

</tr>

<tr>
<td style='text-align:center;'>COV</td>
<td style='text-align:center;'>Covered</td>
<td style='text-align:center;'>CAP</td>
<td style='text-align:center;'>Capped</td>
</tr>

<tr>
<td style='text-align:center;'>VNL</td>
<td style='text-align:center;'>Vinyl</td>
<td style='text-align:center;'>COV</td>
<td style='text-align:center;'>Enclosed</td>
</tr>

<tr>
<td style='text-align:center;'>MET</td>
<td style='text-align:center;'>Metal</td>
<td style='text-align:center;'>Enclosed</td>
<td style='text-align:center;'>Encapsulated</td>
</tr>

<tr>
<td style='text-align:center;'>VR</td>
<td style='text-align:center;'>Vinyl Rep. Window</td>
<td style='text-align:center;'>PS</td>
<td style='text-align:center;'>Paint Stabilization</td>
</tr>

<tr>
<td style='text-align:center;'>MR</td>
<td style='text-align:center;'>Metal Rep Window</td>
<td style='text-align:center;'>PRE</td>
<td style='text-align:center;'>Prepared for ENC</td>
</tr>

<tr>
<td style='text-align:center;'>NA</td>
<td style='text-align:center;'>Not Accessible</td>
<td style='text-align:center;'>VR/MR</td>
<td style='text-align:center;'>Vinyl/Metal Rep Window</td>
</tr>

<tr>
<td style='text-align:center;'>NC</td>
<td style='text-align:center;'>No Coating</td>
<td style='text-align:center;'>SFR</td>
<td style='text-align:center;'>Storm Frame Removed</td>
</tr>

<tr>
<td style='text-align:center;'>Tile</td>
<td style='text-align:center;'>Tile (Testing Suggested)</td>
<td style='text-align:center;'>\</td>
<td style='text-align:center;'>Component does not exist</td>
</tr>

<tr>
<td style='text-align:center;'>DC</td>
<td style='text-align:center;'>Dropped Ceiling</td>
<td style='text-align:center;'>SCR</td>
<td style='text-align:center;'>Scraped</td>
</tr>

<tr>
<td></td>
<td></td>
<td style='text-align:center;'>DIP</td>
<td style='text-align:center;'>Dipped</td>
</tr>

<tr>
<td></td>
<td></td>
<td style='text-align:center;'>REM</td>
<td style='text-align:center;'>Removed</td>
</tr>

<tr>
<td></td>
<td></td>
<td style='text-align:center;'>REP</td>
<td style='text-align:center;'>Replaced</td>
</tr>

<tr>
<td></td>
<td></td>
<td style='text-align:center;'>REV</td>
<td style='text-align:center;'>Reversed</td>
</tr>

<tr>
<td></td>
<td></td>
<td style='text-align:center;'>INT</td>
<td style='text-align:center;'>Intact</td>
</tr>

</table>

Comments/Notes: </br>
<p>____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________</p>

<p style='text-align:right; margin:0px; padding:0px;'></p>
<p style='text-align:right; margin:0px; padding:0px;'>Name of Inspector</p>

<p>Floor # ____ (this is the level within building of unit being inspected) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Floor # ____</p>

<center><img src='images/floor_1.png' style='height:100%; width:80%;'/></center>

<div style='page-break-after:always;'></div> 

<table style='border-bottom:25px; border-collapse: collapse; width:100%; margin-bottom:15px;'>

<tr>

<td style='border-bottom: 1px solid #000;'>__________________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>__________________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>__________________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>__________________</td>
<td style='border-bottom: 1px solid #000;'>Page _ of _</td>
	
</tr>

<tr>

<td>Inspector / Risk Assessor</td>
<td style='text-align:center;'>Lic # </td>
<td style='text-align:center;'>Signature</td>
<td style='text-align:center;'>Date</td>
<td style='text-align:center;'></td>

</tr>

</table>

Radiation Handlers Lic # 487R

<table style='border-bottom:25px; border-collapse: collapse; width:100%'>

<tr>

<td style='border-bottom: 1px solid #000;'>Address:</td>
<td style='border-bottom: 1px solid #000; font-weight:bold;'>__________________</td>
<td style='border-bottom: 1px solid #000; font-weight:bold;'>Apt. #</td>

</tr>

</table>

<br/>

<p style='font-weight:bold; margin-bottom:3px;'>ROOM # 1 LIVING ROOM</p>
<table border='1' style='border-bottom:25px; width:100%'>

<tr>

<td style='text-align:center;'>SIDE</td>
<td style='text-align:center;'>LOCATION / <br/> SURFACE</td>
<td style='text-align:center;'>LEAD mg/cm²</td>
<td style='text-align:center;'>TYPE OF <br/> HAZARD</td>
<td style='text-align:center;'>SUBSTRATE</td>
<td style='text-align:center;'>LEHRP <br/> RECOMM</td>
<td style='text-align:center;'>ABATE <br/> DATE</td>
<td style='text-align:center;'>ABATE <br/> METH</td>

</tr>

<tr>

<td style='text-align:center;'>_</td>
<td style='text-align:center;'>___________</td>
<td style='text-align:center;'>___</td>
<td style='text-align:center;'>___________</td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>

</tr>

<tr>

<td style='text-align:center;'>_</td>
<td style='text-align:center;'>___________</td>
<td style='text-align:center;'>___</td>
<td style='text-align:center;'>___________</td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>

</tr>

<tr>

<td style='text-align:center;'>_</td>
<td style='text-align:center;'>___________</td>
<td style='text-align:center;'>___</td>
<td style='text-align:center;'>___________</td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>

</tr>

<tr>

<td style='text-align:center;'>_</td>
<td style='text-align:center;'>___________</td>
<td style='text-align:center;'>___</td>
<td style='text-align:center;'>___________</td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>

</tr>

<tr>
<td colspan='8' style='padding-top:10px; padding-bottom:10px;'>_______________________________________________________</td>
</tr>

</table>



<div style='page-break-after:always;'></div> 

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px;'> Exhibit A </p>

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px;'> ______________________</p>

<center><h1>______________________</h1></center>

</body></html>");

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
//$dompdf->stream();

$output = $dompdf->output();
file_put_contents('report.pdf', $output);
echo "<script>window.location.href='report.pdf'</script>";
?>