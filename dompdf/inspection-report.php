<script language=javascript>
function openerHref() {
	document.update.popup.value = opener.location.href;
}
</script>

<?php

include( '../connect.php');
include( '../session.php');

$dateToday = date("M d, Y");
$iid = isset($_POST['iid']) ? $_POST['iid'] : (isset($_GET['iid']) ? $_GET['iid'] : 0);


$query = "SELECT * FROM inspection WHERE iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (!($row = mysql_fetch_assoc($result))) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/building-list.php");
	exit();
}
//echo "<script>alert('$row[bid]');</script>";

$query = "SELECT * FROM owners INNER JOIN client USING (cid) LEFT JOIN client_phone USING(cid) WHERE bid=$row[bid] AND startdate <= DATE(NOW()) AND enddate > DATE(NOW())";

$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
$cid = $row['cid'];
$title = "Edit Inspection";
if (!$_GET['isPopup']) {
	require_once'header.php';
}
else if ($_GET['isPopup']) {
	echo "<body bgcolor='#dddddd'>";
}

$job = "Lead";
$query = "SELECT inspector.*, inspector_phone.* FROM inspector JOIN insp_assigned ON inspector.tid = insp_assigned.tid LEFT JOIN inspector_phone ON inspector_phone.tid = inspector.tid WHERE insp_assigned.iid = '$iid' AND insp_assigned.job = '$job'";

$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$inspector= mysql_fetch_assoc($result);

//echo "<script>alert('$inspector[firstname]');</script>";


?>

<form name="update" action="report.php" method="post" enctype="multipart/form-data">
<input type="hidden" name="iid" value="<?php print $iid;?>" />
<table>
<tr><td>Upload Building Image:</td><td><input type="file" name="logo" id="fileToUpload" accept="image/*" required></td></tr>
<tr><td>LIRA:</td><td><input type='text' value='Lead Inspection / Risk Assessment' size='30' name='lira' required></td></tr>
<tr><td>Building Address:</td><td><input type='text' value='<?php echo $row['city'] . ', ' . $row['state'] . ' ' . $row['zip']?>' size='30' name='building_address' required></td></tr> 
<tr><td>Inspector Name:</td><td><input type='text' value='<?php echo $inspector['firstname'] . ' ' . $inspector['lastname']?>' size='30' name='inspector_name' required></td></tr>
<tr><td>Inspector Address:</td><td><input type='text' value='<?php echo $inspector['address']?>' size='30' name='inspector_address' required></td></tr>
<tr><td>Inspector Contact:</td><td><input type='text' value='<?php echo $inspector['number']?>' size='30' name='inspector_contact' required></td></tr>
<tr><td>Inspector Email:</td><td><input type='text' value='<?php echo $inspector['email']?>' size='30' name='inspector_email' required></td></tr>
<tr><td>Inspector License:</td><td><input type='text' value='' size='30' name='inspector_license' required></td></tr>
<tr><td>Inspector RA #:</td><td><input type='text' value='' size='30' name='inspector_ra' required></td></tr>

<tr><td>Client Name:</td><td><input type='text' value='<?php echo $row['firstname'] . ' ' . $row['lastname']?>' size='30' name='owner_name' required></td></tr>
<tr><td>Client Address:</td><td><input type='text' value='<?php echo $row['address']?>' size='30' name='owner_address' required></td></tr>
<tr><td>Client Contact:</td><td><input type='text' value='<?php echo $row['number']?>' size='30' name='owner_contact' required></td></tr>
<tr><td>Property Type:</td><td><input type='text' value='' size='30' name='property_type' required></td></tr>
<tr><td>Date Performed:</td><td><input type='text' size='30' value="<?php echo $dateToday?>" name='date_performed' required></td></tr>
<tr><td>Generate PDF Report:</td><td><input type='submit' id='submit' name='submit' value='Submit'></td></tr>
</table>
</form>

<?php
if (!$_GET['isPopup']) {
	require_once'footer.php';
}
else if ($_GET['isPopup']) {
	echo "</body>";
}
?>