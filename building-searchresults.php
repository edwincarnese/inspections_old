<?php
$title = "Buildings - Search Results";

require_once 'session.php';
require_once 'connect.php';
$cid = $_POST['cid'];
unset($_POST['cid']);

$wheres = array();
foreach ($_POST as $field => $value) {
	if ($value) {
		switch ($field) {
			case 'address':
			case 'suffix':
			case 'address2':
			case 'city':
				$value = htmlspecialchars($value);
				$wheres[] = "$field LIKE '%$value%'";
				break;
			case 'streetnum':
			case 'state':
			case 'zip':
			case 'yearbuilt':
				$wheres[] = "$field LIKE '%$value%'";
				break;
			default:
				exit("$field not coded");
		}
	}
}
$wherestring = implode(' AND ', $wheres);

if (!$wherestring) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/building-list.php");
	exit();
}

$query = "SELECT DISTINCT bid, streetnum, address, suffix, address2, city, state, zip FROM building WHERE $wherestring ORDER BY city, zip, address";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 1) {
	$bid = mysql_result($result, 0, 'bid');
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/building-view.php?bid=$bid");
	exit();
}

require_once 'header.php';

foreach ($_POST as $field => $value) {
	if ($value) {
		switch ($field) {
			case 'address':
				$getstring[] = "address=$value";
				break; 
			case 'suffix':
				$getstring[] = "suffix=$value";
				break;
			case 'address2':
				$getstring[] = "address2=$value";
				break;
			case 'city':
				$getstring[] = "city=$value";
				break;
			case 'streetnum':
				$getstring[] = "streetnum=$value";
				break;
			case 'state':
				$getstring[] = "state=$value";
				break;
			case 'zip':
				$getstring[] = "zip=$value";
				break;
			case 'yearbuilt':
				$getstring[] = "yearbuilt=$value";
				break;
			default:
				break;
		}
	}
}
$getstring = implode('&', $getstring);


print "<p><a href=\"building-add.php?$getstring\">Click here to add a building.</a></p>";

print "<p>";
while ($row = mysql_fetch_row($result) ) {
	$bid = array_shift($row);
	$row[1]=$row[0]." ".$row[1]." ".$row[2];
	unset($row[0]);
	unset($row[2]);
	print "<a href=\"javascript:refreshIframe($bid)\">".implode(', ', $row)."</a><br />\n";
}
print "</p>";

?>
<script language="JavaScript">
<!--
function calcHeight()
{
  //find the height of the internal page
  var the_height=
    document.getElementById('floater2').contentWindow.
      document.body.scrollHeight;

  //change the height of the iframe
  document.getElementById('floater2').height=
      the_height;
}
//-->
</script>
<?php
$tempcid = isset($_GET['cid']) ? $_GET['cid'] : 0;
$showFloat = "<iframe src='building-float.php?bid=0&cid=$tempcid' scrolling='no' height='277px' width='368px' frameborder='0' marginwidth='1' marginheight='1' id='floater' name='floater'></iframe>";
$showFloat2 = "<iframe src='' scrolling='no' onLoad='calcHeight();' height='1' width='368px' frameborder='0' marginwidth='1' marginheight='1' id='floater2' name='floater2'></iframe>";
require_once'footer.php';
print "<script language='javascript'>\n";
print "function fill_form() {\n";
foreach ($_POST as $key=>$blah) {
	print "frames['floater'].document.searchForm.$key.value='$blah';\n";
}
print "}</script>\n";
?>
<body onLoad="javascript:fill_form();"/>
<script language="javascript">
function refreshIframe(bidvar) {
	frames['floater2'].location.href = 'building-float.php?bid='+bidvar+'&cid='+<?php echo $cid;?>;
}
</script>