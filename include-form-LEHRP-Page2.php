<?php

$pdf->AddPage('P');
//$pdf->Image('images/fixed/comprehensive-page2.png', 0, 0, 8.5, 11.0, 'PNG');

$query = "SELECT iid, number FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

$iid = $row['iid'];

$x=0;
$y=0;
$lh = 0.4;
// Inspecttion number and pagenumbers
$pdf->SetFont('Times','','10');
$x=1;
$y=$y + 0.2 ;
$pdf->SetXY($x, $y+.37);
$pdf->Cell(0,0, "InspectiOn $row[iid]-$row[number]");

$x=$x+5.5 ;
$pdf->SetXY($x, $y+.37);
$pdf->Cell(0,0, 'Page ');
$x=$x+.4 ;
$pdf->SetXY($x,$y+.37);
$pdf->Cell(0,0, $pdf->PageNo());
$x=$x +.1;
$pdf->SetXY($x,$y+.37);
$pdf->Cell(0,0, ' of ');
$pdf->SetXY($x,$y+.37);
$x=$x+.3 ;
$pdf->SetXY($x,$y+.37);
$pdf->Cell(0,0, '{totalpages}');

//ADDRESS
$query = "SELECT streetnum, address, suffix, city, unitdesc FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

$y=$y+.15;
$pdf->SetXY(1.4, $y);
$pdf->Cell(0,0, "$row[unitdesc], $row[streetnum] $row[address] $row[suffix], $row[city]");

$x=3 ;
$pdf->Rect(.4, $y+.1, 7.6, 0.25);
$y=$y+.2 ;
$pdf->SetFont('Times','B','11');
$pdf->SetXY($x, $y+.02);
$pdf->Cell(0,0,'Required Actions Following Inspection');

$y=$y+.3 ;
$x=.4;
$pdf->SetFont('Times','','9');
$pdf->SetXY($x, $y);
$pdf->Cell(0,0,'This Environmental Lead Inspection Report documents the findings of the Certified Environmental Lead Inspector/Inspector Technician');
$pdf->SetXY($x, $y+.15);
$pdf->Cell(0,0,'for each room/area tested and all media tested (i.e., paint, dust, soil, and/or water). Each inspection page contains a ?Hazard Assessment?');
$pdf->SetXY($x, $y+.3);
$pdf->Cell(0,0,'on the right side which indicates whether the surface or media was Lead-Free, Lead-Safe, or a significant Hazard.');

$x=.75;
$y=$y+.6 ;
$pdf->SetXY($x, $y);
$pdf->SetFont('Times','B','9');
$pdf->Cell(0,0,'Lead-Free');
$pdf->SetFont('Times','','9');
$pdf->SetXY($x+.65, $y);
$pdf->Cell(0,0,'indicates that the surface/media tested contained no lead exposure hazards and no follow up action is required.');

$y=$y+.3 ;
$pdf->SetXY($x, $y);
$pdf->SetFont('Times','B','9');
$pdf->Cell(0,0,'Lead-Safe');
$pdf->SetFont('Times','','9');
$pdf->SetXY($x+.65, $y);
$pdf->Cell(0,0,'indicates that the surface/media tested contained no current lead exposure hazards, but routine maintenance (i.e., ');
$pdf->SetXY($x,$y+.15);
$pdf->Cell(0,0,'keeping painted surfaces intact and soil covered with grass, mulch, etc.) is required to maintain the Lead-Safe condition. ');
$pdf->SetXY($x,$y+.3);
$pdf->Cell(0,0,'An annualre-inspection by a RI Certified Environmental Lead Inspector is required to maintain Lead-Safe status. This annual');
$pdf->SetXY($x,$y+.45);
$pdf->Cell(0,0,'inspection includes dust sampling and a visual inspection of all areas that are not Lead-Free.');

$y=$y+.75 ;
$pdf->SetFont('Times','B','9');
$pdf->SetXY($x, $y);
$pdf->Cell(0,0,'Hazard');
$pdf->SetFont('Times','','9');
$pdf->SetXY($x+.5, $y);
$pdf->Cell(0,0,'indicates that the surface/media tested contained significant lead exposure hazards and requires lead hazard reduction to a');
$pdf->SetXY($x,$y+.15);
$pdf->Cell(0,0,'Lead-Free or Lead-Safe level in accordance with the Rules and Regulations for Lead Poisoning Prevention (?Lead Regulations?).');

//$pdf->SetXY($x,$y+.5);
//MultiCell(float w, float h, string txt [, mixed border [, string align [, int fill]]])

$y=$y+.3 ;
$x=.4;
$pdf->SetFont('Times','B','9');
$pdf->SetXY($x, $y);
$pdf->multiCell(7.5, 0.155,'Consult the inspector or the RI Department of Health prior to performing renovation (unless the area to be renovated is Lead-Free, since disturbing lead-based paint may create significant lead exposures hazards and may be in violation of RI law.');

$y=$y+.45 ;
$x=.4;
$pdf->SetFont('Times','B','9');
$pdf->SetXY($x, $y);
$pdf->Cell(0, 0,'Required Actions if Lead Exposure Hazards are identified:');
$pdf->SetXY($x, $y);
$pdf->Cell(0, 0,'__________________________________________________');

$x=.6;
$y=$y+.2 ;
$pdf->SetFont('Times','','9');
$pdf->SetXY($x, $y);
$pdf->multiCell(7.2,.15,'1. Subsection 11.1(b)(1) of the Lead Regulations exempts lead abatement work from the Lead Regulations when a Certified Environmental Lead Inspector has determined that only ?Spot Removal? is required to correct all lead hazards. See the Certification at the bottom of this page by the Certified Environmental Lead Inspector to determine whether the dwelling unit and/or common areas qualify for Spot Removal. Unless this Certification indicates that the dwelling unit or common areas qualify for Spot Removal, the lead abatement work must be conducted in accordance with the Lead Regulations.');
$y=$y+0.9 ;
$pdf->SetXY($x, $y);
$pdf->multiCell(7.2,.15,'2. A Lead Hazard Reduction Contractor is not required for replacement of windows or doors, provided that any dust or debris is immediately cleaned up upon completion. A Lead Safe Remodeler/Renovator is required for window replacement.');
$y=$y+0.4 ;
$pdf->SetXY($x, $y);
$pdf->multiCell(7.2,.15,'3. A Lead Hazard Reduction Contractor is not required for exterior lead abatement, provided that the work is conducted in accordance with the RI Department of Environmental Management?s Air Pollution Control Regulation #24.');
$y=$y+0.4 ;
$pdf->SetXY($x, $y);
$pdf->multiCell(7.2,.15,'4. Except as noted in 1-3 above, owners who have received a Notice of Violation from the Department of Health along with this Inspection Report must have all interior lead hazards corrected by a Lead Hazard Reduction Contractor, unless the Department of Health has granted a financial hardship exemption.');
$y=$y+0.5 ;
$pdf->SetXY($x, $y);
$pdf->multiCell(7.2,.15,'5. Owners who have not received a Notice of Violation from the Department of Health may either hire a RI Licensed Lead Hazard Reduction Contractor or conduct the work themselves in accordance with the Lead Regulation.');
$x=.4;
$y=$y+0.4 ;
$pdf->SetXY($x, $y);
$pdf->multiCell(7.2,.15, 'Questions regarding abatement options and regulatory requirements can be directed to the Certified Environmental Lead Inspector who conducted the lead inspection or to the RI Department of Health, Environmental Lead Program, at (401) 222-1417.');

$y=$y+0.35 ;
$pdf->SetXY($x, $y);
$pdf->Cell(0, 0,'____________________________________________________________________________________________________________________');

$y=$y+.1 ;
$pdf->SetFont('Times','B','9');
$pdf->SetXY($x, $y);
$pdf->multiCell(7.2,.15,'Spot Removal Exemption:');
$pdf->SetFont('Times','','9');
$pdf->SetXY($x+1.5, $y);
$pdf->multiCell(7.2,.15,'(This section to be completed by a Certified Environmental Lead Inspector only)');

$y=$y+.25;
$pdf->SetFont('Times','','9');
$pdf->SetXY($x, $y);
$pdf->multiCell(7.2,.15,'Subsection 11.1(b)(1) of the Lead Regulations allows a dwelling unit and/or common hallway(s) to qualify for the Spot Removal exemption if the dwelling unit contains less than 15 ft2 of damaged lead-based paint and any common hallway contains less than 3 ft sq of damaged lead-based paint, provided that no room or hallway contains more than 4 damaged components with lead-based paint.');

$y=$y+.55 ;
$pdf->SetFont('Times','B','9');
$pdf->SetXY($x, $y);
$pdf->multiCell(7.2,.15,'Lead Inspector Certification:');
$pdf->SetFont('Times','','9');
$pdf->SetXY($x+1.6, $y);
$pdf->multiCell(7.2,.15,'In accordance with Subsection 11.1(b)(1) of the Lead Regulations, I certify that:');

$y=$y+.25 ;
$pdf->SetFont('Times','B','9');
$pdf->SetXY($x+.2, $y);
$pdf->multiCell(7.2,.15,'Dwelling unit: (Check one only)');  
$y=$y+.25 ;
$pdf->SetFont('Times','','9');
$pdf->SetXY($x+.4, $y);
$pdf->Cell(0,0,'[   ] The');
$pdf->SetFont('Times','B','9');
$pdf->SetXY($x+.85, $y);
$pdf->Cell(0,0,'dwelling unit DOES');
$pdf->SetFont('Times','','9');
$pdf->SetXY($x+1.95, $y);
$pdf->Cell(0,0,'qualify for the Spot Removal exemption in the Lead Regulations.');
$y=$y+.155;
$pdf->SetFont('Times','','9');
$pdf->SetXY($x+.4, $y);
$pdf->Cell(0,0,'[   ] The');
$pdf->SetFont('Times','B','9');
$pdf->SetXY($x+.85, $y);
$pdf->Cell(0,0,'dwelling unit DOES NOT ');
$pdf->SetFont('Times','','9');
$pdf->SetXY($x+2.25, $y);
$pdf->Cell(0,0,'qualify for the Spot Removal exemption in the Lead Regulations.');


$y=$y+.25 ;
$pdf->SetFont('Times','B','9');
$pdf->SetXY($x+.2, $y);
$pdf->multiCell(7.2,.15,'Front Hallway: (Check one only), No Front Hallway ');
$pdf->SetXY($x+3.1, $y+.05);
$pdf->Cell(0,0,'[   ] ');
$pdf->SetFont('Times','B','9');
$y=$y+.25 ;
$pdf->SetFont('Times','','9');
$pdf->SetXY($x+.4, $y);
$pdf->Cell(0,0,'[   ] The');
$pdf->SetFont('Times','B','9');
$pdf->SetXY($x+.85, $y);
$pdf->Cell(0,0,'front hallway DOES');
$pdf->SetFont('Times','','9');
$pdf->SetXY($x+2.00, $y);
$pdf->Cell(0,0,'qualify for the Spot Removal exemption in the Lead Regulations.');
$y=$y+.155;
$pdf->SetFont('Times','','9');
$pdf->SetXY($x+.4, $y);
$pdf->Cell(0,0,'[   ] The');
$pdf->SetFont('Times','B','9');
$pdf->SetXY($x+.85, $y);
$pdf->Cell(0,0,'front hallway DOES NOT');
$pdf->SetFont('Times','','9');
$pdf->SetXY($x+2.28, $y);
$pdf->Cell(0,0,'qualify for the Spot Removal exemption in the Lead Regulations.');


$y=$y+.25 ;
$pdf->SetFont('Times','B','9');
$pdf->SetXY($x+.2, $y);
$pdf->multiCell(7.2,.15,'Rear Hallway: (Check one only), No Rear Hallway ');
$pdf->SetXY($x+3, $y+.05);
$pdf->Cell(0,0,'[   ] ');
$y=$y+.25 ;
$pdf->SetFont('Times','','9');
$pdf->SetXY($x+.4, $y);
$pdf->Cell(0,0,'[   ] The');
$pdf->SetFont('Times','B','9');
$pdf->SetXY($x+.85, $y);
$pdf->Cell(0,0,'rear hallway DOES');
$pdf->SetFont('Times','','9');
$pdf->SetXY($x+1.95, $y);
$pdf->Cell(0,0,'qualify for the Spot Removal exemption in the Lead Regulations.');
$y=$y+.155;
$pdf->SetFont('Times','','9');
$pdf->SetXY($x+.4, $y);
$pdf->Cell(0,0,'[   ] The');
$pdf->SetFont('Times','B','9');
$pdf->SetXY($x+.85, $y);
$pdf->Cell(0,0,'rear hallway DOES NOT ');
$pdf->SetFont('Times','','9');
$pdf->SetXY($x+2.25, $y);
$pdf->Cell(0,0,'qualify for the Spot Removal exemption in the Lead Regulations.');


$x=0.4;
$y=$y+.3;
$query = "SELECT firstname, lastname, elt, eli FROM inspector INNER JOIN insp_assigned USING (tid) WHERE iid=$iid AND job='Lead'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if ($row = mysql_fetch_assoc($result) ) {
	formfix($row);
	$pdf->SetFont('Times','','12');
	$pdf->SetXY(4.45, 9.65);
	$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
	$pdf->SetXY(5.9, 10.0);
	$pdf->Cell(0,0, "$row[eli]");
}
$pdf->SetFont('Times','','9');
$pdf->SetXY($x+.2,$y+.1);
$pdf->Cell(0,0,'___________________________________________________                     ___________________________________________________');

$query = "SELECT DATE_FORMAT(starttime, '%c/%e/%Y') AS startdate, DATE_FORMAT(starttime, '%l:%i %p') AS starttime FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_row($result);
$pdf->SetXY($x+1, $y+0.45);
$pdf->Cell(0,0, $row[0]);

$pdf->SetXY($x+.5,$y+.45);
$pdf->Cell(0,0,'Date: __________________________                                                       RI Certification No. ELI - ___________');
$y=$y+.25;
$pdf->SetXY($x+1.5,$y);
$pdf->Cell(0,0,'(Signature)                                                                               (Type or Print Name of Person Conducting Inspection)');
$y=$y+.4;
$pdf->SetXY($x,$y);
$pdf->Cell(0,0,'_______________________________________________________________________________________________________________________');
$y=$y+.15;
$pdf->SetFont('Times','B','9');
$pdf->SetXY($x,$y);
$pdf->Cell(0,0,'FORM PBLC-23-2 (6/04)');
$pdf->SetXY($x+1.5,$y);
$pdf->SetFont('Times','I','8');
$pdf->Cell(0,0,'Replaces FORM PBLC-23-2 (3/98), which is obsolete.');






?>