<?php

require_once'session.php';
require_once'connect.php';

//print_r($_POST);
//exit();

$unitnumber = 0; //now important!

if ($_POST['reason'] != 'Other') {
	unset($_POST['otherreason']);
}

if ($_POST['year'] < 100) {
	$_POST['year'] += 2000;
}

$_POST['scheddate'] = "$_POST[year]-$_POST[month]-$_POST[day]";

if ($_POST['startampm'] == 'PM') {
	$_POST['starthour'] += 12;
}
$_POST['schedstart'] = "$_POST[starthour]:$_POST[startmin]:00";

if ($_POST['endampm'] == 'PM') {
	$_POST['endhour'] += 12;
}
$_POST['schedend'] = "$_POST[endhour]:$_POST[endmin]:00";

unset($_POST['year']);
unset($_POST['month']);
unset($_POST['day']);
unset($_POST['starthour']);
unset($_POST['startmin']);
unset($_POST['startampm']);
unset($_POST['endhour']);
unset($_POST['endmin']);
unset($_POST['endampm']);

$numunits = $_POST['numunits'];
unset($_POST['numunits']);

//shortcut - all fields are text
foreach ($_POST as $field => $value) {
	$fieldlist[] = $field;
	$valuelist[] = htmlspecialchars($value);
}

/*
Because htmlspecialchars() is being performed on the data, it is possible that some data may be truncated. This can happen if, for example, the data contains '&', which becomes '&amp;', and the resulting string is greater than the allowed column width. However, the column widths are rather generous, and it only affects a few characters, so as such, it was deemed not worth checking the various lengths of each field to see if they would actually be truncated. It should be noted that if no substitutions are performed, the 'maxlength' attribute on the form elements will prevent an entry from being truncated.
*/

$query = "INSERT INTO inspection (created, ".implode(',', $fieldlist).") VALUES (NOW(), '".implode("','", $valuelist)."')";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$query = "SELECT LAST_INSERT_ID()";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$iid = mysql_result($result, 0);

if ($_POST['type'] == 'Comprehensive' || $_POST['type'] == 'Conformance') {
	$query = "INSERT INTO units (iid, number, unitdesc, unittype, insptype) VALUES ($iid, 	$unitnumber, 'Exterior', 'Exterior', '$_POST[type]')";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$unitnumber++;

	for($x=1; $x<=$numunits; $x++) {
		$unitdesc = "Unit $x";
		$query = "INSERT INTO units (iid, number, unitdesc, insptype) VALUES ($iid, $unitnumber, '$unitdesc', '$_POST[type]')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		$unitnumber++;
	}
}

if ($_POST['type'] == 'Comprehensive - Clearance') {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-compclearance-copy-pickinspection.php?iid=$iid&bid=".$_POST['bid']." ");
	exit();
}

if ($_POST['type'] == 'Conformance - Clearance') {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-confclearance-copy-pickinspection.php?iid=$iid&bid=".$_POST['bid']." ");
	exit();
}

//added 8/29/2006
if ($_POST['type'] == 'Comprehensive - Copy') {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-compclearance-copy-pickinspection.php?iid=$iid");
	exit();
}

if ($_POST['type'] == 'Conformance - Copy') {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-confclearance-copy-pickinspection.php?iid=$iid");
	exit();
}

header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-view.php?iid=$iid");

?>