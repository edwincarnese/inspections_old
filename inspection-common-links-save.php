<?php
/*
 * Created on Jan 24, 2006
 *
 * Author: Jason Rankin
 */
?>
<?php
require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

if ($cuid == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}

$query = "SELECT iid FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$iid = mysql_result($result, 0);

$dest = $_POST['dest'] or $dest = $_GET['dest'] or $dest = 'inspection-view.php?iid='.$iid;

if ($_POST['submit'] == 'Save') {
	$query = "DELETE FROM unit_links WHERE unit=$cuid";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	
	if ($_POST['commonids']) {
		foreach ($_POST['commonids'] as $common) {
			$query = "INSERT INTO unit_links (common, unit) VALUES ($common, $cuid)";
			mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		}
	} else {
		$query = "DELETE FROM unit_links WHERE unit=$cuid";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
}

//ENDTIME
$query = "UPDATE units SET endtime=NOW() WHERE cuid=$cuid";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/$dest");
?>