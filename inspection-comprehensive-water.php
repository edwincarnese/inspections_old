<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

$query = "SELECT * FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}
$comp = mysql_fetch_assoc($result);

$iid = $comp['iid'];
$unitnumber = $comp['number'];

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - $comp[unitdesc] - Water";
require_once'header.php';
?>
<hr />
<p><a href="inspection-comprehensive-water-sample.php?cuid=<?php print $cuid; ?>">Add Water Sample</a></p>
<?php
$query = "SELECT * FROM waters WHERE cuid=$comp[cuid]";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result)) {
?>
<table class="info">
<tr><th>Lab Number</th><th>Fixture</th><th>Type</th><th>Brass Fixture</th><th>New Plumbing</th><th>Edit</th></tr>
<?php
	while($row = mysql_fetch_assoc($result) ) {
		print "<tr><td>$row[iid]-$unitnumber"."W$row[number]</td><td>$row[fixture]</td><td>$row[drawflush]</td><td>$row[brass]</td><td>$row[newplumbing]</td><td><a href=\"inspection-comprehensive-water-sample.php?waterid=$row[iid]W$row[number]\">Edit</a></tr>\n";
	}
?>
</table>
<?php
}

print '<hr />';

if (!$comp['bottle']) { //water page has not yet been visited; get basic info
	print '<p><b>Please have the questions in the link below answered before taking water samples.</b></p>';
}
?>
<p><a href="inspection-comprehensive-water-questions.php?cuid=<?php print $cuid; ?>">Water questions / comments</a></p>

<p><a href="inspection-comprehensive-unit.php?cuid=<?php print $cuid; ?>"><?php print $comp['unitdesc']; ?> Main Menu</a></p>

<p><a href="inspection-main.php?iid=<?php print $comp[iid]; ?>">Inspection Main Menu</a></p>

<?php
require_once'footer.php';
?>