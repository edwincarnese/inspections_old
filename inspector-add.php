<?php 
$title = "Add Inspector";

require_once'session.php';
require_once'header.php';
?>

<form action="inspector-save.php" method="post">
<table>
<tr><td>First name:</td><td><input type="text" name="firstname" maxlength="30" /></td></tr>
<tr><td>Last name:</td><td><input type="text" name="lastname" maxlength="30" /></td></tr>
<tr><td>Address:</td><td><input type="text" name="streetnum" maxlength="8" size="5" /><input type="text" name="address" maxlength="30" /><input type="text" name="suffix" maxlength="10" size="7" /></td></tr>
<tr><td>Address2:</td><td><input type="text" name="address2" maxlength="30" /></td></tr>
<tr><td>City:</td><td><input type="text" name="city" maxlength="30" /></td></tr>
<tr><td>State:</td><td><input type="text" name="state" maxlength="2" size="3" /></td></tr>
<tr><td>Zip:</td><td><input type="text" name="zip" maxlength="5" size="6" /></td></tr>
<tr><td>Phone (1):</td><td><input type="text" name="phones[1][1]" maxlength="3" size="4" />-<input type="text" name="phones[1][2]" maxlength="3" size="4" />-<input type="text" name="phones[1][3]" maxlength="4" size="5" /> Ext: <input type="text" name="phones[1][ext]" maxlength="5" size="6" /> Type: <select name="phones[1][type]">
<option value="Work">Work</option>
<option value="Home">Home</option>
<option value="Cell">Cell</option>
<option value="Fax">Fax</option>
</select></td></tr>
<tr><td>Phone (2):</td><td><input type="text" name="phones[2][1]" maxlength="3" size="4" />-<input type="text" name="phones[2][2]" maxlength="3" size="4" />-<input type="text" name="phones[2][3]" maxlength="4" size="5" /> Ext: <input type="text" name="phones[2][ext]" maxlength="5" size="6" /> Type: <select name="phones[2][type]">
<option value="Work">Work</option>
<option value="Home">Home</option>
<option value="Cell">Cell</option>
<option value="Fax">Fax</option>
</select></td></tr>
<tr><td>Phone (3):</td><td><input type="text" name="phones[3][1]" maxlength="3" size="4" />-<input type="text" name="phones[3][2]" maxlength="3" size="4" />-<input type="text" name="phones[3][3]" maxlength="4" size="5" /> Ext: <input type="text" name="phones[3][ext]" maxlength="5" size="6" /> Type: <select name="phones[3][type]">
<option value="Work">Work</option>
<option value="Home">Home</option>
<option value="Cell">Cell</option>
<option value="Fax">Fax</option>
</select></td></tr>
<tr><td>E-mail:</td><td><input type="text" name="email" maxlength="50" /></td></tr>
<tr><td>Start Date:</td><td><input type="text" name="startmonth" maxlength="2" size="3" /> / <input type="text" name="startday" maxlength="2" size="3" /> / <input type="text" name="startyear" maxlength="4" size="5" /></td></tr>
<tr><td>End Date:</td><td><input type="text" name="endmonth" maxlength="2" size="3" /> / <input type="text" name="endday" maxlength="2" size="3" /> / <input type="text" name="endyear" maxlength="4" size="5" /></td></tr>
<tr><td>ELT License Number:&nbsp;&nbsp;</td><td>ELT-<input type="text" name="elt" maxlength="10" size="6" /></td></tr>
<tr><td>Acquired:</td><td><input type="text" name="eltsmonth" maxlength="2" size="3" /> / <input type="text" name="eltsday" maxlength="2" size="3" /> / <input type="text" name="eltsyear" maxlength="4" size="5" /></td></tr>
<tr><td>Expires:</td><td><input type="text" name="eltmonth" maxlength="2" size="3" /> / <input type="text" name="eltday" maxlength="2" size="3" /> / <input type="text" name="eltyear" maxlength="4" size="5" /></td></tr>
<tr><td>ELI License Number:</td><td>ELI-<input type="text" name="eli" maxlength="10" size="6" /></td></tr>
<tr><td>Acquired:</td><td><input type="text" name="elismonth" maxlength="2" size="3" /> / <input type="text" name="elisday" maxlength="2" size="3" /> / <input type="text" name="elisyear" maxlength="4" size="5" /></td></tr>
<tr><td>Expires:</td><td><input type="text" name="elimonth" maxlength="2" size="3" /> / <input type="text" name="eliday" maxlength="2" size="3" /> / <input type="text" name="eliyear" maxlength="4" size="5" /></td></tr>
<tr><td>Comments:</td><td><textarea cols="40" rows="5" name="comments"></textarea></td></tr>
</table>
<p><input type="submit" value="Save" /> <input type="reset" value="Reset" /></p>
</form>
<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>