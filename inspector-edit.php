<?php 
require_once'session.php';
require_once'connect.php';

$tid = $_POST['tid'] or $tid = $_GET['tid'];

$tid += 0; // (force numbers)

$query = "SELECT *, YEAR(eltdate) AS eltyear, DAY(eltdate) AS eltday, MONTH(eltdate) AS eltmonth, YEAR(elidate) AS eliyear, DAY(elidate) AS eliday, MONTH(elidate) AS elimonth, YEAR(eltstart) AS eltsyear, DAY(eltstart) AS eltsday, MONTH(eltstart) AS eltsmonth, YEAR(elistart) AS elisyear, DAY(elistart) AS elisday, MONTH(elistart) AS elismonth, YEAR(startdate) AS startyear, DAY(startdate) AS startday, MONTH(startdate) AS startmonth, YEAR(enddate) AS endyear, DAY(enddate) AS endday, MONTH(enddate) AS endmonth, lead_capable FROM inspector WHERE tid=$tid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspector-list.php");
	exit();
}

$row = mysql_fetch_assoc($result);

$title = "Edit inspector";
require_once'header.php';
?>

<form action="inspector-update.php" method="post">
<input type="hidden" name="tid" value="<?php print $tid;?>" />
<table>
<tr><td>First name:</td><td><input type="text" name="firstname" value="<?php print $row['firstname']; ?>" maxlength="30" /></td></tr>
<tr><td>Last name:</td><td><input type="text" name="lastname" value="<?php print $row['lastname']; ?>" maxlength="30" /></td></tr>
<tr><td>Address:</td><td><input type="text" name="streetnum" value="<?php print $row['streetnum']; ?>" maxlength="8" size="5" /><input type="text" name="address" value="<?php print $row['address']; ?>" maxlength="30" /><input type="text" name="suffix" value="<?php print $row['suffix']; ?>" maxlength="10" size="7" /></td></tr>
<tr><td>Address2:</td><td><input type="text" name="address2" value="<?php print $row['address2']; ?>" maxlength="30" /></td></tr>
<tr><td>City:</td><td><input type="text" name="city" value="<?php print $row['city']; ?>" maxlength="30" /></td></tr>
<tr><td>State:</td><td><input type="text" name="state" value="<?php print $row['state']; ?>" maxlength="2" size="3" /></td></tr>
<tr><td>Zip:</td><td><input type="text" name="zip" value="<?php print $row['zip']; ?>" maxlength="5" size="6" /></td></tr>
<?php

$query = "SELECT * FROM inspector_phone WHERE tid=$tid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$phonecount = 0;
while ($phonerow = mysql_fetch_assoc($result) ) {
	$phonecount++;
	$phonerow[1] = substr($phonerow['number'], 0, 3);
	$phonerow[2] = substr($phonerow['number'], 4, 3);
	$phonerow[3] = substr($phonerow['number'], 8);

?>
<tr><td>Phone (<?php print $phonecount; ?>):</td><td><input type="hidden"  name="phones[<?php print $phonerow['tpid']; ?>][tpid]" value="<?php print $phonerow['tpid']; ?>" /><input type="text" name="phones[<?php print $phonerow['tpid']; ?>][1]" value="<?php print $phonerow[1]; ?>" maxlength="3" size="4" />-<input type="text" name="phones[<?php print $phonerow['tpid']; ?>][2]" value="<?php print $phonerow[2]; ?>" maxlength="3" size="4" />-<input type="text" name="phones[<?php print $phonerow['tpid']; ?>][3]" value="<?php print $phonerow[3]; ?>" maxlength="4" size="5" /> Ext: <input type="text" name="phones[<?php print $phonerow['tpid']; ?>][ext]" value="<?php print $phonerow[ext]; ?>" maxlength="5" size="6" /> Type: <select name="phones[<?php print $phonerow['tpid']; ?>][type]" value="<?php print $phonerow[type]; ?>">
<?php $opts = array('Work','Home','Cell','Fax');
foreach ($opts as $opt) {
	if ($phonerow['type'] == $opt) {
		print "<option value=\"$opt\" selected=\"selected\">$opt</option>\n";
	} else {
		print "<option value=\"$opt\">$opt</option>\n";
	}
}
?>
</select></td></tr>
<?php
}
?>
<tr><td>E-mail:</td><td><input type="text" name="email" value="<?php print $row['email']; ?>" maxlength="50" /></td></tr>
<tr><td>Start Date:</td><td><input type="text" name="startmonth" value="<?php print $row['startmonth']; ?>" maxlength="2" size="3" /> / <input type="text" name="startday" value="<?php print $row['startday']; ?>" maxlength="2" size="3" /> / <input type="text" name="startyear" value="<?php print $row['startyear']; ?>" maxlength="4" size="5" /></td></tr>
<tr><td>End Date:</td><td><input type="text" name="endmonth" value="<?php print $row['endmonth']; ?>" maxlength="2" size="3" /> / <input type="text" name="endday" value="<?php print $row['endday']; ?>" maxlength="2" size="3" /> / <input type="text" name="endyear" value="<?php print $row['endyear']; ?>" maxlength="4" size="5" /></td></tr>
<tr><td>ELT License Number:&nbsp;&nbsp;</td><td>ELT-<input type="text" name="elt" value="<?php print $row['elt']; ?>" maxlength="10" size="6" /></td></tr>
<tr><td>License Obtained:</td><td><input type="text" name="eltsmonth" value="<?php print $row['eltsmonth']; ?>" maxlength="2" size="3" /> / <input type="text" name="eltsday" value="<?php print $row['eltsday']; ?>" maxlength="2" size="3" /> / <input type="text" name="eltsyear" value="<?php print $row['eltsyear']; ?>" maxlength="4" size="5" /></td></tr>
<tr><td>License Expires:</td><td><input type="text" name="eltmonth" value="<?php print $row['eltmonth']; ?>" maxlength="2" size="3" /> / <input type="text" name="eltday" value="<?php print $row['eltday']; ?>" maxlength="2" size="3" /> / <input type="text" name="eltyear" value="<?php print $row['eltyear']; ?>" maxlength="4" size="5" /></td></tr>
<tr><td>ELI License Number:</td><td>ELI-<input type="text" name="eli" value="<?php print $row['eli']; ?>" maxlength="10" size="6" /></td></tr>
<tr><td>License Obtained:</td><td><input type="text" name="elismonth" value="<?php print $row['elismonth']; ?>" maxlength="2" size="3" /> / <input type="text" name="elisday" value="<?php print $row['elisday']; ?>" maxlength="2" size="3" /> / <input type="text" name="elisyear" value="<?php print $row['elisyear']; ?>" maxlength="4" size="5" /></td></tr>
<tr><td>License Expires:</td><td><input type="text" name="elimonth" value="<?php print $row['elimonth']; ?>" maxlength="2" size="3" /> / <input type="text" name="eliday" value="<?php print $row['eliday']; ?>" maxlength="2" size="3" /> / <input type="text" name="eliyear" value="<?php print $row['eliyear']; ?>" maxlength="4" size="5" /></td></tr>
<? 
$query = "SELECT userlevel FROM users WHERE uid=$_SESSION[sid]";
$result = mysql_query($query) or sql_crapout($query."<br>".mysql_error());
$userlevel = mysql_result($result, 0);
if ($userlevel >= 5) {
	print "<tr><td>Lead conf inspections:</td><td><input type=\"checkbox\" name=\"elt_lead\" value=\"yes\"";
	if ($row['elt_lead'] == 'yes') {
		print "checked='checked'";
	}
	print "/></td></tr>";
}
?>
<tr><td valign="top">Comments:</td><td><textarea cols="40" rows="5" name="comments"><?php print $row['comments']; ?></textarea></td></tr>
<? 
	if ($userlevel >= 5) {
	print "<tr><td>Lead Capable:</td><td><input type=\"checkbox\" name=\"lead_capable\" value=\"yes\"";
	if ($row['lead_capable'] == 'yes') {
		print "checked='checked'";
	}
	print "/></td></tr>";
}
	?></td></tr>
</table>
<p><input type="submit" value="Save" /> <input type="reset" value="Reset" /></p>
</form>

<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>