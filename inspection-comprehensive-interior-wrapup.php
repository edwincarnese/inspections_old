<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

$query = "SELECT iid, starttime, unitdesc, number, diagrams FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}

list($iid, $starttime, $unitdesc, $unitnumber, $diagrams) = mysql_fetch_row($result);

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$query = "SELECT * FROM comprehensive_rooms WHERE cuid=$cuid ORDER BY number";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	//switch to setup page
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-comprehensive-interior-setup1.php?cuid=$cuid");
	exit();
}

$title = "$iid-$unitnumber - $address - $unitdesc";
require_once'header.php';
?>
<table>
<tr><td style="vertical-align: top">Typical Schedule</td><td><ul>
<li>Labs shipped 2nd working day</li>
<li>Lab faxes results 1 week after receiving them; We will call if anything doesn't pass</li>
<li>Report will take 1 week to compile and ship.</li>
<li>Expect report within 3 weeks</li>
</ul>
</td>
</tr>
<tr>
<td style="vertical-align: top">You need to:</td>
<td><ul>
<li>Notify Tenants</li>
<li>Correct Problems</li>
<li>Do they need a contractor</li>
<li>Do they need information about doing the repairs, handouts</li>
</ul>
</td>
</tr>
</table>
<table>
<tr><td>Ship Samples</td><td class="box"> </td><td>Prior Balance</td><td class="box"> </td><td>Method of Payment</td><td class="box"> </td></tr>
<tr><td>Lab Results Due</td><td class="box"> </td><td>Today's Chg</td><td class="box"> </td><td>Card or Check#</td><td class="box"> </td></tr>
<tr><td>Expediting</td><td class="box"> </td><td>Exp Chg</td><td class="box"> </td><td colspan="2"> </td></tr>
<tr><td>Owner Smpl</td><td class="box"> </td><td>Sample</td><td class="box"> </td><td colspan="2"> </td></tr>
<tr><td>Report is due by</td><td class="box"> </td><td>Total Due</td><td class="box"> </td><td colspan="2"> </td></tr>
</table>
<?php
require_once'footer.php';
?>