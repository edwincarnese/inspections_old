<?

function copytechs($iid) {	//can only be changed in field with override
	global $locallink, $remotelink;
	$query = "DELETE FROM insp_assigned WHERE iid=$iid";
	mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	$query = "SELECT * FROM insp_assigned WHERE iid=$iid";
	$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
	while ($row = mysql_fetch_assoc($result)) {
		$old_assig_id = $row['assig_id'];
		unset($row['assig_id']);
		$query = "INSERT INTO insp_assigned (iid, tid, job) VALUES ('".implode("','", $row)."')";
		mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));

		$query = "SELECT LAST_INSERT_ID()";
		$result = mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
		$new_assig_id = mysql_result($result, 0);
	
		$query2 = "SELECT * FROM unit_insp_assigned WHERE assig_id=$old_assig_id";
		$result2 = mysql_query($query2, $remotelink) or sql_crapout($query2.'<br />'.mysql_error($remotelink));
		
		while ($row2 = mysql_fetch_assoc($result2)) {
			set_time_limit(60);
			unset($row2['unit_assigned_id']);
			$query = "INSERT INTO unit_insp_assigned (assig_id, cuid, job) VALUES ('".implode("','", $row2)."')";
			mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
			
		}
		$query = "UPDATE unit_insp_assigned SET assig_id=$new_assig_id WHERE assig_id=$old_assig_id";
		mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
			
	}
}
	
function copycalibration($iid) { //let's just hope different units/comps use diff secs
	global $locallink, $remotelink;
	$query = "SELECT * FROM comprehensive_calibration WHERE iid=$iid";
	$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
	while ($row = mysql_fetch_row($result)) {
		array_walk($row, 'slashadd'); //add slashes
		$query = "INSERT IGNORE INTO comprehensive_calibration VALUES ('".implode("','", $row)."')"; //just ignoring duplicate rows; nothing's lost cross-unit
		mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	}
}	

function copyunits($iid) {
	global $locallink, $remotelink, $numdiscreps, $discreps;
	
	$linkupdates = array();
	$lastsyncquery = "SELECT  * from lastsync;";
	$lastsyncresult = mysql_query($lastsyncquery, $remotelink ) or sql_crapout($lastsyncquery.'<br />'.mysql_error($remotelink));
	$lastsync = mysql_result($lastsyncresult, 0);
	$query = "SELECT * FROM units WHERE iid=$iid";
	$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
	while ($row = mysql_fetch_assoc($result)) { //for each row
		$update = 0; //no update unless deemed needed
		array_walk($row, 'slashadd'); //add slashes
		$cuid = array_shift($row); //get rid of cuid off array for implodes
		$query = "SELECT * FROM units WHERE iid=$iid AND number=$row[number]";
		$result2 = mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));

		if (mysql_num_rows($result2) > 1) { //script was bugged; duplicate rows
			while ($row2 = mysql_fetch_assoc($result2)) {
				deleteunit($row2['cuid']);
			}
			$update = 1;
		} else  if (mysql_num_rows($result2) == 1) { //exactly one unit
			$row2 = mysql_fetch_assoc($result2);
			if ($row['endtime'] > $lastsync) { //update only if client data is newer than the last sync
				if ($row2['endtime'] > $lastsync) {	// this is the case where the server and the client have both
													// been updated since the last sync
				$update = 2;
				$numdiscreps++;
				} else {
				deleteunit($row2['cuid']);
				$update = 1;
				}
			}
		} else if (mysql_num_rows($result2) < 1) { //unit added in field
			$update = 1;
		}

		if ($update == 1) {
			//take out endtime; if copy fails, unit won't be considered copied by subsequent attempts
			$endtime = $row['endtime'];
			unset ($row['endtime']);
			$query = "INSERT INTO units (".implode(',', array_keys($row)).") VALUES ('".implode("','", $row)."')";
			mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
			$query = "SELECT LAST_INSERT_ID()"; //get new cuid
			$resultid = mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
			$newid = mysql_result($resultid, 0);
			//unit itself copied; just go down the list
			//unit links done below, after all units completed
			copy_waters($cuid, $newid);
			copy_comprehensive_comments($cuid, $newid);
			copy_comprehensive_ext($cuid, $newid);
			copy_comprehensive_ext_components($cuid, $newid);
			copy_comprehensive_sides($cuid, $newid);
			copy_comprehensive_soil_debris($cuid, $newid);
			copy_comprehensive_soil_object($cuid, $newid);
			copy_comprehensive_rooms($cuid, $newid);
			copy_conformance_comments($cuid, $newid);
			copy_conformance_sides($cuid, $newid);
			copy_conformance_rooms($cuid, $newid);
			copy_unit_comments_assigned($cuid, $newid);
			//copy_inspection_team($iid); //rewrite when techs per unit implemented
			//unit copied (except links); copy endtime
			$query = "UPDATE units SET endtime='$endtime' WHERE cuid=$newid";
			mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
			$unitmap[$cuid] = $newid;
			$linkupdates[] = $cuid;
		} else if ($update == 0){
			$unitmap[$cuid] = $cuid; //if no update, same cuid
		} else if ($update == 2) {
			/*
			 *what to do when we have a discrepancy
			 */
			$discreps[$numdiscreps-1] = array($cuid, $row2['cuid']);
		}
	}
	//unit links; can't be a function; need to know whole unit map for non-commons
	foreach ($linkupdates as $common) {
		//deleting is done while updating the unit
		$newcommon = $unitmap[$common];
		$query = "SELECT unit FROM unit_links WHERE common=$common";
		$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
		while ($row = mysql_fetch_assoc($result)) {
			$newunit = $unitmap[$row['unit']];
			$query = "INSERT IGNORE INTO unit_links (common, unit) VALUES ($newcommon, $newunit)";
			mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
		}
		//flip half, unit could be non-common
		//deleting is done while updating the unit
		$newunit = $unitmap[$common];
		$query = "SELECT common FROM unit_links WHERE unit=$common";
		$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
		while ($row = mysql_fetch_assoc($result)) {
			$newcommon = $unitmap[$row['common']];
			$query = "INSERT IGNORE INTO unit_links (common, unit) VALUES ($newcommon, $newunit)"; //ignore errors
			mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
		}
	}
	
	//field blanks
	$query = "SELECT * FROM comprehensive_wipes WHERE iid=$iid AND number = 0 UNION SELECT * FROM conformance_wipes WHERE iid=$iid AND number = 0";
	$result = mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	if (mysql_num_rows($result) == 0) {
		$query = "SELECT *, 'comprehensive' AS place FROM comprehensive_wipes WHERE iid=$iid AND number = 0 UNION SELECT *, 'conformance' AS place FROM  conformance_wipes WHERE iid=$iid AND number = 0";
		$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
		if ($row = mysql_fetch_assoc($result)) {
			array_walk($row, 'slashadd'); //add slashes
			$place = $row['place'];
			unset($row['place']);
			if ($place == 'comprehensive') {
				$query = "INSERT INTO comprehensive_wipes (".implode(',', array_keys($row)).") VALUES ('".implode("','", $row)."')";
			} else if ($place == 'conformance') {
				$query = "INSERT INTO conformance_wipes (".implode(',', array_keys($row)).") VALUES ('".implode("','", $row)."')";
			} else { //this should never actually happen
				print "<p>The programmer screwed up. Please call him and complain.</p>";
			}
			mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
		}
	}
}

function deleteunit($cuid) {
	global $locallink, $remotelink;
	$query = "DELETE FROM unit_links WHERE common=$cuid OR unit=$cuid";
	mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	$query = "DELETE FROM waters WHERE cuid=$cuid";
	mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	$query = "DELETE FROM comprehensive_comments WHERE cuid=$cuid";
	mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	$query = "DELETE FROM comprehensive_ext WHERE cuid=$cuid";
	mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	
	//let's see if this works....
	$query = "DELETE FROM paintchips WHERE ccid IN (SELECT ccid FROM comprehensive_ext_components WHERE cuid=$cuid)";
	mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	$query = "DELETE FROM comprehensive_ext_components WHERE cuid=$cuid";
	mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	
	$query = "DELETE FROM comprehensive_sides WHERE cuid=$cuid";
	mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	$query = "DELETE FROM comprehensive_soil_debris WHERE cuid=$cuid";
	mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	$query = "DELETE FROM comprehensive_soil_object WHERE cuid=$cuid";
	mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	
	//even more fun...
	$query = "DELETE FROM paintchips WHERE ccid IN (SELECT ccid FROM comprehensive_components INNER JOIN comprehensive_rooms USING (crid) WHERE cuid=$cuid)";
	mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	$query = "DELETE FROM comprehensive_components WHERE crid IN (SELECT crid FROM comprehensive_rooms WHERE cuid=$cuid)";
	mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	$query = "DELETE FROM comprehensive_wipes WHERE crid IN (SELECT crid FROM comprehensive_rooms WHERE cuid=$cuid)";
	mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	$query = "DELETE FROM comprehensive_rooms WHERE cuid=$cuid";
	mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	
	$query = "DELETE FROM conformance_sides WHERE cuid=$cuid";
	mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	$query = "DELETE FROM conformance_wipes WHERE crid IN (SELECT crid FROM conformance_rooms WHERE cuid=$cuid)";
	mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	$query = "DELETE FROM conformance_comments WHERE cuid=$cuid";
	mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	$query = "DELETE FROM conformance_rooms WHERE cuid=$cuid";
	mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	
	$query = "DELETE FROM units WHERE cuid=$cuid";
	mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
}

function copy_waters($oldcuid, $newcuid) {
	global $locallink, $remotelink;
	$query = "SELECT * FROM waters WHERE cuid=$oldcuid";
	$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		$query = "INSERT INTO waters (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	}
}

function copy_comprehensive_comments($oldcuid, $newcuid) {
	global $locallink, $remotelink;
	$query = "SELECT * FROM comprehensive_comments WHERE cuid=$oldcuid";
	$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		$query = "INSERT INTO comprehensive_comments (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	}
}

function copy_comprehensive_ext($oldcuid, $newcuid) {
	global $locallink, $remotelink;
	$query = "SELECT * FROM comprehensive_ext WHERE cuid=$oldcuid";
	$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		$query = "INSERT INTO comprehensive_ext (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	}
}

function copy_comprehensive_ext_components($oldcuid, $newcuid) {
	global $locallink, $remotelink;
	$query = "SELECT * FROM comprehensive_ext_components WHERE cuid=$oldcuid";
	$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		$oldccid = $row['ccid'];
		unset($row['cuid']); //get rid of cuid off array for implodes
		unset($row['ccid']); //get rid of ccid off array for implodes
		$query = "INSERT INTO comprehensive_ext_components (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
		// PAINT CHIPS! Woo hoo!
		$query = "SELECT LAST_INSERT_ID()";
		$resultid = mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
		$newccid = mysql_result($resultid, 0);
		copy_paintchips($oldccid, $newccid, 'Exterior');
	}
}

function copy_paintchips ($oldccid, $newccid, $type) {
	global $locallink, $remotelink;
	$query = "SELECT * FROM paintchips WHERE ccid=$oldccid AND comptype='$type'";
	$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['ccid']); //get rid of cuid off array for implodes
		$query = "INSERT INTO paintchips (ccid, ".implode(',', array_keys($row)).") VALUES ('$newccid','".implode("','", $row)."')";
		mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	}
}

function copy_comprehensive_sides($oldcuid, $newcuid) {
	global $locallink, $remotelink;
	$query = "SELECT * FROM comprehensive_sides WHERE cuid=$oldcuid";
	$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		unset($row['csid']); //auto-increment
		$query = "INSERT INTO comprehensive_sides (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	}
}

function copy_comprehensive_soil_debris($oldcuid, $newcuid) {
	global $locallink, $remotelink;
	$query = "SELECT * FROM comprehensive_soil_debris WHERE cuid=$oldcuid";
	$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		unset($row['csid']); //auto-increment
		$query = "INSERT INTO comprehensive_soil_debris (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	}
}

function copy_comprehensive_soil_object($oldcuid, $newcuid) {
	global $locallink, $remotelink;
	$query = "SELECT * FROM comprehensive_soil_object WHERE cuid=$oldcuid";
	$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		unset($row['csid']); //auto-increment
		$query = "INSERT INTO comprehensive_soil_object (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	}
}


function copy_comprehensive_rooms($oldcuid, $newcuid) {
	global $locallink, $remotelink;
	$query = "SELECT * FROM comprehensive_rooms WHERE cuid=$oldcuid";
	$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		$oldcrid = $row['crid'];
		unset($row['crid']); //get rid of crid off array for implodes
		$query = "INSERT INTO comprehensive_rooms (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
		$query = "SELECT LAST_INSERT_ID()";
		$resultid = mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
		$newcrid = mysql_result($resultid, 0);
		copy_comprehensive_wipes($oldcrid, $newcrid);
		copy_comprehensive_components($oldcrid, $newcrid);
	}
}

function copy_comprehensive_wipes($oldcrid, $newcrid) {
	global $locallink, $remotelink;
	$query = "SELECT * FROM comprehensive_wipes WHERE crid=$oldcrid";
	$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['crid']); //get rid of crid off array for implodes
		$query = "INSERT INTO comprehensive_wipes (crid, ".implode(',', array_keys($row)).") VALUES ('$newcrid','".implode("','", $row)."')";
		mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	}
}

function copy_comprehensive_components($oldcrid, $newcrid) {
	global $locallink, $remotelink;
	$query = "SELECT * FROM comprehensive_components WHERE crid=$oldcrid";
	$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['crid']); //get rid of crid off array for implodes
		$oldccid = $row['ccid'];
		unset($row['ccid']); //get rid of ccid off array for implodes
		$query = "INSERT INTO comprehensive_components (crid, ".implode(',', array_keys($row)).") VALUES ('$newcrid','".implode("','", $row)."')";
		mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
		$query = "SELECT LAST_INSERT_ID()";
		$resultid = mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
		$newccid = mysql_result($resultid, 0);
		copy_paintchips($oldccid, $newccid, 'Interior');
	}
}

function copy_conformance_comments($oldcuid, $newcuid) {
	global $locallink, $remotelink;
	$query = "SELECT * FROM conformance_comments WHERE cuid=$oldcuid";
	$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		$query = "INSERT INTO conformance_comments (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	}
}

function copy_conformance_sides($oldcuid, $newcuid) {
	global $locallink, $remotelink;
	$query = "SELECT * FROM conformance_sides WHERE cuid=$oldcuid";
	$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		unset($row['csid']);
		$query = "INSERT INTO conformance_sides (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	}
}

function copy_conformance_rooms($oldcuid, $newcuid) {
	global $locallink, $remotelink;
	$query = "SELECT * FROM conformance_rooms WHERE cuid=$oldcuid";
	$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		$oldcrid = $row['crid'];
		unset($row['crid']); //get rid of crid off array for implodes
		$query = "INSERT INTO conformance_rooms (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
		$query = "SELECT LAST_INSERT_ID()";
		$resultid = mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
		$newcrid = mysql_result($resultid, 0);
		copy_conformance_wipes($oldcrid, $newcrid);
	}
}
	
function copy_conformance_wipes($oldcrid, $newcrid) {
	global $locallink, $remotelink;
	$query = "SELECT * FROM conformance_wipes WHERE crid=$oldcrid";
	$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['crid']); //get rid of crid off array for implodes
		$query = "INSERT INTO conformance_wipes (crid, ".implode(',', array_keys($row)).") VALUES ('$newcrid','".implode("','", $row)."')";
		mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	}
}

function copy_unit_comments_assigned($oldcuid, $newcuid) {
	global $locallink, $remotelink;
	$query = "SELECT * FROM unit_comments_assigned WHERE cuid=$oldcuid";
	$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
	while ($row = mysql_fetch_assoc($result)) { //for each row
		$query = "INSERT INTO unit_comments_assigned (cuid, comid) VALUES ('$newcuid','$row[comid]')";
		mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	}
}

//**********************************************************************************//
//******************************* NON-COPY FUNCTIONS *******************************//
//**********************************************************************************//

function slashadd(&$item) {
	$item = addslashes($item);
}

function compare_records($table, $server_id, $client_id, $order, $primary) {
	//echo "*******************".$table."************************<br>";
	global $locallink, $remotelink;
	$print = array(); //array to hold keys returned
	//These two queries determine if the server and client have the same number of $table records for the unit
	$query = "SELECT * FROM $table WHERE $primary=$server_id ORDER BY $order";
	//echo $query."<br>";
	$server_result = mysql_query($query, $locallink) or sql_crapout($query."<br>".mysql_error($locallink));
	$server_record_count = mysql_num_rows($server_result);
	$query = "SELECT * FROM $table WHERE $primary=$client_id ORDER BY $order";
	$client_result = mysql_query($query, $remotelink) or sql_crapout($query."<br>".mysql_error($locallink));
	$client_record_count = mysql_num_rows($client_result);
	if ($client_record_count > $server_record_count) {//there's new records on the client so for now we'll just assume we can copy them up to the server
		//this can do the copy later, for testing it should stay commented out though.
		//copy_.$table($client_cuid, $server_cuid);
	}
	else if ($client_record_count == $server_record_count){
		while ($client_row = mysql_fetch_assoc($client_result) ){
		$server_row = mysql_fetch_assoc($server_result);
			unset($client_row[$primary]);
			unset($server_row[$primary]);
			foreach ($client_row as $key=>$val) {
				if (!($val == $server_row[$key])) {
					$print[] = array('field'=>$key, 'unique'=>$order, 'client'=>$client_row, 'server'=>$server_row);
				}
			}
		}
	}
	return $print;
}

function compare_units($client_cuid, $server_cuid) {
	global $locallink, $remotelink;
//	compared directly by cuid
	$waters = compare_records('waters', $server_cuid, $client_cuid, 'number', 'cuid');
	$comp_comments = compare_records('comprehensive_comments', $server_cuid, $client_cuid, 'type', 'cuid');
	$comp_ext = compare_records('comprehensive_ext', $server_cuid, $client_cuid, 'number', 'cuid');
	$comp_ext_components = compare_records('comprehensive_ext_components', $server_cuid, $client_cuid, 'ccid', 'cuid');
	$comp_sides = compare_records('comprehensive_sides', $server_cuid, $client_cuid, 'csid', 'cuid');
	$comp_soil_debris = compare_records('comprehensive_soil_debris', $server_cuid, $client_cuid, 'csid', 'cuid');
	$comp_soil_object = compare_records('comprehensive_soil_object', $server_cuid, $client_cuid, 'csid', 'cuid');
	$comp_rooms = compare_records('comprehensive_rooms', $server_cuid, $client_cuid, 'crid', 'cuid');
	$conf_comments = compare_records('conformance_comments', $server_cuid, $client_cuid, 'type', 'cuid');
	$conf_sides = compare_records('conformance_sides', $server_cuid, $client_cuid, 'csid', 'cuid');
	$conf_rooms = compare_records('conformance_rooms', $server_cuid, $client_cuid, 'crid', 'cuid');
	

//	compared by iid	
	$query = "SELECT iid FROM units WHERE cuid=$server_cuid";
	$result = mysql_query($query, $locallink) or sql_crapout($query."<br>".mysql_error(), $remotelink);
	$server_iid = mysql_result($result, 0);
	$query = "SELECT iid FROM units WHERE cuid=$client_cuid";
	$result = mysql_query($query, $remotelink) or sql_crapout($query."<br>".mysql_error(), $remotelink);
	$client_iid = mysql_result($result, 0);
	
	$comp_wipes = compare_records('comprehensive_wipes', $server_iid, $client_iid, 'number', 'iid');	
	$conf_wipes = compare_records('conformance_wipes', $server_iid, $client_cuid, 'number', 'iid');		
	$paintchips = compare_records('paintchips', $server_iid, $client_iid, 'number', 'iid');
	
	$query = "SELECT crid FROM comprehensive_rooms WHERE cuid=$server_cuid";
	$server_result = mysql_query($query, $locallink) or sql_crapout($query."<br>".mysql_error(), $remotelink);
	$query = "SELECT crid FROM comprehensive_rooms WHERE cuid=$client_cuid";
	$client_result = mysql_query($query, $remotelink) or sql_crapout($query."<br>".mysql_error(), $remotelink);
	while ($client_crid = mysql_fetch_assoc($client_result)) {
		$server_crid = mysql_fetch_assoc($server_result);	
		$comp_components = compare_records('comprehensive_components', $server_crid['crid'], $client_crid['crid'], 'crid', 'crid');
		$print .= print_table_data($comp_components, "comprehensive_components");
	}
	
	//	$print .= print_table_data($units, "units");
	$print .= print_table_data($waters, "waters");
	$print .= print_table_data($comp_comments, "comprehensive_comments");
	$print .= print_table_data($comp_ext, "comprehensive_ext");
	$print .= print_table_data($comp_ext_components, "comprehensive_ext_components");
	$print .= print_table_data($comp_sides, "comprehensive_sides");
	$print .= print_table_data($comp_soil_debris, "comprehensive_soil_debris");
	$print .= print_table_data($comp_soil_object, "comprehensive_soil_object");
	$print .= print_table_data($comp_rooms, "comprehensive_rooms");
	$print .= print_table_data($comp_wipes, "comprehensive_wipes");
	$print .= print_table_data($comp_components, "comprehensive_components");
	$print .= print_table_data($conf_comments, "conformance_comments");
	$print .= print_table_data($conf_sides, "conformance_sides");
	$print .= print_table_data($conf_rooms, "conformance_rooms");
	$print .= print_table_data($conf_wipes, "conformance_wipes");
	if ($print != '') {
		$print = $print = "<tr colspan=3><th colspan=3 class='box2' style='border:solid gray 1px'>Client unit id: ".$client_cuid." Server unit id: ".$server_cuid."</th></tr>".$print;
	}
	return $print;
}

function compare_inspection($iid) {
	//echo "inspection compare!!!";
	global $locallink, $remotelink;
	$inspection = compare_records('inspection', $iid, $iid, 'iid', 'iid');
	$query = "SELECT bid FROM inspection where iid=$iid";
	$client_result = mysql_query($query, $remotelink) or sql_crapout($query."<br>".mysql_error(), $remotelink);
	$client_bid = mysql_result($client_result, 0);
	$server_result = mysql_query($query, $locallink) or sql_crapout($query."<br>".mysql_error(), $remotelink);
	$server_bid = mysql_result($server_result, 0);
	$building = compare_records('building', $server_bid, $client_bid , 'bid','bid');

	$query = "SELECT cid FROM owners WHERE bid=$client_bid";
	$client_result = mysql_query($query, $remotelink) or sql_crapout($query."<br>".mysql_error(), $remotelink);
	$server_result = mysql_query($query, $locallink) or sql_crapout($query."<br>".mysql_error(), $remotelink);
	while ($client_cid = mysql_fetch_assoc($client_result)) {
		$server_cid = mysql_fetch_assoc($server_result);
		if ($server_cid['cid'] && $client_cid['cid']) {
			$db_client = compare_records('client', $server_cid['cid'], $client_cid['cid'], 'cid', 'cid');
		}
		//echo $server_cid['cid']." lhas";
	}

	$print = print_table_data($inspection, "inspection");
	$print .= print_table_data($building, "building");
	$print .= print_table_data($db_client, "client");
	return $print;
}

function print_table_data($input, $table_name) {
	$print = null;
	if (count($input)>0) {
		$print .= "<tr><td colspan=3 class='box2' style='border:solid gray 1px'>".$table_name.":</td></tr>";
		foreach ($input as $val) {
			$print .= "<tr><td>&nbsp;&nbsp;</td><td class='box2' style='border:solid gray 1px'>Field: ".$val['field']." Client Data:<br>";
			foreach ($val['client'] as $fieldname=>$client) {
				$print .= $fieldname.": ".$client.", ";
			}
			$print .= "<br>Server Data:<br>";
			foreach ($val['server'] as $fieldname=>$server) {
				$print .= $fieldname.": ".$server.", ";
			}
			$serv = array('keep'=>'server', 'uid'=>$val['unique'], 'table'=>$table_name);
			$cli = array('keep'=>'client', 'uid'=>$val['unique'], 'table'=>$table_name);
			$def = array('keep'=>'defer', 'uid'=>$val['unique'], 'table'=>$table_name);
			$print .= "</td><td class='box2' style='border:solid gray 1px'><select name='".$table_name."[".$val['client'][$val['unique']]."]'>" .
					"<option value='".serialize($serv)."'>server</option>" .
					"<option value='".serialize($cli)."'>client</option>" .
					"<option value='".serialize($def)."'>defer</option>".
					"</select></td></tr>";
		}
	}
	return $print;
}