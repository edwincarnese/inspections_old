<?php

require_once'session.php';
require_once'connect.php';

//Get something, geez
$ccid = $_POST['ccid'] or $ccid = $_GET['ccid'] or $ccid = 0;
$crid = $_POST['crid'] or $crid = $_GET['crid'] or $crid = 0;
$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;
$type = $_POST['type'] or $type = $_GET['type'] or $type = 0;
$iid = $_POST['iid'] or $iid = $_GET['iid'] or $iid = 0;
$serialnumber = $_POST['serialnumber'] or $serialnumber = $_GET['serialnumber'] or $serialnumber = 0;
$popup = $_GET['isPopup'];

if (!$iid) {
	if (!$cuid) {
		if (!$crid) {
			if (!$ccid) {
				exit('Oops');
			}
			if ($type == 'Interior') {
				$query = "SELECT crid FROM comprehensive_components WHERE ccid=$ccid";
				$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
				$crid = mysql_result($result, 0);
			} else if ($type == 'Exterior') {
				$query = "SELECT cuid FROM comprehensive_ext_components WHERE ccid=$ccid";
				$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
				$cuid = mysql_result($result, 0);
				$crid = 0;
			} else {
				exit ('Unknown type; no specified IDs');
			}
		}
		if ($crid) {
			$query = "SELECT cuid FROM comprehensive_rooms WHERE crid=$crid";
			$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			$cuid = mysql_result($result, 0);
		}
	}
	$query = "SELECT iid, number FROM units WHERE cuid=$cuid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	list($iid, $unitnumber) = mysql_fetch_row($result);
}

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);
if (!$popup) {
	$title = "$iid-$unitnumber - $address - XRF Calibration Check";
	require_once'header.php';
} else {
		?><head>
	<link rel="stylesheet" type="text/css" href="style.css" />
	</head>
	
		<div id="content">
	<?
}
?>
<form action="inspection-comprehensive-xrfcalibration-save.php" method="post">
<input type="hidden" name="ccid" value="<?php print $ccid; ?>" />
<input type="hidden" name="crid" value="<?php print $crid; ?>" />
<input type="hidden" name="cuid" value="<?php print $cuid; ?>" />
<input type="hidden" name="type" value="<?php print $type; ?>" />
<input type="hidden" name="iid" value="<?php print $iid; ?>" />
<p>
Serial Number: <?php
$numbers = array('RMD-LPA-1, SN#2421','RMD-LPA-1B, SN#2538','Niton-XLP 303 AW, SN#8988');
foreach ($numbers as $number) {
	if ($number == $serialnumber) {
		print "<input type=\"radio\" name=\"serialnumber\" value=\"$number\" checked=\"checked\" />$number&nbsp;&nbsp;&nbsp;";
	} else {
		print "<input type=\"radio\" name=\"serialnumber\" value=\"$number\" />$number&nbsp;&nbsp;&nbsp;";
	}
}
?><br />
Standard Type:
<input type="radio" name="standardtype" value="Manufacturer" checked="checked" />Manufacturer&nbsp;&nbsp;
<input type="radio" name="standardtype" value="NIST" />NIST&nbsp;&nbsp;
<input type="radio" name="standardtype" value="Other" />Other&nbsp;&nbsp;
</p>
<table class="straightup">
<tr><th>Standard<br />(mg/cm2)</th><th>Temperature</th><th>Result1</th><th>Result2</th><th>Result3</th><?php
if ($cuid) {
	print '<th>Comments</th>';
}
?></tr>
<tr>
<td><input type="text" name="standard" size="5" value="1.0" /></td>
<td><input type="text" name="temperature" size="5" value="68" /></td>
<td>
<table>
<?php
for($x=0; $x<=9; $x++) {
	print "<tr><td><input type=\"radio\" name=\"xrf11\" value=\"$x\" />$x</td>";
	print "<td><input type=\"radio\" name=\"xrf12\" value=\"$x\" />.$x</td></tr>";
}
?>
</table>
</td>
<td>
<table>
<?php
for($x=0; $x<=9; $x++) {
	print "<tr><td><input type=\"radio\" name=\"xrf21\" value=\"$x\" />$x</td>";
	print "<td><input type=\"radio\" name=\"xrf22\" value=\"$x\" />.$x</td></tr>";
}
?>
</table>
</td>
<td>
<table>
<?php
for($x=0; $x<=9; $x++) {
	print "<tr><td><input type=\"radio\" name=\"xrf31\" value=\"$x\" />$x</td>";
	print "<td><input type=\"radio\" name=\"xrf32\" value=\"$x\" />.$x</td></tr>";
}
?>
</table>
</td>
<?php
if ($cuid) {
?>
<td>
<textarea name="comment" rows="10" cols="40">
<?php
$query = "SELECT comment FROM comprehensive_comments WHERE cuid=$cuid AND type='xrf'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result)) {
	$comment = mysql_result($result, 0);
	print $comment;
}
?>
</textarea>
</td>
<?php
}
?></tr>
</table>
<p>
<input type="submit" name="submit" value="Save" /><input type="reset" value="Reset" /><input type="submit" name="submit" value="Cancel" <? if ($popup) { print "onClick='javascript:self.close();' "; }?>/> | <input type="submit" name="submit" value="Skip XRF Readings" />
</p>
</form>

<p><a href="inspection-main.php?iid=<?php print $iid; ?>">Inspection Main Menu</a></p>

<?php
if (!$popup){
	require_once'footer.php';
} else {
?></div>
<?}?>
