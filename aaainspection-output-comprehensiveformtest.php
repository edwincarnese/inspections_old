<?php
// 10/17/06 rhc Took the extra stuff I had in the file out.
require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;
$cuidunit = $cuid;
require_once'include-form-setup.php';

$complist=array();
$complist(0)=68;
$complist(1)=74;
$complist(1)=77;
$count=0;
require_once'include-form-Comp-Page1.php';
require_once'include-form-Comp-Page2.php';
require_once'include-form-comprehensive-water.php';
require_once'include-form-comprehensive-calibration.php';

//unit diagrams
$query = "SELECT diagrams FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$diagrams = mysql_result($result, 0);
//common areas diagrams
$query = "SELECT diagrams FROM unit_links INNER JOIN units ON (unit_links.common = units.cuid) WHERE unit_links.unit=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while ($row = mysql_fetch_row($result)) {
//	$diagrams += $row[0];
}
//exterior diagrams
$query = "SELECT u1.diagrams FROM units AS u1 INNER JOIN units AS u2 USING (iid) WHERE u1.unittype='Exterior' AND u2.cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$diagrams += mysql_result($result, 0);

for ($x=0; $x<$diagrams; $x++) {
require'include-form-comprehensive-Property-Sketch.php';
}

//rooms

//rooms
$roomquery = "SELECT crid FROM comprehensive_rooms WHERE cuid=$cuid ORDER BY number";
$roomresult = mysql_query($roomquery) or sql_crapout($roomquery.'<br />'.mysql_error());
while ($roomrow = mysql_fetch_row($roomresult)) {
	$crid = $roomrow[0];
	$start = $total = 0;
//	$WMdone = $Wdone = $WWdone = $Ddone =0; //reset crossouts for each room
	do {
		require 'include-form-comprehensive-interior.php';
	} while ($start < $total);
}


//rooms in common areas

$unitquery = "SELECT common FROM unit_links WHERE unit = $cuid";
$unitresult = mysql_query($unitquery) or sql_crapout($unitquery.'<br />'.mysql_error());

while ($unitrow = mysql_fetch_row($unitresult)) {
	$roomquery = "SELECT crid FROM comprehensive_rooms WHERE cuid=$unitrow[0] ORDER BY number";
	$roomresult = mysql_query($roomquery) or sql_crapout($roomquery.'<br />'.mysql_error());
	while ($roomrow = mysql_fetch_row($roomresult)) {
		$cuid = $unitrow[0];
		$crid = $roomrow[0];
		$start = $total = 0;
		$WMdone = $Wdone = $WWdone = $Ddone =0; //reset crossouts for each room
		do {
			require 'include-form-comprehensive-interior.php';
		} while ($start < $total);
	}
}
$cuid = $cuidunit;

//exterior page
$query = "SELECT ext.cuid FROM units INNER JOIN units AS ext USING (iid) WHERE units.cuid=$cuid AND ext.unittype='Exterior'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$maincuid = $cuid;
$cuid = mysql_result($result, 0);
$extcuid = $cuid;
$start = 0;
do {
	require'include-form-comprehensive-exterior.php';
} while ($start < $total);

$cuid = $maincuid;

require_once'include-form-comprehensive-dust.php';

$cuid = $extcuid;
require_once'include-form-comprehensive-soil.php';

// remove the filename and D for live version, they are just for testing on Jay's machine which has issues with pdf plugin.'
$pdf->Output();

?>