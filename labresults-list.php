<?php

require_once'session.php';
require_once'connect.php';

$title = "Inspections with Labs";
require_once'header.php';
if ($_GET['order'] == 'iidup') {
	$iidorder = 'iiddown';
} else {
	$iidorder = 'iidup';
}
if ($_GET['order'] == 'scheddateup') {
	$schedorder = 'scheddatedown';
} else {
	$schedorder = 'scheddateup';
}
?>
<table>
<tr><th><a href="labresults-list.php?order=<?print $iidorder;?>">Insp #</a></th><th>Address</th><th><a href="labresults-list.php?order=<?print $schedorder;?>">Date</a></th></tr>
<?php
switch ($_GET['order']) {
	case 'scheddatedown':
		$orderstring = 'ORDER BY scheddate DESC';
		break;
	case 'scheddateup':
		$orderstring = 'ORDER BY scheddate';
		break;
	case 'iidup':
		$orderstring = 'ORDER BY iid';
		break;
	case 'iiddown':
	default:
		$orderstring = 'ORDER BY iid DESC';
		break;
}		

$query = "SELECT iid FROM waters WHERE results = '' UNION
SELECT iid FROM paintchips WHERE results = '' UNION
SELECT iid FROM comprehensive_wipes WHERE results = '' UNION
SELECT iid FROM conformance_wipes WHERE results = '' UNION
SELECT iid FROM comprehensive_soil_debris INNER JOIN units USING (cuid) WHERE results = '' UNION
SELECT iid FROM comprehensive_sides INNER JOIN units USING (cuid) WHERE results = '' UNION
SELECT iid FROM comprehensive_soil_object INNER JOIN units USING (cuid) WHERE results = ''";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while ($row = mysql_fetch_row($result)) {
	$iids[] = $row[0];
}
if (mysql_num_rows($result)) {
	$iidstring = implode(',', $iids);

	$query = "SELECT inspection.iid, building.streetnum, building.address, building.suffix, building.city, DATE_FORMAT(scheddate, '%c/%e/%Y') AS scheddate FROM building INNER JOIN inspection USING (bid) WHERE iid IN ($iidstring) $orderstring";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while($row = mysql_fetch_assoc($result)) {
		print "<tr><td>$row[iid]-*</td><td><a href=\"labresults-inspection.php?iid=$row[iid]\">$row[streetnum] $row[address] $row[suffix], $row[city]</a></td><td>$row[scheddate]</td></tr>\n";
	}
} else {
	print "No inspections have outstanding lab results.";
}
?>
</table>
<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>