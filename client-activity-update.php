<?php

require_once'session.php';
require_once'connect.php';

$caid = $_POST['caid'] or $caid = $_GET['caid'] or $caid = 0;

$uid = $sid; // $sid from session.php; session ID, user ID, whatever

$_POST['description'] = htmlspecialchars($_POST['description']);

if ($_POST['dueyear'] < 100) {
	$_POST['dueyear'] += 2000;
}
if ($_POST['doneyear'] < 100) {
	$_POST['doneyear'] += 2000;
}

/*
Because htmlspecialchars() is being performed on the data, it is possible that some data may be truncated. This can happen if, for example, the data contains '&', which becomes '&amp;', and the resulting string is greater than the allowed column width. However, the column widths are rather generous, and it only affects a few characters, so as such, it was deemed not worth checking the various lengths of each field to see if they would actually be truncated. It should be noted that if no substitutions are performed, the 'maxlength' attribute on the form elements will prevent an entry from being truncated.
*/
$t= date("h:i:s");;

$datedue = $_POST['dueyear'].'-'.$_POST['duemonth'].'-'.$_POST['dueday'] ." ".$t;

//if ($_POST['status'] == 'Done') {
	$datedone = $_POST['doneyear'].'-'.$_POST['donemonth'].'-'.$_POST['doneday'];
//} else {
//	$datedone = '';
//}

if ($_POST['submit'] == 'Save' && strlen($_POST['description']) > 0) {
	$query = "UPDATE client_activities SET date_due='$datedue', assignedto='$_POST[assignedto]', description='$_POST[description]', display='$_POST[display]', status='$_POST[status]', statusdate='$datedone' WHERE caid=$caid";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
}

$query = "SELECT cid FROM client_activities WHERE caid=$caid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$cid = mysql_result($result, 0);

header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/client-view.php?cid=$cid&isPopup=$_POST[popup]");

?>