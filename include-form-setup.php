<?php

//header("Content-type:application/pdf");
require_once(realpath($_SERVER["DOCUMENT_ROOT"]).'/fpdf152/fpdf.php');


$pdf = new FPDF('P', 'in', 'Letter');
$pdf->SetLeftMargin(0.0);
$pdf->SetRightMargin(0.0);
$pdf->SetTopMargin(0.0);
$pdf->SetAutoPageBreak(true, 0.0);
$pdf->AliasNbPages('{totalpages}');

function formfix(&$array) {
	if (is_array($array)) {
		foreach ($array as &$item) {
			$item = html_entity_decode($item);
		}
	} else {
		$array = html_entity_decode($array);
	}
}
?>