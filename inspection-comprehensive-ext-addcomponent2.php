<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;
//plural now
$itemids = $_POST['itemid'] or $itemids = $_GET['itemid'] or $itemids = array();

$query = "SELECT * FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}
$ext = mysql_fetch_assoc($result);
$unitdesc = $ext['unitdesc'];
$iid = $ext['iid'];
$unitnumber = $ext['number'];

$multids = array();

foreach ($itemids as $itemid) {
	$query = "SELECT * FROM room_item_components WHERE itemid=$itemid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

	if (mysql_num_rows($result) == 1) { //skip step 3 if only one for each
		$row = mysql_fetch_assoc($result);
		if($row['defaultassessment']) {
			$assess = "'$row[defaultassessment]'";
		} else {
			$assess = 'NULL';
		}		
		$query = "INSERT INTO comprehensive_ext_components (cuid, name, side, sid, condition, grouplist, conformancecomponent, displayorder, hazardassessment) VALUES ($cuid, '$component[itemname] $row[componentname]', '$component[side]', $row[dsid], '$row[defaultcondition]', '$row[grouplist]', '$row[conformancecomponent]', $row[displayorder], $assess)";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	} else {
		$multids[] = $itemid; //edit foreach below if reimplemented
	}
}

if (count($multids) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-comprehensive-ext-component.php?cuid=$cuid");
	exit();
}

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - $unitdesc - Add Component";
require_once'header.php';
?>
<p>Check the parts of these components to add.</p>
<form action="inspection-comprehensive-ext-addcomponent3.php" method="post">
<input type="hidden" name="cuid" value="<?php print $cuid; ?>" />
<table class="straightup">
<tr><th>Parts</th><th>Default Side</th><th>Jump</th></tr>
<?php
foreach ($multids as $itemid) {
	$x++;
	$query = "SELECT itemname FROM room_item_list WHERE itemid=$itemid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$itemrow = mysql_fetch_assoc($result);
?>
<tr>
<td>
<a name="item<?php print $x;?>"></a>
<?php
	print "<b>$itemrow[itemname]</b><br />\n";
	$query = "SELECT * FROM room_item_components WHERE itemid=$itemid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	if (mysql_num_rows($result) > 1) { //print itemname only if multiple components
?>
<input type="hidden" name="components[<?php print $x; ?>][itemname]" value="<?php print $itemrow['itemname']; ?>" />
<?php
	}

	while ($row = mysql_fetch_assoc($result)) {
		print "<input type=\"checkbox\" checked=\"checked\" name=\"components[$x][parts][]\" value=\"$row[componentid]\" />$row[componentname]<br />\n";
	}
?>
</td>
<td>
<input type="radio" name="components[<?php print $x; ?>][side]" value="0" checked="checked" />None<br />
<input type="radio" name="components[<?php print $x; ?>][side]" value="1" />1<br />
<input type="radio" name="components[<?php print $x; ?>][side]" value="2" />2<br />
<input type="radio" name="components[<?php print $x; ?>][side]" value="3" />3<br />
<input type="radio" name="components[<?php print $x; ?>][side]" value="4" />4<br />
</td>
<td>
<a href="#item<?php print $x + 1;?>">Next</a>
</td></tr>
<?php
}
?>
</table>
<p><input type="submit" name="submit" value="Save" /> <input type="reset" value="Reset" /> <input type="submit" name="submit" value="Cancel" /></p>
</form>

<p><a href="inspection-comprehensive-unit.php?cuid=<?php print $ext[cuid]; ?>">Exterior Main Menu</a></p>

<p><a href="inspection-main.php?iid=<?php print $ext[iid]; ?>">Inspection Main Menu</a></p>

<?php
require_once'footer.php';
?>