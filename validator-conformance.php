<?php
/*
 * Created on Dec 28, 2005
 *
 * Author: Jason Rankin
 * 
 * This script runs when going to the conformance wrapup report.  It contains short 
 * functions which validate certain aspects of the report and return a boolean value.
 */
?>
<?
require_once 'connect.php';

$cuid = $_GET['cuid'];
$valid = validateDustWipes($cuid);
 if ($valid == 0) {
 	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-conformance-interior-wrapup.php?cuid=$cuid");
} else if ($valid == 1){
	echo "<script language='javascript'>alert('There are no dustwipes for this unit and they have not been deferred.');" .
			"history.back();" .
			"</script>";
} else if ($valid == 2){
	echo "<script language='javascript'>alert('There must be at least 3 dustwipes for a unit.');" .
			"history.back();" .
			"</script>";
 }

function validateDustWipes($cuid) {
	$errors = 0;
	$query = "SELECT COUNT(*) FROM conformance_wipes INNER JOIN conformance_rooms USING (crid) WHERE cuid=$cuid AND conformance_wipes.iid=(SELECT iid FROM units where cuid=$cuid)";
	$result = mysql_query($query) or sql_crapout($query."<br>".mysql_error());	
	$wipeCount = mysql_result($result, 0);
	if ($wipeCount == 0) {
		$query = "SELECT title FROM unit_comments INNER JOIN unit_comments_assigned USING (comid) WHERE unit_comments_assigned.cuid=$cuid";
		$result = mysql_query($query) or sql_crapout($query."<br>".mysql_error());
		$deferred = false;
		while ($row = mysql_fetch_assoc($result)) {
			if ($row['title'] == 'defer wipes') {
				$deferred = true;
			}
		}
		if (!$deferred) {
			$errors = 1;
		}
	} else if ($wipeCount < 3) {
		$errors = 2;
	} 
	return $errors;
}

?>