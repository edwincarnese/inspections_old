<?
require_once'session.php';
?>
<script language=javascript>
function closePopUp() {
	popup = '<?print $_POST['popup'];?>';
	if (popup != '') {
		opener.location.reload(true);
		self.close();
	}
}
</script>
<body onLoad="javascript:closePopUp();"></body>
<?php


require_once'connect.php';

//print_r($_POST);
//exit();

$unitnumber = 0; //now important!

$iid = $_POST['iid'];
unset($_POST['iid']);

if ($_POST['reason'] != 'Other') {
	unset($_POST['otherreason']);
}

$_POST['scheddate'] = "$_POST[year]-$_POST[month]-$_POST[day]";
print $_POST['scheddate'];

if ($_POST['startampm'] == 'PM' && $_POST['starthour'] != 12) {
	$_POST['starthour'] += 12;
}
$_POST['schedstart'] = "$_POST[starthour]:$_POST[startmin]:00";

if ($_POST['endampm'] == 'PM' && $_POST['endhour'] != 12) {
	$_POST['endhour'] += 12;
}
if ($_POST['startampm'] == 'AM' && $_POST['starthour'] == 12) {
	$_POST['starthour'] -= 12;
}
$_POST['schedstart'] = "$_POST[starthour]:$_POST[startmin]:00";

if ($_POST['endampm'] == 'AM' && $_POST['endhour'] == 12) {
	$_POST['endhour'] -= 12;
}
$_POST['schedend'] = "$_POST[endhour]:$_POST[endmin]:00";

unset($_POST['year']);
unset($_POST['month']);
unset($_POST['day']);
unset($_POST['starthour']);
unset($_POST['startmin']);
unset($_POST['startampm']);
unset($_POST['endhour']);
unset($_POST['endmin']);
unset($_POST['endampm']);
unset($_POST['popup']);

$numunits = $_POST['numunits'];
unset($_POST['numunits']);

//shortcut - all fields are text
foreach ($_POST as $field => $value) {
	$updatelist[] = $field . "='" . htmlspecialchars($value) . "'";
}

/*
Because htmlspecialchars() is being performed on the data, it is possible that some data may be truncated. This can happen if, for example, the data contains '&', which becomes '&amp;', and the resulting string is greater than the allowed column width. However, the column widths are rather generous, and it only affects a few characters, so as such, it was deemed not worth checking the various lengths of each field to see if they would actually be truncated. It should be noted that if no substitutions are performed, the 'maxlength' attribute on the form elements will prevent an entry from being truncated.
*/

$query = "UPDATE inspection SET ".implode(',', $updatelist)." WHERE iid=$iid";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());


if ($_POST['popup'] == '') {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-view.php?iid=$iid");
}
?>