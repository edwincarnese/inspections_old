<?php

//$pdf->AddPage('P');
//Startr Form
$x = 0;
$y = 0;
$ml=0.625;
$w=7.25;
$pdf->SetFont('Times','','12');
$pdf->SetLineWidth(0.014);
//Start Banner box package
$y=$y+0.74; 		//Increment Y from last input.
$ys=$y-0.2;  	//Save Y at start.
$ff=65;			//Chang in $y = $fontsize/$ff
$fontsize=10;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml+1.7, $y);
$pdf->Cell(0,0,'RHODE ISLAND HOUSING RESOURCES COMMISSION');
$y=$y+$fontsize/$ff+0.15;	//Increment to next line based on font size
$fontsize=10;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml+2.4, $y);
$pdf->Cell(0,0,'LEAD HAZARD MITIGATION');
$y=$y+$fontsize/$ff+0.17;
$fontsize=10;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml+2.15, $y);
$pdf->Cell(0,0,'CERTIFICATE OF CONFORMANCE');
$y=$y+$fontsize/$ff+0.19;

$pdf->SetXY($ml+5.1, $y);
$pdf->Cell(0,0, "Certificate #:");
$pdf->line($ml+5.9, $y+0.06, $ml+$w, $y+0.06);	
$y=$y+$fontsize/$ff+0.26;

$pdf->SetLineWidth(0.02);
$pdf->line($ml, $y, $ml+$w, $y);	
$pdf->SetLineWidth(0.01);

$fontsize=10;
$y=$y+0.23;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'1. DWELLING OR PREMISES CERTIFIED AS MEETING CONFORMANCE:');
$y=$y+$fontsize/$ff+0.17;
$fontsize=10;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0, "Street:");
$pdf->line($ml+0.4, $y+0.06, $ml+2.95, $y+0.06);	
$pdf->SetXY($ml+2.95, $y);
$pdf->Cell(0,0,'Total Dwelling Units:');
$pdf->line($ml+4.25, $y+0.06, $ml+5.1, $y+0.06);	
$pdf->SetXY($ml+5.1, $y);
$pdf->Cell(0,0,'Plat/Lot #.:');
$pdf->line($ml+5.8, $y+0.06, $ml+$w, $y+0.06);	
$y=$y+$fontsize/$ff+0.17;
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0, "City/Town:");
$pdf->line($ml+0.6, $y+0.06, $ml+2.3, $y+0.06);	
$pdf->SetXY($ml+2.35, $y);
$pdf->Cell(0,0,'Zip:');
$pdf->line($ml+2.65, $y+0.06, $ml+3.2, $y+0.06);	
$pdf->SetXY($ml+3.7, $y);
$pdf->Cell(0,0,'Apartment/Floor/Unit #:');
$pdf->line($ml+5.15, $y+0.06, $ml+$w, $y+0.06);	
$y=$y+$fontsize/$ff+0.13;
$pdf->SetLineWidth(0.02);
$pdf->line($ml, $y, $ml+$w, $y);	
$pdf->SetLineWidth(0.01);
$y=$y+$fontsize/$ff+0.04;

$fontsize=10;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'2. OWNER OF DWELLING OR PREMISES:');
$y=$y+$fontsize/$ff+0.16;

$fontsize=10;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,"Name:");
$pdf->line($ml+0.45, $y+0.06, $ml+3, $y+0.06);	
$pdf->SetXY($ml+3.7, $y);
$pdf->Cell(0,0,'Telephone No.:');
$pdf->line($ml+4.65, $y+0.06, $ml+6.625, $y+0.06);	
$y=$y+$fontsize/$ff+0.14;
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0, "Street:");
$pdf->line($ml+0.45, $y+0.06, $ml+2.5, $y+0.06);	
$pdf->SetXY($ml+2.6, $y);
$pdf->Cell(0,0,'City/Town:');
$pdf->line($ml+3.33, $y+0.06, $ml+4.875, $y+0.06);	
$pdf->SetXY($ml+4.875, $y);
$pdf->Cell(0,0,'State:');
$pdf->line($ml+5.375, $y+0.06, $ml+6, $y+0.06);	
$pdf->SetXY($ml+6, $y);
$pdf->Cell(0,0,'Zip:');
$pdf->line($ml+6.3, $y+0.06, $ml+$w, $y+0.06);	
$y=$y+$fontsize/$ff+0.12;
$pdf->SetLineWidth(0.02);
$pdf->line($ml, $y, $ml+$w, $y);	
$pdf->SetLineWidth(0.01);

$fontsize=10;
$y=$y+0.23;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'3. TENANT INFORMATION:');
$y=$y+$fontsize/$ff+0.16;
$fontsize=10;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0, "Name:");
$pdf->line($ml+0.55, $y+0.06, $ml+3.1, $y+0.06);	
$pdf->SetXY($ml+3.25, $y);
$pdf->Cell(0,0,'Children under 6:');
$pdf->line($ml+4.28, $y+0.06, $ml+4.7, $y+0.06);	
$y=$y+$fontsize/$ff+0.14;
$pdf->SetLineWidth(0.02);
$pdf->line($ml, $y, $ml+$w, $y);	
$pdf->SetLineWidth(0.01);


$fontsize=10;
$y=$y+0.23;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'4. CERTIFICATION OF INDEPENDENT CLEARANCE INSPECTION PERFORMANCE:');
$y=$y+$fontsize/$ff+0.14;
$fontsize=10;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0, "Inspection Type:");
$pdf->line($ml+1, $y+0.06, $ml+4.3, $y+0.06);	
$pdf->SetXY($ml+4.35, $y);
$pdf->Cell(0,0,'Inspection Date:');
$pdf->line($ml+5.35, $y+0.06, $ml+$w, $y+0.06);	
$y=$y+$fontsize/$ff+0.17;
$pdf->SetLineWidth(0.02);
$pdf->line($ml, $y, $ml+$w, $y);	
$pdf->SetLineWidth(0.01);

$fontsize=10;
$y=$y+0.2;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'5. CERTIFICATION OF CONFORMANCE:');
$y=$y+0.05;
$fontsize=10;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml, $y);
$pdf->multiCell(7.25,.18,'The dwelling or premises in Item 1 above is certified to be in conformance with the Lead Hazard Mitigation Standards as of the Certification Date specified below.  Conformance is contingent upon routine maintenance of the property.  This Certification of Conformance shall b valid for two years from the date of Certification or until the next turnover of the dwelling unit, whichever is shorter, provided that no more than one Clearance Inspection shall be required in any twelve months.  This Certification may be extended by receipt of an Affidavit of Completion of Visual Inspection as specified by RIGL 42-128.1-4.5.');

$y=$y+1.16;
$fontsize=10;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml, $y);
$pdf->multiCell(7.25,.16,'I certify that I have conducted the inspection specified in Item 4 above in accordance with the Housing Resources Commission Lead Hazard Mitigation Regulations, and have determined that the dwelling or premises identified above is in  conformance, as defined by these regulations.  I certify that I am not the property owner of the property or an employee of the property owner.');
$y=$y+1.15;

$fontsize=10;
$pdf->SetFont('Times','', $fontsize);

$pdf->line($ml, $y-0.12, $ml+2.35, $y-0.12);	
$pdf->SetXY($ml+0.55, $y);
$pdf->Cell(0,0,'(Signature)');
$pdf->line($ml+2.9, $y-0.1, $ml+6.65, $y-0.1);	
$pdf->SetXY($ml+3.2, $y);
$pdf->Cell(0,0,'(Type or Print Name of Person Conducting Inspection)');

$y=$y+0.3;
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0, "Certification Date:");
$pdf->line($ml+1.125, $y+0.06, $ml+2.625, $y+0.06);	
$pdf->SetXY($ml+3.3, $y);
$pdf->Cell(0,0,'RI License No:');
$pdf->line($ml+4.25, $y+0.06, $ml+6, $y+0.06);	

$y=10.5;
$pdf->SetFont('Times','B', 9);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'HRC LHM FORM - 1 (4/04)');

?>