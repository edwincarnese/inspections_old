<?php

//subtract 0.06 inches from desired position of filst letter
// with 0.1 lineheight, measure from middle using Cell().

/*
Places to put X's using 6-point text:
ITEM		X		Y
Stardard:	0.87	2.88
------------------------
Paint:		2.155	3.51
Soil:		2.155	3.80
Water:		3.18	3.04	(Water, Drinking)
Wipe:		3.18	3.34
------------------------
Lead:		6.76	2.40
*/

require_once'include-form-coc-common.php'; //sets $iid, in case needed (and $unitnumber)

$pdf->SetFont('Arial','',6);
$pdf->SetXY(2.155, 3.51);
$pdf->Cell(0,0, 'X');

//PAINT CHIPS
$query = "SELECT comprehensive_components.ccid, DATE_FORMAT(xrftaken, '%c/%e') AS `date`, DATE_FORMAT(xrftaken, '%l%p') as `time`, comprehensive_rooms.name AS room, comprehensive_components.name, paintchips.number, side, comprehensive_rooms.number AS roomnumber FROM comprehensive_rooms INNER JOIN comprehensive_components USING (crid) INNER JOIN paintchips USING (ccid) WHERE cuid=$cuid AND comptype='Interior' ORDER BY paintchips.number";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) > 9) { // Remove this at some point!!
	exit('Too many samples for one page.');
}

$pdf->SetFont('Arial','','8');
$x = 0.85; $y = 4.43;
while($row = mysql_fetch_assoc($result)) {
	$pdf->SetXY($x, $y);
	$pdf->Cell(0.69,0,$iid.'-'.$unitnumber.'P'.$row['number'],0,0,'C');
	$pdf->SetX($x + 0.78);
	$pdf->Cell(0.54,0,"$row[date]",0,0,'C');
	$pdf->SetX($x + 1.29);
	$pdf->Cell(0.47,0,"$row[time]",0,0,'C');
	$pdf->SetXY($x + 1.78, $y - 0.06);
	$pdf->Cell(0,0,"$row[room] (Rm $row[roomnumber]), Side $row[side]");
	$pdf->SetXY($x + 1.78, $y + 0.06);
	$pdf->Cell(0,0,"$row[name]");
	$pdf->SetX($x + 7.02);
	$pdf->Cell(0,0,"1");
	$y += 0.31;
}
?>