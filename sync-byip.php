<?php

require_once'connect.php';

$SYNC_CONTROL_FILE = "sync-control.txt";

/*
 * Syncing is down, this code needs to stay here
 *
require_once 'header.php';
$fp = fopen($SYNC_CONTROL_FILE, "r");
$val = fgets($fp);
while (!feof($fp)) {
  $printval .= fread($fp, 8192);
}
fclose($fp);
if (strstr($val, "Off")){
	print $printval;
	exit;
}
*/


//The server MUST NOT be the client!
//if ($_SERVER['SERVER_ADDR'] == $_SERVER['REMOTE_ADDR']) {
//	exit('This page may not be accessed from the machine it resides on!');
//}

if (!$_POST['sumbit']) {
	$title = 'Sync';
	require_once'header.php';
	?>
	<form action="sync-byip.php" method="post">
	<p>Download all inspections from the past 
	<select name="past">
	<option value="-1">No Limit</option>
	<option value="0" selected="selected">0</option>
	<option value="1">1</option>
	<option value="2">2</option>
	<option value="3">3</option>
	<option value="4">4</option>
	<option value="5">5</option>
	<option value="6">6</option>
	<option value="7">7</option>
	<option value="8">8</option>
	<option value="9">9</option>
	<option value="10">10</option>
	</select>
	 days and next 
	<select name="next">
	<option value="-1">No Limit</option>
	<option value="1">1</option>
	<option value="2">2</option>
	<option value="3" selected="selected">3</option>
	<option value="4">4</option>
	<option value="5">5</option>
	<option value="6">6</option>
	<option value="7">7</option>
	<option value="8">8</option>
	<option value="9">9</option>
	<option value="10">10</option>
	</select>
	 days.
	</p>
	<p><input type="submit" name="sumbit" value="Go" /></p>
	</form>
	<?php
		require_once'footer.php';
		exit();
} else { //set variables here used way below
	$past = $_POST['past'];
	$next = $_POST['next'];
	
}


$locallink = $link; //avoid confusion; don't change connect.php


// Connect to sql database
$server = '192.168.10.50';
//$user = 'rilttestuser'; //set in connect.php
//$password = 'testmenow'; //set in connect.php
$db = 'zapp_live';
$remotelink = @mysql_connect("$server", "$user", "$password") or exit($server."<br>".$user."<br>".$password."<br>".'Could not connect to the client.');
@mysql_select_db("$db", $remotelink) or exit('Could not speak with server.');

//*****************************************************************************//
//*************************** TABLE STRUCTURE *********************************//
//*****************************************************************************//


//This doesn't seem to work but it's not terribly important during testing/dev


require_once 'mysql-install-lastupdate.php'; //installs the lastupdate table if needed

$query = "SELECT number FROM lastupdate";
$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
$version = mysql_result($result, 0);
$query = "SELECT filename, number FROM updates WHERE number>$version ORDER BY number";
$fileresult = mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
$number = 0;
while ($filerow = mysql_fetch_row($fileresult)) {
	require "$filerow[0]";
	$number = $filerow[1];
}
if ($number) {
	$query = "UPDATE lastupdate SET number=$number";
	mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));
}

//*****************************************************************************//
//*************************** COPY TO SERVER **********************************//
//*****************************************************************************//

include_once 'sync-functions.php';

set_time_limit(300);

$numdiscreps = 0;
$discreps = array();


//Get inspection ids and status' from client
$query = "SELECT iid, status FROM inspection";
$result = mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

while ($row = mysql_fetch_assoc($result)) {
	//update inspection status (on server) for each inspection on client
	$query = "UPDATE inspection SET status='$row[status]' WHERE iid=$row[iid]";
	mysql_query($query, $locallink) or sql_crapout($query.'<br />'.mysql_error($locallink));
	$discrepancy_data = compare_inspection($row['iid']);
	copytechs($row['iid']);
	copycalibration($row['iid']);
	copyunits($row['iid']);
}



//*****************************************************************************//
//****************************  FIND DISCREPANCIES! ***************************//
//*****************************************************************************//
if ($numdiscreps || $discrepancy_data != '') {
	for ($i=0; $i<$numdiscreps; $i++) {
		$discrepancy_data .= compare_units($discreps[$i][0], $discreps[$i][1]);
	}
	if ($discrepancy_data != '') {
		$title = "Discrepancies Found";
		require_once'header.php';
		
		print "There were $numdiscreps discrepancies found between the data on this tablet and the data on the server.<br>";
		print "<form name='discreps' method='POST' action='sync-discreps-action.php'><table class='box'>";
		print "<input type='hidden' name='past' value='$past'/><input type='hidden' name='next' value='$next'/>";
		print $discrepancy_data;
		print "</table><input type='submit' name='submit' value='commit changes'/>";
		require_once 'footer.php';	
	} else {
		header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/sync-discreps-action.php?past=$past&next=$next");
	}
} else {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/sync-discreps-action.php?past=$past&next=$next");
}
?>

