<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

$query = "SELECT * FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}
$ext = mysql_fetch_assoc($result);
$iid = $ext['iid'];
$unitnumber = $ext['number'];

$query = "SELECT * FROM room_item_list WHERE type='Exterior'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - $ext[unitdesc] - Add Component";
require_once'header.php';
?>
<p>Choose the components to add.</p>
<form action="inspection-comprehensive-ext-addcomponent2.php" method="post">
<input type="hidden" name="cuid" value="<?php print $cuid; ?>" />
<p>
<?php

while ($row = mysql_fetch_assoc($result)) {
	print "<input type=\"checkbox\" name=\"itemid[]\" value=\"$row[itemid]\" />$row[itemname]<br />\n";
}
?>
<input type="submit" value="Next" />
</p>
</form>

<p><a href="inspection-comprehensive-unit.php?cuid=<?php print $ext[cuid]; ?>">Exterior Main Menu</a></p>

<p><a href="inspection-main.php?iid=<?php print $ext[iid]; ?>">Inspection Main Menu</a></p>

<?php
require_once'footer.php';
?>