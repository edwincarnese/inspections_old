<script language=javascript>
function openerHref() {
	document.update.popup.value = opener.location.href;
}

</script>
<?php 
$iid = $_POST['iid'] or $iid = $_GET['iid'] or $iid = 0;

//require_once'session.php';
require_once'connect.php';

$title = "Schedule Inspector";
require_once'header.php';
?>
<p>Choose the inspectors to assign to this inspection.</p>

<form name="update" action="inspection-inspector-assign.php" method="post">
<input type="hidden" name="iid" value="<?php print $iid;?>" />

<table>
<tr><th>Lead</th><th>Assist</th><th>Name</th></tr>
<tr><td><input type="radio" name="lead" value="" /></td>
<td></td>
<td>None</td></tr>
<?php
$query = "SELECT inspector.tid, lastname, firstname, job, eli FROM inspector LEFT JOIN insp_assigned ON (inspector.tid=insp_assigned.tid AND insp_assigned.iid=$iid) ORDER BY lastname, firstname";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while ($row = mysql_fetch_assoc($result)) {
	print '<tr><td>';
	if ($row['job'] == 'Lead') {
		print "<input type=\"radio\" name=\"lead\" value=\"$row[tid]\" checked=\"checked\" />";
	} else if ($row['eli']) {
		print "<input type=\"radio\" name=\"lead\" value=\"$row[tid]\" />";
	}
	print '</td><td>';
	if ($row['job'] == 'Assistant') {
		print "<input type=\"checkbox\" name=\"assist[]\" value=\"$row[tid]\" checked=\"checked\" />";
	} else {
		print "<input type=\"checkbox\" name=\"assist[]\" value=\"$row[tid]\" />";
	}
	print "</td><td>$row[lastname], $row[firstname]</td></tr>\n";
}
?>
</table>
<input type="hidden" name="popup"/>
<p>
<input type="submit" name="submit" value="Save" onClick="javascript:openerHref();"/><input type="reset" value="Reset" /><input type="submit" name="submit" value="Cancel" />
</p>

<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>