<?php

require_once'session.php';
require_once'connect.php';

$crid = $_POST['crid'] or $crid = $_GET['crid'] or $crid = 0;

$query = "SELECT * FROM comprehensive_rooms INNER JOIN units USING (cuid) WHERE crid=$crid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}

$room = mysql_fetch_assoc($result);
$unitnumber = $room['number']; //units is second table and will overwrite room #
$iid = $room['iid'];

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - $room[unitdesc] - $room[name] - Edit name";
require_once'header.php';
?>
<form action="inspection-comprehensive-room-updatename.php" method="post">
<input type="hidden" name="crid" value="<?php print $crid; ?>" />
<p>Enter the new name for room <?php print $room['number']; ?>: <input type="text" name="name" value="<?php print $room['name']; ?>" /></p>
<p><input type="submit" name="submit" value="Save" /><input type="reset" value="Reset" /><input type="submit" name="submit" value="Cancel" /></p>
</form>

<p><a href="inspection-comprehensive-unit.php?cuid=<?php print $room['cuid']; ?>"><?php print $room['unitdesc']; ?> Main Menu</a></p>

<p><a href="inspection-main.php?iid=<?php print $room['iid']; ?>">Inspection Main Menu</a></p>

<?php
require_once'footer.php';
?>