<?php 

require_once'session.php';
require_once'connect.php';

$cpid = $_POST['cpid'] or $cpid = $_GET['cpid'] or $cpid = 0;
$cid = $_POST['cid'] or $cid = $_GET['cid'] or $cid = 0;

$query = "SELECT *, DATE_FORMAT(classtime, '%c/%e/%Y %l:%i %p') AS classtime FROM class_types INNER JOIN class USING (ctid) INNER JOIN class_parts USING (clid) WHERE cpid=$cpid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/class-view-schedule.php");
	exit();
}

$row = mysql_fetch_assoc($result);

$clid = $row['clid'];

$query = "SELECT firstname, lastname FROM client WHERE cid=$cid";
$clientresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$client = mysql_fetch_assoc($clientresult);

$title = "Add Client to class";
require_once'header.php';
?>
<?php
print "<p>Add $client[firstname] $client[lastname] to the following class?</p>";
?>
<p><b><?php print $row['name']; ?></b><br />Hours: <?php print $row['hours']; ?><br />Cost: <?php print $row['cost']; ?></p>
<?php

$query = "SELECT * FROM class_parts WHERE clid=$clid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while ($row = mysql_fetch_assoc($result)) {
	$query = "SELECT COUNT(*) FROM class_students WHERE cpid=$row[cpid]";
	$result2 = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$enrolled = mysql_result($result2, 0);
	print "<p><b>Part $row[part]</b>: $row[location], $row[classtime], $row[length] hours.<br />Min: $row[minstudents] Rec: $row[recstudents] Max: $row[maxstudents] Enrolled: $enrolled</p>";
}

?>
<form action="class-client-saveall.php" method="post">
<input type="hidden" name="cid" value="<?php print $cid; ?>" />
<input type="hidden" name="clid" value="<?php print $clid; ?>" />
<p><input type="submit" name="submit" value="Yes" /> <input type="submit" name="submit" value="No" /></p>
</form>
<?php
require_once'footer.php';
?>