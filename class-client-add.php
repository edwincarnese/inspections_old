<?php 

require_once'session.php';
require_once'connect.php';

$cpid = $_POST['cpid'] or $cpid = $_GET['cpid'] or $cpid = 0;
$cid = $_POST['cid'] or $cid = $_GET['cid'] or $cid = 0;

$query = "SELECT *, DATE_FORMAT(classtime, '%c/%e/%Y %l:%i %p') AS classtime FROM class_types INNER JOIN class USING (ctid) INNER JOIN class_parts USING (clid) WHERE cpid=$cpid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/class-view-schedule.php");
	exit();
}

$row = mysql_fetch_assoc($result);

if ($row['part'] == 1) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/class-client-addall.php?cpid=$cpid&cid=$cid");
	exit();
}

$clid = $row['clid'];

$query = "SELECT firstname, lastname FROM client WHERE cid=$cid";
$clientresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$client = mysql_fetch_assoc($clientresult);

$title = "Add Client to class";
require_once'header.php';
?>
<?php
print "<p>Add $client[firstname] $client[lastname] to the following class part?</p>";
?>
<p><b><?php print $row['name']; ?>, part <?php print $row['part']; ?></b><br />
<?php
$query = "SELECT COUNT(*) FROM class_students WHERE cpid=$row[cpid]";
$result2 = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$enrolled = mysql_result($result2, 0);
print "$row[location], $row[classtime], $row[length] hours.<br />Min: $row[minstudents] Rec: $row[recstudents] Max: $row[maxstudents] Enrolled: $enrolled";
?>
</p>
<form action="class-client-save.php" method="post">
<input type="hidden" name="cid" value="<?php print $cid; ?>" />
<input type="hidden" name="cpid" value="<?php print $cpid; ?>" />
<p><input type="submit" name="submit" value="Yes" /> <input type="submit" name="submit" value="No" /></p>
</form>
<?php
require_once'footer.php';
?>