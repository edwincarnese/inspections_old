<?php

$pdf->AddPage('P');
$pdf->SetFont('Arial','',14);

//ADDRESS, TOP
$query = "SELECT inspection.iid, streetnum, address, suffix, city, DATE_FORMAT(starttime, '%c/%e/%Y') AS start, unitdesc, number FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);
$y=.2;
//Inspection #
$pdf->SetFont('Times','B','10');
$pdf->SetXY(1.5, $y);
$pdf->Cell(0,0, "Inspection $row[iid]-$row[number]");
//PAGE NUMBERS
$pdf->SetXY(6.75,$y);
$pdf->Cell(0,0, 'Page '. $pdf->PageNo().' of  '. '{totalpages}');

$ml=0.625;
$w=7.25;
$pdf->SetLineWidth(0.01);
//Interior Inspection Banner
$x=2.5 ;
$y=$y+.2;
$pdf->SetFont('Times','B','12');
$pdf->SetXY($ml, $y);
$pdf->Cell($w, 0.3,'Interior Dust', 1, 0, 'C');
$y=$y+.28;

$iid = $row['iid'];

//Unit Specifications
//ADDRESS, TOP////, start, dustlab
$query = "SELECT inspection.iid, streetnum, address, suffix, city, unitdesc, dustLab, starttime, number FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

$pdf->SetFont('Times','','10');
//Unit and address
$x=$ml;
$y=$y+.35;
$pdf->SetXY($x, $y);
$pdf->Cell(0, 0, 'Floor / Apt. No.');
$pdf->SetXY($x, $y);
$pdf->Line($ml+1, $y+0.05, $ml+2.2, $y+0.05);
//$pdf->Rect($x+1.2, $y, 1, 0);
$pdf->SetXY($ml+1.0, $y-0.05);
$pdf->Cell(1.1, 0, $row['unitdesc'], 0, 0, 'C');
$x=3;
$pdf->SetXY($x, $y);
$pdf->Cell($x, 0, 'Street /City:');
//$pdf->SetXY($x, $y);
$pdf->Line($ml+3.25, $y+0.05, $ml+7.25, $y+0.05);
$pdf->SetXY($x+1, $y-0.05);
$pdf->Cell(3.5,0, "$row[streetnum] $row[address] $row[suffix], $row[city]", 0, 0, 'C');

//Sampling method.
$x=$ml;
$y=$y+.25;
$pdf->SetXY($x, $y);
$pdf->Cell($x, 0, 'Sampling Method: (circle one)');
$pdf->SetFont('Times','B','10');
$pdf->SetXY($x+1.875, $y);
$pdf->Cell($x, 0, 'Wet Wipe       Vacuum');
// Rectangle around "Wet Wipe"
$pdf->Rect($x+1.9, $y-0.095, 0.66, 0.17);
// Rectangle around "Vacuum"
//$pdf->Rect($x+2.7, $y-0.095, 0.6, 0.17);
$pdf->SetFont('Times','','10');

//Sampling Date line
$x=$ml;
$y=$y+.3;
$pdf->SetXY($x, $y);
$pdf->Cell($x, 0, 'Sampling Date:');
$pdf->Line($ml+1, $y+0.05, $ml+2.375, $y+0.05);
$pdf->SetXY($x+1.1, $y-0.05);
$pdf->Cell(1.275, 0, $row['starttime'], 0, 0, 'C');
$x=$x+2.875;
$pdf->SetXY($x, $y);
$pdf->Cell(0, 0, 'Laboratory Utilized:');
$pdf->Line($x+1.25, $y+0.05, $ml+$w, $y+0.05);
//$pdf->Cell(3.25,0, $row['dustLab'], 0, 0, 'C');
$pdf->SetXY($x+1.3, $y-0.05);
$pdf->Cell(3.0,0, 'Dust Lab', 0, 0, 'C');


//Sample Grid
$y=$y+.15;
$x=$ml;

$pdf->SetLineWidth(0.01);
$pdf->Line($ml, $y, $ml, $y+6.5);
$x=$ml+1.5;
$pdf->Line($x, $y, $x, $y+6.5);
$x=$ml+2;
$pdf->Line($x, $y, $x, $y+6.5);
$x=$ml+2.875;
$pdf->Line($x, $y, $x, $y+6.5);
$x=$ml+3.5;
$pdf->Line($x, $y, $x, $y+6.5);
$x=$ml+4.375;
$pdf->Line($x, $y, $x, $y+6.5);
$x=$ml+5.125;
$pdf->Line($x, $y, $x, $y+6.5);
$x=$ml+6;
$pdf->Line($x, $y, $x, $y+6.5);
$x=$ml+6.5;
$pdf->Line($x, $y, $x, $y+6.5);
$x=$ml+7.25;
$pdf->Line($x, $y, $x, $y+6.5);


$pdf->Line($ml, $y, $ml+7.25, $y);

//Grid Headings
$pdf->SetXY($ml+.2, $y+.1);
$pdf->Cell(0,0, 'Testing Location');
$pdf->SetXY($ml+.3, $y+.25);
$pdf->SetFont('Times','','8');
$pdf->Cell(0,0, '(Room / Side)');
$pdf->SetXY($ml+.2, $y+.4);
$pdf->SetFont('Times','','10');
$pdf->Cell(0,0, '');

$pdf->SetXY($ml+1.55, $y+.1);
$pdf->Cell(0,0, 'Paint');
$pdf->SetXY($ml+1.5, $y+.25);
$pdf->Cell(0,0, '(Chips)');
$pdf->SetXY($ml+1.55, $y+.4);
$pdf->Cell(0,0, 'Y / N');

$pdf->SetXY($ml+2.125, $y+.1);
$pdf->Cell(0,0, 'Sampled');
$pdf->SetXY($ml+2.225, $y+.25);
$pdf->Cell(0,0, 'Area');
$pdf->SetXY($ml+2.05, $y+.4);
$pdf->Cell(0,0, 'Dimensions');

$pdf->SetXY($ml+3.03, $y+.1);
$pdf->Cell(0,0, 'Lab');
$pdf->SetXY($ml+2.93, $y+.25);
$pdf->Cell(0,0, 'Sample');
$pdf->SetXY($ml+2.9, $y+.4);
$pdf->Cell(0,0, 'Number');

$pdf->SetXY($ml+3.55, $y+.1);
$pdf->Cell(0,0, 'Micrograms/');
$pdf->SetXY($ml+3.75, $y+.25);
$pdf->Cell(0,0, 'Wipe');
$pdf->SetXY($ml+3.7, $y+.4);
$pdf->Cell(0,0, 'Result');

$pdf->SetXY($ml+4.4, $y+.1);
$pdf->Cell(0,0, 'Conversion');
$pdf->SetXY($ml+4.55, $y+.25);
$pdf->Cell(0,0, 'Factor');
$pdf->SetXY($ml+.2, $y+.4);
$pdf->Cell(0,0, '');

$pdf->SetXY($ml+5.18, $y+.1);
$pdf->Cell(0,0, 'Micrograms/');
$pdf->SetXY($ml+5.22, $y+.25);
$pdf->Cell(0,0, 'square foot');
$pdf->SetXY($ml+5.35, $y+.4);
$pdf->Cell(0,0, 'Result');

$pdf->SetXY($ml+6.1, $y+.1);
$pdf->Cell(0,0, 'Spot');
$pdf->SetXY($ml+6.1, $y+.25);
$pdf->Cell(0,0, 'Test');
$pdf->SetXY($ml+6.05, $y+.4);
$pdf->Cell(0,0, '(+ / -)');

$pdf->SetXY($ml+6.625, $y+.1);
$pdf->Cell(0,0, 'Hazard');
$pdf->SetXY($ml+6.5, $y+.25);
$pdf->Cell(0,0, 'Assessment');
$pdf->SetXY($ml+6.6, $y+.4);
$pdf->Cell(0,0, 'F / S / H');

$y=$y+.5;
$c=0;

while ($c < 6.2) { 
	$pdf->Line($ml, $y+$c, $ml+$w, $y+$c);
	$c=$c+.25;
}

//nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn
$x=$ml+.02;
$s=$y;
$pdf->SetFont('Times','B','6');
$pdf->SetXY($x, $s+.06);
$pdf->Cell(0,0, 'Floor');
$pdf->SetXY($x, $s+.15);
$pdf->Cell(0,0, 'Room / Side');
$s=$s+.25;
$pdf->SetFont('Times','B','6');
$pdf->SetXY($x, $s+.06);
$pdf->Cell(0,0, 'Window Sill');
$pdf->SetXY($x, $s+.15);
$pdf->Cell(0,0, 'Room / Side');
$s=$s+.25;
$pdf->SetFont('Times','B','6');
$pdf->SetXY($x, $s+.06);
$pdf->Cell(0,0, 'Carpet / Upholstery');
$pdf->SetXY($x, $s+.15);
$pdf->Cell(0,0, 'Room / Side');
$s=$s+.25;
$pdf->SetFont('Times','B','6');
$pdf->SetXY($x, $s+.06);
$pdf->Cell(0,0, 'Child&acute;s Bedroom');
$pdf->SetXY($x, $s+.15);
$pdf->Cell(0,0, 'Room / Side');
$s=$s+.25;
$pdf->SetFont('Times','B','6');
$pdf->SetXY($x, $s+.06);
$pdf->Cell(0,0, 'Child&acute;s Playroom');
$pdf->SetXY($x, $s+.15);
$pdf->Cell(0,0, 'Room / Side');

$pdf->SetFont('Times','','10');

$y=$y+6.2-0.3;
$pdf->SetXY($ml+0.375, $y);
$pdf->Cell(0, 0, 'Field Blank');

$y=$y+0.2;
$pdf->Rect($ml+.375, $y, 6.5, 0.25);
$pdf->SetFont('Times','B','10');
$y=$y+0.13;
$pdf->SetXY($ml+.375+.5, $y);
$pdf->Cell(0, 0, 'KEY TO HEADINGS: Hazard Assesment :        F                          S                          H');
$pdf->SetFont('Times','','10');
$pdf->SetXY($ml+.375+.5+3, $y);
$pdf->Cell(0, 0, '= Lead-Free        = Lead-Safe           = Hazard');

//Comments
$y=$y+.25;
$x=$ml;

$h=1;
$pdf->SetLineWidth(0.01);
$pdf->Line($ml, $y, $ml, $y+$h);
$pdf->Line($ml+7.25, $y, $ml+7.25, $y+$h);


$rowstable=5;
$hr=$h/$rowstable;
$c=0;

while ($c < ($hr*($rowstable+.9))) { 
	$pdf->Line($ml, $y+$c, $ml+7.25, $y+$c);
	$c=$c+$hr;
}

$y=$y+0.1;
$pdf->SetXY($ml, $y);
$pdf->Cell(0, 0, ' Comments:');

//SAMPLES

$query = "SELECT *, comprehensive_rooms.number AS roomnumber FROM comprehensive_rooms INNER JOIN comprehensive_wipes USING (crid) WHERE cuid=$cuid AND iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) > 25) {
	exit('Too many samples');
}
$pdf->SetFont('Arial','','8');
$basex = $ml; $basey = 3.66; $mody = 0;

while ($row = mysql_fetch_assoc($result) ) {
	formfix($row);
	$pdf->SetXY($basex, $basey + $mody - 0.12);
	$pdf->Cell(0,0, "($row[roomnumber]) $row[name]");
	$pdf->SetXY($basex, $basey + $mody);
	if ($row['side'] == 'None' || $row['side'] == 'Center') {
		$side = $row['side'];
	} else {
		$side = "S$row[side]";
	}
//$ml, 1.5, 2.0, 2.875, 3.5, 4.375, 5.125, 6.0, 6,5, 7.25.
	$pdf->Cell(0,0, "$row[surface] ($side)");
	$pdf->SetXY($basex + 1.5, $basey + $mody);
	$pdf->Cell(0.5,0, $row['paintchips'], 0, 0, 'C');
	$pdf->SetXY($basex + 2.0, $basey + $mody);
	$pdf->Cell(0.875,0, $row['areawidth'].'"x'.$row['arealength'].'"', 0, 0, 'C');
	$pdf->SetXY($basex + 2.875, $basey + $mody);
	$pdf->Cell(0.625,0, "$iid-$unitnumber"."D$row[number]", 0, 0, 'C');
	$pdf->SetXY($basex + 3.5, $basey + $mody);
	$pdf->Cell(0.875,0, $row['results'], 0, 0, 'C');
	$pdf->SetXY($basex + 4.375, $basey + $mody);
	$pdf->Cell(0.75,0, sprintf("%.2f", $row['conversion']), 0, 0, 'C');
	$pdf->SetXY($basex + 5.125, $basey + $mody);
	$pdf->Cell(0.875,0, $row['squarefootresult'], 0, 0, 'C');
	$pdf->SetXY($basex + 6.0, $basey + $mody);
	$pdf->Cell(0.5,0, $row['spottest'], 0, 0, 'C');
	$pdf->SetXY($basex + 6.5, $basey + $mody);
	
	if ($row['hazardassessment'] == 'Lead-free') {
		$pdf->Cell(0.75,0,'F', 0, 0, 'L');
	} else if ($row['hazardassessment'] == 'Lead-safe') {
		$pdf->Cell(0.75,0,'S', 0, 0, 'L');
	} else if ($row['hazardassessment'] == 'Hazard') {
		$pdf->Cell(0.75,0,'H', 0, 0, 'R');
	}
	$mody += 0.25;
}

//COMMON AREA SAMPLES
$query = "SELECT cuid, units.number AS unitnumber, unitdesc FROM units INNER JOIN unit_links ON (units.cuid=unit_links.common) WHERE unit=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while ($row = mysql_fetch_row($result)) {
	formfix($row);
	list($localcuid, $unitnumber, $unitdesc) = $row;
	$pdf->SetXY($basex, $basey + $mody);
	$pdf->SetFont('Arial','B','10');
	$pdf->Cell(0,0, $unitdesc);
	$pdf->SetFont('Arial','','10');
	$mody += 0.25;

	$query = "SELECT *, comprehensive_rooms.number AS roomnumber, units.number AS unitnumber FROM units INNER JOIN comprehensive_rooms USING (cuid) INNER JOIN comprehensive_wipes USING (crid) WHERE comprehensive_rooms.cuid=$localcuid AND units.iid=$iid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

	if (mysql_num_rows($result) > 25) {
		exit('Too many samples');
	}

while ($row = mysql_fetch_assoc($result) ) {
	formfix($row);
	$pdf->SetXY($basex, $basey + $mody - 0.12);
	$pdf->Cell(0,0, "($row[roomnumber]) $row[name]");
	$pdf->SetXY($basex, $basey + $mody);
	if ($row['side'] == 'None' || $row['side'] == 'Center') {
		$side = $row['side'];
	} else {
		$side = "S$row[side]";
	}
//$ml, 1.5, 2.0, 2.875, 3.5, 4.375, 5.125, 6.0, 6,5, 7.25.
	$pdf->Cell(0,0, "$row[surface] ($side)");
	$pdf->SetXY($basex + 1.5, $basey + $mody);
	$pdf->Cell(0.5,0, $row['paintchips'], 0, 0, 'C');
	$pdf->SetXY($basex + 2.0, $basey + $mody);
	$pdf->Cell(0.875,0, $row['areawidth'].'"x'.$row['arealength'].'"', 0, 0, 'C');
	$pdf->SetXY($basex + 2.875, $basey + $mody);
	$pdf->Cell(0.625,0, "$iid-$unitnumber"."D$row[number]", 0, 0, 'C');
	$pdf->SetXY($basex + 3.5, $basey + $mody);
	$pdf->Cell(0.875,0, $row['results'], 0, 0, 'C');
	$pdf->SetXY($basex + 4.375, $basey + $mody);
	$pdf->Cell(0.75,0, sprintf("%.2f", $row['conversion']), 0, 0, 'C');
	$pdf->SetXY($basex + 5.125, $basey + $mody);
	$pdf->Cell(0.875,0, $row['squarefootresult'], 0, 0, 'C');
	$pdf->SetXY($basex + 6.0, $basey + $mody);
	$pdf->Cell(0.5,0, $row['spottest'], 0, 0, 'C');
	$pdf->SetXY($basex + 6.5, $basey + $mody);
	
	if ($row['hazardassessment'] == 'Lead-free') {
		$pdf->Cell(0.75,0,'F', 0, 0, 'L');
	} else if ($row['hazardassessment'] == 'Lead-safe') {
		$pdf->Cell(0.75,0,'S', 0, 0, 'L');
	} else if ($row['hazardassessment'] == 'Hazard') {
		$pdf->Cell(0.75,0,'H', 0, 0, 'R');
	}
	$mody += 0.25;
  }
}
//*/
//FB
$query = "SELECT * FROM comprehensive_wipes WHERE iid=$iid AND number=0";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$basex = $ml; $basey = 8.10; $mody = 0;

$row = mysql_fetch_assoc($result);

$pdf->SetXY($basex + 2.875, $basey + $mody);
$pdf->Cell(0.625, 0, "$iid-*"."D$row[number]", 0, 0, 'C');
$pdf->SetXY($basex + 3.5, $basey + $mody);
$pdf->Cell(0.875, 0, $row['results'], 0, 0, 'C');

$pdf->SetXY($basex + 5.125, $basey + $mody);
$pdf->Cell(0.975, 0, $row['squarefootresult'], 0, 0, 'C');

$pdf->SetXY($basex + 6.5, $basey + $mody);
//$ml, 1.5, 2.0, 2.875, 3.5, 4.375, 5.125, 6.0, 6,5, 7.25.

if ($row['hazardassessment'] == 'Lead-free') {
	$pdf->Cell(0.75,0,'F', 0, 0, 'L');
} else if ($row['hazardassessment'] == 'Lead-safe') {
	$pdf->Cell(0.75,0,'S', 0, 0,'L');
} else if ($row['hazardassessment'] == 'Hazard') {
	$pdf->Cell(075,0,'H', 0, 0, 'R');
}
//*/


//COMMENTS
$query = "SELECT comment FROM comprehensive_comments WHERE cuid=$cuid AND type='dust'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result)) {
	formfix($row);
	$lh = 0.20;
	$pdf->SetXY(0.42, 9.35);
	$pdf->MultiCell(7.72, $lh, mysql_result($result, 0));
}

//date/time at bottom

$query = "SELECT DATE_FORMAT(starttime, '%c/%e/%Y') AS startdate, DATE_FORMAT(starttime, '%l:%i %p') AS starttime FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_row($result);


$pdf->SetXY(6, 10.10);
$pdf->Cell(0,0, '___________  /  ___________ ');
$pdf->SetXY(7.05, 10.10);
$pdf->Cell(0,0, $row[0]);
$pdf->SetXY(6, 10.30);
$pdf->Cell(0,0, '      Initials                 Date ');

$pdf->SetFont('Times','','9');
$pdf->SetXY($ml, 10.60);
$pdf->Cell(0,0, 'FORM PBLC-23-9 (9/97) Replaces OEHRA II (4/97) p.9, which is obsolete.');


?>