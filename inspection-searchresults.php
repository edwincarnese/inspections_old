<?php
$title = "Inspections - Search Results";

require_once 'session.php';
require_once 'connect.php';

$wheres = array();
foreach ($_POST as $field => $value) {
	if ($value) {
		switch ($field) {
			case 'address':
			case 'iid':
				$value = htmlspecialchars($value);
				$wheres[] = "$field LIKE '%$value%'";
				break;
			case 'streetnum':
			case 'suffix':
				$wheres[] = "$field = '$value'";
				break;
			default:
				exit("$field not coded");
		}
	}
}
$wherestring = implode(' AND ', $wheres);

if (!$wherestring) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}

$query = "SELECT DISTINCT inspection.iid, building.bid, address, address2, city, state, zip FROM inspection INNER JOIN building USING (bid) WHERE $wherestring ORDER BY iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 1) {
	$iid = mysql_result($result, 0, 'iid');
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-view.php?iid=$iid");
	exit();
}

require_once 'header.php';

print "<p>";
while ($row = mysql_fetch_row($result) ) {
	$iid = array_shift($row);
	$bid = array_shift($row);
	print "<a href=\"inspection-view.php?iid=$iid\">".$iid."-* ".implode(', ', $row)."</a><br />\n";
}
print "</p>";

?>
<p><a href="index.php">Main Menu</a></p>
<?php
require_once 'footer.php';
?>