<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

$query = "SELECT * FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}
$comp = mysql_fetch_assoc($result);

$iid = $comp['iid'];
$unitnumber = $comp['number'];

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - $comp[unitdesc] - Water";
require_once'header.php';
?>
<form action="inspection-comprehensive-water-savequestions.php" method="post">
<input type="hidden" name="cuid" value="<?php print $cuid; ?>" />
<table>
<tr><td>Is bottled water being used?</td>
<td><input type="radio" name="bottle" value="Yes" <?php if ($comp['bottle'] == 'Yes') { print 'checked="checked"'; } ?> />Yes 
<input type="radio" name="bottle" value="No" <?php if ($comp['bottle'] == 'No') { print 'checked="checked"'; } ?> />No</td>
<td>&nbsp;&nbsp;Source: <input type="text" name="bottlesource" value="<?php print $comp['bottlesource']; ?>" /></td></tr>
<tr><td>Is tap water being used to make infant formula?</td>
<td><input type="radio" name="infant" value="Yes" <?php if ($comp['infant'] == 'Yes') { print 'checked="checked"'; } ?> />Yes 
<input type="radio" name="infant" value="No" <?php if ($comp['infant'] == 'No') { print 'checked="checked"'; } ?> />No</td>
<td>&nbsp;&nbsp;Source: <input type="text" name="infantsource" value="<?php print $comp['infantsource']; ?>" /></td></tr>
<tr><td>Have you ever participated in a water testing program?</td>
<td><input type="radio" name="testing" value="Yes" <?php if ($comp['testing'] == 'Yes') { print 'checked="checked"'; } ?> />Yes 
<input type="radio" name="testing" value="No" <?php if ($comp['testing'] == 'No') { print 'checked="checked"'; } ?> />No</td>
<td>&nbsp;&nbsp;Source: <input type="text" name="testingsource" value="<?php print $comp['testingsource']; ?>" /></td></tr>
<tr><td>Are lead pipes present in the home?</td>
<td><input type="radio" name="leadpipes" value="Yes" <?php if ($comp['leadpipes'] == 'Yes') { print 'checked="checked"'; } ?> />Yes 
<input type="radio" name="leadpipes" value="No" <?php if ($comp['leadpipes'] == 'No') { print 'checked="checked"'; } ?> />No</td>
<td>&nbsp;&nbsp;Source: <input type="text" name="leadpipessource" value="<?php print $comp['leadpipessource']; ?>" /></td></tr>
</table>
<p>Comments:<br /><textarea name="comment" rows="6" cols="50"><?php
$query = "SELECT comment FROM comprehensive_comments WHERE cuid=$cuid AND type='water'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) ) {
	$comment = mysql_result($result, 0);
}
print $comment;
?></textarea></p>
<p><input type="submit" name="submit" value="Save" /> <input type="reset" value="Reset" /> <input type="submit" name="submit" value="Cancel" /></p>
</form>

<p><a href="inspection-comprehensive-unit.php?cuid=<?php print $cuid; ?>"><?php print $comp['unitdesc']; ?> Main Menu</a></p>

<p><a href="inspection-main.php?iid=<?php print $comp[iid]; ?>">Inspection Main Menu</a></p>

<?php
require_once'footer.php';
?>