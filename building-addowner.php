<?php 
require_once'connect.php';
require_once'session.php';

$bid = isset($_POST['bid']) ? $_POST['bid'] : (isset($_GET['bid']) ? $_GET['bid'] : 0);

$bid += 0; // (force numbers)

$title = "Building - add owner";
require_once'header.php';
?>

<p>Search for a client to add as owner to this building.</p>

<form action="building-addownersearchresults.php" method="post">
<input type="hidden" name="bid" value="<?php print $bid; ?>" />
<table>
<tr><td>First name:</td><td><input type="text" name="firstname" maxlength="30" /></td></tr>
<tr><td>Last name:</td><td><input type="text" name="lastname" maxlength="30" /></td></tr>
<tr><td>Company:</td><td><input type="text" name="company" maxlength="75" /></td></tr>
<tr><td>City:</td><td><input type="text" name="city" maxlength="30" /></td></tr>
<tr><td>State:</td><td><input type="text" name="state" maxlength="2" size="3" /></td></tr>
<tr><td>Zip:</td><td><input type="text" name="zip" maxlength="5" size="6" /></td></tr>
<tr><td>Phone:</td><td><input type="text" name="phone1" maxlength="3" size="4" />-<input type="text" name="phone2" maxlength="3" size="4" />-<input type="text" name="phone3" maxlength="4" size="5" /></td></tr>
</table>
<p><input type="submit" value="Search" /> <input type="reset" value="Reset" /></p>
</form>

<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>