<?php 

require_once'session.php';

$title = "Search Inspections";
require_once'header.php';
?>

<p>Enter as much information as is known.</p>

<form action="inspection-searchresults.php" method="post">
<table>
<tr><td>Inspection #:</td><td><input type="text" name="iid" maxlength="8" size="5" />
<tr><td>Address:</td><td><input type="text" name="streetnum" maxlength="8" size="5" /><input type="text" name="address" maxlength="30" /><input type="text" name="suffix" maxlength="10" size="7" /></td></tr>
</table>
<p><input type="submit" value="Search" /> <input type="reset" value="Reset" /></p>
</form>
<p><a href="inspection-list.php">View Inspection List</a></p>
<p><a href="index.php">Main Menu</a></p>

<?php
require_once'footer.php';
?>