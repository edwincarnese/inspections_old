<?php

require_once'session.php';
require_once'connect.php';

$userlevelquery = "SELECT userlevel FROM users WHERE uid=$_SESSION[sid]";
$userlevelresult = mysql_query($userlevelquery) or sql_crapout($userlevelquery.'<br />'.mysql_error());
$userlevel = mysql_result($userlevelresult, 0);

$title = "Inspector Listing";
require_once'header.php';

if (isset($_POST['inactive_tid'])) {
	$val = $_POST['inactive_tid'];
	$query = "UPDATE inspector SET comments='$_POST[inspector_inactive]' WHERE tid=$val";
	mysql_unbuffered_query($query) or sql_crapout($query."<br>".mysql_error());
}
if (isset($_POST['primary_tid'])) {
	$val = $_POST['primary_tid'];
	$query = "UPDATE inspector SET comments='$_POST[inspector_primary]' WHERE tid=$val";
	mysql_unbuffered_query($query) or sql_crapout($query."<br>".mysql_error());
}
if (isset($_POST['secondary_tid'])) {
	$val = $_POST['secondary_tid'];
	$query = "UPDATE inspector SET comments='$_POST[inspector_secondary]' WHERE tid=$val";
	mysql_unbuffered_query($query) or sql_crapout($query."<br>".mysql_error());
}
if (isset($_POST['lead_capable_tid'])) {
	$val = $_POST['lead_capable_tid'];
	$query = "UPDATE inspector SET lead_capable='$_POST[inspector_leadcapable]' WHERE tid=$val";
	mysql_unbuffered_query($query) or sql_crapout($query."<br>".mysql_error());
}
?>

<p><a href="inspector-add.php">Add inspector</a></p>
<table cellspacing="5" border="0">
<tr><th>Name</th><th>Lic.</th><th>Cell Phone</th><th>License Expires</th>
<?php
if ($userlevel >4) {
	print "<th>Inactive</th><th>Primary</th><th>Secondary</th><th>Lead Capable</th></tr>";
	$query = "SELECT inspector.tid, firstname, lastname, eli, elt, comments, DATE_FORMAT(elidate, '%c/%e/%Y') AS elidate, DATE_FORMAT(eltdate, '%c/%e/%Y') AS eltdate, number, lead_capable FROM inspector LEFT JOIN inspector_phone ON (inspector.tid=inspector_phone.tid AND inspector_phone.type = 'Cell') ORDER BY lastname, firstname";
	
} else {
	$query = "SELECT inspector.tid, firstname, lastname, eli, elt, comments, DATE_FORMAT(elidate, '%c/%e/%Y') AS elidate, DATE_FORMAT(eltdate, '%c/%e/%Y') AS eltdate, number FROM inspector LEFT JOIN inspector_phone ON (inspector.tid=inspector_phone.tid AND inspector_phone.type = 'Cell')WHERE comments!='inactive' ORDER BY lastname, firstname";
}
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while ($row = mysql_fetch_assoc($result)) {
	print "<tr><td><a href=\"inspector-view.php?tid=$row[tid]\">$row[lastname], $row[firstname]</a></td><td>";
	if ($row['eli']) {
		print 'ELI';
	} else if ($row['elt']) {
		print 'ELT';
	} else {
		print 'None';
	}
	print "</td><td>$row[number]</td><td>";
	if ($row['eli']) {
		print $row['elidate'];
	} else if ($row['elt']) {
		print $row['eltdate'];
	}
		if ($userlevel > 4) {
		?>
		</td><td>		
		<form name="inactive[<?php print $row['tid'];?>]" method="POST" action="<?php  print $_SERVER['PHP_SELF']; ?>">
		<input type='hidden' value='<?php print $row['tid'];?>' name='inactive_tid' />
		<?php
		print "<input name='inspector_inactive' type='checkbox'";
		if ($row['comments'] == "inactive" ) {
			print " checked='checked' value='' ";
		} else {
			print " value='inactive' ";
		}
		print "onClick='javascript:submit();'/>";
		?></form><?php
		?>
		</td><td>
		<form name="primary[<?php print $row['tid'];?>]" method="POST" action="<?php print $_SERVER['PHP_SELF']; ?>">
		<input type='hidden' value='<?php print $row['tid'];?>' name='primary_tid' />
		<?php
		print "<input name='inspector_primary' type='checkbox'";
		if ($row['comments'] == "primary" ) {
			print " checked='checked' value='' ";
		} else {
			print " value='primary' ";
		}
		print "onClick='javascript:submit();'/>";
		?></form><?php
		?>
		</td><td>
		<form name="secondary[<?php print $row['tid'];?>]" method="POST" action="<?php print $_SERVER['PHP_SELF']; ?>">
		<input type='hidden' value='<?php print $row['tid'];?>' name='secondary_tid' />
		<?php
		print "<input name='inspector_secondary' type='checkbox'";
		if ($row['comments'] == "secondary" ) {
			print " checked='checked' value='' ";
		} else {
			print " value='secondary' ";
		}
		print "onClick='javascript:submit();'/></td>";
		?></form><?php
		?>
		</td><td>
		<form name="lead_capable[<?php print $row['tid'];?>]" method="POST" action="<?php print $_SERVER['PHP_SELF']; ?>">
		<input type='hidden' value='<?php print $row['tid'];?>' name='lead_capable_tid' />
		<?php
		print "<input name='inspector_leadcapable' type='checkbox'";
		if ($row['lead_capable'] == "yes" ) {
			print " checked='checked' value='' ";
		} else {
			print " value='yes' ";
		}
		print "onClick='javascript:submit();'/></td></tr>";
		?></form><?php
	}
}
?>
</table>

<?php
require_once'footer.php';
?>