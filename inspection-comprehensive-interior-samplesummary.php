<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

$query = "SELECT iid, starttime, unitdesc, number FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}

list($iid, $starttime, $unitdesc, $unitnumber) = mysql_fetch_row($result);

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - $unitdesc";
require_once'header.php';
?>
<!-- ***************************** WATER **************************** -->

<hr />
<p>Water Samples:&nbsp;&nbsp;&nbsp;<a href="inspection-comprehensive-water-sample.php?cuid=<?php print $cuid; ?>">Add Water Sample</a></p>
<?php
$query = "SELECT * FROM waters WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result)) {
?>
<table class="info">
<tr><th>Lab Number</th><th>Fixture</th><th>Type</th><th>Brass Fixture</th><th>New Plumbing</th><th>Edit</th></tr>
<?php
	while($row = mysql_fetch_assoc($result) ) {
		print "<tr><td>$iid-$unitnumber"."W$row[number]</td><td>$row[fixture]</td><td>$row[drawflush]</td><td>$row[brass]</td><td>$row[newplumbing]</td><td><a href=\"inspection-comprehensive-water-sample.php?waterid=$row[iid]W$row[number]\">Edit</a></tr>\n";
	}
?>
</table>
<?php
} else {
	print "<p><i>No Water Samples</i></p>";
}
?>
<p>Water Comments:&nbsp;&nbsp;&nbsp;<a href="inspection-comprehensive-water-questions.php?cuid=<?php print $cuid; ?>">Edit</a></p>
<?php
$query = "SELECT comment FROM comprehensive_comments WHERE cuid=$cuid AND type='water'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) ) {
	$comment = mysql_result($result, 0);
}
print $comment;
?>
<hr />
<!-- ************************ DUST WIPES ******************************** -->
<p>Dust Wipes:&nbsp;&nbsp;&nbsp;<a href="inspection-comprehensive-wipe.php?cuid=<?php print $cuid; ?>">Add Wipe</a></p>
<?php
$query = "SELECT CONCAT(iid, '-', comprehensive_wipes.number) AS wid, comprehensive_wipes.number AS wipenumber, comprehensive_rooms.number, comprehensive_rooms.name, comprehensive_wipes.surface, side, arealength, areawidth FROM comprehensive_wipes INNER JOIN comprehensive_rooms USING (crid) WHERE cuid=$cuid AND comprehensive_wipes.iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result)) {
?>
<table class="info">
<tr><th>Sample #</th><th>Room #</th><th>Room Name</th><th>Side</th><th>Surface</th><th>Width</th><th>Height</th><th>Edit</th></tr>
<?php
	while ($row = mysql_fetch_assoc($result)) {
		print "<tr><td>$iid-$unitnumber"."D$row[wipenumber]</td><td>$row[number]</td><td>$row[name]</td><td>$row[side]</td><td>$row[surface]</td><td>$row[areawidth]&quot;</td><td>$row[arealength]&quot;</td><td><a href=\"inspection-comprehensive-wipe.php?cuid=$cuid&amp;wid=$row[wid]\">Edit</a></td></tr>\n";
	}
?>
</table>
<?php
} else {
	print "<p><i>No Dust Wipes</i></p>";
}
?>
<p>Dust Comments:&nbsp;&nbsp;&nbsp;<a href="inspection-comprehensive-wipe-comments.php?cuid=<?php print $cuid; ?>">Edit</a><br />
<!--<textarea name="comment" rows="6" cols="50"> --><?php
$query = "SELECT comment FROM comprehensive_comments WHERE cuid=$cuid AND type='dust'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) ) {
	$comment = mysql_result($result, 0);
}
print $comment;
?><!-- </textarea> --></p>
<hr />
<!-- *************************** PAINT CHIPS ************************* -->

<p>Paint Chip Samples:</p>
<table class="info">
<tr><th>Room Number</th><th>Room Name</th><th>Component</th><th>Side</th><th>Substrate</th><th>Sample Number</th></tr>
<?php
$query = "SELECT comprehensive_components.ccid, comprehensive_rooms.number, comprehensive_rooms.name AS roomname, comprehensive_components.name AS compname, comprehensive_components.side AS side, substrates.name AS substrate, paintchips.number AS chipnumber FROM comprehensive_rooms INNER JOIN comprehensive_components USING (crid) INNER JOIN substrates USING (sid) INNER JOIN paintchips ON (comprehensive_components.ccid=paintchips.ccid) WHERE cuid=$cuid AND paintchips.comptype='Interior' ORDER BY number, ccid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while($row = mysql_fetch_assoc($result)) {
//	print_r($row);
	print "<tr><td>$row[number]</td><td>$row[roomname]</td><td>$row[compname]</td><td>$row[side]</td><td>$row[substrate]</td><td>$iid-$unitnumber"."P$row[chipnumber]</td></tr>";
}
?>
</table>

<hr />

<a href="inspection-comprehensive-xrfsummary.php?cuid=<?php print $cuid;?>">XRF Summary</a></p>

<p><a href="inspection-comprehensive-unit.php?cuid=<?php print $cuid; ?>"><?php print $unitdesc; ?> Main Menu</a></p>

<p><a href="inspection-main.php?iid=<?php print $iid; ?>">Inspection Main Menu</a></p>
<?php
require_once'footer.php';
?>