<?php

//This is the list of pages that will be stored in the history
$STORED_PAGES_ARRAY = array('client-view.php', 'inspection-view.php', 'inspector-view.php', 'building-view.php');
$MAX_RECORDS = 15;

include_once('connect.php');
if (isset($_SESSION['sid'])) {
	$userid = $_SESSION["sid"];

	$tmp =$_SERVER['PHP_SELF'];
	$self = substr($tmp, strrpos($tmp,'/')+1);

	$storeflag = false;
	foreach ($STORED_PAGES_ARRAY as $val) {
		if (!strncasecmp($self, $val, strlen($val))) {
			$storeflag = true;
		}
	}

	//Storage
	if ($storeflag) {
		foreach ($_GET as $key => $i) {
	 		switch ($key) {
				case 'iid':
				    $pagetype = "inspection";
					$pageid = $i;
				    break;
				case 'cid':
				    $pagetype = "client";
				    $pageid = $i;
				    break;
				case 'tid':
				    $pagetype = "inspector";
				    $pageid = $i;
					break;
				case 'bid':
				    $pagetype = "building";
				    $pageid = $i;
				    break;
				default:
				    $pagetype = "unknown";
				    $pageid = $i;
			}
		}
		if ($pagetype != "unknown") {
			$historyquery = "SELECT * FROM history WHERE uid=$userid ORDER BY date";
			$historyresult = mysql_query($historyquery) or sql_crapout($historyquery.mysql_error());
	
			while ($val = mysql_fetch_assoc($historyresult)) {
				if ($val['pid'] == $pageid && $val['type'] == $pagetype) {
					$deletequery2 = "DELETE FROM history WHERE pid=".$val["pid"]." AND type='".$val["type"]."'";
					mysql_unbuffered_query($deletequery2) or sql_crapout($deletequery2.mysql_error());
				}
			}
	
			if (mysql_num_rows($historyresult)>=$MAX_RECORDS) {
			    if (mysql_data_seek($historyresult, 0)) {
				    $userRecord = mysql_fetch_assoc($historyresult);
				    $deletequery = "DELETE FROM history WHERE shid=".$userRecord['shid'];
					mysql_unbuffered_query($deletequery) or sql_crapout($userRecord['shid']." ".$deletequery.mysql_error());
			    }
			}
			
			$query = "INSERT INTO history (uid, type, pid) VALUES ($userid, '$pagetype', $pageid)";
			mysql_unbuffered_query($query) or sql_crapout($query.mysql_error());
		}

	}


	//Display
	$query = "SELECT * FROM history where uid=$userid ORDER BY date DESC LIMIT 10 ";
	$result = mysql_query($query) or sql_crapout($query.mysql_error());
	echo '<table class="table table-bordered datatables"  border="0" cellspacing="1" cellpadding="2" style="color:#b0bb0b">';
	echo "<thead><th>Report</th></thead>";
	echo "<tbody>";
	while ($val = mysql_fetch_assoc($result)) {
		switch ($val['type']) {
		    case 'client':
		        $id_type = 'cid';
		        $namequery = 'SELECT firstname, lastname FROM client WHERE cid='.$val['pid'];
		        $nameresult = mysql_query($namequery) or sql_crapout($namequery.mysql_error());
		        $temp = mysql_fetch_assoc($nameresult);
		        $title = $temp['lastname'].", ".$temp['firstname'];
		        break;
			case 'inspection':
			    $id_type = 'iid';
			    $namequery = 'SELECT building.streetnum, building.address, building.address2, building.city, building.state FROM building, inspection WHERE iid='.$val['pid'].' AND building.bid=inspection.bid';
		        $nameresult = mysql_query($namequery) or sql_crapout($namequery.mysql_error());
		        $temp = mysql_fetch_assoc($nameresult);
		        $title = $temp['streetnum']." ".$temp['address']." ";
		        if ($temp['address2'] != '') {
				  $title .= $temp['address2']." ";
				}
				$title .= "<br>".$temp['city'].", ".$temp['state'];
			    break;
			case 'inspector':
			    $id_type = 'tid';
			    $namequery = 'SELECT firstname, lastname FROM inspector WHERE tid='.$val['pid'];
		        $nameresult = mysql_query($namequery) or sql_crapout($namequery.mysql_error());
		        $temp = mysql_fetch_assoc($nameresult);
		        $title = $temp['lastname'].", ".$temp['firstname'];
			    break;
			case 'building':
				$id_type = 'bid';
				$namequery = 'SELECT streetnum, address, address2, city, state FROM building WHERE bid='.$val['pid'];
		        $nameresult = mysql_query($namequery) or sql_crapout($namequery.mysql_error());
		        $temp = mysql_fetch_assoc($nameresult);
		        $title = $temp['streetnum']." ".$temp['address']." ";
		        if ($temp['address2'] != '') {
				  $title .= $temp['address2']." ";
				}
				$title .= "<br>".$temp['city'].", ".$temp['state'];
				break;
			default:
			   // exit("no type set");
		}
		
		
		echo "<tr class=\"box2\"><td><a href=\"http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/".$val['type']."-view.php?$id_type=".$val['pid']."\">".$title."<br><text>".$val['type']."</text>";
		if ($id_type == 'iid') {
			echo " <text >".$val['pid']."</text>";
		}
		echo "</td></a></tr>";

		
	}
}
?>
<tbody>
</table>
