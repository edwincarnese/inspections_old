<input type="hidden" name="ccid" value="<?php print $ccid; ?>" />
<!-- script used by >9.9, second can be used by no paint and post 77 if needed -->
<script type="text/javascript">
function unchecktenths() {
	var name; var i;
	for (i=0; i<=9; i++) {
		name = "xrf2" + i;
		document.getElementById(name).checked = false;
	}
}

function uncheckones() { //currently not used
	var name; var i;
	for (i=0; i<=9; i++) {
		name = "xrf" + i;
		document.getElementById(name).checked = false;
	}
	document.getElementById("xrfa").checked = false;
	document.getElementById("xrfx").checked = false;
	document.getElementById("xrfy").checked = false;
	document.getElementById("xrfz").checked = false;
}

function uncheckspecials() { //unchecks 'None', '>9.9', and 'AP '; used on tenths click
	document.getElementById("xrfa").checked = false;
	document.getElementById("xrfy").checked = false;
	document.getElementById("xrfz").checked = false;
}
</script>
<table class="straightup">
<tr><th>Description</th><th>Side</th><th>Substrate</th><th>Condition</th><th>XRF</th><th>Submit</th><th>Actions</th></tr>
<tr>
<?php
print "<td style=\"width: 100px\">$comp[name]</td>";
//side
print '<td>';
//$sides = array('None','1','2','3','4','Center');
$sides = array('A','B','C','D');
$count = 0;
foreach ($sides as $side) {
$letterSide = $comp['side'];
	if ($letterSide[0] == $side || $letterSide[1] == $side || $letterSide[2] == $side || $letterSide[3] == $side) {
		print "<input type=\"checkbox\" name=\"side-$count\" value=\"$side\" checked=\"checked\" /><span class=\"current\">$side</span><br />\n";
	} else {
		print "<input type=\"checkbox\" name=\"side-$count\" value=\"$side\" />$side<br />\n";
	}
$count++;
}
print '</td>';
//substrate
print '<td>';
$query = "SELECT sid, name FROM substrates";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while ($row = mysql_fetch_row($result) ) {
	if ($comp['sid'] == $row[0]) {
		print "<input type=\"radio\" name=\"sid\" value=\"$row[0]\" checked=\"checked\" /><span class=\"current\">$row[1]</span><br />\n";
	} else {
		print "<input type=\"radio\" name=\"sid\" value=\"$row[0]\" />$row[1]<br />\n";
	}
}
print '</td>';
//condition
print '<td>';
$conditions = array('Intact', 'Intact Where Visible', 'Damaged Touchup', 'Damaged', 'Assumed Damaged', 'Damaged - Friction', 'Covered', 'Intact - Factory Finish', 'Door Missing', 'No Paint', 'Post \'77');
foreach ($conditions as $condition) {
	if ($comp['hazards'] == $condition) {
		print "<input type=\"radio\" name=\"condition\" value=\"$condition\" checked=\"checked\" /><span class=\"current\">$condition</span><br />";
	} else {
		print "<input type=\"radio\" name=\"condition\" value=\"$condition\" />$condition<br />";
	}
}
//following treat xrf special 
/* REMOVE REDUNDANT
$conditions = array('No Paint', 'Post \'77');
foreach ($conditions as $condition) {
	if ($comp['condition'] == $condition) {
		print "<input type=\"radio\" name=\"condition\" value=\"$condition\" checked=\"checked\" /><span class=\"current\">$condition</span><br />";
	} else {
		print "<input type=\"radio\" name=\"condition\" value=\"$condition\" />$condition<br />";
	}
}*/
print '</td>';

//XRF
if ($skipXRF) {
	$disablestring = ' disabled="disabled" ';
} else {
	$disablestring = '';
}

print '<td><table>';
if ($comp['xrf']) {
	list($first, $second) = explode('.', $comp['xrf']);
} else {
	$first = $second = '   ';
}
if ($skipXRF) {
	print "<input type=\"hidden\" name=\"xrf\" value=\"$first\" />";
}
print "<!-- $first -->";
if ($first == '   ') {
	//spaces necessary in values; save script assumes concat if < 3 characters
	print "<tr><td colspan=\"2\"><input type=\"radio\" id=\"xrfa\"  name=\"xrf\" value=\"   \" checked=\"checked\" onclick=\"unchecktenths()\" $disablestring /><span class=\"current\">None</span></td></tr>";
} else {
	print "<tr><td colspan=\"2\"><input type=\"radio\" id=\"xrfa\"  name=\"xrf\" value=\"   \" onclick=\"unchecktenths()\" $disablestring />None</td></tr>";
}
if ($first == '-0') {
	print "<tr><td colspan=\"2\"><input type=\"radio\" id=\"xrfx\"  name=\"xrf\" value=\"-0\" checked=\"checked\" $disablestring /><span class=\"current\">-0</span></td></tr>";
} else {
	print "<tr><td colspan=\"2\"><input type=\"radio\" id=\"xrfx\"  name=\"xrf\" value=\"-0\" $disablestring />-0</td></tr>";
}
for($x=0; $x<=9; $x++) {
	if ($first == "$x" && $first != '-0') { // 0 and -0 get confused; hence second half
		print "<tr><td><input type=\"radio\" id=\"xrf$x\" name=\"xrf\" value=\"$x\" checked=\"checked\" $disablestring /><span class=\"current\">$x</span></td>";
	} else {
		print "<tr><td><input type=\"radio\" id=\"xrf$x\"  name=\"xrf\" value=\"$x\" $disablestring />$x</td>";
	}
	if ($second == "$x" && $first != '>9') {
		print "<td><input type=\"radio\" id=\"xrf2$x\" name=\"xrf2\" value=\"$x\" onclick=\"uncheckspecials()\" checked=\"checked\" $disablestring /><span class=\"current\">.$x</span></td></tr>";
	} else {
		print "<td><input type=\"radio\" id=\"xrf2$x\" name=\"xrf2\" value=\"$x\" onclick=\"uncheckspecials()\" $disablestring />.$x</td></tr>";
	}
}
if ($first == '>9') {
	print "<tr><td colspan=\"2\"><input type=\"radio\" id=\"xrfy\"  name=\"xrf\" value=\"&gt;9.9\" onclick=\"unchecktenths()\" checked=\"checked\" $disablestring /><span class=\"current\">&gt;9.9</span></td></tr>";
} else {
	print "<tr><td colspan=\"2\"><input type=\"radio\" id=\"xrfy\"  name=\"xrf\" value=\"&gt;9.9\" onclick=\"unchecktenths()\" $disablestring />&gt;9.9</td></tr>";
}
if ($comp['xrf'] == 'AP') {
	//spaces necessary in values; save script assumes concat if < 3 characters
	print "<tr><td colspan=\"2\"><input type=\"radio\" id=\"xrfz\"  name=\"xrf\" value=\"AP \" onclick=\"unchecktenths()\" checked=\"checked\" $disablestring /><span class=\"current\">AP</span></td></tr>";
} else {
	print "<tr><td colspan=\"2\"><input type=\"radio\" id=\"xrfz\"  name=\"xrf\" value=\"AP \" onclick=\"unchecktenths()\" $disablestring />AP</td></tr>";
}
print '</table></td>';
//assessment - deprecated (header removed from table above)
/*
print '<td>';
$assessments = array('Lead-free', 'Lead-safe', 'Hazard');
foreach ($assessments as $asmt) {
	if ($comp['hazardassessment'] == $asmt) {
		print "<input type=\"radio\" name=\"hazardassessment\" value=\"$asmt\" checked=\"checked\" /><span class=\"current\">$asmt</span><br />";
	} else {
		print "<input type=\"radio\" name=\"hazardassessment\" value=\"$asmt\" />$asmt<br />";
	}
}
print '</td>';
*/
?>
<td>
<input type="submit" name="submit" value="Save" /><br /><br /><input type="reset" value="Reset" /><br /><br /><input type="submit" name="submit" value="Cancel" /><br /><br /><br /><br /><input type="submit" name="submit" value="Delete" />
</td>
<td>
<?php
//$testops = array('+','-');
$testops = array('Component does not exist','Capped','Enclosed','Encapsulated','Paint Stabilization','Prepared for ENC','Vinyl/Metal Rep Window','Storm Frame Removed','Scraped','Dipped','Removed','Replaced','Reversed INT Intact');
foreach ($testops as $op) {
	if ($comp['spottest'] == $op) {
		print "<input type=\"radio\" name=\"spottest\" value=\"$op\" checked=\"checked\" /><span class=\"current\">$op</span><br />";
	} else {
		print "<input type=\"radio\" name=\"spottest\" value=\"$op\" />$op<br />";
	}
}
if (!$comp['spottest']) {
	print "<input type=\"radio\" name=\"spottest\" value=\"\" checked=\"checked\" /><span class=\"current\">None</span><br />";
} else {
	print "<input type=\"radio\" name=\"spottest\" value=\"\" />None<br />";
}
?>
<hr />
<b>Paint Sample</b>
<hr />
<?
//$type set in calling script
$query = "SELECT COUNT(*) FROM paintchips WHERE ccid=$ccid and comptype='$type'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_result($result, 0) > 0) {
?>
<input type="radio" name="paintchips" value="Yes" checked="checked" /><span class="current">Yes</span><br />
<input type="radio" name="paintchips" value="No" />No<br />
<?php
} else {
?>
<input type="radio" name="paintchips" value="Yes" />Yes<br />
<input type="radio" name="paintchips" value="No" checked="checked" /><span class="current">No</span><br />
<?php
}
?>
</td>
</tr>
</table>

<p>Current values are in <span class="current">this color</span>.</p>

<?php
if ($skipXRF) {

 print '<p>XRF reading are being skipped. Before taking XRF readings, they must be re-enabled. <input type="submit" name="submit" value="Enable XRF Readings" /></p>'; 

} else {
	print "<p>Next XRF calibration due by: $shownexttime<br />Current time: $shownowtime</p>";
}