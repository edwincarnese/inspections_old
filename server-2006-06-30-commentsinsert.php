<?php

require_once'connect.php';

$query = "INSERT INTO `comments` (`commentText` , `commentDefault` ) VALUES ('Owner states that the basement and attic are lockable, not part of the finished living space, and are inaccessible by kids. They are therefore beyond the scope of this inspection', 'checked')  ";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$query = "INSERT INTO `comments` (`commentText` , `commentDefault` ) VALUES ('According to the Lead Hazard Mitigation Act, common areas, hallways, and exterior areas owned by the condominium association are not required to be inspected to issue a Certificate of Conformance for a condominium. They are therefore beyond the scope of this inspection', 'checked')  ";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$query = "INSERT INTO `comments` (`commentText` , `commentDefault` ) VALUES ('All coated surfaces are assumed to contain lead-based paint unless otherwise indicated as lead free in this report', '')  ";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$query = "INSERT INTO `comments` (`commentText` , `commentDefault` ) VALUES ('Dust wipes are deferred until repairs are complete. Owner is credited for one set of dust wipes.', '')  ";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$query = "INSERT INTO `comments` (`commentText` , `commentDefault` ) VALUES ('Dustwipes were passed on the initial inspection.', '')  ";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());


?>