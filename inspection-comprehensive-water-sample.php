<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;
$waterid = $_POST['waterid'] or $waterid = $_GET['waterid'] or $waterid = 0;

if ($waterid) {
	list($iid, $number) = explode('W', $waterid);
	$query = "SELECT * FROM waters WHERE iid=$iid AND number=$number";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	if (mysql_num_rows($result)) {
		$water = mysql_fetch_assoc($result);
		$cuid = $water['cuid'];
	}
} else {
	$water['fixture'] = 'Kitchen';
	$water['drawflush'] = 'Flush';
	$water['brass'] = 'Yes';
}

$query = "SELECT * FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}
$comp = mysql_fetch_assoc($result);

$iid = $comp['iid'];
$unitnumber = $comp['number'];

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - $comp[unitdesc] - Water Sample";
require_once'header.php';
?>
<form action="inspection-comprehensive-water-savesample.php" method="post">
<input type="hidden" name="cuid" value="<?php print $cuid; ?>" />
<input type="hidden" name="iid" value="<?php print $comp['iid']; ?>" />
<?php if ($number) { ?>
<input type="hidden" name="number" value="<?php print $number; ?>" />
<?php } ?>
<table class="straightup">
<tr><th>Fixture</th><th>Type</th><th>Brass Fixture</th><th>New Plumbing</th></tr>
<tr>
<td><input type="text" name="fixture" maxlength="30" value="<?php print $water['fixture']; ?>" /></td>
<td>
<?php
$drawflushs = array('Flush','Draw');
foreach ($drawflushs as $drawflush) {
	if ($water['drawflush'] == $drawflush) {
		print "<input type=\"radio\" name=\"drawflush\" value=\"$drawflush\" checked=\"checked\" />$drawflush<br />\n";
	} else {
		print "<input type=\"radio\" name=\"drawflush\" value=\"$drawflush\" />$drawflush<br />\n";
	}
}
?>
</td>
<td>
<?php
$brasss = array('Yes','No');
foreach ($brasss as $brass) {
	if ($water['brass'] == $brass) {
		print "<input type=\"radio\" name=\"brass\" value=\"$brass\" checked=\"checked\" />$brass<br />\n";
	} else {
		print "<input type=\"radio\" name=\"brass\" value=\"$brass\" />$brass<br />\n";
	}
}
?>
<td>
<?php
$newplumbings = array('Yes','No');
foreach ($newplumbings as $newplumbing) {
	if ($water['newplumbing'] == $newplumbing) {
		print "<input type=\"radio\" name=\"newplumbing\" value=\"$newplumbing\" checked=\"checked\" />$newplumbing<br />\n";
	} else {
		print "<input type=\"radio\" name=\"newplumbing\" value=\"$newplumbing\" />$newplumbing<br />\n";
	}
}
?>
</tr>
</table>
<p><input type="submit" name="submit" value="Save" /><input type="reset" value="Reset" /><input type="submit" name="submit" value="Cancel" /></p>
</form>

<p><a href="inspection-comprehensive-unit.php?cuid=<?php print $cuid; ?>"><?php print $comp['unitdesc']; ?> Main Menu</a></p>

<p><a href="inspection-main.php?iid=<?php print $comp[iid]; ?>">Inspection Main Menu</a></p>

<?php
require_once'footer.php';
?>