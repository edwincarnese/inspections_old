<?php

$pdf->AddPage('L');
$pdf->Image('images/fixed/chainofcustody.png', 0, 0, 11.0, 8.5, 'PNG');

$lh = 0.1; // line height


$pdf->SetFont('Arial','',6);
$pdf->SetXY(6.76, 2.40); //Lead
$pdf->Cell(0,0, 'X');

$query = "SELECT streetnum, address, suffix, city, state, inspection.iid, unitdesc, DATE_FORMAT(units.starttime, '%c/%e/%Y %l%p') AS start, units.number AS unitnumber FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);

$lh = 0.4;
$pdf->SetFont('Arial','','10');
$pdf->SetXY(1.60, 1.23);
$pdf->Cell(0,0, "$row[streetnum] $row[address] $row[suffix], $row[unitdesc]");
$pdf->SetXY(1.85, 1.44);
$pdf->Cell(0,0, "$row[city]");
$pdf->SetXY(2.18, 1.84);
$pdf->Cell(0,0, "$row[iid]-$row[unitnumber]$sample"); //$sample set by calling script; abbreviation for sample type (D, W, S, or P)
$pdf->SetXY(6.50, 1.78); //state where sample collected
$pdf->Cell(0,0, "$row[state]");
$iid = $row['iid'];
$date = $row['start'];
$unitnumber = $row['unitnumber'];

//Standard or Next Day
$TATquery="select coc_tat from inspection where iid=$iid";
$TATresult = mysql_query($TATquery) or sql_crapout($TATquery.'<br />'.mysql_error());
$TATrow = mysql_fetch_assoc($TATresult);


if ($TATrow['coc_tat']=='X'){
$pdf->SetXY(0.85, 2.25); //EXPRESS
$pdf->Cell(0,0, 'X');
}
else{
$pdf->SetXY(0.85, 2.88); //STANDARD
$pdf->Cell(0,0, 'X');
}

$query = "SELECT number, SUBSTRING(client_phone.type, 1, 1) FROM inspection INNER JOIN owners USING (bid) INNER JOIN client_phone USING (cid) WHERE inspection.iid=$iid LIMIT 1";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if ($row = mysql_fetch_row($result)) {
	$pdf->SetXY(1.85, 1.65);
	$pdf->Cell(0,0, "$row[1]$row[0]");
}

$query = "SELECT firstname, lastname, email FROM insp_assigned INNER JOIN inspector USING (tid) WHERE iid=$iid ORDER BY job LIMIT 1";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);

$pdf->SetXY(3.93, 1.33);
$lh = 0.2;
$pdf->SetFont('Arial','','9');
$pdf->MultiCell(3.9, $lh, "Please fax results and email results to leadsafe@rileadtechs.com and $row[email].");

$pdf->SetXY(2.12, 7.16);
$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
$pdf->SetXY(6.78, 7.16);
$pdf->Cell(0,0, $date);
?>