<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;
$crid = $_POST['crid'] or $crid = $_GET['crid'] or $crid = 0;

$query = "SELECT iid, starttime, unitdesc, number, diagrams FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}

list($iid, $starttime, $unitdesc, $unitnumber, $diagrams) = mysql_fetch_row($result);

$addressquery = "SELECT address FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$query = "SELECT * FROM comprehensive_rooms WHERE cuid=$cuid ORDER BY number";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	//switch to setup page
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-comprehensive-interior-setup1.php?cuid=$cuid");
	exit();
}

$title = "$iid-$unitnumber - $address - $unitdesc";
require_once'header.php';
?>
<p>Are you sure you want to delete 
<?php
$query = "SELECT * FROM comprehensive_rooms WHERE crid=$crid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
print "$row[name] (Room $row[number])? It contains ";

$query = "SELECT COUNT(*) FROM comprehensive_components WHERE crid=$crid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
print mysql_result($result, 0)." components, ";

$query = "SELECT COUNT(*) FROM comprehensive_wipes WHERE crid=$crid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
print mysql_result($result, 0)." wipes, and ";

$query = "SELECT COUNT(paintchips) FROM paintchips INNER JOIN comprehensive_components USING (ccid) WHERE crid=$crid AND comptype='Interior'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
print mysql_result($result, 0)." paint chip samples.";

?>
</p>
<form action="inspection-comprehensive-deleteroom.php" method="post">
<input type="hidden" name="crid" value="<?php print $crid; ?>" />
<input type="submit" name="submit" value="Delete" /> <input type="submit" name="submit" value="Cancel" />
<?php
require_once'footer.php';
?>