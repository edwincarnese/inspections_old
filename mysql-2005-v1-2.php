<?php

$query = "ALTER TABLE  `inspection` CHANGE  `type`  `type` ENUM(  'Conformance',  'Comprehensive',  'Conformance - Clearance',  'Comprehensive - Clearance',  'Pre-Inspection Walkthrough', 'Post-Inspection Walkthrough',  'Limited' ) DEFAULT  'Conformance' NOT NULL";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "CREATE TABLE  `users` (
 `uid` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
 `tid` SMALLINT UNSIGNED,
 `username` VARCHAR( 16 ) NOT NULL ,
 `password` VARCHAR( 16 ) NOT NULL ,
PRIMARY KEY (  `uid` )
)";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "INSERT INTO users (tid, username, password) SELECT tid, LOWER(firstname), LOWER(lastname) FROM inspector;";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `comprehensive_components` CHANGE  `side`  `side` ENUM(  'None',  '1',  '2',  '3',  '4',  'Center' )";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `comprehensive_ext_components` CHANGE  `side`  `side` ENUM(  'None',  '1',  '2',  '3',  '4',  'Center' )";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `client` ADD  `mailingnum` VARCHAR( 8 ) NOT NULL AFTER  `zip` ,
ADD  `mailingaddress` VARCHAR( 30 ) NOT NULL AFTER  `mailingnum` ,
ADD  `mailingsuffix` VARCHAR( 10 ) NOT NULL AFTER  `mailingaddress`";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `client` ADD  `nickname` VARCHAR( 30 ) NOT NULL AFTER  `lastname`";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `client` ADD  `title` VARCHAR( 6 ) NOT NULL AFTER  `cid`";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `client` ADD  `mi` VARCHAR( 1 ) NOT NULL AFTER  `firstname`";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `client` ADD  `companytitle` VARCHAR( 75 ) NOT NULL AFTER  `company`";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `client` ADD  `balancedue` VARCHAR( 12 ) NOT NULL AFTER  `ownedunits` ,
ADD  `agent` VARCHAR( 30 ) NOT NULL AFTER  `balancedue` ,
ADD  `licensenumber` VARCHAR( 15 ) NOT NULL AFTER  `agent`";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `owners` ADD  `relationship` ENUM(  'Owner' ) NOT NULL AFTER  `bid`";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `units` CHANGE  `diagrams`  `diagrams` TINYINT( 3 ) UNSIGNED DEFAULT  '1' NOT NULL";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `client` ADD  `mailingaddress2` VARCHAR( 30 ) NOT NULL AFTER  `mailingsuffix` ";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `users` ADD  `firstname` VARCHAR( 30 ) NOT NULL AFTER  `tid` ,
ADD  `lastname` VARCHAR( 30 ) NOT NULL AFTER  `firstname` ,
ADD  `initials` VARCHAR( 5 ) NOT NULL AFTER  `lastname` ";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `inspection` ADD  `created` DATETIME NOT NULL AFTER  `bid`";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `inspection` ADD  `signed` DATETIME NOT NULL ,
ADD  `printed` DATETIME NOT NULL ,
ADD  `mailed` DATETIME NOT NULL ,
ADD  `cocprinted` DATETIME NOT NULL";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "CREATE TABLE  `class` (
 `clid` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
 `ctid` TINYINT UNSIGNED NOT NULL ,
PRIMARY KEY (  `clid` )
)";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "CREATE TABLE `class_parts` (
  `cpid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `clid` smallint(5) unsigned NOT NULL default '0',
  `part` tinyint(3) unsigned NOT NULL default '0',
  `minstudents` tinyint(3) unsigned NOT NULL default '0',
  `recstudents` tinyint(3) unsigned NOT NULL default '0',
  `maxstudents` tinyint(3) unsigned NOT NULL default '0',
  `length` tinyint(3) unsigned NOT NULL default '0',
  `location` varchar(50) NOT NULL default '',
  `classtime` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`cpid`)
)";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "CREATE TABLE  `class_types` (
 `ctid` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
 `name` VARCHAR( 50 ) NOT NULL ,
 `hours` TINYINT UNSIGNED NOT NULL ,
 `cost` DECIMAL( 5, 2 ) NOT NULL ,
 `qualifications` VARCHAR( 50 ) NOT NULL ,
 `parts` TINYINT UNSIGNED NOT NULL ,
PRIMARY KEY (  `ctid` )
)";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "CREATE TABLE  `class_students` (
 `cid` MEDIUMINT UNSIGNED NOT NULL ,
 `cpid` SMALLINT UNSIGNED NOT NULL ,
 `attended` ENUM(  'Yes',  'No',  '' ) ,
 `score` VARCHAR( 5 ) NOT NULL ,
 `certnumber` VARCHAR( 10 ) NOT NULL ,
PRIMARY KEY (  `cid` ,  `cpid` )
)";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `client` ADD  `mailingcity` VARCHAR( 30 ) NOT NULL AFTER  `mailingaddress2` ,
ADD  `mailingstate` VARCHAR( 2 ) NOT NULL AFTER  `mailingcity` ,
ADD  `mailingzip` VARCHAR( 5 ) NOT NULL AFTER  `mailingstate`";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "CREATE TABLE `client_activities` (
  `caid` mediumint(9) NOT NULL auto_increment,
  `cid` mediumint(9) NOT NULL default '0',
  `date_entered` datetime NOT NULL default '0000-00-00 00:00:00',
  `date_due` date NOT NULL default '0000-00-00',
  `enteredby` smallint(5) unsigned NOT NULL default '0',
  `assignedto` smallint(5) unsigned NOT NULL default '0',
  `status` enum('Incomplete','Done') NOT NULL default 'Incomplete',
  `statusdate` datetime NOT NULL default '0000-00-00 00:00:00',
  `description` varchar(255) NOT NULL default '',
  `display` enum('Always','Incomplete') NOT NULL default 'Incomplete',
  PRIMARY KEY  (`caid`)
)";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `class_types` ADD  `defaultmin` TINYINT UNSIGNED NOT NULL ,
ADD  `defaultrec` TINYINT UNSIGNED NOT NULL ,
ADD  `defaultmax` TINYINT UNSIGNED NOT NULL ,
ADD  `defaultlocation` VARCHAR( 50 ) NOT NULL";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

?>