<?php
 
$pdf->AddPage('P');
 
$x = 0;
$y = 0;
$ml= 0.625;
$w = 7.25;
$pdf->SetFont('Times','',10);

$query = "SELECT building.bid, streetnum, address, suffix, city, state, zip, numunits, yearbuilt, plat, lot, inspection.iid, unitdesc, section8, publichousing, starttime, scheddate, number, designation, units.owneroccupied as unitowneroccupied, inspection.owneroccupied as inspowneroccupied FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);
$starttime=$row[starttime];
$iid = $row['iid'];

$x=1.4;
$y=$y + 0.2 ;

$pdf->SetXY($x, $y);
$pdf->Cell(0,0, "Inspection $row[iid]-$row[number]");

//Banner box package
$y=$y+0.3;
$ys=$y-0.2;
$fontsize=12;
$ff=70;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($x+1.625, $y);
$pdf->Cell(0,0,'Rhode Island Department of Health');
$y=$y+$fontsize/$ff;
$fontsize=10;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($x+2, $y);
$pdf->Cell(0,0,'Environmental: Lead Program');
$y=$y+0.08+$fontsize/$ff;
$fontsize=12;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($x+1.15, $y);
$pdf->Cell(0,0,'Notification of Annual Reinspection Requirement');
$y=$y+$fontsize/$ff;

$pdf->Rect($ml, $ys, $w, $y-$ys);

//Inspection Address
//Banner box package
$y=$y+0.1;  //This adjusts position of entire box
$ys=$y-0.1; // This adjusts the boxes height
$fontsize=12;
$ff=65;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'1.  Dwelling or Premises where Lead Inspection Conducted:');
$y=$y+$fontsize/$ff+0.1;

$fontsize=10;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml+.3, $y);
$pdf->Cell(0,0,'Street:');
$pdf->SetXY($ml+0.8, $y);
$pdf->Cell(0, 0, "$row[streetnum] $row[address] $row[suffix]");
$pdf->line($ml+0.75, $y+0.065,4.5, $y+0.065);

$pdf->SetXY($ml+4.6, $y);
$y=$y+$fontsize/$ff+0.1;

$pdf->SetXY($ml+.3, $y);
$pdf->Cell(0,0,'City/Town:');
$pdf->SetXY($ml+1.1, $y);
$pdf->Cell(0,0, $row['city']);
$pdf->line($ml+1.06, $y+0.075, $ml+2.125, $y+0.075);
$pdf->SetXY($ml+2.3, $y);
$pdf->Cell(0,0,'Zip:');
$pdf->SetXY($ml+2.8, $y);
$pdf->Cell(0,0, $row['zip'] );
$pdf->line($ml+2.75, $y+0.075,$ml+3.75, $y+0.065);

$y=$y+$fontsize/$ff+0.1;
$pdf->Rect($ml, $ys, $w, $y-$ys);

// Owner
$bid = $row['bid'];
//$scheddate = date("m/d/Y",strtotime($row['scheddate']));
//$stattime = date("m/d/Y",strtotime($row['starttime']));

$query = "SELECT client.cid, firstname, lastname, streetnum, address, suffix, city, state, zip,startdate FROM client INNER JOIN owners USING (cid) WHERE bid=$bid AND startdate <= DATE(NOW()) AND enddate >= DATE(NOW()) LIMIT 1";		
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());		

if ($row = mysql_fetch_assoc($result)) {		
            formfix($row);
			$clearanceDate = $row['startdate'];		
            $cid = $row['cid'];		
		$startdate=$row['startdate'];

}
///////////////
$y=$y+0.1;
$ys=$y-0.1;
$fontsize=12;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'2.  Owner of Dwelling or Premises:');
$y=$y+$fontsize/$ff+0.1;

$fontsize=10;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml+.3, $y);
$pdf->Cell(0,0,'Name:');
$pdf->SetXY($ml+0.85, $y);
$ownersname="$row[firstname] $row[lastname]";
$pdf->Cell(0,0, $ownersname);
//$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
$pdf->line($ml+0.75, $y+0.075,$ml+4.25, $y+0.065);
$yphone=$y;
$xphone=$ml+4.6;
$pdf->SetXY($ml+4.6, $y);
$pdf->Cell(0,0,'Telephone No.:');
$pdf->line($ml+5.55, $y+0.075,$ml+6.7, $y+0.065);
$y=$y+$fontsize/$ff+0.1;
$pdf->SetXY($ml+.3, $y);
$pdf->Cell(0,0,'Street:');
$pdf->SetXY($ml+0.85, $y);
$pdf->Cell(0,0, "$row[streetnum] $row[address] $row[suffix]");
$pdf->line($ml+0.75, $y+0.075,$ml+4.25, $y+0.065);
$y=$y+$fontsize/$ff+0.1;

$pdf->SetXY($ml+0.3, $y);
$pdf->Cell(0,0,'City/Town:');
$pdf->SetXY($ml+1.16, $y);
$pdf->Cell(0,0, "$row[city]");
$pdf->line($ml+1.06, $y+0.075,$ml+2.125, $y+0.065);
$pdf->SetXY($ml+2.75, $y);
$pdf->Cell(0,0,'State:');
$pdf->SetXY($ml+3.3, $y);
$pdf->Cell(0,0, "$row[state]");
$pdf->line($ml+3.2, $y+0.075,$ml+4.25, $y+0.065);
$pdf->SetXY($ml+4.75, $y);
$pdf->Cell(0,0,'Zip:');
$pdf->SetXY($ml+5.16, $y);
$pdf->Cell(0,0, "$row[zip]");
$pdf->line($ml+5.06, $y+0.075,$ml+6.0, $y+0.065);
$y=$y+$fontsize/$ff+0.1;
////////////////////////
            $query = "SELECT type, number, ext FROM client_phone WHERE cid=$cid GROUP BY type";
            $result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                        formfix($row);
                        if ($row['ext']) {
                                    $nums[$row['type']] = "$row[number] ext. $row[ext]";
                        } else {
                                    $nums[$row['type']] = "$row[number]";
                        }
            }

$pdf->SetXY($xphone+0.95, $yphone);
            if ($nums['Home']) {
$pdf->Cell(0,0, $nums['Home']);
            } else if ($nums['Cell']) {
$pdf->Cell(0,0, $nums['Cell'] . ' (Cell)');

                        $cellused = 1;

}
/*
$pdf->SetXY($basex + 5.75, $basey + 0.48);

            if ($nums['Work']) {

$pdf->Cell(0,0, $nums['Work']);

            } else if ($nums['Cell'] && $cellused < 1) {

$pdf->Cell(0,0, $nums['Cell'] . ' (Cell)');
}
*/

$pdf->Rect($ml, $ys, $w, $y-$ys);

///
$query = "SELECT DATE_FORMAT(starttime, '%c/%e/%Y') AS startdate, DATE_FORMAT(starttime, '%l:%i %p') AS starttime FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_row($result);

$pdf->Cell(0,0, $row[0]);
// Inspector

$query = "SELECT firstname, lastname, elt, eli, units.starttime > elistart AS useeli FROM inspector INNER JOIN insp_assigned USING ( tid ) INNER JOIN units USING ( iid ) WHERE cuid =$cuid ORDER BY eli DESC  LIMIT 0 , 30 ";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

/*
while (($row = mysql_fetch_assoc($result)) && $x < 4) {
            formfix($row);
$pdf->SetXY($x, $y + $ydrop);
$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
$pdf->SetXY($x + 3.61, $y + $ydrop);
            if ($row['eli'] && $row['useeli']) {
$pdf->Cell(0,0, "ELI- $row[eli]");
            } else {
$pdf->Cell(0,0, "ELT- $row[elt]");

            }


        $ydrop=$ydrop+0.48;

//          $x++;

}
*/
/////
$y=$y+0.1;
$ys=$y-0.1;
$fontsize=12;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'3.  Certification By Inspector:');
$y=$y+$fontsize/$ff+0.1;
$pdf->SetXY($ml+0.3, $y);
$pdf->Cell(0,0,'Expiration Date of Lead-Safe Certificate:');
$pdf->SetXY($ml+3.38, $y);
$pdf->Cell(0,0, $row[0]);
$pdf->SetXY($ml+3.38, $y+.25);
$pdf->Cell(0,0, 'need to add 1 year');
$pdf->line($ml+3.375, $y+0.075,$ml+5.25, $y+0.065);
$y=$y+$fontsize/$ff;
$fontsize=10;
$pdf->SetFont('Times','I', $fontsize);
$pdf->SetXY($ml+0.3, $y);
$pdf->Cell(0,0,'[1 year from date of certification of Lead-Safe status.]');
$y=$y+$fontsize/$ff+0.1;

$pdf->SetFont('Times','','10');
$pdf->SetXY($ml+0.3, $y);
$pdf->multiCell(6.5,.18,'I certify that I am responsible for the inspection of the dwelling or premises specified in item 1 above and have determined that it is Lead-Safe, as defined by the Rhode Island Rules and Regulations for Lead Poisoning Prevention.');
$y=$y+$fontsize/$ff+0.7;

$pdf->SetXY($ml+4.5, $y);
$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
$pdf->line($ml+0.3, $y+0.075,$ml+3.0, $y+0.065);
$pdf->line($ml+3.75, $y+0.075,$ml+7.0, $y+0.065);

$pdf->Cell(0,0, $ownersname);
$pdf->Cell(0,0, "$row[firstname] $row[lastname]");

$y=$y+0.15;
$fontsize=10;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml+1.2, $y);
$pdf->Cell(0,0,'(Signature)');
$pdf->SetXY($ml+4.5, $y);
$pdf->Cell(0,0,'(Type or Print Name of Inspector)');
$y=$y+$fontsize/$ff+0.1;

$pdf->SetXY($ml+0.65, $y);
$pdf->Cell(0,0,'What Date goes here Inspector');
$pdf->SetXY($ml+0.3, $y);
$pdf->Cell(0,0,'Date:');
$pdf->line($ml+0.625, $y+0.075,$ml+2.0, $y+0.065);
$pdf->SetXY($ml+4.0, $y);
$pdf->Cell(0,0,'RI Certification No.: ELI- $row[eli]');
$pdf->line($ml+5.25, $y+0.075,$ml+6.5, $y+0.065);
$y=$y+$fontsize/$ff+0.1;

$pdf->Rect($ml, $ys, $w, $y-$ys);

$y=$y+0.1;
$ys=$y-0.1;
$fontsize=12;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'4.  Certification By Owner:');
$y=$y+$fontsize/$ff+0.1;

$pdf->SetFont('Times','','10');
$pdf->SetXY($ml+0.3, $y);
$pdf->multiCell(6.5,.18,'I acknowledge that I received a copy of this Notification of Annual Re-inspection Requirement.  I Certify, that I will, within three business days, provide a copy of this Notification of Annual Re-inspection Requirement to any tenant(s) of the property referenced I Item 1 above.  I understand that this Notification of Annual Re-inspection Requirement is a record/report which must be disclosed to all future tenants, prior to signing any lease or otherwise occupying these premises, in accordance with Section 9.2 of the Rhode Island Rules and Regulations for Lead Poisoning Prevention.  I also understand that a Certified Environmental Lead Inspector must conduct a re-inspection of the property specified in Item 1 above, in accordance with Section 4.4 of the Rhode Island Rules and Regulations for Lead Poisoning Prevention, prior to the expiration date specified in Item 3 above, in order to maintain my property in a Lead-Safe condition.');

$y=$y+2.0;
$pdf->line($ml+0.3, $y+0.075,$ml+3.0, $y+0.065);
$pdf->line($ml+3.75, $y+0.075,$ml+7.00, $y+0.065);

$pdf->SetXY($ml+4.5, $y);
$pdf->Cell(0,0, $ownersname);

$fontsize=10;
$y=$y+0.15;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml+1.2, $y);
$pdf->Cell(0,0,'(Signature)');
$pdf->SetXY($ml+4.0, $y);
$pdf->Cell(0,0,'(Type or Print Name of Owner/Owners Agent)');
$y=$y+$fontsize/$ff+0.1;

$pdf->SetXY($ml+0.65, $y);
$pdf->Cell(0,0,'What Date goes here Owner');
$pdf->SetXY($ml+0.3, $y);
$pdf->Cell(0,0,'Date:');
$pdf->line($ml+0.625, $y+0.065, $ml+2.0, $y+0.065);
$y=$y+$fontsize/$ff+0.1;

$pdf->Rect($ml, $ys, $w, $y-$ys);
//
$y=$y+0.1;
$ys=$y-0.1;
$fontsize=12;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'5.  Notification to Tenants(s):');
$y=$y+$fontsize/$ff;

$pdf->SetFont('Times','','10');
$pdf->SetXY($ml+0.3, $y);
$pdf->multiCell(6.5,.18,'The above referenced property was certified Lead-Safe by the Environmental Lead Inspector specified in Item 3 above and will remain Lead-Safe only as long as lead-based paint stays intact and soil containing lead stays covered (with grass, mulch, etc.).');
$pdf->SetFont('Times','B','10');
$pdf->SetXY($ml+0.3, $y+0.35);
$pdf->multiCell(6.5,.18,'                                            YOU SHOULD IMMEDIATELY REPORT TO YOUR LANDLORD ANY PEELING OR CHIPPING PAINT OF ANY BARE SOIL AROUND HOUSE,  AND REQUET THAT THESE CONDITION BE PROMPTOY CORRECTED IN ACCORDANCE WITH THE RHODE ISLAND RULES AND REGULATIONS FOR LEAD POISONING PREVENTION.');

$pdf->SetFont('Times','','10');
$pdf->SetXY($ml+0.3, $y+0.88);
$pdf->multiCell(6.5,.18,'                                                                                                                               Notification  to  your  landlord should be in writing.  Your landlord is requi9red to have a Certified Environmental Lead Inspector reinspect the property prior to the expiration date specified in Item 3 above, to ensure that the property has been maintained in a Lead-Safe condition.');

$y=$y+1.6;

$pdf->Rect($ml, $ys, $w, $y-$ys);


$pdf->SetFont('Times','','9');
$pdf->SetXY($ml, 10.60);
$pdf->Cell(0,0, 'FORM PBLC-22(9/97)');



?>
