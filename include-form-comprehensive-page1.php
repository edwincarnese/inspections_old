<?php

$pdf->AddPage('P');
$pdf->Image('images/fixed/comprehensive-page1v2.png', 0, 0, 8.5, 11.0, 'PNG');
$pdf->SetFont('Arial','',14);

$pdf->SetXY(0.90, 1.30);
$pdf->Cell(0,0, 'X');

// x - 0.03; y - 0.06 from line start

//PAGE NUMBERS
$pdf->SetFont('Arial','','12');
//$pdf->SetXY(7.12, 0.36);
//$pdf->Cell(0,0, $pdf->PageNo());
$pdf->SetXY(7.70, 0.36);
$pdf->Cell(0,0, '{totalpages}');

//ADDRESS
$query = "SELECT building.bid, streetnum, address, suffix, city, state, zip, numunits, yearbuilt, plat, lot, inspection.iid, unitdesc, section8, publichousing, starttime, number, designation FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

$lh = 0.4;
$pdf->SetFont('Arial','','12');

$pdf->SetXY(1.25, 0.35);
$pdf->Cell(0,0, "Inspection $row[iid]-$row[number]");

$iid = $row['iid'];
$bid = $row['bid'];

$pdf->SetXY(0.45, 1.96);
$pdf->Cell(0, 0, "$row[streetnum] $row[address] $row[suffix]");
$pdf->SetXY(3.67, 1.80);
$pdf->Cell(0, 0, $row['unitdesc']);
$pdf->SetXY(3.67, 1.96);
$pdf->Cell(0, 0, $row['designation']);
$pdf->SetXY(4.52, 1.96);
$pdf->Cell(0,0, $row['city'].', '.$row['state']);
$pdf->SetXY(0.65, 2.44);
$pdf->Cell(0,0, $row['zip']);
$pdf->SetXY(2.14, 2.44);
$pdf->Cell(0,0, $row['numunits']);

$query = "SELECT COUNT(*) FROM comprehensive_rooms WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$rooms = mysql_result($result, 0);
$pdf->SetXY(3.71, 2.44);
$pdf->Cell(0,0, $rooms);

$pdf->SetXY(4.84, 2.44);
$pdf->Cell(0,0, $row['yearbuilt']);

$pdf->SetXY(5.96, 2.44);
$pdf->Cell(0,0, $row['plat']);
$pdf->SetXY(7.17, 2.44);
$pdf->Cell(0,0, $row['lot']);

$query = "SELECT under6, DATE_FORMAT(starttime, '%c/%e/%Y') AS startdate, owneroccupied FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$under6 = mysql_result($result, 0, 'under6');
$startdate = mysql_result($result, 0, 'startdate'); //used in inspector section
$owneroccupied = mysql_result($result, 0, 'owneroccupied');

$pdf->SetXY(1.06, 2.92);
$pdf->Cell(0,0, $under6);

$pdf->SetLineWidth(0.014);
if($owneroccupied == 'Yes') {
	$pdf->Rect(3.92, 2.99, 0.13, 0.14);
} else { // No
	$pdf->Rect(4.13, 2.99, 0.13, 0.14);
}
if($row['section8'] == 'Yes') {
	$pdf->Rect(5.29, 2.99, 0.13, 0.14);
} else { // No
	$pdf->Rect(5.50, 2.99, 0.13, 0.14);
}
if($row['publichousing'] == 'Yes') {
	$pdf->Rect(7.14, 3.00, 0.13, 0.14);
} else { // No
	$pdf->Rect(7.35, 3.00, 0.13, 0.14);
}

//OWNER
$query = "SELECT client.cid, firstname, lastname, streetnum, address, suffix, city, state, zip FROM client INNER JOIN owners USING (cid) WHERE bid=$bid AND startdate <= DATE(NOW()) AND enddate >= DATE(NOW()) LIMIT 1";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if ($row = mysql_fetch_assoc($result)) {
	formfix($row);
	$cid = $row['cid'];

	$basex = 0.45; $basey = 3.74;

	$pdf->SetXY($basex, $basey);
	$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
	$pdf->SetXY($basex + 3.75, $basey);
	$pdf->Cell(0,0, "$row[streetnum] $row[address] $row[suffix]");
	$pdf->SetXY($basex, $basey + 0.48);
	$pdf->Cell(0,0, "$row[city], $row[state] $row[zip]");

	$query = "SELECT type, number, ext FROM client_phone WHERE cid=$cid GROUP BY type";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) {
		formfix($row);
		if ($row['ext']) {
			$nums[$row['type']] = "$row[number] ext. $row[ext]";
		} else {
			$nums[$row['type']] = "$row[number]";
		}
	}

	$pdf->SetXY($basex + 3.08, $basey + 0.48);
	if ($nums['Home']) {
		$pdf->Cell(0,0, $nums['Home']);
	} else if ($nums['Cell']) {
		$pdf->Cell(0,0, $nums['Cell'] . ' (Cell)');
		$cellused = 1;
	}
	$pdf->SetXY($basex + 5.75, $basey + 0.48);
	if ($nums['Work']) {
		$pdf->Cell(0,0, $nums['Work']);
	} else if ($nums['Cell'] && $cellused < 1) {
		$pdf->Cell(0,0, $nums['Cell'] . ' (Cell)');
	}
}

//INSPECTORS
$query = "SELECT firstname, lastname, elt, eli, units.starttime > elistart AS useeli FROM inspector INNER JOIN insp_assigned USING (tid) INNER JOIN units USING (iid) WHERE cuid=$cuid ORDER BY job";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$x = $mody = 0;
$basex = 0.45; $basey = 5.04;
while (($row = mysql_fetch_assoc($result)) && $x < 4) {
	formfix($row);
	$pdf->SetXY($basex, $basey + $mody);
	$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
	$pdf->SetXY($basex + 3.61, $basey+$mody);
	if ($row['eli'] && $row['useeli']) {
		$pdf->Cell(0,0, "ELI- $row[eli]");
	} else {
		$pdf->Cell(0,0, "ELT- $row[elt]");
	}
	if ($x == 0) {
		$pdf->SetXY($basex + 6.00, $basey+$mody);
		$pdf->Cell(0,0, $startdate); //retreived in address section
	}
	$mody += 0.475;
	$x++;
}

//REASON
$query = "SELECT reason, otherreason FROM inspection WHERE iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
list($reason, $otherreason) = mysql_fetch_row($result);
$pdf->SetFont('Arial','',12);
switch ($reason) {
	case 'Department of Health Initiated':
		$pdf->SetXY(0.60, 7.31);
		break;
	case 'Licensed Child Care Facility':
		$pdf->SetXY(0.60, 7.46);
		break;
	case 'School':
		$pdf->SetXY(0.60, 7.61);
		break;
	case 'Code Enforcement':
		$pdf->SetXY(0.60, 7.76);
		break;
	case 'Investigation of Improper Lead Hazard Reduction/Illegal Abatement':
		$pdf->SetXY(3.71, 7.31);
		break;
	case 'Private Client - Property Transfer':
		$pdf->SetXY(3.71, 7.41);
		break;
	case 'Dept. of Human Services Initiated':
		$pdf->SetXY(3.71, 7.76);
		break;
	default:
		$pdf->SetXY(3.71, 7.61);
		if ($reason == 'Other') {
			$other = $otherreason;
		} else { //conformance reason
			$other = $reason;
		}
		break;
}
$pdf->Cell(0,0, 'X');
if ($other) {
	$pdf->SetXY(4.43, 7.61);
	$pdf->Cell(0,0, $other);
}

//MEDIA TESTED
//PAINT
$query = "SELECT COUNT(*) FROM comprehensive_rooms INNER JOIN comprehensive_components USING (crid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$paint = mysql_result($result, 0);
$query = "SELECT COUNT(*) FROM comprehensive_ext_components WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$paint += mysql_result($result, 0);
$pdf->SetFont('Arial','','12');
if ($paint) {
	$pdf->SetXY(1.40, 8.48);
	$pdf->Cell(0,0, 'X');
}

//SOIL
//*****uncommented these lines to handle soil samples 
//*****added timetaken to the where clause in the 2nd sql query
//*****Question: Why is there data automatically being inserted on comprehensive_sides table?

$query = "SELECT u2.cuid FROM units AS u1 INNER JOIN units AS u2 USING (iid) WHERE u1.cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$extcuid = mysql_result($result, 0);
$query = "SELECT COUNT(*) FROM comprehensive_sides WHERE cuid=$extcuid AND inaccessible='No' AND cover='None' and timetaken <> '0000-00-00 00:00:00'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$soils = mysql_result($result, 0);
$query = "SELECT COUNT(*) FROM comprehensive_soil_debris WHERE cuid=$extcuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$soils += mysql_result($result, 0);
$query = "SELECT COUNT(*) FROM comprehensive_soil_object WHERE cuid=$extcuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$soils += mysql_result($result, 0);

//$soils = 1;
if ($soils) {
	$pdf->SetXY(3.00, 8.48);
	$pdf->Cell(0,0, 'X');
}

//WATER
$query = "SELECT COUNT(*) FROM waters WHERE cuid=$cuid AND iid=$iid"; //iid unnecessary?
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$waters = mysql_result($result, 0);
if ($waters) {
	$pdf->SetXY(4.56, 8.48);
	$pdf->Cell(0,0, 'X');
}

//DUST
$query = "SELECT COUNT(*) FROM comprehensive_wipes INNER JOIN comprehensive_rooms USING (crid) WHERE cuid=$cuid AND iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$waters = mysql_result($result, 0);
if ($waters) {
	$pdf->SetXY(6.38, 8.48);
	$pdf->Cell(0,0, 'X');
}


?>