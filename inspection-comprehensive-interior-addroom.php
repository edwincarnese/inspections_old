<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

$query = "SELECT * FROM room_list";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$roomlist = array();
while ($row = mysql_fetch_row($result)) {
	$roomlist[$row[0]] = str_replace(' ','&nbsp;',$row[0]);
}

$query = "SELECT iid, unitdesc, number FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
list($iid, $unitdesc, $unitnumber) = mysql_fetch_row($result);

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - $unitdesc - Add room";
require_once'header.php';
?>
<form action="inspection-comprehensive-interior-addroom-save.php" method="post">
<input type="hidden" name="cuid" value="<?php print $cuid; ?>" />
<p>Add new room: 
<select name="addafter">
<option value="0">At start</option>
<?php
$query = "SELECT number, name FROM comprehensive_rooms WHERE cuid=$cuid ORDER BY number";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$rooms = mysql_num_rows($result);
while ($row = mysql_fetch_row($result)) {
	$x++;
	if ($x == $rooms) {
		print "<option value=\"$row[0]\" selected=\"selected\">After $row[1]</option>\n";
	} else {
		print "<option value=\"$row[0]\">After $row[1]</option>\n";
	}
}
?>
</select></p>
<table class="info">
<tr><th>Room Description</th></tr>
<?php
$columns = 4;
print "<tr><td><table>";
$numitems = 0;
foreach ($roomlist as $room => $roomlabel) {
	if ($numitems % $columns == 0) {
		print "<tr>";
	}
	print "<td><input type=\"radio\" name=\"room\" value=\"$room\" />$roomlabel</td>";
	$numitems++;
	if ($numitems % $columns == 0) {
		print "</tr>\n";
	}
}
if ($numitems % $columns) {
	while ($numitems % $columns) {
		print "<td> </td>";
		$numitems++;
	}
	print "</tr>";
}
print "<tr><td colspan=\"4\"><input type=\"radio\" name=\"room\" value=\"Other\" />Other <input type=\"text\" name=\"otherroom\" maxlength=\"30\" /></td></tr>";
print "</table></td></tr>\n";
?>
</table>
<p><input type="submit" name="submit" value="Save" /> <input type="submit" name="submit" value="Cancel" /></p>
</form>
<?php
require_once'footer.php';
?>