<?php

require_once'connect.php';

$query = "ALTER TABLE building add column numUnitsConfirmed tinyint(2) default 0";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$query = "ALTER TABLE users add column userlevel tinyint(8) not null default 0";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$query = "ALTER TABLE owners add column getReport enum('yes', 'no') default 'no'";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$query = "ALTER TABLE units add column dustLab varchar(128)";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$query = "ALTER TABLE units add column designation varchar(8)";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$query = "ALTER TABLE inspector add column elt_lead enum('yes', 'no') default 'no'";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
?>
