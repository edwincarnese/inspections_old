<?php
require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

if (!isset($dest)) {
	$dest = $_POST['dest'] or $dest = $_GET['dest'] or $dest = 0;
}
if ($cuid == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}

$query = "SELECT iid, unitdesc FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
list($iid, $unitdesc) = mysql_fetch_row($result);

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-* - $address - $unitdesc - Links";
require_once'header.php';
?>
<p>Select the units linked to this common.</p>
<form action="inspection-unit-links-save.php" method="post">
<input type="hidden" name="dest" value="<?php print $dest; ?>" />
<input type="hidden" name="cuid" value="<?php print $cuid; ?>" />

<p>
<?php
$query = "SELECT cuid, unitdesc, unit_links.common FROM units LEFT JOIN unit_links ON (units.cuid=unit_links.unit AND unit_links.common=$cuid) WHERE iid=$iid AND unittype='Interior' AND (unit_links.common=$cuid OR unit_links.common IS NULL) ORDER BY number, unittype";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while ($row = mysql_fetch_assoc($result)) {

	if ($row['common']) {
		print "<input type=\"checkbox\" name=\"unitids[]\" value=\"$row[cuid]\" checked=\"checked\" />";
	} else {
		print "<input type=\"checkbox\" name=\"unitids[]\" value=\"$row[cuid]\" />";
	}
	print "$row[unitdesc]<br />\n";
}
?>
</p>

<p>
<input type="submit" name="submit" value="Save" /><input type="reset" value="Reset" /><input type="submit" name="submit" value="Cancel" />
</p>
