<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

$query = "SELECT * FROM comprehensive_ext WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}

$room = mysql_fetch_assoc($result);

$query = "SELECT iid, unitdesc, number FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
list($iid, $unitdesc, $unitnumber) = mysql_fetch_row($result);

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - $unitdesc";
require_once'header.php';
?>
<form action="inspection-comprehensive-ext-savewindow.php" method="post">
<input type="hidden" name="cuid" value="<?php print $cuid; ?>" />
<table class="straightup">
<tr><th>Basement Windows</th><th>Attic Windows</th><th>Garage Windows</th></tr>
<tr><td>
<table>
<?php
$x= 0;
print "<tr><td colspan=\"2\">";
if ($room['basementwindows'] == $x) {
	print "<input type=\"radio\" name=\"basementwindows\" value=\"$x\" checked=\"checked\" />$x";
} else {
	print "<input type=\"radio\" name=\"basementwindows\" value=\"$x\" />$x";
}
print '</td></tr>';

for ($x=1; $x<=10; $x++) {
	print '<tr><td>';
	if ($room['basementwindows'] == $x) {
		print "<input type=\"radio\" name=\"basementwindows\" value=\"$x\" checked=\"checked\" />$x";
	} else {
		print "<input type=\"radio\" name=\"basementwindows\" value=\"$x\" />$x";
	}
	print '</td><td>';
	$y = $x + 10;
	if ($room['basementwindows'] == $y) {
		print "<input type=\"radio\" name=\"basementwindows\" value=\"$y\" checked=\"checked\" />$y";
	} else {
		print "<input type=\"radio\" name=\"basementwindows\" value=\"$y\" />$y";
	}
	print '</td></tr>';
}
?>
</table>
</td>
<td>
<table>
<?php
print "<tr><td colspan=\"2\">";
$x = 0;
if ($room['atticwindows'] == $x) {
	print "<input type=\"radio\" name=\"atticwindows\" value=\"$x\" checked=\"checked\" />$x";
} else {
	print "<input type=\"radio\" name=\"atticwindows\" value=\"$x\" />$x";
}
print '</td></tr>';

for ($x=1; $x<=10; $x++) {	
	print '<tr><td>';
	if ($room['atticwindows'] == $x) {
		print "<input type=\"radio\" name=\"atticwindows\" value=\"$x\" checked=\"checked\" />$x";
	} else {
		print "<input type=\"radio\" name=\"atticwindows\" value=\"$x\" />$x";
	}
	print '</td><td>';
	$y = $x + 10;
	if ($room['atticwindows'] == $y) {
		print "<input type=\"radio\" name=\"atticwindows\" value=\"$y\" checked=\"checked\" />$y";
	} else {
		print "<input type=\"radio\" name=\"atticwindows\" value=\"$y\" />$y";
	}
	print '</td></tr>';
}
?>
</table>
</td>
<td>
<table>
<?php
print "<tr><td colspan=\"2\">";
$x = 0;
if ($room['garagewindows'] == $x) {
	print "<input type=\"radio\" name=\"garagewindows\" value=\"$x\" checked=\"checked\" />$x";
} else {
	print "<input type=\"radio\" name=\"garagewindows\" value=\"$x\" />$x";
}
print '</td></tr>';

for ($x=1; $x<=10; $x++) {	
	print '<tr><td>';
	if ($room['garagewindows'] == $x) {
		print "<input type=\"radio\" name=\"garagewindows\" value=\"$x\" checked=\"checked\" />$x";
	} else {
		print "<input type=\"radio\" name=\"garagewindows\" value=\"$x\" />$x";
	}
	print '</td><td>';
	$y = $x + 10;
	if ($room['garagewindows'] == $y) {
		print "<input type=\"radio\" name=\"garagewindows\" value=\"$y\" checked=\"checked\" />$y";
	} else {
		print "<input type=\"radio\" name=\"garagewindows\" value=\"$y\" />$y";
	}
	print '</td></tr>';
}
?>
</table>
</td>
</tr>
</table>
<p><input type="submit" name="submit" value="Update" /></p>
</form>

<p><a href="inspection-comprehensive-unit.php?cuid=<?php print $room['cuid']; ?>">Exterior Main Menu</a></p>

<p><a href="inspection-main.php?iid=<?php print $room['iid']; ?>">Inspection Main Menu</a></p>

<?php
require_once'footer.php';
?>