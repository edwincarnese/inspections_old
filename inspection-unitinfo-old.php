<?php 
require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

$query = "SELECT * FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}

$row = mysql_fetch_assoc($result);

$title = "Edit Unit";
require_once'header.php';
?>

<form action="inspection-unitupdate.php" method="post">
<input type="hidden" name="cuid" value="<?php print $cuid;?>" />
<table>
<tr><td>Unit Description:</td><td><input type="text" name="unitdesc" value="<?php print $row['unitdesc']; ?>" maxlength="25" /></td></tr>
<tr><td>Unit Type:</td><td>
<?php
$values = array( 'Interior', 'Interior Common');
foreach ($values as $value) {
	if ($row['unittype'] == $value) {
		print "<input type=\"radio\" name=\"unittype\" value=\"$value\" checked=\"checked\" />$value&nbsp;";
	} else {
		print "<input type=\"radio\" name=\"unittype\" value=\"$value\" />$value&nbsp;";
	}
}
?>
</td></tr>
<tr><td colspan="2"><br /><b>The below fields should be skipped for common areas.</b></td></tr>
<tr><td>Tenant:</td><td><input type="text" name="tenant" value="<?php print $row['tenant']; ?>" maxlength="30" /></td></tr>
<tr><td>Years resided at unit:</td><td><input type="text" name="tenantyears" value="<?php print $row['tenantyears']; ?>" maxlength="150" /></td></tr>
<tr><td>Children Under 6:</td><td><input type="text" size="4" maxlength="2" name="under6" value="<?php print $row['under6']; ?>" /></td></tr>
<tr><td>Owner Occupied:</td><td>
<?php
$values = array('Yes','No');
foreach ($values as $value) {
	if ($row['owneroccupied'] == $value) {
		print "<input type=\"radio\" name=\"owneroccupied\" value=\"$value\" checked=\"checked\" />$value&nbsp;";
	} else {
		print "<input type=\"radio\" name=\"owneroccupied\" value=\"$value\" />$value&nbsp;";
	}
}
?></td></tr>
<tr><td>Section 8:</td><td>
<?php
$values = array('Yes','No');
foreach ($values as $value) {
	if ($row['section8'] == $value) {
		print "<input type=\"radio\" name=\"section8\" value=\"$value\" checked=\"checked\" />$value&nbsp;";
	} else {
		print "<input type=\"radio\" name=\"section8\" value=\"$value\" />$value&nbsp;";
	}
}
?></td></tr>
<tr><td>Public Housing:</td><td>
<?php
$values = array('Yes','No');
foreach ($values as $value) {
	if ($row['publichousing'] == $value) {
		print "<input type=\"radio\" name=\"publichousing\" value=\"$value\" checked=\"checked\" />$value&nbsp;";
	} else {
		print "<input type=\"radio\" name=\"publichousing\" value=\"$value\" />$value&nbsp;";
	}
}
?></td></tr>
</table>
<p><input type="submit" name="submit" value="Save" /> <input type="reset" value="Reset" /> <input type="submit" name="submit" value="Cancel" /></p>
</form>

<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>