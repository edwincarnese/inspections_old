<?php

require_once'session.php';
require_once'connect.php';

if(isset($_SERVER['HTTP_USER_AGENT']) and strpos($_SERVER['HTTP_USER_AGENT'],'MSIE')) {
	header('Content-Type: application/force-download');
} else {
	header('Content-Type: application/octet-stream');
}
header('Content-disposition: attachment; filename=owners.csv');

print '"First Name","Last Name","Address","City","Insp #s"'."\n";

$query = "SELECT firstname, lastname, CONCAT(streetnum, ' ', address, ' ', suffix), city, cid FROM client WHERE cid>0 ORDER BY lastname, firstname";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while ($row = mysql_fetch_assoc($result)) {
	$iids = array();
	$query = "SELECT iid FROM owners INNER JOIN inspection USING (bid) WHERE cid=$row[cid]";
	$result2 = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while($row2 = mysql_fetch_row($result2) ) {
		$iids[] = $row2[0];
	}
	$row['cid'] = implode(',', $iids);
	print '"'.implode('","', $row).'"'."\n";
}

?>