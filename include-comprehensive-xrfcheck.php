<?php

if (!$skipXRF) {
	$query = "SELECT MAX(timetaken) FROM comprehensive_calibration WHERE iid=$iid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$timetaken = mysql_result($result,0);

	$query = "SELECT serialnumber, DATE_ADD('$timetaken', INTERVAL 4 HOUR) AS nexttime, DATE_FORMAT(DATE_ADD('$timetaken', INTERVAL 4 HOUR), '%l:%i:%s %p') AS shownexttime, NOW() AS nowtime, DATE_FORMAT(NOW(), '%l:%i:%s %p') AS shownowtime FROM comprehensive_calibration WHERE iid=$iid AND timetaken = '$timetaken'";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$row = mysql_fetch_assoc($result);

	if ($row['nowtime'] > $row['nexttime'] || !$row) {
		header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-comprehensive-xrfcalibration.php?iid=$iid&cuid=$cuid&crid=$crid&ccid=$ccid&type=$type&serialnumber=$row[serialnumber]");
		exit();
	}

	$shownexttime = $row['shownexttime'];
	$shownowtime = $row['shownowtime'];
} else {
	$shownexttime = 'XRF Readings skipped';
	$shownowtime = 'XRF Readings skipped';
}

?>