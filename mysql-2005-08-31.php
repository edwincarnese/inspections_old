<?php

$query = "ALTER TABLE  `room_item_components` ADD  `dsid` TINYINT UNSIGNED NOT NULL ,
ADD  `defaultcondition` ENUM(  'Intact',  'Intact Where Visible',  'Damaged Touchup',  'Damaged',  'Assumed Damaged',  'No Paint',  'Post \'77',  'Damaged - Friction' ) ,
ADD  `defaultassessment` ENUM(  'Lead-free',  'Lead-safe',  'Hazard',  'Assumed Hazard' ) ,
ADD  `conformancecomponent` ENUM(  'Ceiling',  'Walls',  'Trim',  'Floor',  'Window',  'Door',  'Cabinet',  'Siding',  'Doors',  'Porch',  'Windows',  'Soffit',  'Railing',  'Stairs',  'Columns',  'Foundation',  'Garage',  'Fence',  'Outbuilding',  'Other' ) DEFAULT  'Other' NOT NULL ,
ADD  `order` SMALLINT UNSIGNED NOT NULL ;";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `room_item_list` DROP  `dsid` ,
DROP  `defaultcondition` ,
DROP  `defaultassessment` ;";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `room_item_components` ADD  `grouplist` SET(  'Woodwork/Molding',  'Window',  'Window Well',  'Door',  'Exterior Siding',  'Exterior Trim',  'Porches' ) NOT NULL AFTER  `defaultassessment` ;";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `comprehensive_components` ADD  `grouplist` SET(  'Woodwork/Molding',  'Window',  'Window Well',  'Door',  'Exterior Siding',  'Exterior Trim',  'Porches' ) NOT NULL AFTER  `condition` ,
ADD  `conformancecomponent` ENUM(  'Ceiling',  'Walls',  'Trim',  'Floor',  'Window',  'Door',  'Cabinet',  'Siding',  'Doors',  'Porch',  'Windows',  'Soffit',  'Railing',  'Stairs',  'Columns',  'Foundation', 'Garage',  'Fence',  'Outbuilding',  'Other' ) DEFAULT  'Other' NOT NULL AFTER  `grouplist` ,
ADD  `order` SMALLINT UNSIGNED NOT NULL AFTER  `conformancecomponent` ;";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `comprehensive_ext_components` ADD  `grouplist` SET(  'Woodwork/Molding',  'Window',  'Window Well',  'Door',  'Exterior Siding',  'Exterior Trim',  'Porches' ) NOT NULL AFTER  `condition` ,
ADD  `conformancecomponent` ENUM(  'Ceiling',  'Walls',  'Trim',  'Floor',  'Window',  'Door',  'Cabinet',  'Siding',  'Doors',  'Porch',  'Windows',  'Soffit',  'Railing',  'Stairs',  'Columns',  'Foundation', 'Garage',  'Fence',  'Outbuilding',  'Other' ) DEFAULT  'Other' NOT NULL AFTER  `grouplist` ,
ADD  `order` SMALLINT UNSIGNED NOT NULL AFTER  `conformancecomponent` ;";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `room_item_components` CHANGE  `order`  `displayorder` SMALLINT( 5 ) UNSIGNED DEFAULT  '0' NOT NULL";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `comprehensive_components` CHANGE  `order`  `displayorder` SMALLINT( 5 ) UNSIGNED DEFAULT  '0' NOT NULL";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));

$query = "ALTER TABLE  `comprehensive_ext_components` CHANGE  `order`  `displayorder` SMALLINT( 5 ) UNSIGNED DEFAULT  '0' NOT NULL";
mysql_query($query, $remotelink) or sql_crapout($query.'<br />'.mysql_error($remotelink));


?>
