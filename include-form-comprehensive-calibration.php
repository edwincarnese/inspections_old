<?php

$pdf->AddPage('P');

$iid = issetBlank($row, 'iid');
$x=0;
$y=0;
$ml=0.625;
$w=7.25;
//ADDRESS, TOP
$query = "SELECT inspection.iid, streetnum, address, suffix, city, DATE_FORMAT(starttime, '%c/%e/%Y') AS start, unitdesc, number FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

//Inspection #
$pdf->SetFont('Times','B','10');
$pdf->SetXY(1.5, .2);
$pdf->Cell(0,0, "Inspection $row[iid]-$row[number]");
$ml=0.625;
//Calibration Inspection Banner
$x=2.5 ;
$y=.2;
$pdf->Rect($ml, $y+0.15, 7.25, 0.3);
$y=$y+.3 ;
$pdf->SetFont('Times','B','14');
$pdf->SetXY($x+.875, $y);
$pdf->Cell(0,0,'Calibration Data');

//PAGE NUMBERS
$pdf->SetFont('Times','','10');
$pdf->SetXY(6.5, $y);
$pdf->Cell(0,0, 'Page _____ of _____');
$pdf->SetXY(6.9, $y);
$pdf->Cell(0, 0, $pdf->PageNo());
$pdf->SetXY(7.4, $y);
$pdf->Cell(0,0, '{totalpages}');

$iid = $row['iid'];

//Unit Specifications
//ADDRESS, TOP
$query = "SELECT inspection.iid, streetnum, address, suffix, city, unitdesc, number FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

$pdf->SetFont('Times','','12');

$x=$ml;
$y=$y+.5;
$pdf->SetXY($x, $y);
$pdf->Cell($x, 0, 'Floor / Apt. No.');
$pdf->SetXY($x, $y);
$pdf->Rect($x+1.2, $y+.1, 1, 0);
$pdf->SetXY($x+0.9, $y);
$pdf->Cell(1.7, 0, $row['unitdesc'], 0, 0, 'C');
$x=3;
$pdf->SetXY($x, $y);
$pdf->Cell($x, 0, 'Street /City:');
$pdf->SetXY($x, $y);
$pdf->Rect($x+.95, $y+.1, 3.85, 0);
$pdf->SetXY($x+1.3, $y);
$pdf->Cell(2.95,0, "$row[streetnum] $row[address] $row[suffix], $row[city]", 0, 0, 'C');

//XRF Data
$query = "SELECT manufacturer, serialnumber, standardtype FROM comprehensive_calibration WHERE iid=$iid LIMIT 1";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);

$x=$ml;
$y=$y+.625;
$pdf->SetXY($x, $y);
$pdf->Cell($x, 0, 'XRF Manufacturer, Model & Serial No.'); 
$pdf->Line($x+2.75, $y+0.08, $ml+$w, $y+0.08);
$pdf->SetXY($x+2.75, $y);
$pdf->Cell(4.5,0, $row['manufacturer'].' '. $row['serialnumber'], 0, 0, 'C');

$y=$y+.375;
if ($row['standardtype'] == 'Manufacturer') {
	$pdf->SetXY(3.1, $y);
	$pdf->Cell(0,0, 'X');
} else if ($row['standardtype'] == 'NIST') {
	$pdf->SetXY(3.78, $y);
	$pdf->Cell(0,0, 'X');
} else {
	$pdf->SetXY(5.14, $y);
	$pdf->Cell(0,0, $row['standardtype']);
}

$pdf->SetXY($x, $y);
$pdf->Cell(0, 0, 'Standard Type:       Manufacturer _______   NIST _______   Other _______________________________');

//$iid = $row['iid'];
//Samples Header
$x=$ml;
$y=$y+.4375;
$w=1.035;
$h=0.625;
$pdf->Rect($x, $y, $w, $h);
$pdf->SetXY($x+.25, $y+.25);
$pdf->Cell(0, 0, 'Standard');
$pdf->SetFont('Times','','8');
$pdf->SetXY($x+.25, $y+.4);
$pdf->Cell(0, 0, '(mg/sq. cm)');
$pdf->SetFont('Times','','10');
$x=$x+$w;
$pdf->Rect($x, $y, $w, $h);
$pdf->SetXY($x+.35, $y+.25);
$pdf->Cell(0, 0, 'Time');
$x=$x+$w;
$pdf->Rect($x, $y, $w, $h);
$pdf->SetXY($x+.15, $y+.25);
$pdf->Cell(0, 0, 'Temperature');
$x=$x+$w;
$w2=$w;
$pdf->Rect($x, $y+.425, $w, 0.2);
$pdf->SetXY($x+.45, $y+.55);
$pdf->Cell(0, 0, '1');
$pdf->Rect($x, $y, 3*$w, 0.425);
$pdf->SetXY($x+1.3, $y+.1);
$pdf->Cell(0, 0, 'Results');
$pdf->SetXY($x+1.25, $y+.3);
$pdf->SetFont('Times','','8');
$pdf->Cell(0, 0, '(mg/sq. cm)');
$pdf->SetFont('Times','','10');
$x=$x+$w;
$w2=$w2+$w;
$pdf->Rect($x, $y+.425, $w, 0.2);
$pdf->SetXY($x+.45, $y+.55);
$pdf->Cell(0, 0, '2');
$x=$x+$w;
$w2=$w2+$w;
$pdf->Rect($x, $y+.425, $w, 0.2);
$pdf->SetXY($x+.45, $y+.55);
$pdf->Cell(0, 0, '3');
$x=$x+$w;
$pdf->Rect($x, $y, $w, $h);
$pdf->SetXY($x+.25, $y+.25);
$pdf->Cell(0, 0, 'Average');


//Sample Table
$x=$ml;
$y=$y+.625;
$wt=$w;
$h=2.5;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$wt=$wt+$w;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$wt=$wt+$w;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$wt=$wt+$w;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$wt=$wt+$w;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$wt=$wt+$w;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$wt=$wt+$w;
$w2=$w;
$pdf->Rect($x, $y, $w, $h);
/*
$height=.25
while ($height < 2) {
	$pdf->Rect($ml, $y+$height, $w*7, 0.25);
	$height=$height+.5;
}
*/
$pdf->Rect($ml, $y+0.25, $w*7, 0.25);
$pdf->Rect($ml, $y+0.75, $w*7, 0.25);
$pdf->Rect($ml, $y+1.25, $w*7, 0.25);
$pdf->Rect($ml, $y+1.75, $w*7, 0.25);
$pdf->Rect($ml, $y+2.0, $w*7, 0.25);

//READINGS
$pdf->SetFont('Times','',10);
#0.228
$basex = 1.00; $basey = 3.23; $mody = 0;
#1.70	3.20	4.25	5.25	6.25	7.35

$query = "SELECT standard, DATE_FORMAT(timetaken, '%l:%i %p') AS timetaken, temperature, reading1, reading2, reading3 FROM comprehensive_calibration WHERE iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while ($row = mysql_fetch_assoc($result) ) {
	formfix($row);
	$pdf->SetXY($basex, $basey + $mody);
	$pdf->Cell(0,0, sprintf("%1.1f", $row['standard']));
	$pdf->SetXY($basex + 0.8, $basey + $mody);
	$pdf->Cell(0.8,0, $row['timetaken'], 0, 0, 'C');
	$pdf->SetXY($basex + 1.5, $basey + $mody);
	$pdf->Cell(1,0, $row['temperature'], 0, 0, 'C');
	$pdf->SetXY($basex + 2.75, $basey + $mody);
	$pdf->Cell(1,0, sprintf("%1.1f", $row['reading1']), 0, 0, 'C');
	$pdf->SetXY($basex + 3.8, $basey + $mody);
	$pdf->Cell(1,0, sprintf("%1.1f", $row['reading2']), 0, 0, 'C');
	$pdf->SetXY($basex + 4.85, $basey + $mody);
	$pdf->Cell(1,0, sprintf("%1.1f", $row['reading3']), 0, 0, 'C');
	$pdf->SetXY($basex + 5.85, $basey + $mody);
	$avg = round(($row['reading1'] + $row['reading2'] + $row['reading3']) / 3, 2);
	$pdf->Cell(1,0, sprintf("%.2f", $avg), 0, 0, 'C');
	$mody += 0.25;
}

//COMMENTS
$y=$y+2.825;
$pdf->SetXY($ml+2.875, $y);
$pdf->SetFont('Times','B',12);
$pdf->Cell(0, 0, 'Inspection Comments:');

$y=$y+0.25;
$pdf->SetXY($ml, $y+.12);
$pdf->SetFont('Times','B',10);
$pdf->Cell(0, 0, 'Comments:');
$pdf->SetFont('Times','',10);
$pdf->Rect($ml, $y, $w*7, 3);
$pdf->Rect($ml, $y+0.25, $w*7, 0.25);
$pdf->Rect($ml, $y+0.75, $w*7, 0.25);
$pdf->Rect($ml, $y+1.25, $w*7, 0.25);
$pdf->Rect($ml, $y+1.75, $w*7, 0.25);
$pdf->Rect($ml, $y+2.25, $w*7, 0.25);
$pdf->Rect($ml, $y+2.75, $w*7, 0.25);

$pdf->SetFont('Times','',10);
//$pdf->SetXY(1.4, $y+.12);
//$pdf->Cell(0,0, "1) Any coating not otherwise designated as lead-free in this report is assumed to be positive for lead-based paint.");

$query = "SELECT comment FROM comprehensive_comments WHERE cuid=$cuid AND type='xrf'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$pdf->SetFont('Arial','',10);
if($row = mysql_fetch_row($result)) {
	formfix($row);
	$lh = 0.25;
	$pdf->SetXY($ml+0.05, 6.4);
	$pdf->MultiCell($w-0.05, $lh, $row[0], 0);
}
//COMMENTS
//if (mysql_num_rows($result)) {
#7.45	7.98 0.33 0.165		0.52	8.04	7.52
//	$lh = 0.25;
	//$pdf->MultiCell(7.05, $lh, mysql_result($result, 0));
//}

//date/time at bottom
$query = "SELECT DATE_FORMAT(starttime, '%c/%e/%Y') AS startdate, DATE_FORMAT(starttime, '%l:%i %p') AS starttime FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_row($result);

$pdf->SetXY(6.0, 9.90);
$pdf->Cell(0,0, '___________  /  ___________ ');
$pdf->SetXY(7.05, 9.90);
$pdf->Cell(0,0, $row[0]);
$pdf->SetXY(6.0, 10.10);
$pdf->Cell(0,0, '      Initials                 Date ');

$pdf->SetFont('Times','','9');
$pdf->SetXY($ml, 10.50);
$pdf->Cell(0,0, 'FORM PBLC-23-4(10/02) Replaces OEHRA II (4/97) p.4');



?>