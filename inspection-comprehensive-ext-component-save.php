<?php

require_once'session.php';
require_once'connect.php';

$ccid = $_POST['ccid'];

$side = "";
if(isset($_POST["side-0"]) || isset($_POST["side-1"]) || isset($_POST["side-2"]) || isset($_POST["side-3"]))
{
$side_0 = $_POST["side-0"];
$side_1 = $_POST["side-1"];
$side_2 = $_POST["side-2"];
$side_3 = $_POST["side-3"];
$side .= $side_0 . $side_1 . $side_2 . $side_3;
}

$conditions = mysql_real_escape_string($_POST['condition']); // ADDED CODE TO ACCEPT SINGLE QUOTE

$query = "SELECT cuid FROM comprehensive_ext_components WHERE ccid=$ccid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
list ($cuid) = mysql_fetch_row($result);

if ($_POST['submit'] == 'Cancel') {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-comprehensive-exterior.php?cuid=$cuid");
	exit();
}

if ($_POST['submit'] == 'Save' || $_POST['submit'] == 'Enable XRF Readings' ) {

	if ($_POST['comment']) {
		$query = "INSERT INTO comprehensive_comments (cuid, type, comment) VALUES ($cuid, 'xrf', '$_POST[comment]') ON DUPLICATE KEY UPDATE comment='$_POST[comment]'";
	} else {
		$query = "DELETE FROM comprehensive_comments WHERE cuid=$cuid AND type='xrf'";
	}
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

	if (strlen($_POST['xrf']) < 3) {
		$xrf = ($_POST['xrf']+0).'.'.($_POST['xrf2']+0);
	} else {
		$xrf = $_POST['xrf'];
	}
	
	if ($_POST['condition'] == "Post \\'77" || $_POST['condition'] == 'No Paint') {
		$xrf = '';
	}

	// ************ AUTO-ASSESSMENT CODE ************ //
	switch($_POST['condition']) {
		case 'No Paint':
		case 'Post \\\'77': // passed as "Post \'77", escape \ and '
			$_POST['hazardassessment'] = 'Lead-free';
			break;
		case 'Intact':
		case 'Intact Where Visible':
		case 'Intact - Factory Finish':
		case 'Covered':
			$_POST['hazardassessment'] = 'Lead-safe';
			break;
		default:
			$_POST['hazardassessment'] = 'Hazard';
	}

	//check to see if XRF reading has changed; if so, assume taken now
	$query = "SELECT xrf FROM comprehensive_ext_components WHERE ccid=$_POST[ccid]";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	if ($xrf == mysql_result($result, 0)) { //leave alone if same
		$query = "UPDATE comprehensive_ext_components SET side='$side', sid='$_POST[sid]', xrf='$xrf', hazards='$conditions', spottest='$_POST[spottest]', hazardassessment='$_POST[hazardassessment]' WHERE ccid=$_POST[ccid]";
	} else { //change time if not
		$query = "UPDATE comprehensive_ext_components SET xrftaken=NOW(), side='$side', sid='$_POST[sid]', xrf='$xrf', hazards='$conditions', spottest='$_POST[spottest]', hazardassessment='$_POST[hazardassessment]' WHERE ccid=$_POST[ccid]";
	}
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

	//update sides on components that don't have one yet (assumed to be same item)
	$query = "UPDATE comprehensive_ext_components SET side='$side' WHERE cuid=$cuid AND side IS NULL";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

	//paint chips moved to separate table
	if ($_POST['paintchips'] == 'Yes') {
		$query = "SELECT COUNT(*) FROM paintchips WHERE ccid=$ccid and comptype='Exterior'";
		$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		if (mysql_result($result, 0) == 0) { //not inserted
			//now we need the iid
			$query = "SELECT iid FROM comprehensive_ext_components INNER JOIN units USING (cuid) WHERE ccid=$ccid";
			$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			$iid = mysql_result($result, 0);
			//now find max number
			$query = "SELECT MAX(number) FROM paintchips WHERE iid=$iid";
			$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			$max = mysql_result($result, 0);
			$next = $max + 1;
			$query = "INSERT INTO paintchips (iid, number, ccid, comptype) VALUES ($iid, $next, $ccid, 'Exterior')";
			mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		} //no else, leave alone if already inserted
	} else { //No
		$query = "DELETE FROM paintchips WHERE ccid=$ccid and comptype='Exterior'";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
	//ENDTIME
	$query = "UPDATE units SET endtime=NOW() WHERE cuid=$cuid";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
}

$query = "SELECT cuid FROM comprehensive_ext_components WHERE ccid=$ccid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$cuid = mysql_result($result, 0);

if ($_POST['submit'] == 'Delete') { //must delete after determining unit
	$query = "DELETE FROM comprehensive_ext_components WHERE ccid=$ccid";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$query = "DELETE FROM paintchips WHERE ccid=$ccid and comptype='Exterior'";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	//ENDTIME
	$query = "UPDATE units SET endtime=NOW() WHERE cuid=$cuid";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
}	

if ($_POST['submit'] == 'Enable XRF Readings') {
	$_SESSION['skipXRF'] = false;
}

//header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-comprehensive-ext-component.php?cuid=$cuid");

header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-comprehensive-exterior.php?cuid=$cuid");
?>