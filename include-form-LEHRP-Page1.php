<?php

$pdf->AddPage('P');
$pdf->SetTextColor(0,0,0);
$x = 0;
$y = 0;
$ml=0.5;
$ff=65;
$w=7.5; 
$pdf->SetFont('Arial','',14);
$pdf->SetLineWidth(0.01);

$query = "SELECT building.bid, streetnum, address, suffix, city, state, zip, numunits, yearbuilt, plat, lot, inspection.iid, unitdesc, section8, publichousing, starttime, scheddate, number, designation, type, units.owneroccupied as unitowneroccupied, inspection.owneroccupied as inspowneroccupied FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";

$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);
$bid = $row[bid];
$building_address = $row[streetnum] . ' ' . $row[address] . ' ' . $row[suffix] . ', ' . $row[city] . ', ' . $row[state]. ' ' . $row[zip];
$row_number = $row[iid]."".-$row[number];

$pdf->SetFont('Times','','12');
//$pdf->SetXY(7.12, 0.36);
$x=1.4;
$y=$y + 0.3 ;
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, "Inspection $row_number");

$pdf->SetXY(7,$y);
$pdf->Cell(0,0, 'Page '. $pdf->PageNo().' of  '. '{totalpages}');

// LEHRP INFO
$query = "SELECT lehrp.* FROM lehrp WHERE lehrp.bid = $bid";

$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);
$date_performed = $row['date_performed'];
$property_type = $row['property_type'];
$description = $row['description'];

// BUILDING INFO

$query = "SELECT lehrp_images.image_path FROM lehrp_images JOIN lehrp ON lehrp_images.pid = lehrp.pid WHERE lehrp.bid = $bid";

$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);
$image_path = $row['image_path'];

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','B','16');
$pdf->SetXY($ml, $y);
$pdf->Image('lehra_images/kkclogo.png', 3.1, 0.5, 0, 1.0);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','B','20');
$pdf->SetXY($ml, $y);
$pdf->Cell( $w, 2,"$description", 0, 0, 'C');

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','B','20');
$pdf->SetXY($ml, $y);
$pdf->Cell( $w, 2.5,"$building_address", 0, 0, 'C');


if($image_path == "")
{
}
else
{
$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','B','16');
$pdf->SetXY($ml, $y);
$pdf->Image("$image_path", 1.6, 2.5, 0, 4);
}

// INSPECTOR INFO

$query = "SELECT inspector.*, inspector_phone.*, insp_assigned.* FROM inspector JOIN inspector_phone ON inspector.tid = inspector_phone.tid JOIN inspection ON inspection.bid = $bid LEFT JOIN insp_assigned ON insp_assigned.tid = inspector.tid AND inspection.iid = insp_assigned.iid WHERE inspection.bid = $bid";

$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

$inspector_tid = $row[tid];
$inspector_name = $row[firstname] . ' ' . $row[lastname];
$inspector_address = $row[streetnum] . ' ' . $row[address] . ' ' . $row[suffix] . ', ' . $row[city] . ', ' . $row[state]. ' ' . $row[zip];
$inspector_contact = $row[number];
$inspector_email = $row[email];
$inspector_jobs = $row[job];

if($inspector_jobs == "")
{
$inspector_job = "NH Licensed Risk Assessor";
}
else
{
$inspector_job = "NH Licensed Risk Assessor/" . $inspector_jobs;
}

//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY($ml, 6.1);
$pdf->Cell( $w, 1.5,'Prepared by:', 0, 0);

//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$pdf->SetFont('Times','B','12');
$pdf->SetXY($ml, 6.1);
$pdf->Cell( $w, 2,"$inspector_name", 0, 0);

//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$pdf->SetFont('Times','','12');
$pdf->SetXY($ml, 6.1);
$pdf->Cell( $w, 2.5,"$inspector_job", 0, 0);

//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$pdf->SetFont('Times','','12');
$pdf->SetXY($ml, 6.1);
$pdf->Cell( $w, 3,"Inspector RA # 00$inspector_tid", 0, 0);



// CLIENT INFO

$query = "SELECT client.*, client_phone.* FROM client JOIN owners ON owners.cid = client.cid JOIN client_phone ON client_phone.cid = client.cid WHERE owners.bid = $bid";

$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);
$client_name = $row[firstname] . ' ' . $row[mi] . ' ' . $row[lastname];
$client_address = $row[streetnum] . ' ' . $row[address] . ' ' . $row[suffix] . ', ' . $row[city] . ', ' . $row[state]. ' ' . $row[zip];
$contact = $row[number];

//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(5, 6.1);
$pdf->Cell( $w, 1.5,'Prepared for:', 0, 0);

//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$pdf->SetFont('Times','B','12');
$pdf->SetXY(5, 6.1);
$pdf->Cell( $w, 2,"$client_name", 0, 0);

//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$pdf->SetFont('Times','','12');
$pdf->SetXY(5, 6.1);
$pdf->Cell( $w, 2.5,"$client_address", 0, 0);

//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$pdf->SetFont('Times','','12');
$pdf->SetXY(5, 6.1);
$pdf->Cell( $w, 3,"$contact", 0, 0);


// INSPECTOR CONTACT

//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY($ml, 7.5);
$pdf->Cell( $w, 1.5,'Contact Information:', 0, 0);

//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$pdf->SetFont('Times','B','12');
$pdf->SetXY($ml, 7.5);
$pdf->Cell( $w, 2,'K. Kirkwood Consulting, LLC', 0, 0);

//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$pdf->SetFont('Times','','12');
$pdf->SetXY($ml, 7.5);
$pdf->Cell( $w, 2.5,"$inspector_address", 0, 0);

//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$pdf->SetFont('Times','','12');
$pdf->SetXY($ml, 7.5);
$pdf->Cell( $w, 3, "$inspector_contact", 0, 0);

//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$pdf->SetFont('Times','','12');
$pdf->SetXY($ml, 7.5);
$pdf->Cell( $w, 3.5, "$inspector_email", 0, 0);

/*
// PAGE 2

// TABLE OF CONTENTS

$pdf->AddPage('P');
 
$x = 0;
$y = 0;
$ml=0.5;
$ff=65;
$w=7.5; 
$pdf->SetFont('Arial','',14);
$pdf->SetLineWidth(0.01);

$pdf->SetFont('Times','','12');
//$pdf->SetXY(7.12, 0.36);
$x=1.4;
$y=$y + 0.3 ;
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, "Inspection $row_number");
$pdf->SetXY(7,$y);
$pdf->Cell(0,0, 'Page '. $pdf->PageNo().' of  '. '{totalpages}');

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','B','14');
$pdf->SetXY($ml, $y);
$pdf->Cell( $w, 0.5,'Table of Contents', 0, 0, 'C');

//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$pdf->SetFont('Times','B','12');
$pdf->SetXY($ml, $y);
$pdf->Cell( $w, 1.5, "Introduction and Executive Summary...................................................................................................................", 0, 0);

//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$pdf->SetFont('Times','B','12');
$pdf->SetXY(8, $y);
$pdf->Cell( $w, 1.5, "3", 0, 0);

// PAGE 3

$pdf->AddPage('P');
 
$x = 0;
$y = 0;
$ml=0.5;
$ff=65;
$w=7.5; 
$pdf->SetFont('Arial','',14);
$pdf->SetLineWidth(0.01);

$pdf->SetFont('Times','','12');
//$pdf->SetXY(7.12, 0.36);
$x=1.4;
$y=$y + 0.3 ;
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, "Inspection $row_number");
$pdf->SetXY(7,$y);
$pdf->Cell(0,0, 'Page '. $pdf->PageNo().' of  '. '{totalpages}');

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','B','14');
$pdf->SetXY($ml, $y);
$pdf->Cell( $w, 0.5,'Introduction and Executive Summary', 0, 0, 'C');

$y=1.3 ;
$x=.4;
$pdf->SetFont('Times','','12');
$pdf->SetXY(0.5, $y);
$pdf->multiCell(7.5, 0.300,"          This is a lead inspection on a $property_type located at $building_address. The inspection was conducted at the request of the $client_name, and was provided by $inspector_job, $inspector_name.

           The field work was performed on $date_performed The inspection was performed within the current acceptable industry guidelines including: the US Environmental Protection Agency's (EPA) title X, the Housing and Urban Development (HUD) lead safe housing rule, and the State of New Hampshire's RSA 130-A & Chapter He-P 1600, lead poisoning prevention and control rules (see table of contents)");

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','B','14');
$pdf->SetXY($ml, $y);
$pdf->Cell( $w, 5.5,'What is the difference between an inspection and risk assessment?', 0, 0, 'C');

$y=4.8;
$x=.4;
$pdf->SetFont('Times','','12');
$pdf->SetXY(0.5, $y);
$pdf->multiCell(7.5, 0.300,"          An inspection is a surface-by-surface investigation to determine whether there is lead-based paint in a home or child-occupied facility, and where it is located. Inspections can be legally performed only by certified inspectors or risk assessors. Lead-based paint inspections determine the presence of lead-based paint. It is particularly helpful in determining whether lead-based paint is present prior to purchasing, renting, or renovating a home, and identifying potential sources of lead exposure at any time.

          A risk assessment is an on-site investigation to determine the presence, type, severity, and location of lead-based paint hazards (including lead hazards in paint, dust, and soil) and provides suggested ways to control them. Risk assessments can be legally performed only by certified risk assessors. Lead-based paint risk assessments are particularly helpful in determining sources of current exposure and in designing possible solutions.

            You can also have a combined inspection and risk assessment.");


// PAGE 4

$pdf->AddPage('P');
 
$x = 0;
$y = 0;
$ml=0.5;
$ff=65;
$w=7.5; 
$pdf->SetFont('Arial','',14);
$pdf->SetLineWidth(0.01);


$pdf->SetFont('Times','','12');
//$pdf->SetXY(7.12, 0.36);
$x=1.4;
$y=$y + 0.3 ;
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, "Inspection $row_number");
$pdf->SetXY(7,$y);
$pdf->Cell(0,0, 'Page '. $pdf->PageNo().' of  '. '{totalpages}');

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','B','14');
$pdf->SetXY($ml, $y);
$pdf->Cell( $w, 0.5,'EPA §745.107 Disclosure Requirements for Sellers and Lessors', 0, 0, 'C');


$y=1.3 ;
$x=.4;
$pdf->SetFont('Times','','12');
$pdf->SetXY(0.5, $y);
$pdf->multiCell(7.5, 0.250,"1) The following activities shall be completed before the purchaser or lessee is obligated under any contract to purchase or lease target housing this is not otherwise an exempt transaction pursuant to §745.107. Nothing in this section implies a positive obligation on the seller or lessor to conduct any evaluation or reduction activity.

           a) The seller or lessor shall provide the purchaser or lessee with an EPA approved lead hazard information pamphlet. Such pamphlets include the EPA document entitled \"Protect Your Family from Lead in Your Home\" (EPA #747- K-94-001) or an equivalent pamphlet that has been approved for use in that State by the EPA.

           b) The seller or lessor shall also disclose to the purchaser or lessee the presence of any known lead-based paint and/or lead-based paint hazards in the target housing being sold or leased. The seller or lessor shall also disclose any additional information available concerning the known lead-based paint and/or lead-based paint hazards, such as the basis for the determination that lead-based paint and/or lead-based paint hazards exist, the location of the lead-based paint and/or lead-based paint hazards, and the condition of the painted surfaces.

           c) The seller or lessor shall disclose to each agent the presence of any known lead- based paint and/or lead based paint hazards in the target housing being sold or leased and the existence of any available records or reports pertaining to lead-based paint and or lead-based paint hazards. The seller or lessor shall also disclose any additional information available concerning the known lead-based paint and/or lead based paint hazards, such as the basis for the determination that lead-based paint and/or lead-based paint hazards exist, the location of the lead-based paint and or lead-based paint hazards, and the condition of the painted surfaces.

           d) The seller or lessor shall provide the purchaser or lessee with any records or reports available to the seller or lessor pertaining to lead-based paint and or lead-based paint hazards in the target housing being sold or leased. This requirement includes records or reports regarding common areas. This requirement also includes records or reports regarding other residential dwellings in multifamily target housing, if such information is part of an evaluation or reduction of lead-based paint and/or lead based paint hazards in the target housing.

2) If any of the disclosure activities identified in paragraph (1) of this section occurs after the purchaser or lessee has provided an offer to purchase or lease the housing, the seller or lessor shall complete the required disclosure activities prior to accepting the purchaser's or lessee's offer and allow the purchaser or lessee an opportunity to review the information and possibly amend the offer.");



// PAGE 5

$pdf->AddPage('P');
 
$x = 0;
$y = 0;
$ml=0.5;
$ff=65;
$w=7.5; 
$pdf->SetFont('Arial','',14);
$pdf->SetLineWidth(0.01);


$pdf->SetFont('Times','','12');
//$pdf->SetXY(7.12, 0.36);
$x=1.4;
$y=$y + 0.3 ;
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, "Inspection $row_number");
$pdf->SetXY(7,$y);
$pdf->Cell(0,0, 'Page '. $pdf->PageNo().' of  '. '{totalpages}');

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','B','14');
$pdf->SetXY($ml, $y);
$pdf->Cell( $w, 0.8,'VISUAL ASSESSMENT SUMMARY', 0, 0, 'C');

// top line
$c=$c+.05;
$h2=$c;
$pdf->Line($ml, 0.5, $ml+$w, 0.5);


$c=$c+.05;
$h2=$c;
$pdf->Line($ml, 1.5, $ml+$w, 1.5);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(0.7, 0.75);
$pdf->Cell( $w, 2,'Interior', 0, 0);


// INTERIOR BUILDING INFO

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(1.9, 0.75);
$pdf->Cell( $w, 2,'___', 0, 0);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(3.1, 0.75);
$pdf->Cell( $w, 2,'___', 0, 0);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(4.5, 0.75);
$pdf->Cell( $w, 2,'___', 0, 0);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(5.85, 0.75);
$pdf->Cell( $w, 2,'___', 0, 0);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(7.2, 0.75);
$pdf->Cell( $w, 2,'___', 0, 0);

$c=$c+.05;
$h2=$c;
$pdf->Line($ml, 2, $ml+$w, 2);

$c=$c+.05;
$h2=$c;
$pdf->Line($ml, 2.5, $ml+$w, 2.5);


$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(0.7, 1);
$pdf->Cell( $w, 2.5,'Exterior', 0, 0);


// EXTERIOR BUILDING INFO

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(1.9, 0.75);
$pdf->Cell( $w, 3,'___', 0, 0);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(3.1, 0.75);
$pdf->Cell( $w, 3,'___', 0, 0);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(4.5, 0.75);
$pdf->Cell( $w, 3,'___', 0, 0);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(5.85, 0.75);
$pdf->Cell( $w, 3,'___', 0, 0);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(7.2, 0.75);
$pdf->Cell( $w, 3,'___', 0, 0);


$c=$c+.05;
$h2=$c;
$pdf->Line($ml, 2, $ml+$w, 2);

$c=$c+.05;
$h2=$c;
$pdf->Line($ml, 2.5, $ml+$w, 2.5);


// separated lines
$pdf->Line(1.5, 1, 1.5, 2.5); 
$pdf->Line(2.6, 1, 2.6, 2.5);
$pdf->Line(4, 1, 4, 2.5);
$pdf->Line(5.3, 1, 5.3, 2.5);
$pdf->Line(6.7, 1, 6.7, 2.5);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(1.6, 1);
$pdf->Cell( $w, 0.3,'Deteriorated', 0, 0);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(1.85, 1);
$pdf->Cell( $w, 0.7,'Paint', 0, 0);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(2.8, 1);
$pdf->Cell( $w, 0.3,'Visible Dust', 0, 0);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(2.75, 1);
$pdf->Cell( $w, 0.7,'Accumulation', 0, 0);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(4.3, 1);
$pdf->Cell( $w, 0.3,'Bare Soil', 0, 0);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(5.45, 1);
$pdf->Cell( $w, 0.3,'Friction/Impact', 0, 0);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(5.75, 1);
$pdf->Cell( $w, 0.7,'Points', 0, 0);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(7.1, 1);
$pdf->Cell( $w, 0.3,'Chew', 0, 0);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(7, 1);
$pdf->Cell( $w, 0.7,'Surfaces', 0, 0);

$pdf->Line($ml, 0.5, $ml, 2.5); // left side line
$pdf->Line($ml+$w, 0.5, $ml+$w, 2.5); // right side line


// bottom line
$c=$c+.05;
$h2=$c;
$pdf->Line($ml, 1, $ml+$w, 1);


$c=$c+.05;
$h2=$c;
$pdf->Line($ml, 3.5, $ml+$w, 3.5);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(1, 2);
$pdf->Cell( $w, 2.8,"__________________________________________", 0, 0);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(6.6, 2);
$pdf->Cell( $w, 2.8,"$date_performed", 0, 0);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(0.8, 2);
$pdf->Cell( $w, 3.3,"$inspector_name, $inspector_job RA # 00$inspector_tid", 0, 0);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','','12');
$pdf->SetXY(7.1, 2);
$pdf->Cell( $w, 3.3,"Date", 0, 0);

$y=1.3 ;
$x=.4;
$pdf->SetFont('Times','','12');
$pdf->SetXY(0.5, 4.5);
$pdf->multiCell(7.5, 0.250,"           Accessible building components and repersentative surfaces from selected areas of the building were tested to determine the presence of lead based paint, utilizing XRF testing technology. The XRF Pb-Test is an energy dispersive x-ray fluorescence (EDXRF) spectrometer that uses a sealed, highly purified Cobalt-57 radioisotope source (<12mCi) to excite a test sample's constituent elements. The Pb-Test utilizes the recently developed Cadmium Telluride (CdTe) Schottky diode detectors. In this inspection, a Thermo-Niton, XL-303 XRF, Serial number 8988 was used, which is a complete lead paint analysis system that quickly, accurately, and non-destructively measures the concentration of lead-based paint (LBP) on surfaces. The XRF is used in accordance with the following Performance Characteristic Sheet; Niton XLp 300, 9/24/2004, ed.1.

           *Please note: Accessible building components were tested to determine the presence of lead based paint. Each surface identified to be a potential lead exposure hazard by the visual inspection and having a distinct paint history, was tested for the presence of lead. Please refer to the property specific cover sheet for an explanation of the abbreviations used in your inspection and the property field notes for the actual testing results for the inspection.");



// PAGE 6

$pdf->AddPage('P');
 
$x = 0;
$y = 0;
$ml=0.5;
$ff=65;
$w=7.5; 
$pdf->SetFont('Arial','',14);
$pdf->SetLineWidth(0.01);


$pdf->SetFont('Times','','12');
//$pdf->SetXY(7.12, 0.36);
$x=1.4;
$y=$y + 0.3 ;
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, "Inspection $row_number");
$pdf->SetXY(7,$y);
$pdf->Cell(0,0, 'Page '. $pdf->PageNo().' of  '. '{totalpages}');

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','B','14');
$pdf->SetXY($ml, $y);
$pdf->Cell( $w, 0.8,'XRF Testing Results and Lead-Based Paint Hazards', 0, 0, 'C');


$y=1.3 ;
$x=.4;
$pdf->SetFont('Times','','12');
$pdf->SetXY(0.5, 1.5);
$pdf->SetTextColor(0,0,0);
$pdf->multiCell(7.5, 0.250,"           XRF readings of 1.0mg/cm² or greater are considered to be lead based paint in accordance with the U.S. Environmental Protection Agency (EPA), Department of Housing and Urban Development (HUD) and the State of New Hampshire Department of Health and Human Services. These are indicated in             on this report.");

$y=1.3 ;
$x=.4;
$pdf->SetFont('Times','B','12');
$pdf->SetXY(6.97, 2);
$pdf->SetTextColor(255, 255, 0);
$pdf->multiCell(7.5, 0.250,"yellow");

$pdf->SetTextColor(0,0,0);

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','B','14');
$pdf->SetXY($ml, $y);
$pdf->Cell( $w, 3,'New Hampshire defines a lead exposure hazard as:', 0, 0, 'C');

$y=1.3 ;
$x=.4;
$pdf->SetFont('Times','','12');
$pdf->SetXY(0.5, 3.5);
$pdf->multiCell(7.5, 0.250,"(a) The presence of lead base substances on chewable, accessible, horizontal surfaces that protrude more than ½ inch and are located more than 6 inches but less than 4 feet from the floor or ground;");

$y=1.3 ;
$x=.4;
$pdf->SetFont('Times','','12');
$pdf->SetXY(0.5, 4);
$pdf->multiCell(7.5, 0.250,"(b) Lead based substances which are peeling, chipping, chalking or cracking or any paint located on an interior or exterior surface or fixture that is damaged or deteriorated and is likely to become accessible to a child;");


$y=1.3 ;
$x=.4;
$pdf->SetFont('Times','','12');
$pdf->SetXY(0.5, 4.5);
$pdf->multiCell(7.5, 0.250,"(c) Lead based substances on interior or exterior surfaces that are subject to abrasion or friction or subject to damage by repeated impact; or");

$y=1.3 ;
$x=.4;
$pdf->SetFont('Times','','12');
$pdf->SetXY(0.5, 5);
$pdf->multiCell(7.5, 0.250,"(d) Bare soil in play areas or the rest of the yard that contains lead in concentration equal to or greater than the limits defined in RSA130-A:1, These hazards will be indicated in       on this report.");           
          
$y=1.3 ;
$x=.4;
$pdf->SetFont('Times','B','12');
$pdf->SetXY(4.9, 5.25);
$pdf->SetTextColor(255, 0, 6);
$pdf->multiCell(7.5, 0.250,"red");

$pdf->SetTextColor(0,0,0);

$y=1.3 ;
$x=.4;
$pdf->SetFont('Times','','12');
$pdf->SetXY(0.5, 6);
$pdf->multiCell(7.5, 0.250,"NOTE: The report presented in the property specific field notes lists the results for all surfaces identified as part of the lead inspection. This includes surfaces that are not lead paint, as well as surfaces that contain lead paint, but are not hazards at the time of the inspection. All surfaces which contain a dangerous level of lead in paint and are not lead hazards at the time of the lead inspection, may become a hazard if their condition or use changes. These surfaces should be checked periodically to ensure that they do not become lead hazards (yellow).

           There may be surfaces omitted from the lead inspection due to not being visible, or accessible to the lead inspector risk assessor. Any surface not listed on the lead inspection report should be assumed to contain lead, and considered a lead hazard, as applicable, for hazard purposes.

           Intact surfaces (under the hazard column) which become loose, and then identified by a lead inspector during a re-inspection or clearance inspection, must be fully abated. This requirement exists even if the surface was not a hazard during the initial lead inspection. Therefore, make sure these surfaces have not become loose prior to the clearance inspection.");


// PAGE 7

$pdf->AddPage('P');

$x = 0;
$y = 0;
$ml=0.5;
$ff=65;
$w=7.5; 
$pdf->SetFont('Arial','',14);
$pdf->SetLineWidth(0.01);


$pdf->SetFont('Times','','12');
//$pdf->SetXY(7.12, 0.36);
$x=1.4;
$y=$y + 0.3 ;
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, "Inspection $row_number");
$pdf->SetXY(7,$y);
$pdf->Cell(0,0, 'Page '. $pdf->PageNo().' of  '. '{totalpages}');

$y=1.3 ;
$x=.4;
$pdf->SetFont('Times','','12');
$pdf->SetXY(0.5, 0.8);
$pdf->multiCell(7.5, 0.250,"           When ceramic tile is present, it is tested for informational coatings as any lead content would be limited to the glazing, or paint under the glazing. This is not considered a coating by most regulations. If lead is present, avoid breaking up the tiles as this could release lead.");

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','B','14');
$pdf->SetXY($ml, 1.8);
$pdf->Cell( $w, 0.8,'Explanation of Lead Inspection Risk Assessment Report Form Columns', 0, 0, 'C');

$y=1.3 ;
$x=.4;
$pdf->SetFont('Times','','12');
$pdf->SetXY(0.5, 2.8);
$pdf->multiCell(7.5, 0.250,"           This section provides general information needed to understand the lead inspection risk assessment report. However, you should speak to your lead inspector risk assessor before you start to do any work on your home.");

$y=1.3 ;
$x=.4;
$pdf->SetFont('Times','B','12');
$pdf->SetXY(0.5, 3.8);
$pdf->multiCell(7.5, 0.250,"SIDE");

$y=1.3 ;
$x=.4;
$pdf->SetFont('Times','','12');
$pdf->SetXY(0.5, 3.8);
$pdf->multiCell(7.5, 0.250,"          - Refers to A, B, C, or D side of the building or room. See the diagram on the cover sheet. The \"A\" Side of the building or room is the side facing the street that faces the property its address (usually known as the front of the building). Keeping your back to this street from the \"A\" side move clockwise to the \"B\" side on your left, the \"C\" side opposite you, and the \"D\" side to the right.");

$y=1.3 ;
$x=.4;
$pdf->SetFont('Times','B','12');
$pdf->SetXY(0.5, 5);
$pdf->multiCell(7.5, 0.250,"LOCATION/SURFACE");

$y=1.3 ;
$x=.4;
$pdf->SetFont('Times','','12');
$pdf->SetXY(0.5, 5);
$pdf->multiCell(7.5, 0.250,"                                         - Refers to the building component(s) being tested. Some surfaces may be made up of more than one part. For example, \"Baseboard\" may refer to four separate pieces of wood (one on each wall), but it is still considered one surface.");

$y=1.3 ;
$x=.4;
$pdf->SetFont('Times','B','12');
$pdf->SetXY(0.5, 6);
$pdf->multiCell(7.5, 0.250,"LEAD");

$y=1.3 ;
$x=.4;
$pdf->SetFont('Times','','12');
$pdf->SetXY(0.5, 6);
$pdf->multiCell(7.5, 0.250,"           - The actual lead result. Each surface tested must have a result recorded in the \"Lead\" column. A number shows that the surface was tested with an XRF analyzer. A number (or average number) equal to or greater than 1.0mg/cm2, or are \"COV\" means the surface is currently covered or enclosed by material such as carpet, metal, or paneling. \"NA\" means that the surface was not available for testing due to an obstruction. Not all lead paint must be abated. This column does not tell you why a surface needs abating. The abatement standards below may not apply for interim controls. Speak to your risk assessor for more information.");

$y=1.3 ;
$x=.4;
$pdf->SetFont('Times','B','12');
$pdf->SetXY(0.5, 7.8);
$pdf->multiCell(7.5, 0.250,"TYPE OF HAZARD");

$y=1.3 ;
$x=.4;
$pdf->SetFont('Times','','12');
$pdf->SetXY(0.5, 7.8);
$pdf->multiCell(7.5, 0.250,"                                 - \"F/I\" Circled means that the surface is an abrasion/friction/impact surface and must be abated. \"CH\" Circled means that the surface is \"Chewable Accessible Horizontal\" and must be abated to a minimum of four feet high, two inches in from the edge or corner. \"D\" Circled means that the surface is damaged or deteriorated and must, at minimum, be stabilized. If more than one choice is highlighted, the rules for abatement may vary depending upon what method of abatement you choose. Speak to the lead inspector risk assessor for more information.");


*/

$pdf->AddPage('P');
 
$x = 0;
$y = 0;
$ml=0.5;
$ff=65;
$w=7.5; 
$pdf->SetFont('Arial','',14);
$pdf->SetLineWidth(0.01);

 
//PAGE NUMBERS

$query = "SELECT building.bid, streetnum, address, suffix, city, state, zip, numunits, yearbuilt, plat, lot, inspection.iid, unitdesc, section8, publichousing, starttime, scheddate, number, designation, type, units.owneroccupied as unitowneroccupied, inspection.owneroccupied as inspowneroccupied FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";

$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

$inspowneroccupied=$row['inspowneroccupied'];
$unitowneroccupied=$row['unitowneroccupied'];
$section8=$row[section8];
$publichousing=$row[publichousing];
$insptype=$row[type];
$iid = $row['iid'];

//require_once'debugview.php';

$pdf->SetFont('Times','','12');
//$pdf->SetXY(7.12, 0.36);
$x=1.4;
$y=$y + 0.3 ;
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, "Inspection $row[iid]-$row[number]");

$pdf->SetXY(7,$y);
$pdf->Cell(0,0, 'Page '. $pdf->PageNo().' of  '. '{totalpages}');

$x=2.5 ;
//$pdf->Rect(.4, $y+0.15, 7.6, 0.3);
$y=$y+0.2;
$pdf->SetFont('Times','B','16');
$pdf->SetXY($ml, $y);
$pdf->Cell( $w, 0.3,'Environmental Lead Inspection Report', 1, 0, 'C');
//$pdf->SetXY($x, $y+.1);
//$pdf->Cell(0,0,'Environmental Lead Inspection Report','B','c');
$pdf->SetFont('Times','B','13');
$x=$x+1;
$y=$y+.5;
$pdf->SetXY($x, $y);
$pdf->Cell(0,0,'Type of Inspection');
$pdf->SetXY($x, $y);
$pdf->Cell(0,0,'________________');
 
//ADDRESS


$lh = 0.4;
$y=$y+0.4;
//MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
//SQL statement to identify type of inspection.
/*
$query1 = "select * from units where cuid=$cuid";
$result1 = mysql_query($query1) or sql_crapout($query1.'<br />'.mysql_error());
$row1 = mysql_fetch_assoc($result1);
formfix($row1);
$insptype=$row1['insptype'];
*/
//MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM

$pdf->SetFont('Times','','10');		
$x=0.6;		
if ($insptype=='Comprehensive') {		
$pdf->SetXY($x,  $y);		
$pdf->Cell(0,0, 'X');		
		}
$pdf->SetXY($x+0.3,  $y);		
$pdf->Cell(0,0, 'Comprehensive');		
$pdf->SetXY($x-0.12,  $y);		
$pdf->Cell(0,0, '_____');		
 		
$x=2.0;		
//$insptype='Limited'		
if ($insptype=='Limited') {       		
$pdf->SetXY($x,  $y);		
$pdf->Cell(0,0, 'X');		
		}
$pdf->SetXY($x+0.3,  $y);		
$pdf->Cell(0,0, 'Limited');		
$pdf->SetXY($x-0.12,  $y);		
$pdf->Cell(0,0, '_____');		

$x=3.0;		
if ($insptype=='Annual Inspection') {        		
$pdf->SetXY($x,  $y);		
$pdf->Cell(0,0, 'X');		
		}
$pdf->SetXY($x+0.3,  $y);		
$pdf->Cell(0,0, 'Annual Inspection');		
$pdf->SetXY($x-0.12,  $y);		
$pdf->Cell(0,0, '_____');		

$x=4.6;		
if ($insptype=='Comprehensive - Clearance')  {            		
$pdf->SetXY($x,  $y);		
$pdf->Cell(0,0, 'X');		
        }

if ($insptype=='Conformance Clearance')  {            		
$pdf->SetXY($x,  $y);		
$pdf->Cell(0,0, 'X');		
        }				
		$pdf->SetXY($x+0.3,  $y);
$pdf->Cell(0,0, 'Clearance Inspection');		
$pdf->SetXY($x-0.12,  $y);		
$pdf->Cell(0,0, '_____');		

$x=6.3;		
if ($insptype=='Lead Assessment') {            		
$pdf->SetXY($x,  $y);		
$pdf->Cell(0,0, 'X');		
        }		
		$pdf->SetXY($x+0.3,  $y);
$pdf->Cell(0,0, 'Lead Assessment');
$pdf->SetXY($x-0.12,  $y);
$pdf->Cell(0,0, '_____');


$y=$y+0.1;
$pdf->SetFont('Times','','11');
$pdf->SetXY(0.4, $y);
$pdf->line($ml, $y, $ml+$w, $y);
 
$iid = $row['iid'];
$bid = $row['bid'];
$scheddate = date("m/d/Y",strtotime($row['scheddate']));
$starttime = date("m/d/Y",strtotime($row['starttime']));

$pdf->SetFont('Times','B','13');
$x=3.5 ;
$y=$y+0.2;
$pdf->SetXY($x, $y);
$pdf->Cell(0,0,'Address Inspected');
$pdf->SetXY($x, $y);
$pdf->Cell(0,0,'________________');
$pdf->SetFont('Times','','12');

$pdf->Cell(0, 0, $row['unitdesc']);
$x=1.0;
$y=$y+.35;
$pdf->SetXY($ml, $y);
$pdf->Cell(3, 0, $row['streetnum'].'  '. $row['address'].'  '. $row['suffix'], 0, 0, 'C');
$x=3.75;
$pdf->SetXY($x, $y);
$pdf->Cell(0.75, 0, $row['designation'], 0, 0, 'C');
$x=$x+1.1;
$pdf->SetXY($x, $y);
$pdf->Cell(($w-$x),0, $row['city'].'    '.$row['zip'], 0, 0, 'C' );
/*
$pdf->Cell(0,0, $row['city']. ', '.$row['state'] $row['zip']);
*/
$pdf->SetXY(0.4,$y);
$pdf->Cell(0,0,'___________________________________');
$pdf->SetXY(3.67,$y);
$pdf->Cell(0,0,'__________');
$pdf->SetXY(4.75,$y);
$pdf->Cell(0,0,'______________________________________');
$y=$y+.17;
$pdf->SetXY(1.0,$y);
$pdf->Cell(0,0,'(No & Street Address)');
$pdf->SetXY(3.67,$y);
$pdf->Cell(0,0,'(Apt. / Floor)');
$pdf->SetXY(5.5,$y);
$pdf->Cell(0,0,'(City / Town & Zip Code)');
 
$query = "SELECT COUNT(*) FROM comprehensive_rooms WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$rooms = mysql_result($result, 0);
 
$x=0.5;
$y=$y+.32;
$pdf->SetXY($x+0.05, $y);
$pdf->Cell(0.625,0, $row['numunits'], 0, 0, 'C');
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, '__________');
$pdf->SetXY($x+.15, $y+.2);
$pdf->Cell(0,0, '(Units)');
 
$x=$x+1.1;
$pdf->SetXY($x+0.05, $y);
$pdf->Cell(0.625,0, $rooms, 0, 0, 'C' );
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, '__________');
$pdf->SetXY($x+.05, $y+.2);
$pdf->Cell(0,0, '(# Rooms) ');
 
$x=$x+1.1;
$pdf->SetXY($x+0.05, $y);
$pdf->Cell(0.625,0, $row['yearbuilt'], 0, 0, 'C');
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, '___________');
$pdf->SetXY($x+.05, $y+.2);
$pdf->Cell(0,0, '(Year Built)');
 
$x=$x+1.15;
$pdf->SetXY($x+0.05, $y);
$pdf->Cell(0.625,0, $row['plat'], 0, 0, 'C');
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, '__________');
$pdf->SetXY($x+.1, $y+.2);
$pdf->Cell(0,0, '  (Plat)  ');


$x=$x+1.1;
$pdf->SetXY($x+0.05, $y);
$pdf->Cell(0.625,0, $row['lot'], 0, 0, 'C');
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, '__________');
$pdf->SetXY($x+.15, $y+.2);
$pdf->Cell(0,0, '  (Lot)   ');

//check to see if buiding is owneroccupied
$boo = '';
$query = "SELECT * FROM units INNER JOIN inspection ON inspection.iid = units.iid INNER JOIN building ON building.bid = inspection.bid WHERE units.iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$under6 = mysql_result($result, 0, 'under6');
 while ($row = mysql_fetch_row($result)) {
	$isbuildingowneroccupied = $row['owneroccupied'];
	if ($isbuildingowneroccupied == 'Yes'){
	$boo = 'Yes';
	}
}
 
 
$x=$x+1.1;
$pdf->SetXY($x+0.05, $y);
$pdf->Cell(2.0,0, $under6, 0, 0, 'C');
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, '____________________');
$pdf->SetXY($x+.1, $y+.2);
$pdf->Cell(0,0, '(# Children Under 6)');
 
$pdf->SetFont('Times','','10');
$y=$y+0.65;
$owneroccupied = mysql_result($result, 0, 'owneroccupied');
$pdf->SetXY(0.4,$y);
$pdf->Cell(0,0,'Inspected Unit Occupied by Owner: Y/N        Premise Occupied by Owner: Y/N       Section 8: Y/N       Public Housing: Y/N');
$pdf->SetXY(0.4,$y+.05);
$pdf->line($ml, $y+0.15, $ml+$w, $y+0.15);
$yshift=-0.067;
$pdf->SetLineWidth(0.014);

//  Need to identify if building owner occupied

if($unitowneroccupied == 'Yes') {
$pdf->Rect(2.46, $y+$yshift, 0.14, 0.14);
$inspowneroccupied = $unitowneroccupied;
} else { // No
$pdf->Rect(2.61, $y+$yshift, 0.13, 0.14);
}		
/*
if($boo == 'Yes'){		
		$pdf->Rect(4.65, $y+$yshift, 0.13, 0.14);
} else {$pdf->Rect(4.8, $y+$yshift, 0.13, 0.14);}		
*/
//if($row['$owneroccupied'] == 'Yes') {		

if($inspowneroccupied == 'Yes') {
$pdf->Rect(4.65, $y+$yshift, 0.14, 0.14);		
} else  { 		
$pdf->Rect(4.8, $y+$yshift, 0.14, 0.14);		
}

//require_once'debugview.php';

if($section8 == 'Yes') {
$pdf->Rect(5.73, $y+$yshift, 0.13, 0.14);
} else { // No
$pdf->Rect(5.87, $y+$yshift, 0.13, 0.14);
}
if($publichousing == 'Yes') {
$pdf->Rect(7.15, $y+$yshift, 0.13, 0.14);
} else { // No
$pdf->Rect(7.29, $y+$yshift, 0.13, 0.14);
}
/*
if($row['section8'] == 'Yes') {
$pdf->Rect(5.73, $y+$yshift, 0.13, 0.14);
} else { // No
$pdf->Rect(5.87, $y+$yshift, 0.13, 0.14);
}
if($row['publichousing'] == 'Yes') {
$pdf->Rect(7.15, $y+$yshift, 0.13, 0.14);
} else { // No
$pdf->Rect(7.29, $y+$yshift, 0.13, 0.14);
}
*/ 
$y=$y+.25;
$pdf->SetFont('Times','B','13');
$pdf->SetXY(3.6, $y);
$pdf->Cell(0,0,'Owner Information');		
$pdf->SetXY(3.6, $y);		
$pdf->Cell(0,0,'_________________');		
 		
//OWNER		

$pdf->SetFont('Times','','12');		
$query = "SELECT client.cid, firstname, lastname, streetnum, address, suffix, city, state, zip,startdate FROM client INNER JOIN owners USING (cid) WHERE bid=$bid AND startdate <= DATE(NOW()) AND enddate >= DATE(NOW()) LIMIT 1";		
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());		
if ($row = mysql_fetch_assoc($result)) {		
            formfix($row);
			$clearanceDate = $row['startdate'];		
            $cid = $row['cid'];		
		$startdate=$row['startdate'];
            $basex = 0.5; $basey = 3.9;		
 		
$pdf->SetXY($basex, $basey);		
$pdf->Cell(3.5,0, "$row[firstname] $row[lastname]", 0, 0, 'C');
$pdf->SetXY($basex + 4.0, $basey);
$pdf->Cell(3.5,0, "$row[streetnum] $row[address] $row[suffix]", 0, 0, 'C');
$pdf->SetXY($basex, $basey + 0.48);
$pdf->Cell(3.5,0, "$row[city], $row[state] $row[zip]", 0, 0, 'C');
 
            $query = "SELECT type, number, ext FROM client_phone WHERE cid=$cid GROUP BY type";
            $result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                        formfix($row);
                        if ($row['ext']) {
                                    $nums[$row['type']] = "$row[number] ext. $row[ext]";
                        } else {
                                    $nums[$row['type']] = "$row[number]";
                        }
            }

 

$pdf->SetXY($basex + 3.7, $basey + 0.48);
            if ($nums['Home']) {
$pdf->Cell(0,0, $nums['Home']);
            } else if ($nums['Cell']) {
$pdf->Cell(2.0,0, $nums['Cell'] . ' (Cell)', 0, 0, 'C');

                        $cellused = 1;

            }

$pdf->SetXY($basex + 5.75, $basey + 0.48);

            if ($nums['Work']) {

$pdf->Cell(2.0,0, $nums['Work'], 0, 0, 'C');

            } else if ($nums['Cell'] && $cellused < 1) {

$pdf->Cell(2.0,0, $nums['Cell'] . ' (Cell)', 0, 0, 'C');

            }

}

 

$y=$y+.45;

$pdf->SetFont('Times','','12');

$pdf->SetXY(0.4, $y+.03);

$pdf->Cell(0,0,' ________________________________________         _____________________________________________');

$y=$y+.2;

$pdf->SetXY(1.6, $y);

$pdf->Cell(0,0,'(Name)');

$pdf->SetXY(5.0, $y);

$pdf->Cell(0,0,'(No. & Street Address)');

 

$y=$y+.26;

$x=.4;

$yshift=0.04;

$pdf->SetXY($x, $y+$yshift);

$pdf->Cell(0,0,'_________________________________________');

$pdf->SetXY($x+.55, $y+.2);

$pdf->Cell(0,0,'(City / Town, State & Zip)');

 

$pdf->SetXY($x+3.6, $y+$yshift);

$pdf->Cell(0,0,'______________________');

$pdf->SetXY($x+4.0, $y+.2);

$pdf->Cell(0,0,'(Home Phone)');

 

$pdf->SetXY($x+5.6, $y+$yshift);

$pdf->Cell(0,0,'______________________');

$pdf->SetXY($x+6.0, $y+.2);

$pdf->Cell(0,0,'(Work Phone)');

 

$y=$y+.3;

$pdf->SetXY(0.4,$y);

$pdf->Line($ml, $y, $ml+$w, $y);

 

//Inspector and Company

 

$y=$y+.2;

$pdf->SetFont('Times','B','13');

$pdf->SetXY(3.25, $y);

$pdf->Cell(0,0,'Inspector/Assessor Information');

$pdf->SetXY(3.25, $y);

$pdf->Cell(0,0,'___________________________');

 

$x = 0.6;

$y=$y+.3;
$ydrop=0;
$pdf->SetFont('Times','','12');
//print $query;

//$startdate = mysql_result($result, 0, 'startdate'); //used in inspector section

//INSPECTORS

$query = "SELECT firstname, lastname, elt, eli, units.starttime, elistart AS useeli FROM inspector INNER JOIN insp_assigned USING ( tid ) INNER JOIN units USING ( iid ) WHERE cuid =$cuid ORDER BY eli DESC  LIMIT 0 , 30 ";

$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
//$row1= mysql_fetch_assoc($result);

$datequery = "SELECT DATE_FORMAT(starttime, '%c/%e/%Y') AS startdate, DATE_FORMAT(starttime, '%l:%i %p') AS starttime FROM units WHERE cuid=$cuid";
$dateresult = mysql_query($datequery) or sql_crapout($datequery.'<br />'.mysql_error());
$daterow = mysql_fetch_row($dateresult);

$starttime = $daterow[0];
$pdf->SetXY($basex + 5.75, $y + $ydrop);
$pdf->Cell(0,0, $starttime); //retreived in address section"

/*    Need to skip ELI row if no ELI
		if ($row['eli'] && $row['useeli']) {
			    $ydrop=$ydrop+0.48;
						} 
//*/
while (($row = mysql_fetch_assoc($result)) && $x < 4) {
            formfix($row);
				$pdf->SetXY($x, $y + $ydrop);
				$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
				$pdf->SetXY($x + 3.61, $y + $ydrop);
							if ($row['eli'] && $row['useeli']) {
				$pdf->Cell(0,0, "ELI- $row[eli]");
							} else {
				$pdf->Cell(0,0, "ELT- $row[elt]");

            }


        $ydrop=$ydrop+0.48;

//          $x++;

}
 
$y=$y+0.05;
$pdf->SetFont('Times','','10');
$pdf->SetXY(0.4, $y);
$pdf->Cell(0,0,'___________________________________________________   _______________                         ____________________');
$pdf->SetXY(0.6, $y+0.15);
$pdf->Cell(0,0,'(Inspector/Assessor)                         (Signature)                             (Cert. No.)                                     (Date Inspected)');
$pdf->SetXY(0.4, $y+0.48);
$pdf->Cell(0,0,'___________________________________________________   _______________');
$pdf->SetXY(0.6, $y+0.48+0.15);
$pdf->Cell(0,0,'(Inspector Technician)                      (Signature)                             (Cert. No.)');
$pdf->SetXY(0.4, $y+0.96);
$pdf->Cell(0,0,'___________________________________________________   _______________');
$pdf->SetXY(0.6, $y+0.96+0.15);
$pdf->Cell(0,0,'(Inspector Technician)                      (Signature)                             (Cert. No.)');
$pdf->SetXY(0.4, $y+1.44);
$pdf->Cell(0,0,'___________________________________________________   _______________ ');
$pdf->SetXY(0.6, $y+1.44+0.15);
$pdf->Cell(0,0,'(Inspector Technician)                      (Signature)                            (Cert. No.)');$pdf->SetLineWidth(0.014);
$pdf->Rect(5.3, $y+.25, 2.7, 1.6);
//$pdf->Image('images/fixed/comprehensive-page1v2.png', 0, 0, 8.5, 11.0, 'PNG');

//$pdf->Image('anchor_new2.png', 5.3, $y+.8,.7,.7, 'PNG');

//$pdf->SetLineWidth(0.014);//$pdf->Rect(5.4, $y+.75, 0.6, 0.7);
 
$pdf->SetXY(6.15, $y+.5);
$pdf->SetFont('Times','B','12');
$pdf->Cell(0,0,'Inspecting Firm');
$pdf->SetXY(6.05, $y+.8);
$pdf->SetFont('Times','','11');
$pdf->Cell(0,0,'Rhode Island');
$pdf->SetXY(6.10, $y+1.0);
$pdf->Cell(0,0,'Lead Technicians, Inc');
$pdf->SetXY(6.15, $y+1.2);
$pdf->Cell(0,0,'120 Amaral Street');
$pdf->SetXY(6.20, $y+1.4);
$pdf->Cell(0,0,'East Providence, RI 02915');
 
//Reason
$y=$y+.25+1.6;
$pdf->SetXY(0.4,$y);
$pdf->Cell(0,0,'___________________________________________________________________________________________________');
$query = "SELECT reason, otherreason FROM inspection WHERE iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());list($reason, $otherreason) = mysql_fetch_row($result);
$pdf->SetFont('Times','',12);
 
switch ($reason) {
            case 'Department of Health Initiated':
$pdf->SetXY(0.60, $y+.6);
                        break;
            case 'Licensed Child Care Facility':
$pdf->SetXY(0.60, $y+.75);
                        break;
            case 'School':
$pdf->SetXY(0.60, $y+0.9);
                        break;
            case 'Code Enforcement':
$pdf->SetXY(0.60, $y+1.05);
                        break;
            case 'Investigation of Improper Lead Hazard Reduction/Illegal Abatement':
$pdf->SetXY(3.71, $y+0.6);
                        break;
            case 'Private Client - Property Transfer':
$pdf->SetXY(3.71, $y+0.75);
                        break;
            case 'Dept. of Human Services Initiated':
$pdf->SetXY(3.71, $y+1.05);
                        break;
            default:
$pdf->SetXY(3.71, $y+0.9);
                       if ($reason == 'Other') {
                                   $other = $otherreason;
                       } else { //conformance reason
                                   $other = $reason;
                       }
                       break;
}
$pdf->Cell(0,0, 'X');

if ($other) {
$pdf->SetXY(4.43, $y+0.9);
$pdf->Cell(0,0, $other);
}
 
//
$y=$y+0.25;
$pdf->SetFont('Times','B','13');
$pdf->SetXY(3.4,$y);
$pdf->Cell(0,0,'Reason for Inspection');
$pdf->SetXY(3.4,$y);
$pdf->Cell(0,0,'___________________');
$pdf->SetFont('Times','','10');
$y=$y+0.2;
$pdf->SetXY(3.4,$y);
$pdf->SetFont('Times','','10');
$pdf->SetXY(0.4,$y+.15);
$pdf->Cell(0,0,'______ Dept. of Health Initiated');
$pdf->SetXY(3.5,$y+.15);
$pdf->Cell(0,0,'_______ Investigation of Improper Lead Hazard Reduction / Illegal Abatement');
$pdf->SetXY(0.4,$y+.3);
$pdf->Cell(0,0,'______ Licensed Child Care Facility ');
$pdf->SetXY(3.5,$y+.3);
$pdf->Cell(0,0,'_______ Private Client ? Property Transfer');
$pdf->SetXY(0.4,$y+.45);
$pdf->Cell(0,0,'______ School');
$pdf->SetXY(3.5,$y+.45);
$pdf->Cell(0,0,'_______ Other ___________________________________________________');
$pdf->SetXY(0.4,$y+.6);
$pdf->Cell(0,0,'______ Code Enforcement');
$pdf->SetXY(3.5,$y+.6);
$pdf->Cell(0,0,'_______ Dept. of Human Services Initiated ___________________________');
$y=$y+.7;
$pdf->SetXY(0.4,$y);
$pdf->Cell(0,0,'____________________________________________________________________________________________________________');
 
//Media
$y=$y+.2;
$pdf->SetFont('Times','B','13');
$pdf->SetXY(3.4, $y);
$pdf->Cell(0,0,'Media Tested Includes');
$pdf->SetXY(3.4, $y);
$pdf->Cell(0,0,'___________________');
$pdf->SetFont('Times','','10');
$y=$y+.35;

//PAINT
$query = "SELECT COUNT(*) FROM comprehensive_rooms INNER JOIN comprehensive_components USING (crid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$paint = mysql_result($result, 0);			
$query = "SELECT COUNT(*) FROM comprehensive_ext_components WHERE cuid=$cuid";			
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$paint += mysql_result($result, 0);			
$pdf->SetFont('Times','','12');			
$x=1.4;			
if ($paint) {			
             			
$pdf->SetXY($x,  $y);			
$pdf->Cell(0,0, 'X');			
			}
$pdf->SetXY($x+0.3,  $y);			
$pdf->Cell(0,0, 'Paint');			
$pdf->SetXY($x-0.12,  $y);			
$pdf->Cell(0,0, '_____');

 
//SOIL
//*****uncommented these lines to handle soil samples
//*****added timetaken to the where clause in the 2nd sql query
//*****Question: Why is there data automatically being inserted on comprehensive_sides table?
 
$query = "SELECT u2.cuid FROM units AS u1 INNER JOIN units AS u2 USING (iid) WHERE u1.cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$extcuid = mysql_result($result, 0);
$query = "SELECT COUNT(*) FROM comprehensive_sides WHERE cuid=$extcuid AND inaccessible='No' AND cover='None' and timetaken <> '0000-00-00 00:00:00'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$soils = mysql_result($result, 0);
$query = "SELECT COUNT(*) FROM comprehensive_soil_debris WHERE cuid=$extcuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$soils += mysql_result($result, 0);			
$query = "SELECT COUNT(*) FROM comprehensive_soil_object WHERE cuid=$extcuid";			
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$soils += mysql_result($result, 0);			
 			
//$soils = 1;			
$x=3.0;			
if ($soils) {			
             			
$pdf->SetXY($x,  $y);			
$pdf->Cell(0,0, 'X');			
			}
$pdf->SetXY($x+0.3,  $y);			
$pdf->Cell(0,0, 'Soil');			
$pdf->SetXY($x-0.12,  $y);			
$pdf->Cell(0,0, '_____');			


//WATER			
$query = "SELECT COUNT(*) FROM waters WHERE cuid=$cuid AND iid=$iid"; //iid unnecessary?"			
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$waters = mysql_result($result, 0);			
$x=4.56;			
if ($waters) {			
             			
$pdf->SetXY($x,  $y);			
$pdf->Cell(0,0, 'X');			
			}
$pdf->SetXY($x+0.3,  $y);			
$pdf->Cell(0,0, 'Water');			
$pdf->SetXY($x-0.12,  $y);			
$pdf->Cell(0,0, '_____');			


//DUST			
$query = "SELECT COUNT(*) FROM comprehensive_wipes INNER JOIN comprehensive_rooms USING(crid) WHERE cuid=$cuid AND iid=$iid";	
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$dust = mysql_result($result, 0);			
$x=6.38;			
if ($dust) {			
            			
$pdf->SetXY($x,  $y);			
$pdf->Cell(0,0, 'X');			
			}
$pdf->SetXY($x+0.3,  $y);			
$pdf->Cell(0,0, 'Dust');			
$pdf->SetXY($x-0.12,  $y);			
$pdf->Cell(0,0, '_____');			

 
$y=$y+.25;
$pdf->SetXY(0.4,$y);
$pdf->Cell(0,0,'__________________________________________________________________________________________');
 
$y=$y+.25;
$pdf->SetFont('Times','B','12');
$pdf->SetXY(0.4, $y);
$pdf->Cell(0,0,'Notice to Owners:');
$pdf->SetFont('Times','','9');
$pdf->SetXY(1.8,$y);
$pdf->Cell(0,0,'R.I. lead regulations allow certified lead inspectors to assume that any material contains lead without testing, based on');
$yinc=0.12;
$y=$y+$yinc;
$pdf->SetXY(1.8,$y);

$pdf->Cell(0,0,'professional expertise. This assumption may save the owner inspection costs but may result in additional abatement cost.');
$y=$y+$yinc+.03;
$pdf->SetFont('Times','B','12');
$pdf->SetXY(1.8,$y);
$pdf->Cell(0,0,'You are advised to discuss this issue with your inspector in detail prior to inspection.');
 
$y=$y+0.4;
$pdf->SetFont('Times','B','16');
$pdf->SetXY(1.8,$y);
$pdf->Cell(0,0,'THIS REPORT SERVES AS A NOTICE TO ABATE');

$y=$y+0.25;
$pdf->SetXY(1.4,$y);
$pdf->Cell(0,0,'WHEN SIGNIFICANT LEAD HAZARDS ARE IDENTIFIED');
$pdf->SetFont('Times','','10');
$y=$y+0.3;
$pdf->SetXY(0.4,$y);
$pdf->Cell(0,0,'____________________________________________________________________________________________________________');

$y=$y+0.3;
$pdf->SetXY(0.4,$y);
$pdf->Cell(0,0,'FORM PBLC-23-1 (7/05) Replaces FORM PBLC-23-1 (1/03) p.1, which is obsolete.');


?>
