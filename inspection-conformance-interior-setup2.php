<?php

require_once'session.php';
require_once'connect.php';

$rooms = $_POST['digit1'] . $_POST['digit2'];

$query = "SELECT * FROM room_list";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$roomlist = array();
while ($row = mysql_fetch_row($result)) {
	$roomlist[$row[0]] = str_replace(' ','&nbsp;',$row[0]);
}

$query = "SELECT iid, unitdesc, number FROM units WHERE cuid=$_POST[cuid]";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
list($iid, $unitdesc, $unitnumber) = mysql_fetch_row($result);

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - Interior Setup";
require_once'header.php';
?>
<form action="inspection-conformance-interior-setup-save.php" method="post">
<input type="hidden" name="cuid" value="<?php print $_POST['cuid']; ?>" />
<input type="hidden" name="numrooms" value="<?php print $rooms; ?>" />
<table class="info">
<tr><th>Room Number</th><th>Room Description</th></tr>
<?php
$columns = 5;
for ($x=1; $x<=$rooms; $x++) {
	print "<tr><td>$x</td><td><table>";
	$numitems = 0;
	foreach ($roomlist as $room => $roomlabel) {
		if ($numitems % $columns == 0) {
			print "<tr>";
		}
		print "<td><input type=\"radio\" name=\"rooms[$x]\" value=\"$room\" />$roomlabel</td>";
		$numitems++;
		if ($numitems % $columns == 0) {
			print "</tr>\n";
		}
	}
	if ($numitems % $columns) {
		while ($numitems % $columns) {
			print "<td> </td>";
			$numitems++;
		}
		print "</tr>";
	}
	print "<tr><td colspan=\"4\"><input type=\"radio\" name=\"rooms[$x]\" value=\"Other\" />Other <input type=\"text\" name=\"otherroom[$x]\" maxlength=\"30\" /></td></tr>";
	print "</table></td></tr>\n";
}
?>
</table>
<p><input type="submit" value="Next" /></p>
</form>
<?php
require_once'footer.php';
?>