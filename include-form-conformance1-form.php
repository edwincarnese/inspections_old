<?php

//$pdf->AddPage('P');
$pdf->Image('hrc_logo.png', 6.625, 0.25, 1, 1, 'PNG');
//$pdf->Image('RI_LeadInspectionForm_Page_01.png', 0, 0, 8.5, 11.0, 'PNG');
//Startr Form
$x = 0;
$y = 0;
$ml=.625;
$w=7.25;
$pdf->SetFont('Times','','12');
$pdf->SetLineWidth(0.014);
//Start Banner box package
$y=$y+0.58; 		//Increment Y from last input.
$ys=$y-0.2;  	//Save Y at start.
$ff=65;			//Chang in $y = $fontsize/$ff
$fontsize=13;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml+1.275, $y);
$pdf->Cell(0,0,'RHODE ISLAND HOUSING RESOURCES COMMISSION');
$y=$y+$fontsize/$ff;	//Increment to next line based on font size
$fontsize=11;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml+1.625, $y);
$pdf->Cell(0,0,'MITIGATION CLEARANCE INSPECTION REPORT');
$y=$y+0.2+$fontsize/$ff;
$fontsize=10;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml+2.825, $y);
$pdf->Cell(0,0,'TYPE OF INSPECTION');
$y=$y+$fontsize/$ff;

//$pdf->Rect($ml, $ys, $w, $y-$ys);	//create box
//End Banner box package
$fontsize=11;
$y=$y+.22;
$pdf->SetFont('Times','', $fontsize);
$pdf->Rect(1.45, $y-0.08, 0.12, 0.12);	//create box
$pdf->SetXY($ml+1.0, $y);
$pdf->Cell(0,0,'Independent Clearance Inspection');
$pdf->Rect($ml+3.87, $y-0.08, 0.12, 0.12);	//create box
$pdf->SetXY($ml+4.17, $y);
$pdf->Cell(0,0,"Presumptive Compliance");
$y=$y+0.35;
$pdf->Rect($ml, $y-0.1, $w, 0.2);	//create box
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'Address Inspected');
$y=$y+$fontsize/$ff+.2;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'No./Street');
$pdf->line($ml+0.875, $y+0.08, $ml+3, $y+0.08);	
$pdf->SetXY($ml+3.2, $y);
$pdf->Cell(0,0,'City/State');
$pdf->line($ml+4, $y+0.08, $ml+5.2, $y+0.08);	
$pdf->SetXY($ml+5.4, $y);
$pdf->Cell(0,0,'Zip Code');
$pdf->line($ml+6.2, $y+0.08, $ml+$w, $y+0.08);	
$y=$y+$fontsize/$ff;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'Address');
$y=$y+$fontsize/$ff;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'Total Units');
$pdf->line($ml+0.875, $y+0.08, $ml+1.5, $y+0.08);	
$pdf->SetXY($ml+1.7, $y);
$pdf->Cell(0,0,'Apt/Fl/Unit #');
$pdf->line($ml+2.6, $y+0.08, $ml+3.0, $y+0.08);	
$pdf->SetXY($ml+3.2, $y);
$pdf->Cell(0,0,'Plot/Lot');
$pdf->line($ml+4, $y+0.08, $ml+5.2, $y+0.08);	
$pdf->SetXY($ml+5.4, $y);
$pdf->Cell(0,0,'Year Built');
$pdf->line($ml+6.2, $y+0.08, $ml+$w, $y+0.08);	
$y=$y+$fontsize/$ff+0.1;

$y=$y+0.05;
$pdf->Rect($ml, $y-0.1, $w, 0.2);	//create box
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'Owner Information');
$y=$y+$fontsize/$ff+.2;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'Name');
$pdf->line($ml+1.0, $y+0.08, $ml+3.2, $y+0.08);	
$pdf->SetXY($ml+3.3, $y);
$pdf->Cell(0,0,'No/Street Address');
$pdf->line($ml+4.4, $y+0.08, $ml+$w, $y+0.08);	
$y=$y+$fontsize/$ff+0.02;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'City/State');
$pdf->line($ml+1.0, $y+0.08, $ml+3.2, $y+0.08);	
$pdf->SetXY($ml+3.3, $y);
$pdf->Cell(0,0,'Zip Code');
$pdf->line($ml+4.4, $y+0.08, $ml+$w, $y+0.08);	
$y=$y+$fontsize/$ff+0.02;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'Home Phone');
$pdf->line($ml+1.0, $y+0.08, $ml+3.2, $y+0.08);	
$pdf->SetXY($ml+3.3, $y);
$pdf->Cell(0,0,'Work Phone');
$pdf->line($ml+4.4, $y+0.08, $ml+$w, $y+0.08);	
$y=$y+$fontsize/$ff+0.1;

//$pdf->line($ml, $y, $w, $y);	//create box
$y=$y+0.17;
$pdf->Rect($ml, $y-0.1, $w, 0.2);	//create box
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'Tenant Information');
$y=$y+$fontsize/$ff+.2;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'Name');
$pdf->line($ml+0.625, $y+0.08, $ml+3.2, $y+0.08);	
$pdf->SetXY($ml+3.3, $y);
$pdf->Cell(0,0,'Children under 6');
$pdf->line($ml+5.1, $y+0.08, $ml+5.6, $y+0.08);	
$pdf->SetXY($ml+5.65, $y);
$pdf->Cell(0,0,'YES');
$pdf->line($ml+6.1, $y+0.08, $ml+6.6, $y+0.08);	
$pdf->SetXY($ml+6.65, $y);
$pdf->Cell(0,0,'NO');
$y=$y+$fontsize/$ff+0.18;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'Year(s) resided in unit:');
$pdf->line($ml+1.75, $y+0.08, $ml+3.75, $y+0.08);	
$y=$y+$fontsize/$ff;

//$pdf->line($ml, $y, $w, $y);	//create box
$y=$y+0.17;
$pdf->Rect($ml, $y-0.1, $w, 0.2);	//create box
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'Lead Inspector / Technician Information');
$y=$y+$fontsize/$ff+.2;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'Lead Inspector Name');
$pdf->line($ml+2.25, $y+0.08, $ml+4.5, $y+0.08);	
$pdf->SetXY($ml+4.7, $y);
$pdf->Cell(0,0,'Lic #');
$pdf->line($ml+5.25, $y+0.08, $ml+7, $y+0.08);	
$y=$y+$fontsize/$ff+0.15;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'Lead Inspector Signature');
$pdf->line($ml+2.25, $y+0.08, $ml+4.5, $y+0.08);	
$y=$y+$fontsize/$ff+.2;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'Inspector Technician Name');
$pdf->line($ml+2.25, $y+0.08, $ml+4.5, $y+0.08);	
$pdf->SetXY($ml+4.7, $y);
$pdf->Cell(0,0,'Lic #');
$pdf->line($ml+5.2, $y+0.08, $ml+7, $y+0.08);	
$y=$y+$fontsize/$ff+0.15;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'Inspector Technician Signature');
$pdf->line($ml+2.25, $y+0.08, $ml+4.5, $y+0.08);	
$y=$y+$fontsize/$ff+0.15;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'Date Inspected');
$pdf->line($ml+2.25, $y+0.08, $ml+4.5, $y+0.08);	
$pdf->Cell(0,0,'Time');
$pdf->line($ml+5.2, $y+0.08, $ml+7, $y+0.08);	
$y=$y+$fontsize/$ff+0.1;

//$pdf->line($ml, $y, $w, $y);	//create box
$y=$y+0.19;
$pdf->Rect($ml, $y-0.1, $w, 0.2);	//create box
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'Reason for Inspection');
$y=$y+$fontsize/$ff+.21;
$ybox=-0.08;
$pdf->Rect($ml+.02, $y+$ybox, 0.12, 0.12);	//create box
$pdf->SetXY($ml+0.3, $y);
$pdf->Cell(0,0,'Independent Clearance Inspection');
$pdf->Rect($ml+3.1, $y+$ybox, 0.12, 0.12);	//create box
$pdf->SetXY($ml+3.38, $y);
$pdf->Cell(0,0,'Visual Inspection');
$y=$y+$fontsize/$ff+0.12;
$pdf->Rect($ml+.02, $y+$ybox, 0.12, 0.12);	//create box
$pdf->SetXY($ml+0.3, $y);
$pdf->Cell(0,0,'Tenant Complaint');
$pdf->Rect($ml+3.1, $y+$ybox, 0.12, 0.12);	//create box
$pdf->SetXY($ml+3.38, $y);
$pdf->Cell(0,0,'Private Client - Property Transfer');
$y=$y+$fontsize/$ff+0.09;
$pdf->Rect($ml+.02, $y+$ybox, 0.12, 0.12);	//create box
$pdf->SetXY($ml+0.3, $y);
$pdf->Cell(0,0,'Presumptive Compliance');
$pdf->Rect($ml+3.1, $y+$ybox, 0.12, 0.12);	//create box
$pdf->SetXY($ml+3.38, $y);
$pdf->Cell(0,0,'Private Client');
$y=$y+$fontsize/$ff+0.09;
$pdf->Rect($ml+.02, $y+$ybox, 0.12, 0.12);	//create box
$pdf->SetXY($ml+0.3, $y);
$pdf->Cell(0,0,'Code Enforcement');
$pdf->Rect($ml+3.1, $y+$ybox, 0.12, 0.12);	//create box
$pdf->SetXY($ml+3.38, $y);
$pdf->Cell(0,0,'Other');
$pdf->line($ml+3.8, $y+0.08, $ml+6, $y+0.08);	
$y=$y+$fontsize/$ff+0.17;

$fontsize=11;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'Notice to Owner:');
$y=$y+0.1;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml+1.25, $y);
$pdf->multiCell(5.06,.18,'R. I. Lead Hazard Mitigation Regulations allow certified Lead Inspectors and/or Lead Inspector Technicians to verify that the unit tested has met the required standards based on professional expertise.');
$y=$y+0.36;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml+1.25, $y);
$pdf->multiCell(5.06,.18,'                                                               You are advised to discuss this issue with your inspector in detail prior to inspection.');
$y=$y+0.7;

$fontsize=11;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'THIS REPORT SERVES AS A NOTICE THAT THE UNIT MITIGATED HAS SUCCESSFULLY PASSED');
$y=$y+0.2;
$pdf->SetXY($ml+0.2, $y);
$pdf->Cell(0,0,'THE STANDARDS ESTABLISHED UNDR THE LEAD HAZARD MITIGATION REGULATIONS.');

$y=10.6;
$pdf->SetFont('Times','B', 9);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'HRC LHM FORM -2 (4/04)');

//End Form
?>