<?php

require_once'session.php';
require_once'connect.php';

$crid = $_POST['crid'] or $crid = $_GET['crid'] or $crid = 0;

$query = "SELECT *, units.number AS unitnumber FROM units INNER JOIN comprehensive_rooms USING (cuid) WHERE crid=$crid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}

$room = mysql_fetch_assoc($result);
$unitnumber = $room['unitnumber']; //units is second table and will overwrite room #
$iid = $room['iid'];
$number = $room['number'];
$cuid = $room['cuid'];

$addressquery = "SELECT address FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - $room[unitdesc] - $room[name] (Room $room[number]) - Component Sides";
require_once'header.php';
?>
<form action="inspection-comprehensive-room-component-sides-save.php" method="post">
<input type="hidden" name="crid" value="<?php print $crid;?>" />
<?php

$query = "SELECT ccid, comprehensive_components.name, side FROM comprehensive_components LEFT JOIN substrates USING (sid) WHERE crid=$crid ORDER BY comprehensive_components.displayorder, comprehensive_components.ccid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result)) {
?>
<table class="info">
<tr><th>Component</th><th>Side</th></tr>
<?php
$countLoop = 0;
	while ($row = mysql_fetch_assoc($result)) {
		print "<tr><td>$row[name]</a></td><td>\n";
		//$sides = array('None','1','2','3','4','Center');

//$countLoop = 0; 

//echo "<script> alert('$letterSide : $letterSide[0] : $loopSize'); </script>";

$sides = array('A','B','C','D');
foreach ($sides as $side) {


$loopSize = strlen($row['side']);
$letterSide = $row['side'];

//echo "<script> alert('SIDE: $side LETTER: $letterSide : LOOP SIZE: $countLoop'); </script>";

if ($letterSide[0] == $side || $letterSide[1] == $side || $letterSide[2] == $side || $letterSide[3] == $side) 
{
print "<input type=\"checkbox\" name=\"sides[$row[ccid]-$side]\" value=\"$side\" checked=\"checked\" /><span class=\"current\">$side</span>\n";
} 
else 
{
print "<input type=\"checkbox\" name=\"sides[$row[ccid]-$side]\" value=\"$side\" />$side\n";
}
//$countLoop++;
		}
		print "</td></tr>\n\n";
	}
?>
</table>
<?php
}
?>
<p>
<input type="submit" name="submit" value="Save" /><input type="reset" value="Reset" /><input type="submit" name="submit" value="Cancel" />
</p>
</form>
<?php
require_once'footer.php';
?>