<?php


require_once'session.php';
require_once'connect.php';
require_once'helper.php';

/*
print_r($_POST);
exit();
*/

$cid = isset($_POST['cid']) ? $_POST['cid'] : (isset($_GET['cid']) ? $_GET['cid'] : 0);
unset($_POST['cid']);

//shortcut - all fields are text
foreach ($_POST as $field => $value) {
	$fieldlist[] = $field;
	$valuelist[] = htmlspecialchars($value);
}
$valuelist[0] = (int) $valuelist[0];
/*
Because htmlspecialchars() is being performed on the data, it is possible that some data may be truncated. This can happen if, for example, the data contains '&', which becomes '&amp;', and the resulting string is greater than the allowed column width. However, the column widths are rather generous, and it only affects a few characters, so as such, it was deemed not worth checking the various lengths of each field to see if they would actually be truncated. It should be noted that if no substitutions are performed, the 'maxlength' attribute on the form elements will prevent an entry from being truncated.
*/

$fieldlist[count($fieldlist)] = "image_name";
$valuelist['image_name'] = uploadImage();


$query = "INSERT INTO building (".implode(',', $fieldlist).") VALUES ('".implode("','", $valuelist)."')";

mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$query = "SELECT LAST_INSERT_ID()";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$bid = mysql_result($result, 0);

if ($cid) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/building-addowner-verify.php?bid=$bid&cid=$cid");
	exit();
}

header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/building-view.php?bid=$bid");

function random_string($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}
function uploadImage() {
	$target_dir = "images/buildings/";
	$target_file = $target_dir . random_string(4).basename($_FILES["building_image"]["name"]);

	$uploadOk = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	// Check if image file is a actual image or fake image
	if(isset($_POST["submit"])) {
	    $check = getimagesize($_FILES["building_image"]["tmp_name"]);
	    if($check !== false) {
	        echo "File is an image - " . $check["mime"] . ".";
	        $uploadOk = 1;
	    } else {
	        echo "File is not an image.";
	        $uploadOk = 0;
	    }
	}
	// Check if file already exists
	if (file_exists($target_file)) {
	    echo "Sorry, file already exists.";
	    $uploadOk = 0;
	}



	// Check file size
	if ($_FILES["building_image"]["size"] > 500000) {
	    echo "Sorry, your file is too large.";
	    $uploadOk = 0;
	}
	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	&& $imageFileType != "gif" ) {
	    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
	    $uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
	    echo "Sorry, your file was not uploaded.";
	// if everything is ok, try to upload file
	} else {
	    if (move_uploaded_file($_FILES["building_image"]["tmp_name"], $target_file)) {
	        echo "The file ". basename( $_FILES["building_image"]["name"]). " has been uploaded.";

	    } else {
	        echo "Sorry, there was an error uploading your file.";
	        
	    }
	}

	return $target_file;

}
?>