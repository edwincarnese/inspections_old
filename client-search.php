<?php 

require_once'session.php';

$title = "Search clients";
require_once'header.php';
?>

<p>Enter as much information as is known.</p>

<form action="client-searchresults.php" method="post">
<table>
<tr><td>First name:</td><td><input type="text" name="firstname" maxlength="30" /></td></tr>
<tr><td>Last name:</td><td><input type="text" name="lastname" maxlength="30" /></td></tr>
<tr><td>Company:</td><td><input type="text" name="company" maxlength="75" /></td></tr>
<tr><td>City:</td><td><input type="text" name="city" maxlength="30" /></td></tr>
<tr><td>State:</td><td><input type="text" name="state" maxlength="2" size="3" /></td></tr>
<tr><td>Zip:</td><td><input type="text" name="zip" maxlength="5" size="6" /></td></tr>
<tr><td>Phone:</td><td><input type="text" name="phone1" maxlength="3" size="4" />-<input type="text" name="phone2" maxlength="3" size="4" />-<input type="text" name="phone3" maxlength="4" size="5" /></td></tr>
</table>
<p><input type="submit" value="Search" /> <input type="reset" value="Reset" /></p>
</form>
<p><a href="client-add.php">Add New Client</a></p>
<p><a href="client-list.php">View Client List</a></p>

<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>