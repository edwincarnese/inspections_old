<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

$query = "SELECT iid, unitdesc, number, diagrams FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
list($iid, $unitdesc, $unitnumber, $diagrams) = mysql_fetch_row($result);

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - $unitdesc";
require_once'header.php';
?>
<form action="inspection-comprehensive-unit-savediagrams.php" method="post">
<input type="hidden" name="cuid" value="<?php print $cuid; ?>" />
<p>Diagrams: <input type="text" name="diagrams" value="<?php print $diagrams; ?>" size="3" /> <input type="submit" name="submit" value="Update" /></p>
</form>

<p><a href="inspection-comprehensive-unit.php?cuid=<?php print $cuid; ?>"><?php print $unitdesc; ?> Main Menu</a></p>

<p><a href="inspection-main.php?iid=<?php print $iid; ?>">Inspection Main Menu</a></p>

<?php
require_once'footer.php';
?>