<?php 

require_once'session.php';
require_once'connect.php';

//Breaking with convention, this script does not actually save any information to the database. It merely stores the variables in with the session until all parts have been placed.

if ($_POST['submit'] == 'Save') {

	$ctid = $_SESSION['ctid'] or $_POST['ctid'] or $ctid = $_GET['ctid'] or $ctid = 0;
	$part = $_SESSION['part'] or $_POST['part'] or $part = $_GET['part'] or $part = 1;
	$date = $_POST['date'] or $date = $_GET['date'] or $date = 1;

	$query = "SELECT * FROM class_types WHERE ctid=$ctid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	if (mysql_num_rows($result) == 0) {
		header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/class-schedule.php");
		exit();
	}
	$row = mysql_fetch_assoc($result);

	if ($_POST['startampm'] == 'PM' && $_POST['starthour'] < 12) {
		$hour = $_POST['starthour'] + 12;
	} else {
		$hour = $_POST['starthour'] + 0;
	}
	$minute = $_POST['startminute'] + 0;

	$_SESSION['classes'][$part]['start'] = "$date $hour:$minute:00";
	$_SESSION['classes'][$part]['minstudents'] = $_POST['minstudents'];
	$_SESSION['classes'][$part]['recstudents'] = $_POST['recstudents'];
	$_SESSION['classes'][$part]['maxstudents'] = $_POST['maxstudents'];
	$_SESSION['classes'][$part]['location'] = $_POST['location'];
	$part++;
	$_SESSION['part'] = $part;
}

if ($part > $row['parts']) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/class-schedule-save.php");
} else {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/class-schedule2.php");
}

?>