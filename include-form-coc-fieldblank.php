<?php

//subtract 0.06 inches from desired position of filst letter
// with 0.1 lineheight, measure from middle using Cell().

/*
Places to put X's using 6-point text:
ITEM		X		Y
Stardard:	0.87	2.88
------------------------
Paint:		2.155	3.51
Soil:		2.155	3.80
Water:		3.18	3.04	(Water, Drinking)
Wipe:		3.18	3.34
------------------------
Lead:		6.76	2.40
*/

require_once'include-form-coc-common.php'; //sets $iid, in case needed

$query = "SELECT insptype FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$insptype = strtolower(mysql_result($result, 0));

$pdf->SetFont('Arial','',6);
$pdf->SetXY(3.18, 3.34);
$pdf->Cell(0,0, 'X');

//WIPES
$query = "SELECT *, DATE_FORMAT(timetaken, '%c/%e') AS `date`, DATE_FORMAT(timetaken, '%l%p') as `time` FROM comprehensive_wipes WHERE iid=$iid AND number = 0 UNION SELECT *, DATE_FORMAT(timetaken, '%c/%e') AS `date`, DATE_FORMAT(timetaken, '%l%p') as `time` FROM conformance_wipes WHERE iid=$iid AND number = 0";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) > 9) { // Remove this at some point!!
	exit('Too many samples for one page.');
}

$pdf->SetFont('Arial','','8');
$x = 0.85; $y = 4.43;
while($row = mysql_fetch_assoc($result)) {
	$pdf->SetXY($x, $y);
	$pdf->Cell(0.69,0,"$iid-*"."D$row[number]",0,0,'C');
	$pdf->SetX($x + 0.78);
	$pdf->Cell(0.54,0,"$row[date]",0,0,'C');
	$pdf->SetX($x + 1.29);
	$pdf->Cell(0.47,0,"$row[time]",0,0,'C');
	$pdf->SetX($x + 1.78);
	$pdf->Cell(0,0,"$row[surface]");
	$pdf->SetXY($x + 3.57, $y - 0.06);
//	$pdf->MultiCell(0,0,"$row[areawidth]\"x");
	$pdf->SetXY($x + 3.57, $y + 0.06);
//	$pdf->MultiCell(0,0,"$row[arealength]\"");
	$pdf->SetXY($x + 7.02, $y);
	$pdf->Cell(0,0,"1");
	$y += 0.31;
}
?>