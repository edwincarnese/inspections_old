<?php

$x = 0;
$y = 0;
$ml=.625;
$w=7.25;
$pdf->SetFont('Times','','12');
$pdf->SetLineWidth(0.014);
//Start Banner box package
$y=$y+0.2; 		//Increment Y from last input.
$ff=65;			//Chang in $y = $fontsize/$ff
$pdf->Image('anchor_new2.png', 4, $y,.5,.55, 'PNG');
$y=$y+0.7; 		//Increment Y from last input.
//
$fontsize=25;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml+1.05, $y);
$pdf->Cell(0,0,'Rhode Island Lead Technicians, Inc.');
$y=$y+$fontsize/$ff-0.25;
$pdf->SetLineWidth(0.02);
$pdf->line($ml+0.75, $y, $ml+$w-0.75, $y);
$pdf->SetLineWidth(0.014);
$y=$y+0.165;
$fontsize=10;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml+2.3, $y);
$pdf->Cell(0,0,'120 Amaral Street, East Providence, RI  02915');
$y=$y+$fontsize/$ff;
$pdf->SetXY($ml+1.2, $y);
$pdf->Cell(0,0,'(401)438-9988    Fax: (401)438-9938  www.rileadtechs.com     leadsafe@rileadtechs.com');
$y=$y+$fontsize/$ff;
$pdf->SetXY($ml+2.4, $y);
$pdf->Cell(0,0,'Lead Paint Questions?  Lead Paint Answers!');
$y=$y+0.2+$fontsize/$ff;

?>