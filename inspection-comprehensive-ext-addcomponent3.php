<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

if ($_POST['submit'] == 'Save') {
	foreach ($_POST['components'] as $component) {
		if ($component['parts']) {
			foreach ($component['parts'] as $part) {
				//could also implode array and use one query per item; [ IN () ]
				// would be unable to use index but would reduce number of queries
				$query = "SELECT * FROM room_item_components WHERE componentid=$part";
				$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
				$row = mysql_fetch_assoc($result);
				if($row['defaultassessment']) {
					$assess = "'$row[defaultassessment]'";
				} else {
					$assess = 'NULL';
				}
				foreach ($row as &$field) {
					$field = addslashes($field);
				}

				$query = "INSERT INTO comprehensive_ext_components (cuid, name, side, sid, condition, grouplist, conformancecomponent, displayorder, hazardassessment) VALUES ($cuid, '$component[itemname] $row[componentname]', '$component[side]', $row[dsid], '$row[defaultcondition]', '$row[grouplist]', '$row[conformancecomponent]', $row[displayorder], $assess)";
				mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			}
		}
	}

}

header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-comprehensive-ext-component.php?cuid=$cuid");
?>