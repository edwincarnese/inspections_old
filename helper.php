<?php

function getPostIsset($key, $default = 0){
	return isset($_POST[$key]) ? $_POST[$key] : (isset($_GET[$key]) ? $_GET[$key] : $default);
}

function issetBlank($val, $key = "") {
	return isset($val[$key]) ? $val[$key] : "";
}

?>
