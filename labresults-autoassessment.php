<?php

require_once'session.php';
require_once'connect.php';

$iid = $_POST['iid'] or $iid = $_GET['iid'] or $iid = 0;

//WATER
$query = "SELECT * FROM units INNER JOIN waters USING (cuid) WHERE units.iid=$iid and results!=''";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result)) {
	while ($row = mysql_fetch_assoc($result)) {
		if ($row['drawflush'] == 'Draw' && $row['results'] == '<5.0' /* || $row['results'] < 5 */) {
			$hazardassessment = 'Lead-free';
		} else if ($row['results'] < 15) {
			$hazardassessment = 'Lead-safe';
		} else {
			$hazardassessment = 'Hazard';
		}
		$query = "UPDATE waters SET hazardassessment='$hazardassessment' WHERE iid=$row[iid] AND number=$row[number]";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
}



//WIPES
$x = 1;
$query = "SELECT *, 'conformance_wipes' AS thetable FROM units INNER JOIN conformance_rooms USING (cuid) INNER JOIN conformance_wipes USING (crid) WHERE units.iid=$iid AND results!=''";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result)) {
	while ($row = mysql_fetch_assoc($result)) {
		dowipe($row);
	}
}

$query = "SELECT *, 'comprehensive_wipes' AS thetable FROM units INNER JOIN comprehensive_rooms USING (cuid) INNER JOIN comprehensive_wipes USING (crid) WHERE units.iid=$iid AND results!=''";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result)) {
	while ($row = mysql_fetch_assoc($result)) {
		dowipe($row);
	}
}

$query = "SELECT *, 'conformance_wipes' AS thetable FROM conformance_wipes WHERE iid=$iid AND number=0 AND results!=''";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result)) {
	while ($row = mysql_fetch_assoc($result)) {
		dowipe($row);
	}
}

$query = "SELECT *, 'comprehensive_wipes' AS thetable FROM comprehensive_wipes WHERE iid=$iid AND number=0 AND results!=''";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result)) {
	while ($row = mysql_fetch_assoc($result)) {
		dowipe($row);
	}
}

function dowipe ($row) {
	if (substr($row['results'], 0, 1) == '<') {
		$lt = 1;
		$res = substr($row['results'], 1);
	} else {
		$res = $row['results'];
	}
	$hazardassessment = 'Lead-safe';
	if ($row['conversion']) {
		$row['squarefootresult'] = round($res / $row['conversion'], 2);
		if (substr($row['results'], 0, 3) == '<20' /* || $row['results'] < 20 */) {
			$hazardassessment = 'Lead-free';
		} else if ($row['squarefootresult'] < 20) {
			$hazardassessment = 'Lead-free';
		} else if ($row['surface'] == 'Sill') {
			if ($row['squarefootresult'] > 250) {
				$hazardassessment = 'Hazard';
			}
		} else if ($row['surface'] == 'Window Well') {
			if ($row['squarefootresult'] > 400) {
				$hazardassessment = 'Hazard';
			}
		} else if ($row['squarefootresult'] > 40) {
			$hazardassessment = 'Hazard';
		} else if ($row['number'] == 0) { //field blank must test negative
			$hazardassessment = 'Hazard';
		}
		if ($lt == 1) {
			$row['squarefootresult'] = '<' . $row['squarefootresult'];
		}
		$query = "UPDATE $row[thetable] SET hazardassessment='$hazardassessment', squarefootresult='$row[squarefootresult]' WHERE iid=$row[iid] AND number=$row[number]";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
}


//PAINT CHIPS
$query = "SELECT *, 'comprehensive_ext_components' AS thetable FROM units INNER JOIN comprehensive_ext_components USING (cuid) INNER JOIN paintchips ON (paintchips.ccid = comprehensive_ext_components.ccid AND paintchips.comptype='Exterior') WHERE units.iid=$iid AND results!=''";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result)) {
	while ($row = mysql_fetch_assoc($result)) {
		dochips($row);
	}
}

$query = "SELECT *, 'comprehensive_components' AS thetable FROM units INNER JOIN comprehensive_rooms USING (cuid) INNER JOIN comprehensive_components USING (crid) INNER JOIN paintchips ON (paintchips.ccid = comprehensive_components.ccid AND paintchips.comptype='Interior') WHERE units.iid=$iid AND results!=''";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result)) {
	while ($row = mysql_fetch_assoc($result)) {
		dochips($row);
	}
}

function dochips ($row) {
	if ( /* $row['results'] == '<150' || */ $row['results'] <= 150) {
		$hazardassessment = 'Lead-free';
	} else if ($row['condition'] == 'Damaged - Friction') {
		$hazardassessment = 'Hazard';
	} else if ($row['condition'] == 'Intact' || $row['condition'] == 'Intact Where Visible') {
		$hazardassessment = 'Lead-safe';
	} else if ($row['results'] > 600) {
		$hazardassessment = 'Hazard';
	} else {
		$hazardassessment = 'Lead-safe';
	}
	$query = "UPDATE $row[thetable] SET hazardassessment='$hazardassessment' WHERE ccid=$row[ccid]";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
}

//SOIL
$query = "SELECT *, 'comprehensive_sides' AS thetable FROM units INNER JOIN comprehensive_sides USING (cuid) WHERE units.iid=$iid AND results!=''";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result)) {
	while ($row = mysql_fetch_assoc($result)) {
		dosoil($row);
	}
}

$query = "SELECT *, 'comprehensive_soil_debris' AS thetable FROM units INNER JOIN comprehensive_soil_debris USING (cuid) WHERE units.iid=$iid AND results!=''";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result)) {
	while ($row = mysql_fetch_assoc($result)) {
		dosoil($row);
	}
}

$query = "SELECT *, 'comprehensive_soil_object' AS thetable FROM units INNER JOIN comprehensive_soil_object USING (cuid) WHERE units.iid=$iid AND results!=''";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result)) {
	while ($row = mysql_fetch_assoc($result)) {
		dosoil($row);
	}
}

function dosoil($row) {
	if ( /* $row['results'] == '<150' || */ $row['results'] <= 150) {
		$hazardassessment = 'Lead-free';
	} else if ($row['results'] <= 400) {
		$hazardassessment = 'Lead-safe';
	} else if ($row['results'] <= 1000 && strlen($row['cover']) > 4) { // isn't 'None', but exists; options are None, Mulch, Grass, Pavement
		$hazardassessment = 'Lead-safe';
	} else {
		$hazardassessment = 'Hazard';
	}
	$query = "UPDATE $row[thetable] SET hazardassessment='$hazardassessment' WHERE csid=$row[csid]";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
}

if ($_GET['goto']) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/labresults-inspection.php?iid=$_GET[goto]");
} else {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/labresults-list.php");
}
?>