<?php 

require_once'session.php';
require_once'connect.php';

$caid = isset($_GET['caid']) ? $_GET['caid'] :  0;
$popup = $_GET['isPopup'];

$uid = $sid; // $sid from session.php; session ID, user ID, whatever

$query = "SELECT * FROM client_activities WHERE caid=$caid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/client-list.php");
	exit();
}
$row = mysql_fetch_assoc($result);

if (!$popup){
	$title = "Edit an Activity";
	require_once'header.php';
} else {
		?>
		<head>
	<link rel="stylesheet" type="text/css" href="style.css" />
	</head>
	<div id="content">
	<?php
}
?>

<script type="text/javascript">
function fillDate() {
	document.getElementById("month").value = <?php print date('m'); ?>;
	document.getElementById("day").value = <?php print date('d'); ?>;
	document.getElementById("year").value = <?php print date('Y'); ?>;
}
</script>

<form action="client-activity-update.php" method="post">

<input type="hidden" name="caid" value="<?php print $caid; ?>" />
<table>
<?php
if ($popup) {
	print "<input type='hidden' name='popup' value='true'/>";
}
list ($dueyear, $duemonth, $dueday) = explode('-', $row['date_due']);
?>
<tr><td>Date Due:</td><td><input type="text" name="duemonth" maxlength="2" size="3" value="<?php print $duemonth; ?>" />/<input type="text" name="dueday" maxlength="2" size="3" value="<?php print $dueday; ?>" />/<input type="text" name="dueyear" maxlength="4" size="5" value="<?php print $dueyear; ?>" /></td></tr>

<tr><td>Assigned to:</td><td><select name="assignedto">
<option value="">Choose a user:</option>
<?php
$query = "SELECT uid, firstname, lastname FROM users ORDER BY lastname";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while($userrow = mysql_fetch_assoc($result)) {
	if ($row['assignedto'] == $userrow['uid'] ) {
		print "<option value=\"$userrow[uid]\" selected=\"selected\">$userrow[firstname] $userrow[lastname]</option>\n";
	} else {
		print "<option value=\"$userrow[uid]\">$userrow[firstname] $userrow[lastname]</option>\n";
	}
}
?>
</select></td></tr>

<tr><td>Description:</td><td><textarea name="description" rows="5" cols="30"><?php print $row['description']; ?></textarea></td></tr>
<tr><td>Display:</td><td>
<?php
$displays = array('Incomplete','Always');
foreach ($displays as $display) {
	if ($display == $row['display']) {
		print "<input type=\"radio\" name=\"display\" value=\"$display\" checked=\"checked\" />$display ";
	} else {
		print "<input type=\"radio\" name=\"display\" value=\"$display\" />$display ";
	}
}
?>
</td></tr>
<tr><td>Status:</td><td>
<?php
$statuss = array('Incomplete','Done');
foreach ($statuss as $status) {
	if ($status == 'Done') {
		$fn = 'onClick="fillDate()"';
	} else {
		$fn = '';
	}
	if ($status == $row['status']) {
		print "<input type=\"radio\" name=\"status\" value=\"$status\" $fn checked=\"checked\" />$status ";
	} else {
		print "<input type=\"radio\" name=\"status\" value=\"$status\" $fn />$status ";
	}
}
?>
</td></tr>
<?php
if ($row['statusdate'] > '1000-01-01') {
	list ($doneyear, $donemonth, $doneday) = explode('-', $row['statusdate']);
} else {
	$doneyear = $donemonth = $doneday = '';
}
?>
<tr><td>Status Date:</td><td><input type="text" name="donemonth" id="month" maxlength="2" size="3" value="<?php print $donemonth; ?>" />/<input type="text" name="doneday" id="day" maxlength="2" size="3" value="<?php print $doneday; ?>" />/<input type="text" name="doneyear" id="year" maxlength="4" size="5" value="<?php print $doneyear; ?>" /></td></tr>
</table>

<p><input type="submit" name="submit" value="Save" /> <input type="reset" value="Reset" /> <input type="submit" name="submit" value="Cancel" /></p>
</form>

<?php
if (!$popup){
	print '<p><a href="index.php">Main Menu</a></p>';
	require_once'footer.php';
} else {
?></div>
<?php }?>
