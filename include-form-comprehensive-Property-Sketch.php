<?php

$pdf->AddPage('P');
//$pdf->Image('images/fixed/comprehensive-calibration.png', 0, 0, 8.5, 11.0, 'PNG');

// x - 0.03; y - 0.06 from line start
$iid = $row['iid'];

//ADDRESS, TOP
$query = "SELECT inspection.iid, streetnum, address, suffix, city, DATE_FORMAT(starttime, '%c/%e/%Y') AS start, unitdesc, number FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

//Inspection #
$pdf->SetFont('Times','B','10');
$pdf->SetXY(1.5, .2);
$pdf->Cell(0,0, "Inspection $row[iid]-$row[number]");
$ml=0.625;
//Calibration Inspection Banner
$x=2.5 ;
$y=.2;
$pdf->Rect($ml, $y+0.15, 7.25, 0.3);
$y=$y+.3 ;
$pdf->SetFont('Times','B','14');
$pdf->SetXY($x, $y);
$pdf->Cell(0,0,'Floor Plan - Property Sketch (Grid)');

//PAGE NUMBERS
$pdf->SetFont('Times','','10');
$pdf->SetXY(6.5, $y);
$pdf->Cell(0,0, 'Page _____ of _____');
$pdf->SetXY(6.9, $y);
$pdf->Cell(0, 0, $pdf->PageNo());
$pdf->SetXY(7.4, $y);
$pdf->Cell(0,0, '{totalpages}');

$iid = $row['iid'];

//Unit Specifications
//ADDRESS, TOP
$query = "SELECT inspection.iid, streetnum, address, suffix, city, unitdesc, number FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

$pdf->SetFont('Times','','12');

$x=$ml;
$y=$y+.5;
$pdf->SetXY($x, $y);
$pdf->Cell($x, 0, 'Floor / Apt. No.');
$pdf->SetXY($x, $y);
$pdf->line($ml+1.2, $y+0.1, $ml+2.3,  $y+0.1);	
$pdf->SetXY($x+1.13, $y);
$pdf->Cell(0, 0, $row['unitdesc']);
$x=3;
$pdf->SetXY($x, $y);
$pdf->Cell($x, 0, 'Street /City:');
$pdf->SetXY($x, $y);
$pdf->Rect($x+.95, $y+.1, 3.85, 0);
$pdf->SetXY($x+1.3, $y);
$pdf->Cell(0,0, "$row[streetnum] $row[address] $row[suffix], $row[city]");


$x=$ml;
$y=$y+.625;
$pdf->SetLineWidth(0.001);


$c=$ml;
while ($c < 8.0) {
	
	$pdf->Rect($c, 1.375, 0, 7.8);
	
	$c=$c+.2;
}

$c=1.375;
while ($c < 9.2) {
	
	$pdf->Rect($ml, $c, 7.2, 0);
	
	$c=$c+.2;
	
}

//date/time at bottom
$query = "SELECT DATE_FORMAT(starttime, '%c/%e/%Y') AS startdate, DATE_FORMAT(starttime, '%l:%i %p') AS starttime FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_row($result);

$pdf->SetXY($ml, $c+0.15);
$pdf->Cell(0,0, "Name of Street that Side 1 is facing: _________________________________________________________");
$pdf->SetXY(3.0, $c+0.15);
$address = issetBlank($row,'address');
$suffix = issetBlank($row,'suffix');

$pdf->Cell(0,0, "$address $suffix");

$pdf->SetXY(5.9, 9.90);
$pdf->Cell(0,0, '___________  /  ___________ ');
$pdf->SetXY(7.05, 9.90);
$pdf->Cell(0,0, $row[0]);
$pdf->SetXY(5.9, 10.10);
$pdf->Cell(0,0, '      Initials                 Date ');

$pdf->SetFont('Times','','9');
$pdf->SetXY($ml, 10.50);
$pdf->Cell(0,0, 'FORM PBLC-23-5B(9/97) Replaces OEHRA II (4/97) p.6b, which may be used.');



?>