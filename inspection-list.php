<?php

require_once'session.php';
require_once'connect.php';

$title = "Inspection Listing";
require_once'header.php';

$order = isset($_GET['order']) ? $_GET['order'] : ""; 

$link = "<a href=\"$_SERVER[PHP_SELF]?order=$order";
if (isset($_GET['active']) == 'active') {
	$link .= "&active=all";
} else {
	$link .= "&active=active";
}
$link .= "\">Active/All</a>";
print $link;
?>

<table border="0">
<tr><th><a href="inspection-list.php?order=iid">Insp #</a></th><th>Address, <a href="inspection-list.php?order=city">City</a>, <a href="inspection-list.php?order=zip">Zip</a></th><th><a href="inspection-list.php?order=scheddate">Date</a></th><th>Type</th><th>Team</th><th>Units</th><th>Inspected</th></tr>
<?php
switch ($order) {
	case 'city':
		$orderstring = 'ORDER BY city';
		break;
	case 'zip':
		$orderstring = 'ORDER BY zip';
		break;
	case 'scheddate':
	default:
		$orderstring = 'ORDER BY scheddate DESC';
		break;
	case 'iid':
		$orderstring = 'ORDER BY iid DESC';
		break;
}		

if (isset($_GET['active']) == 'active') {
	$query = "SELECT inspection.iid, streetnum, address, address2, city, state, zip, DATE_FORMAT(scheddate, '%c/%e/%Y') AS inspdate, type, COUNT(cuid) AS cnt, COUNT(starttime) AS cnt2 FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE inspection.status!='Completed' GROUP BY inspection.iid $orderstring";
} else {
	$query = "SELECT inspection.iid, streetnum, address, address2, city, state, zip, DATE_FORMAT(scheddate, '%c/%e/%Y') AS inspdate, type, COUNT(cuid) AS cnt, COUNT(starttime) AS cnt2 FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) GROUP BY inspection.iid $orderstring";
}
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while ($row = mysql_fetch_assoc($result)) {
	print "<tr><td valign='top'>$row[iid]-*</td><td valign='top' class=\"left\"><a href=\"inspection-view.php?iid=$row[iid]\">$row[streetnum] $row[address], ";
	if ($row['address2']) {
		print "$row[address2], ";
	}
	print "<br />$row[city], $row[state], $row[zip]</a></td><td valign='top'>$row[inspdate]</td><td valign='top'>$row[type]</td><td valign='top'>";
	$query = "SELECT firstname, lastname FROM insp_assigned INNER JOIN inspector USING (tid) WHERE iid=$row[iid]";
	$result2 = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row2 = mysql_fetch_row($result2)) {
		print "$row2[0] $row2[1]<br />";
	}
	print "</td><td valign='top'>$row[cnt]</td><td valign='top'>$row[cnt2]</td></tr>\n";
}
?>
</table>
<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>