<?php
//Only pass number via $_POST for pre-existing wipes; save script assumes update if passed
//Maybe this should be change using INSERT ON DUPLICATE KEY UPDATE?
// ********* $insptype MUST BE SET IN CALLING SCRIPT ******
if ($wid) {
	list($iid, $number) = explode('-', $wid);
	$table = $insptype.'_wipes';
	$query = "SELECT * FROM $table WHERE iid=$iid AND number=$number";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	if (mysql_num_rows($result) ) {
		$wipe = mysql_fetch_assoc($result);
	}
	print "<p><b>Sample Number: $iid-$unitnumberD$number</b></p>";	
	print "<input type=\"hidden\" name=\"iid\" value=\"$iid\" />\n";
	print "<input type=\"hidden\" name=\"number\" value=\"$number\" />\n";
} else {
	$query = "(SELECT number FROM comprehensive_wipes WHERE iid=$iid) UNION (SELECT number FROM conformance_wipes WHERE iid=$iid) ORDER BY number DESC LIMIT 1";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$number = mysql_result($result,0) + 1;
	print "<p><b>New Sample Number: $iid-$unitnumber"."D$number</b></p>";	
}

?>
<input type="hidden" name="cuid" value="<?php print $cuid; ?>" />
<table class="straightup">
<tr><th>Room</th><th>Side</th><th>Surface</th><th>Paint Chips</th><th>12x12</th><th>Area Width (in)</th><th>Area Height (in)</th><!-- <th>Lab Number</th><th>Micrograms / Wipe Result</th><th>Conversion Factor</th><th>Microgram/Square Foot Result</th><th>Spot Test</th><th>Hazard Assessment</th> --></tr>
<tr><td>
<?php
$table = $insptype.'_rooms';
$query = "SELECT crid, name, number FROM $table WHERE cuid=$cuid ORDER BY number";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while ($row = mysql_fetch_assoc($result)) {
	if ($crid == $row['crid'] || $wipe['crid'] == $row['crid']) {
		print "<input type=\"radio\" name=\"crid\" value=\"$row[crid]\" checked=\"checked\" />($row[number]) $row[name]<br />\n";
	} else {
		print "<input type=\"radio\" name=\"crid\" value=\"$row[crid]\" />($row[number]) $row[name]<br />\n";
	}
}
?>
</td>
<td>
<?php
$sides = array('None','1','2','3','4','Center');
foreach ($sides as $side) {
	if ($wipe['side'] == $side) {
		print "<input type=\"radio\" name=\"side\" value=\"$side\" checked=\"checked\" />$side<br />\n";
	} else {
		print "<input type=\"radio\" name=\"side\" value=\"$side\" />$side<br />\n";
	}
}
?>
</td>
<td>
<?php
$query = "SELECT * FROM wipe_list";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while($row = mysql_fetch_row($result)) {
	if ($wipe['surface'] == $row[0]) {
		print "<input type=\"radio\" name=\"surface\" value=\"$row[0]\" checked=\"checked\" />$row[0]<br />\n";
	} else {
		print "<input type=\"radio\" name=\"surface\" value=\"$row[0]\" />$row[0]<br />\n";
	}
}
?>
</td>
<td>
<?php if ($wipe['paintchips'] == 'Yes') {
?>
<input type="radio" name="paintchips" value="Yes" checked="checked" />Yes<br />
<input type="radio" name="paintchips" value="No" />No
<?php
} else { // also serves as default
?>
<input type="radio" name="paintchips" value="Yes" />Yes<br />
<input type="radio" name="paintchips" value="No" checked="checked" />No
<?php
}
?>
</td>
<td><input type="checkbox" name="a12" value="1" />Use 12&quot;x12&quot;</td>
<td>
<table>
<?php

if (substr($wipe['areawidth'], ' ')) {
	$width = explode(' ',$wipe['areawidth']);
	$w1 = (int)($width[0] / 10);
	$w2 = $width[0] % 10;
	$w3 = $width[1];
} else {
	$w1 = (int)($wipe['areawidth'] / 10);
	$w2 = $width[0] % 10;
	$w3 = '.0';
}
if (substr($wipe['arealength'], ' ')) {
	$length = explode(' ',$wipe['arealength']);
	$l1 = (int)($length[0] / 10);
	$l2 = $length[0] % 10;
	$l3 = $length[1];
} else {
	$l1 = (int)($wipe['arealength'] / 10);
	$l2 = $length[0] % 10;
	$l3 = '.0';
}

$fracs = array('.0','1/8','1/4','3/8','1/2','5/8','3/4','7/8','','');
for ($x=0; $x<=9; $x++) {
	print "<tr>\n";
	if ($x == 0 || $x == $w1) {
		print "<td><input type=\"radio\" name=\"w1\" value=\"$x\" checked=\"checked\" />$x</td>";
	} else {
		print "<td><input type=\"radio\" name=\"w1\" value=\"$x\" />$x</td>";
	}
	if ($x == 0 || $x == $w2) {
		print "<td><input type=\"radio\" name=\"w2\" value=\"$x\" checked=\"checked\" />$x</td>\n";
	} else {
		print "<td><input type=\"radio\" name=\"w2\" value=\"$x\" />$x</td>\n";
	}
	//fractions
	if ($fracs[$x]) {
		if ($x == 0 || $x == $w3) {
			print "<td><input type=\"radio\" name=\"w3\" value=\"$fracs[$x]\" checked=\"checked\" />$fracs[$x]</td>\n";
		} else {
			print "<td><input type=\"radio\" name=\"w3\" value=\"$fracs[$x]\" />$fracs[$x]</td>\n";
		}
	} else {
		print "<td> </td>\n";
	}
	print "</tr>\n";
}
?>
</table>
</td>
<td>
<table>
<?php
$fracs = array('.0','1/8','1/4','3/8','1/2','5/8','3/4','7/8','','');
for ($x=0; $x<=9; $x++) {
	print "<tr>\n";
	//tens
	if ($x == 0 || $x == $l1) {
		print "<td><input type=\"radio\" name=\"l1\" value=\"$x\" checked=\"checked\" />$x</td>";
	} else {
		print "<td><input type=\"radio\" name=\"l1\" value=\"$x\" />$x</td>";
	}
	if ($x == 0 || $x == $l2) {
		print "<td><input type=\"radio\" name=\"l2\" value=\"$x\" checked=\"checked\" />$x</td>\n";
	} else {
		print "<td><input type=\"radio\" name=\"l2\" value=\"$x\" />$x</td>\n";
	}
	//fractions
	if ($fracs[$x]) {
		if ($x == 0 || $x == $l3) {
			print "<td><input type=\"radio\" name=\"l3\" value=\"$fracs[$x]\" checked=\"checked\" />$fracs[$x]</td>\n";
		} else {
			print "<td><input type=\"radio\" name=\"l3\" value=\"$fracs[$x]\" />$fracs[$x]</td>\n";
		}
	} else {
		print "<td> </td>\n";
	}
	print "</tr>\n";
}
?>
</table>
</td>
</tr>
</table>
<p><input type="submit" name="submit" value="Save" /> <input type="submit" name="submit" value="Save and New" /> <input type="reset" value="Reset" /> <input type="submit" name="submit" value="Cancel" /></p>
