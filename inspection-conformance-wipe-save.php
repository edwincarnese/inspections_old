<?php

require_once'session.php';
require_once'connect.php';

if ($_POST['crid']) {
	$query = "SELECT cuid FROM conformance_rooms WHERE crid=$_POST[crid]";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$cuid = mysql_result($result, 0);
	$crid = $_POST['crid'];
} else {
	$cuid = $_POST['cuid'];
}


if ($_POST['submit'] == 'Save' || $_POST['submit'] == 'Save and New') {

	if ($_POST['l1'] == '0') {
		$_POST['l1'] = '';
	}
	if ($_POST['l3'] == '.0') {
		$_POST['l3'] = '';
		$lenfrac = 0;
	} else {
		$lenfrac = substr($_POST['l3'],0,1) / substr($_POST['l3'], 2);
	}
	if ($_POST['w1'] == '0') {
		$_POST['w1'] = '';
	}
	if ($_POST['w3'] == '.0') {
		$_POST['w3'] = '';
		$widfrac = 0;
	} else {
		$widfrac = substr($_POST['w3'],0,1) / substr($_POST['w3'], 2);
	}
	
	$declength = $_POST['l1']*10 + $_POST['l2'] + $lenfrac;
	$decwidth = $_POST['w1']*10 + $_POST['w2'] + $widfrac;

	$length = $_POST['l1'].$_POST['l2'].' '.$_POST['l3'];
	$width = $_POST['w1'].$_POST['w2'].' '.$_POST['w3'];

	if ($_POST['a12']) {
		$length = $width = $declength = $decwidth = 12;
	}
	
	$conversion = round($declength * $decwidth / 144, 4);

	if ($crid) { // no $crid breaks script
		$query = "SELECT iid FROM conformance_rooms INNER JOIN units USING (cuid) WHERE crid=$_POST[crid]";
		$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		$iid = mysql_result($result, 0);
		
		$area = $length * $width;
		if ($area > 14.4) {
			if ($_POST['number']) {
				$query = "UPDATE conformance_wipes SET surface='$_POST[surface]', side='$_POST[side]', arealength='$length', areawidth='$width', paintchips='$_POST[paintchips]', conversion=$conversion WHERE iid=$iid AND number=$_POST[number]";
			} else {
				$query = "(SELECT number FROM comprehensive_wipes WHERE iid=$iid) UNION (SELECT number FROM conformance_wipes WHERE iid=$iid) ORDER BY number DESC LIMIT 1";
				$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
				$number = mysql_result($result,0) + 1;
				$query = "INSERT INTO conformance_wipes (iid, number, crid, timetaken, surface, side, paintchips, arealength, areawidth, conversion) VALUES ($iid, $number, '$_POST[crid]', NOW(), '$_POST[surface]', '$_POST[side]', '$_POST[paintchips]', '$length', '$width', $conversion)";
			}
			mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		} else {
			$error = "The wipe area must be larger than 14.4 square inches. $area";
		}
	} else {
		$error .="<br>You must select a room.";
	}

	//ENDTIME
	$query = "UPDATE units SET endtime=NOW() WHERE cuid=$cuid";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
}


if ($_POST['submit'] == 'Yes') { //coming from field blank page
	$query = "SELECT iid FROM units WHERE cuid=$cuid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$iid = mysql_result($result, 0);
	$query = "INSERT INTO conformance_wipes (iid, number, timetaken, surface) VALUES ($iid, 0, NOW(), 'Field Blank')";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-conformance-wipe.php?cuid=$cuid&crid=$crid");
	exit();

	//ENDTIME
	$query = "UPDATE units SET endtime=NOW() WHERE cuid=$cuid";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
}
if ($_POST['submit'] == 'Save and New' && $crid) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-conformance-wipe.php?cuid=$cuid");
} else if ($crid && $error==''){
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-conformance-interior.php?cuid=$cuid");
} else {
	echo "<body onLoad=\"javascript:goBackError('$error'); \"/>";
	
	}
?>
<script language=javascript>
function goBackError(errorstring) {
	alert(errorstring);
	window.location = '<? print "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-conformance-wipe.php?cuid=$cuid&error=$error"; ?>';
	
}
</script>