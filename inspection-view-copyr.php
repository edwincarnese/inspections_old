<?php 

include_once( 'session.php');
include_once( 'connect.php');

$iid = $_POST['iid'] or $iid = $_GET['iid'] or $iid = 0;
$updateDate = $_POST['updateDate'] or $updateDate = $_GET['updateDate'] or $updateDate = 0;
$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

/*if ($updateDate =="true" ){
$query = 'update inspection set scheddate=\''.date("Y-m-d").'\' where iid='.$iid.' ';
//print $query;
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
}*/

$query = "SELECT *, DATE_FORMAT(scheddate, '%c/%e/%Y') AS scheddate, DATE_FORMAT(schedstart, '%l:%i %p') AS schedstart, DATE_FORMAT(schedend, '%l:%i %p') AS schedend FROM inspection WHERE iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}

$insp = mysql_fetch_assoc($result);
$status = $insp['status'];

$title = "View Building Inspection: ".$insp['iid']."-*";
require_once'header.php';
/*
<table frame="border" border="2" rules="none" cellspacing="0" cellpadding="1" width="360px">
<tr>
	<td class="box2">Insp#: <?php print "$insp[iid]-* Date: $insp[scheddate] Start Time: $insp[schedstart]"; ?></td>
</tr>
</table>
<br>*/
?>
<SCRIPT LANGUAGE="JavaScript">

function popUp(URL) {
day = new Date();
id = day.getTime();
eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=825,height=530,top=575');");
}

function confirmUnits(dest) {
	<? 
	$query = "SELECT numUnitsConfirmed, numUnits FROM building, inspection where inspection.iid=$iid and building.bid=inspection.bid";
	$result = mysql_query($query) or sql_crapout($query."<br>".mysql_error());
	$blah = mysql_fetch_assoc($result);
	print "confirmed = ".$blah['numUnitsConfirmed'].";";
	print "numUnits = ".$blah['numUnits'].";";
	?>
/*	if (confirmed == 0) {
		var boxClick = confirm("Please confirm that the number of units is correct for this building.  If yes, click ok, otherwise click cancel and enter the correct information.");
		if (boxClick) {
			window.location.href = dest;
		} else {
			
		}
	}
	else {
		window.location.href = dest;
	}		
}
*/
function confirmDelete() {
	var conf = confirm('Are you sure you want to delete this unit?');
	if (conf) {
		return true;
	} else {
		return false;
	}
}
</script>
<?/*************************************** CLIENT INFO******************************/?>
<?php
if (isset($_GET['cid'])){
	$query = "SELECT * FROM client WHERE cid=".$_GET['cid'];
}
else {
	$query = "SELECT * FROM owners INNER JOIN client USING (cid) WHERE bid=$insp[bid] AND startdate <= DATE(NOW()) AND enddate > DATE(NOW())";
}
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
$cid = $row['cid'];

?>
<table>
	<tr>
		<td>
		<?
		if ($cid != '') {
			?>
		<table frame="border" border="2" rules="none" cellspacing="1" cellpadding="1" width="360px"" > 
			<tr><th class="box" align="left" colspan="3">Client Information <font size="-1"><a href="javascript:popUp('client-edit.php?cid=<?php print $row['cid'];?>&isPopup=true')" class="box2" style="border:solid gray 1px">click to edit</font></a></th></tr>
			<tr>
				<td class="box2"><?php print $row['title']." ".$row['firstname']." ".$row['mi']." ".$row['lastname']; ?><br>
				<?php print $row['streetnum']." ".$row['address']." ".$row['suffix']." ".$row['address2']; ?><br>
				<?php print $row['city'].", ".$row['state']." ".$row['zip']; ?></td>
			
				<td class="box2" colspan="2" valign="top">Company: <?php print $row['company']; ?><br>
				Title: <?php print $row['companytitle']; ?></td>
			</tr>
				<td class="box2" colspan="3">Mailing Address:<br>
				<?php print $row['mailingstreetnum']." ".$row['mailingaddress']." ".$row['mailingsuffix']." ".$row['mailingaddress2']; ?><br>
				<?php print $row['mailingcity'].", ".$row['mailingstate']." ".$row['mailingzip']; ?></td>
			</tr>
			
			<tr>
			<?php
			
			$query = "SELECT * FROM client_phone WHERE cid=$row[cid]";
			$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			
			$phonecount = 0;
			$phonerow = mysql_fetch_assoc($result);
			$phonerow[1] = substr($phonerow['number'], 0, 3);
			$phonerow[2] = substr($phonerow['number'], 4, 3);
			$phonerow[3] = substr($phonerow['number'], 8);
			
			?>
				<td class="box2">Phone (1): <?php print "$phonerow[number]"; ?></td>
				<td class="box2">Ext: <?php print $phonerow['ext']; ?></td>
				<td class="box2">Type: <?php print $phonerow['type']; ?></td>
			</tr>
			<tr>
			<?php
			$phonerow = mysql_fetch_assoc($result);
			?>
				<td class="box2">Phone (2): <?php print "$phonerow[number]"; ?></td>
				<td class="box2">Ext: <?php print $phonerow['ext']; ?></td>
				<td class="box2">Type: <?php print $phonerow['type']; ?></td>
			</tr>
			<tr>
			<?php
			$phonerow = mysql_fetch_assoc($result);
			?>
				<td class="box2">Phone (3): <?php print "$phonerow[number]"; ?></td>
				<td class="box2">Ext: <?php print $phonerow['ext']; ?></td>
				<td class="box2">Type: <?php print $phonerow['type']; ?></td>
			</tr>
			<tr>
				<td class="box2" colspan="2">E-mail: <a href="mailto:<?php print $row['email']; ?>"><?php print $row['email']; ?></a></td>
				<td class="box2">Class completed: <?php print $row['class_comp']; ?></td> 
			</tr>
			<tr>
				<td class="box2">Class taken with: <?php print $row['class_with']; ?></td>
				<td class="box2" colspan="2">Relationshp: </php print $ros['relationship']; ?></td>
			</tr>
			</table>
			<p> </p>
			<?php
		} else {
			print "There are no clients/owners associated with this building";
		}
			$query = "SELECT * FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
			$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			$row = mysql_fetch_assoc($result);
			$bid = $row['bid'];
			?>
		</td>
<? /********************************************** BUILDING INFO *********************************/?>
		<td valign="top" >
			<table frame="border" border="2" rules="none" cellspacing="1" cellpadding="1" width="350px">
			<tr><th align="left" colspan="3" class="box">Building Information <font size="-1"><a href="javascript:popUp('building-edit.php?bid=<?php print $row['bid'];?>&isPopup=true')" class="box2" style="border:solid gray 1px">click to edit</font></a>
			<? if ($_SESSION['sid']==1){ ?><font size="-1"><a href="inspection-building-copy.php?bid=<?php print $row['bid'];?>" class="box2" style="border:solid gray 1px">copy building</font></a><? } ?></th></tr>
			<tr>
				<td class="box2" colspan="2"> <?php print $row['streetnum']." ".$row['address']." ".$row['suffix']." ".$row['address2']; ?><br>
				<?php print $row['city'].", ".$row['state']." ".$row['zip']; ?></td>
				<td class="box2" valign="top">Number of Units: <?php print $row['numunits']; ?></td>
			</tr>
			
			<tr>
				<td class="box2" colspan="2">Year Built: <?php print $row['yearbuilt'];?><br>
				Other: <?php print $row['other'];?><br>
				Plat: <?php print $row['plat']; ?><br>
				Lot: <?php print $row['lot']; ?></td>
				<td class="box2" valign="top">
				Insurance Agent: <?php print $row['InsuranceAgent']; ?><br>
				Insurance Company: <?php print $row['InsuranceCo']; ?><br>
				Policy #: <?php print $row['InsurancePolicyNo']; ?></td>
			</tr>
			</table>
			<br>
			<?php

/********************************* ALL PEOPLE ********************************************/			
			$query = "SELECT firstname, lastname, relationship, getReport, owners.cid, owners.bid FROM client LEFT JOIN owners ON client.cid=owners.cid WHERE owners.bid=$bid";
			$result = mysql_query($query) or sql_crapout($query."<br />".mysql_error());?>
			<table frame="border" border="2" rules="none" cellspacing="1" cellpadding="2" width="350px">
			<tr><th align="left" colspan="3" class="box">Other Associated People <font size="-1"><a href="javascript:alert('This function is not yet available.')" class="box2" style="border:solid gray 1px">click to edit</font></a></th></tr>
			<tr><th align="left" class="box">Name</th><th align="left" class="box">Relationship</th><th align="left" class="box">Report</th></tr>
			<?php
			while ($row = mysql_fetch_assoc($result)) {?>
				<tr>
					<td class='box2'><a href="inspection-view.php?iid=<?php print $iid;?>&cid=<?php print $row['cid'];?>"><?php print $row['lastname'].", ".$row['firstname']; ?></a></td>
					<td class='box2'><?php print $row['relationship']; ?></td>
					<td class='box2'><a href="owners-change-getreport.php?bid=<? print $row['bid'];?>&cid=<? print $row['cid'];?>&iid=<? print $iid;?>"><?php print $row['getReport']; ?></a></td>
					</tr>
				
			<?php
			}?>
			</table>
		</td>
	</tr>
	
</table>
<? /********************************** INSPECTION INFORMATION ******************************/?>
<table>
	<tr>
		<td>
			<table frame="border" border="2" rules="none" cellspacing="1" cellpadding="1" width="720">
				<tr>
				  <th class="box" align="left" colspan="3">Inspection Information 
				<font size="-1">
				<a href="javascript:popUp('inspection-schedule-edit.php?iid=<?php print $iid;?>&isPopup=true')" class="box2" style="border:solid gray 1px">click to edit
				</a><a href="javascript:popUp('inspection-boilerplate.php?iid=<?php print $iid;?>&isPopup=true')" class="box2" style="border:solid gray 1px">boiler plate
				</a><a href="javascript:popUp('inspection-inspector-schedule-elt-eli-test.php?iid=<?php print $iid;?>&isPopup=true')" class="box2" style="border:solid gray 1px">Inspectors</a>

				<a href="javascript:popUp('inspection-comprehensive-xrfcalibration.php?iid=<?php print $iid;?>&isPopup=true')" class="box2" style="border:solid gray 1px">XRF Calibration
				</a><a href="javascript:popUp('inspection-comprehensive-xrfsummary.php?iid=<?php print $iid;?>&isPopup=true')" class="box2" style="border:solid gray 1px">XRF Summary
				</a>
				
				
				</font></th>
				</tr>
				<tr>
					<td class="box2">Type: <?php print $insp[type]; ?></td>
					<td class="box2">Reason: <?php print $insp[reason]; ?></td>
					<?php
					if ($insp['otherreason']) {
						print "<td class=\"box2\">$insp[otherreason]</td>";
					}
					?>
					<td class="box2">Financial Info:</td>
				</tr>
				<tr>
					<td class="box2">Date: <?php print $insp[scheddate]; ?><br>
					Start: <?php print $insp[schedstart]; ?><br>
					End: <?php print $insp[schedend]; ?>
					</td>
					<td class="box2" valign="top">Owner Occupied?: <?php print $insp['owneroccupied']; ?></td>
					<td class="box2"></td>
				</tr>
<? /********************************************* INSPECTOR INFORMATION ****************************?>				
				<tr>
				<td class="box2" colspan="3">
					<table frame="all" border="2" rules="none" cellspacing="1" cellpadding="2" align="left" style="background-color:#ddd" width="400">
						<tr><th class="box" align="left" colspan="4">Inspection Team <font size="-1"><a href="javascript:popUp('inspection-inspector-schedule.php?iid=<?php print $iid;?>')" class="box2" style="border:solid gray 1px">click to edit</font></a></th></tr>
						<tr align="left"><th class="box">License</th><th class="box">Lic. Date</th><th class="box">Name</th><th class="box">Lead</th></tr>
						<?php
						$query = "SELECT firstname, lastname, eli, DATE_FORMAT(elidate, '%c/%e/%Y') AS elidate, elt, DATE_FORMAT(eltdate, '%c/%e/%Y') AS eltdate, job FROM insp_assigned INNER JOIN inspector USING (tid) WHERE iid=$insp[iid] ORDER BY job, lastname";
						$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
						
						if (mysql_num_rows($result)) {
							while($row = mysql_fetch_assoc($result)) {
								?>
								<tr align="left" valign="top"><td class="box2"><?
								if ($row['eli']) {
									print "ELI: ".$row['eli']."</td><td class=\"box2\">".$row['elidate'];
								}
								else if ($row['elt']) {
									print "ELT: ".$row['elt']."</td><td class=\"box2\">".$row['eltdate'];
								}
								?></td>
								<td class="box2"><? print $row['lastname'].", ".$row['firstname'];?></td>
								<td class="box2"><?
								if ($row['job'] == 'Lead') {
									print " <b>(Lead)</b>";
								}
								?></td></tr><?
							}
						}
						
						?>
					</table>
				</td>
				</tr>*/?>
			</table>
			<br>
<? /************************************* UNIT INFORMATION **********************************/?>
			<table frame="all" border="2" rules="none" cellspacing="1" cellpadding="1" width="720">
				<tr><th class="box" align="left" colspan="15">Units <font size="-1"><a href="inspection-unit-add.php?iid=<?php print $iid;?>" class="box2" style="border:solid gray 1px" >add a unit</a></font></th></tr>
				<tr align="left"><th class="box">No.</th><th class="box">Desig.</th><th class="box">Description</th><th class="box">Insp. Type</th><th class="box">Status</th><th class="box">Date</th><th class="box">Do</th><th class="box">Other</th><th class="box">Inspectors</th><th class="box">Unit Type</th><th class="box"><? if ($sid==1){ ?>Action<? }else { ?>Del. <? } ?></th></tr>
				<?php
				$query2 = "SELECT number, cuid, starttime, unittype, unitdesc, designation, insptype, units.iid, inspection.status, inspection.scheddate FROM units, inspection, building WHERE units.iid=inspection.iid AND building.bid=inspection.bid AND building.bid=$bid ORDER BY iid desc";
				$result2 = mysql_query($query2) or sql_crapout($query2."<br />".mysql_error());
				while ($row = mysql_fetch_assoc($result2)) {
					?>
					<tr align="left" valign="top">
						<td class="box2"><? print $row['iid']."-".$row['number']; ?></td>
						<td class="box2"><? print $row['designation']; ?> </td>
						<td class="box2"><a href="javascript:popUp('inspection-unitinfo.php?cuid=<? print $row['cuid']; ?>&isPopup=true')"><? print $row['unitdesc']; ?></a></td>
						<td class="box2"><? 
							if ($row['starttime'] || $row['unittype'] != 'Interior') {
								if ($row['insptype']=='Conformance') {
									print "conf.";
								}
								else if ($row['insptype']=='Comprehensive') {
									print "comp.";
								}
							} else {
								if ($row['insptype']=='Conformance') {
									print "<a href=\"inspection-unit-switchtype.php?cuid=$row[cuid]\">conf.</a>";
								}
								else if ($row['insptype']=='Comprehensive') {
									print "<a href=\"inspection-unit-switchtype.php?cuid=$row[cuid]\">comp.</a>";
								}
								
							}?></td>
						<td class="box2"><? print $row['status']; ?></td>
						<td class="box2"><? print $row['scheddate']; ?></td>
						<td class="box2" align="center" valign="center"><font size="-1"><?php 
							if (strncasecmp($row['insptype'], "conformance", 11) == 0 ||strncasecmp($row['insptype'], "presunptive compliance", 22) == 0 || strncasecmp($row['insptype'], "comprehensive", 13) == 0 ) {
								print "<a href=\"javascript:confirmUnits('inspection-".strtolower($row['insptype'])."-unit.php?cuid=$row[cuid]');\" class=\"box2\" style=\"border:solid gray 1px\">";
							} else {
								print "<a href=\"javascript:alert('comming attraction!');\" class=\"box2\" style=\"border:solid gray 1px\">";
							}?>
							 do</a></font></td>
						<td class="box2"><?
							if ($row['unittype'] == 'Interior') {
								print "<a href=\"inspection-output.php?cuid=$row[cuid]\">Output";
							}
							else if (strstr($row['unittype'], 'Common')) {
								print "<a href=\"inspection-unit-links.php?cuid=$row[cuid]&amp;dest=inspection-view.php\">Links</a>";
							}?> </td>
							<td class="box2">
										
						<?php	
						$inspectorquery = "SELECT firstname, lastname FROM unit_insp_assigned INNER JOIN insp_assigned USING (assig_id) INNER JOIN inspector USING (tid) WHERE iid=".$row['iid']." AND unit_insp_assigned.cuid=".$row['cuid'];
						$inspectorresult = mysql_query($inspectorquery) or sql_crapout($inspectorquery."<br />".mysql_error());
						$inspector = mysql_fetch_assoc($inspectorresult);
							print "<a href=\"javascript:popUp('inspection-inspector-schedule-elt-eli-test.php?isPopup=true&iid=$iid&cuid=$row[cuid]&unitassign=true');\">".substr($inspector['firstname'],0,1).". ".substr($inspector['lastname'],0,1).".</a><br>";
						print "</td>";?>
						<td class="box2"><? print $row['unittype']; ?></td>
						<td class="box2" align="center" valign="center"><font size="-1"><font size="-1">
						  <? if ($_SESSION['sid']==1){ ?>
						</font><a href="inspection-unit-copy.php?cuid=<? print $row['cuid'];?>&iid=<? print $row['iid'] ?>" class="box2" style="border:solid gray 1px">copy</a> | <a onClick="return confirmDelete();" href="inspection-unit-remove.php?cuid=<? print $row['cuid'];?>" class="box2" style="border:solid gray 1px">del.</a><? }else { ?><a onClick="return confirmDelete();" href="inspection-unit-remove.php?cuid=<? print $row['cuid'];?>" class="box2" style="border:solid gray 1px">del.</a><? } ?></font></td>
				<?
				}
				?>
			</table>	
		</td>
	</tr>
</table>
</p>
<?

require_once'footer.php';

?>
