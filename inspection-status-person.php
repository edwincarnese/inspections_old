<?php

require_once'connect.php';
require_once'session.php';

$title = "Inspection Status Person";
require_once'header.php';
?>
<table>
<tr><th><a href="inspection-status-person.php?order=iid">Insp </a></th><th>Address, <a href="inspection-status-person.php?order=city">City</a>, <a href="inspection-status-person.php?order=zip">Zip</a></th><th><a href="inspection-status-person.php?order=scheddate">Date</a></th><th>Type</th><th>Team</th></tr>
<?php
switch ($_GET['order']) {
	case 'city':
		$orderstring = 'ORDER BY city';
		break;
	case 'zip':
		$orderstring = 'ORDER BY zip';
		break;
	case 'scheddate':
		$orderstring = 'ORDER BY scheddate DESC';
		break;
	case 'iid':
	default:
		$orderstring = 'ORDER BY iid DESC';
		break;
}

$wherestring = "((client.firstname='' OR client.lastname='') OR (client.company='' OR client.companytitle='')) OR client.streetnum='' OR client.address='' OR client.city='' OR client.state='' OR client.zip='' OR client.mailingnum='' OR client.mailingcity='' OR client.mailingstate='' OR client.mailingzip=''";
        
$query = "SELECT distinct inspection.iid, building.address, building.address2, building.city, building.state, building.zip, DATE_FORMAT(inspection.scheddate, '%c/%e/%Y') AS inspdate, type FROM inspection INNER JOIN building USING (bid) INNER JOIN owners USING (bid) WHERE owners.cid IN(SELECT cid FROM client WHERE $wherestring) and inspection.status!='Completed' $orderstring";

$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while ($row = mysql_fetch_assoc($result)) {

    $tmptime = $row['inspdate'];
    $tmptimearray = explode('/', $tmptime);
    $tmptimestamp = mktime(0,0,0,$tmptimearray[0], $tmptimearray[1], $tmptimearray[2]);
    if ($tmptimestamp <= time() + 604800 and $tmptimestamp >= time() - 604800){
        print "<tr style=\"color:#DC143C\"><td>$row[iid]-*</td><td class=\"left\"><a style=\"color:#8B1A1A\" href=\"inspection-view.php?iid=$row[iid]\">$row[address], ";
        if ($row['address2']) {
		  print "$row[address2], ";
        }
        print "<br />$row[city], $row[state], $row[zip]</a></td><td>";
    } else {
        print "<tr><td>$row[iid]-*</td><td class=\"left\"><a href=\"inspection-view.php?iid=$row[iid]\">$row[address], ";
        if ($row['address2']) {
	       print "$row[address2], ";
        }
	   print "<br />$row[city], $row[state], $row[zip]</a></td><td>";
  }

    print "$tmptime</td><td>$row[type]</td><td>";
	$query = "SELECT firstname, lastname FROM insp_assigned INNER JOIN inspector USING (tid) WHERE iid=$row[iid]";
	$result2 = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row2 = mysql_fetch_row($result2)) {
		print "$row2[0] $row2[1]<br />";
	}
	print "</td></tr>\n";
}
?>
</table>
<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>
}


