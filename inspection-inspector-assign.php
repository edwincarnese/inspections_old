<?php 
require_once'session.php';
?>
<script language=javascript>
function closePopUp() {
	popup = '<?print $_POST['popup'];?>';
	if (popup != '') {
		opener.location.reload(true);
		self.close();
	}
}
</script>
<body onLoad="javascript:closePopUp();"></body>
<?
require_once'connect.php';


$iid = $_POST['iid'] or $iid = $_GET['iid'] or $iid = 0;

if ($_POST['submit'] == 'Save') {
	//$query = "DELETE FROM insp_assigned WHERE iid=$iid";
	//mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$query = "CREATE TEMPORARY TABLE tmp (iid mediumint(9), tid mediumint(9), job varchar(12))";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	
	if ($_POST['lead']) {
		if ($_POST['lead'] == "None") {
			$query = "DELETE from insp_unit_assigned WHERE assig_id IN (SELECT assig_id FROM insp_assigned WHERE iid=$iid AND job='Lead'";
			mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			
			$query = "DELETE from insp_assigned WHERE iid=$iid AND job='Lead'";
			mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			
		} else {
			$query = "INSERT INTO tmp (iid, tid, job) VALUES ($iid, $_POST[lead], 'Lead')";
			mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			
		//	$query = "DELETE from insp_unit_assigned WHERE assig_id IN (SELECT assig_id FROM insp_assigned WHERE iid=$iid AND tid NOT IN (SELECT tmp.tid FROM tmp) AND job='Lead')";
		//	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			
			$query = "DELETE FROM insp_assigned WHERE iid=$iid AND tid NOT IN (SELECT tmp.tid FROM tmp) AND job='Lead'";
			mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			
			$query = "INSERT INTO insp_assigned (iid, tid, job) SELECT DISTINCT tmp.iid, tmp.tid, tmp.job FROM tmp, insp_assigned WHERE tmp.tid NOT IN (SELECT tid from insp_assigned where iid=$iid)";
			mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		}
	}

	else {
		foreach ($_POST['assist'] AS $tid) {
			if ($tid != $_POST['lead']) {
				$query = "INSERT INTO tmp (iid, tid, job) VALUES ($iid, $tid, 'Assistant')";
				mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			}
		}
	//	$query = "DELETE from insp_unit_assigned WHERE assig_id IN (SELECT assig_id FROM insp_assigned WHERE iid=$iid AND tid NOT IN (SELECT tmp.tid FROM tmp) AND job='Assistant')";
	//	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		
		$query = "DELETE FROM insp_assigned WHERE iid=$iid AND tid NOT IN (SELECT tmp.tid FROM tmp) AND job='Assistant'";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		
		$query = "INSERT INTO insp_assigned (iid, tid, job) SELECT DISTINCT tmp.iid, tmp.tid, tmp.job FROM tmp, insp_assigned WHERE tmp.tid NOT IN (SELECT tid from insp_assigned where iid=$iid)";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
		
	
}
//check for only one inspector, if yes put them in all units

$query = "SELECT * FROM insp_assigned WHERE iid=$iid";
$result = mysql_query($query) or sql_crapout($query."<br>".mysql_error());

if (mysql_num_rows($result) == 1) {
	$inspector = mysql_fetch_assoc($result);
	$cuidquery = "SELECT cuid FROM units WHERE iid=$iid";
	$cuidresult = mysql_query($cuidquery) or sql_crapout($cuidquery."<br>".mysql_error());
	while ($row = mysql_fetch_assoc($cuidresult)){
		$query = "INSERT INTO unit_insp_assigned (assig_id, cuid, job) VALUES ($inspector[assig_id], $row[cuid], 'Lead')";
		$result = mysql_query($query) or sql_crapout($query."<br>".mysql_error());
	}
}

//check for <= 3 assistants on comprehensives

$query = "SELECT DISTINCT insp_assigned.* FROM insp_assigned, inspection WHERE insp_assigned.iid=$iid AND job='Assistant' AND inspection.type='Comprehensive' AND inspection.iid=insp_assigned.iid";
$result = mysql_query($query) or sql_crapout($query."<br>".mysql_error());

if (mysql_num_rows($result) <= 3) {
	while ($inspector = mysql_fetch_assoc($result)) {
		$cuidquery = "SELECT cuid FROM units WHERE iid=$iid";
		$cuidresult = mysql_query($cuidquery) or sql_crapout($cuidquery."<br>".mysql_error());
		while ($row = mysql_fetch_assoc($cuidresult)){
			$insquery = "INSERT INTO unit_insp_assigned (assig_id, cuid, job) VALUES ($inspector[assig_id], $row[cuid], 'Assistant')";
			$insresult = mysql_query($insquery) or sql_crapout($insquery."<br>".mysql_error());
		}
	}
}
if ($_POST['popup'] == '') {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-view.php?iid=$iid");
}
?>