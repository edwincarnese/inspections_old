<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

$query = "SELECT iid, insptype FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
list($iid, $insptype) = mysql_fetch_row($result);

if ($insptype == 'Conformance') {
	$query = "UPDATE units SET insptype='Comprehensive' WHERE cuid=$cuid";
} 
 else if ($insptype =='Comprehensive') {
	$query = "UPDATE units SET insptype='Conformance' WHERE cuid=$cuid";
}/* 
else if ($insptype =='LERA') {
	$query = "UPDATE units SET insptype='Conformance' WHERE cuid=$cuid";
}*/
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

//check to see if common/exterior units need type changed
$query = "SELECT * FROM units WHERE unittype='Interior' AND insptype='LERA' AND iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

//Set all common units to conformance, then change units to comprehensive as needed
$query = "UPDATE units SET insptype='Conformance' WHERE iid=$iid AND unittype!='Interior'";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result)) {
	$query = "UPDATE units SET insptype='LERA' WHERE iid=$iid AND unittype='Exterior'";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	//update each common area connected to a comp unit
	while ($row = mysql_fetch_assoc($result)) {
		$query = "UPDATE units INNER JOIN unit_links ON (units.cuid=unit_links.common) SET insptype='LERA' WHERE unit_links.unit=$row[cuid]";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
}

$query = "UPDATE units SET endtime=NOW() WHERE cuid=$cuid";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-view.php?iid=$iid");
?>