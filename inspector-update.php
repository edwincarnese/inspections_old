<?php

require_once'session.php';
require_once'connect.php';

//print_r($_POST);

$phones = $_POST['phones'];
$tid = $_POST['tid'];
$_POST['eltdate'] = "$_POST[eltyear]-$_POST[eltmonth]-$_POST[eltday]";
$_POST['eltstart'] = "$_POST[eltsyear]-$_POST[eltsmonth]-$_POST[eltsday]";
$_POST['elidate'] = "$_POST[eliyear]-$_POST[elimonth]-$_POST[eliday]";
$_POST['elistart'] = "$_POST[elisyear]-$_POST[elismonth]-$_POST[elisday]";
$_POST['startdate'] = "$_POST[startyear]-$_POST[startmonth]-$_POST[startday]";
$_POST['enddate'] = "$_POST[endyear]-$_POST[endmonth]-$_POST[endday]";

unset($_POST['phones']);
unset($_POST['tid']);
unset($_POST['eltyear']);
unset($_POST['eltday']);
unset($_POST['eltmonth']);
unset($_POST['eliyear']);
unset($_POST['eliday']);
unset($_POST['elimonth']);
unset($_POST['eltsyear']);
unset($_POST['eltsday']);
unset($_POST['eltsmonth']);
unset($_POST['elisyear']);
unset($_POST['elisday']);
unset($_POST['elismonth']);
unset($_POST['startyear']);
unset($_POST['startday']);
unset($_POST['startmonth']);
unset($_POST['endyear']);
unset($_POST['endday']);
unset($_POST['endmonth']);

/*
print_r($_POST);
exit();
*/

if (!$_POST['eli']) {
	$elis = ', eli=NULL, elidate=NULL'; //set explicitly if blank
	unset($_POST['eli']);
	unset($_POST['elistart']);
	unset($_POST['elidate']);
}

if (strlen($_POST['enddate']) < 6) {
	$ends = ', enddate = NULL';
	unset($_POST['enddate']);
}

//shortcut - all fields are text
foreach ($_POST as $field => $value) {
	$updates[] = $field.'='."'".htmlspecialchars($value)."'"; //see note in inspector-save.php
}

$query = "UPDATE inspector SET ".implode(', ', $updates).$elis." WHERE tid=$tid";
//print $query;
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if ($phones) {
	foreach ($phones as $phone) {
		$phonenum = "$phone[1]-$phone[2]-$phone[3]";
		$phoneext = $phone[ext];
		$phonetype = $phone[type];
	
		if (strlen($phonenum) == 12) {
			$query = "UPDATE inspector_phone SET number='$phonenum', ext='$phoneext', type='$phonetype' WHERE tpid=$phone[tpid]";
			mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		}
	}
}

header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspector-view.php?tid=$tid");
?>