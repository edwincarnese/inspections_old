<?php

require_once'session.php';
require_once'connect.php';

if(isset($_SERVER['HTTP_USER_AGENT']) and strpos($_SERVER['HTTP_USER_AGENT'],'MSIE')) {
	header('Content-Type: application/force-download');
} else {
	header('Content-Type: application/octet-stream');
}
header('Content-disposition: attachment; filename=apprentices.csv');

print '"Inspection #","Unit #","App First Name","App Last Name","App ELT"'."\n";

$query = "SELECT iid, number FROM units";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while ($row = mysql_fetch_assoc($result)) {
	print "\"$row[iid]\",\"$row[number]\"";
	$query = "SELECT firstname, lastname, elt FROM inspector INNER JOIN insp_assigned USING (tid) WHERE iid=$row[iid]";
	$result2 = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while($row2 = mysql_fetch_row($result2) ) {
		print ",\"$row2[0]\",\"$row2[1]\",\"$row2[2]\"";
	}
	print "\n";
}

?>