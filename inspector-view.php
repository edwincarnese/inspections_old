<?php 
$title = "View Inspector";
require_once'session.php';
require_once'connect.php';

$tid = $_POST['tid'] or $tid = $_GET['tid'];

$tid += 0; // (force numbers)

$query = "SELECT *, DATE_FORMAT(eltdate, '%c/%e/%Y') AS eltdate, DATE_FORMAT(elidate, '%c/%e/%Y') AS elidate, DATE_FORMAT(startdate, '%c/%e/%Y') AS startdate, DATE_FORMAT(enddate, '%c/%e/%Y') AS enddate FROM inspector WHERE tid=$tid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspector-list.php");
	exit();
}

$row = mysql_fetch_assoc($result);
/*
print "<!--\n";
print_r($row);
print "\n-->";
*/

require_once'header.php';
?>
<p><a href="inspector-edit.php?tid=<?php print $tid; ?>">Edit</a></p>
<?php
print "<p>$row[firstname] $row[lastname]<br />\n";
print "$row[streetnum] $row[address] $row[suffix]<br />\n";
if ($row['address2']) {print "$row[address2]<br />\n";}
print "$row[city], $row[state] $row[zip]<br />
$row[email]<br />\n";

$query = "SELECT * FROM inspector_phone WHERE tid=$tid";
$result2 = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while ($row2 = mysql_fetch_assoc($result2) ) {
	print "($row2[type]) $row2[number]&nbsp;&nbsp;&nbsp;";
	if ($row2['ext']) { print "Ext: $row2[ext]"; }
	print "<br />\n";
}
print "</p>\n";

print "<p>Start Date: $row[startdate]";
if ($row['enddate']) {
	print "<br />End Date: $row[enddate]";
}

print "<p>ELT License Number: $row[elt]";
print "<br />Acquired: $row[eltstart]";
print "<br />Expires: $row[eltdate]";
if ($row['eli']) {
	print "<br />\nELI License Number: $row[eli]";
	print "<br />Acquired: $row[elistart]";
	print "<br />Expires: $row[elidate]";
}
print "</p>\n";

if ($row['comments']) {
	print "\n<p>Comments:<br />$row[comments]</p>";
}
print "<br> Lead Capable: $row[lead_capable]";
?>
<hr />
<p>Add another phone number to this inspector:</p>
<form action="inspector-addnumber.php" method="post">
<p>
<input type="hidden" name="tid" value="<?php print $tid; ?>" />
Number: <input type="text" name="phone1" maxlength="3" size="4" />-<input type="text" name="phone2" maxlength="3" size="4" />-<input type="text" name="phone3" maxlength="4" size="5" /> Ext: <input type="text" name="phoneext" maxlength="5" size="6" /> Type: <select name="phonetype">
<option value="Work">Work</option>
<option value="Home">Home</option>
<option value="Cell">Cell</option>
<option value="Fax">Fax</option>
</select><br />
<input type="submit" value="Add" />
</p>
</form>

<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>
