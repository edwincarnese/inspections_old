<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

$query = "SELECT * FROM units  WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}

$unit = mysql_fetch_assoc($result);
$iid = $unit['iid'];
$unitnumber = $unit['number'];
$diagrams = $unit['diagrams'];

$query = "UPDATE units SET starttime=NOW() WHERE cuid=$cuid AND (starttime < '1000-01-01' OR starttime IS NULL)";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$addressquery = "SELECT address FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - $unit[unitdesc] - Component Sides";
require_once'header.php';
?>
<form action="inspection-comprehensive-ext-component-sides-save.php" method="post">
<input type="hidden" name="cuid" value="<?php print $cuid;?>" />
<?php

$query = "SELECT ccid, comprehensive_ext_components.name, side FROM comprehensive_ext_components WHERE cuid=$cuid ORDER BY comprehensive_ext_components.displayorder, comprehensive_ext_components.ccid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result)) {
?>
<table class="info">
<tr><th>Component</th><th>Side</th></tr>
<?php
	while ($row = mysql_fetch_assoc($result)) {
		print "<tr><td>$row[name]</a></td><td>\n";
		//$sides = array('None','1','2','3','4','Center');
$sides = array('A','B','C','D');
		foreach ($sides as $side) {

$loopSize = strlen($row['side']);
$letterSide = $row['side'];

			if ($letterSide[0] == $side || $letterSide[1] == $side || $letterSide[2] == $side || $letterSide[3] == $side) {
				print "<input type=\"checkbox\" name=\"sides[$row[ccid]-$side]\" value=\"$side\" checked=\"checked\" /><span class=\"current\">$side</span>\n";
			} else {
				print "<input type=\"checkbox\" name=\"sides[$row[ccid]-$side]\" value=\"$side\" />$side\n";
			}
		}
		print "</td></tr>\n\n";
	}
?>
</table>
<?php
}
?>
<p>
<input type="submit" name="submit" value="Save" /><input type="reset" value="Reset" /><input type="submit" name="submit" value="Cancel" />
</p>
</form>
<?php
require_once'footer.php';
?>