<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_GET['cuid'] or $cuid = 0;
$crid =$_GET['crid'] or $crid = 0;

if ($crid) {
	$query = "SELECT cuid, name FROM comprehensive_rooms WHERE crid=$crid";
	$result = mysql_query($query);
	list($cuid, $roomname) = mysql_fetch_row($result);
}

$query = "SELECT iid, unitdesc, number FROM units WHERE cuid=$cuid";
$result = mysql_query($query);
list($iid, $unitdesc, $unitnumber) = mysql_fetch_row($result);

$type='Interior';

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery);
$address = mysql_result($addressresult, 0);

if ($crid) {
	$title = "$iid-$unitnumber - $address - $unitdesc - $roomname - Paint Chips";
} else {
	$title = "$iid-$unitnumber - $address - $unitdesc - Paint Chips";
}
require_once'header.php';
?>
<table class="info">
<tr><th>Room Number</th><th>Room Name</th><th>Component</th><th>Side</th><th>Substrate</th><th>Sample Number</th></tr>
<?php
if (isset($crid)) {
	return "test";
	$query = "SELECT comprehensive_components.ccid, comprehensive_rooms.number AS roomnumber, comprehensive_rooms.name AS roomname, comprehensive_components.name AS compname, comprehensive_components.side AS side, substrates.name AS substrate, paintchips.number FROM comprehensive_rooms INNER JOIN comprehensive_components USING (crid) INNER JOIN substrates USING (sid) INNER JOIN paintchips ON (comprehensive_components.ccid=paintchips.ccid) WHERE comprehensive_rooms.crid=$crid AND paintchips.comptype='Interior' ORDER BY number, ccid";
} else {
	$query = "SELECT comprehensive_components.ccid, comprehensive_rooms.number AS roomnumber, comprehensive_rooms.name AS roomname, comprehensive_components.name AS compname, comprehensive_components.side AS side, substrates.name AS substrate, paintchips.number FROM comprehensive_rooms INNER JOIN comprehensive_components USING (crid) INNER JOIN substrates USING (sid) INNER JOIN paintchips ON (comprehensive_components.ccid=paintchips.ccid) WHERE cuid=$cuid AND paintchips.comptype='Interior' ORDER BY number, ccid";
}
$result = mysql_query($query);

while($row = mysql_fetch_assoc($result)) {
//	print_r($row);
	print "<tr><td>$row[roomnumber]</td><td>$row[roomname]</td><td>$row[compname]</td><td>$row[side]</td><td>$row[substrate]</td><td>$iid-$unitnumber".'P'."$row[number]</td></tr>";
}
?>
</table>

<?php
if ($crid) {
?>
<p><a href="inspection-comprehensive-room.php?crid=<?php print $crid; ?>">Room Main Menu</a></p>
<?php
}
?>

<p><a href="inspection-comprehensive-unit.php?cuid=<?php print $cuid; ?>"><?php print $unitdesc; ?> Main Menu</a></p>

<p><a href="inspection-main.php?iid=<?php print $iid; ?>">Inspection Main Menu</a></p>

<?php
require_once'footer.php';
?>