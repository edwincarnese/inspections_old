<?php 
require_once'connect.php';

require_once'session.php';

$title = "Add a client";
require_once'header.php';


$firstname = isset($_GET['firstname']) ? $_GET['firstname'] : '';
$lastname = isset($_GET['lastname']) ? $_GET['lastname'] : '';
$company = isset($_GET['company']) ? $_GET['company'] : '';
$city = isset($_GET['city']) ? $_GET['city'] : '';
$state = isset($_GET['state']) ? $_GET['state'] : '';
$zip = isset($_GET['zip']) ? $_GET['zip'] : '';


?>

<form action="client-save.php" method="post">
Nickname: <input type="text" name="nickname"  maxlength="30" size="15" />
Title: <input type="text" name="title"  maxlength="4" size="4" />
First Name: <input type="text" name="firstname"  maxlength="30" size="15" onkeyup="redme(this);" value="<?php print $firstname;?>"/>
MI: <input type="text" name="mi"  maxlength="1" size="2"/>
Last Name:<input type="text" name="lastname"  maxlength="30" size="15" onkeyup="redme(this);" value="<?php print $lastname;?>"/>
<br />
Company: <input type="text" name="company"  maxlength="75" size="30" onkeyup="redme(this);" value="<?php print $company;?>"/>
Comp Title: <input type="text" name="companytitle"  maxlength="75" size="30" onkeyup="redme(this);" />
<br />
St # <input type="text" name="streetnum"  maxlength="8" size="5" onkeyup="redme(this);" />
Address: <input type="text" name="address"  maxlength="30" onkeyup="redme(this);" />
Suffix: <input type="text" name="suffix"  maxlength="10" size="7" onkeyup="redme(this);" />
Address2:<input type="text" name="address2"  maxlength="30" /><br />
City:<input type="text" name="city"  maxlength="30" onkeyup="redme(this);" value="<?php print $city;?>"/>
State:<input type="text" name="state"  maxlength="2" size="3" onkeyup="redme(this);" value="<?php print $state;?>"/>
Zip:<input type="text" name="zip"  maxlength="5" size="6" onkeyup="redme(this);" value="<?php print $zip;?>"/><br />
Mailing St #:<input type="text" name="mailingnum"  maxlength="8" size="5" onkeyup="redme(this);" />
Mailing Address:<input type="text" name="mailingaddress"  maxlength="30" onkeyup="redme(this);" />
Mailing Suffix:<input type="text" name="mailingsuffix"  maxlength="10" size="7" onkeyup="redme(this);" />
Mailing 2:<input type="text" name="mailingaddress2"  maxlength="30" /><br />
<br />
Mailing City:<input type="text" name="mailingcity" maxlength="30" onkeyup="redme(this);" />
Mailing State:<input type="text" name="mailingstate" maxlength="2" size="3" onkeyup="redme(this);" />
Maling Zip:<input type="text" name="mailingzip" maxlength="5" size="6" onkeyup="redme(this);" /><br />
Phone (1):<input type="hidden" name="phones[1][cpid]"  /><input type="text" name="phones[1][1]"  maxlength="3" size="4" onkeyup="redme(this);" />-<input type="text" name="phones[1][2]"  maxlength="3" size="4" onkeyup="redme(this);" />-<input type="text" name="phones[1][3]"  maxlength="4" size="5" onkeyup="redme(this);" /> Ext: <input type="text" name="phones[1][ext]"  maxlength="5" size="6" /> Type: <select name="phones[1][type]" >
<?php $opts = array('Work','Home','Cell','Fax');
foreach ($opts as $opt) {
	if ($phonerow['type'] == $opt) {
		print "<option value=\"$opt\" selected=\"selected\">$opt</option>\n";
	} else {
		print "<option value=\"$opt\">$opt</option>\n";
	}
}
?>
</select> Balance Due: <input type="text" name="balancedue"  /><br />
Phone (2):<input type="hidden" name="phones[2][cpid]"  /><input type="text" name="phones[2][1]"  maxlength="3" size="4" />-<input type="text" name="phones[2][2]"  maxlength="3" size="4" />-<input type="text" name="phones[2][3]"  maxlength="4" size="5" /> Ext: <input type="text" name="phones[2][ext]"  maxlength="5" size="6" /> Type: <select name="phones[2][type]" >
<?php $opts = array('Work','Home','Cell','Fax');
foreach ($opts as $opt) {
	if ($phonerow['type'] == $opt) {
		print "<option value=\"$opt\" selected=\"selected\">$opt</option>\n";
	} else {
		print "<option value=\"$opt\">$opt</option>\n";
	}
}
?>
</select> Agent: <input type="text" name="agent"  /><br />
Phone (3):<input type="hidden" name="phones[3][cpid]" /><input type="text" name="phones[3][1]" maxlength="3" size="4" />-<input type="text" name="phones[3][2]" maxlength="3" size="4" />-<input type="text" name="phones[3][3]" maxlength="4" size="5" /> Ext: <input type="text" name="phones[3][ext]" maxlength="5" size="6" /> Type: <select name="phones[3][type]" >
<?php $opts = array('Work','Home','Cell','Fax');
foreach ($opts as $opt) {
	if ($phonerow['type'] == $opt) {
		print "<option value=\"$opt\" selected=\"selected\">$opt</option>\n";
	} else {
		print "<option value=\"$opt\">$opt</option>\n";
	}
}
?>
</select> License #: <input type="text" name="licensenumber" /><br />
<?php echo $row['comments']; ?> 
<br />
E-mail:<input type="text" name="email" maxlength="50" />
Class completed: <input type="radio" name="class_comp" />Yes&nbsp;<input type="radio" name="class_comp" />No
Class taken with:<input type="text" name="class_with" maxlength="50" onkeyup="redme(this);" /><br />
Comments:<textarea cols="40" rows="5" name="comments"><?php echo $row['comments']; ?></textarea><br />
<p><input type="submit" value="Save" /> <input type="reset" value="Reset" /></p>
</form>

<p><a href="index.php">Main Menu</a></p>
<?php

require_once'footer.php';
?>