<?php

require_once'connect.php';

$file = fopen('classlist.csv', 'r');

while ( ($fields = fgetcsv($file)) !== false) {
	foreach ($fields as &$field) {
		$field = addslashes($field);
	}
	$fields[7] = str_pad($fields[7], 5, '0', STR_PAD_LEFT); //fix excel dropping 0's from zip
	
	//fix date crap
	$chunks = explode('/', $fields[11]);
	if (count($chunks) > 1) {
		$date = "'$chunks[2]-$chunks[0]-$chunks[1]'";
	} else {
		$date = "NULL";
	}
	
	print "\n";
	$query = "INSERT INTO client (firstname, lastname, address, address2, city, state, zip, email, ownedunits, certdate, certnumber) VALUES ('$fields[1]', '$fields[0]', '$fields[3]', '$fields[4]', '$fields[5]', '$fields[6]', '$fields[7]', '$fields[8]', '$fields[10]', $date, '$fields[12]')";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$query = "SELECT LAST_INSERT_ID()";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$cid = mysql_result($result, 0);
	print $cid;

	//work phones
	print "\t".$fields[9];
	$fields[9] = preg_replace('/[^\dxX]+/', '', $fields[9]); //blank non-digit, non-x's
	print "\t".$fields[9];
	if ($fields[9]) {
//		print "\t".$fields[9];
		$parts = explode('x', $fields[9]);
		if (is_array($parts) && count($parts) > 1) {
			$ext = $parts[1];
		}
		trim($parts[0]);
		if (strlen($parts[0]) == 7) {
			$parts[0] = '401'.$parts[0];
		}
		$number = substr($parts[0], 0, 3).'-'.substr($parts[0], 3, 3).'-'.substr($parts[0], 6, 4);
		if (strlen($number) == 12) {
			$query = "INSERT INTO client_phone (cid, type, number, ext) VALUES ($cid, 'Work', '$number', '$ext')";
			mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		}
	}
	
	//"home" phones
	$phones = preg_split('/\s*(|\\\\)\/\s*|\s*,\s*/', $fields[2], -1, PREG_SPLIT_NO_EMPTY);
//	print_r($phones);
	if ($phones) {
		foreach ($phones as $phone) {
			print "\t".$phone;
			$phone = preg_replace('/[^\dxX]+/', '', $phone); //blank non-digit, non-x's
//			print "\t".$phone;
			if ($phone) {
//				print "\t".$phone;
				$parts = explode('x', $phone);
				if (is_array($parts) && count($parts) > 1) {
					$ext = $parts[1];
				}
				trim($parts[0]);
				if (strlen($parts[0]) == 7) {
					$parts[0] = '401'.$parts[0];
				}
				$number = substr($parts[0], 0, 3).'-'.substr($parts[0], 3, 3).'-'.substr($parts[0], 6, 4);
				if (strlen($number) == 12) {
					$query = "INSERT INTO client_phone (cid, type, number, ext) VALUES ($cid, 'Home', '$number', '$ext')";
					mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
				}
			}
		}
	}

} //next line
print "\n";
?>