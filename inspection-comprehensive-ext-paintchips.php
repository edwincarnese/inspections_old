<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

$query = "SELECT iid, unitdesc, number FROM units WHERE cuid=$cuid";
$res = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
list($iid, $unitdesc, $unitnumber) = mysql_fetch_row($res);

$type='Exterior';

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - $unitdesc - Paint Chip Samples";
require_once'header.php';
?>
<table class="info">
<tr><th>Component</th><th>Side</th><th>Substrate</th><th>Sample Number</th></tr>
<?php
$query = "SELECT comps.ccid, comps.name AS compname, comps.side AS side, substrates.name AS substrate FROM substrates INNER JOIN comprehensive_ext_components AS comps USING (sid) INNER JOIN paintchips USING (ccid) WHERE cuid=$cuid AND paintchips.comptype='Exterior' ORDER BY ccid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while($row = mysql_fetch_assoc($result)) {
	print "<tr><td>$row[compname]</td><td>$row[side]</td><td>$row[substrate]</td><td>$iid".'<b>PE</b>'."$row[ccid]</td></tr>";
}
?>
</table>

<p><a href="inspection-comprehensive-unit.php?cuid=<?php print $cuid; ?>"><?php print $unitdesc; ?> Main Menu</a></p>

<p><a href="inspection-main.php?iid=<?php print $iid; ?>">Inspection Main Menu</a></p>

<?php
require_once'footer.php';
?>