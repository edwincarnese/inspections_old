<?php

//this page requires crid

$pdf->AddPage('P');
$pdf->Image('images/fixed/comprehensive-exterior-test.png', 0, 0, 8.5, 11.0, 'PNG');

// x - 0.03; y - 0.06 from line start

//PAGE NUMBERS
$pdf->SetFont('Arial','','12');
$pdf->SetXY(7.02, 0.26);
$pdf->Cell(0,0, $pdf->PageNo());
$pdf->SetXY(7.53, 0.26);
$pdf->Cell(0,0, '{totalpages}');

//ADDRESS, TOP
$query = "SELECT inspection.iid, streetnum, address, suffix, city, unitdesc, number FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

$unitnumber = $row['number'];

$lh = 0.4;
$pdf->SetFont('Arial','','12');

$pdf->SetXY(1.75, 0.15);
$pdf->Cell(0,0, "Inspection $row[iid]-$row[number]");

$iid = $row['iid'];

$pdf->SetXY(1.02, 0.76);
$pdf->Cell(0,0, "$row[streetnum] $row[address] $row[suffix], $row[city]");
$pdf->SetXY(5.61, 0.76);
$pdf->Cell(0,0, 'Schneider Laboratories, Inc');

$query = "SELECT basementwindows, atticwindows, garagewindows FROM comprehensive_ext WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
$pdf->SetXY(1.94, 1.02);
$pdf->Cell(0,0, $row['basementwindows']);
$pdf->SetXY(4.00, 1.02);
$pdf->Cell(0,0, $row['atticwindows']);
$pdf->SetXY(6.25, 1.02);
$pdf->Cell(0,0, $row['garagewindows']);


//DRAW PART OF GRID
$pdf->SetLineWidth(0.0185);
$pdf->Rect(0.32, 1.75, (8.20-0.42), (9.23-1.75));
$pdf->Line(1.82, 1.75, 1.82, 9.23);
$pdf->Line(2.25, 1.75, 2.25, 9.23);
$pdf->Line(3.01, 1.75, 3.01, 9.23);
$pdf->Line(3.39, 1.75, 3.39, 9.23);
$pdf->Line(4.52, 1.75, 4.52, 9.23);
$pdf->Line(5.35, 1.75, 5.35, 9.23);
$pdf->Line(5.81, 1.75, 5.81, 9.23);
$pdf->Line(6.48, 1.75, 6.48, 9.23);
$pdf->Line(7.22, 1.75, 7.22, 9.23);

$lh = 0.22;
$pdf->SetXY(0.32, 1.75);
$map = array('Lead-free' => 'F      ', 'Lead-safe' => 'S      ', 'Hazard' => '      H');
$condmap = array('Intact' => 'I      ', 'Intact Where Visible' => 'IWV      ', 'Damaged Touchup' => '      DT', 'Damaged' => '      D', 'Assumed Damaged' => '      D', 'No Paint' => 'N      ', 'Post \'77' => '78      ', 'Damaged - Friction' => '      DF', 'Covered' => 'CV      ', 'Intact - Factory Finish' => 'IFF      ');
$start = $start + 0; //favorite force to number trick

$query = "SELECT SQL_CALC_FOUND_ROWS comprehensive_ext_components.ccid, comprehensive_ext_components.name, side, abbreviation AS substrate, xrf, DATE_FORMAT(xrftaken, '%l:%i %p') AS xrftaken, 'condition', spottest, paintchips.number AS chipnumber, paintchips.results, hazardassessment FROM comprehensive_ext_components INNER JOIN substrates USING (sid) LEFT JOIN paintchips ON (comprehensive_ext_components.ccid=paintchips.ccid AND paintchips.comptype='Exterior') WHERE cuid=$cuid ORDER BY comprehensive_ext_components.displayorder, comprehensive_ext_components.ccid LIMIT $start, 34";

$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while (($row = mysql_fetch_assoc($result)) && ($pdf->GetY() < 9.00)) { //don't overflow
	formfix($row);
	$start++; //keep running count for next time
	$pdf->SetX(0.32);
	$nowy = $pdf->GetY();
	$pdf->MultiCell( (1.82-0.32), $lh, $row['name'], 1, 'L');
	$nexty = $pdf->GetY();
	$pdf->SetXY(1.82, $nowy);
	$pdf->Cell( (2.35-1.92), ($nexty - $nowy), $row['side'], 1, 0, 'C');
	$pdf->Cell( (3.11-2.35), ($nexty - $nowy), $row['substrate'], 1, 0, 'C');
	$pdf->Cell( (3.49-3.11), ($nexty - $nowy), $row['xrf'], 1, 0, 'C');
	//don't display xrf time, but keep items aligned
	$row['xrftaken'] = '';
	$pdf->Cell( (4.62-3.49), ($nexty - $nowy), $row['xrftaken'], 1, 0, 'C');
	$pdf->Cell( (5.45-4.62), ($nexty - $nowy), $condmap[$row['condition']], 1, 0, 'C');
	$pdf->Cell( (5.91-5.45), ($nexty - $nowy), $row['spottest'], 1, 0, 'C');
	if ($row['chipnumber']) {
		$sample = $iid.'-'.$unitnumber.'P'.$row['chipnumber'];
	} else {
		$sample = '';
	}	
	$pdf->Cell( (6.58-5.91), ($nexty - $nowy), $sample, 1, 0, 'C');
	$pdf->Cell( (7.32-6.58), ($nexty - $nowy), $row['results'], 1, 0, 'C');
	$haz = $row['hazardassessment'];
	$letter = $map["$haz"];
	$pdf->Cell( (8.20-7.32), ($nexty - $nowy), " $letter ", 1, 0, 'C');
	$pdf->SetY($nexty);
}
mysql_free_result($result); //can sometimes be big

$query = "SELECT FOUND_ROWS()";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$total = mysql_result($result, 0);
// It is the calling script's job to deal with $start and $total appropriately.

//crossed out items
$query = "SELECT COUNT(*) FROM comprehensive_ext_components WHERE cuid=$cuid AND  FIND_IN_SET('Exterior Siding', grouplist) > 0";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_result($result, 0) == 0) {
	if (!isset($ESdone)) { //not done in a previous loop
		if ($pdf->GetY() < 9.00) {
			$start++; //keep running count for next time
			$pdf->SetX(0.32);
			$nowy = $pdf->GetY();
			$pdf->MultiCell( (1.82-0.32), $lh, 'Exterior Siding', 1, 'L');
			$pdf->Line(0.36, $pdf->GetY()-0.11, 1.55, $pdf->GetY()-0.11);
			$nexty = $pdf->GetY();
			$pdf->SetXY(1.82, $nowy);
			$pdf->Cell( (2.35-1.92), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (3.11-2.35), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (3.49-3.11), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (4.62-3.49), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (5.45-4.62), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (5.91-5.45), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (6.58-5.91), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (7.32-6.58), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (8.20-7.32), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->SetY($nexty);
			$ESdone = 1;
		} else {
			$total++; //need to trigger a loop, perhaps
		}
	}
}
$query = "SELECT COUNT(*) FROM comprehensive_ext_components WHERE cuid=$cuid AND FIND_IN_SET('Exterior Trim', grouplist) > 0";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_result($result, 0) == 0) {
	$total += 1;
	if (!isset($ETdone)) { //not done in a previous loop
		if ($pdf->GetY() < 9.00) {
			$start++; //keep running count for next time
			$pdf->SetX(0.32);
			$nowy = $pdf->GetY();
			$pdf->MultiCell( (1.82-0.32), $lh, 'Exterior Trim', 1, 'L');
			$pdf->Line(0.36, $pdf->GetY()-0.11, 1.55, $pdf->GetY()-0.11);
			$nexty = $pdf->GetY();
			$pdf->SetXY(1.82, $nowy);
			$pdf->Cell( (2.35-1.92), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (3.11-2.35), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (3.49-3.11), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (4.62-3.49), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (5.45-4.62), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (5.91-5.45), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (6.58-5.91), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (7.32-6.58), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (8.20-7.32), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->SetY($nexty);
			$ETdone = 1;
		} else {
			$total++; //need to trigger a loop, perhaps
		}
	}
}
$query = "SELECT COUNT(*) FROM comprehensive_ext_components WHERE cuid=$cuid AND FIND_IN_SET('Porches', grouplist) > 0";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_result($result, 0) == 0) {
	$total += 1;
	if (!isset($EPdone)) { //not done in a previous loop
		if ($pdf->GetY() < 9.00) {
			$start++; //keep running count for next time
			$pdf->SetX(0.32);
			$nowy = $pdf->GetY();
			$pdf->MultiCell( (1.82-0.32), $lh, 'Porches', 1, 'L');
			$pdf->Line(0.36, $pdf->GetY()-0.11, 1.55, $pdf->GetY()-0.11);
			$nexty = $pdf->GetY();
			$pdf->SetXY(1.82, $nowy);
			$pdf->Cell( (2.35-1.92), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (3.11-2.35), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (3.49-3.11), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (4.62-3.49), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (5.45-4.62), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (5.91-5.45), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (6.58-5.91), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (7.32-6.58), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( (8.20-7.32), ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->SetY($nexty);
			$EPdone = 1;
		} else {
			$total++; //need to trigger a loop, perhaps
		}
	}
}

while ($pdf->GetY() < 9.00) { //extra lines
	$pdf->SetX(0.32);
	$pdf->MultiCell( (8.20-0.42), $lh, '', 1, 'L');
}

//date/time at bottom
$query = "SELECT DATE_FORMAT(starttime, '%c/%e/%Y') AS startdate, DATE_FORMAT(starttime, '%l:%i %p') AS starttime FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_row($result);
$pdf->SetXY(6.96, 10.36);
$pdf->Cell(0,0, $row[0]);

?>