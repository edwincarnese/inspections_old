<?php

require_once'session.php';
require_once'connect.php';

$ccid = $_POST['ccid'] or $ccid = $_GET['ccid'] or $ccid = 0;
$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

if ($ccid) { //item was selected
	$query = "SELECT * FROM comprehensive_ext_components WHERE ccid=$ccid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	if (mysql_num_rows($result) == 0) {
		header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
		exit();
	}
	$comp = mysql_fetch_assoc($result);
	$cuid = $comp['cuid'];
}	

//get ext info, either way.
$query = "SELECT * FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}
$unit = mysql_fetch_assoc($result);
$iid = $unit['iid'];
$unitnumber = $unit['number'];

if (!$ccid) { //item was not selected
	$query = "SELECT * FROM comprehensive_ext_components WHERE cuid=$cuid AND hazardassessment IS NULL ORDER BY comprehensive_ext_components.displayorder, comprehensive_ext_components.ccid LIMIT 1";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	if (mysql_num_rows($result) == 0) {
		if ($cuid) {
			header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-comprehensive-exterior.php?cuid=$cuid");
		} else {
			header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
		}
		exit();
	}
	$comp = mysql_fetch_assoc($result);
	$ccid = $comp['ccid'];
}

//Separate fetch for paintchips now
$query = "SELECT COUNT(*) FROM paintchips WHERE ccid=$ccid AND comptype='Exterior'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_result($result, 0) ) {
	$comp['paintchips'] == 'Yes';
} else {
	$comp['paintchips'] == 'No';
} //good enough for the include file

$type='Exterior';
require_once'include-comprehensive-xrfcheck.php';

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$droptitle = true;
$title = "$iid-$unitnumber - $address - $unit[unitdesc] - Component - $comp[name]";
require_once'header.php';
?>
<p></p>
<form action="inspection-comprehensive-ext-component-save.php" method="post">
<?php

// ********************* FORM MOVED TO A COMMON FILE *************************** //

require_once'include-common-component.php';
?>

<p>XRF / unit comments (Use save button above):<br />
<textarea name="comment" rows="6" cols="50"><?php
$query = "SELECT comment FROM comprehensive_comments WHERE cuid=$cuid AND type='xrf'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) ) {
	$comment = mysql_result($result, 0);
}
print $comment;
?></textarea></p>
</form>

<p><a href="inspection-comprehensive-unit.php?cuid=<?php print $cuid; ?>">Exterior Main Menu</a></p>

<p><a href="inspection-main.php?iid=<?php print $unit[iid]; ?>">Inspection Main Menu</a></p>

<?php
require_once'footer.php';
?>