<?php

require_once'session.php';
require_once'connect.php';

/*
print_r($_POST);
exit();
*/

$side = "";
if(isset($_POST["side-0"]) || isset($_POST["side-1"]) || isset($_POST["side-2"]) || isset($_POST["side-3"]))
{
$side_0 = $_POST["side-0"];
$side_1 = $_POST["side-1"];
$side_2 = $_POST["side-2"];
$side_3 = $_POST["side-3"];
$side .= $side_0 . $side_1 . $side_2 . $side_3;
}

$ccid = $_POST['ccid'];
$pass_condition = mysql_real_escape_string($_POST['condition']);

$query = "SELECT comprehensive_rooms.crid, cuid FROM comprehensive_components INNER JOIN comprehensive_rooms USING (crid) WHERE ccid=$ccid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
list ($crid, $cuid) = mysql_fetch_row($result);

if ($_POST['submit'] == 'Cancel') {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-comprehensive-room.php?crid=$crid");
	exit();
}

if ($_POST['submit'] == 'Save' || $_POST['submit'] == 'Enable XRF Readings' ) {

	if ($_POST['comment']) {
		$query = "INSERT INTO comprehensive_comments (cuid, type, comment) VALUES ($cuid, 'xrf', '$_POST[comment]') ON DUPLICATE KEY UPDATE comment='$_POST[comment]'";
	} else {
		$query = "DELETE FROM comprehensive_comments WHERE cuid=$cuid AND type='xrf'";
	}
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

	if (strlen($_POST['xrf']) < 3) {
		$xrf = ($_POST['xrf']+0).'.'.($_POST['xrf2']+0);
	} else {
		$xrf = $_POST['xrf'];
	}

	if ($_POST['condition'] == "Post \\'77" || $_POST['condition'] == 'No Paint') {
		$xrf = '';
//echo "<script> alert('$_POST[condition]'); </script>";
	}
	
	// ************ AUTO-ASSESSMENT CODE ************ //
	switch($_POST['condition']) {
		case 'No Paint':
		case 'Post \\\'77': // passed as "Post \'77", escape \ and '
			$_POST['hazardassessment'] = 'Lead-free';
			break;
		case 'Intact':
		case 'Intact Where Visible':
		case 'Intact - Factory Finish':
		case 'Covered':
			$_POST['hazardassessment'] = 'Lead-safe';
			break;
		default:
			$_POST['hazardassessment'] = 'Hazard';
	}

	//check to see if XRF reading has changed; if so, assume taken now
	$query = "SELECT xrf FROM comprehensive_components WHERE ccid=$_POST[ccid]";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	if ($xrf == mysql_result($result, 0)) { //leave alone if same
		$query = "UPDATE comprehensive_components SET side='$side', sid='$_POST[sid]', xrf='$xrf', hazards='$pass_condition', spottest='$_POST[spottest]', hazardassessment='$_POST[hazardassessment]' WHERE ccid=$_POST[ccid]";
	} else { //change time if not
		$query = "UPDATE comprehensive_components SET xrftaken=NOW(), side='$side', sid='$_POST[sid]', xrf='$xrf', hazards='$pass_condition', spottest='$_POST[spottest]', hazardassessment='$_POST[hazardassessment]' WHERE ccid=$_POST[ccid]";
	}
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	
	//update sides on components that don't have one yet (assumed to be same item)
	$query = "UPDATE comprehensive_components SET side='$side' WHERE crid=$crid AND side IS NULL";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	
	//paint chips moved to separate table
	if ($_POST['paintchips'] == 'Yes') {
		$query = "SELECT COUNT(*) FROM paintchips WHERE ccid=$ccid and comptype='Interior'";
		$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		if (mysql_result($result, 0) == 0) { //not inserted
			//now we need the iid
			$query = "SELECT iid FROM comprehensive_components INNER JOIN comprehensive_rooms USING (crid) INNER JOIN units USING (cuid) WHERE ccid=$ccid";
			$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			$iid = mysql_result($result, 0);
			//now find max number
			$query = "SELECT MAX(number) FROM paintchips WHERE iid=$iid";
			$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			$max = mysql_result($result, 0);
			$next = $max + 1;
			$query = "INSERT INTO paintchips (iid, number, ccid, comptype) VALUES ($iid, $next, $ccid, 'Interior')";
			mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		} //no else, leave alone if already inserted
	} else { //No
		$query = "DELETE FROM paintchips WHERE ccid=$ccid and comptype='Interior'";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
	
	//ENDTIME
	$query = "UPDATE units SET endtime=NOW() WHERE cuid=$cuid";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
}

if ($_POST['submit'] == 'Delete') { //must delete after determining room
	$query = "DELETE FROM comprehensive_components WHERE ccid=$ccid";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$query = "DELETE FROM paintchips WHERE ccid=$ccid and comptype='Interior'";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

	//ENDTIME
	$query = "UPDATE units SET endtime=NOW() WHERE cuid=$cuid";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

echo "<script>window.location.href = 'http://inspections.homeenergyremedies.com//inspection-comprehensive-room.php?crid=$crid'</script>";

}

if ($_POST['submit'] == 'Enable XRF Readings') {
	$_SESSION['skipXRF'] = false;
}
 

header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-comprehensive-room.php?crid=$crid");

//header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-comprehensive-room-component.php?ccid=$ccid");

//header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-comprehensive-room-component.php?crid=$crid");
?>