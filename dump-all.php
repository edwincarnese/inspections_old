<?php

require_once'session.php';
require_once'connect.php';

if(isset($_SERVER['HTTP_USER_AGENT']) and strpos($_SERVER['HTTP_USER_AGENT'],'MSIE')) {
	header('Content-Type: application/force-download');
} else {
	header('Content-Type: application/octet-stream');
}
header('Content-disposition: attachment; filename=full.csv');

print '"First Name","Last Name","Address","City","State","Zip","Building Address","Building City","Building State","Building Zip","Inspection #","Inspection Type","Inspection Date (Scheduled)","Inspection Time (Scheduled)","Inspection Started","Units Inspected","Inspector First Name","Inspector Last Name","ELT","ELI"'."\n";


$query = "SELECT firstname, lastname, CONCAT(client.streetnum, ' ', client.address, ' ', client.suffix), client.city, client.state, client.zip, CONCAT(building.streetnum, ' ', building.address, ' ', building.suffix) AS badd, building.city AS bcity, building.state AS bstate, building.zip AS bzip, iid, type, DATE_FORMAT(scheddate, '%c/%e/%Y'), schedstart, DATE_FORMAT((SELECT MIN(starttime) FROM units WHERE iid=inspection.iid), '%c/%e/%Y %h:%i%p') FROM client INNER JOIN owners ON (client.cid=owners.cid AND startdate <= CURDATE() and enddate >= CURDATE() ) INNER JOIN building USING (bid) INNER JOIN inspection USING (bid)";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while ($row = mysql_fetch_assoc($result)) {
	$units = array();
	print '"'.implode('","', $row).'"';
	$query = "SELECT unitdesc FROM units WHERE iid=$row[iid]";
	$result2 = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while($row2 = mysql_fetch_row($result2) ) {
		$units[] = $row2[0];
	}
	print ',"'.implode(',', $units).'"';
	$query = "SELECT firstname, lastname, elt, eli FROM inspector INNER JOIN insp_assigned USING (tid) WHERE iid=$row[iid]";
	$result2 = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while($row2 = mysql_fetch_row($result2) ) {
		print ",\"$row2[0]\",\"$row2[1]\",\"$row2[2]\",\"$row2[3]\"";
	}
	print "\n";
}

?>