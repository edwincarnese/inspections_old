<?php

//$pdf->AddPage('P');
//$pdf->Image('images/fixed/conformance2.png', 0, 0, 8.5, 11.0, 'PNG');
//Startr Form

$x = 0;
$y = 0;
$ml=.625;
$w=7.25;
$pdf->SetFont('Times','','12');
$pdf->SetLineWidth(0.014);
//$pdf->SetLineWidth(0.04);
//Start Banner box package
$y=$y+0.52; 		//Increment Y from last input.
$ys=$y-0.2;  	//Save Y at start.
$ff=65;			//Chang in $y = $fontsize/$ff
$fontsize=12;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml+2.35, $y);
$pdf->Cell(0,0,'CLEARANCE INSPECTION');
$pdf->line($ml, $y+0.12, $ml+$w, $y+0.12);	
$pdf->line($ml, $y+0.14, $ml+$w, $y+0.14);	
$y=$y+$fontsize/$ff;	//Increment to next line based on font size
//End Banner box package
$fontsize=10;
$y=$y+0.12;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'Property Address:');
$pdf->SetXY($ml+4.4, $y);
$pdf->Cell(0,0,'Unit:');
$pdf->line($ml, $y+$fontsize/($ff*2), $ml+$w, $y+$fontsize/($ff*2));	
$y=$y+0.25;

$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'Date:');
$pdf->line($ml+0.6, $y+$fontsize/($ff*2), $ml+1.4, $y+$fontsize/($ff*2));	
$pdf->SetXY($ml+1.4, $y);
$pdf->Cell(0,0,'Inspector Name:');
$pdf->line($ml+2.4, $y+$fontsize/($ff*2), $ml+5, $y+$fontsize/($ff*2));	
$pdf->SetXY($ml+5.0, $y);
$pdf->Cell(0,0,'License #:');
$pdf->line($ml+5.7, $y+$fontsize/($ff*2), $ml+6.5, $y+$fontsize/($ff*2));	
$pdf->SetXY($ml+5.4, $y);
$y=$y+$fontsize/$ff;	//Increment to next line based on font size
$y=$y+0.08;
////
//Banner box package
$ys=$y-0.12; // This adjusts the boxes height
$fontsize=10;
$ff=65;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml+0.1, $y+0.05);
$pdf->Cell(0,0,'Visual Inspection:');
$pdf->SetXY($ml+1.365, $y+0.05);
$pdf->Cell(0,0,'Passed:');
$pdf->Rect($ml+2, $y-0.02, 0.1, 0.1);
$pdf->SetXY($ml+2.365, $y+0.05);
$pdf->Cell(0, 0, 'Inspection Failed:');
$pdf->Rect($ml+3.75, $y-0.02, 0.1, 0.1);
$pdf->SetXY($ml+4.375, $y+0.05);
$pdf->Cell(0, 0, 'Time of Inspection:');
$y=$y+$fontsize/$ff;
$pdf->Rect($ml, $ys, $w, $y-$ys);
$y=$y+0.15;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'1.  INTERIOR PAINT HAZARD[S]');
$pdf->line($ml+3.8, $y+$fontsize/($ff*2), $ml+4.2, $y+$fontsize/($ff*2));	
$pdf->SetXY($ml+4.3, $y);
$pdf->Cell(0,0, 'YES');
$pdf->line($ml+5.1, $y+$fontsize/($ff*2), $ml+5.5, $y+$fontsize/($ff*2));	
$pdf->SetXY($ml+5.6, $y);
$pdf->Cell(0,0, 'NO');
////////
$yi=0.26;
$y=$y+$yi;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'2.  EXTERIOR PAINT HAZARD[S]');
$pdf->line($ml+3.8, $y+$fontsize/($ff*2), $ml+4.2, $y+$fontsize/($ff*2));	
$pdf->SetXY($ml+4.3, $y);
$pdf->Cell(0,0, 'YES');
$pdf->line($ml+5.1, $y+$fontsize/($ff*2), $ml+5.5, $y+$fontsize/($ff*2));	
$pdf->SetXY($ml+5.6, $y);
$pdf->Cell(0,0, 'NO');
$y=$y+$yi;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'3.  DUST HAZARD[S]');
$pdf->line($ml+1.5, $y+$fontsize/($ff*2), $ml+3.7, $y+$fontsize/($ff*2));	
$pdf->line($ml+3.8, $y+$fontsize/($ff*2), $ml+4.2, $y+$fontsize/($ff*2));	
$pdf->SetXY($ml+4.3, $y);
$pdf->Cell(0,0, 'YES');
$pdf->line($ml+5.1, $y+$fontsize/($ff*2), $ml+5.5, $y+$fontsize/($ff*2));	
$pdf->SetXY($ml+5.6, $y);
$pdf->Cell(0,0, 'NO');
$y=$y+$yi;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'4.  SOIL HAZARD[S]');
$pdf->line($ml+1.5, $y+$fontsize/($ff*2), $ml+3.7, $y+$fontsize/($ff*2));	
$pdf->line($ml+3.8, $y+$fontsize/($ff*2), $ml+4.2, $y+$fontsize/($ff*2));	
$pdf->SetXY($ml+4.3, $y);
$pdf->Cell(0,0, 'YES');
$pdf->line($ml+5.1, $y+$fontsize/($ff*2), $ml+5.5, $y+$fontsize/($ff*2));	
$pdf->SetXY($ml+5.6, $y);
$pdf->Cell(0,0, 'NO');
$y=$y+$yi;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'1.  FRONT COMMON HAZARD[S]');
$pdf->line($ml+3.8, $y+$fontsize/($ff*2), $ml+4.2, $y+$fontsize/($ff*2));	
$pdf->SetXY($ml+4.3, $y);
$pdf->Cell(0,0, 'YES');
$pdf->line($ml+5.1, $y+$fontsize/($ff*2), $ml+5.5, $y+$fontsize/($ff*2));	
$pdf->SetXY($ml+5.6, $y);
$pdf->Cell(0,0, 'NO');
$y=$y+$yi;
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'6.  REAR COMMON HAZARD[S]');
$pdf->line($ml+3.8, $y+$fontsize/($ff*2), $ml+4.2, $y+$fontsize/($ff*2));	
$pdf->SetXY($ml+4.3, $y);
$pdf->Cell(0,0, 'YES');
$pdf->line($ml+5.1, $y+$fontsize/($ff*2), $ml+5.5, $y+$fontsize/($ff*2));	
$pdf->SetXY($ml+5.6, $y);
$pdf->Cell(0,0, 'NO');

//Banner box package
$y=$y+0.42;
$ys=$y-0.1; // This adjusts the boxes height
$fontsize=10;
$ff=65;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'INTERIOR ROOMS');
$y=$y+$fontsize/$ff-0.05;
$pdf->Rect($ml, $ys, $w, $y-$ys);
$y=$y+0.2;
///////
//Banner box package
$ys=$y; // This adjusts the boxes height
$fontsize=10;
$ff=65;
$shiftx=$ml;
$wb=0.625;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($shiftx+0.1, $y);
$y=$y+$fontsize/$ff;
$pdf->Cell(0,0,'Room #');
$pdf->SetXY($shiftx+0.1, $y);
$y=$y+$fontsize/$ff;
$pdf->Cell(0,0,'');
$pdf->SetXY($shiftx+0.1, $y);
$y=$y+$fontsize/$ff;
$pdf->Cell(0,0,'');
//$pdf->Rect($shiftx, $ys-0.08, $wb, $y-$ys);

$shiftx=$shiftx+$wb;
$wb=1.25;
$y=$ys;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($shiftx+0.3, $y);
$y=$y+$fontsize/$ff;
$pdf->Cell(0,0,'Room Name');
$pdf->SetXY($shiftx+0.1, $y);
$y=$y+$fontsize/$ff;
$pdf->Cell(0,0,'');
$pdf->SetXY($shiftx+0.1, $y);
$y=$y+$fontsize/$ff;
$pdf->Cell(0,0,'');
//$pdf->Rect($shiftx, $ys-0.08, $wb, $y-$ys);

$y=$ys;
$shiftx=$shiftx+$wb;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($shiftx+2, $y);
$y=$y+$fontsize/$ff;
$pdf->Cell(0,0,'Paint Hazards (circled)');
$pdf->SetXY($shiftx+0.1, $y);
$y=$y+$fontsize/$ff;
$pdf->Cell(0,0,'');
$pdf->SetXY($shiftx, $y);
$y=$y+$fontsize/$ff;
$pdf->Cell(0,0,' YES    NO');
//$pdf->Rect($shiftx, $ys-0.08, $w-$shiftx+$ml, $y-$ys);

//$pdf->line($ml, $y+.12, $ml+$w, $y+.12);

$y=$y-0.045;
$yi=0.1665;
		$pdf->line($ml, $ys-0.08, $ml+$w, $ys-0.08);

$count=13;
		$pdf->line($ml, $ys-0.08, $ml, $y+($yi*($count))+0.12);
		$pdf->line($ml+0.625, $ys-0.08, $ml+0.625, $y+($yi*($count))+0.12);
		$pdf->line($ml+1.89, $ys-0.08, $ml+1.89, $y+($yi*($count))+0.12);
		$pdf->line($ml+2.29, $y-0.04, $ml+2.29, $y+($yi*($count))+0.12);
		$pdf->line($ml+2.69, $y-0.04, $ml+2.69, $y+($yi*($count))+0.12);
		$pdf->line($ml+$w, $ys-0.08, $ml+$w, $y+($yi*($count))+0.12);
$c=0;		
$yfudge=0.04;
	while ($c <= $count) {
		$pdf->SetXY($ml+2.75, $y+0.05);
		$pdf->Cell(0,0,'Ceiling , Walls, Trim, Floor, Window,  Door, Cabinet, Other');
		$pdf->line($ml, $y-$yfudge, $ml+$w, $y-$yfudge);
	
		$c=$c+1;
		$y=$y+$yi;
}
		$pdf->line($ml, $y-$yfudge, $ml+$w, $y-$yfudge);
///////

//Banner box package
$y=$y+0.25;
$ys=$y-0.1; // This adjusts the boxes height
$fontsize=10;
$ff=65;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml+0.1, $y);
$pdf->Cell(0,0,'EXTERIOR:');
$y=$y+$fontsize/$ff-0.05;
$pdf->Rect($ml, $ys, $w, $y-$ys);
$y=$y+0.2;
///////
//Banner box package
$ys=$y; // This adjusts the boxes height
$fontsize=10;
$ff=65;
$shiftx=$ml;
$wb=0.625;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($shiftx+0.1, $y);
$y=$y+$fontsize/$ff;
$pdf->Cell(0,0,'Side #');
$pdf->SetXY($shiftx+0.1, $y);
$y=$y+$fontsize/$ff;
$pdf->Cell(0,0,'');
$pdf->SetXY($shiftx+0.1, $y);
$y=$y+$fontsize/$ff;
$pdf->Cell(0,0,'');
//$pdf->Rect($shiftx, $ys-0.08, $wb, $y-$ys);

$shiftx=$shiftx+$wb;
$wb=1.25;
$y=$ys;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($shiftx+0.3, $y);
$y=$y+$fontsize/$ff;
$pdf->Cell(0,0,'Side Name');
$pdf->SetXY($shiftx+0.1, $y);
$y=$y+$fontsize/$ff;
$pdf->Cell(0,0,'');
$pdf->SetXY($shiftx+0.1, $y);
$y=$y+$fontsize/$ff;
$pdf->Cell(0,0,'');
//$pdf->Rect($shiftx, $ys-0.08, $wb, $y-$ys);

$y=$ys;
$shiftx=$shiftx+$wb;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($shiftx+2, $y);
$y=$y+$fontsize/$ff;
$pdf->Cell(0,0,'Paint Hazards (circled)');
$pdf->SetXY($shiftx+0.1, $y);
$y=$y+$fontsize/$ff;
$pdf->Cell(0,0,'');
$pdf->SetXY($shiftx, $y-0.01);
$y=$y+$fontsize/$ff;
$pdf->Cell(0,0,' YES    NO');
//$pdf->Rect($shiftx, $ys-0.08, $w-$shiftx+$ml, $y-$ys);

//$pdf->line($ml, $y+.12, $ml+$w, $y+.12);

$y=$y-0.065;
$yi=0.164;
		$pdf->line($ml, $ys-0.08, $ml+$w, $ys-0.08);

$count=3;
$yy=$y+($yi*2*$count)+0.3;
		$pdf->line($ml, $ys-0.08, $ml, $yy);
		$pdf->line($ml+0.625, $ys-0.08, $ml+0.625, $yy);
		$pdf->line($ml+1.89, $ys-0.08, $ml+1.89, $yy);
		$pdf->line($ml+2.29, $y-0.02, $ml+2.29, $yy);
		$pdf->line($ml+2.69, $y-0.02, $ml+2.69, $yy);
		$pdf->line($ml+$w, $ys-0.08, $ml+$w, $yy);
$c=0;		
$pdf->SetFont('Times','', $fontsize);
$yfudge=0.2;
	while ($c <= $count) {
		$pdf->SetXY($ml+2.72, $y+0.05);
		$pdf->Cell(0,0,'Siding, Doors, Trim, Porch, Windows, Doors, Soffit, Railing, Stairs, Columns, ');
		$y=$y+$yi;
		$pdf->SetXY($ml+2.72, $y+0.05);
		$pdf->Cell(0,0,'Foundation, Garage, Fence, Outbuilding, Other');
		$pdf->line($ml, $y-$yfudge, $ml+$w, $y-$yfudge);
	
		$c=$c+1;
		$y=$y+$yi;
}
		$y=$y+$yi;
		$pdf->line($ml, $y-$yfudge, $ml+$w, $y-$yfudge);
//////////////////////
// Comments
$y=$y+.2;
$pdf->SetXY($ml, $y);
$pdf->Cell(0, 0, ' Comments:');
$y=$y+0.21;
$count=6;
$pdf->SetLineWidth(0.01);
$c=1;

while ($c <= $count) { 
	$pdf->Line($ml, $y, $ml+$w, $y);
	$y=$y+$yi;
	$c=$c+1;
}

$y=$y+0.1;
$pdf->SetXY($ml+1, $y);
$pdf->Cell(0,0,'Initials');
$pdf->SetXY($ml+2.3, $y);
$pdf->Cell(0,0,'Date');
$pdf->SetXY($ml+4, $y);
$pdf->Cell(0,0,'Time');
$y=$y+0.055;
$pdf->Line($ml+1.5, $y, $ml+2.25, $y);
$pdf->Line($ml+2.7, $y, $ml+3.6, $y);
$pdf->Line($ml+4.4, $y, $ml+5.5, $y);

////////////////
$y=10.5;
$pdf->SetFont('Times','B', 9);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'HRC LHM FORM -2 (4/04)');

//End Form
?>