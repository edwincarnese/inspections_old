<?php

require_once'connect.php';

$conditions = array('DT' => 'Damaged Touchup', 'DF' => 'Damaged - Friction', 'N' => 'No Paint', '78' => 'Post \\\'77');
$groupmap = array('T' => 'Woodwork/Molding', 'W' => 'Window,Window Well', 'D' => 'Door', 'ES' => 'Exterior Siding', 'ET' => 'Exterior Trim', 'P' => 'Porches');

$query = "TRUNCATE TABLE room_item_list";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$query = "TRUNCATE TABLE room_item_components";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$file = file('components.txt');

foreach ($file as $line) {
	$fields = explode(',', $line);
	$query = "SELECT itemid FROM room_item_list WHERE itemname='$fields[1]' AND type='$fields[0]'";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	if (mysql_num_rows($result) < 1) {
		$query = "INSERT INTO room_item_list (itemname, type) VALUES ('$fields[1]', '$fields[0]')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		$query = "SELECT LAST_INSERT_ID()";
		$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
	$itemid = mysql_result($result, 0);
	//defaultassessment; only lead-free if N or 78
	if ($fields[4] == 'N' || $fields[4] == '78') {
		$assess = "'Lead-free'";
	} else {
		$assess = 'NULL';
	}
	$fields[4] = $conditions["$fields[4]"]; //rename conditions
	//expand groups
	if ($fields[5]) {
		$groups = explode('/', $fields[5]);
		foreach ($groups as &$group) {
			$group = $groupmap["$group"];
		}
		$fields[5] = implode(',', $groups); //implode back with commas
	}
	//get sid
	$query = "SELECT sid FROM substrates WHERE name='$fields[3]'";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	if (mysql_num_rows($result) == 0) {
		exit("Unknown substrate: $fields[3]");
	}
	$dsid = mysql_result($result, 0);
	
	//ready?
	$query = "INSERT INTO room_item_components (itemid, componentname, dsid, defaultcondition, defaultassessment, grouplist, conformancecomponent, displayorder) VALUES ($itemid, '$fields[2]', $dsid, '$fields[4]', $assess, '$fields[5]', '$fields[6]', $fields[7])";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
}

?>