<?php

$pdf->AddPage('P');
//$pdf->Image('images/fixed/conformance2.png', 0, 0, 8.5, 11.0, 'PNG');
$pdf->SetFont('Arial','',12);

$cuidstart = $cuid;

//$pdf->line(1, 1, 8, 1.3);	
//$pdf->SetXY(0.5, 0.15);
//$pdf->Cell(0,0, "$start , $count, $total");


require'include-form-conformance2-form.php';

$query = "SELECT streetnum, address, suffix, DATE_FORMAT(starttime, '%c/%e/%Y') AS inspdate, DATE_FORMAT(starttime, '%l:%i %p') AS insptime, inspection.iid, unitdesc, number FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);


$iid = $row['iid'];

$pdf->SetFont('Arial','','12');

$pdf->SetXY(1.75, 0.15);
$pdf->Cell(0,0, "Inspection $row[iid]-$row[number]");

//TOP INFO
$pdf->SetXY(1.9, 0.83);
$pdf->Cell(0,0, "$row[streetnum] $row[address] $row[suffix]", 'C');
$pdf->SetXY(5.45, 0.83);
$pdf->Cell(0,0, "$row[unitdesc]", 'C');
$pdf->SetXY(1.15, 1.08);
$pdf->Cell(0,0, "$row[inspdate]");

$pdf->SetXY(6.28, 1.35);
$pdf->Cell(0,0, "$row[insptime]");

$query = "SELECT firstname, lastname, eli, elt FROM inspector INNER JOIN insp_assigned USING (tid) INNER JOIN unit_insp_assigned USING (assig_id) WHERE iid=$iid LIMIT 1";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);
$pdf->SetXY(3.00, 1.08);
$pdf->Cell(0,0, "$row[firstname] $row[lastname]", 'C');
$pdf->SetXY(6.30, 1.08);
if ($row['eli']) {
	$pdf->Cell(0,0, "ELI-$row[eli]", 'C');
} else if ($row['elt']) {
	$pdf->Cell(0,0, "ELT-$row[elt]", 'C');
}


//INTERIOR ROOMS
$query = "SELECT number, name, hazards, hazardareas, otherhazards FROM conformance_rooms WHERE cuid=$cuid ORDER by number LIMIT $start, 34";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$total = mysql_num_rows($result);

/*
if (mysql_num_rows($result) > 14) {
	exit('Too many rooms');
}
//*/
$pdf->SetFont('Arial','',9);

$basex = 0.88; $basey = 4.13; #2.80, 0.32 inc ++

$mody = 0; $check = 'No';
$count=0;
$pdf->SetLineWidth(0.014);
//while (($row = mysql_fetch_assoc($result)) && ($pdf->GetY() < 8.4)) { //don't overflow

while(($row = mysql_fetch_assoc($result)) && ($count<14)) {
	formfix($row);
	$start++; //keep running count for next time
	$count++;
	$pdf->SetXY($basex, $basey + $mody);
	$pdf->Cell(0,0, "$row[number]");
	$pdf->SetXY($basex + (1.27 - 0.82), $basey + $mody);
	$pdf->Cell(0,0, "$row[name]");
	$v=$visual;
//	$pdf->setxy(4.25, 0.2+$mody);
//	$pdf->Cell(0,0, "78 $row[hazards] $visual $hazard kkk");
// The first time through $row[hazards] = 'Yes'.
// From then on $visual =  failed.

	if ($row['hazards'] == 'Yes' || $row['hazardareas']!='') {
		$pdf->SetXY($basex + (2.62 - 0.82), $basey + $mody);
		$check = 'Yes';
		$visual = 'failed';
	} else if ($row['hazards'] == 'No') { //this should be a "Duh!"
		$pdf->SetXY($basex + (3.03 - 0.82), $basey + $mody);
	}
	$pdf->Cell(0,0, "X");
	$areas = explode(',', $row['hazardareas']);
//	$pdf->Cell(0,0, "$visual ciud $cuid 86 $hazard $row['hazards'] $row['hazardareas']");

	foreach ($areas as $area) {
		switch ($area) {
			case 'Ceiling':
				$pdf->Rect(3.40, $basey+$mody-0.10, 0.44, 0.13);
				break;
			case 'Walls':
				$pdf->Rect(3.91, $basey+$mody-0.10, 0.37, 0.13);
				break;
			case 'Trim':
				$pdf->Rect(4.34, $basey+$mody-0.10, 0.32, 0.13);
				break;
			case 'Floor':
				$pdf->Rect(4.72, $basey+$mody-0.10, 0.35, 0.13);
				break;
			case 'Window':
				$pdf->Rect(5.13, $basey+$mody-0.10, 0.51, 0.13);
				break;
			case 'Door':
				$pdf->Rect(5.70, $basey+$mody-0.10, 0.33, 0.13);
				break;
			case 'Cabinet':
				$pdf->Rect(6.09, $basey+$mody-0.10, 0.47, 0.13);
				break;
			case 'Other':
				$pdf->Rect(6.66, $basey+$mody-0.10, 0.35, 0.13);
				break;
		}
	}
	$pdf->SetXY($basex + (7.08 - 0.82), $basey + $mody);
	$pdf->Cell(0.9,0, $row['otherhazards']);
	$mody += 0.165;
}

if ($check == 'Yes') {
	$pdf->SetXY(4.55, 1.64);
} else {
	$pdf->SetXY(5.85, 1.64);
}
$pdf->SetFont('Arial','',10);
$pdf->Cell(0,0, "X");
$check = '';
//rooms in common areas from comprehensive
/*
$unitquery = "SELECT common FROM unit_links WHERE unit = $cuid";
$unitresult = mysql_query($unitquery) or sql_crapout($unitquery.'<br />'.mysql_error());

while ($unitrow = mysql_fetch_row($unitresult)) {
	$roomquery = "SELECT crid FROM comprehensive_rooms WHERE cuid=$unitrow[0] ORDER BY number";
	$roomresult = mysql_query($roomquery) or sql_crapout($roomquery.'<br />'.mysql_error());
	while ($roomrow = mysql_fetch_row($roomresult)) {
		$cuid = $unitrow[0];
		$crid = $roomrow[0];
		$start = $total = 0;
		$WMdone = $Wdone = $WWdone = $Ddone =0; //reset crossouts for each room
		do {
			require 'include-form-comprehensive-interior.php';
		} while ($start < $total);
	}
}
//*/  //Unit 246  common units 266, 267, 268
//COMMON AREA ROOMS
$query = "SELECT common, insptype, unittype FROM unit_links INNER JOIN units ON (unit_links.common=units.cuid) WHERE unit=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$total = $total + mysql_num_rows($result);

while(($row = mysql_fetch_assoc($result)) && ($count<14)) {
	formfix($row);
			$start++; //keep running count for next time
			$count++;
	$localcuid = $row['common'];
	if ($row['unittype'] == "Front Common" || $row['unittype'] == "Interior Common" ) {
		$unittype = "FC";
	} else if ($row['unittype'] == "Rear Common" ) {
		$unittype = "RC";
	}

	if ($row['insptype'] == 'Conformance') { //common area is also conformance
		$query2 = "SELECT number, name, hazards, hazardareas, otherhazards FROM conformance_rooms 	WHERE cuid=$localcuid ORDER BY number";
		$result2 = mysql_query($query2) or sql_crapout($query2.'<br />'.mysql_error());
/*
		if (mysql_num_rows($result2) > 14) {
			exit('Too many rooms');
		}
//*/
		$pdf->SetFont('Arial','',9);

	while(($row = mysql_fetch_assoc($result2)) && ($count<14)) {
			$start++; //keep running count for next time
			$count++;
			$pdf->SetXY($basex - 0.13, $basey + $mody); // -0.13 special for common area
			$pdf->Cell(0,0, "$unittype-$row2[number]");
			$pdf->SetXY($basex + (1.27 - 0.82), $basey + $mody);
			$pdf->Cell(0,0, "$row2[name]");
			if ($row2['hazards'] == 'Yes') {
				$pdf->SetXY($basex + (2.62 - 0.82), $basey + $mody);
				$check = 'Yes';
				$visual = 'failed';
			} else if ($row2['hazards'] == 'No') { //this should be a "Duh!"
				$pdf->SetXY($basex + (3.03 - 0.82), $basey + $mody);
			}
			$pdf->Cell(0,0, "X");
			$areas = explode(',', $row2['hazardareas']);
			foreach ($areas as $area) {
				switch ($area) {
					case 'Ceiling':
						$pdf->Rect(3.40, $basey+$mody-0.10, 0.44, 0.13);
						break;
					case 'Walls':
						$pdf->Rect(3.91, $basey+$mody-0.10, 0.37, 0.13);
						break;
					case 'Trim':
						$pdf->Rect(4.34, $basey+$mody-0.10, 0.32, 0.13);
						break;
					case 'Floor':
						$pdf->Rect(4.72, $basey+$mody-0.10, 0.35, 0.13);
						break;
					case 'Window':
						$pdf->Rect(5.13, $basey+$mody-0.10, 0.51, 0.13);
						break;
					case 'Door':
						$pdf->Rect(5.70, $basey+$mody-0.10, 0.33, 0.13);
						break;
					case 'Cabinet':
						$pdf->Rect(6.09, $basey+$mody-0.10, 0.47, 0.13);
						break;
					case 'Other':
						$pdf->Rect(6.66, $basey+$mody-0.10, 0.35, 0.13);
						break;
				}
			}
			$pdf->SetXY($basex + (7.08 - 0.82), $basey + $mody);
			$pdf->Cell(0.9,0, $row2['otherhazards']);
			$mody += 0.165; //next line
		} //end room loop
	} else if ($row['insptype'] == 'Comprehensive') { //common area is comprehensive... crap!
		//Room info
		$query = "SELECT crid, number, name FROM comprehensive_rooms WHERE cuid=$localcuid ORDER BY number";
		$result2 = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

		if (mysql_num_rows($result2) > 14) {
			exit('Too many rooms');
		}

		$pdf->SetFont('Arial','',9);

		while(($row = mysql_fetch_assoc($result)) && ($count<14)) {
			$start++; //keep running count for next time
			$count++;
			$hazard = 0;
			$crid = $row2['crid'];
			$pdf->SetXY($basex - 0.13, $basey + $mody); // -0.13 special for common area
			$pdf->Cell(0,0, "C$localcuid-$row2[number]");
			$pdf->SetXY($basex + (1.27 - 0.82), $basey + $mody);
			$pdf->Cell(0,0, "$row2[name]");
			//************************* COMPONENTS *************************
			//Ceiling
			$query = "SELECT COUNT(*) FROM comprehensive_components WHERE crid=$crid AND hazardassessment='Hazard' AND conformancecomponent = 'Ceiling'";
			$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			if ($count = mysql_result($compresult, 0)) {
				$pdf->Rect(3.40, $basey+$mody-0.10, 0.44, 0.13);
				$hazard += $count;
			}
			//Walls
			$query = "SELECT COUNT(*) FROM comprehensive_components WHERE crid=$crid AND hazardassessment='Hazard' AND conformancecomponent = 'Wall'";
			$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			if ($count = mysql_result($compresult, 0)) {
				$pdf->Rect(3.91, $basey+$mody-0.10, 0.37, 0.13);
				$hazard += $count;
			}
			//Trim
			$query = "SELECT COUNT(*) FROM comprehensive_components WHERE crid=$crid AND hazardassessment='Hazard' AND conformancecomponent = 'Trim'";
			$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			if ($count = mysql_result($compresult, 0)) {
				$pdf->Rect(4.34, $basey+$mody-0.10, 0.32, 0.13);
				$hazard += $count;
			}
			//Floor
			$query = "SELECT COUNT(*) FROM comprehensive_components WHERE crid=$crid AND hazardassessment='Hazard' AND conformancecomponent = 'Floor'";
			$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			if ($count = mysql_result($compresult, 0)) {
				$pdf->Rect(4.72, $basey+$mody-0.10, 0.35, 0.13);
				$hazard += $count;
			}
			//Window
			$query = "SELECT COUNT(*) FROM comprehensive_components WHERE crid=$crid AND hazardassessment='Hazard' AND conformancecomponent = 'Window'";
			$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			if ($count = mysql_result($compresult, 0)) {
				$pdf->Rect(5.13, $basey+$mody-0.10, 0.51, 0.13);
				$hazard += $count;
			}
			//Door
			$query = "SELECT COUNT(*) FROM comprehensive_components WHERE crid=$crid AND hazardassessment='Hazard' AND  conformancecomponent = 'Door'";
			$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			if ($count = mysql_result($compresult, 0)) {
				$pdf->Rect(5.70, $basey+$mody-0.10, 0.33, 0.13);
				$hazard += $count;
			}
			//Cabinet
			$query = "SELECT COUNT(*) FROM comprehensive_components WHERE crid=$crid AND hazardassessment='Hazard' AND conformancecomponent = 'Cabinet'";
			$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			if ($count = mysql_result($compresult, 0)) {
				$pdf->Rect(6.09, $basey+$mody-0.10, 0.47, 0.13);
				$hazard += $count;
			}
			//Other
			$query = "SELECT COUNT(*) FROM comprehensive_components WHERE crid=$crid AND hazardassessment='Hazard'AND conformancecomponent = 'Other'";
			$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
			if ($count = mysql_result($compresult, 0)) {
				$pdf->Rect(6.66, $basey+$mody-0.10, 0.35, 0.13);
				$hazard += $count;
			}
			if ($hazard) {
				$pdf->SetXY($basex + (2.62 - 0.82), $basey + $mody);
				$check = 'Yes';
				$visual = 'failed';
			} else {
				$pdf->SetXY($basex + (3.03 - 0.82), $basey + $mody);
			}
			$pdf->Cell(0,0, "X");
			$mody += 0.165; //next line
		} //end room loop
	} //end unit if
	if ($check == 'Yes') {
		$frquery = "SELECT unittype from units where cuid=$localcuid";
		$frresult = mysql_query($frquery) or sql_crapout($frquery."<br>".mysql_error());
		if (mysql_result($frresult,0) == "Front Common" || mysql_result($frresult,0) == "Interior Common" ) {
			$frontcheck = 'Yes';
		}
		if (mysql_result($frresult,0) == "Rear Common" ) {
			$rearcheck = 'Yes';
		}
	}
} //end unit loop


//INTERIOR COMMON AREAS CHECKBOX
if ($frontcheck == 'Yes') { //yes
	$pdf->SetXY(4.55, 2.67);
	$visual = 'failed';
} else {
	$pdf->SetXY(5.85, 2.67);
}
$pdf->SetFont('Arial','',10);
$pdf->Cell(0,0, "X"); //FRONT COMMONS

if ($rearcheck == 'Yes') {
	$pdf->SetXY(4.55, 2.94);
	$visual = 'failed';
} else {
	$pdf->SetXY(5.85, 2.94);
}
$pdf->SetFont('Arial','',10);
$pdf->Cell(0,0, "X"); //REAR COMMONS


//EXTERIOR
$basex = 0.88; $basey = 7.53;
#same spots as above
#7.32 7.48
/*
item	x	width
siding	3.40	0.4	
doors	3.83	0.38
trim	4.25	0.32
porch	4.60	0.36
windows	4.99	0.58
doors?	5.60	--
soffit	6.01	0.36
railing	6.40	0.46
stairs	6.89	0.34
columns	7.26	0.52
-------
foundation	3.40	0.78
garage		4.11	0.42
fence outbuilding	4.56	0.91
other	5.60	0.34
*/

$mody = 0; $check = $soil = 'No';

$query = "SELECT insptype FROM units WHERE iid=$iid AND unittype='Exterior'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$type = mysql_result($result, 0);

if ($type == 'Conformance') { //all conformance inspection!
	$sidename='';
	$sidenumber='';
	$query = "SELECT conformance_sides.number, name, hazards, hazardareas, soilhazard, otherhazards FROM conformance_sides INNER JOIN units USING (cuid) WHERE iid=$iid AND unittype='Exterior' ORDER BY number";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while($row = mysql_fetch_assoc($result) ) {
	
		formfix($row);
		$pdf->SetFont('Arial','',12);
		$pdf->SetXY($basex, $basey + $mody);
		$pdf->Cell(0,0, "$row[number]");
		$pdf->SetXY($basex + (1.27 - 0.82), $basey + $mody);
		$pdf->Cell(0,0, "$row[name]");
		if ($row['soilhazard'] == 'Yes'){
		$soil = 'Yes';
		//side names
		if ($row['name']=='Front'){
		$side1name = 'Front';}
		if ($row['name']=='Left'){
		$side2name = 'Left';}
		if ($row['name']=='Rear'){
		$side3name = 'Rear';}
		if ($row['name']=='Right'){
		$side4name = 'Right';}
		//side numbers
		if ($row['number']=='1'){
		$side1number = '1';}
		if ($row['number']=='2'){
		$side2number = '2';}
		if ($row['number']=='3'){
		$side3number = '3';}
		if ($row['number']=='4'){
		$side4number = '4';}
		
		}
		if ($row['hazards'] == 'Yes') {
			$pdf->SetXY($basex + (2.60 - 0.82), $basey + $mody);
			$check = 'Yes';
			$visual = 'failed';
		} else if ($row['hazards'] == 'No') { //this should be a "Duh!"
			$pdf->SetXY($basex + (2.97 - 0.82), $basey + $mody);
		}
		
		if ($row['hazardareas']!='Soil'){
		$pdf->Cell(0,0, "X");}
		$areas = explode(',', $row['hazardareas']);
		foreach ($areas as $area) {
			switch ($area) {
				case 'Siding':
					$pdf->Rect(3.39, $basey+$mody-0.22, 0.40, 0.13);
					break;
				case 'Doors':
					$pdf->Rect(3.83, $basey+$mody-0.22, 0.38, 0.13);
					break;
				case 'Trim':
					$pdf->Rect(4.24, $basey+$mody-0.22, 0.32, 0.13);
					break;
				case 'Porch':
					$pdf->Rect(4.59, $basey+$mody-0.22, 0.36, 0.13);
					break;
				case 'Windows':
					$pdf->Rect(4.99, $basey+$mody-0.22, 0.58, 0.13);
					break;
				case 'Soffit':
					$pdf->Rect(5.99, $basey+$mody-0.22, 0.36, 0.13);
					break;
				case 'Railing':
					$pdf->Rect(6.38, $basey+$mody-0.22, 0.46, 0.13);
					break;
				case 'Stairs':
					$pdf->Rect(6.87, $basey+$mody-0.22, 0.34, 0.13);
					break;
				case 'Columns':
					$pdf->Rect(7.25, $basey+$mody-0.22, 0.52, 0.13);
					break;
				case 'Foundation':
					$pdf->Rect(3.39, $basey+$mody-0.06, 0.68, 0.13);
					break;
				case 'Garage':
					$pdf->Rect(4.10, $basey+$mody-0.06, 0.42, 0.13);
					break;
				case 'Fence':
					$pdf->Rect(4.54, $basey+$mody-0.06, 0.31, 0.13);
					break;
				case 'Outbuilding':
					$pdf->Rect(4.88, $basey+$mody-0.06, 0.64, 0.13);
					break;
				case 'Soil':
					$soil = 'Yes'; //just a checkbox up top
					break;
				case 'Other':
					$pdf->Rect(5.58, $basey+$mody-0.06, 0.34, 0.13);
					break;
			}
		}
		$pdf->SetXY($basex + (5.98 - 0.82), $basey + $mody + 0.02);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(2.0,0, $row['otherhazards']);
		$mody += 0.325;
	}
} else if ($type == 'Comprehensive') { //part comprehensive inspection!
	$sides = array(1 => 'Front', 2 => 'Left', 3 => 'Rear', 4 => 'Right');
	$query = "SELECT cuid FROM units WHERE iid=$iid AND unittype='Exterior'";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$localcuid = mysql_result($result, 0);
	for ($side = 1; $side<=4; $side++) {
		$hazard = 0; //reset for each side
		$pdf->SetFont('Arial','',12);
		$pdf->SetXY($basex, $basey + $mody);
		$pdf->Cell(0,0, "$side");
		$pdf->SetXY($basex + (1.27 - 0.82), $basey + $mody);
		$pdf->Cell(0,0, "$sides[$side]");

		//************************* COMPONENTS *************************
		//Siding
		$query = "SELECT COUNT(*) FROM comprehensive_ext_components WHERE cuid=$localcuid AND side=$side AND hazardassessment='Hazard' AND conformancecomponent = 'Siding'";
		$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		if ($count = mysql_result($compresult, 0)) {
			$pdf->Rect(3.39, $basey+$mody-0.21, 0.40, 0.13);
			$hazard += $count;
		}
		//Doors
		$query = "SELECT COUNT(*) FROM comprehensive_ext_components WHERE cuid=$localcuid AND side=$side AND hazardassessment='Hazard' AND conformancecomponent = 'Door'";
		$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		if ($count = mysql_result($compresult, 0)) {
			$pdf->Rect(3.83, $basey+$mody-0.21, 0.38, 0.13);
			$hazard += $count;
		}
		//Trim
		$query = "SELECT COUNT(*) FROM comprehensive_ext_components WHERE cuid=$localcuid AND side=$side AND hazardassessment='Hazard' AND conformancecomponent = 'Trim'";
		$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		if ($count = mysql_result($compresult, 0)) {
			$pdf->Rect(4.24, $basey+$mody-0.21, 0.32, 0.13);
			$hazard += $count;
		}
		//Porch
		$query = "SELECT COUNT(*) FROM comprehensive_ext_components WHERE cuid=$localcuid AND side=$side AND hazardassessment='Hazard' AND conformancecomponent = 'Porch'";
		$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		if ($count = mysql_result($compresult, 0)) {
			$pdf->Rect(4.59, $basey+$mody-0.21, 0.36, 0.13);
			$hazard += $count;
		}
		//Windows
		$query = "SELECT COUNT(*) FROM comprehensive_ext_components WHERE cuid=$localcuid AND side=$side AND hazardassessment='Hazard' AND conformancecomponent = 'Window'";
		$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		if ($count = mysql_result($compresult, 0)) {
			$pdf->Rect(4.99, $basey+$mody-0.21, 0.58, 0.13);
			$hazard += $count;
		}
		//Soffit
		$query = "SELECT COUNT(*) FROM comprehensive_ext_components WHERE cuid=$localcuid AND side=$side AND hazardassessment='Hazard' AND conformancecomponent = 'Soffit'";
		$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		if ($count = mysql_result($compresult, 0)) {
			$pdf->Rect(5.99, $basey+$mody-0.21, 0.36, 0.13);
			$hazard += $count;
		}
		//Railing
		$query = "SELECT COUNT(*) FROM comprehensive_ext_components WHERE cuid=$localcuid AND side=$side AND hazardassessment='Hazard' AND conformancecomponent = 'Railing'";
		$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		if ($count = mysql_result($compresult, 0)) {
			$pdf->Rect(6.38, $basey+$mody-0.21, 0.46, 0.13);
			$hazard += $count;
		}
		//Stairs
		$query = "SELECT COUNT(*) FROM comprehensive_ext_components WHERE cuid=$localcuid AND side=$side AND hazardassessment='Hazard' AND conformancecomponent = 'Stairs'";
		$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		if ($count = mysql_result($compresult, 0)) {
			$pdf->Rect(6.87, $basey+$mody-0.21, 0.34, 0.13);
			$hazard += $count;
		}
		//Columns
		$query = "SELECT COUNT(*) FROM comprehensive_ext_components WHERE cuid=$localcuid AND side=$side AND hazardassessment='Hazard' AND conformancecomponent = 'Columns'";
		$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		if ($count = mysql_result($compresult, 0)) {
			$pdf->Rect(7.25, $basey+$mody-0.21, 0.52, 0.13);
			$hazard += $count;
		}
		//Foundation
		$query = "SELECT COUNT(*) FROM comprehensive_ext_components WHERE cuid=$localcuid AND side=$side AND hazardassessment='Hazard' AND conformancecomponent = 'Foundation'";
		$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		if ($count = mysql_result($compresult, 0)) {
			$pdf->Rect(3.39, $basey+$mody-0.05, 0.68, 0.13);
			$hazard += $count;
		}
		//Garage
		$query = "SELECT COUNT(*) FROM comprehensive_ext_components WHERE cuid=$localcuid AND side=$side AND hazardassessment='Hazard' AND conformancecomponent = 'Garage'";
		$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		if ($count = mysql_result($compresult, 0)) {
			$pdf->Rect(4.10, $basey+$mody-0.05, 0.42, 0.13);
			$hazard += $count;
		}
		//Fence
		$query = "SELECT COUNT(*) FROM comprehensive_ext_components WHERE cuid=$localcuid AND side=$side AND hazardassessment='Hazard' AND conformancecomponent = 'Fence'";
		$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		if ($count = mysql_result($compresult, 0)) {
			$pdf->Rect(4.54, $basey+$mody-0.05, 0.31, 0.13);
			$hazard += $count;
		}
		//Outbuilding
		$query = "SELECT COUNT(*) FROM comprehensive_ext_components WHERE cuid=$localcuid AND side=$side AND hazardassessment='Hazard' AND conformancecomponent = 'Outbuilding'";
		$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		if ($count = mysql_result($compresult, 0)) {
			$pdf->Rect(4.88, $basey+$mody-0.05, 0.64, 0.13);
			$hazard += $count;
		}
		//Other
		$query = "SELECT COUNT(*) FROM comprehensive_ext_components WHERE cuid=$localcuid AND side=$side AND hazardassessment='Hazard' AND conformancecomponent = 'Other'";
		$compresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		if ($count = mysql_result($compresult, 0)) {
			$pdf->Rect(5.58, $basey+$mody-0.10, 0.35, 0.13);
			$hazard += $count;
		}
		if ($hazard) {
			$pdf->SetXY($basex + (2.60 - 0.82), $basey + $mody);
			$check = 'Yes';
			$visual = 'failed';
		} else {
			$pdf->SetXY($basex + (2.97 - 0.82), $basey + $mody);
		}
		$pdf->Cell(0,0, "X");
		$mody += 0.325;
	} //end side loop
	//************************* SOIL *************************
	$query = "SELECT COUNT(*) FROM comprehensive_sides WHERE cuid=$localcuid AND hazardassessment='Hazard' UNION SELECT COUNT(*) FROM comprehensive_soil_debris WHERE cuid=$localcuid AND hazardassessment='Hazard' UNION SELECT COUNT(*) FROM comprehensive_soil_object WHERE cuid=$localcuid AND hazardassessment='Hazard'";
	//$query = "select count(*) from conformance_sides where cuid=$localcuid AND soilhazard='Yes'" ;
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

	while ($row = mysql_fetch_row($result)) {
		if ($row[0]) {
			$soil = 'Yes';
			$visual = 'failed';

		}
	}
} 

;//end comprehensive unit

if ($soil == "No") {
	$query = "SELECT cuid FROM units WHERE iid=$iid AND unittype='Exterior'";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$localcuid = mysql_result($result, 0);
	$soilcommentsquery = "SELECT * FROM unit_comments INNER JOIN unit_comments_assigned USING (comid) WHERE cuid=$localcuid and title LIKE '%soil%'";
	$soilcommentsresult = mysql_query($soilcommentsquery) or sql_crapout($soilcommentsquery."<br>".mysql_error());
	if (mysql_num_rows($soilcommentsresult) > 0) {
		$soil = "Deferred";
	}
}	
if ($check == 'Yes') {
	$pdf->SetXY(4.55, 1.90);
} else {
	$pdf->SetXY(5.85, 1.90);
}
$pdf->SetFont('Arial','',10);
$pdf->Cell(0,0, "X");

if ($soil == "Deferred"){
	$pdf->SetXY(2.50, 2.41);
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(0,0, 'Deferred');
} else if ($soil == 'Yes') {
	$pdf->SetXY(4.55, 2.41);
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(0,0, 'X');
	if ($side1number!=''){			
	$pdf->SetXY(2.09, 2.41);
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(0,0, $side1number.' - '.$side1name);}
	if ($side2number!=''){
	$pdf->SetXY(2.67, 2.41);
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(0,0, $side2number.' - '.$side2name);}
	if ($side3number!=''){
	$pdf->SetXY(3.16, 2.41);
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(0,0, $side3number.' - '.$side3name);}
	if ($side4number!=''){
	$pdf->SetXY(3.71, 2.41);
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(0,0, $side4number.' - '.$side4name);}
	
} else {
	$pdf->SetXY(5.85, 2.41);	
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(0,0, 'X');
}

//print $sidename.' - '.$sidenumber;
//DUST HAZARDS

$query = "SELECT COUNT(*) FROM conformance_rooms INNER JOIN conformance_wipes USING (crid) WHERE cuid=$cuid AND hazardassessment='Hazard'"; //iid needed in case of crid in comprehensive
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$hazard = mysql_result($result, 0);

$query = "SELECT COUNT(*) FROM conformance_rooms LEFT JOIN conformance_wipes USING (crid) WHERE iid=$iid AND (hazardassessment IS NULL OR hazardassessment = '')"; //iid needed in case of crid in comprehensive
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$unfilled = mysql_result($result, 0);

$query = "SELECT COUNT(*) FROM conformance_rooms LEFT JOIN conformance_wipes USING (crid) WHERE iid=$iid"; //iid needed in case of crid in comprehensive
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$wipecount = mysql_result($result, 0);

$pdf->SetFont('Arial','',10);
if ($hazard) { //yes
	$pdf->SetXY(4.55, 2.16);
	$visual = 'failed'; //does this count?
	$pdf->Cell(0,0, "X");
} else if ($unfilled) {
	//do nothing
	$visual = 'none';
} else if ($wipecount == 0) {
	//do nothing
	$pdf->SetXY(2.50, 2.16);
	$pdf->Cell(0,0, "Deferred");
	$visual = 'none';
} else {
	$pdf->SetXY(5.85, 2.16);
	$pdf->Cell(0,0, "X");
}

//VISUAL INSPECTION
if ($visual == 'failed') {//marked if failed
	$pdf->SetXY(4.34, 1.35);
} else if ($visual == 'none') {
} else {
	$pdf->SetXY(2.59, 1.35);
}
$pdf->SetFont('Arial','',10);
$pdf->Cell(0,0, 'X');


//comments
$pdf->SetXY(0.65, 8.93);
$lh = 0.16;
$query = "SELECT comment FROM conformance_comments WHERE cuid=$cuid AND type='unit'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while ($row = mysql_fetch_row($result)) {
	formfix($row);
	$output_comments[] = $row[0];
}


$query = "SELECT comment from unit_comments LEFT JOIN unit_comments_assigned USING (comid) WHERE unit_comments_assigned.cuid=$cuid AND unit_comments.title LIKE '%soil%'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while ($row = mysql_fetch_row($result)) {
	formfix($row);
	$output_comments[] = $row[0];
}
if (is_array($output_comments)) {
	$output_comments = implode('  ', $output_comments);
}

$pdf->MultiCell(7.25, $lh, $output_comments);


//date/time at bottom
$query = "SELECT DATE_FORMAT(starttime, '%c/%e/%Y') AS startdate, DATE_FORMAT(starttime, '%l:%i %p') AS starttime FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_row($result);
$pdf->SetXY(3.35, 10.29);
$pdf->Cell(0,0, $row[0]);
$pdf->SetXY(5.09, 10.29);
$pdf->Cell(0,0, $row[1]);


//PAGE NUMBERS
$y=10.6;
$pdf->SetFont('Times','','10');
$pdf->SetXY(7.2, $y);
$pdf->Cell(0,0, 'Page     of');
$pdf->SetXY(7.525, $y);
$pdf->Cell(0, 0, $pdf->PageNo());
$pdf->SetXY(7.85, $y);
$pdf->Cell(0,0, '{totalpages}');
$pdf->SetXY(6, $y+0.2);
$pdf->Cell(0,0, $cuid);
/*$pdf->SetXY(6.5, $y+0.2);
$pdf->Cell(0,0, $count);
$pdf->SetXY(7, $y+0.2);
$pdf->Cell(0,0, $total);
//*/


?>