<?php

require_once'session.php';
require_once'connect.php';

//MySQL will automatically strip out -0, etc.
$iid = $_POST['iid'] or $iid = $_GET['iid'] or $iid = 0;

/*
print_r($_POST);
exit();
*/

$unitnumber = 0; //now important!

$numunits = $_POST['numunits'];
unset($_POST['numunits']);

//shortcut - all fields are text
foreach ($_POST as $field => $value) {
	$fieldlist[] = $field;
	$valuelist[] = htmlspecialchars($value);
}

$query = "INSERT INTO inspection (iid, bid, ".implode(',', $fieldlist).") VALUES ($iid, 0, '".implode("','", $valuelist)."')";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());


$query = "INSERT INTO units (iid, number, unitdesc, unittype, insptype) VALUES ($iid, $unitnumber, 'Exterior', 'Exterior', '$_POST[type]')";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$unitnumber++;

$query = "SELECT LAST_INSERT_ID()";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$cuid = mysql_result($result, 0);

if ($_POST['type'] == 'Comprehensive') {
	$query = "INSERT INTO comprehensive_ext (cuid) VALUES ($cuid)";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

// here in case we decide to do it here
/*	
	for ($x=1; $x<=4; $x++) { //prefill soil samples
		$query = "INSERT INTO comprehensive_sides (cuid, object, side) VALUES ($cuid, 'Side', $x)";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
*/
}

//Now with a default common area!
$query = "INSERT INTO units (iid, number, unitdesc, unittype, insptype) VALUES ($iid, $unitnumber, 'Common Area', 'Interior Common', '$_POST[type]')";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$unitnumber++;

for($x=1; $x<=$numunits; $x++) {
	$unitdesc = "Unit $x";
	$query = "INSERT INTO units (iid, number, unitdesc, insptype) VALUES ($iid, $unitnumber, '$unitdesc', '$_POST[type]')";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$unitnumber++;
}

header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-view.php?iid=$iid");
?>