<?php 

require_once'session.php';
require_once'connect.php';
require_once'helper.php';


$bid = getPostIsset('bid');


$query = "SELECT * FROM building WHERE bid=$bid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (!($row = mysql_fetch_assoc($result))) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/building-list.php");
	exit();
}



$query = "SELECT * FROM owners WHERE bid=$bid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (!mysql_num_rows($result)) {
	print "<script language='javascript'>\n";
	print "alert('You can not schedule an inspection for a building without an owner or other associated person.');\n";
	print "history.back();";
	print "</script>";
	exit();
}


$title = "Schedule Inspection";
require_once'header.php';
?>

<form action="inspection-schedule-save.php" method="post">
<input type="hidden" name="bid" value="<?php print $bid;?>" />
<table>
<tr><td>Units being inspected:</td><td><input type="text" name="numunits" maxlength="3" size="4" value="<?php print $row['numunits']; ?>" /></td></tr>
<tr><td style="vertical-align: top">Inspection type:</td><td><input type="radio" name="type" value="Conformance" checked="checked" /> Conformance  <input type="radio" name="type" value="Comprehensive" /> Comprehensive<input type="radio" name="type" value="Limited" /> Limited<br />
<input type="radio" name="type" value="Conformance - Clearance" /> Conformance - Clearance  <input type="radio" name="type" value="Comprehensive - Clearance" /> Comprehensive - Clearance<br />
<? if ($_SESSION['sid']==1){ ?>
<input type="radio" name="type" value="Conformance - Copy" /> Conformance - Copy  <input type="radio" name="type" value="Comprehensive - Copy" /> Comprehensive - Copy<br />
<? } ?>

<input type="radio" name="type" value="Pre-Inspection Walkthrough" /> Pre-Inspection Walkthrough  <input type="radio" name="type" value="Post-Inspection Walkthrough" /> Post-Inspection Walkthrough<br />
</tr>
<tr><td style="vertical-align:top">Reason:</td><td>
<!-- Reasons -->
<table>
<tr>
<td><input type="radio" name="reason" value="Independent Clearance Inspection" /> Independent Clearance Inspection</td>
<td><input type="radio" name="reason" value="Owner Request" /> Owner Request</td>
</tr>
<tr>
<td><input type="radio" name="reason" value="Code Enforcement" /> Code Enforcement</td>
<td><input type="radio" name="reason" value="Visual Inspection" /> Visual Inspection</td>
</tr>
<tr>
<td><input type="radio" name="reason" value="Private Client" /> Private Client</td>
<td><input type="radio" name="reason" value="Private Client - Property Transfer" /> Private Client - Property Transfer</td>
</tr>
<tr>
<td><input type="radio" name="reason" value="Tenant Complaint" /> Tenant Complaint</td>
<td><input type="radio" name="reason" value="Presumptive Compliance" /> Presumptive Compliance</td>
</tr>
<tr>
<td><input type="radio" name="reason" value="School" /> School</td>
<td><input type="radio" name="reason" value="Licensed Child Care Facility" /> Licensed Child Care Facility</td>
</tr>
<tr>
<td><input type="radio" name="reason" value="Department of Health Initiated" /> Department of Health Initiated</td>
<td><input type="radio" name="reason" value="Dept. of Human Services Initiated" /> Dept. of Human Services Initiated</td>
</tr>
<tr>
<td colspan="2"><input type="radio" name="reason" value="Investigation of Improper Lead Hazard Reduction/Illegal Abatement" /> Investigation of Improper Lead Hazard Reduction/Illegal Abatement</td>
</tr>
<tr>
<td colspan="2"><input type="radio" name="reason" value="Other" /> Other: <input type="text" name="otherreason" size="60" maxlength="60" /></td>
</tr>
</table>
</td></tr>
<tr><td colspan="2">Is Building Owner Occupied?
<?php
$values = array('Yes','No');
foreach ($values as $value) {
	if (isset($row['owneroccupied']) == $value) {
		print "<input type=\"radio\" name=\"owneroccupied\" value=\"$value\" checked=\"checked\" />$value&nbsp;";
	} else {
		print "<input type=\"radio\" name=\"owneroccupied\" value=\"$value\" />$value&nbsp;";
	}
}

$query = "SELECT DATE_ADD(CURRENT_DATE(), INTERVAL 14 DAY)";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
list ($year, $month, $day) = explode('-', mysql_result($result, 0));
?>
</td></tr>
<tr><td>Scheduled Date:</td><td><input type="text" name="month" value="<?php print $month;?>" maxlength="2" size="3" /> / <input type="text" name="day" value="<?php print $day;?>" maxlength="2" size="3" /> / <input type="text" name="year" value="<?php print $year;?>" maxlength="4" size="5" /></td></tr>
<tr><td>Scheduled Start:</td><td><input type="text" name="starthour" maxlength="2" size="3" value="8"/>:<input type="text" name="startmin" maxlength="2" size="3" value="00"/> <input type="radio" name="startampm" value="AM" checked="checked"/>AM&nbsp;<input type="radio" name="startampm" value="PM" />PM</td></tr>
<tr><td>Scheduled End:</td><td><input type="text" name="endhour" maxlength="2" size="3" value="8"/>:<input type="text" name="endmin" maxlength="2" size="3" value="00"/> <input type="radio" name="endampm" value="AM" />AM&nbsp;<input type="radio" name="endampm" value="PM" checked="checked"/>PM</td></tr>
</table>
<p><input type="submit" value="Save" /> <input type="reset" value="Reset" /></p>
</form>

<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>