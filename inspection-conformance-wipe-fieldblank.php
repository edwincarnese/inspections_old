<?php

require_once'session.php';
require_once'connect.php';

$crid = $_POST['crid'] or $crid = $_GET['crid'] or $crid = 0;
$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

$query = "SELECT iid, number FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
list($iid, $unitnumber) = mysql_fetch_row($result);

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - Field Blank";
require_once'header.php';
?>
<form action="inspection-conformance-wipe-save.php" method="post">
<input type="hidden" name="cuid" value="<?php print $cuid; ?>" />
<input type="hidden" name="crid" value="<?php print $crid; ?>" />
<p>A field blank has not been taken. One must be taken before dust wipes can be added.<br />Take a field blank now?</p>
<p>The field blank sample number will be: <b><?php print "$iid-D0"; ?></b>.</p>
<p><input type="submit" name="submit" value="Yes" /> <input type="submit" name="submit" value="No" /></p>
</form>
<?php
require_once'footer.php';
?>