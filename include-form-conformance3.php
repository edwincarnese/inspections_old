<?php

$pdf->AddPage('P');
//$pdf->Image('images/fixed/conformance3.png', 0, 0, 8.5, 11.0, 'PNG');
$pdf->SetFont('Arial','',12);

require_once'include-form-conformance3-form.php';

$query = "SELECT city, streetnum, address, suffix, DATE_FORMAT(starttime, '%c/%e/%Y') AS inspdate, inspection.iid, unitdesc, number, dustLab FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

$iid = $row['iid'];

$lh = 0.4;
$pdf->SetFont('Arial','','10');

$pdf->SetXY(1.75, 0.15);
$pdf->Cell(0,0, "Inspection $row[iid]-$row[number] $cuid");

$iid = $row['iid'];
$bid = $row['bid'];

// x - 0.03; y - 0.06 from line start

//TOP INFO
$pdf->SetXY(1.44, 0.84);
$pdf->Cell(0,0, "$row[streetnum] $row[address] $row[suffix], $row[city]");
$pdf->SetXY(5.65, 0.84);
$pdf->Cell(0,0, "$row[unitdesc]");
$pdf->SetXY(1.57, 1.48);
$pdf->Cell(0,0, "$row[inspdate]");
$pdf->SetLineWidth(0.014);
$pdf->Rect(2.97, 1.09, 0.64, 0.16);
$pdf->SetXY(4.71, 1.48);
$pdf->Cell(0,0, "$row[dustLab]");

//table order matters, need number from wipes table
$query = "SELECT *, units.number AS unitnumber, conformance_rooms.number AS roomnumber FROM units INNER JOIN conformance_rooms USING (cuid) INNER JOIN conformance_wipes USING (crid) WHERE units.cuid=$cuid ORDER BY conformance_wipes.number";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
/*
if (mysql_num_rows($result) > 7) {
	exit('Too many samples');
}
*/
$basex = 0.625; $basey = 2.46; #2.80, 0.32 inc ++
#0.47 1.55 2.14 3.00 3.68 4.60 5.43 6.35 7.09

$mody = 0; $x=0;
while($row = mysql_fetch_assoc($result) ) {
	formfix($row);
	$pdf->SetXY($basex, $basey + $mody - 0.12);
	$pdf->Cell(0,0, "($row[roomnumber]) $row[name]");
	$pdf->SetXY($basex, $basey + $mody);
	if ($row['side'] == 'Center' || $row['side'] == 'None') {
		$pdf->Cell(0,0, "$row[surface]");
	} else {
			$pdf->Cell(0,0, "$row[surface]- S$row[side]");
	}
	$pdf->SetXY($basex + (1.65), $basey + $mody);
	$pdf->Cell(0,0, "$row[paintchips]");
	$pdf->SetXY($basex + (2.1), $basey + $mody);
	$pdf->Cell(0,0, "$row[areawidth]\"x$row[arealength]\"");
	$pdf->SetXY($basex + (2.85), $basey + $mody);
	$pdf->Cell(0,0, "$iid-$row[unitnumber]D$row[number]");
	$pdf->SetXY($basex + (3.68), $basey + $mody);
	$pdf->Cell(0,0, "$row[results]");
	$pdf->SetXY($basex + (4.60), $basey + $mody);
	$pdf->Cell(0,0, sprintf("%.2f", round("$row[conversion]", 2)));
	$pdf->SetXY($basex + (5.43), $basey + $mody);
	$pdf->Cell(0,0, $row['squarefootresult']);
	$pdf->SetXY($basex + (6.35), $basey + $mody);
	$pdf->Cell(0,0, "$row[spottest]");
	$pdf->SetXY($basex + (6.5), $basey + $mody);
	$pdf->Cell(0,0, "$row[hazardassessment]");
	$mody += 0.32; $x++;
}

//common areas
$query = "SELECT common, insptype FROM unit_links INNER JOIN units ON (unit_links.common=units.cuid) WHERE unit=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while ($row = mysql_fetch_assoc($result)) {
	$localcuid = $row['common'];

	//table order matters, need number from wipes table
	if ($row['insptype'] == 'Conformance') {
		$query = "SELECT *, units.number AS unitnumber, conformance_rooms.number AS roomnumber FROM units INNER JOIN conformance_rooms USING (cuid) INNER JOIN conformance_wipes USING (crid) WHERE units.cuid=$localcuid ORDER BY conformance_wipes.number";
	} else if ($row['insptype'] == 'Comprehensive') {
		$query = "SELECT *, units.number AS unitnumber, comprehensive_rooms.number AS roomnumber FROM units INNER JOIN comprehensive_rooms USING (cuid) INNER JOIN comprehensive_wipes USING (crid) WHERE units.cuid=$localcuid ORDER BY comprehensive_wipes.number";
	}
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

	if (mysql_num_rows($result) > 7) {
		exit('Too many samples');
	}

	while($row = mysql_fetch_assoc($result) ) {
		formfix($row);
		$pdf->SetXY($basex, $basey + $mody);
		$pdf->Cell(0,0, "Common $row[surface]");
		$pdf->SetXY($basex + (1.55 - 0.47), $basey + $mody);
		$pdf->Cell(0,0, "$row[paintchips]");
		$pdf->SetXY($basex + (2.14 - 0.47), $basey + $mody);
		$pdf->Cell(0,0, "$row[arealength]\"x$row[areawidth]\"");
		$pdf->SetXY($basex + (3.00), $basey + $mody);
		$pdf->Cell(0,0, "$iid-$row[unitnumber]D$row[number]");
		$pdf->SetXY($basex + (3.68 - 0.47), $basey + $mody);
		$pdf->Cell(0,0, "$row[results]");
		$pdf->SetXY($basex + (4.60 - 0.47), $basey + $mody);
		$pdf->Cell(0,0, sprintf("%.2f", round("$row[conversion]", 2)));
		$pdf->SetXY($basex + (5.43 - 0.47), $basey + $mody);
		$pdf->Cell(0,0, $row['squarefootresult']);
		$pdf->SetXY($basex + (6.35 - 0.47), $basey + $mody);
		$pdf->Cell(0,0, "$row[spottest]");
		$pdf->SetXY($basex + (7.09 - 0.47), $basey + $mody);
		$pdf->Cell(0,0, "$row[hazardassessment]");
		$mody += 0.32; $x++;
	}
}

while ($x < 7) {
	$mody += 0.32; $x++;
}

//field blank
$query = "SELECT * FROM comprehensive_wipes WHERE iid=$iid AND number = 0 UNION SELECT * FROM  conformance_wipes WHERE iid=$iid AND number = 0";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while($row = mysql_fetch_assoc($result) ) {
	formfix($row);
//	$pdf->SetXY($basex, $basey + $mody);
//	$pdf->Cell(0,0, "$row[surface]");
	$pdf->SetXY($basex + (1.55), $basey + $mody);
	$pdf->Cell(0,0, "$row[paintchips]");
	$pdf->SetXY($basex + (2.14), $basey + $mody);
//	$pdf->Cell(0,0, "$row[arealength]\"x$row[areawidth]\"");
	$pdf->SetXY($basex + (3.00), $basey + $mody);
	$pdf->Cell(0,0, "$iid-*D$row[number]");
	$pdf->SetXY($basex + (3.68), $basey + $mody);
	$pdf->Cell(0,0, "$row[results]");
	$pdf->SetXY($basex + (4.60), $basey + $mody);
//	$pdf->Cell(0,0, round("$row[conversion]", 2));
	$pdf->SetXY($basex + (5.43), $basey + $mody);
	$pdf->Cell(0,0, $row['squarefootresult']);
	$pdf->SetXY($basex + (6.35), $basey + $mody);
	$pdf->Cell(0,0, "$row[spottest]");
	$pdf->SetXY($basex + (6.5), $basey + $mody);
	$pdf->Cell(0,0, "$row[hazardassessment]");
	$mody += 0.32; $x++;
}

//comments
$pdf->SetXY(0.63, 5.94);
$lh = 0.16;
$query = "SELECT comment FROM conformance_comments WHERE cuid=$cuid AND type='dust'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while ($row = mysql_fetch_row($result)) {
	formfix($row);
	$output[] = $row[0];
}
$query = "SELECT comment from unit_comments LEFT JOIN unit_comments_assigned USING (comid) WHERE unit_comments_assigned.cuid=$cuid AND unit_comments.title NOT LIKE '%soil%'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while ($row = mysql_fetch_row($result)) {
	formfix($row);
	$output[] = $row[0];
}
if ($output) {
	$output = implode('  ', $output);
}
$pdf->MultiCell(7.25, $lh, $output);

//date/time at bottom
$query = "SELECT DATE_FORMAT(starttime, '%c/%e/%Y') AS startdate, DATE_FORMAT(starttime, '%l:%i %p') AS starttime FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_row($result);
$pdf->SetXY(4.08, 9.92);
$pdf->Cell(0,0, $row[0]);
$pdf->SetXY(6.34, 9.92);
$pdf->Cell(0,0, $row[1]);

//PAGE NUMBERS
$y=10.6;
$pdf->SetFont('Times','','10');
$pdf->SetXY(7.2, $y);
$pdf->Cell(0,0, 'Page     of');
$pdf->SetXY(7.525, $y);
$pdf->Cell(0, 0, $pdf->PageNo());
$pdf->SetXY(7.85, $y);
$pdf->Cell(0,0, '{totalpages}');

?>