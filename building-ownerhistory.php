<?php 
$title = "Building Owner History";

require_once'session.php';
require_once'connect.php';

$bid = $_POST['bid'] or $bid = $_GET['bid'];

$bid += 0; // (force numbers)

$query = "SELECT streetnum, address, suffix, address2, city, state, zip FROM building WHERE bid=$bid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/building-list.php");
	exit();
}

$row = mysql_fetch_assoc($result);
/*
print "<!--\n";
print_r($row);
print "\n-->";
*/

require_once'header.php';

print "<p>Owner History for:<br />";
print "$row[streetnum] $row[address] $row[suffix]<br />\n";
if ($row['address2']) {print "$row[address2]<br />\n";}
print "$row[city], $row[state] $row[zip]<br />";

$query = "SELECT client.cid, firstname, lastname, startdate AS sd, enddate AS ed, DATE_FORMAT(startdate, '%c/%e/%Y') AS startdate, DATE_FORMAT(enddate, '%c/%e/%Y') AS enddate FROM owners INNER JOIN client USING (cid) WHERE bid=$bid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

print "<table><tr><th>Owner</th><th>Start Date</th><th>End Date</th></tr>";
while ($row = mysql_fetch_assoc($result) ) {
	print "<tr><td><a href=\"client-view.php?cid=$row[cid]\">$row[firstname] $row[lastname]</a></td><br />";
	if ($row['sd'] > '1000') {
		print "<td>$row[startdate]</td>";
	} else {
		print "<td><i>None</i></td>";
	}
	if ($row['ed'] < '9000') {
		print "<td>$row[enddate]</td>";
	} else {
		print "<td><i>None</i></td>";
	}
	print "</tr>\n";
}
print "</table>";

?>
<p><a href="building-view.php?bid=<?php print $bid; ?>">View Building</a></p>

<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>