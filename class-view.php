<?php 

require_once'session.php';
require_once'connect.php';

$clid = $_POST['clid'] or $clid = $_GET['clid'] or $clid = 0;

$query = "SELECT * FROM class INNER JOIN class_types USING (ctid) WHERE clid=$clid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/class-view-schedule.php");
	exit();
}

$row = mysql_fetch_assoc($result);
$clid = $row['clid'];

$title = "View Class";
require_once'header.php';
?>
<p>Class type: <?php print $row['name']; ?><br />Hours: <?php print $row['hours']; ?><br />Cost: <?php print $row['cost']; ?></p>
<?php
$query = "SELECT *, DATE_FORMAT(classtime, '%c/%e/%Y %l:%i %p') AS classtime FROM class_parts WHERE clid=$clid ORDER BY part";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while ($row = mysql_fetch_assoc($result)) {
	$query = "SELECT COUNT(*) FROM class_students WHERE cpid=$row[cpid]";
	$result2 = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$enrolled = mysql_result($result2, 0);
	print "<p>Part $row[part]: $row[location], $row[classtime], $row[length] hours.<br />Min: $row[minstudents] Rec: $row[recstudents] Max: $row[maxstudents] Enrolled: $enrolled</p>";
}

?>
<p><a href="class-view-schedule.php">View Class Schedule</a></p>

<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>