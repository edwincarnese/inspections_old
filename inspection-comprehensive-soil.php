<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

$query = "SELECT iid, number FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}
list($iid, $unitnumber) = mysql_fetch_row($result);

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - Exterior - Soil";
require_once'header.php';
?>
<p>Side Samples:&nbsp;&nbsp;&nbsp;<a href="inspection-comprehensive-soil-sides.php?cuid=<?php print $cuid; ?>">Edit</a></p>
<table class="info">
<tr><th>Side</th><th>Inaccessible</th><th>Exposed</th><th>Ground Cover</th><th>Distance</th><th>Depth</th><th>Lab Number</th><th>Hazard Assessment</th></tr>
<?php
$query = "SELECT * FROM comprehensive_sides WHERE cuid=$cuid ORDER BY side";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) {
	for ($x=1; $x<=4; $x++) { //prefill soil side samples
		$query = "INSERT INTO comprehensive_sides (cuid, side) VALUES ($cuid, $x)";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
	//get new default data
	$query = "SELECT * FROM comprehensive_sides WHERE cuid=$cuid ORDER BY side";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
}

while ($row = mysql_fetch_assoc($result)) {
	print "<tr><td>$row[side]</td>";
	print "<td>$row[inaccessible]</td>";
	if ($row['inaccessible'] == 'No') {
		if ($row['cover'] != 'Pavement') {
			print '<td>Yes</td>';
			print "<td>$row[cover]</td>";
			print "<td>$row[distance]</td><td>$row[depth]</td>";
			print "<td>$iid<b>SS</b>$row[side]</td>";
		} else {
			print '<td>No</td>';
			print "<td>$row[cover]</td>";
			print "<td> </td><td> </td><td> </td>";
		}
	} else {
		print "<td> </td><td> </td><td> </td><td> </td><td> </td>";
	}
	print "<td>$row[hazardassessment]</td></tr>";
}
?>
</table>

<p>Soil Around Other Exterior Objects&nbsp;&nbsp;&nbsp;<a href="inspection-comprehensive-soil-object.php?cuid=<?php print $cuid; ?>">Add</a></p>

<?php
$query = "SELECT * FROM comprehensive_soil_object WHERE cuid=$cuid ORDER BY side";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result)) {
?>
<table class="info">
<tr><th>Object</th><th>Side</th><th>Distance</th><th>Depth</th><th>Exposed</th><th>Ground Cover</th><th>Sample Number</th><th>Hazard Assessment</th></tr>
<?php
	while ($row = mysql_fetch_assoc($result)) {
		print "<tr><td><a href=\"inspection-comprehensive-soil-object.php?csid=$row[csid]\">$row[object]</a></td><td>$row[side]</td><td>$row[distance]</td><td>$row[depth]</td><td>";
		if ($row['cover'] == 'None') {
			print 'Yes';
		} else {
			print 'No';
		}
		print "</td><td>$row[cover]</td><td>$iid"."<b>SO</b>$row[samplenumber]</td><td>$row[hazardassessment]</td></tr>";
	}
?>
</table>
<?php
}
?>

<p>Soil Around Exterior Debris&nbsp;&nbsp;&nbsp;<a href="inspection-comprehensive-soil-debris.php?cuid=<?php print $cuid; ?>">Add</a></p>

<?php
$query = "SELECT * FROM comprehensive_soil_debris WHERE cuid=$cuid ORDER BY side";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result)) {
?>
<table class="info">
<tr><th>Debris</th><th>Side</th><th>Location</th><th>Distance</th><th>Depth</th><th>Sample Number</th><th>Hazard Assessment</th></tr>
<?php
	while ($row = mysql_fetch_assoc($result)) {
		print "<tr><td><a href=\"inspection-comprehensive-soil-debris.php?csid=$row[csid]\">$row[debris]</a></td><td>$row[side]</td><td>$row[location]</td><td>$row[distance]</td><td>$row[depth]</td><td>$iid"."<b>SD</b>$row[samplenumber]</td><td>$row[hazardassessment]</td></tr>";
	}
?>
</table>
<?php
}
?>

<!--
<form action="inspection-comprehensive-soil-savecomments.php" method="post">
<input type="hidden" name="cuid" value="<?php print $cuid; ?>" />
-->
<p>Comments:&nbsp;&nbsp;&nbsp;<a href="inspection-comprehensive-soil-comments.php?cuid=<?php print $cuid; ?>">Edit</a><br />
<!--<textarea name="comment" rows="6" cols="50"> --><?php
$query = "SELECT comment FROM comprehensive_comments WHERE cuid=$cuid AND type='soil'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) ) {
	$comment = mysql_result($result, 0);
}
print $comment;
?><!-- </textarea> --></p>
<!--
<p><input type="submit" name="submit" value="Save" /> <input type="reset" value="Reset" /> <input type="submit" name="submit" value="Cancel" /></p>
</form>
-->
<p><a href="inspection-comprehensive-exterior.php?cuid=<?php print $cuid; ?>">Exterior Main Menu</a></p>

<p><a href="inspection-main.php?iid=<?php print $iid; ?>">Inspection Main Menu</a></p>
<?php
require_once'footer.php';
?>