<?php

require_once'session.php';
require_once'connect.php';

$crid = $_POST['crid'] or $crid = $_GET['crid'] or $crid = 0;

if ($_POST['parts'] <= 1) { //skip step 3 if only one
	$name = $_POST['name'];
	$query = "INSERT INTO comprehensive_components (crid, name) VALUES ($crid, '$name')";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-comprehensive-room-component.php?crid=$crid");
	exit();
}

$query = "SELECT * FROM comprehensive_rooms INNER JOIN units USING (cuid) WHERE crid=$crid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}
$room = mysql_fetch_assoc($result);
$roomname = $room['name'];
$iid = $room['iid'];
$unitnumber = $room['number']; //units is second table and will overwrite room #

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - $roomname - Add Component";
require_once'header.php';
?>
<p>Enter the names of the parts of this component and choose default values. "<?php print $_POST['name']?>" will be added to the front of each part.</p>
<form action="inspection-comprehensive-room-addcustom3.php" method="post">
<input type="hidden" name="crid" value="<?php print $crid; ?>" />
<table class="straightup">
<tr><th>Parts</th><th>Default Side</th><th>Default Substrate</th></tr>
<tr>
<?php $x = 1; /*less rewriting of standard form*/ ?>
<td>
<input type="hidden" name="components[<?php print $x; ?>][itemname]" value="<?php print $_POST['name']; ?>" />
<?php
for($y=1; $y<=$_POST['parts']; $y++) {
?>
<input type="text" name="components[<?php print $x; ?>][parts][]" /><br />
<?php
}
?>
</td>
<td>
<input type="radio" name="components[<?php print $x; ?>][side]" value="0" checked="checked" />None<br />
<input type="radio" name="components[<?php print $x; ?>][side]" value="1" />1<br />
<input type="radio" name="components[<?php print $x; ?>][side]" value="2" />2<br />
<input type="radio" name="components[<?php print $x; ?>][side]" value="3" />3<br />
<input type="radio" name="components[<?php print $x; ?>][side]" value="4" />4<br />
</td>
<td>
<input type="radio" name="components[<?php print $x; ?>][sid]" value="" checked="checked" />None<br />
<?php
	$query = "SELECT sid, name FROM substrates";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_row($result) ) {
		if ($row[0] == $itemrow['dsid']) {
			print "<input type=\"radio\" name=\"components[$x][sid]\" value=\"$row[0]\" checked=\"checked\" />$row[1]<br />\n";
		} else {
			print "<input type=\"radio\" name=\"components[$x][sid]\" value=\"$row[0]\" />$row[1]<br />\n";
		}
	}
?>
</td></tr>
</table>
<p><input type="submit" name="submit" value="Save" /> <input type="reset" value="Reset" /> <input type="submit" name="submit" value="Cancel" /></p>
</form>

<p><a href="inspection-comprehensive-room.php?crid=<?php print $crid; ?>">Room Main Menu</a></p>

<p><a href="inspection-comprehensive-unit.php?cuid=<?php print $room[cuid]; ?>"><?php print $room['unitdesc']; ?> Main Menu</a></p>

<p><a href="inspection-main.php?iid=<?php print $room[iid]; ?>">Inspection Main Menu</a></p>

<?php
require_once'footer.php';
?>