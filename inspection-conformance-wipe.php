<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;
$wid = $_POST['wid'] or $wid = $_GET['wid'] or $wid = 0;

$query = "SELECT iid, number FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
list($iid, $unitnumber) = mysql_fetch_row($result);

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

//check if field blank taken yet
$query = "SELECT * FROM comprehensive_wipes WHERE iid=$iid AND number = 0 UNION SELECT * FROM  conformance_wipes WHERE iid=$iid AND number = 0";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-conformance-wipe-fieldblank.php?cuid=$cuid");
	exit();
}

$title = "$iid-$unitnumber - $address - Sample";

require_once'header.php';

if (isset($_GET['error'])) {
	print $_GET['error'];
}
?>

<form action="inspection-conformance-wipe-save.php" method="post">
<?php

$insptype='conformance';
require_once'include-common-wipe.php';
?>
</form>
<?php
require_once'footer.php';
?>