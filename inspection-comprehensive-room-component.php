<?php

require_once'session.php';
require_once'connect.php';

$ccid = $_POST['ccid'] or $ccid = $_GET['ccid'] or $ccid = 0;
$crid = $_POST['crid'] or $crid = $_GET['crid'] or $crid = 0;

if ($ccid) { //item was selected
	$query = "SELECT * FROM comprehensive_components WHERE ccid=$ccid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	if (mysql_num_rows($result) == 0) {
		header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
		exit();
	}
	$comp = mysql_fetch_assoc($result);
	$crid = $comp['crid'];
}	

//get room info, either way.
$query = "SELECT * FROM comprehensive_rooms INNER JOIN units USING (cuid) WHERE crid=$crid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}
$room = mysql_fetch_assoc($result);
$unitnumber = $room['number']; //units is second table and will overwrite room #
$iid = $room['iid'];
$cuid = $room['cuid']; //gets comments in calibration page if needed

if (!$ccid) { //item was not selected
	$query = "SELECT * FROM comprehensive_components WHERE crid=$crid AND hazardassessment IS NULL ORDER BY comprehensive_components.displayorder, comprehensive_components.ccid LIMIT 1";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	if (mysql_num_rows($result) == 0) {
		if ($crid) {
			header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-comprehensive-room.php?crid=$crid");
		} else {
			header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
		}
		exit();
	}
	$comp = mysql_fetch_assoc($result);
	$ccid = $comp['ccid'];
}

//Separate fetch for paintchips now
$query = "SELECT COUNT(*) FROM paintchips WHERE ccid=$ccid AND comptype='Interior'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_result($result, 0) ) {
	$comp['paintchips'] == 'Yes';
} else {
	$comp['paintchips'] == 'No';
} //good enough for the include file

$type='Interior';
require_once'include-comprehensive-xrfcheck.php';

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$droptitle = true;
$title = "$iid-$unitnumber - $address - $room[name] - Component - $comp[name]";
require_once'header.php';
?>
<p></p>
<form action="inspection-comprehensive-room-component-save.php" method="post">
<?php

// ********************* FORM MOVED TO A COMMON FILE *************************** //

require_once'include-common-component.php';
?>

<p>XRF / unit comments (Use save button above):<br />
<textarea name="comment" rows="6" cols="50"><?php
$query = "SELECT comment FROM comprehensive_comments WHERE cuid=$cuid AND type='xrf'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) ) {
	$comment = mysql_result($result, 0);
}
print $comment;
?></textarea></p>
</form>

<?php
if (0) {
?>
<p><a href="inspection-comprehensive-room.php?crid=<?php print $room[crid]; ?>">Room Main Menu</a></p>

<p><a href="inspection-comprehensive-unit.php?cuid=<?php print $room[cuid]; ?>"><?php print $room['unitdesc']; ?> Main Menu</a></p>

<p><a href="inspection-main.php?iid=<?php print $room[iid]; ?>">Inspection Main Menu</a></p>
<?php
}
?>
<?php
require_once'footer.php';
?>