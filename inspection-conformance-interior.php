<?php
require_once'session.php';
require_once'connect.php';
require_once'helper.php';

$cuid = getPostIsset('cuid');

//Get Inspection Types 




$query = "SELECT iid, starttime, unitdesc, number, unittype FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}

list($iid, $starttime, $unitdesc, $unitnumber, $unittype) = mysql_fetch_row($result);

$query = "SELECT * FROM conformance_rooms WHERE cuid=$cuid ORDER BY number";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	//switch to setup page
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-conformance-interior-setup1.php?cuid=$cuid");
	exit();
}
?>
<script language=javascript>
function openerHref() {
	if (document.update.tenant.value == '') {
		var confBox = confirm ("You did not enter a tenant.  Click cancel to enter a tenant name (or 'Vacant' if the unit is vacant).  Click ok to leave this blank but know that you'll have to fill it in before printing the report.'");
		if (confBox) {
			return true;
			//document.form.submit;
		}
		else {return false;}
	}
}
</script>
<script language=javascript>
function submitform(){
document.form.submit;
}

</script>
<?
$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - $unitdesc Summary";
require_once'header.php';

$query = "SELECT * FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}

$row = mysql_fetch_assoc($result);
$selectedInsptype = $row['insptype']; // ADDED CODE
$insp = $row['insptype'];

?>
 <form action="inspection-unitupdate.php" name="update" method="post">
      <input type="hidden" name="popup" value=''/>
      <input type="hidden" name="cuid" value="<?php print $cuid;?>" />
	  <table frame="box">
  <tr>
   
    
  </tr>
  <tr>
    <td><table>
      <tr>
        <td>Designation:
          <input type="text" name="designation" maxlength="4" size="3" value="<?php print $row['designation']; ?>" />
        </td>
      </tr>
      <tr>
        <td colspan="2" >Unit Description:
          <input type="text" name="unitdesc" value="<?php print $row['unitdesc']; ?>" maxlength="25" /></td>
      </tr>
      <tr>
        <td>Unit Type:
          <table class="box">
                <tr>
                  <?php
							$values = array( 'Interior', 'Interior Common', 'Front Common', 'Rear Common');
							foreach ($values as $value) {
								if ($row['unittype'] == $value) {
									print "<td><input type=\"radio\" name=\"unittype\" value=\"$value\" checked=\"checked\" />$value&nbsp;";
								} else {
									print "<td><input type=\"radio\" name=\"unittype\" value=\"$value\" />$value&nbsp;";
								}
								print "</td>";
								if ($value == "Interior Common") {
									print "</tr><tr>";
								}
								
							}
							?>
                </tr>
            </table></td>
      </tr>
    </table>
        <?
if (!strstr($unittype, 'Common')) {
?>
    </td>
    <td><table>
      <tr>
        <td><table>
          <tr>
            <td colspan="2"><br />
                    <b>The below fields should be skipped for common areas.</b></td>
          </tr>
          <tr>
            <td>Tenant:</td>
            <td><input type="text" name="tenant" value="<?php print $row['tenant']; ?>" maxlength="30" /></td>
          </tr>
          <tr>
            <td>Years resided at unit:</td>
            <td><input type="text" name="tenantyears" value="<?php print $row['tenantyears']; ?>" maxlength="150" /></td>
          </tr>
          <tr>
            <td>Children Under 6:</td>
            <td><input type="text" size="4" maxlength="2" name="under6" value="<?php print $row['under6']; ?>" /></td>
          </tr>
          <tr>
            <td>Owner Occupied:</td>
            <td><?php
									$values = array('Yes','No');
									foreach ($values as $value) {
										if ($row['owneroccupied'] == $value) {
											print "<input type=\"radio\" name=\"owneroccupied\" value=\"$value\" checked=\"checked\" />$value&nbsp;";
										} else {
											print "<input type=\"radio\" name=\"owneroccupied\" value=\"$value\" />$value&nbsp;";
										}
									}
									?>
            </td>
          </tr>
          <tr>
            <td>Section 8:</td>
            <td><?php
									$values = array('Yes','No');
									foreach ($values as $value) {
										if ($row['section8'] == $value) {
											print "<input type=\"radio\" name=\"section8\" value=\"$value\" checked=\"checked\" />$value&nbsp;";
										} else {
											print "<input type=\"radio\" name=\"section8\" value=\"$value\" />$value&nbsp;";
										}
									}
									?>
            </td>
          </tr>
          <tr>
            <td>Public Housing:</td>
            <td><?php
									$values = array('Yes','No');
									foreach ($values as $value) {
										if ($row['publichousing'] == $value) {
											print "<input type=\"radio\" name=\"publichousing\" value=\"$value\" checked=\"checked\" />$value&nbsp;";
										} else {
											print "<input type=\"radio\" name=\"publichousing\" value=\"$value\" />$value&nbsp;";
										}
									}
									?>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td ><input type="submit" name="submit" value="Save" <? //onclick="return openerHref()"?>/>
        <input name="reset" type="reset" value="Reset" /></td>
    <td rowspan="3" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <th width="40%" scope="col"><div align="left">Inspection Type </div></th>
        <th width="20%" scope="col"><div align="left"></div></th>
        <th width="40%" scope="col"><div align="left">Inspectors Assigned </div></th>
      </tr>
      <tr>
        <td><div align="left"><img src="spacer.gif" width="150" height="1"></div></td>
        <td><div align="left"><img src="spacer.gif" width="50" height="1"></div></td>
        <td><div align="left"><img src="spacer.gif" width="170" height="1"></div></td>
      </tr>
      <tr>
        <td>
          <label for="select"></label>
          <div align="left">
            <!-- name and id = inspectiontype -->
            <select name="insptype" size="4" multiple="multiple" id="insptype">
              <option <?php if($selectedInsptype == "Conformance"){echo "selected";}?> >Conformance</option>
              <option <?php if($selectedInsptype == "Comprehensive"){echo "selected";}?> >Comprehensive</option>
              <option>Conformance - Clearance</option>
              <option>Comprehensive - Clearance</option>
              <option>Pre-Inspection Walkthrough</option>
              <option>Post-Inspection Walkthrough</option>
              <option>Limited', 'Annual Inspection</option>
              <option>Clearance Inspection</option>
              <option>Lead Assessment</option>
              <option>Presumptive Compliance</option>
            </select>
            </div>
                </td>
        <td><div align="left"></div></td>
        <td align="center" valign="top">
		  <div align="left">
		    <? 
		//Get Inspectors Currently Assigned to this Inspection
		$IIDquery ="select iid from units where cuid = $cuid";
		$IIDresult = mysql_query($IIDquery) or sql_crapout($IIDquery.'<br />'.mysql_error());
		$iid = mysql_result($IIDresult,0);
		
		$query = "SELECT firstname, lastname,b.tid FROM `inspector` a JOIN `insp_assigned` b ON a.tid = b.tid where  iid=$iid";
		$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
?>
		      <select name="assig_id" size="4" multiple="multiple" id="assig_id">
		        
	            <? 
		
		 $count = 0;
		while ($count <mysql_num_rows($result))
			{
				$row = mysql_fetch_assoc($result);
				print '<option value='.$row['tid'].'>'.$row['firstname'].' '.$row['lastname'].'</option>';
				$count++ ;
			}
		?>
	            </select>
          </div></td>
      </tr>
    </table></form></td>
  </tr>
  <tr>
    <td><?
			$dest = substr(strrchr($_SERVER['REQUEST_URI'], '/'), 1);
			include_once 'inspection-common-links.php'; 
			?>
    </td>
  </tr>
  <?
	} else {
	?>
  <input type="submit" name="submit" value="Save" onclick="return openerHref()"/>
  <input name="reset" type="reset" value="Reset" />
  <td><? 
		$dest = substr(strrchr($_SERVER['REQUEST_URI'], '/'), 1);
		include_once 'inspection-unit-links.php'; 
		?>
  </td>
  </tr>
  <?
	}
	?>
</table>
<p><a href="inspection-conformance-interior-rooms.php?cuid=<?php print $cuid;?>">Room Inspection</a></p>

<p>Unit Comments:&nbsp;&nbsp;&nbsp;<a href="inspection-conformance-unit-comments.php?cuid=<?php print $cuid; ?>">Edit</a><br />
<!--<textarea name="comment" rows="6" cols="50"> --><?php
$comment = ''; //CLEAR!
$query = "SELECT comment FROM inspection_comments WHERE cuid=$cuid AND type='unit' AND inspection_type='Conformance'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) ) {
	$comment = mysql_result($result, 0);
}
print $comment;
?><!-- </textarea> --></p>

<?php
$query = "SELECT comment FROM inspection_comments WHERE cuid=$cuid AND type='prior_wipe' AND inspection_type='Conformance'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) > 0) {
	print "<p>Prior Wipes</p>";
	$prior = mysql_fetch_assoc($result);
	print $prior['comment'];	
}

$query = "SELECT CONCAT(iid, '-', conformance_wipes.number) AS wid, conformance_wipes.number AS wipenumber, conformance_rooms.number, conformance_rooms.name, conformance_wipes.surface, side, arealength, areawidth FROM conformance_wipes INNER JOIN conformance_rooms USING (crid) WHERE cuid=$cuid AND conformance_wipes.iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$query = "SELECT dustlab from units where cuid=$cuid";
$dustlabresult = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$dustlab = mysql_fetch_assoc($dustlabresult);

?>
<p>Dust Wipes:&nbsp;&nbsp;&nbsp;<a href="inspection-conformance-wipe.php?cuid=<?php print $cuid; ?>">Add Wipe</a> | <a href="inspection-conformance-defer.php?cuid=<?php print $cuid;?>&defer=wipes">Defer</a></p>
<p>Choose the lab you are sending the dust wipes to.  If you don't choose, Schneider will be assumed.</p>
<form action="inspection-conformance-dustlab.php" method="POST" name="frmLab">
<input type="hidden" value="<?php print $cuid; ?>" name="cuid" />
<input type="radio" name="lab" onclick="window.location.href='inspection-conformance-dustlab.php?cuid=<?php print $cuid; ?>&lab=Schneider'" value="Schneider Laboratories, Inc." checked="checked" <?
	if (strncasecmp($dustlab['dustlab'], "schneider", 9) == 0) {
		print "checked='checked'";
	}?>>Schneider Laboratories, Inc.
	<? 
	if ($dustlab['dustlab']=="Schneider Laboratories, Inc." || $dustlab['dustlab']=="") {
	?><script language="javascript">
	window.location.href='inspection-conformance-dustlab.php?cuid=<?php print $cuid; ?>&lab=Schneider'
	</script><?
	}
	?>
&nbsp; &nbsp;
<input type="radio" name="lab" onclick="window.location.href='inspection-conformance-dustlab.php?cuid=<?php print $cuid; ?>&lab=IATL'" value="IATL"  <?
	if (strncasecmp($dustlab['dustlab'], "IATL", 9) == 0) {
		print "checked='checked'";
	}?>>IATL 
	
<input type="hidden" name="cmdSave" value="Save">
</form>
<?php
if (mysql_num_rows($result)) {
?>

<table>
<tr><th>Sample #</th><th>Room #</th><th>Room Name</th><th>Side</th><th>Surface</th><th>Area</th><th>Edit</th></tr>
<?php
	while ($row = mysql_fetch_assoc($result)) {
		print "<tr><td>$iid-$unitnumber"."D$row[wipenumber]</td><td>$row[number]</td><td>$row[name]</td><td>$row[side]<td>$row[surface]</td><td>$row[areawidth]&quot; x $row[arealength]&quot;</td><td><a href=\"inspection-conformance-wipe.php?cuid=$cuid&amp;wid=$row[wid]\">Edit</a></td></tr>\n";
	}
?>
</table>

<?php
}
/*
<p>Dust Comments:&nbsp;&nbsp;&nbsp;<a href="inspection-conformance-wipe-comments.php?cuid=<?php print $cuid; ?>">Edit</a><br />
<!--<textarea name="comment" rows="6" cols="50"> --><?php
$comment = ''; //CLEAR!
$query = "SELECT comment FROM conformance_comments WHERE cuid=$cuid AND type='dust'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) ) {
	$comment = mysql_result($result, 0);
}
print $comment;
*/
?><!-- </textarea> --></p>
<!--
<p><a href="inspection-conformance-interior-addroom.php?cuid=<?php print $cuid; ?>">Add new room</a></p>
-->

<p><a href="inspection-conformance-unit-notes.php?cuid=<?php print $cuid; ?>">Private Unit Inspection Notes</a> | 
<?
if (!strstr($unittype, 'Common')) { 
	?>
	<a href="validator-conformance.php?cuid=<?php print $cuid;?>">Wrap up</a></p>
	<?
}

?>
<script language="javascript">
function checkCommons(links) {
	if (links <= 0) {
		alert("There are no commone areas linked to this unit.  Are there common stairs or halls that lead to the unit?");
	}
	window.location = "inspection-view.php?iid=<?php echo $iid; ?>";
}
</script>

<?php
	$query = "SELECT COUNT(*) FROM unit_links WHERE unit=$cuid";
	$result = mysql_query($query) or sql_crapout($query."<br />".mysql_error());
	$links = mysql_result($result, 0);
?>
<p><a href="javascript:checkCommons(<?php echo $links;?>);">Inspection Main Menu</a></p>

<?php
$_SESSION['isInclude'] = true;
require_once'footer.php';
?>