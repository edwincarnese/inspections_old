<?php

$pdf->AddPage('P');

$iid = $row['iid'];

//ADDRESS, TOP
$query = "SELECT inspection.iid, streetnum, address, suffix, city, DATE_FORMAT(starttime, '%c/%e/%Y') AS start, unitdesc, dustlab, number FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

//Inspection #
$pdf->SetFont('Times','B','10');
$pdf->SetXY(1.5, .2);
$pdf->Cell(0,0, "Inspection $row[iid]-$row[number]");
$ml=0.625;

//PAGE NUMBERS
$pdf->SetXY(6.75,$y);
$pdf->Cell(0,0, 'Page '. $pdf->PageNo().' of  '. '{totalpages}');

//Water Inspection Banner
$x=2.5 ;
$y=.2;
$pdf->Rect($ml, $y+0.15, 7.25, 0.3);
$y=$y+.3 ;
$pdf->SetFont('Times','B','14');
$pdf->SetXY($x+.875, $y);
$pdf->Cell(0,0,'Water Inspection');

//PAGE NUMBERS
$pdf->SetFont('Times','','10');
$pdf->SetXY(6.5, $y);
$pdf->Cell(0,0, 'Page _____ of _____');
$pdf->SetXY(6.9, $y);
$pdf->Cell(0, 0, $pdf->PageNo());
$pdf->SetXY(7.4, $y);
$pdf->Cell(0,0, '{totalpages}');

$iid = $row['iid'];

//Unit Specifications
$pdf->SetFont('Times','','10');

$x=$ml;
$y=$y+.5;
$pdf->SetXY($x, $y);
$pdf->Cell($x, 0, 'Floor / Apt. No.');
$pdf->SetXY($x, $y);
$pdf->Rect($x+1, $y+.1, 1.2, 0);
$pdf->SetXY($x+1.13, $y);
$pdf->Cell(0, 0, $row['unitdesc']);
$x=3;
$pdf->SetXY($x, $y);
$pdf->Cell($x, 0, 'Street /City:');
$pdf->SetXY($x, $y);
$pdf->Rect($x+.75, $y+.1, 4.05, 0);
$pdf->SetXY($x+1.3, $y);
$pdf->Cell(0,0, "$row[streetnum] $row[address] $row[suffix], $row[city]");

$x=$ml;
$y=$y+.375;
$pdf->SetXY($x, $y);
$pdf->Cell($x, 0, 'Sampling Date:');
$pdf->SetXY($x, $y);
$pdf->Rect($x+1, $y+.1, 1.2, 0);
$pdf->SetXY($x+1.3, $y);
$pdf->Cell(0, 0, $row['start']);
$x=3;
$pdf->SetXY($x, $y);
$pdf->Cell($x, 0, 'Laboratory Utilized:');
$pdf->SetXY($x, $y);
$pdf->Rect($x+1.25, $y+.1, 3.55, 0);
$pdf->SetXY($x+1.5, $y);
$pdf->Cell(0,0, $row['dustlab']);

//Samples Header
$x=$ml;
$y=$y+.3125;
$w=1;
$pdf->Rect($x, $y, $w, 0.75);
$pdf->SetXY($x+.27, $y+.35);
$pdf->Cell(0, 0, 'Fixture');
$x=$x+$w;
$w=0.625;
$pdf->Rect($x, $y, $w, 0.75);
$pdf->SetXY($x+.13, $y+.35);
$pdf->Cell(0, 0, 'Draw');
$pdf->SetXY($x+.09, $y+.5);
$pdf->Cell(0, 0, '(Flush)');
$x=$x+$w;
$w=0.845;
$x2=$x;
$w2=$w;
$pdf->Rect($x, $y+0.25, $w, 0.5);
$pdf->SetXY($x+.23, $y+.35);
$pdf->Cell(0, 0, 'Brass');
$pdf->SetXY($x+.2, $y+.5);
$pdf->Cell(0, 0, 'Fixture');
$pdf->SetXY($x+.15, $y+.65);
$pdf->Cell(0, 0, 'Y or N');
$x=$x+$w;
$w=0.845;
$pdf->Rect($x, $y+0.25, $w, 0.5);
$pdf->SetXY($x+.26, $y+.35);
$pdf->Cell(0, 0, 'New');
$pdf->SetXY($x+.1, $y+.5);
$pdf->Cell(0, 0, 'Plumbing');
$pdf->SetXY($x+.15, $y+.65);
$pdf->Cell(0, 0, 'Y or N');
$w2=$w2+$w;
$pdf->Rect($x2, $y, $w2, 0.25);
$pdf->SetXY($x2+.26, $y+.1);
$pdf->Cell(0, 0, 'Field Observations');

$x=$x+$w;
$w=1;
$pdf->Rect($x, $y, $w, 0.75);
$pdf->SetXY($x+.4, $y+.35);
$pdf->Cell(0, 0, 'Lab');
$pdf->SetXY($x+.3, $y+.5);
$pdf->Cell(0, 0, 'Sample');
$pdf->SetXY($x+.3, $y+.65);
$pdf->Cell(0, 0, 'Number');
$x=$x+$w;
$w=0.625;
$pdf->Rect($x, $y, $w, 0.75);
$pdf->SetXY($x+.2, $y+.2);
$pdf->Cell(0, 0, 'Lab');
$pdf->SetXY($x+.07, $y+.35);
$pdf->Cell(0, 0, 'Results');
$pdf->SetXY($x+.15, $y+.5);
$pdf->Cell(0, 0, 'AA');
$pdf->SetXY($x+.15, $y+.65);
$pdf->Cell(0, 0, '(ppb)');
$x=$x+$w;
$x2=$x;
$w=0.805;
$w2=$w;
$pdf->Rect($x, $y+0.25, $w, 0.5);
$pdf->SetXY($x+.1, $y+.35);
$pdf->Cell(0, 0, '');
$pdf->SetXY($x+.25, $y+.5);
$pdf->Cell(0, 0, 'Lead');
$pdf->SetXY($x+.25, $y+.65);
$pdf->Cell(0, 0, 'Free');
$x=$x+$w;
$w=0.805;
$w2=$w2+$w;
$pdf->Rect($x, $y+0.25, $w, 0.5);
$pdf->SetXY($x, $y+.35);
$pdf->Cell(0, 0, '(Check One)');
$pdf->SetXY($x+.25, $y+.5);
$pdf->Cell(0, 0, 'Lead');
$pdf->SetXY($x+.25, $y+.65);
$pdf->Cell(0, 0, 'Safe');
$x=$x+$w;
$w=0.805;
$w2=$w2+$w;
$pdf->Rect($x, $y+0.25, $w, 0.5);
$pdf->SetXY($x+.2, $y+.5);
$pdf->Cell(0, 0, 'Hazard');

$pdf->Rect($x2, $y, $w2, 0.25);
$pdf->SetXY($x2+.6, $y+.15);
$pdf->Cell(0, 0, 'Hazard Assessment');

//Sample Table
$x=$ml;
$y=$y+0.9125;
$w=1;
$wt=$w;
$h=1;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$w=0.625;
$wt=$wt+$w;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$w=0.845;
$wt=$wt+$w;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$w=0.845;
$wt=$wt+$w;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$w=1;
$wt=$wt+$w;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$w=0.625;
$wt=$wt+$w;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$w=0.805;
$wt=$wt+$w;
$w2=$w;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$w=0.805;
$wt=$wt+$w;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$w=0.805;
$wt=$wt+$w;
$pdf->Rect($x, $y, $w, $h);

$pdf->Rect($ml, $y+0.25, $wt, 0.25);
$pdf->Rect($ml, $y+0.5, $wt, 0.25);

//Sample Table other Major Drinking Taps

$pdf->SetXY($ml+2.4, $y+1.25);
$pdf->Cell(0, 0, 'OTHER MAJOR DRINKING WATER TAPS');

$x=$ml;
$y=$y+1.5;
$w=1;
$wt=$w;
$h=1;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$w=0.625;
$wt=$wt+$w;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$w=0.845;
$wt=$wt+$w;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$w=0.845;
$wt=$wt+$w;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$w=1;
$wt=$wt+$w;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$w=0.625;
$wt=$wt+$w;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$w=0.805;
$wt=$wt+$w;
$w2=$w;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$w=0.805;
$wt=$wt+$w;
$pdf->Rect($x, $y, $w, $h);
$x=$x+$w;
$w=0.805;
$wt=$wt+$w;
$pdf->Rect($x, $y, $w, $h);

$pdf->Rect($ml, $y+0.25, $wt, 0.25);
$pdf->Rect($ml, $y+0.5, $wt, 0.25);

//SAMPLES
$query = "SELECT * FROM waters WHERE cuid=$cuid"; //iid not needed, water only comp
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) > 4) {
	exit('Too many samples');
}
$basex = $ml; $basey = 2.72; $mody = 0;
#0.265
while ($row = mysql_fetch_assoc($result) ) {
	formfix($row);
	$pdf->SetXY($ml+0.05, $basey + $mody);
	$pdf->Cell(1,0, $row['fixture'], 0, 0, 'L');
	$pdf->SetXY($ml + 1, $basey + $mody);
	$pdf->Cell(0.625,0, $row['drawflush'], 0, 0, 'C');
	$pdf->SetXY($ml + 1.625, $basey + $mody);
	$pdf->Cell(0.845,0, $row['brass'], 0, 0, 'C');
	$pdf->SetXY($ml + 2.47, $basey + $mody);
	$pdf->Cell(0.845,0, $row['newplumbing'], 0, 0, 'C');
	$pdf->SetXY($ml + 3.315, $basey + $mody);
	$pdf->Cell(1.0,0, $row['iid'].'W'.$row['number'], 0, 0, 'C');
	$pdf->SetXY($ml + 4.315, $basey + $mody);
	$pdf->Cell(0.625,0, $row['results'], 0, 0, 'C');
	
	if ($row['hazardassessment'] == 'Lead-free') {
		$pdf->SetXY($ml + 5.25, $basey + $mody);
	} else if ($row['hazardassessment'] == 'Lead-safe') {
		$pdf->SetXY($ml + 6.075, $basey + $mody);
	} else if ($row['hazardassessment'] == 'Hazard') {
		$pdf->SetXY($ml + 6.875, $basey + $mody);
	}
	$pdf->Cell(0,0, 'X');
	$mody += 0.25;
}

//QUESTIONS

$query = "SELECT bottle, bottlesource, infant, infantsource, testing, testingsource, leadpipes, leadpipessource FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

//$pdf->SetFont('Times','','10');
$pdf->SetFont('Times','','12');

$x=$ml;
$y=$y+1.35;
$xyes=$ml+2.25;
$xno=$ml+2.25+0.625;

$pdf->SetXY($x, $y);
$pdf->Cell(0, 0, 'Is bottle water being used?');
$pdf->SetXY($x+2.25, $y);
$pdf->Cell(0, 0, 'Yes       No        Source: _______________________________________');
$pdf->SetXY(4.86, $y);
$pdf->Cell(0,0, $row['bottlesource']);

if ($row['bottle'] == 'Yes') {
	$pdf->Rect(2.9, $y-0.09, 0.31, 0.16);
} else {
	$pdf->Rect(3.45,$y-0.09, 0.26, 0.16);
}

$y=$y+.25;
$pdf->SetXY($x, $y);
$pdf->Cell(0, 0, 'Is tap water being used to');
$pdf->SetXY($x, $y+0.2);
$pdf->Cell(0, 0, 'make infant formula?');
$pdf->SetXY($x+2.25, $y);
$pdf->Cell(0, 0, 'Yes       No        Source: _______________________________________');
$pdf->SetXY(4.86, $y);
$pdf->Cell(0,0, $row['infantsource']);
if ($row['infant'] == 'Yes') {
	$pdf->Rect(2.9, $y-0.09, 0.31, 0.16);
} else {
	$pdf->Rect(3.45,$y-0.09, 0.26, 0.16);
}

$y=$y+.5;
$pdf->SetXY($x, $y);
$pdf->Cell(0, 0, 'Have you ever participated');
$pdf->SetXY($x, $y+0.2);
$pdf->Cell(0, 0, 'in a water testing program?');
$pdf->SetXY($x+2.25, $y);
$pdf->Cell(0, 0, 'Yes       No        Source: _______________________________________');
$pdf->SetXY(4.86, $y);
$pdf->Cell(0,0, $row['testingsource']);
if ($row['testing'] == 'Yes') {
	$pdf->Rect(2.9, $y-0.09, 0.31, 0.16);
} else {
	$pdf->Rect(3.45,$y-0.09, 0.26, 0.16);
}

$y=$y+.5;
$pdf->SetXY($x, $y);
$pdf->Cell(0, 0, 'Lead pipes present in home?');
$pdf->SetXY($x+2.25, $y);
$pdf->Cell(0, 0, 'Yes       No        Source: _______________________________________');
$pdf->SetXY(4.86, $y);
$pdf->Cell(0,0, $row['leadpipessource']);
if ($row['leadpipes'] == 'Yes') {
	$pdf->Rect(2.9, $y-0.09, 0.31, 0.16);
} else {
	$pdf->Rect(3.45,$y-0.09, 0.26, 0.16);
}


//Comment Table
$y=$y+0.375;
$pdf->SetXY($ml, $y+.12);
$pdf->Cell(0, 0, 'Comments:');
$pdf->Rect($ml, $y, $wt, 0.25);
$pdf->Rect($ml, $y+0.25, $wt, 0.25);
$pdf->Rect($ml, $y+0.5, $wt, 0.25);
$pdf->Rect($ml, $y+0.75, $wt, 0.25);
$pdf->Rect($ml, $y+1, $wt, 0.25);
$pdf->Rect($ml, $y+1.25, $wt, 0.25);
$pdf->Rect($ml, $y+1.5, $wt, 0.25);
$pdf->Rect($ml, $y+1.75, $wt, 0.25);


//COMMENTS
$query = "SELECT comment FROM comprehensive_comments WHERE cuid=$cuid AND type='water'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result)) {
#7.45	7.98 0.33 0.165		0.52	8.04	7.52
	formfix($row);
	$lh = 0.25;
	$pdf->SetXY($ml + 0.1, 7.33);
	$pdf->MultiCell(7.05, $lh, mysql_result($result, 0));
}
//date/time at bottom
$query = "SELECT DATE_FORMAT(starttime, '%c/%e/%Y') AS startdate, DATE_FORMAT(starttime, '%l:%i %p') AS starttime FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_row($result);

$pdf->SetXY(5.8, 9.90);
$pdf->Cell(0,0, '___________  / ___________ ');
$pdf->SetXY(6.95, 9.90);
$pdf->Cell(0,0, $row[0]);
$pdf->SetXY(5.8, 10.10);
$pdf->Cell(0,0, '      Initials               Date ');

$pdf->SetFont('Times','','9');
$pdf->SetXY($ml, 10.50);
$pdf->Cell(0,0, 'FORM PBLC-23-3 (9/97) Replaces OEHRA II (4/97)p.3, which may be used.');

?>