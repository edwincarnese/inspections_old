<?php

require_once'session.php';
require_once'connect.php';

/*
print_r($_POST);
exit();
*/

$phones = $_POST['phones'];
$_POST['eltdate'] = "$_POST[eltyear]-$_POST[eltmonth]-$_POST[eltday]";
$_POST['eltstart'] = "$_POST[eltsyear]-$_POST[eltsmonth]-$_POST[eltsday]";
$_POST['elidate'] = "$_POST[eliyear]-$_POST[elimonth]-$_POST[eliday]";
$_POST['elistart'] = "$_POST[elisyear]-$_POST[elismonth]-$_POST[elisday]";
$_POST['startdate'] = "$_POST[startyear]-$_POST[startmonth]-$_POST[startday]";
$_POST['enddate'] = "$_POST[endyear]-$_POST[endmonth]-$_POST[endday]";

unset($_POST['phones']);
unset($_POST['eltyear']);
unset($_POST['eltday']);
unset($_POST['eltmonth']);
unset($_POST['eliyear']);
unset($_POST['eliday']);
unset($_POST['elimonth']);
unset($_POST['eltsyear']);
unset($_POST['eltsday']);
unset($_POST['eltsmonth']);
unset($_POST['elisyear']);
unset($_POST['elisday']);
unset($_POST['elismonth']);
unset($_POST['startyear']);
unset($_POST['startday']);
unset($_POST['startmonth']);
unset($_POST['endyear']);
unset($_POST['endday']);
unset($_POST['endmonth']);

if (!$_POST['eli']) {
	unset($_POST['eli']);
	unset($_POST['elidate']);
}

if (strlen($_POST['enddate']) < 6) {
	$ends = ', enddate = NULL';
	unset($_POST['enddate']);
}

//shortcut - all fields are text
foreach ($_POST as $field => $value) {
	$fieldlist[] = $field;
	$valuelist[] = htmlspecialchars($value);
}

/*
Because htmlspecialchars() is being performed on the data, it is possible that some data may be truncated. This can happen if, for example, the data contains '&', which becomes '&amp;', and the resulting string is greater than the allowed column width. However, the column widths are rather generous, and it only affects a few characters, so as such, it was deemed not worth checking the various lengths of each field to see if they would actually be truncated. It should be noted that if no substitutions are performed, the 'maxlength' attribute on the form elements will prevent an entry from being truncated.
*/


$query = "INSERT INTO inspector (".implode(',', $fieldlist).") VALUES ('".implode("','", $valuelist)."')";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$query = "SELECT LAST_INSERT_ID()";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$tid = mysql_result($result, 0);

foreach ($phones as $phone) {
	$phonenum = "$phone[1]-$phone[2]-$phone[3]";
	$phoneext = $phone[ext];
	$phonetype = $phone[type];
	
	if (strlen($phonenum) == 12) {
		$query = "INSERT INTO inspector_phone (tid, number, ext, type) VALUES ($tid, '$phonenum','$phoneext','$phonetype')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
}

header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspector-list.php");
?>