<?php 

require_once'session.php';
require_once'connect.php';

$cid = isset($_POST['cid']) ? $_POST['cid'] : (isset($_GET['cid']) ? $_GET['cid'] : 0);
$bug = isset($_GET['bug']) ? $_GET['bug'] :"";
$popup = isset($_GET['isPopup']) ? $_GET['isPopup'] :"";

$uid = $sid; // $sid from session.php; session ID, user ID, whatever

if ($cid == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/client-list.php");
	exit();
}
if (!$bug) { 
	$title = "Add an activity";
} else {
	$title = "Report a bug";
}
if (!$popup) {
	require_once'header.php';
} else {
		?>
		<head>
	<link rel="stylesheet" type="text/css" href="style.css" />
	</head>
	<div id="content">
	<?php 
}
	?>

<form action="client-activity-save.php" method="post">
<?php if ($bug){
	print "<input type='hidden' name='bug' value='$HTTP_REFERER'/>";
}
if ($popup){
	print "<input type='hidden' name='popup' value='true'/>";
}
?>

<input type="hidden" name="cid" value="<?php print $cid; ?>" />
<table>
<?php $nextWeek = time() + (7 * 24 * 60 * 60);
if (!$bug) {
	?>
	<tr><td>Date Due:</td><td><input type="text" name="duemonth" value="<?php print date('m', $nextWeek);?>" maxlength="2" size="3" />/<input type="text" name="dueday" value="<?php print date('j', $nextWeek);?>" maxlength="2" size="3" />/<input type="text" name="dueyear" value="<?php print date('Y', $nextWeek);?>" maxlength="4" size="5" /></td></tr>
	
	<tr><td>Assigned to:</td><td><select name="assignedto">
	<!-- <option value="">Choose a user:</option> -->
	<?php
	$query = "SELECT uid, firstname, lastname FROM users ORDER BY lastname";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	
	while($row = mysql_fetch_assoc($result)) {
		if ($uid == $row['uid'] ) {
			print "<option value=\"$row[uid]\" selected=\"selected\">$row[firstname] $row[lastname]</option>\n";
		} else {
			print "<option value=\"$row[uid]\">$row[firstname] $row[lastname]</option>\n";
		}
	}
} else {
	print "<input type='hidden' name='duemonth' value='0'/><input type='hidden' name='dueyear' value='0'/><input type='hidden' name='dueday' value='0'/>";
	print "<input type='hidden' name='assignedto' value='$sid'/>";
}
?>
</select></td></tr>

<tr><td>Description:</td><td><textarea name="description" rows="5" cols="30"></textarea></td></tr>
<tr><td>Display:</td><td><input type="radio" name="display" value="Incomplete" checked="checked" />Only if Incomplete <input type="radio" name="display" value="Always" />Always</td></tr>
</table>
<p><input type="submit" name="submit" value="Save" /> <input type="reset" value="Reset" /> <input type="submit" name="submit" value="Cancel" /></p>
</form>

<?php
if (!$popup){
	print '<p><a href="index.php">Main Menu</a></p>';
	require_once'footer.php';
} else {
	echo "</div>";
}

?>
