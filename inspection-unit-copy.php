<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;
$iid = $_POST['iid'] or $iid = $_GET['iid'] or $iid=0;


//get inspection type information
	$query = "SELECT * FROM inspection WHERE iid=$iid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$row = mysql_fetch_assoc($result);
	$type=$row['type'];
	
//based on inspection type, copy the unit and other pertenant information
//Conformance
if ($type=='Conformance' or $type=='Conformance - Clearance'){
print $type;
//**********************************************************************************//
//******************************* NON-COPY FUNCTIONS *******************************//
//**********************************************************************************//

function slashadd(&$item) {
	$item = addslashes($item);
}
function copy_conformance_rooms($oldcuid, $newcuid) {
	global $iid;
	$query = "SELECT * FROM conformance_rooms WHERE cuid=$oldcuid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		$oldcrid = $row['crid'];
		unset($row['crid']); //get rid of crid off array for implodes
		$query = "INSERT INTO conformance_rooms (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
}

function copy_conformance_wipes($oldcuid, $newcuid) {
	global $iid;
	$query = "SELECT * FROM conformance_wipes INNER JOIN conformance_rooms USING (crid) WHERE cuid=$oldcuid";
	$result = mysql_query($query) or sql_crapout($query."<br />".mysql_error());
	while ($row = mysql_fetch_assoc($result)) {
		$comment .= "Wipe:".$row['iid']."-".$row['number']."-".$row['number']."  Taken: ".$row['timetaken']."  Surface: ".$row['surface']."  Side: ".$row['side']."<br>Paintchips: ".$row['paintchipe']."  Length: ".$row['arealength']."  Width: ".$row['areawidth']."  Results: ".$row['results']."<br>Conversion: ".$row['conversion']."  Square Foot Results: ".$row['squarefootresults']."   Spot test: ".$row['spottest']."  Hazzard Assessment: ".$row['hazzardassessment']."<br /><br />";
	}print 'wipes copied';
}
	global $iid;
	$linkupdates = array();
	$query = "SELECT * FROM units WHERE cuid=$cuid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		$cuid = array_shift($row); //get rid of cuid off array for implodes
		unset ($row['endtime']);
		//unset ($row['starttime']);
		$row['iid'] = $iid;
		//get max number
		$query1='select max(number) as number from units where iid='.$iid;
		$result1 = mysql_query($query1) or sql_crapout($query1.'<br />'.mysql_error());
		$row1 = mysql_fetch_assoc($result1);
		
		$row['number']=$row1['number']+1;
		$row['designation']='';
		$row['unitdesc']='Unit '.$row['number'];
		$query = "INSERT INTO units (".implode(',', array_keys($row)).") VALUES ('".implode("','", $row)."')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		$query = "SELECT LAST_INSERT_ID()"; //get new cuid
		$resultid = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		$newid = mysql_result($resultid, 0);
		copy_conformance_rooms($cuid, $newid);
		copy_conformance_wipes($cuid, $newid);
		$unitmap["$cuid"] = $newid;
		$linkupdates[] = $cuid;
		
}}


//Comprehensive
if ($type=='Comprehensive' or $type=='Comprehensive - Clearance'){
//**********************************************************************************//
//******************************* NON-COPY FUNCTIONS *******************************//
//**********************************************************************************//

function slashadd(&$item) {
	$item = addslashes($item);
}

function copy_comprehensive_rooms($oldcuid, $newcuid) {
	global $iid;
	$query = "SELECT * FROM comprehensive_rooms WHERE cuid=$oldcuid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		$oldcrid = $row['crid'];
		unset($row['crid']); //get rid of crid off array for implodes
		$query = "INSERT INTO comprehensive_rooms (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		$query = "SELECT LAST_INSERT_ID()";
		$resultid = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		$newcrid = mysql_result($resultid, 0);
		print 'room copied';
		copy_comprehensive_components($oldcrid, $newcrid);
	}
}
function copy_comprehensive_components($oldcrid, $newcrid) {
	global $iid;
	$query = "SELECT * FROM comprehensive_components WHERE crid=$oldcrid AND (hazardassessment != 'Lead-free' AND hazardassessment != 'Lead-safe')";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['crid']); //get rid of crid off array for implodes
		$oldccid = $row['ccid'];
		unset($row['ccid']); //get rid of ccid off array for implodes
		$query = "INSERT INTO comprehensive_components (crid, ".implode(',', array_keys($row)).") VALUES ('$newcrid','".implode("','", $row)."')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		print 'components copied';
	}}
	global $iid;
	$linkupdates = array();
	$query = "SELECT * FROM units WHERE cuid=$cuid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		$cuid = array_shift($row); //get rid of cuid off array for implodes
		unset ($row['endtime']);
		unset ($row['starttime']);
		$row['iid'] = $iid;
		//get max number
		$query1='select max(number) as number from units where iid='.$iid;
		$result1 = mysql_query($query1) or sql_crapout($query1.'<br />'.mysql_error());
		$row1 = mysql_fetch_assoc($result1);
		
		$row['number']=$row1['number']+1;
		$row['designation']='';
		$row['unitdesc']='Unit '.$row['number'];
		$query = "INSERT INTO units (".implode(',', array_keys($row)).") VALUES ('".implode("','", $row)."')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		$query = "SELECT LAST_INSERT_ID()"; //get new cuid
		$resultid = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		$newid = mysql_result($resultid, 0);
		print 'unit copied';
		//unit itself copied; just go down the list
		//unit links done below, after all units completed
		//copy_comprehensive_ext($cuid, $newid);
		//copy_comprehensive_ext_components($cuid, $newid);
		//copy_comprehensive_sides($cuid, $newid);
		//copy_comprehensive_soil_debris($cuid, $newid);
		//copy_comprehensive_soil_object($cuid, $newid);
		copy_comprehensive_rooms($cuid, $newid);
		//copy_conformance_comments($cuid, $newid);
		//copy_conformance_sides($cuid, $newid);
		//copy_conformance_rooms($cuid, $newid);
		$unitmap["$cuid"] = $newid;
		$linkupdates[] = $cuid;
}



}	
print '<script language=javascript>';
print 'window.location.href="inspection-view.php?iid='.$iid.'"';
print '</script>';
?>