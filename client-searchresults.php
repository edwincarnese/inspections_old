<?php
require_once'session.php';
$title = "Clients - Search Results";


require_once'connect.php';

$float = $_POST['float'];
unset($_POST['float']);
	
if (strlen($_POST['phone1']) < 3) {
	$_POST['phone1'] .= '%';
}
if (strlen ($_POST['phone2']) < 3) {
	$_POST['phone2'] = '%'.$_POST['phone2'].'%';
}
if (strlen ($_POST['phone3']) < 4) {
	$_POST['phone3'] = '%'.$_POST['phone3'];
}
$_POST['number'] = "$_POST[phone1]-$_POST[phone2]-$_POST[phone3]";
unset($_POST['phone1']);
unset($_POST['phone2']);
unset($_POST['phone3']);
if (strlen($_POST['number']) == 3) {
	unset($_POST['number']);
}

$wheres = array();
foreach ($_POST as $field => $value) {
	if ($value) {
		switch ($field) {
			case 'firstname':
			case 'lastname':
			case 'company':
			case 'city':
			case 'number':
				$value = htmlspecialchars($value);
				$wheres[] = "$field LIKE '%$value%'";
				break;
			case 'state':
			case 'zip':
				$wheres[] = "$field LIKE '%$value%'";
				break;
			default:
				exit("$field not coded");
		}
	}
}
$wherestring = implode(' AND ', $wheres);

if (!$wherestring) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/client-list.php");
	exit();
}

$query = "SELECT DISTINCT client.cid, lastname, firstname, CONCAT(streetnum, ' ', address, ' ', suffix), city, state FROM client LEFT JOIN client_phone USING (cid) WHERE $wherestring ORDER BY lastname, firstname";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 1) {
	$cid = mysql_result($result, 0, 'cid');
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/client-view.php?cid=$cid");
	exit();
}

require_once'header.php';

foreach ($_POST as $field => $value) {
	if ($value) {
		switch ($field) {
			case 'firstname':
				$getstring[] = "firstname=$value";
				break;
			case 'lastname':
				$getstring[] = "lastname=$value";
				break;
			case 'company':
				$getstring[] = "company=$value";
				break;
			case 'city':
				$getstring[] = "city=$value";
				break;
			case 'number':
				$getstring[] = "number=$value";
				break;
			case 'state':
				$getstring[] = "state=$value";
				break;
			case 'zip':
				$getstring[] = "zip=$value";
				break;
			default:
				break;
		}
	}
}

$getstring = implode("&", $getstring);


print "<p><a href=\"client-add.php?$getstring\">Click here to add a client.</a></p>";

print "<p>";
while ($row = mysql_fetch_row($result) ) {
	$cid = array_shift($row);
	print "<a href=\"javascript:refreshIframe($cid);\">".implode(', ', $row)."</a><br />\n";
}
print "</p>";
?>
<script language="JavaScript">
<!--
function calcHeight()
{
  //find the height of the internal page
  var the_height=
    document.getElementById('floater2').contentWindow.
      document.body.scrollHeight;

  //change the height of the iframe
  document.getElementById('floater2').height=
      the_height;
}
//-->
</script>

<?php

$showFloat = "<iframe src='client-float.php?cid=0' scrolling='no' height='277px' width='368px' frameborder='0' marginwidth='1' marginheight='1' id='floater' name='floater'></iframe>";
$showFloat2 = "<iframe src='' scrolling='no' onLoad='calcHeight();' height='1' width='368px' frameborder='0' marginwidth='1' marginheight='1' id='floater2' name='floater2'></iframe>";
require_once'footer.php';
print "<script language='javascript'>\n";
print "function fill_form() {\n";
foreach ($_POST as $key=>$blah) {
	print "frames['floater'].document.searchForm.$key.value='$blah';\n";
}
print "}</script>\n";
?>
<body onLoad="javascript:fill_form();"/>
<script language="javascript">
function refreshIframe(cidvar) {
	frames['floater2'].location.href = 'client-float.php?cid='+cidvar;
}

</script>