<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;
  
if ($_POST['submit'] == 'Save') {
	foreach ($_POST['sides'] as $csid => $data) {
		if ($data['inaccessible'] != 'No') {
			$data['cover'] = 'None';
			$data['distance'] = $data['depth'] = '';
			//$data['hazardassessment']='Lead-safe';
		}
		if ($data['cover'] == 'Pavement' OR $data['cover'] == 'Mulch' OR $data['cover'] == 'Grass' ) {
			$data['distance'] = $data['depth'] = '';
			$exposed='No';
		}
		$setstring = array();
		foreach ($data as $field => $value) {
                        $passValue = mysql_real_escape_string($value); // added code to accept single quote
			$setstring[] = "$field='".htmlspecialchars($passValue)."'"; 
                        //echo implode(",",$setstring);
		}   
		$query = "UPDATE comprehensive_sides SET ".implode(',',$setstring)." WHERE csid=$csid";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		if ($exposed == 'No'){
		$query = "UPDATE comprehensive_sides SET hazardassessment='Lead-Safe' WHERE csid=$csid";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		
		}
		//update timestamp if first time this side visited
		$query = "UPDATE comprehensive_sides SET timetaken = NOW() WHERE csid=$csid AND timetaken < '1000-01-01'";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}

	//ENDTIME
	$query = "UPDATE units SET endtime=NOW() WHERE cuid=$cuid";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
}

header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-comprehensive-soil.php?cuid=$cuid");
?>