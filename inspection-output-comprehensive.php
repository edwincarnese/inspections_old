<?php
require_once'session.php';
require_once'connect.php';
require_once'helper.php';

$cuid = getPostIsset('cuid');

if ($cuid == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}

$query = "SELECT units.iid, units.number, address, unitdesc FROM units INNER JOIN inspection USING (iid) INNER JOIN building USING (bid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
list($iid, $unitnumber, $address, $unitdesc) = mysql_fetch_row($result);

$title = "$iid-$unitnumber - $address - $unitdesc - Output";
require_once'header.php';
?>

<p><a href="inspection-output-lehraform.php?cuid=<?php print $cuid;?>" style="color:#F0059D; font-family:Calibri;" target="_new">LEHRA</a><br />
<p><a href="report-nh.php?cuid=<?php print $cuid;?>" style="color:#F0059D; font-family:Calibri;" target="_new">NH</a><br />

<p><a href="inspection-output-comprehensiveform.php?cuid=<?php print $cuid;?>" target="_new">Comprehensive Report</a><br />
<a href="inspection-output-coc-wipe.php?cuid=<?php print $cuid;?>" target="_new">Chain of Custody - Dust Wipes</a><br />
<a href="inspection-output-coc-fieldblank.php?cuid=<?php print $cuid;?>" target="_new">Chain of Custody - Field Blank</a><br />
<a href="inspection-output-coc-water.php?cuid=<?php print $cuid;?>" target="_new">Chain of Custody - Water</a><br />
<a href="inspection-output-coc-soil.php?cuid=<?php print $cuid;?>" target="_new">Chain of Custody - Soil</a><br />
<a href="inspection-output-coc-paintchips.php?cuid=<?php print $cuid;?>" target="_new">Chain of Custody - Paint Chips</a><br />


<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>