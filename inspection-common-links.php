<?php
/*
 * Created on Jan 24, 2006
 *
 * Author: Jason Rankin
 */
?>
<?php

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

$query = "SELECT iid, unitdesc FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
list($iid, $unitdesc) = mysql_fetch_row($result);

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-* - $address - $unitdesc - Links";
require_once'header.php';
?>
<p>Select the commons linked to this unit.</p>
<form action="inspection-common-links-save.php" method="post">
<input type="hidden" name="dest" value="<?php print $dest; ?>" />
<input type="hidden" name="cuid" value="<?php print $cuid; ?>" />

<p>
<?php
$query = "SELECT cuid, unitdesc, unit_links.unit FROM units LEFT JOIN unit_links ON (units.cuid=unit_links.common AND unit_links.unit=$cuid) WHERE iid=$iid AND unittype!='Interior' AND (unit_links.unit=$cuid OR unit_links.unit IS NULL) ORDER BY number, unittype";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while ($row = mysql_fetch_assoc($result)) {
	if ($row['unit']) {
		print "<input type=\"checkbox\" name=\"commonids[]\" value=\"$row[cuid]\" checked=\"checked\" />";
	} else {
		print "<input type=\"checkbox\" name=\"commonids[]\" value=\"$row[cuid]\" />";
	}
	print "$row[unitdesc]<br />\n";
}
?>
</p>

<p>
<input type="submit" name="submit" value="Save" /><input type="reset" value="Reset" /><input type="submit" name="submit" value="Cancel" />
</p>
