<?php
$title = "Inspectors - Search Results";
require_once'session.php';
require_once'connect.php';

if (strlen ($_POST['phone2']) == 0) {
	$_POST['phone2'] = '%';
}
$_POST['number'] = "$_POST[phone1]-$_POST[phone2]-$_POST[phone3]";
unset($_POST['phone1']);
unset($_POST['phone2']);
unset($_POST['phone3']);
if (strlen($_POST['number']) == 3) {
	unset($_POST['number']);
}

$wheres = array();
foreach ($_POST as $field => $value) {
	if ($value) {
		switch ($field) {
			case 'firstname':
			case 'lastname':
			case 'city':
			case 'number':
				$value = htmlspecialchars($value);
				$wheres[] = "$field LIKE '%$value%'";
				break;
			case 'state':
			case 'zip':
				$wheres[] = "$field = '$value'";
				break;
			default:
				exit("$field not coded");
		}
	}
}
$wherestring = implode(' AND ', $wheres);

if (!$wherestring) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspector-list.php");
	exit();
}

$query = "SELECT DISTINCT inspector.tid, lastname, firstname, CONCAT(streetnum, ' ', address, ' ', suffix), city, state FROM inspector INNER JOIN inspector_phone USING (tid) WHERE $wherestring";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 1) {
	$tid = mysql_result($result, 0, 'tid');
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspector-view.php?tid=$tid");
	exit();
}

require_once'header.php';


print "<p><a href=\"inspector-add.php\">Click here to add an inspector.</a></p>";

print "<p>";
while ($row = mysql_fetch_row($result) ) {
	$tid = array_shift($row);
	print "<a href=\"inspector-view.php?tid=$tid\">".implode(', ', $row)."</a><br />\n";
}
print "</p>";

?>
<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>