<?php
require_once'session.php';
require_once'connect.php';

$iid = $_POST['iid'] or $iid = $_GET['iid'] or $iid = 0;

if ($iid == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}

$query = "SELECT status FROM inspection WHERE iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$status = mysql_result($result, 0);
if ($status == 'Completed') { //ignore if completed
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}

$query = "UPDATE inspection SET status='In Progress' WHERE iid=$iid";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-* - $address - Main";
require_once'header.php';
?>
<p>Click a unit to begin its inspection.</p>
<p>
<?php
$query = "SELECT cuid, number, unitdesc, unittype, insptype FROM units WHERE iid=$iid ORDER BY number, unittype";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while ($row = mysql_fetch_assoc($result)) {
	$insptype = strtolower($row['insptype']);
	print "$iid-$row[number]&nbsp;&nbsp;<a href=\"inspection-$insptype-unit.php?cuid=$row[cuid]\">$row[unitdesc] ($row[unittype])</a>&nbsp;&nbsp;($row[insptype])";
	if ($row['unittype'] == 'Interior Common') {
		print "&nbsp;&nbsp;&nbsp;<a href=\"inspection-unit-links.php?cuid=$row[cuid]\">Links</a>";
	}
	print "<br />\n";
}
?>
</p>

<p><a href="inspection-comprehensive-xrfcalibration.php?iid=<?php print $iid;?>">XRF Calibration</a><br />
<a href="inspection-comprehensive-xrfsummary.php?iid=<?php print $iid;?>">XRF Summary</a></p>
<!--
<p><a href="inspection-review.php">Review Inspection</a></p>
-->
<p><a href="inspection-view.php?iid=<?php print $iid;?>">View Building Inspection page</a></p>

<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>