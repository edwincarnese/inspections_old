<?php
require_once'session.php';
require_once'connect.php';
require_once'helper.php';

$cuid = getPostIsset('cuid');

if ($cuid == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}
?>
<SCRIPT LANGUAGE="JavaScript">

function popUp(URL) {
day = new Date();
id = day.getTime();
eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=825,height=530,top=575');");
}
</script>

<?php

$query = "SELECT units.iid, units.number, address, unitdesc FROM units INNER JOIN inspection USING (iid) INNER JOIN building USING (bid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
list($iid, $unitnumber, $address, $unitdesc) = mysql_fetch_row($result);

$title = "$iid-$unitnumber - $address - $unitdesc - Output";
require_once'header.php';
?>

<!-- <p><a href="javascript:popUp('dompdf/inspection-report.php?iid=<?php echo $iid;?>&isPopup=true')" style='color:#F0059D; font-family:calibri;'>Kate's Report</a><br /> -->

<!-- <p><a href="dompdf/inspection-report.php?iid=<?php echo $iid;?>&isPopup=true" target="_blank" style='color:#F0059D; font-family:calibri;'>Kate's Report</a><br />  -->

<p><a href="inspection-output-conformanceform.php?cuid=<?php print $cuid;?>" target="_new">Conformance Report</a><br />
<a href="inspection-output-coc-wipe.php?cuid=<?php print $cuid;?>" target="_new">Chain of Custody - Dust Wipes</a><br />
<a href="inspection-output-coc-fieldblank.php?cuid=<?php print $cuid;?>" target="_new">Chain of Custody - Field Blank</a><br />
<p><a href="inspection-output-FontTest.php?cuid=<?php print $cuid;?>" target="_new">Font Test</a><br />
</p>

<p><a href="inspection-view.php?iid=<?php print $iid;?>">View Building Inspection page</a></p>

<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>