<?php

//this page requires crid, shoud not assume a cuid, must not set cuid

$pdf->AddPage('P');
$x = 0;
$y = 0;
$ml=0.5;
$ff=65;
$w=7.5; 
$pdf->SetFont('Arial','',14);

//ADDRESS, TOP
$query = "SELECT inspection.iid, streetnum, address, suffix, city, DATE_FORMAT(starttime, '%c/%e/%Y') AS start, unitdesc, number FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);
$y=.2;
//Inspection #
$pdf->SetFont('Times','B','10');
$pdf->SetXY(1.5, $y);
$pdf->Cell(0,0, "Inspection $row[iid]-$row[number]");
$pdf->SetXY(3, $y);

//$pdf->Cell(0,0, "$crid $start $total $WMdone $Wdone $WWdone $Ddone $crid");
$ml=0.625;
//Interior Inspection Banner
$x=2.5;
$y=$y+.2;
$pdf->Rect($ml, $y+0.15, 7.25, 0.3);
$y=$y+.31;
$pdf->SetFont('Times','B','12');
$pdf->SetXY($x, $y);
$pdf->Cell(0,0,'Interior Paint Inspection ');
$pdf->SetFont('Times','','10');
$pdf->SetXY($x+2.0, $y);
$pdf->Cell(0,0,'(Required if built pre-1978)');

//PAGE NUMBERS
$pdf->SetFont('Times','','10');
$pdf->SetXY(6.5, $y);
$pdf->Cell(0,0, 'Page _____ of _____');
$pdf->SetXY(6.9, $y);
$pdf->Cell(0, 0, $pdf->PageNo());
$pdf->SetXY(7.4, $y);
$pdf->Cell(0,0, '{totalpages}');

$iid = $row['iid'];

//Unit Specifications

//ADDRESS, TOP
$query = "SELECT inspection.iid, streetnum, address, suffix, city, unitdesc, number FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);


$pdf->SetFont('Times','','10');

$x=$ml;
$y=$y+.35;
$pdf->SetXY($x, $y);
$pdf->Cell($x, 0, 'Floor / Apt. No.');
$pdf->SetXY($x, $y);
$pdf->Rect($x+1, $y+.1, 1.3, 0);
$pdf->SetXY($x+1, $y);
$pdf->Cell(1.3, 0, $row['unitdesc'], 0, 0, 'C');
$x=3;
$pdf->SetXY($x, $y);
$pdf->Cell($x, 0, 'Street /City:');
$pdf->SetXY($x, $y);
$pdf->Rect($x+.95, $y+.1, 3.85, 0);
$pdf->SetXY($x+1.3, $y);
$pdf->Cell(0,0, "$row[streetnum] $row[address] $row[suffix], $row[city]");

$y=$y+0.35;

$query = "SELECT number, doors, windows, name, cuid FROM comprehensive_rooms WHERE crid=$crid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0, 'Room#');
$pdf->SetXY($ml+.6, $y);
$pdf->Cell(0,0, $row['number']);
$pdf->SetXY($ml+.9, $y);
$pdf->Cell(0,0, 'Door Count');
$pdf->SetXY($ml+1.825, $y);
$pdf->Cell(0,0, $row['doors']);
$pdf->SetXY($ml+2.1, $y);
$pdf->Cell(0,0, 'Window Count');
$pdf->SetXY(3.9, $y);
$pdf->Cell(0,0, $row['windows']);
$pdf->SetXY($ml+3.5, $y);
$pdf->Cell(0,0, 'Laboratory Utilized:');
$pdf->SetXY($ml+5, $y);
$pdf->Cell(0,0, 'Schneider Laboratories, Inc');
$y=$y+0.03;
$pdf->SetLineWidth(0.005);
$pdf->SetXY($ml+0.05, $y+.18);
$pdf->Cell(1.57,0, 'Room Description', 0, 0, 'C');
$pdf->SetXY($ml+1.58, $y+.18);
$pdf->Cell(0,0, 'Side');
$pdf->SetXY($ml+2.1, $y+.22);
$pdf->Cell(0,0, 'Substrate');
$pdf->SetXY($ml+2.1, $y+.35);
$pdf->Cell(0,0, '(see key)');
$pdf->SetXY($ml+3, $y+.22);
$pdf->Cell(0,0, 'XRF');
$pdf->SetFont('Times','','8');
$pdf->SetXY($ml+2.8, $y+.35);
$pdf->SetFont('Times','','10');
$pdf->Cell(0,0, '(mg/sq.cm)');
$pdf->SetXY($ml+3.66, $y+.22);
$pdf->Cell(0,0, 'Condition');
$pdf->SetXY($ml+3.56, $y+.35);
$pdf->Cell(0,0, 'I / D / DT / N');
$pdf->SetFont('Times','','8');
$pdf->SetXY($ml+4.5, $y+.15);
$pdf->Cell(0,0, 'Spot');
$pdf->SetXY($ml+4.5, $y+.27);
$pdf->Cell(0,0, 'Test');
$pdf->SetXY($ml+4.5, $y+.38);
$pdf->Cell(0,0, '(+/-)');
$pdf->SetXY($ml+5.1, $y+.15);
$pdf->Cell(0,0, 'Lab');
$pdf->SetXY($ml+5, $y+.26);
$pdf->Cell(0,0, 'Sample');
$pdf->SetXY($ml+5, $y+.37);
$pdf->Cell(0,0, 'Number');
$pdf->SetXY($ml+6.025, $y+.15);
$pdf->Cell(0,0, 'Lab');
$pdf->SetXY($ml+5.975, $y+.26);
$pdf->Cell(0,0, 'Results');
$pdf->SetXY($ml+6.0, $y+.37);
$pdf->Cell(0,0, '(ppm)');
$pdf->SetXY($ml+6.725, $y+.15);
$pdf->Cell(0,0, 'Hazard');
$pdf->SetXY($ml+6.625, $y+.26);
$pdf->Cell(0,0, 'Assessment');
$pdf->SetXY($ml+6.72, $y+.37);
$pdf->Cell(0,0, 'F / S / H');
$pdf->SetFont('Times','','10');
$pdf->SetXY($ml, $y+.35);
$pdf->Cell(1.58,0, $row['name'], 0, 0, 'C');


$c=9.0;
$pdf->SetFont('Times','B','12');
$pdf->SetXY($ml+.15, $c-.07);
$pdf->Cell(0, 0, 'KEY TO HEADINGS');

$pdf->SetFont('Times','','10');
$c=$c+.05;
$h2=$c;
$pdf->Line($ml, $c, $ml+$w, $c);
$c=$c+.094;
$pdf->SetXY($ml+.5, $c);
$pdf->Cell(0, 0, 'Substrate');
$pdf->SetXY($ml+1.6, $c);
$pdf->Cell(0, 0, 'WD = Wood  PS = Plaster / Sheetrock  MA = Masonry  ME = Metal  O = Other');
$c=$c+.18;
$pdf->SetXY($ml+1.6, $c);
$pdf->Cell(0, 0, 'VW = Varnished Wood  V = Vinyl VCB = Vinyl Cove Base Molding   PNL = Paneling');
$c=$c+.09325;
$pdf->Line($ml, $c, $ml+$w, $c);
$c=$c+.09375;
$pdf->SetXY($ml+.5, $c);
$pdf->Cell(0, 0, 'Condition');
$pdf->SetXY($ml+1.6, $c);
$pdf->Cell(0, 0, 'I = Intact   DT = Touch-up   D = Damaged   N = No Paint   78 = Post 1977');
$c=$c+.09375;
$pdf->Line($ml, $c, $ml+$w, $c);
$c=$c+.09375;
$pdf->SetXY($ml+.25, $c);
$pdf->Cell(0, 0, 'Hazard Assessment');
$pdf->SetXY($ml+1.6, $c);
$pdf->Cell(0, 0, 'F = Lead-Free   S = Lead-Safe   H = Hazard');
$c=$c+.09375;
$pdf->Line($ml, $c, $ml+$w, $c);

$pdf->Line($ml, $h2, $ml, $c);
$pdf->Line($ml+1.5, $h2, $ml+1.5, $c);
$pdf->Line($ml+$w+0.05, $h2, $ml+$w+0.05, $c);

$lh = 0.22;
$pdf->SetXY(0.42, 1.75);
$map = array('Lead-free' => 'F            ', 'Lead-safe' => 'S            ', 'Hazard' => '            H');
$condmap = array('Intact' => 'I               ', 'Intact Where Visible' => 'IWV              ', 'Damaged Touchup' => '              DT', 'Damaged' => '              D', 'Assumed Damaged' => '              D', 'No Paint' => 'N              ', 'Post \'77' => '78              ', 'Damaged - Friction' => '              DF', 'Covered' => 'CV              ', 'Intact - Factory Finish' => 'IFF              ');
$start = $start + 0; //favorite force to number trick
//get the paint chip results

//$ml, 1.5, 0.3, 1.0, 0.7, 0.94, 0.36, 0.9, 0.9, 0.7

//
$query = "SELECT SQL_CALC_FOUND_ROWS comprehensive_components.ccid, comprehensive_components.name, side, abbreviation AS substrate, xrf, DATE_FORMAT(xrftaken, '%l:%i %p') AS xrftaken, 'condition', spottest, paintchips.number AS chipnumber, paintchips.results, hazardassessment FROM comprehensive_components LEFT JOIN substrates USING (sid) LEFT JOIN paintchips ON (comprehensive_components.ccid=paintchips.ccid AND paintchips.comptype='Interior') WHERE crid=$crid ORDER BY comprehensive_components.displayorder, comprehensive_components.ccid LIMIT $start, 34";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
//$pdf->SetXY($ml, $y+.5);
$h3=0.4;
	$nowy = $pdf->GetY()-0.25;
	$pdf->SetXY($ml, $nowy);

			$pdf->SetX($ml);
			$pdf->Cell( 1.5,  $h3, '', 1, 0, 'L');
			$pdf->Cell( 0.5,  $h3, '', 1, 0, 'L');
			$pdf->Cell( 0.8,  $h3, '', 1, 0, 'L');
			$pdf->Cell( 0.7,  $h3, '', 1, 0, 'L');
			$pdf->Cell( 0.94, $h3, '', 1, 0, 'L');
			$pdf->Cell( 0.36, $h3, '', 1, 0, 'L');
			$pdf->Cell( 0.9,  $h3, '', 1, 0, 'L');
			$pdf->Cell( 0.9,  $h3, '', 1, 0, 'L');
			$pdf->MultiCell( 0.7,  $h3, '', 1, 0, 'L');

while (($row = mysql_fetch_assoc($result)) && ($pdf->GetY() < 8.4)) { //don't overflow
	formfix($row);
	$start++; //keep running count for next time
	$pdf->SetX($ml);
	$nowy = $pdf->GetY();
	$pdf->MultiCell( 1.5, $lh, $row['name'], 1, 'L');
	$nexty = $pdf->GetY();
	$pdf->SetXY(1.5+$ml, $nowy);
	$pdf->Cell( 0.5, ($nexty - $nowy), $row['side'], 1, 0, 'C');
	$pdf->Cell( 0.8, ($nexty - $nowy), $row['substrate'], 1, 0, 'C');
	$pdf->Cell( 0.7, ($nexty - $nowy), $row['xrf'], 1, 0, 'C');
	//don't display xrf time, but keep items aligned
//	$row['xrftaken'] = '';
//	$pdf->Cell( 0.001, ($nexty - $nowy), $row['xrftaken'], 1, 0, 'C');
	$pdf->Cell( 0.94, ($nexty - $nowy), $condmap[$row['condition']], 1, 0, 'C');
	$pdf->Cell( 0.36, ($nexty - $nowy), $row['spottest'], 1, 0, 'C');
	if ($row['chipnumber']) {
		$sample = $iid.'-'.$unitnumber.'P'.$row['chipnumber'];
	} else {
		$sample = '';
	}
	$pdf->Cell( 0.9, ($nexty - $nowy), $sample, 1, 0, 'C');
	//display paint chip sample results
	$pdf->Cell( 0.9, ($nexty - $nowy), $row['results'], 1, 0, 'C');
	$haz = $row['hazardassessment'];
	$letter = $map["$haz"];
	$pdf->Cell( 0.7, ($nexty - $nowy), " $letter ", 1, 0, 'C');
	$pdf->SetY($nexty);

}


//mysql_free_result($result); //can sometimes be big

$query = "SELECT FOUND_ROWS()";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$total = mysql_result($result, 0);
// It is the calling script's job to deal with $start and $total appropriately.


//crossed out items

$query = "SELECT COUNT(*) FROM comprehensive_components WHERE crid=$crid AND  FIND_IN_SET('Woodwork/Molding', grouplist) > 0";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_result($result, 0) == 0) {
	if (!isset($WMdone)) { //not done in a previous loop
		if ($pdf->GetY() < 9.00) {
			$start++; //keep running count for next time
			$pdf->SetX($ml);
			$nowy = $pdf->GetY();
			$pdf->MultiCell( 1.5, $lh, 'Woodwork/Molding', 1, 'L');
			$pdf->Line($ml+0.02, $pdf->GetY()-0.11, 1.85, $pdf->GetY()-0.11);
			$nexty = $pdf->GetY();
			$pdf->SetXY($ml+1.5, $nowy);
//			$pdf->Cell( 1.5, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.5, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.8, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.7, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.94, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.36, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.9, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.9, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.7, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->SetY($nexty);
			$WMdone = 1;
		} else {
			$total++; //need to trigger a loop, perhaps
		}
	}
}


$query = "SELECT COUNT(*) FROM comprehensive_components WHERE crid=$crid AND FIND_IN_SET('Window', grouplist) > 0";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_result($result, 0) == 0) {
	if (!isset($Wdone)) { //not done in a previous loop
		if ($pdf->GetY() < 9.00) {
			$start++; //keep running count for next time
			$pdf->SetX($ml);
			$nowy = $pdf->GetY();
			$pdf->MultiCell( 1.5, $lh, 'Window', 1, 'L');
			$pdf->Line($ml+0.02, $pdf->GetY()-0.11, $ml+0.6, $pdf->GetY()-0.11);
			$nexty = $pdf->GetY();
			$pdf->SetXY($ml+1.5, $nowy);
//			$pdf->Cell( 1.5, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.5, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.8, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.7, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.94, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.36, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.9, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.9, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.7, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->SetY($nexty);
			$Wdone = 1;
		} else {
			$total++; //need to trigger a loop, perhaps
		}
	}
}

$query = "SELECT COUNT(*) FROM comprehensive_components WHERE crid=$crid AND FIND_IN_SET('Window Well', grouplist) > 0";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_result($result, 0) == 0) {
//	$total += 1;
	if (!isset($WWdone)) { //not done in a previous loop
		if ($pdf->GetY() < 9.00) {
			$start++; //keep running count for next time
			$pdf->SetX($ml);
			$nowy = $pdf->GetY();
			$pdf->MultiCell( 1.5, $lh, 'Window Well', 1, 'L');
			$pdf->Line($ml+0.02, $pdf->GetY()-0.11, 1.5, $pdf->GetY()-0.11);
			$nexty = $pdf->GetY();
			$pdf->SetXY($ml+1.5, $nowy);
//			$pdf->Cell( 1.5, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.5, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.8, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.7, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.94, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.36, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.9, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.9, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.7, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->SetY($nexty);
			$Wdone = 1;
		} else {
			$total++; //need to trigger a loop, perhaps
		}
	}
}

$query = "SELECT COUNT(*) FROM comprehensive_components WHERE crid=$crid AND FIND_IN_SET('Door', grouplist) > 0";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_result($result, 0) == 0) {

//	$total += 1;
	if (!isset($Ddone)) { //not done in a previous loop
		if ($pdf->GetY() < 9.00) {
			$start++; //keep running count for next time
			$pdf->SetX($ml);
			$nowy = $pdf->GetY();
			$pdf->MultiCell( 1.5, $lh, 'Door', 1, 'L');
			$pdf->Line($ml+0.02, $pdf->GetY()-0.11, 0.97, $pdf->GetY()-0.11);
			$nexty = $pdf->GetY();
			$pdf->SetXY($ml+1.5, $nowy);
//			$pdf->Cell( 1.5, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.5, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.8, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.7, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.94, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.36, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.9, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.9, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->Cell( 0.7, ($nexty - $nowy), '', 1, 0, 'C');
			$pdf->SetY($nexty);
			$Ddone = 1;
		} else {
			$total++; //need to trigger a loop, perhaps
		}
	}
}

while ($pdf->GetY() < 8.50) { //extra lines
			$pdf->SetX($ml);
			$pdf->Cell( 1.5,  $lh, '', 1, 0, 'L');
			$pdf->Cell( 0.5,  $lh, '', 1, 0, 'L');
			$pdf->Cell( 0.8,  $lh, '', 1, 0, 'L');
			$pdf->Cell( 0.7,  $lh, '', 1, 0, 'L');
			$pdf->Cell( 0.94, $lh, '', 1, 0, 'L');
			$pdf->Cell( 0.36, $lh, '', 1, 0, 'L');
			$pdf->Cell( 0.9,  $lh, '', 1, 0, 'L');
			$pdf->Cell( 0.9,  $lh, '', 1, 0, 'L');
			$pdf->MultiCell( 0.7,  $lh, '', 1, 0, 'L');

}


//date/time at bottom

$query = "SELECT DATE_FORMAT(starttime, '%c/%e/%Y') AS startdate, DATE_FORMAT(starttime, '%l:%i %p') AS starttime FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_row($result);


$pdf->SetXY(6.1, 10.10);
$pdf->Cell(0,0, '___________  /  ___________ ');
$pdf->SetXY(7.05, 10.10);
$pdf->Cell(0,0, $row[0]);
$pdf->SetXY(6, 10.30);
$pdf->Cell(0,0, '      Initials                 Date ');

$pdf->SetFont('Times','','9');
$pdf->SetXY($ml, 10.60);
$pdf->Cell(0,0, 'FORM PBLC-23-6A (9/97) Replaces OEHRA II (4/97) p.6a, which may be used.');



?>