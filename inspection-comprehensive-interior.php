<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

$query = "SELECT iid, starttime, unitdesc, number, diagrams FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}

list($iid, $starttime, $unitdesc, $unitnumber, $diagrams) = mysql_fetch_row($result);

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$query = "SELECT * FROM comprehensive_rooms WHERE cuid=$cuid ORDER BY number";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	//switch to setup page
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-comprehensive-interior-setup1.php?cuid=$cuid");
	exit();
}

$title = "$iid-$unitnumber - $address - $unitdesc";
require_once'header.php';
?>
<table>
<tr><th>Room Name</th><th>Doors</th><th>Windows</th><th>Delete</th></tr>
<?php
while ($row = mysql_fetch_assoc($result)) {
	print "<tr><td><a href=\"inspection-comprehensive-room.php?crid=$row[crid]\">($row[number]) $row[name]</a></td><td>$row[doors]</td><td>$row[windows]</td><td><a href=\"inspection-comprehensive-deleteroom-confirm.php?crid=$row[crid]&amp;cuid=$cuid\" class=\"red\">Delete</a></td></tr>\n";
}
?>
</table>
<p><a href="inspection-comprehensive-interior-addroom.php?cuid=<?php print $cuid; ?>">Add new room</a></p>
<p>Diagrams:&nbsp;&nbsp;&nbsp;<?php print $diagrams; ?>&nbsp;&nbsp;&nbsp;<a href="inspection-comprehensive-unit-diagrams.php?cuid=<?php print $cuid; ?>">Edit</a></p>
<?php
$query = "SELECT CONCAT(iid, '-', comprehensive_wipes.number) AS wid, comprehensive_wipes.number AS wipenumber, comprehensive_rooms.number, comprehensive_rooms.name, comprehensive_wipes.surface, side, arealength, areawidth FROM comprehensive_wipes INNER JOIN comprehensive_rooms USING (crid) WHERE cuid=$cuid AND comprehensive_wipes.iid=$iid ORDER BY wipenumber";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

?>
<p>Dust Wipes:&nbsp;&nbsp;&nbsp;<a href="inspection-comprehensive-wipe.php?cuid=<?php print $cuid; ?>">Add Wipe</a></p>
<?php
if (mysql_num_rows($result)) {
?>
<table>
<tr><th>Sample #</th><th>Room #</th><th>Room Name</th><th>Side</th><th>Surface</th><th>Area</th><th>Edit</th></tr>
<?php
	while ($row = mysql_fetch_assoc($result)) {
		print "<tr><td>$iid-$unitnumber"."D$row[wipenumber]</td><td>$row[number]</td><td>$row[name]</td><td>$row[side]</td><td>$row[surface]</td><td>$row[arealength]&quot; x $row[areawidth]&quot;</td><td><a href=\"inspection-comprehensive-wipe.php?cuid=$cuid&amp;wid=$row[wid]\">Edit</a></td></tr>\n";
	}
?>
</table>
<?php
}
?>
<p>Dust Comments:&nbsp;&nbsp;&nbsp;<a href="inspection-comprehensive-wipe-comments.php?cuid=<?php print $cuid; ?>">Edit</a><br />
<!--<textarea name="comment" rows="6" cols="50"> --><?php
$query = "SELECT comment FROM comprehensive_comments WHERE cuid=$cuid AND type='dust'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) ) {
	$comment = mysql_result($result, 0);
}
print $comment;
?><!-- </textarea> --></p>
<p><a href="inspection-comprehensive-water.php?cuid=<?php print $cuid; ?>">Water Inspection</a></p>

<p><a href="inspection-comprehensive-interior-paintchips.php?cuid=<?php print $cuid; ?>">Paint Chip Samples</a> |
<a href="inspection-comprehensive-xrfcalibration.php?cuid=<?php print $cuid; ?>">XRF Calibration</a> |
<a href="inspection-comprehensive-xrfsummary.php?cuid=<?php print $cuid;?>">XRF Summary</a> |
<a href="inspection-comprehensive-interior-samplesummary.php?cuid=<?php print $cuid;?>">Sample Summary</a> |
<a href="inspection-comprehensive-unit-notes.php?cuid=<?php print $cuid;?>">Private Unit Inspection Notes</a> |
<a href="inspection-comprehensive-interior-wrapup.php?cuid=<?php print $cuid;?>">Wrap up</a></p>
<script language="javascript">
function checkCommons(links) {
	if (links <= 0) {
		alert("There are no commone areas linked to this unit.  Are there common stairs or halls that lead to the unit?");
	}
	window.location = "inspection-view.php?iid=<?php echo $iid; ?>";
}
</script>
<?php
	$query = "SELECT COUNT(*) FROM unit_links WHERE unit=$cuid";
	$result = mysql_query($query) or sql_crapout($query."<br />".mysql_error());
	$links = mysql_result($result, 0);
?>
<p><a href="javascript:checkCommons(<?php echo $links;?>);">Inspection Main Menu</a></p>
<?php
require_once'footer.php';
?>