<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;
$csid = $_POST['csid'] or $csid = $_GET['csid'] or $csid = 0;

if ($csid) {
	$query = "SELECT * FROM comprehensive_soil_debris WHERE csid=$csid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	if (mysql_num_rows($result)) {
		$debris = mysql_fetch_assoc($result);
		$cuid = $debris['cuid'];
		$samplenumber = $debris['samplenumber'];
	}
} else { //defaults for new samples
	$debris['depth'] = 1;
}

$query = "SELECT iid, number FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}
list($iid, $unitnumber) = mysql_fetch_row($result);

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - Exterior - Debris";
require_once'header.php';
?>
<form action="inspection-comprehensive-soil-debris-save.php" method="post">
<input type="hidden" name="cuid" value="<?php print $cuid; ?>" />
<input type="hidden" name="csid" value="<?php print $csid; ?>" />
<table class="straightup">
<tr><th>Debris</th><th>Side of<br />House</th><th>Location</th><th>Distance (ft)</th><th>Depth (in)</th><th>Lab Number</th></tr>
<tr>
<td><input type="text" name="debris" value="<?php print $debris['debris']; ?>" /></td>
<td>
<?php
for ($x=1; $x<=4; $x++) {
	if ($x == $debris['side']) {
		print "<input type=\"radio\" name=\"side\" value=\"$x\" checked=\"checked\" />$x<br />\n";
	} else {
		print "<input type=\"radio\" name=\"side\" value=\"$x\" />$x<br />\n";
	}
}
?>
</td>
<td><input type="text" name="location" value="<?php print $debris['location']; ?>" /></td>
<td><input type="text" size="8" name="distance" value="<?php print $debris['distance']; ?>" /></td>
<td><input type="text" size="8" name="depth" value="<?php print $debris['depth']; ?>" /></td>
<td>
<?php
if (!$samplenumber) {
	$query = "SELECT MAX(samplenumber) FROM comprehensive_soil_debris WHERE cuid=$cuid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$samplenumber = mysql_result($result, 0) + 1;
}
print "$iid"."SD$samplenumber</td>";
?>
<input type="hidden" name="samplenumber" value="<?php print $samplenumber; ?>" />
</td>
</tr>
</table>
<p><input type="submit" name="submit" value="Save" /><input type="reset" value="Reset" /><input type="submit" name="submit" value="Cancel" /></p>
</form>

<p><a href="inspection-comprehensive-exterior.php?cuid=<?php print $cuid; ?>">Exterior Main Menu</a></p>

<p><a href="inspection-main.php?iid=<?php print $iid; ?>">Inspection Main Menu</a></p>
<?php
require_once'footer.php';
?>