<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;

$query = "SELECT iid, number FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}
list($iid, $unitnumber) = mysql_fetch_row($result);

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - Exterior - Soil (Edit)";
require_once'header.php';
?>
<form action="inspection-comprehensive-soil-save.php" method="post">
<input type="hidden" name="cuid" value="<?php print $cuid; ?>" />
<table class="straightup">
<tr><th>Side</th><th>Inaccessible</th><th>Ground Cover</th><th>Distance (ft)</th><th>Depth (in)</th><th>Lab Number</th></tr>
<?php
$query = "SELECT * FROM comprehensive_sides WHERE cuid=$cuid ORDER BY side";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while ($row = mysql_fetch_assoc($result)) {
	print "<tr><td>$row[side]</td>
<td>";
	$inaccessibles = array('No','Wet/Rain','Covered by Debris','Covered by Ice/snow','Other');
	foreach ($inaccessibles as $inaccessible) {
		if ($row['inaccessible'] == $inaccessible) {
			print "<input type=\"radio\" name=\"sides[$row[csid]][inaccessible]\" value=\"$inaccessible\" checked=\"checked\" />$inaccessible<br />";
		} else {
			print "<input type=\"radio\" name=\"sides[$row[csid]][inaccessible]\" value=\"$inaccessible\" />$inaccessible<br />";
		}
	}
	print '</td><td>';
	$covers = array('None','Pavement','Grass','Mulch');
	foreach ($covers as $cover) {
		if ($row['cover'] == $cover) {
			print "<input type=\"radio\" name=\"sides[$row[csid]][cover]\" value=\"$cover\" checked=\"checked\" />$cover<br />";
		} else {
			print "<input type=\"radio\" name=\"sides[$row[csid]][cover]\" value=\"$cover\" />$cover<br />";
		}
	}
	print "</td>
<td><input type=\"text\" size=\"8\" name=\"sides[$row[csid]][distance]\" value=\"$row[distance]\" /></td>
<td><input type=\"text\" size=\"8\" name=\"sides[$row[csid]][depth]\" value=\"$row[depth]\" /></td>
<td>$iid"."SS$row[side]</td>";
print "</tr>\n";
}
?>
</table>
<p><input type="submit" name="submit" value="Save" /><input type="reset" value="Reset" /><input type="submit" name="submit" value="Cancel" /></p>
</form>

<p><a href="inspection-comprehensive-exterior.php?cuid=<?php print $cuid; ?>">Exterior Main Menu</a></p>

<p><a href="inspection-main.php?iid=<?php print $iid; ?>">Inspection Main Menu</a></p>
<?php
require_once'footer.php';
?>