<?php

//subtract 0.06 inches from desired position of filst letter
// with 0.1 lineheight, measure from middle using Cell().

/*
Places to put X's using 6-point text:
ITEM		X		Y
Stardard:	0.87	2.88
------------------------
Paint:		2.155	3.51
Soil:		2.155	3.80
Water:		3.18	3.04	(Water, Drinking)
Wipe:		3.18	3.34
------------------------
Lead:		6.76	2.40
*/

$pdf->SetFont('Arial','',6);
$pdf->SetXY(2.155, 3.80);
$pdf->Cell(0,0, 'X');
/*
//SIDES
$query = "SELECT side, DATE_FORMAT(timetaken, '%c/%e') AS `date`, DATE_FORMAT(timetaken, '%l%p') as `time` FROM comprehensive_sides WHERE cuid=$cuid AND inaccessible='No' AND cover='None' ORDER BY side";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$total += mysql_num_rows($result);

$pdf->SetFont('Arial','','8');
$x = 0.85; $y = 4.43;
while($row = mysql_fetch_assoc($result)) {
	$pdf->SetXY($x, $y);
	$pdf->Cell(0.69,0,$iid.'-0SS'.$row['side'],0,0,'C'); //cheat and just use -0
	$pdf->SetX($x + 0.78);
	$pdf->Cell(0.54,0,"$row[date]",0,0,'C');
	$pdf->SetX($x + 1.29);
	$pdf->Cell(0.47,0,"$row[time]",0,0,'C');
	$pdf->SetX($x + 1.78);
	$pdf->Cell(0,0,"Side $row[side]");
	$pdf->SetXY($x + 7.02, $y);
	$pdf->Cell(0,0,"1");
	$y += 0.31;
}

//DEBRIS
$query = "SELECT samplenumber, DATE_FORMAT(timetaken, '%c/%e') AS `date`, DATE_FORMAT(timetaken, '%l%p') as `time`, debris, side FROM comprehensive_soil_debris WHERE cuid=$cuid AND inaccessible='No' ORDER BY samplenumber";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$total += mysql_num_rows($result);

if ($total > 9) { // Remove this at some point!!
	exit('Too many samples for one page.');
}

$pdf->SetFont('Arial','','8');
while($row = mysql_fetch_assoc($result)) {
	$pdf->SetXY($x, $y);
	$pdf->Cell(0.69,0,$iid.'-0SD'.$row['samplenumber'],0,0,'C');
	$pdf->SetX($x + 0.78);
	$pdf->Cell(0.54,0,"$row[date]",0,0,'C');
	$pdf->SetX($x + 1.29);
	$pdf->Cell(0.47,0,"$row[time]",0,0,'C');
	$pdf->SetX($x + 1.78);
	$pdf->Cell(0,0,"$row[debris], Side $row[side]");
	$pdf->SetXY($x + 7.02, $y);
	$pdf->Cell(0,0,"1");
	$y += 0.31;
}

//OBJECTS
$query = "SELECT samplenumber, DATE_FORMAT(timetaken, '%c/%e') AS `date`, DATE_FORMAT(timetaken, '%l%p') as `time`, object, side FROM comprehensive_soil_object WHERE cuid=$cuid AND inaccessible='No' AND cover='None' ORDER BY samplenumber";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$total += mysql_num_rows($result);

if ($total > 9) { // Remove this at some point!!
	exit('Too many samples for one page.');
}

$pdf->SetFont('Arial','','8');
while($row = mysql_fetch_assoc($result)) {
	$pdf->SetXY($x, $y);
	$pdf->Cell(0.69,0,$iid.'-0SO'.$row['samplenumber'],0,0,'C');
	$pdf->SetX($x + 0.78);
	$pdf->Cell(0.54,0,"$row[date]",0,0,'C');
	$pdf->SetX($x + 1.29);
	$pdf->Cell(0.47,0,"$row[time]",0,0,'C');
	$pdf->SetX($x + 1.78);
	$pdf->Cell(0,0,"$row[object], Side $row[side]");
	$pdf->SetXY($x + 7.02, $y);
	$pdf->Cell(0,0,"1");
	$y += 0.31;
}

*/
?>