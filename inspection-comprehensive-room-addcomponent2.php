<?php

require_once'session.php';
require_once'connect.php';

$crid = $_POST['crid'] or $crid = $_GET['crid'] or $crid = 0;
//this is now plural
$itemids = $_POST['itemid'] or $itemids = $_GET['itemid'] or $itemids = array();

$query = "SELECT * FROM comprehensive_rooms INNER JOIN units USING (cuid) WHERE crid=$crid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}
$room = mysql_fetch_assoc($result);
$roomname = $room['name'];
$iid = $room['iid'];
$unitnumber = $room['number']; //units is second table and will overwrite room #

$multids = array();

foreach ($itemids as $itemid) {
	$query = "SELECT * FROM room_item_components WHERE itemid=$itemid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

	if (mysql_num_rows($result) == 1) { //skip step 3 if only one for each
		$row = mysql_fetch_assoc($result);
		if($row['defaultassessment']) {
			$assess = "'$row[defaultassessment]'";
		} else {
			$assess = 'NULL';
		}		
		$query = "INSERT INTO comprehensive_components (crid, name, side, sid, condition, grouplist, conformancecomponent, displayorder, hazardassessment) VALUES ($crid, '$component[itemname] $row[componentname]', '$component[side]', $row[dsid], '$row[defaultcondition]', '$row[grouplist]', '$row[conformancecomponent]', $row[displayorder], $assess)";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	} else {
		$multids[] = $itemid; //edit foreach below if reimplemented
	}
}

if (count($multids) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-comprehensive-room-component.php?crid=$crid");
	exit();
}

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - $roomname - Add Component";
require_once'header.php';
?>
<p><!-- Items with single components have been added. -->Check the parts of these components to add.</p>
<form action="inspection-comprehensive-room-addcomponent3.php" method="post">
<input type="hidden" name="crid" value="<?php print $crid; ?>" />
<table class="straightup">
<tr><th>Parts</th><th>Default Side</th><th>Jump</th></tr>
<?php
foreach ($multids as $itemid) {
	$x++;
	$query = "SELECT itemname FROM room_item_list WHERE itemid=$itemid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$itemrow = mysql_fetch_assoc($result);
?>
<tr>
<td>
<a name="item<?php print $x;?>"></a>
<?php
	print "<b>$itemrow[itemname]</b><br />\n";
	$query = "SELECT * FROM room_item_components WHERE itemid=$itemid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	if (mysql_num_rows($result) > 1) { //print itemname only if multiple components
?>
<input type="hidden" name="components[<?php print $x; ?>][itemname]" value="<?php print $itemrow['itemname']; ?>" />
<?php
	}
	while ($row = mysql_fetch_assoc($result)) {
		print "<input type=\"checkbox\" checked=\"checked\" name=\"components[$x][parts][]\" value=\"$row[componentid]\" />$row[componentname]<br />\n";
	}
?>
</td>
<td>
<input type="radio" name="components[<?php print $x; ?>][side]" value="0" checked="checked" />None<br />
<input type="radio" name="components[<?php print $x; ?>][side]" value="1" />1<br />
<input type="radio" name="components[<?php print $x; ?>][side]" value="2" />2<br />
<input type="radio" name="components[<?php print $x; ?>][side]" value="3" />3<br />
<input type="radio" name="components[<?php print $x; ?>][side]" value="4" />4<br />
</td>
<td>
<a href="#item<?php print $x + 1;?>">Next</a>
</td></tr>
<?php
}
?>
</table>
<p><input type="submit" name="submit" value="Save" /> <input type="reset" value="Reset" /> <input type="submit" name="submit" value="Cancel" /></p>
</form>

<p><a href="inspection-comprehensive-room.php?crid=<?php print $crid; ?>">Room Main Menu</a></p>

<p><a href="inspection-comprehensive-unit.php?cuid=<?php print $room[cuid]; ?>"><?php print $room['unitdesc']; ?> Main Menu</a></p>

<p><a href="inspection-main.php?iid=<?php print $room[iid]; ?>">Inspection Main Menu</a></p>

<?php
require_once'footer.php';
?>