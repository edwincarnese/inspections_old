<?php
 
$pdf->AddPage('P');
 
$iid = $row['iid'];
$x = 0;
$y = 0;
$ml=.625;
$w=7.25;
$pdf->SetFont('Times','','12');
$pdf->SetLineWidth(0.013);
//PAGE NUMBERS
$query = "SELECT building.bid, streetnum, address, suffix, city, state, zip, numunits, yearbuilt, plat, lot, inspection.iid, unitdesc, section8, publichousing, starttime, scheddate, number, designation,units.owneroccupied as unitowneroccupied, inspection.owneroccupied as inspowneroccupied FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);  /// What is this?
/// Need to identify int, ext and soil as lead safe.
$buildingstreetaddress="$row[streetnum] $row[address] $row[suffix]";
$buildingunits="$row[units]";
$buildingcity="$row[city]";
$buildingzip="$row[zip]";

$iid = $row['iid'];

//Start Need help understanding this quesry		

$query = "SELECT client.cid, firstname, lastname, streetnum, address, suffix, city, state, zip,startdate FROM client INNER JOIN owners USING (cid) WHERE bid=$bid AND startdate <= DATE(NOW()) AND enddate >= DATE(NOW()) LIMIT 1";
/*		
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());		
*/
$ownersname="$row[firstname] $row[lastname]";
$owneraddress="$row[streetnum] $row[address] $row[suffix]";
$ownercity="$row[city]";
$ownerstate="$row[state]";
$ownerzip="$row[zip]";

//Find owners phone
$ownerphone='needs to be found';
/*
            $query = "SELECT type, number, ext FROM client_phone WHERE cid=$cid GROUP BY type";
            $result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                        formfix($row);
                        if ($row['ext']) {
                                    $nums[$row['type']] = "$row[number] ext. $row[ext]";
                        } else {
                                    $nums[$row['type']] = "$row[number]";
                        }
            }

$pdf->SetXY($xphone+0.95, $yphone);
            if ($nums['Home']) {
$pdf->Cell(0,0, $nums['Home']);
            } else if ($nums['Cell']) {
$pdf->Cell(0,0, $nums['Cell'] . ' (Cell)');

                        $cellused = 1;

}
*/
///
//Start Contractor query
$contractortype='Contractor Type';
$contractorLicNumber='License #';
$contractorentityname='Firm or person responcsible';
$contractorstartdate='Start Date';
$contractorenddate='End Date';
$contractorcertoficial='Certifying Official';
$contractorsigningdate='Contractor Signing Date ';
$contractor='Contractor ';
//End Contractor query
//
////////////////////////



//Start Inspection number
$x=1.4;
$y=$y + 0.3 ;
$pdf->SetXY($x, $y);
$pdf->Cell(0,0, "Inspection $row[iid]-$row[number]");
//End Inspection number

//Start Banner box package
$y=$y+0.3; 		//Increment Y from last input.
$ys=$y-0.2;  	//Save Y at start.
$ff=65;			//Chang in $y = $fontsize/$ff
$fontsize=14;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($x+1.625, $y);
$pdf->Cell(0,0,'Rhode Island Department of Health');
$y=$y+$fontsize/$ff+.15;	//Increment to next line based on font size
$fontsize=14;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($x+1.7, $y);
$pdf->Cell(0,0,'Certification of Lead-Safe Status');
$y=$y+0.08+$fontsize/$ff;
//$pdf->Rect($ml, $ys, $w, $y-$ys);	//create box
$pdf->line($ml, $y, $ml+$w, $y);
//End Banner box package
//Start If comprehensive then.
if ($insptype=='Comprehensive') {		
$pdf->SetXY($x,  $y);		
$pdf->Cell(0,0, 'X');		
		}
//End If comprehensive then.

//ADDRESS
//Start Banner box package
//Banner box package
$y=$y+0.1;  //This adjusts position of entire box
$ys=$y-0.1; // This adjusts the boxes height
$x=$ml;
$fontsize=12;
$ff=65;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'1.  Dwelling or Premises where Lead Inspection Conducted:');
$y=$y+$fontsize/$ff+0.1;

$fontsize=10;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'Street:');
$pdf->SetXY($ml+0.44, $y);
$pdf->Cell(0, 0, $buildingstreetaddress);
$pdf->line($ml+0.45, $y+0.065,4.3, $y+0.065);
$pdf->SetXY($ml+3.75, $y);
$pdf->Cell(0,0,'Specify Floor(s)/Apartment(s)/Portions');
$pdf->SetXY($ml+6.2, $y);
$pdf->Cell(0, 0, $buildingunits);
$pdf->line($ml+6.1, $y+0.065, $ml+$w, $y+0.065);

$pdf->SetXY($ml+4.6, $y);
$y=$y+$fontsize/$ff+0.1;
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'City/Town:');
$pdf->SetXY($ml+0.75, $y);
$pdf->Cell(0,0, $buildingcity);
$pdf->line($ml+0.7, $y+0.065, $ml+2.875, $y+0.065);
$pdf->SetXY($ml+3, $y);
$pdf->Cell(0,0,'Zip:');
$pdf->SetXY($ml+3.3, $y);
$pdf->Cell(0,0, $buildingzip );
$pdf->line($ml+3.3, $y+0.065,$ml+4.5, $y+0.065);

$y=$y+$fontsize/$ff+0.1;
$pdf->line($ml, $y, $ml+$w, $y);
//$pdf->Rect($ml, $ys, $w, $y-$ys);
//
//Owner
$y=$y+0.1;
$ys=$y-0.1;
$fontsize=12;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'2.  Owner of Dwelling or Premises:');
$y=$y+$fontsize/$ff+0.1;

$fontsize=10;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'Name:');
$pdf->SetXY($ml+0.5, $y);
$pdf->Cell(0,0, $ownersname);
$pdf->line($ml+0.38, $y+0.065,$ml+4.25, $y+0.065);
$pdf->SetXY($ml+4.6, $y);
$pdf->Cell(0,0,'Telephone No.:');
$pdf->line($ml+5.55, $y+0.065,$ml+$w, $y+0.065);
$pdf->SetXY($ml+5.6, $y);
$pdf->Cell(0,0, $ownerphone);
$y=$y+$fontsize/$ff+0.1;
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'Street:');
$pdf->SetXY($ml+0.5, $y);
$pdf->Cell(0,0, $owneraddress);
$pdf->line($ml+0.45, $y+0.065,$ml+3.125, $y+0.065);
$pdf->SetXY($ml+3.2, $y);
$pdf->Cell(0,0,'City/Town:');
$pdf->SetXY($ml+4, $y);
$pdf->Cell(0,0, $ownercity);
$pdf->line($ml+3.9, $y+0.065,$ml+5.75, $y+0.065);
$pdf->SetXY($ml+5.75, $y);
$pdf->Cell(0,0,'State:');
$pdf->SetXY($ml+6.125, $y);
$pdf->Cell(0,0, $ownerstate);
$pdf->line($ml+6.125, $y+0.065,$ml+6.375, $y+0.065);
$pdf->SetXY($ml+6.35, $y);
$pdf->Cell(0,0,'Zip:');
$pdf->SetXY($ml+6.7, $y);
$pdf->Cell(0,0, $ownerzip);
$pdf->line($ml+6.65, $y+0.065,$ml+$w, $y+0.065);
$y=$y+$fontsize/$ff+0.1;

$pdf->line($ml, $y, $ml+$w, $y);
//$pdf->Rect($ml, $ys, $w, $y-$ys);

// Certification of performance.
//Owner
$y=$y+0.1;
$ys=$y-0.1;
$fontsize=12;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'3.  Certification of lead Hazard Reduction/Lead Hazard Control Performance:');
$y=$y+$fontsize/$ff;
$fontsize=10;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml+0.2, $y);
$pdf->Cell(0,0,'(Only Required for Clearance Inspections Following Lead Hazard Reduction or Lead Hazerd Control Activity)');
$y=$y+$fontsize/$ff+0.1;
$pdf->SetFont('Times','', $fontsize);

$pdf->SetXY($ml+0.375, $y);
$pdf->Cell(0,0,'RI License Lead azard Reduction Contractor');
$pdf->line($ml, $y+0.065,$ml+0.375, $y+0.065);
$pdf->SetXY($ml+3.5, $y);
$pdf->Cell(0,0,'RI Licensed No:  LHR ');
$pdf->line($ml+5, $y+0.065,$ml+6, $y+0.065);
If ($contractortype=='LHR') {
	$pdf->SetXY($ml+0.1, $y);
	$pdf->Cell(0,0, "X");
	$pdf->SetXY($ml+4.85, $y);
	$pdf->Cell(0,0, $contractorLicNumber);
}

$y=$y+$fontsize/$ff+0.1;

$pdf->SetXY($ml+0.375, $y);
$pdf->Cell(0,0,'Lead-Safe Remodeler / Renovator');
$pdf->line($ml, $y+0.065,$ml+0.375, $y+0.065);
$pdf->SetXY($ml+3.5, $y);
$pdf->Cell(0,0,'RI Licensed No:  LRM ');
$pdf->line($ml+5, $y+0.065,$ml+6, $y+0.065);
If ($contractortype=='LRM') {
	$pdf->SetXY($ml+0.1, $y);
	$pdf->Cell(0,0, "X");
	$pdf->SetXY($ml+4.85, $y);
	$pdf->Cell(0,0, $contractorLicNumber);
}

$y=$y+$fontsize/$ff+0.1;

$pdf->SetXY($ml+0.375, $y);
$pdf->Cell(0,0,'Owner of Dwelling or Premises');
$pdf->line($ml, $y+0.065,$ml+0.375, $y+0.065);
If ($contractortype=='O') {
	$pdf->SetXY($ml+0.1, $y);
	$pdf->Cell(0,0, "X");
}

$y=$y+$fontsize/$ff+0.1;

$pdf->SetXY($ml+3.3, $y);
$pdf->Cell(0,0, $contractor);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'Print Name of Individual or Firm pwrforming the work:');
$pdf->line($ml+3.25, $y+0.065,$ml+$w, $y+0.065);
$y=$y+$fontsize/$ff+0.1;

$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'Start Date:');
$pdf->line($ml+0.635, $y+0.065,$ml+2.25, $y+0.065);

$pdf->SetXY($ml+2.5, $y);
$pdf->Cell(0,0,'Completion Date:');
$pdf->line($ml+3.625, $y+0.065,$ml+5.125, $y+0.065);

$y=$y+$fontsize/$ff+0.1;

//Signature

$fontsize=10;
$pdf->line($ml, $y+0.065,$ml+3.0, $y+0.065);
$pdf->line($ml+3.25, $y+0.065,$ml+5.875, $y+0.065);
$pdf->line($ml+6.1, $y+0.065,$ml+$w, $y+0.065);
$pdf->SetXY($ml+3.25, $y);
$pdf->Cell(0,0, $contractorcertoficial);
$pdf->SetXY($ml+5.7, $y);
$pdf->Cell(0,0,'Date:');
$pdf->SetXY($ml+6.15, $y);
$pdf->Cell(0,0, $contractorsigningdate);

$y=$y+$fontsize/$ff;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml+1.2, $y);
$pdf->Cell(0,0,'(Signature)');
$pdf->SetXY($ml+3.25, $y);
$pdf->Cell(0,0,'(Type or Print Name of Certifiying Official)');

$y=$y+$fontsize/$ff+0.1;
$pdf->line($ml, $y, $ml+$w, $y);
//$pdf->Rect($ml, $ys, $w, $y-$ys);

//Certification of Lead-Safe Status
$y=$y+0.1;
$ys=$y-0.1;
$fontsize=12;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'4.  Certification of Lead-Safe Status:');
$y=$y+$fontsize/$ff+0.1;
$pdf->SetFont('Times','B','10');
$pdf->SetXY($ml, $y);
$pdf->multiCell(6.5,.18,'The dwelling or premises identified in Item 1 above is certified as Lead-Safe as of the Certification Date specified below.  Lead-Safe status us contingent upon routine maintenance of the property.  This Certification of Lead-Safe Status shall expire no later than one year from the Certification Date specified below.  Certification of Lead-Safe Status may only be issued consequent to a Comprehensive Environmental Lead Inspection.');
$y=$y+$fontsize/$ff+0.7;
$pdf->SetFont('Times','','10');
$pdf->SetXY($ml, $y);
$pdf->multiCell(6.5,.18,'I certify that I have conducted the inspection specified in Item 4 above in accordance with the RI Rules and Regulations for Lead Poisoning Prevention, and have determined that the dwelling or premises identified above is Lead-Safe, as defined by these regulations.');
$y=$y+$fontsize/$ff+0.7;

$pdf->SetXY($ml+4.5, $y);
$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
$pdf->line($ml, $y+0.075,$ml+3.0, $y+0.065);
$pdf->line($ml+3.75, $y+0.075,$ml+7.0, $y+0.065);

$pdf->Cell(0,0, $ownersname);
$pdf->Cell(0,0, "$row[firstname] $row[lastname]");

$y=$y+0.15;
$fontsize=10;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml+1.2, $y);
$pdf->Cell(0,0,'(Signature)');
$pdf->SetXY($ml+4.5, $y);
$pdf->Cell(0,0,'(Type or Print Name of Inspector)');
$y=$y+$fontsize/$ff+0.1;

$pdf->SetXY($ml+0.35, $y);
$pdf->Cell(0,0,'What Date goes here Inspector');
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'Date:');
$pdf->line($ml+0.325, $y+0.075,$ml+2.0, $y+0.065);
$pdf->SetXY($ml+4.0, $y);
$pdf->Cell(0,0,'RI Certification No.: ELI- $row[eli]');
$pdf->line($ml+5.25, $y+0.075,$ml+6.5, $y+0.065);
$y=$y+$fontsize/$ff+0.1;

$pdf->SetFont('Times','','10');
$pdf->SetXY($ml, $y);
$pdf->multiCell(6.5,.18,'If the inspection specified above was conducted by and Environmental Lead Inspector Technician who has not fulfilled the requirements of Section 4.4(c)(2) of the regulations, this Certification must be reviewed and countervsigned below by a Rhode Island Certified Environmental Lead Inspector.');
$y=$y+$fontsize/$ff+0.7;

$pdf->SetXY($ml+4.5, $y);
$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
$pdf->line($ml, $y+0.075,$ml+3.0, $y+0.065);
$pdf->line($ml+3.75, $y+0.075,$ml+7.0, $y+0.065);

$pdf->Cell(0,0, $ownersname);
$pdf->Cell(0,0, "$row[firstname] $row[lastname]");

$y=$y+0.15;
$fontsize=10;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml+1.2, $y);
$pdf->Cell(0,0,'(Signature)');
$pdf->SetXY($ml+4.5, $y);
$pdf->Cell(0,0,'(Type or Print Name of Inspector)');
$y=$y+$fontsize/$ff+0.1;

$pdf->SetXY($ml+0.35, $y);
$pdf->Cell(0,0,'What Date goes here Inspector');
$pdf->SetXY($ml, $y);
$pdf->Cell(0,0,'Date:');
$pdf->line($ml+0.325, $y+0.075,$ml+2.0, $y+0.065);
$pdf->SetXY($ml+4.0, $y);
$pdf->Cell(0,0,'RI Certification No.: ELI- $row[eli]');
$pdf->line($ml+5.25, $y+0.075,$ml+6.5, $y+0.065);
$y=$y+$fontsize/$ff+0.1;

$pdf->line($ml, $y, $ml+$w, $y);
//$pdf->Rect($ml, $ys, $w, $y-$ys);

$y=10.5;
$pdf->SetFont('Times','', $fontsize);
$pdf->SetXY($ml,$y);
$pdf->Cell(0,0,'FORM PBLC-15 (8/02)');
$pdf->SetXY($ml+1.5,$y);
$pdf->Cell(0,0,'ONE COPY TO EACH Contractor, ELI Inspector, Property Owner, RIDOH ');
$y=$y+$fontsize/$ff;
$pdf->SetFont('Times','B', $fontsize);
$pdf->SetXY($ml+2.5,$y);
$pdf->Cell(0,0,'PREVIOUS EDITION OBSOLETE');




//////////////////////fffffffffffffffffffffff
//Start room count
$query = "SELECT COUNT(*) FROM comprehensive_rooms WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$rooms = mysql_result($result, 0);
//End room count
//Start Which table did this come from
$pdf->Cell(0,0, $row['numunits']);
$pdf->Cell(0,0, '(Units)');
$pdf->Cell(0,0, $rooms);
$pdf->Cell(0,0, $row['yearbuilt']);
$pdf->Cell(0,0, $row['plat']);
$pdf->Cell(0,0, $row['lot']);
//End Which table did this come from
// Start check to see if buiding is owneroccupied
$boo = '';
$query = "SELECT * FROM units INNER JOIN inspection ON inspection.iid = units.iid INNER JOIN building ON building.bid = inspection.bid WHERE units.iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$under6 = mysql_result($result, 0, 'under6');
 while ($row = mysql_fetch_row($result)) {
	$isbuildingowneroccupied = $row['owneroccupied'];
	if ($isbuildingowneroccupied == 'Yes'){
	$boo = 'Yes';
	}
}
// End almost check to see if buiding is owneroccupied
//  More owner occupied
$owneroccupied = mysql_result($result, 0, 'owneroccupied');
$pdf->SetLineWidth(0.014);

//  Need to identify if building owner occupied
/*
if($owneroccupied == 'Yes') {
$pdf->Rect(2.61, $y+$yshift, 0.13, 0.14);
} else { // No
$pdf->Rect(2.46, $y+$yshift, 0.13, 0.14);
}		
// Start Place yes/no box
if($boo == 'Yes'){		
		$pdf->Rect(4.65, $y+$yshift, 0.13, 0.14);
} else {$pdf->Rect(4.8, $y+$yshift, 0.13, 0.14);}		

if($row['$owneroccupied'] == 'Yes') {		
$pdf->Rect(4.65, $y+$yshift, 0.13, 0.14);		
            $pdf->Rect(2.46, $y+$yshift, 0.13, 0.14);		
} else  {if($row['$owneroccupied'] == 'No') {		
$pdf->Rect(4.8, $y+$yshift, 0.13, 0.14);		
}
// End almost check to see if buiding is owneroccupied
//OWNER		
//Start Need help understanding this quesry		
$query = "SELECT client.cid, firstname, lastname, streetnum, address, suffix, city, state, zip,startdate FROM client INNER JOIN owners USING (cid) WHERE bid=$bid AND startdate <= DATE(NOW()) AND enddate >= DATE(NOW()) LIMIT 1";		
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());		
if ($row = mysql_fetch_assoc($result)) {		
            formfix($row);
			$clearanceDate = $row['startdate'];		
            $cid = $row['cid'];		
		$startdate=$row['startdate'];
            $basex = 0.5; $basey = 3.8;		
 		
$pdf->SetXY($basex, $basey);		
$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
$pdf->SetXY($basex + 4.0, $basey);
$pdf->Cell(0,0, "$row[streetnum] $row[address] $row[suffix]");
$pdf->SetXY($basex, $basey + 0.48);
$pdf->Cell(0,0, "$row[city], $row[state] $row[zip]");
 
            $query = "SELECT type, number, ext FROM client_phone WHERE cid=$cid GROUP BY type";
            $result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                        formfix($row);
                        if ($row['ext']) {
                                    $nums[$row['type']] = "$row[number] ext. $row[ext]";
                        } else {
                                    $nums[$row['type']] = "$row[number]";
                        }
            }

 

$pdf->SetXY($basex + 3.7, $basey + 0.48);
            if ($nums['Home']) {
$pdf->Cell(0,0, $nums['Home']);
            } else if ($nums['Cell']) {
$pdf->Cell(0,0, $nums['Cell'] . ' (Cell)');

                        $cellused = 1;

            }

$pdf->SetXY($basex + 5.75, $basey + 0.48);

            if ($nums['Work']) {

$pdf->Cell(0,0, $nums['Work']);

            } else if ($nums['Cell'] && $cellused < 1) {

$pdf->Cell(0,0, $nums['Cell'] . ' (Cell)');

            }

}
//End Need help understanding this quesry		

//print $query;

//$startdate = mysql_result($result, 0, 'startdate'); //used in inspector section

//INSPECTORS
$query = "SELECT firstname, lastname, elt, eli, units.starttime > elistart AS useeli FROM inspector INNER JOIN insp_assigned USING ( tid ) INNER JOIN units USING ( iid ) WHERE cuid =$cuid ORDER BY eli DESC  LIMIT 0 , 30 ";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$pdf->SetXY($basex + 5.75, $y + $ydrop);
$pdf->Cell(0,0, $starttime); //retreived in address section"
while (($row = mysql_fetch_assoc($result)) && $x < 4) {
            formfix($row);
$pdf->SetXY($x, $y + $ydrop);
$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
$pdf->SetXY($x + 3.61, $y + $ydrop);
            if ($row['eli'] && $row['useeli']) {
$pdf->Cell(0,0, "ELI- $row[eli]");
            } else {
$pdf->Cell(0,0, "ELT- $row[elt]");

            }


        $ydrop=$ydrop+0.48;

//          $x++;

}

//Rilt
$pdf->SetXY(6.15, $y+.5);
$pdf->SetFont('Times','B','12');
$pdf->Cell(0,0,'Inspecting Firm');
$pdf->SetXY(6.05, $y+.8);
$pdf->SetFont('Times','','11');
$pdf->Cell(0,0,'Rhode Island');
$pdf->SetXY(6.10, $y+1.0);
$pdf->Cell(0,0,'Lead Technicians, Inc');
$pdf->SetXY(6.15, $y+1.2);
$pdf->Cell(0,0,'120 Amaral Street');
$pdf->SetXY(6.20, $y+1.4);
$pdf->Cell(0,0,'East Providence, RI 02915');
//Rilt
 
//Start Reason
$query = "SELECT reason, otherreason FROM inspection WHERE iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());list($reason, $otherreason) = mysql_fetch_row($result);
$pdf->SetFont('Times','',12);
 
switch ($reason) {
            case 'Department of Health Initiated':
$pdf->SetXY(0.60, $y+.6);
                        break;
            case 'Licensed Child Care Facility':
$pdf->SetXY(0.60, $y+.75);
                        break;
            case 'School':
$pdf->SetXY(0.60, $y+0.9);
                        break;
            case 'Code Enforcement':
$pdf->SetXY(0.60, $y+1.05);
                        break;
            case 'Investigation of Improper Lead Hazard Reduction/Illegal Abatement':
$pdf->SetXY(3.71, $y+0.6);
                        break;
            case 'Private Client - Property Transfer':
$pdf->SetXY(3.71, $y+0.75);
                        break;
            case 'Dept. of Human Services Initiated':
$pdf->SetXY(3.71, $y+1.05);
                        break;
            default:
$pdf->SetXY(3.71, $y+0.9);
                       if ($reason == 'Other') {
                                   $other = $otherreason;
                       } else { //conformance reason
                                   $other = $reason;
                       }
                       break;
}
$pdf->Cell(0,0, 'X');

if ($other) {
$pdf->SetXY(4.43, $y+0.9);
$pdf->Cell(0,0, $other);
}
//End Reason
//Start Media
//PAINT
$query = "SELECT COUNT(*) FROM comprehensive_rooms INNER JOIN comprehensive_components USING (crid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$paint = mysql_result($result, 0);			
$query = "SELECT COUNT(*) FROM comprehensive_ext_components WHERE cuid=$cuid";			
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$paint += mysql_result($result, 0);			
$pdf->SetFont('Times','','12');			
$x=1.4;			
if ($paint) {			
             			
$pdf->SetXY($x,  $y);			
$pdf->Cell(0,0, 'X');			
			}
$pdf->SetXY($x+0.3,  $y);			
$pdf->Cell(0,0, 'Paint');			
$pdf->SetXY($x-0.12,  $y);			
$pdf->Cell(0,0, '_____');
 
//SOIL
$query = "SELECT u2.cuid FROM units AS u1 INNER JOIN units AS u2 USING (iid) WHERE u1.cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$extcuid = mysql_result($result, 0);
$query = "SELECT COUNT(*) FROM comprehensive_sides WHERE cuid=$extcuid AND inaccessible='No' AND cover='None' and timetaken <> '0000-00-00 00:00:00'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$soils = mysql_result($result, 0);
$query = "SELECT COUNT(*) FROM comprehensive_soil_debris WHERE cuid=$extcuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$soils += mysql_result($result, 0);			
$query = "SELECT COUNT(*) FROM comprehensive_soil_object WHERE cuid=$extcuid";			
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$soils += mysql_result($result, 0);			
			
//$soils = 1;			
$x=3.0;			
if ($soils) {			
             			
$pdf->SetXY($x,  $y);			
$pdf->Cell(0,0, 'X');			
			}
$pdf->SetXY($x+0.3,  $y);			
$pdf->Cell(0,0, 'Soil');			
$pdf->SetXY($x-0.12,  $y);			
$pdf->Cell(0,0, '_____');			

//WATER			
$query = "SELECT COUNT(*) FROM waters WHERE cuid=$cuid AND iid=$iid"; //iid unnecessary?"			
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$waters = mysql_result($result, 0);			
$x=4.56;			
if ($waters) {			
             			
$pdf->SetXY($x,  $y);			
$pdf->Cell(0,0, 'X');			
			}
$pdf->SetXY($x+0.3,  $y);			
$pdf->Cell(0,0, 'Water');			
$pdf->SetXY($x-0.12,  $y);			
$pdf->Cell(0,0, '_____');			


//DUST			
$query = "SELECT COUNT(*) FROM comprehensive_wipes INNER JOIN comprehensive_rooms USING(crid) WHERE cuid=$cuid AND iid=$iid";	
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());$dust = mysql_result($result, 0);			
$x=6.38;			
if ($dust) {			
            			
$pdf->SetXY($x,  $y);			
$pdf->Cell(0,0, 'X');			
			}
$pdf->SetXY($x+0.3,  $y);			
$pdf->Cell(0,0, 'Dust');			
$pdf->SetXY($x-0.12,  $y);			
$pdf->Cell(0,0, '_____');			
}
//End Media 
$y=$y+0.3;
$pdf->SetXY(0.4,$y);
$pdf->Cell(0,0,'FORM PBLC-23-1 (7/05) Replaces FORM PBLC-23-1 (1/03) p.1, which is obsolete.');
*/

?>
