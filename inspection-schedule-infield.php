<?php 

require_once'session.php';
require_once'connect.php';

$title = "In-field Inspection";
require_once'header.php';
?>

<form action="inspection-schedule-save.php" method="post">
<table>
<tr><td>Inspection ID:</td><td><input type="text" name="iid" size="5" /></td></tr>
<tr><td>Units being inspected:</td><td><input type="text" name="numunits" maxlength="3" size="4" value="<?php print $row[numunits]; ?>" /></td></tr>
<tr><td>Inspection type:</td><td><input type="radio" name="type" value="Conformance" /> Conformance  <input type="radio" name="type" value="Comprehensive" /> Comprehensive</td></tr>
</table>
<p><input type="submit" value="Save" /> <input type="reset" value="Reset" /></p>
</form>

<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>