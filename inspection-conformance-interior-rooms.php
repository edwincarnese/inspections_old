<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;
$error = $_GET['error'];

$query = "SELECT iid, starttime, unitdesc, number FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}

list($iid, $starttime, $unitdesc, $unitnumber) = mysql_fetch_row($result);

$query = "SELECT * FROM conformance_rooms WHERE cuid=$cuid ORDER BY number";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	//switch to setup page
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-conformance-interior-setup1.php?cuid=$cuid");
	exit();
}

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - $unitdesc - Room Inspection";

//require_once'header.php'; --> need to adjust header info on this page.  ?>
<?php 
print '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript" language="javascript" src="all.js">
startList = function() {
if (document.all&&document.getElementById) {
navRoot = document.getElementById("nav");
for (i=0; i<navRoot.childNodes.length; i++) {
node = navRoot.childNodes[i];
if (node.nodeName=="LI") {
node.onmouseover=function() {
this.className+=" over";
  }
  node.onmouseout=function() {
  this.className=this.className.replace�
	(" over", "");
   }
   }
  }
 }
}
window.onload=startList;
window.onunload=document.form.submit;
</script>
<?php if ($title) {
	print "<title>RI Lead Techs - $title</title>\n";
} else {
	print "<title>RI Lead Techs</title>\n";
}
?>
</head>

<body onunload="form.submit()">
	<div id="main">
		<div id="navbar">
			<table id="navtable">
				<tr>
				<td>
					<ul id="nav"> 
						<li><a href="#">Main Menu</a>  
					      <ul> 
					      	<li><a href="index.php">Home</a></li>
					        <li><a href="class-view-schedule.php">View Class Schedule</a></li>
					        <li><a href="dump-list.php">Data Dumps</a></li>
					        <li><a href="addressconvert.php">Address Converting</a></li>
					        <li><a href="inspection-status.php">Inspection Status</a></li>
					        <?
					        if (isset($_SESSION['sid'])) {
						        require_once 'connect.php';
						        $headerquery = "SELECT userlevel FROM users WHERE uid=$_SESSION[sid]";
								$headerresult = mysql_query($headerquery) or sql_crapout($headerquery."<br>".mysql_error());
								$headeruserlevel = mysql_result($headerresult, 0);
								if ($headeruserlevel >=5) {
									print "<li><a href='sync-control.php'>Sync Control</a></li>";
								}
					        }
							?>
					      </ul> 
					    </li> 
					</ul>
				</td>
				<td>
					<ul>
						<li><a href="#">Search/Lists</a>
						 <ul>
						 	<li><a href="client-list.php">People</a></li>
						 	<li><a href="building-list.php">Buildings</a></li>
						 	<li><a href="inspection-search.php">Inspections</a></li>
						 	<li><a href="inspector-search.php">Inspectors</a></li>
						 </ul>
						</li>
					</ul>
				</td>
				<?/*
				<td><a href="client-list.php">People</a></td>
				<td><a href="building-search.php">Buildings</a></td>
				<td><a href="inspector-search.php">Inspectors</a></td>*/?>
				<td><ul><li><a href="labresults-list.php">Lab Input</a></li></ul></td>
				<?//<td><a href="inspection-search.php">Inspections</a></td>?>
				<td><ul><li><a href="dump-list.php">Reports</a></li></ul></td>
				<td><ul><li><a href="session.php?logout=true">Logout</a></li></ul></td>
				
				<?/*<td>Comments</td>
				<td>Inspector</td>
				<td>Print Page</td>*/?>
				
				</tr>
			</table>
		</div>
		<div id="content">
	
<?php if ($title && !$droptitle) {
	print "<h1>$title</h1>\n";
}
?>
<?
if ($error == 1) {
	print "<table width='550'><tr><td><font size='+2' color='red'>If you chose 'Yes' for hazards then you must enter a hazard area for that room or switch it to no.</font></td></tr></table>";
} else if ($error == 2) {
	print "<table width='550'><tr><td><font size='+2' color='red'>If you chose 'No' for hazards then you must deselect all hazard areas for that room.</font></td></tr></table>";
}
?>
<form action="inspection-conformance-interior-save.php" method="post">
<input type="hidden" name="cuid" value="<?php print $cuid; ?>" />
<table class="info">
<tr><th>Room #</th><th>Room Name</th><th>Paint Hazards</th><th>Hazard Areas</th><th>Delete</th></tr>
<?php

$hazards = array('Yes', 'No');
$hazardareas = array('Ceiling','Walls','Trim','Floor','Window','Door','Cabinet'); //Other is seperate

while ($row = mysql_fetch_assoc($result)) {
	print "<tr><td>$row[number]</td><td>$row[name]</td>";
	print "<td>";
	foreach ($hazards as $hazard) {
		if ($row['hazards'] == $hazard) {
			print "<input type=\"radio\" name=\"data[$row[crid]][hazards]\" value=\"$hazard\"  checked=\"checked\" />$hazard&nbsp;";
		} else {
			print "<input type=\"radio\" name=\"data[$row[crid]][hazards]\" value=\"$hazard\" />$hazard&nbsp;";
		}
	}
	print "</td>";
	print "<td>";
	print "<table>";
	$savedhazardareas = explode(',', $row['hazardareas']);
	$numitems = 0;
	$columns = 4;
	foreach ($hazardareas as $hazard) {
		if ($numitems % $columns == 0) {
			print "<tr>";
		}
		if (in_array($hazard, $savedhazardareas)) {
			print "<td><input type=\"checkbox\" name=\"data[$row[crid]][hazardareas][]\" value=\"$hazard\" checked=\"checked\" />$hazard</td>";
		} else {
			print "<td><input type=\"checkbox\" name=\"data[$row[crid]][hazardareas][]\" value=\"$hazard\" />$hazard</td>";
		}
		$numitems++;
		if ($numitems % $columns == 0) {
			print "</tr>\n";
		}
	}
	if ($numitems % $columns) {
		while ($numitems % $columns) {
			print "<td> </td>";
			$numitems++;
		}
		print "</tr>";
	}
	print "<tr>";
	if (in_array('Other', $savedhazardareas)) {
		print "<td colspan=\"$columns\"><input type=\"checkbox\" name=\"data[$row[crid]][hazardareas][]\" value=\"Other\" checked=\"checked\" />Other <input type=\"text\" name=\"data[$row[crid]][otherhazards]\" value=\"$row[otherhazards]\" maxlength=\"100\" /></td>";
	} else {
		print "<td colspan=\"$columns\"><input type=\"checkbox\" name=\"data[$row[crid]][hazardareas][]\" value=\"Other\" />Other <input type=\"text\" name=\"data[$row[crid]][otherhazards]\" value=\"$row[otherhazards]\" maxlength=\"100\" /></td>";
	}
	print "</table></td><td><a href=\"inspection-conformance-deleteroom.php?crid=$row[crid]&amp;cuid=$cuid&del=false\" class=\"red\">Delete</a></td></tr>";
}
?>
</table>
<p><input type="submit" name="submit" value="Save" /> <input type="submit" name="submit" value="Add New Room" /><input type="reset" value="Reset" /> <input type="submit" name="submit" value="Cancel" /></p>
</form>

<p><a href="inspection-view.php?iid=<?php print $iid; ?>">Inspection Main Menu</a></p>
<?php
require_once'footer.php';
?>