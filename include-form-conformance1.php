<?php

$pdf->AddPage('P');

$pdf->SetFont('Arial','',12);

require_once'include-form-conformance1-form.php';

$query = "SELECT building.bid, streetnum, address, suffix, city, state, zip, numunits,designation, insptype, yearbuilt, plat, lot, inspection.iid, unitdesc, number, units.cuid FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

$iid = $row['iid'];
$bid = $row['bid'];
$cuid = $row['cuid'];
$reason = $row['reason'];
// 10/19/6 forcing reason.  remove when the reason field is put in the units table.
$reason = 'Independent Clearance Inspection';
$lh = 0.4;
$pdf->SetFont('Arial','','10');

$pdf->SetXY(1.75, 0.15);
$pdf->Cell(0,0, "Inspection $row[iid]-$row[number]");

// x - 0.03; y - 0.06 from line start

//$pdf->SetXY(1.42, 1.51);
//$pdf->Cell(0, 0, 'X');
$pdf->SetFont('Times','','10');		
$x=0.6;		
if ($row['insptype']=='Conformance' or $row['insptype']=='Conformance Clearance') {		
	$pdf->SetXY(1.42, 1.51);		
	$pdf->Cell(0,0, 'X');		
}
if ($row['insptype']=='Presumptive Compliance') {		
	$pdf->SetXY(4.47, 1.51);		
	$pdf->Cell(0,0, 'X');		
}

//$row[type]
//ADDRESS
$pdf->SetXY(1.50, 2.26);
$pdf->Cell(0, 0, "$row[streetnum] $row[address] $row[suffix]");
$pdf->SetXY(4.72, 2.26);
$pdf->Cell(0,0, $row['city'].', '.$row['state']);
$pdf->SetXY(6.95, 2.26);
$pdf->Cell(0,0, $row['zip']);
$pdf->SetXY(1.53, 2.6);
$pdf->Cell(0,0, $row['numunits']);
$pdf->SetXY(2.90, 2.43);
$pdf->Cell(0,0, $row['unitdesc']);
$pdf->SetXY(3.26, 2.6);
$pdf->Cell(0,0, $row['designation']);
$pdf->SetXY(4.72, 2.6);
//add colon separator if both
if ($row['plat'] && $row['lot']) {
	$pdf->Cell(0,0, $row['plat'].' / '.$row['lot']);
} else {
	$pdf->Cell(0,0, $row['plat'].$row['lot']);
}
$pdf->SetXY(6.95, 2.6);
$pdf->Cell(0,0, $row['yearbuilt']);

//OWNER
$query = "SELECT client.cid, firstname, lastname, company, streetnum, address, suffix, city, state, zip FROM client INNER JOIN owners USING (cid) WHERE bid=$bid AND startdate <= DATE(NOW()) AND enddate >= DATE(NOW())";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) {
	exit("Building requires owner. Form blows up without owner.");
}
$row = mysql_fetch_assoc($result);
formfix($row);
$cid = $row['cid'];

$basex = 1.64; $basey = 3.28;

$pdf->SetXY($basex, $basey);
if ($row['firstname']=='' or $row['lastname']=='') {
	$pdf->Cell(0,0,"$row[company]");
}
else {
	$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
}
$pdf->SetXY($basex + 3.5, $basey);
$pdf->Cell(0,0, "$row[streetnum] $row[address] $row[suffix]");
$pdf->SetXY($basex, $basey + 0.2);
$pdf->Cell(0,0, "$row[city], $row[state]");
$pdf->SetXY($basex + 3.5, $basey + 0.18);
$pdf->Cell(0,0, "$row[zip]");

$query = "SELECT type, number, ext FROM client_phone WHERE cid=$cid GROUP BY type";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while ($row = mysql_fetch_assoc($result)) {
	formfix($row);
	if ($row['ext']) {
		$nums[$row['type']] = "$row[number] ext. $row[ext]";
	} else {
		$nums[$row['type']] = "$row[number]";
	}
}

$pdf->SetXY($basex, $basey + 0.39);
if ($nums['Home']) {
	$pdf->Cell(0,0, $nums['Home']);
} else if ($nums['Cell']) {
	$pdf->Cell(0,0, $nums['Cell'] . ' (Cell)');
	$cellused = 1;
}
$pdf->SetXY($basex + 3.5, $basey + 0.39);
if ($nums['Work']) {
	$pdf->Cell(0,0, $nums['Work']);
} else if ($nums['Cell'] && $cellused < 1) {
	$pdf->Cell(0,0, $nums['Cell'] . ' (Cell)');
}

//TENANT
$query = "SELECT tenant, tenantyears, under6, reason, otherreason FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);
$reason=$row['reason'];
$otherreason=$row['otherreason'];

$pdf->SetXY(1.25, 4.46);
$pdf->Cell(0,0, $row['tenant']);
$pdf->SetXY(2.75, 4.80);
$pdf->Cell(0,0, $row['tenantyears']);
if ($row['under6']) {
	$pdf->SetXY(5.90, 4.46);
} else {
	$pdf->SetXY(6.90, 4.46);
}
$pdf->Cell(0,0, 'X');

//INSPECTORS
$query = "SELECT firstname, lastname, elt, eli FROM inspector INNER JOIN insp_assigned USING (tid) INNER JOIN unit_insp_assigned USING (assig_id) WHERE iid=$iid AND unit_insp_assigned.cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$x = $mody = 0;
$basex = 3.05; $basey = 5.52;
$pdf->SetFont('Arial','',12);
while (($row = mysql_fetch_assoc($result)) && $x < 2) {
	formfix($row);
	if ($row['eli']) {
		$pdf->SetXY($basex, $basey + $mody);
		$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
		$pdf->SetXY($basex + 2.88, $basey+$mody);
		$pdf->Cell(0,0, "ELI-$row[eli]");
	} else {
		if ($x == 0) {
			$mody += 0.7;
			$x++;
		}
		$pdf->SetXY($basex, $basey + $mody);
		$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
		$pdf->SetXY($basex + 2.88, $basey+$mody);
		$pdf->Cell(0,0, "ELT-$row[elt]");
	}
	$mody += 0.7;
	$x++;
}

while ($x < 2) {
	$mody += 0.7; //bump down if no second inspector for time
	$x++;
}

$mody -= 0.07; //whatever
$query = "SELECT DATE_FORMAT(starttime, '%c/%e/%Y') AS inspdate, DATE_FORMAT(starttime, '%l:%i %p') AS insptime FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
$pdf->SetXY($basex, $basey + $mody);
$pdf->Cell(0,0, "$row[inspdate]");
$pdf->SetXY($basex + 2.90, $basey + $mody);
$pdf->Cell(0,0, "$row[insptime]");

switch ($reason) {
	case 'Independent Clearance Inspection':
		$pdf->SetXY(0.61, 7.67);
		break;
	case 'Tenant Complaint':
		$pdf->SetXY(0.61, 7.96);
		break;
	case 'Presumptive Compliance':
		$pdf->SetXY(0.61, 8.22);
		break;
	case 'Code Enforcement':
		$pdf->SetXY(0.61, 8.49);
		break;
	case 'Visual Inspection':
		$pdf->SetXY(3.68, 7.67);
		break;
	case 'Private Client - Property Transfer':
		$pdf->SetXY(3.68, 7.96);
		break;
	case 'Private Client':
		$pdf->SetXY(3.68, 8.22);
		break;
	default:
		$pdf->SetXY(4.54, 8.49);
		$pdf->Cell(0,0, $otherreason);
		$pdf->SetXY(3.685, 8.47);
		break;
}
$pdf->Cell(0,0, 'X');

//PAGE NUMBERS
$y=10.6;
$pdf->SetFont('Times','','10');
$pdf->SetXY(7.2, $y);
$pdf->Cell(0,0, 'Page     of');
$pdf->SetXY(7.525, $y);
$pdf->Cell(0, 0, $pdf->PageNo());
$pdf->SetXY(7.85, $y);
$pdf->Cell(0,0, '{totalpages}');


?>