<?php


$dots = "...................................................................";


$data = "
<div style='width: 100%'>
<h3 style='text-align:center;margin-bottom:30px'>TABLE OF CONTENTS</h3>
<table style='width: 100%' >
	<tr>
		<td style='width:300px' class='title'>Introduction and Executive Summary</td><td style='width:300px'>$dots</td><td style='margin-right:80px;width:150px;text-align:center'>2</td>
	</tr>
	<tr>
		<td style='width:300px' class='title'>What is the difference between an inspection and risk assessment?</td><td style='width:300px'>$dots</td><td style='margin-right:80px;width:150px;text-align:center'>2</td>
	</tr>
	<tr>
		<td style='width:300px' class='title'>EPA §745.107 Disclosure Requirements for Sellers and Lessors</td><td style='width:300px'>$dots</td><td style='margin-right:80px;width:150px;text-align:center'>3</td>
	</tr>
	<tr>
		<td style='width:300px' class='title'>VISUAL ASSESSMENT SUMMARY</td><td style='width:300px'>$dots</td><td style='margin-right:80px;width:150px;text-align:center'>4</td>
	</tr>
	<tr>
		<td style='width:300px' class='title'>XRF Testing Results and Lead-Based Paint Hazards</td><td style='width:300px'>$dots</td><td style='margin-right:80px;width:150px;text-align:center'>5</td>
	</tr>
	<tr>
		<td style='width:300px' class='title'>New Hampshire defines a lead exposure hazard as:</td><td style='width:300px'>$dots</td><td style='margin-right:80px;width:150px;text-align:center'>5</td>
	</tr>
	<tr>
		<td style='width:300px' class='title'>Explanation of Lead Inspection Risk Assessment Report Form Columns:</td><td style='width:300px'>$dots</td><td style='margin-right:80px;width:150px;text-align:center'>6</td>
	</tr>
	<tr>
		<td style='width:300px' class='title'>Conditions and Limitations:</td><td style='width:300px'>$dots</td><td style='margin-right:80px;width:150px;text-align:center'>8</td>
	</tr>
	<tr>
		<td style='width:300px' class='title'>New Hampshire Lead-Safe Requirements:</td><td style='width:300px'>$dots</td><td style='margin-right:80px;width:150px;text-align:center'>9</td>
	</tr>
	<tr>
		<td style='width:300px' class='title'>He-P 1609.02 Abatement Methods:</td><td style='width:300px'>$dots</td><td style='margin-right:80px;width:150px;text-align:center'>9</td>
	</tr>
	<tr>
		<td style='width:300px' class='title'>PART He-P 1610 STANDARDS FOR INTERIM CONTROLS:</td><td style='width:300px'>$dots</td><td style='margin-right:80px;width:150px;text-align:center'>13</td>
	</tr>
	<tr>
		<td style='width:300px' class='title'>He-P 1609.03 Encapsulant Products and Their Use:</td><td style='width:300px'>$dots</td><td style='margin-right:80px;width:150px;text-align:center'>13</td>
	</tr>
	<tr>
		<td style='width:300px' class='title'>Testing Protocols:</td><td style='width:300px'>$dots</td><td style='margin-right:80px;width:150px;text-align:center'>18</td>
	</tr>
	<tr>
		<td style='width:300px' class='title'>Calibration</td><td style='width:300px'>$dots</td><td style='margin-right:80px;width:150px;text-align:center'>19</td>
	</tr>
	<tr>
		<td style='width:300px' class='title'>Testing</td><td style='width:300px'>$dots</td><td style='margin-right:80px;width:150px;text-align:center'>19</td>
	</tr>
	<tr>
		<td style='width:300px' class='title'>Testing Calibration</td><td style='width:300px'>$dots</td><td style='margin-right:80px;width:150px;text-align:center'>19</td>
	</tr>
	<tr>
		<td style='width:300px' class='title'>Walls</td><td style='width:300px'>$dots</td><td style='margin-right:80px;width:150px;text-align:center'>20</td>
	</tr>
	<tr>
		<td style='width:300px' class='title'>Windows System</td><td style='width:300px'>$dots</td><td style='margin-right:80px;width:150px;text-align:center'>20</td>
	</tr>
</table>
</div>
";




return $data;

?>

