
<?php

$data = "
	
<p style='margin-left:100px;'>b It is raining in such a manner as to create surface run-off of contaminated soil; and</p>

<p style='margin-left:70px;'>(3) All contaminated soil shall be disposed of in accordance with He-P 1608.11(e) and (f)</p>

<p style='margin-left:250px;'><a href='http://www.gencourt.state.nh.us/rules/state_agencies/sourcehe-p.html'>Source.</a> #5920, eff 1-1-95; amd by #6096, eff 9-22-
95; ss by #7181, eff 12-24-99; ss by #7495, eff 5-23-
01; ss by #8039, eff 2-13-04; ss by #8932, eff 7-6-07;
ss by #9986, eff 9-1-11 (from He-P 1605.08)</p>

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px; margin-top:40px;'> PART He-P 1610 STANDARDS FOR INTERIM CONTROLS </p>

<p style='font-weight:bold;'>He-P 1610.01 Interim Control Standards<</p>

<p style='margin-left:50px;'>(a) He-P 1610 shall apply to any owner or person who has been issued an order of lead
hazard reduction and who has chosen to utilize interim controls as a means of
temporarily reducing lead exposure hazards on a dwelling, dwelling unit or child care
facility.</p>

<p style='margin-left:50px;'>(b) All interim control activities shall be conducted in accordance with RSA 130-A and He-P 1600.</p>

<p style='margin-left:50px;'>(c) An owner, owner-contractor, licensed lead abatement contractor, licensed lead
inspector or licensed risk assessor shall request permission in writing from the
commissioner to use interim controls as an alternative to abatement by submitting a
“Request for Use of Interim Controls” form (January 2011 edition) and a copy of the
LEHRP as described He-P 1608.05 to the department at least 5 business days prior to
the anticipated start date of lead hazard reduction work.</p>

<p style='margin-left:50px;'>(d) Interim control work shall not be conducted until written approval is received from the department.</p>

<p style='margin-left:50px;'>(e) The commissioner shall deny a request to use interim controls when it is found that:</p>

<p style='margin-left:70px;'>(1) The person making the request has been found in violation of one or more provisions of RSA 130-A and He-P 1600; or</p>

<p style='margin-left:70px;'>(2) The requested plan does not satisfy the intent of the rules as an alternative to complying with the rules.</p>

<p style='margin-left:50px;'>(f) The commissioner shall revoke permission for the use of interim controls and order the owner to abate all lead exposure hazards when it is found that:</p>

<p style='margin-left:70px;'>(1) The owner has failed to maintain a current certificate of compliance – interim controls; or</p>

<p style='margin-left:70px;'>(2) When a compliance inspection conducted in accordance with He-P 1605.04
reveals that the property with a certificate of compliance – interim controls no longer
meets the requirements of He-P 1600.</p>

<p style='margin-left:250px;'><a href='http://www.gencourt.state.nh.us/rules/state_agencies/sourcehe-p.html'>Source.</a> #5920, eff 1-1-95; ss by #7181, eff 12-24-99; ss by
#7495, eff 5-23-01; ss by #8039, eff 2-13-04; ss by #8932,
eff 7-6-07; ss by #9986, eff 9-1-11 (from He-P 1606.02)</p>

<p style='font-weight:bold;'>
He-P 1609.03 Encapsulant Products and Their Use
</p>

<p style='margin-left:50px;'>(a) Encapsulant products shall be approved in accordance with RSA 130-A:1, VII, or (p) and (q) below prior to their use.</p>

<p style='margin-left:50px;'>(b) Except for a licensed lead abatement contractor or owner-contractor, any person who
wishes to use an encapsulant product shall request permission from the department in
writing prior to initiating the work or activity and include a copy of the LEHRP describing
the components that encapsulant products are requested to be used on.</p>

<p style='margin-left:50px;'>(c) Encapsulant products shall be applied:</p>

<p style='margin-left:80px;'>(1) After passing substrate assessment testing using the “Pull-Off Tape Test for
Adhesion” or the “Assessment of Painted Surfaces for Adhesion” (ASTM E 1796-
03), for each architectural system, element or building component where an
encapsulant product is to be used;</p>

<p style='margin-left:80px;'>(2) Only after all surface preparation, and any other phases of lead hazard reduction work, including painting, component removal or both, is complete;</p>

<p style='margin-left:80px;'>(3) In accordance with the manufacturer’s criteria; and</p>

<p style='margin-left:80px;'>(4) In accordance with ASTM E 1796-03 Standard Guide for Selection and Use of Liquid Coating Encapsulation Products for Leaded Paint in Buildings.</p>

<p style='margin-left:50px;'>(d) Encapsulant products shall not be used on any surface(s) that:</p>

<p style='margin-left:80px;'>(1) Fails the substrate assessments tests such as the “Pull-Off Tape Test for Adhesion” or the “Assessment of Painted Surfaces for Adhesion” (ASTM E 1796-03); or</p>

<p style='margin-left:80px;'>(2) Is not recommended for encapsulation or restricted by the product
manufacturer.</p>

<p style='margin-left:50px;'>(e) Surface preparation as described in (c)(2) shall include:</p>

<p style='margin-left:80px;'>(1) Cleaning and deglossing with a strong detergent or similar deglossing agent or by wet sanding, if necessary;</p>

<p style='margin-left:80px;'>(2) Making minor repairs such as filling holes with plaster or spackling; and</p>

<p style='margin-left:80px;'>(3) Paint stabilization of the interior, exterior or both, as described in He-P 1610.02, as required.</p>

<p style='margin-left:50px;'>(f) All encapsulant debris generated through the application process and any unused
encapsulant not suitable for application shall be disposed of in accordance with the
encapsulant manufacturer’s instructions.</p>

<p style='margin-left:50px;'>(g) When encapsulant products have been used and the dwelling, dwelling unit or child
care facility has no documentation of passing the substrate assessment or the substrate
assessment test has been failed for any architectural system, element or building
component, the risk assessor shall not issue a certificate of compliance – abatement.</p>

<p style='margin-left:50px;'>(h) The owner shall perform a visual inspection of the encapsulated surfaces as recommended by the manufacturer and as follows:</p>

<p style='margin-left:80px;'>(1) 30 days after application;</p>

<p style='margin-left:80px;'>(2) 6 months after application;</p>

<p style='margin-left:80px;'>(3) Annually thereafter; and</p>

<p style='margin-left:80px;'>(4) Whenever there is a change in tenant occupancy.</p>

<p style='margin-left:50px;'>(i) The visual inspection required by (h) above shall determine whether the encapsulant has maintained its integrity and is not:</p>

<p style='margin-left:80px;'>(1) Cracked;</p>

<p style='margin-left:80px;'>(2) Peeling;</p>

<p style='margin-left:80px;'>(3) Sagging;</p>

<p style='margin-left:80px;'>(4) Bubbling;</p>

<p style='margin-left:80px;'>(5) Water damaged or evidencing other moisture related problems;</p>

";
return $data;

?>

