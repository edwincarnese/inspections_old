

<?php

$data = "
<p style='text-indent:50px;'>
*Please note: Accessible building components were tested to determine the presence of
lead based paint. Each surface identified to be a potential lead exposure hazard by the visual
inspection and having a distinct paint history, was tested for the presence of lead. Please refer to
the property specific cover sheet for an explanation of the abbreviations used in your inspection
and the property field notes for the actual testing results for the inspection.
</p>

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:40px;'> XRF Testing Results and Lead-Based Paint Hazards </p>

<p style='text-indent:50px;'>XRF readings of 1.0mg/cm² or greater are considered to be lead based paint in accordance
with the U.S. Environmental Protection Agency (EPA), Department of Housing and Urban
Development (HUD) and the State of New Hampshire Department of Health and Human Services.
These are indicated in <span style='background:yellow; font-weight:bold; color:black;'>yellow</span> on this report.</p>

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px; margin-top:40px;'> New Hampshire defines a lead exposure hazard as: </p>

<p style='margin-left:50px;'>(a) The presence of lead base substances on chewable, accessible, horizontal surfaces
that protrude more than ½ inch and are located more than 6 inches but less than 4
feet from the floor or ground;</p>

<p style='margin-left:50px;'>(b) Lead based substances which are peeling, chipping, chalking or cracking or any paint
located on an interior or exterior surface or fixture that is damaged or deteriorated
and is likely to become accessible to a child;</p>

<p style='margin-left:50px;'>(c) Lead based substances on interior or exterior surfaces that are subject to abrasion or
friction or subject to damage by repeated impact; or</p>

<p style='margin-left:50px;'>(d) Bare soil in play areas or the rest of the yard that contains lead in concentration equal
to or greater than the limits defined in RSA130-A:1, These hazards will be indicated in
<span style='background:red; font-weight:bold; color:black;'>red</span> on this report.</p>

<br/>

<p>NOTE: The report presented in the property specific field notes lists the results for all surfaces
identified as part of the lead inspection. This includes surfaces that are not lead paint, as well as
surfaces that contain lead paint, but are not hazards at the time of the inspection. All surfaces
which contain a dangerous level of lead in paint and are not lead hazards at the time of the lead
inspection, may become a hazard if their condition or use changes. These surfaces should be
checked periodically to ensure that they do not become lead hazards (yellow).</p









";
return $data;

?>

