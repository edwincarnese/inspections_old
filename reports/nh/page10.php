
<?php

$data = "
	
	<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px; margin-top:40px;'> New Hampshire Lead-Safe Requirements </p>

	<p style='text-indent:50px'>Lead exposure hazards may be controlled either by Lead Abatement or Interim Control.
	Please refer to New Hampshire Code of Administrative Rules Chapter He-P 1600 Lead Poisoning
	Prevention and Control Rules provided below for specific information and requirements.</p>

	<p style='margin-left:30px; font-weight:bold;'>He-P 1609.02 Abatement Methods</p>

	<p style='margin-left:50px;'>(a) One or more of the following lead hazard reduction techniques shall be used on lead-
	based painted surfaces to meet abatement standards:</p>

	<p style='margin-left:70px;'>(1) Removal of lead-based paint by:</p>

	<p style='margin-left:100px;'>a. Removal and replacement of any component with a component that is free of lead-based substances; or</p>

	<p style='margin-left:100px;'>b. Removal of the surface coating down to the substrate by:</p>

	<p style='margin-left:120px;'>1. Wet sanding;</p>
	<p style='margin-left:120px;'>2. Utilizing of non-flammable chemical strippers, which do not contain methylene chloride;</p>
	<p style='margin-left:120px;'>3. Removing the lead containing component for off-site stripping and then reinstalling;</p>
	<p style='margin-left:120px;'>4. Scraping with the aid of a caustic paint remover;</p>
	<p style='margin-left:120px;'>5. Misting the surface with water and wet scraping;</p>
	<p style='margin-left:120px;'>6. Controlled low-level heating element, which produces a temperature no greater than 700 degrees Fahrenheit;</p>
	<p style='margin-left:120px;'>7. Machine sanding using a sander equipped with a HEPA local vacuum
	exhaust sized to match the tool to feather edges and prepare substrate for
	repainting or sealing;</p>


<p style='margin-left:120px;'>8. Machine planing using a planing tool equipped with a HEPA local vacuum exhaust sized to match the tool;</p>
<p style='margin-left:120px;'>9. Abrasive blasting using a HEPA local vacuum exhaust sized to match the tool;</p>
<p style='margin-left:120px;'>10. Dry scraping within 6 inches of an area that would present an electrical hazard if other methods were used; or</p>
<p style='margin-left:120px;'>11. Any other method approved by the department through a variance in
accordance with He-P 1605.03;</p>

<p style='margin-left:70px;'>(2) Application of an encapsulant product that is:</p>
<p style='margin-left:100px;'>a. Approved in accordance with He-P 1609.03(a);</p>
<p style='margin-left:100px;'>b. Used only on those surfaces specified by the manufacturer; and</p>
<p style='margin-left:100px;'>c. Applied in accordance with the manufacturer’s instructions;</p>


<p style='margin-left:70px;'>(3) Enclosure of the surface so that no lead containing surface remains by:</p>

<p style='margin-left:100px;'>a. First labeling the surface to be enclosed with a warning, “Danger: Lead-Based
Paint” written in permanent ink with lettering no less than one inch high
horizontally and vertically approximately every 16 square feet on large
components, such as walls and floors, and every 4 linear feet on small
components, such as baseboards;</p>

<p style='margin-left:100px;'>b. Securely fastening and affixing all junctions of floors, walls, ceilings, and other
joined surfaces by fastening with nails, screws or an adhesive recommended by
the manufacturer for the covering so that the covering remains in place and the
physical integrity of the covering remains intact to prevent removal, and then
caulking and sealing all seams;</p>

<p style='margin-left:100px;'>c. Covering floor surfaces with wall to wall carpeting, vinyl flooring, ceramic tile,
wood or stone, or similar durable material intended for use as flooring;</p>

<p style='margin-left:100px;'>d. Covering all other interior surfaces with wood, vinyl, aluminum, plastic or
similar durable materials, except that vinyl wallpaper and plastic sheeting shall
not be allowed;</p>

<p style='margin-left:100px;'>e. Covering walls or ceiling surfaces with gypsum board, fiberglass mats, vinyl
wall coverings, formica, tile, paneling or other material that does not tear, chip
or peel;</p>

<p style='margin-left:100px;'>f. Enclosing exterior surfaces with aluminum, vinyl siding, wood, concrete or
similar durable material after covering with breathable building wrap; and</p>

<p style='margin-left:100px;'>g. Enclosing exterior trim with aluminum or vinyl coil stock;</p>





";
return $data;

?>

