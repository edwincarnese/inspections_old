<?php


$cuid = getPostIsset('cuid');

$query = "SELECT 
		  units.* ,inspection.* ,insp_assigned.* , 
		  `inspector`.address as inspector_address,
		  `inspector`.email as inspector_email,
		  inspector.*, 
		  building.*,
		  building.address as building_address,
		  `client`.firstname as owner_fname,
		  `client`.address as client_address,
		  `client`.lastname as owner_lname,
		  `client_phone`.number as owner_contact,
		  `inspector_phone`.number as inspector_work_contact

		  FROM units,inspection ,insp_assigned , inspector , building , owners , client , client_phone , inspector_phone
		  WHERE cuid=$cuid AND 
		  `units`.iid = `inspection`.iid AND
		  `inspection`.iid = `insp_assigned`.iid AND
		  `inspection`.bid = `building`.bid AND
		  `building`.bid = `owners`.bid AND
		  `owners`.cid = `client`.cid AND
		  `client_phone`.cid = `client`.cid AND
		  `inspector`.tid = `insp_assigned`.tid AND
		  `inspector`.tid = `inspector_phone`.tid OR
		  `inspector_phone`.tid = NULL AND
		  `inspector_phone`.type = 'Work'
		  ";



$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$data = mysql_fetch_array($result, MYSQL_ASSOC);

$default_top = "10px";

$building_address = issetBlank($data,'building_address');
$prepared_by = issetBlank($data,'firstname') . " " . issetBlank($data,'lastname');
$prepared_for = issetBlank($data,'owner_fname') . " " . issetBlank($data,'owner_lname');
$inspector_pos = "NH Licensed Risk Assessor/Lead";
$inspector_no = "Inspector RA # 0064";
$client_address = issetBlank($data,'client_address');
$client_contact = issetBlank($data,'owner_contact');

$inspector_company_name = "K. Kirkwood Consulting, LLC";
$inspector_address = issetBlank($data,'inspector_address');
$inspector_contact = issetBlank($data,'inspector_work_contact');
$inspector_email = issetBlank($data,'inspector_email');
$property_type = "";
$date_performed = "";

$data = "

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:40px;'> Introduction and Executive Summary </p>
<p style='text-indent:50px;'> This is a lead isnpection on a ". $property_type ." located at ". $client_address .". The inspection was conducted at the request of the owners ". $prepared_for ." and was provided by ". $inspector_no .", ". $prepared_by .".
</p>
<p style='text-indent:50px;'>
The field work was performed on ". $date_performed ." The inspection was performed
within the current acceptable industry guidelines including: the US Environmental Protection
Agency's (EPA) title X, the Housing and Urban Development (HUD) lead safe housing rule, and
the State of New Hampshire's RSA 130-A & Chapter He-P 1600, lead poisoning prevention and
control rules (see table of contents)
</p>
<p style='text-align:center; font-weight:bold; font-size:20px; margin-top:40px; margin-bottom:40px;'> What is the difference between an inspection and risk assessment? </p>
<p style='text-indent:50px;'> An inspection is a surface-by-surface investigation to determine whether there is lead-based paint in a home or child-occupied facility, and where it is located. Inspections can be legally
performed only by certified inspectors or risk assessors. Lead-based paint inspections determine
the presence of lead-based paint. It is particularly helpful in determining whether lead-based
paint is present prior to purchasing, renting, or renovating a home, and identifying potential
sources of lead exposure at any time. </p>
<p style='text-indent:50px; align:justify;'> A risk assessment is an on-site investigation to determine the presence, type, severity,
and location of lead-based paint hazards (including lead hazards in paint, dust, and soil) and
provides suggested ways to control them. Risk assessments can be legally performed only by
certified risk assessors. Lead-based paint risk assessments are particularly helpful in determining
sources of current exposure and in designing possible solutions. </p>
<p style='text-indent:50px;'> You can also have a combined inspection and risk assessment. </p>
";


return $data;

?>