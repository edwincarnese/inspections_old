
<?php

$data = "
<span style='margin-left:50px;'>Make:</span> 
<span style='margin-left:89px;'>Niton LLC</span><br/>

<span style='margin-left:50px;'>Tested Model:</span> 
<span style='margin-left:28;'>XLp 300</span> <br/>

<span style='margin-left:50px;'>Source:</span> 
<span style='margin-left:80px;'>109 Cd</span> <br/>

<span style='margin-left:50px;'>Note:</span> 
<p style='margin-left:183px; margin-top:-20px;'>This PCS is also applicable to the equivalent model variations indicated below, for the Lead-in-Paint K+L variable reading time mode, in the XLi and XLp series:</p>

<span style='margin-left:210px; margin-bottom:5px;'>XLi 300A, XLi 301A, XLi 302A and XLi 303A.</span> <br/>
<span style='margin-left:210px; margin-bottom:5px;'>XLp 300A, XLp 301A, XLp 302A and XLp 303A.</span> <br/>
<span style='margin-left:210px; margin-bottom:5px;'>XLi 700A, XLi 701A, XLi 702A and XLi 703A.</span> <br/>
<span style='margin-left:210px; margin-bottom:5px;'>XLp 700A, XLp 701A, XLp 702A, and XLp 703A.</span> <br/> <br/> 

<span>Note:</span>
<p style='margin-left:45px; margin-top:-22px;'>The XLi and XLp versions refer to the shape of the handle part of the instrument. The
differences in the model numbers reflect other modes available, in addition to Lead-in-
Paint modes. The manufacturer states that specifications for these instruments are
identical for the source, detector, and detector electronics relative to the Lead-in-Paint
mode.</p>

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px;'> FIELD OPERATION GUIDANCE </p>

<p style='font-weight:bold; margin-bottom:5px;'>OPERATING PARAMETERS:</p>
<p style='margin-top:-10px;'>Lead-in-Paint K+L variable reading time mode.</p>

<p style='font-weight:bold; margin-bottom:5px;'>XRF CALIBRATION CHECK LIMITS:</p>
<table border='1' style='margin-top:-10px; margin-bottom:15px; width: 100%;'>
	<tr>
		<td style='padding:10px;'>____________ (inclusive)</td>
	</tr>
</table>

<p>The calibration of the XRF instrument should be checked using the paint film nearest 1.0 mg/cm 2 in the NIST Standard Reference Material (SRM) used (e.g., for NIST SRM 2579, use the 1.02 mg/cm 2 film).</p>

<p>If readings are outside the acceptable calibration check range, follow the manufacturer's instructions to bring the instruments into control before XRF testing proceeds.</p>

<p style='font-weight:bold; margin-bottom:5px;'>SUBSTRATE CORRECTION:</p>

<p style='margin-top:-10px;'>For XRF results using Lead-in-Paint K+L variable reading time mode, substrate correction is not needed for:</p>

<p style='text-indent:50px;'>Brick, Concrete, Drywall, Metal, Plaster, and Wood</p>

<p style='font-weight:bold; margin-bottom:5px;'>INCONCLUSIVE RANGE OR THRESHOLD:</p>

<table border='1' style='width: 100%; margin-bottom:30px;'>
	<tr>
		<td style='text-align:center; font-weight:bold;'>K+L MODE <br/> READING DESCRIPTION</td>
		<td style='text-align:center; font-weight:bold;'>SUBSTRATE</td>
		<td style='text-align:center; font-weight:bold;'>THRESHOLD <br/> (mg/cm²)</td>
	</tr>
	<tr>
		<td style='text-align:center;'>____________</td>
		<td style='text-align:center;'>
			____________ <br/>
			____________ <br/>
			____________ <br/>
			____________ <br/>
			____________ <br/>
			____________ <br/>
		</td>
		<td style='text-align:center;'>
			____________ <br/>
			____________ <br/>
			____________ <br/>
			____________ <br/>
			____________ <br/>
			____________ <br/>
		</td>
	</tr>
</table>

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px;'> BACKGROUND INFORMATION </p>

<p style='font-weight:bold;'>EVALUATION DATA SOURCE AND DATE:</p>

<p>This sheet is supplemental information to be used in conjunction with Chapter 7 of the HUD Guidelines for
the Evaluation and Control of Lead-Based Paint Hazards in Housing &quot;HUD Guidelines&quot;. Performance
parameters shown on this sheet are calculated from the EPA/HUD evaluation using archived building
components. Testing was conducted in August 2004 on 133 testing combinations. The instruments that
were used to perform the testing had new sources; one instrument’s was installed in November 2003 with 40 mCi initial strength, and the other’s was installed June 2004 with 40 mCi initial strength.</p>

<p style='font-weight:bold;'>OPERATING PARAMETERS:</p>

<p>Performance parameters shown in this sheet are applicable only when properly operating the instrument susing the manufacturer's instructions and procedures described in Chapter 7 of the HUD Guidelines.</p>

<p style='font-weight:bold;'>SUBSTRATE CORRECTION VALUE COMPUTATION:</p>

<p>Substrate correction is not needed for brick, concrete, drywall, metal, plaster or wood when using Lead-in- Paint K+L variable reading time mode, the normal operating mode for these instruments. If substrate correction is desired, refer to Chapter 7 of the HUD Guidelines for guidance on correcting XRF results for substrate bias.</p>

<p style='font-weight:bold;'>EVALUATING THE QUALITY OF XRF TESTING:</p>

<p>Randomly select ten testing combinations for retesting from each house or from two randomly selected units in multifamily housing. Use the K+L variable time mode readings.</p>

<p>Conduct XRF retesting at the ten testing combinations selected for retesting.</p>

<p>Determine if the XRF testing in the units or house passed or failed the test by applying the steps below.</p>

<p style='margin-left:50px;'>Compute the Retest Tolerance Limit by the following steps:</p>

<p style='margin-left:80px;'>Determine XRF results for the original and retest XRF readings. Do not correct the
original or retest results for substrate bias. In single-family housing a result is defined as
the average of three readings. In multifamily housing, a result is a single reading.
Therefore, there will be ten original and ten retest XRF results for each house or for the
two selected units.</p>

<p style='margin-left:120px;'>Calculate the average of the original XRF result and retest XRF result for each testing combination.</p>

<p style='margin-left:120px;'>Square the average for each testing combination.</p>

<p style='margin-left:120px;'>Add the ten squared averages together. Call this quantity C.</p>

<p style='margin-left:120px;'>Multiply the number C by 0.0072. Call this quantity D.</p>

<p style='margin-left:120px;'>Add the number 0.032 to D. Call this quantity E.</p>

<p style='margin-left:120px;'>Take the square root of E. Call this quantity F.</p>

<p style='margin-left:50px;'>Multiply F by 1.645. The result is the Retest Tolerance Limit.</p>

<p style='margin-left:50px;'>Compute the average of all ten original XRF results.</p>

<p style='margin-left:50px;'>Compute the average of all ten re-test XRF results.</p>

<p style='margin-left:50px;'>Find the absolute difference of the two averages.</p>

<p style='margin-left:50px;'>If the difference is less than the Retest Tolerance Limit, the inspection has passed the retest. If
the difference of the overall averages equals or exceeds the Retest Tolerance Limit, this
procedure should be repeated with ten new testing combinations. If the difference of the overall
averages is equal to or greater than the Retest Tolerance Limit a second time, then the
inspection should be considered deficient.</p>

<p>Use of this procedure is estimated to produce a spurious result approximately 1% of the time. That is, results of this procedure will call for further examination when no examination is warranted in approximately 1 out of 100 dwelling units tested.</p>

<p style='font-weight:bold;'>TESTING TIMES:</p>

<p>For the Lead-in-Paint K+L variable reading time mode, the instrument continues to read until it is moved
away from the testing surface, terminated by the user, or the instrument software indicates the reading is
complete. The following table provides testing time information for this testing mode. The times have
been adjusted for source decay, normalized to the initial source strengths as noted above. Source
strength and type of substrate will affect actual testing times. At the time of testing, the instruments had
source strengths of 26.6 and 36.6 mCi.</p>





<div style='page-break-after:always;'></div> 

<table border='1' style='width:100%; margin-bottom:20px;'>

<tr>
	<td style='text-align:center; font-weight:bold; padding:10px;' colspan='7'>
		Testing Times Using K+L Reading Mode (Seconds)
	</td>
</tr>

<tr>
	<td style='text-align:center;'>
		
	</td>
	<td style='text-align:center;' colspan='3'>
		All Data
	</td>
	<td style='text-align:center;' colspan='3'>
		Median for laboratory-measured lead levels (mg/cm²)
	</td>
</tr>

<tr>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________ 
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
</tr>

<tr>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
</tr>

<tr>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
</tr>

<tr>
	<td style='text-align:center;'>
		____________ <br/> ____________ <br/> ____________ <br/> ____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
	<td style='text-align:center;'>
		____________
	</td>
</tr>

</table>

<p style='font-weight:bold;'>CLASSIFICATION RESULTS:</p>
<p>XRF results are classified as positive if they are greater than or equal to the threshold, and negative if they are less than the threshold.</p>

<p style='font-weight:bold;'>DOCUMENTATION:</p>
<p>A document titled Methodology for XRF Performance Characteristic Sheets provides an explanation of
the statistical methodology used to construct the data in the sheets, and provides empirical results from
using the recommended inconclusive ranges or thresholds for specific XRF instruments. For a copy of
this document call the National Lead Information Center Clearinghouse at 1-800-424-LEAD.</p>

<table border='1'>
<tr>
<td style='padding:5px;'>This XRF Performance Characteristic Sheet was developed by the Midwest Research Institute (MRI)
and QuanTech, Inc., under a contract between MRI and the XRF manufacturer. HUD has determined
that the information provided here is acceptable when used as guidance in conjunction with Chapter 7,
Lead-Based Paint Inspection, of HUD’s Guidelines for the Evaluation and Control of Lead-Based Paint
Hazards in Housing.</td>
</tr>
</table>

";
return $data;

?>

