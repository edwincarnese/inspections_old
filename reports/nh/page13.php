
<?php

$data = "
	
<p style='margin-left:80px;'>(6) Blistering;</p>

<p style='margin-left:80px;'>(7) Open to the environment in a manner that could damage the encapsulated area; or</p>

<p style='margin-left:80px;'>(8) Otherwise altered in a manner which jeopardizes its protective qualities.</p>

<p style='margin-left:50px;'>(j) If signs of wear or deterioration, as described in (i) above, are found during the visual
inspection, the owner shall visually inspect the encapsulated surfaces at least every 3
months for the next 6 months, then annually thereafter.</p>

<p style='margin-left:50px;'>(k) If the encapsulation fails to maintain its integrity or if repairs are needed and the
affected area involves less than 6 square feet of surface, the repair shall be considered in-
place management and shall be remedied in accordance with the encapsulant
manufacturer’s recommendations, He-P 1608, and He-P 1610.02 through He-P 1610.05.</p>

<p style='margin-left:50px;'>(l) When repairing a surface as described in (k) above, a property owner shall not engage in any practice prohibited under He-P 1609.02(c).</p>

<p style='margin-left:50px;'>(m) When a repair of the affected area involves more than 6 square feet of surface area,
the property owner shall remedy in accordance with He-P 1608 and either He-P 1609 or
He-P 1610, including the requirement for a clearance inspection with dust wipes for the
area where work occurred.</p>

<p style='margin-left:50px;'>(n) In addition to the record keeping requirements of He-P 1608.15, the owner shall maintain the following records for the life of the encapsulant product:</p>

<p style='margin-left:80px;'>(1) Documentation of:</p>

<p style='margin-left:120px;'>a. The name of the encapsulant product applied;</p>

<p style='margin-left:120px;'>b. The results of the “Pull-Off Tape Test for Adhesion” or the “Assessment of Painted Surfaces for Adhesion” test (ASTM E 1796-03);</p>

<p style='margin-left:120px;'>c. The location of the encapsulant application; and</p>

<p style='margin-left:120px;'>d. The date of encapsulant application; and</p>

<p style='margin-left:80px;'>(2) Written documentation of the visual inspections required by (h) through (j) above.</p>

<p style='margin-left:50px;'>(o) The owner shall make all records required by (n) above available to:</p>

<p style='margin-left:80px;'>(1) The commissioner upon request; and</p>

<p style='margin-left:80px;'>(2) An owner or entity upon the sale, lease, rental or transfer of interest in the dwelling, dwelling unit or child care facility.</p>

<p style='margin-left:50px;'>(p) The commissioner shall approve encapsulant products for lead hazard reduction work that have been tested and meet or exceed:</p>

<p style='margin-left:80px;'>(1) ASTM E 1795-04, Standard Specification for Non-Reinforced Liquid Coating Encapsulation Products for Leaded Paint in Buildings; or</p>

<p style='margin-left:80px;'>(2) ASTM E 1797-04, Standard Specification for Reinforced Liquid Coating Encapsulation Products for Leaded Paint in Buildings.</p>

<p style='margin-left:50px;'>(q) Manufacturers shall submit the following documentation to the commissioner prior to the encapsulation product being approved:</p>

<p style='margin-left:80px;'>(1) Documentation in the form of a performance testing report showing:</p>

<p style='margin-left:120px;'>a. Compliance with the applicable ASTM standard;</p>

<p style='margin-left:120px;'>b. That all testing was conducted by an independent and National
Voluntary Laboratory Accreditation Program (NVLAP) certified testing laboratory; and</p>

<p style='margin-left:120px;'>c. The minimum dry film thickness at which the lead encapsulant product meets or exceeds the requirements of the applicable ASTM standard in (p) above for interior and/or exterior use; and</p>

<p style='margin-left:80px;'>(2) Documentation showing that the encapsulation product:</p>

<p style='margin-left:120px;'>a. Is warranteed by the product manufacturer to perform for at least 20 years as a durable barrier between the lead-based paint and the environment in locations or conditions similar to those of the planned application; and</p>

<p style='margin-left:120px;'>b. Is formulated with an FDA-approved anti-ingestant ingredient which
deters oral contact with the cured film and which discourages ingestion of
delaminated coatings.</p>

<p style='margin-left:250px;'><a href='http://www.gencourt.state.nh.us/rules/state_agencies/sourcehe-p.html'>Source.</a> #5920, eff 1-1-95; ss by #7181, eff 12-24-99;
ss by #7495, eff 5-23-01; ss by #8039, eff 2-13-04; ss
by #8932, eff 7-6-07; ss by #9986, eff 9-1-11 (from
He-P 1605.09</p>

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px; margin-top:40px;'> Testing Protocols </p>

<p style='margin-left:80px;'>The following steps are taken by a licensed lead inspector to conduct an accurate lead inspection:</p>

<p style='margin-left:50px;'>1. Draw a floor plan for the property.</p>

<p style='margin-left:50px;'>2. Calibrate the XRF</p>

<p style='margin-left:50px;'>3. List all painted testing combinations by room equivalent including
the color and substrates (metal, sheetrock, concrete, plaster,
wood, vinyl).</p>

<p style='margin-left:50px;'>4. Select testing combinations, location and surface of where to test with XRF.</p>

<p style='margin-left:50px;'>5. Test surfaces with the XRF </p>

<p style='margin-left:65px;'>a. Collect and analyze paint chip samples if necessary.</p>

<p style='margin-left:50px;'>6. Classify the condition of the lead surfaces on the report pages</p>

<p style='margin-left:50px;'>7. Post Calibrate the XRF</p>

<p style='margin-left:50px;'>8. Evaluate the work and results to ensure the quality of the inspection.</p>

<p style='margin-left:50px;'>9. Document all findings in the report.</p>

<p style='margin-left:50px;'>10. Submit the completed report within 10 days of the inspection.</p>

<p style='font-weight:bold; font-size:20px;'> Calibration </p>

<p style='text-indent:50px;'>Calibrate the XRF prior to and at the completion of lead paint testing and at least every
four (4) hours. To calibrate the NITON XLP 300, put the XRF in K&L mode on the 1.02 block. Each
of the three readings must be taken for 20 nominal seconds, as shown on the XRF screen. When
performing the lead inspection, switch to Standard Mode.</p>

<p style='font-weight:bold; font-size:20px;'> Testing </p>

<p style='text-indent:50px;'>When properties are under order, full lead inspections must be performed on the interior
of the unit and all interior and exterior common areas. For multi-family housing, all interior
common areas accessible to occupants must be included in the inspection (i.e. for 3 story
buildings, all interior stairways must be inspected for any unit in the property).</p>

<p style='text-indent:50px;'>It is the inspector's job to test a surface that is representative of each type of painted (or otherwise coated) testing combination in every room equivalent.</p>

<p style='text-indent:50px;'>When testing for lead paint using an XRF, certain adjacent building components can be
grouped together if they have the same painting history. A testing combination is made up of the
room equivalent, component and substrate. If they are all similar they can be grouped together
and tested once.</p>

<p style='font-weight:bold; font-size:20px;'> Testing Combination </p>

<p style='text-indent:50px;'>A testing combination is a unique combination of room equivalent, building component
type, and substrate. The following may be grouped together as a single testing combination as
long as they have the same painting histories:</p>

<p style='font-weight:bold; font-size:20px;'> Walls </p>

<p style='text-indent:50px;'>All four walls should be tested separately. Floor, radiator, and ceilings should each be
tested once</p>

<p style='font-weight:bold; font-size:20px;'>Window Systems </p>

<p style='text-indent:50px; margin:20px;'>Testing combination 1: aprons, casing, interior stops, and jambs</p>
<p style='text-indent:50px;  margin:20px;'>Testing combination 2: interior window muntins and window sashes</p>
<p style='text-indent:50px;  margin:20px;'>Testing combination 3: exterior window muntins and window sashes</p>
<p style='text-indent:50px;  margin:20px;'>Testing combination 4: Interior window sill</p>

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px;'> Performance Characteristic Sheet </p>

<p style='font-weight:bold;'>EFFECTIVE DATE: <span style='margin-left:25px;'>September 24, 2004 </span> <span style='margin-left:250px;'>EDITION NO.: 1</span></p>

<p style='font-weight:bold; margin-bottom:5px;'>MANUFACTURER AND MODEL:</p>

";
return $data;

?>

