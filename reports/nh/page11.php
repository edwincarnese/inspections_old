
<?php

$data = "
	
<p style='margin-left:70px;'>(4) Reversal of all component parts of a woodwork surface such that:</p>

<p style='margin-left:100px;'>a. No surface containing a lead exposure hazard remains exposed; and</p>

<p style='margin-left:100px;'>b. All seams are caulked and sealed; and</p>

<p style='margin-left:70px;'>(5) Permanent fastening of window sashes to eliminate friction surfaces if not
otherwise prohibited by any state laws, rules or local ordinances for health, building
and fire safety.</p>

<p style='margin-left:50px;'>(b) The materials used in (a)(3) above shall:</p>

<p style='margin-left:70px;'>(1) Comply with all state laws, rules or local ordinances for health, building and fire
safety; and</p>

<p style='margin-left:70px;'>(2) Only be used in places that the manufacturer intended them to be used.</p>

<p style='margin-left:50px;'>(c) The following methods shall be prohibited when performing lead-based substance abatement:</p>	

<p style='margin-left:70px;'>(1) Dry scraping or sanding except as allowed by (a) (1) b.11. above;</p>

<p style='margin-left:70px;'>(2) Dry sweeping of lead contaminated areas or surfaces;</p>

<p style='margin-left:70px;'>(3) Dry abrasive blasting using sand, grit or any other particulate without a HEPA local vacuum exhaust tool;</p>

<p style='margin-left:70px;'>(4) Utilizing mechanical sanding, planning, grinding or other removal equipment without a HEPA local vacuum exhaust tool;</p>

<p style='margin-left:70px;'>(5) Torch or open-flame burning;</p>

<p style='margin-left:70px;'>(6) Propane-fueled heat grids;</p>

<p style='margin-left:70px;'>(7) Heating elements operating above 700 degrees Fahrenheit;</p>

<p style='margin-left:70px;'>(8) Uncontained hydroblasting or high-pressure wash;</p>

<p style='margin-left:70px;'>(9) Use of methylene chloride or solutions containing methylene chloride in interior work areas; and</p>

<p style='margin-left:70px;'>(10) Encapsulants that have not been approved under He-P 1609.03(a).</p>

<p style='margin-left:50px;'>(d) The following precautions shall be used when conducting lead-based substance removal on properties listed in or determined eligible for the National Register of Historic
Places:</p>

<p style='margin-left:70px;'>(1) When an orbital sander with a HEPA local vacuum exhaust sized to match the tool is used, such device shall be used only as a finishing or smoothing tool;</p>

<p style='margin-left:70px;'>(2) When a belt sander with a HEPA local vacuum exhaust sized to match the tool is used, such device shall be used only on flat surfaces; and</p>

<p style='margin-left:70px;'>(3) When abrasive blasting with a HEPA local vacuum exhaust sized to match the tool is performed, such method shall only be used on cast and wrought iron, steel or
concrete substrates under the supervision of a professionally qualified art or
architectural conservator.</p>

<p style='margin-left:50px;'>(e) When soil lead levels are equal to or greater than 5,000 parts per million (ppm), soil abatement shall occur by one of the following:</p>

<p style='margin-left:70px;'>(1) The contaminated soil shall be completely excavated to a depth of at least 6 inches and replaced with soil containing less than 200 ppm lead;</p>

<p style='margin-left:70px;'>(2) When the soil below 2 inches from the surface has been found to contain lead
below 1,500 ppm, the contaminated soil shall be excavated to a depth of at least 2
inches, the remaining rototilled, and the excavated soil replaced with soil containing
less than 200 ppm lead; or</p>

<p style='margin-left:70px;'>(3) The contaminated soil shall be completely enclosed with asphalt or concrete.</p>

<p style='margin-left:50px;'>(f) During soil abatement:</p>

<p style='margin-left:70px;'>(1) Surface run-off and the windblown spread of lead-contaminated soil shall be prevented by either:</p>

<p style='margin-left:100px;'>a. Keeping bare soil wet during the entire period of abatement; or</p>

<p style='margin-left:100px;'>b. Temporarily covering exposed sites with polyethylene sheeting with the covering secured in place at all edges and seams;</p>

<p style='margin-left:70px;'>(2) Soil removal activities shall not be conducted when:</p>

<p style='margin-left:100px;'>a. The constant wind speed exceeds 20 miles per hour; or</p>


";
return $data;

?>

