<?php


$cuid = getPostIsset('cuid');

$query = "SELECT 
		  units.* ,inspection.* ,insp_assigned.* , 
		  `inspector`.address as inspector_address,
		  `inspector`.email as inspector_email,
		  inspector.*, 
		  building.*,
		  building.address as building_address,
		  `client`.firstname as owner_fname,
		  `client`.address as client_address,
		  `client`.lastname as owner_lname,
		  `client_phone`.number as owner_contact,
		  `inspector_phone`.number as inspector_work_contact

		  FROM units,inspection ,insp_assigned , inspector , building , owners , client , client_phone , inspector_phone
		  WHERE cuid=$cuid AND 
		  `units`.iid = `inspection`.iid AND
		  `inspection`.iid = `insp_assigned`.iid AND
		  `inspection`.bid = `building`.bid AND
		  `building`.bid = `owners`.bid AND
		  `owners`.cid = `client`.cid AND
		  `client_phone`.cid = `client`.cid AND
		  `inspector`.tid = `insp_assigned`.tid AND
		  `inspector`.tid = `inspector_phone`.tid OR
		  `inspector_phone`.tid = NULL AND
		  `inspector_phone`.type = 'Work'
		  ";



$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$data = mysql_fetch_array($result, MYSQL_ASSOC);

$default_top = "10px";

$building_address = issetBlank($data,'building_address');
$prepared_by = issetBlank($data,'firstname') . " " . issetBlank($data,'lastname');
$prepared_for = issetBlank($data,'owner_fname') . " " . issetBlank($data,'owner_lname');
$inspector_pos = "NH Licensed Risk Assessor/Lead";
$inspector_no = "Inspector RA # 0064";
$client_address = issetBlank($data,'client_address');
$client_contact = issetBlank($data,'owner_contact');

$inspector_company_name = "K. Kirkwood Consulting, LLC";
$inspector_address = issetBlank($data,'inspector_address');
$inspector_contact = issetBlank($data,'inspector_work_contact');
$inspector_email = issetBlank($data,'inspector_email');
$image_name = $data['image_name'];




$data = "

<div class='col-md-12'>

	<div class='row'>
		<div class='col-md-12'>
			<div class ='col-md-6 text-center'>
	   			<img class='' src='images/logo.png'>
	   		<div>	
	   	</div>
	</div>

	<div class='row'>
		<div class='col-md-12'>
			<h2 style= 'font-weight:bold;text-align:center'>Lead Inspection</h2>
			<h4 style= 'text-align:center'><strong>$building_address</strong></h4>
		</div>
	</div>

	<div class='row' style = 'margin-top:10px'>
	   	<div class='col-md-12'>
	   		<div class ='col-md-8 text-center'>
	   			<img class='img-responsive' src='$image_name'>
	   		</div>
	   	</div>
	</div>

	<div class='row' style = 'margin-top:23px'>
		<div class = 'col-md-12' >
	   		<div class='col-md-6' style='width:200px;float:left;margin-left:30px'>
	   			<span><strong> Prepared by: </strong></span><br>
	   			<span><strong> $prepared_by </strong></span><br>
	   			<span> $inspector_pos </span><br>
	   			<span> $inspector_no </span><br>
	   		</div>

	   		<div class='col-md-6' style='width:200px;float:right'>
	   			<span><strong> Prepared for: </strong></span><br>
	   			<span><strong> $prepared_for <strong></span><br>
	   			<span> $client_address </span><br>
	   			<span> $client_contact </span><br>
	   		</div>
	   	</div>
	</div>


</div>

<div class='row' style = 'margin-top:150px; margin-left:30px'>
	<div class = 'col-md-6'  style='float:left;margin-left:17px' >
   			<span><strong> Contact Information:</strong> </span><br>
   			<span><strong> $inspector_company_name </strong></span><br>
   			<span> $inspector_address </span><br>
   			<span> $inspector_contact </span><br>
   			<span> $inspector_email </span><br>
   	</div>
</div>

";


return $data;

?>