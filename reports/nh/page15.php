
<?php

$data = "


<div style='page-break-after:always;'></div> 

<table style='width:100%;'>

<tr>

<td style='text-align:left;'>
25 Lowell St. <br/>
Manchester,  <br/>
NH 03101 </td>

<td style='text-align:center;'><img src='images/Untitled.png' style='border-style: solid; border-color:green; margin-bottom:5px;'/> <br/>
<a href='http://www.kkirkwood.com'>www.kkirkwood.com</a> <br/>
Lead Inspection / Risk Assessment Report
</td>

<td style='text-align:right;'>
Phone: 603-781-4304 <br/>
Email: <br/>
Kate@kkirkwood.com</td>

</tr>

</table>

<p style='text-align:right;'>X-Ray Fluorescence Niton Xlp 303 AW: Serial number 8988</p>

Property Address:

<table style='width:100%; border-bottom:25px; border-collapse: collapse;'>
<tr>

<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>

</tr>

<tr>

<td style='text-align:center;'>Street Number</td>
<td style='text-align:center;'>Address</td>
<td style='text-align:center;'>Apartment #</td>
<td style='text-align:center;'>City</td>
<td style='text-align:center;'>State</td>
<td style='text-align:center;'>Zip</td>

</tr>
</table>

<br/>

Owner Information:

<table style='width:100%; border-bottom:25px; border-collapse: collapse;'>
<tr>

<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>

</tr>

<tr>

<td style='text-align:center;'>First Name</td>
<td style='text-align:center;'>Last Name</td>
<td style='text-align:center;'>Address</td>
<td style='text-align:center;'>City</td>
<td style='text-align:center;'>State</td>
<td style='text-align:center;'>Zip</td>

</tr>
</table>

<br/>

<table style='border-bottom:25px; border-collapse: collapse; width:100%'>
<tr>

<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>

</tr>

<tr>

<td style='text-align:center;'>Phone</td>
<td style='text-align:center;'>Email</td>

</tr>

</table>

<br/>

Client Information (if different from owner):

<table style='border-bottom:25px; border-collapse: collapse; width:100%'>

<tr>

<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>____________</td>

</tr>

<tr>

<td style='text-align:center;'>Address</td>
<td style='text-align:center;'>City</td>
<td style='text-align:center;'>State</td>
<td style='text-align:center;'>Zip</td>

</tr>

</table>

<br/>
<br/>
<span>
<table border='1' style='margin-left:20px;' cellspacing='0' cellpadding='0'>
	<tr>
		<td style='padding: 10px;'>Depth Indicator:</td> 
		<td style='text-align:center; padding: 10px;'>1st</td>
		<td style='text-align:center; padding: 10px;'>2nd</td>
	</tr>
	<tr>
		<td style='padding: 10px;'>RES</td> 
		<td colspan='2' style='padding: 10px; text-align:center;'>__</td>		
	</tr>
	<tr>
		<td colspan='3' style='padding: 10px;'>Calibration:</td>
	</tr>
	<tr>
		<td style='padding: 10px;'>Start: 1st</td> 
		<td style='padding: 10px;'>Start: __</td> 
		<td style='padding: 10px;'>Start: __</td>
	</tr>
	<tr>
		<td style='padding: 10px;'>2nd</td> 
		<td style='padding: 10px;'>Start: __</td> 
		<td style='padding: 10px;'>Start: __</td>
	</tr>
	<tr>
		<td style='padding: 10px;'>3nd</td> 
		<td style='padding: 10px;'>Start: __</td> 
		<td style='padding: 10px;'>Start: __</td>
	</tr>
	<tr>
		<td colspan='3' style='padding: 10px;'>Calibration:</td>
	</tr>
	<tr>
		<td style='padding: 10px;'>End: 1st</td> 
		<td style='padding: 10px;'>Start: __</td> 
		<td style='padding: 10px;'>Start: __</td>
	</tr>
	<tr>
		<td style='padding: 10px;'>2nd</td> 
		<td style='padding: 10px;'>Start: __</td> 
		<td style='padding: 10px;'>Start: __</td>
	</tr>
	<tr>
		<td style='padding: 10px;'>3nd</td> 
		<td style='padding: 10px;'>Start: __</td> 
		<td style='padding: 10px;'>Start: __</td>
	</tr>
</table>


<table border='1' style='margin-top:-405px; margin-left:270px;' cellspacing='0' cellpadding='0'>

<tr>
<td style='padding: 10px;'>Number of Rooms in Unit:</td>
<td style='text-align:center; padding:10px;'>__</td>
</tr>

<tr>
<td style='padding: 10px;'>Property Type:</td>
<td style='text-align:center; padding:10px;'>__________</td>
</tr>

<tr>
<td style='padding: 10px;'>Condominium # of Units:</td>
<td style='text-align:center; padding:10px;'>__</td>
</tr>

<tr>
<td style='padding: 10px;'>Year of Construction:</td>
<td style='text-align:center; padding:10px;'>____</td>
</tr>

</table>

</span>

<div style='page-break-after:always;'></div> 

<table border='1' style='width:100%'>

<tr>
<td style='font-weight:bold; text-align:center;'>Key:</td>
<td style='font-weight:bold; text-align:center;'>Lead Column</td>
<td style='font-weight:bold; text-align:center;'>Key:</td>
<td style='font-weight:bold; text-align:center;'>Abatement/I.C. Method Column</td>

</tr>

<tr>
<td style='text-align:center;'>COV</td>
<td style='text-align:center;'>Covered</td>
<td style='text-align:center;'>CAP</td>
<td style='text-align:center;'>Capped</td>
</tr>

<tr>
<td style='text-align:center;'>VNL</td>
<td style='text-align:center;'>Vinyl</td>
<td style='text-align:center;'>COV</td>
<td style='text-align:center;'>Enclosed</td>
</tr>

<tr>
<td style='text-align:center;'>MET</td>
<td style='text-align:center;'>Metal</td>
<td style='text-align:center;'>Enclosed</td>
<td style='text-align:center;'>Encapsulated</td>
</tr>

<tr>
<td style='text-align:center;'>VR</td>
<td style='text-align:center;'>Vinyl Rep. Window</td>
<td style='text-align:center;'>PS</td>
<td style='text-align:center;'>Paint Stabilization</td>
</tr>

<tr>
<td style='text-align:center;'>MR</td>
<td style='text-align:center;'>Metal Rep Window</td>
<td style='text-align:center;'>PRE</td>
<td style='text-align:center;'>Prepared for ENC</td>
</tr>

<tr>
<td style='text-align:center;'>NA</td>
<td style='text-align:center;'>Not Accessible</td>
<td style='text-align:center;'>VR/MR</td>
<td style='text-align:center;'>Vinyl/Metal Rep Window</td>
</tr>

<tr>
<td style='text-align:center;'>NC</td>
<td style='text-align:center;'>No Coating</td>
<td style='text-align:center;'>SFR</td>
<td style='text-align:center;'>Storm Frame Removed</td>
</tr>

<tr>
<td style='text-align:center;'>Tile</td>
<td style='text-align:center;'>Tile (Testing Suggested)</td>
<td style='text-align:center;'>\</td>
<td style='text-align:center;'>Component does not exist</td>
</tr>

<tr>
<td style='text-align:center;'>DC</td>
<td style='text-align:center;'>Dropped Ceiling</td>
<td style='text-align:center;'>SCR</td>
<td style='text-align:center;'>Scraped</td>
</tr>

<tr>
<td></td>
<td></td>
<td style='text-align:center;'>DIP</td>
<td style='text-align:center;'>Dipped</td>
</tr>

<tr>
<td></td>
<td></td>
<td style='text-align:center;'>REM</td>
<td style='text-align:center;'>Removed</td>
</tr>

<tr>
<td></td>
<td></td>
<td style='text-align:center;'>REP</td>
<td style='text-align:center;'>Replaced</td>
</tr>

<tr>
<td></td>
<td></td>
<td style='text-align:center;'>REV</td>
<td style='text-align:center;'>Reversed</td>
</tr>

<tr>
<td></td>
<td></td>
<td style='text-align:center;'>INT</td>
<td style='text-align:center;'>Intact</td>
</tr>

</table>

Comments/Notes: </br>
<p>____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________</p>

<p style='text-align:right; margin:0px; padding:0px;'></p>
<p style='text-align:right; margin:0px; padding:0px;'>Name of Inspector</p>

<p>Floor # ____ (this is the level within building of unit being inspected) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Floor # ____</p>

<center><img src='images/floor_1.png' style='height:100%; width:80%;'/></center>

<div style='page-break-after:always;'></div> 

<table style='border-bottom:25px; border-collapse: collapse; width:100%; margin-bottom:15px;'>

<tr>

<td style='border-bottom: 1px solid #000;'>__________________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>__________________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>__________________</td>
<td style='border-bottom: 1px solid #000; text-align:center;'>__________________</td>
<td style='border-bottom: 1px solid #000;'>Page _ of _</td>
	
</tr>

<tr>

<td>Inspector / Risk Assessor</td>
<td style='text-align:center;'>Lic # </td>
<td style='text-align:center;'>Signature</td>
<td style='text-align:center;'>Date</td>
<td style='text-align:center;'></td>

</tr>

</table>

Radiation Handlers Lic # 487R

<table style='border-bottom:25px; border-collapse: collapse; width:100%'>

<tr>

<td style='border-bottom: 1px solid #000;'>Address:</td>
<td style='border-bottom: 1px solid #000; font-weight:bold;'>__________________</td>
<td style='border-bottom: 1px solid #000; font-weight:bold;'>Apt. #</td>

</tr>

</table>

<br/>

<p style='font-weight:bold; margin-bottom:3px;'>ROOM # 1 LIVING ROOM</p>
<table border='1' style='border-bottom:25px; width:100%'>

<tr>

<td style='text-align:center;'>SIDE</td>
<td style='text-align:center;'>LOCATION / <br/> SURFACE</td>
<td style='text-align:center;'>LEAD mg/cm²</td>
<td style='text-align:center;'>TYPE OF <br/> HAZARD</td>
<td style='text-align:center;'>SUBSTRATE</td>
<td style='text-align:center;'>LEHRP <br/> RECOMM</td>
<td style='text-align:center;'>ABATE <br/> DATE</td>
<td style='text-align:center;'>ABATE <br/> METH</td>

</tr>

<tr>

<td style='text-align:center;'>_</td>
<td style='text-align:center;'>___________</td>
<td style='text-align:center;'>___</td>
<td style='text-align:center;'>___________</td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>

</tr>

<tr>

<td style='text-align:center;'>_</td>
<td style='text-align:center;'>___________</td>
<td style='text-align:center;'>___</td>
<td style='text-align:center;'>___________</td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>

</tr>

<tr>

<td style='text-align:center;'>_</td>
<td style='text-align:center;'>___________</td>
<td style='text-align:center;'>___</td>
<td style='text-align:center;'>___________</td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>

</tr>

<tr>

<td style='text-align:center;'>_</td>
<td style='text-align:center;'>___________</td>
<td style='text-align:center;'>___</td>
<td style='text-align:center;'>___________</td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>
<td style='text-align:center;'></td>

</tr>

<tr>
<td colspan='8' style='padding-top:10px; padding-bottom:10px;'>_______________________________________________________</td>
</tr>

</table>



<div style='page-break-after:always;'></div> 

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px;'> Exhibit A </p>

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px;'> ______________________</p>

<center><h1>______________________</h1></center>

";
return $data;

?>

