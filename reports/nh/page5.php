<?php


$data = "

<table border='1'>
<tr><td colspan='6'> <p style='font-weight:bold; font-size:20px; text-align:center; margin-bottom:0px;'>VISUAL ASSESSMENT SUMMARY</p> </td>
</tr>
<tr>
<td></td>
<td style='text-align:center; padding: 10px;'> Deteriorated Paint </td>
<td style='text-align:center; padding: 10px;'> Visible Dust Accumulation </td>
<td style='text-align:center; padding: 10px;'> Bare<br/>Soil </td>
<td style='text-align:center; padding: 10px;'> Friction/Impact Points </td>
<td style='text-align:center; padding: 10px;'> Chew Surfaces </td>
</tr>
<tr>
<td style='text-align:center; padding: 10px;'>Interior</td>
<td style='text-align:center; padding: 10px;'> _______ </td>
<td style='text-align:center; padding: 10px;'> _______ </td>
<td style='text-align:center; padding: 10px;'> _______ </td>
<td style='text-align:center; padding: 10px;'> _______ </td>
<td style='text-align:center; padding: 10px;'> _______ </td>
</tr>
<tr>
<td style='text-align:center; padding: 10px;'>Exterior</td>
<td style='text-align:center; padding: 10px;'> _______ </td>
<td style='text-align:center; padding: 10px;'> _______ </td>
<td style='text-align:center; padding: 10px;'> _______ </td>
<td style='text-align:center; padding: 10px;'> _______ </td>
<td style='text-align:center; padding: 10px;'> _______ </td>
</tr>
</table>

<br/>
<br/>
<br/>

<div style='text-align:center;'>

</div>
<hr style='margin:2px;'/>

<em style='margin-right:170px;'>Name of Inspector</em> Date 

<br/>
<br/>
<br/>

<p style='text-indent:50px;'>
Accessible building components and repersentative surfaces from selected areas of the
building were tested to determine the presence of lead based paint, utilizing XRF testing
technology. The XRF Pb-Test is an energy dispersive x-ray fluorescence (EDXRF) spectrometer
that uses a sealed, highly purified Cobalt-57 radioisotope source (&lt;12mCi) to excite a test
sample's constituent elements. The Pb-Test utilizes the recently developed Cadmium Telluride
(CdTe) Schottky diode detectors. In this inspection, a Thermo-Niton, XL—303 XRF, Serial number
8988 was used, which is a complete lead paint analysis system that quickly, accurately, and non-
destructively measures the concentration of lead-based paint (LBP) on surfaces. The XRF is used
in accordance with the following Performance Characteristic Sheet; Niton XLp 300, 9/24/2004,
ed.1. Please request additional information if you require specific sampling
methodologies associated with the XRF. Appendix D provides a glossary of terms common to
Lead-Based Paint.</p>

<p style='text-indent:50px;'>
*Please note: Accessible building components were tested to determine the presence of
lead based paint. Each surface identified to be a potential lead exposure hazard by the visual
inspection and having a distinct paint history, was tested for the presence of lead. Please refer to
the property specific cover sheet for an explanation of the abbreviations used in your inspection
and the property field notes for the actual testing results for the inspection.
</p>
";


return $data;

?>