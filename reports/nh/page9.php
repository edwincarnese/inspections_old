
<?php

$data = "
	
	<p><span style='font-weight:bold;'>ABATEMENT METHOD</span> - The abatement method used to bring a surface into full compliance or lead safe.</p>

	<p><span style='font-weight:bold;'>SOIL AND DUST TESTING</span> -  If your property undergoes a risk assessment, soil and dust testing
	may be required. There is also a space for the lead inspector risk assessor to indicate the amount
	and location of dust and bare soil, laboratory results, method of remediation, and the date of
	remediation. For properties under DHHS Order of Lead Hazard Reduction, when weather
	conditions (example: snow on the ground) prohibit sampling of soil at the time of inspection, soil
	sampling must be performed within 14 calendar days after soil conditions allow for such testing.</p>


	<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px; margin-top:40px;'> Conditions and Limitations </p>

	<p style='text-indent:50px;'>
	K. Kirkwood Consulting, LLC (KKC) has performed the tasks described in this report
	requested by the Client in a thorough and professional manner consistent with commonly
	accepted standard industry practices, using state of the art practices and best available known
	technology, as of the date of the assessment. KKC cannot guarantee and does not warrant that
	this Assessment has identified all adverse environmental factors and/or conditions affecting the
	subject property on the date of the Assessment. KKC cannot and will not warrant that the
	Assessment that was requested by the client will satisfy the dictates of, or provide a legal defense
	in connection with, any environmental laws or regulations. It is the responsibility of the client to
	know and abide by all applicable laws, regulations, and standards, including EPA’s Renovation,
	Repair and Painting regulation. This certification is available through KKC division Lead-Edu, for
	more information contact Lead-Edu at 603-781-4304 or visit <a href='http://www.lead-edu.info'> www.lead-edu.info.</a>
	</p>

	<p style='text-indent:50px;'>
	The results reported and conclusions reached by KKC are solely for the benefit of the
	client. The results and opinions in this report, based solely upon the conditions found on the
	property as of the date of the Assessment, will be valid only as of the date of the Assessment.
	KKC assumes no obligation to advise the client of any changes in any real or potential lead hazards
	at this residence that may or may not be later brought to our attention. However, KKC is always available for and interested in providing any additional guidance and/or services required
	following this inspection.
	</p>
";
return $data;

?>

