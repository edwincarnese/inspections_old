
<?php

$data = "


<p style='text-indent:50px;'>There may be surfaces omitted from the lead inspection due to not being visible, or
accessible to the lead inspector risk assessor. Any surface not listed on the lead inspection report should be assumed to contain lead, and considered a lead hazard, as applicable, for hazard
purposes.</p>

<p style='text-indent:50px;'>Intact surfaces (under the hazard column) which become loose, and then identified by a
lead inspector during a re-inspection or clearance inspection, must be fully abated. This
requirement exists even if the surface was not a hazard during the initial lead inspection.
Therefore, make sure these surfaces have not become loose prior to the clearance inspection.</p>

<p style='text-indent:50px;'>When ceramic tile is present, it is tested for informational coatings as any lead content
would be limited to the glazing, or paint under the glazing. This is not considered a coating by
most regulations. If lead is present, avoid breaking up the tiles as this could release lead.</p>

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:30px; margin-top:40px;'> Explanation of Lead Inspection Risk Assessment Report Form Columns </p>

<p style='text-indent:50px;'>This section provides general information needed to understand the lead inspection risk
assessment report. However, you should speak to your lead inspector risk assessor before you
start to do any work on your home.</p>

<p><span style='font-weight:bold;'>SIDE</span> - Refers to A, B, C, or D side of the building or room. See the diagram on the cover sheet.
The &quot;A&quot; Side of the building or room is the side facing the street that faces the property its address
(usually known as the front of the building). Keeping your back to this street from the &quot;A&quot; side
move clockwise to the &quot;B&quot; side on your left, the &quot;C&quot; side opposite you, and the &quot;D&quot; side to the
right.</p>

<p><span style='font-weight:bold;'>LOCATION/SURFACE</span> - Refers to the building component(s) being tested. Some surfaces may be
made up of more than one part. For example, &quot;Baseboard&quot; may refer to four separate pieces of
wood (one on each wall), but it is still considered one surface.</p>


";
return $data;

?>

