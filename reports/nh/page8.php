
<?php

$data = "



<p><span style='font-weight:bold;'>LEAD</span> - The actual lead result. Each surface tested must have a result recorded in the &quot;Lead&quot;
column. A number shows that the surface was tested with an XRF analyzer. A number (or average
number) equal to or greater than 1.0mg/cm2, or are &quot;COV&quot; means the surface is currently
covered or enclosed by material such as carpet, metal, or paneling. &quot;NA&quot; means that the surface
was not available for testing due to an obstruction. Not all lead paint must be abated. This column
does not tell you why a surface needs abating. The abatement standards below may not apply
for interim controls. Speak to your risk assessor for more information.</p>

<p><span style='font-weight:bold;'>TYPE OF HAZARD</span> - &quot;F/I&quot; Circled means that the surface is an abrasion/friction/impact surface
and must be abated. &quot;CH&quot; Circled means that the surface is &quot;Chewable Accessible Horizontal&quot;
and must be abated to a minimum of four feet high, two inches in from the edge or corner. &quot;D&quot;
Circled means that the surface is damaged or deteriorated and must, at minimum, be stabilized.
If more than one choice is highlighted, the rules for abatement may vary depending upon what
method of abatement you choose. Speak to the lead inspector risk assessor for more information.</p>

<p><span style='font-weight:bold;'>SUBSTRATE</span> - This column is only completed during a risk assessment for LERP or work scope
information. The designation in this column records the substrate: W=Wood, P=Plaster,
MET=Metal, MAS=Masonry, O=Other (will be identified in the comments box)</p>

<p><span style='font-weight:bold;'>LEAD EXPOSURE HAZARD REDUCTION PLAN (LEHRP) RECOMMENDATION</span> - This is the recommendation for lead exposure hazard reduction.</p>

<p><span style='font-weight:bold;'>LEAD EXPOSURE HAZARD REDUCTION PLAN (LEHRP) ACTUAL</span> - This is the method used by the lead abatement contractor to remediate the lead hazard.</p>

<p><span style='font-weight:bold;'>ABATE DATE</span> - The date that the lead inspector or risk assessor re-inspects the surface and finds
that it has been successfully abated for full compliance or lead safe. Full compliance is defined as
no lead paint hazards remain in the home, it has been deleaded. Lead safe means that any lead
based paint remaining in the home has been properly covered and does not pose a threat to the
occupants provided it remains covered, intact and properly maintained.</p>


";
return $data;

?>

