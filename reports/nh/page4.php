<?php


$cuid = getPostIsset('cuid');

$query = "SELECT 
		  units.* ,inspection.* ,insp_assigned.* , 
		  `inspector`.address as inspector_address,
		  `inspector`.email as inspector_email,
		  inspector.*, 
		  building.*,
		  building.address as building_address,
		  `client`.firstname as owner_fname,
		  `client`.address as client_address,
		  `client`.lastname as owner_lname,
		  `client_phone`.number as owner_contact,
		  `inspector_phone`.number as inspector_work_contact

		  FROM units,inspection ,insp_assigned , inspector , building , owners , client , client_phone , inspector_phone
		  WHERE cuid=$cuid AND 
		  `units`.iid = `inspection`.iid AND
		  `inspection`.iid = `insp_assigned`.iid AND
		  `inspection`.bid = `building`.bid AND
		  `building`.bid = `owners`.bid AND
		  `owners`.cid = `client`.cid AND
		  `client_phone`.cid = `client`.cid AND
		  `inspector`.tid = `insp_assigned`.tid AND
		  `inspector`.tid = `inspector_phone`.tid OR
		  `inspector_phone`.tid = NULL AND
		  `inspector_phone`.type = 'Work'
		  ";



$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$data = mysql_fetch_array($result, MYSQL_ASSOC);

$default_top = "10px";

$building_address = issetBlank($data,'building_address');
$prepared_by = issetBlank($data,'firstname') . " " . issetBlank($data,'lastname');
$prepared_for = issetBlank($data,'owner_fname') . " " . issetBlank($data,'owner_lname');
$inspector_pos = "NH Licensed Risk Assessor/Lead";
$inspector_no = "Inspector RA # 0064";
$client_address = issetBlank($data,'client_address');
$client_contact = issetBlank($data,'owner_contact');

$inspector_company_name = "K. Kirkwood Consulting, LLC";
$inspector_address = issetBlank($data,'inspector_address');
$inspector_contact = issetBlank($data,'inspector_work_contact');
$inspector_email = issetBlank($data,'inspector_email');
$property_type = "";
$date_performed = "";

$data = "

<p style='text-align:center; font-weight:bold; font-size:20px; margin-bottom:40px;'> EPA §745.107 Disclosure Requirements for Sellers and Lessors </p>

<p>1) The following activities shall be completed before the purchaser or lessee is obligated under
any contract to purchase or lease target housing this is not otherwise an exempt transaction
pursuant to §745.107. Nothing in this section implies a positive obligation on the seller or lessor
to conduct any evaluation or reduction activity.</p>

	<p style='margin-left:20px;'>
		a) The seller or lessor shall provide the purchaser or lessee with an EPA approved lead
		hazard information pamphlet. Such pamphlets include the EPA document entitled
		'Protect Your Family from Lead in Your Home' (EPA #747- K-94-001) or an equivalent
		pamphlet that has been approved for use in that State by the EPA.
	</p>

	<p style='margin-left:20px;'>
		b) The seller or lessor shall also disclose to the purchaser or lessee the presence of any
		known lead-based paint and/or lead-based paint hazards in the target housing being sold
		or leased. The seller or lessor shall also disclose any additional information available
		concerning the known lead-based paint and/or lead-based paint hazards, such as the
		basis for the determination that lead-based paint and/or lead-based paint hazards exist,
		the location of the lead-based paint and/or lead-based paint hazards, and the condition
		of the painted surfaces.
	</p>

	<p style='margin-left:20px;'>
		c) The seller or lessor shall disclose to each agent the presence of any known lead- based
		paint and/or lead based paint hazards in the target housing being sold or leased and the
		existence of any available records or reports pertaining to lead-based paint and or lead-
		based paint hazards. The seller or lessor shall also disclose any additional information
		available concerning the known lead-based paint and/or lead based paint hazards, such
		as the basis for the determination that lead-based paint and/or lead-based paint hazards
		exist, the location of the lead-based paint and or lead-based paint hazards, and the
		condition of the painted surfaces.
	</p>

	<p style='margin-left:20px;'>
		d) The seller or lessor shall provide the purchaser or lessee with any records or reports
		available to the seller or lessor pertaining to lead-based paint and or lead-based paint
		hazards in the target housing being sold or leased. This requirement includes records or
		reports regarding common areas. This requirement also includes records or reports
		regarding other residential dwellings in multifamily target housing, if such information is
		part of an evaluation or reduction of lead-based paint and/or lead based paint hazards in
		the target housing.
	</p>

<p>
	2) If any of the disclosure activities identified in paragraph (1) of this section occurs after the
	purchaser or lessee has provided an offer to purchase or lease the housing, the seller or lessor
	shall complete the required disclosure activities prior to accepting the purchaser's or lessee's
	offer and allow the purchaser or lessee an opportunity to review the information and possibly
	amend the offer.
</p>
"

;


return $data;

?>