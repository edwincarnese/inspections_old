<?php

require_once'session.php';
require_once'connect.php';
$cid = isset($_POST['cid']) ? $_POST['cid'] : (isset($_GET['cid']) ? $_GET['cid'] : 0);

$title = "Add building";
require_once'header.php';
?>

<form action="building-save.php" method="post" enctype="multipart/form-data">
<?php if ($cid) { ?>
<input type="hidden" name="cid" value="<?php print $cid;?>" />
<?php } ?>
<table>
<tr><td>Total Units:</td><td><input type="text" name="numunits" maxlength="3" length="4" /></td></tr>
<tr><td>Street Address:</td><td><input type="text" name="streetnum" maxlength="10" size="5" value="<?php print isset($_GET['streetnum']) ? $_GET['streetnum'] : ''; ?>"/><input type="text" name="address" maxlength="30" value="<?php print isset($_GET['address']) ? $_GET['address'] : ""; ?>" /><input type="text" name="suffix" maxlength="8" size="7" value="<?php print isset($_GET['suffix']) ? $_GET['suffix'] : ""; ?>"/></td></tr>
<tr><td>Street Address 2:</td><td><input type="text" name="address2" maxlength="30" value="<?php print isset($_GET['address2']) ? $_GET['address2']: ""; ?>"/></td></tr>
<tr><td>City:</td><td><input type="text" name="city" maxlength="30" value="<?php print isset($_GET['city']) ? $_GET['city'] : ''; ?>"/></td></tr>
<tr><td>State:</td><td><input type="text" name="state" maxlength="2" size="3" value="<?php print isset($_GET['state']) ? $_GET['state'] : ''; ?>"/></td></tr>
<tr><td>Zip:</td><td><input type="text" name="zip" maxlength="5" size="6" value="<?php print isset($_GET['zip']) ? $_GET['zip'] : ''; ?>"/></td></tr>
<tr><td>Year Built:</td><td><input type="text" name="yearbuilt" maxlength="6" size="7" value="<?php print isset($_GET['yearbuilt']) ? $_GET['yearbuilt'] : ''; ?>" /></td></tr>
<tr><td>Plat:</td><td><input type="text" name="plat" maxlength="30" /></td></tr>
<tr><td>Lot:</td><td><input type="text" name="lot" maxlength="30" /></td></tr>
<tr><td>Other:</td><td><input type="text" name="other" maxlength="30" /></td></tr>
<tr><td>Insurance Agent:</td><td><input type="text" name="InsuranceAgent" maxlength="50" /></td></tr>
<tr><td>Insurance Company:</td><td><input type="text" name="InsuranceCo" maxlength="50" /></td></tr>
<tr><td>Insurance Policy Number:</td><td><input type="text" name="InsurancePolicyNo" maxlength="20" /></td></tr>
<tr><td>Comments:</td><td><textarea cols="40" rows="5" name="comments"></textarea></td></tr>
<tr><td>Building Image:</td><td><input type="file" name="building_image"></td></tr>

</table>
<p><input type="submit" value="Save" /> <input type="reset" value="Reset" /></p>
</form>

<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>