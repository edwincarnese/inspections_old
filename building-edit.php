<?php

require_once 'session.php';
require_once 'helper.php';

?>
<script language=javascript>
function openerHref() {
	document.update.popup.value = opener.location.href;
}
</script>
<?php 

$title = "Edit building";
if (!isset($_GET['isPopup'])) {
	require_once'header.php';
}
else if ($_GET['isPopup']) {
	echo "<body bgcolor='#dddddd'>";
}
include_once 'connect.php';

$bid = getPostIsset('bid');

$query = "SELECT * FROM building WHERE bid=$bid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/building-list.php");
	exit();
}

$row = mysql_fetch_assoc($result);


?>

<form name="update" action="building-update.php" method="post" enctype="multipart/form-data">
<input type="hidden" name="bid" value="<?php print $bid;?>" />
<p>
Year Built:<input type="text" name="yearbuilt" value="<?php print $row['yearbuilt']; ?>" maxlength="6" size="7" />
Other:<input type="text" name="other" value="<?php print $row['other']; ?>" maxlength="30" />
Plat:<input type="text" name="plat" value="<?php print $row['plat']; ?>" maxlength="30" />
Lot:<input type="text" name="lot" value="<?php print $row['lot']; ?>" maxlength="30" />
<br />

Street#:<input type="text" name="streetnum" value="<?php print $row['streetnum']; ?>" size="6" />
Address:<input type="text" name="address" value="<?php print $row['address']; ?>" />
Suffix:<input type="text" name="suffix" value="<?php print $row['suffix']; ?>" size="6" />
Address 2:<input type="text" name="address2" value="<?php print $row['address2']; ?>" />
<br />
City:<input type="text" name="city" value="<?php print $row['city']; ?>" />
State:<input type="text" name="state" value="<?php print $row['state']; ?>" size="3" />
Zip:<input type="text" name="zip" value="<?php print $row['zip']; ?>" size="6" />
<br />
Number of Units:<input type="text" name="numunits" value="<?php print $row['numunits']; ?>" maxlength="3" size="4" />
Is Building Owner Occupied?:
<?php
$values = array('Yes','No');
foreach ($values as $value) {
	$owneroccupied = issetBlank($row, 'owneroccupied');
	if ( $owneroccupied == $value) {
		print "<input type=\"radio\" name=\"owneroccupied\" value=\"$value\" checked=\"checked\" />$value&nbsp;";
	} else {
		print "<input type=\"radio\" name=\"owneroccupied\" value=\"$value\" />$value&nbsp;";
	}
}
?>

<br />
Insurance Agent:<input type="text" name="InsuranceAgent" value="<?php print $row['InsuranceAgent']; ?>" maxlength="50" />
Insurance Company:<input type="text" name="InsuranceCo" value="<?php print $row['InsuranceCo']; ?>" maxlength="50" />
Policy #:<input type="text" name="InsurancePolicyNo" value="<?php print $row['InsurancePolicyNo']; ?>" maxlength="20" />
<br />
Comments:<textarea cols="40" rows="5" name="comments"><?php print $row['comments']; ?></textarea>
<input type="hidden" name="popup">

<?php
$query = "SELECT * FROM lehrp WHERE bid=$bid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if($row = mysql_fetch_assoc($result)) 
{
$date_performed = $row['date_performed'];
$property_type = $row['property_type'];
$description = $row['description'];
}
?>

<br /><br /><!-- 
Description: <input type="text" name="description" value="<?php print isset($description) ? $description : "";?>" maxlength="50" required/> <br/>
Property Type: <input type="text" name="property_type" value="<?php print isset($property_type) ? $property_type : "";?>" maxlength="50" required/> <br/>
Date Performed: <input type="text" name="date_performed" value="<?php print isset($date_performed) ? $date_performed : "";?>" maxlength="50" required/>
 -->
<br /><br />
Select building image to upload: <br />
<input type="file" name="building_image" id="building_image" accept="image/*">









<table border="1">
<?php 

$query = "SELECT * FROM lehrp WHERE bid=$bid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if($row = mysql_fetch_assoc($result)) 
{
$lerph_pid = $row['pid'];
}

$query = "SELECT * FROM lehrp_images WHERE bid=$bid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while($row = mysql_fetch_assoc($result)) 
{
$image = $row['image_path'];
$pid = $row['pid'];

if($lerph_pid == $pid)
{
echo "
<tr>
<td> <img src='$image' alt='Smiley face' height='120'> </td> <td><input type='radio' name='image_selected' value='$pid' checked> Choose Image<br></td>
</tr>";
}
else
{
echo "
<tr>
<td> <img src='$image' alt='Smiley face' height='120'> </td> <td><input type='radio' name='image_selected' value='$pid'> Choose Image<br></td>
</tr>";
}

}
?>





</table>


<br/><br/>
<p><input type="submit" value="Save" onClick="javascript:openerHref();"/> <input type="reset" value="Reset" /> <input type="submit" name="submit" value="Cancel" onClick="javascript:self.close();"/></p>
</form>

<?php
if (!isset($_GET['isPopup'])) {
	require_once'footer.php';
}
else if ($_GET['isPopup']) {
	echo "</body>";
}
?>