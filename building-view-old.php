<?php 
$title = "View Building";

require_once'session.php';
require_once'connect.php';

$bid = $_POST['bid'] or $bid = $_GET['bid'];

$bid += 0; // (force numbers)

$query = "SELECT * FROM building WHERE bid=$bid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/building-list.php");
	exit();
}

$row = mysql_fetch_assoc($result);
/*
print "<!--\n";
print_r($row);
print "\n-->";
*/

require_once'header.php';
?>
<p><a href="building-edit.php?bid=<?php print $bid; ?>">Edit</a></p>
<p>
<?php
print "$row[streetnum] $row[address] $row[suffix]<br />\n";
if ($row['address2']) {print "$row[address2]<br />\n";}
print "$row[city], $row[state] $row[zip]<br />";

print "Built in: $row[yearbuilt]<br />
Total Units: $row[numunits]<br />
Plat: $row[plat]<br />
Lot: $row[lot]<br />
Other: $row[other]<br />
Insurance Agent: $row[InsuranceAgent]<br />
Insurance Company: $row[InsuranceCo]<br />
Insurance Policy Number: $row[InsurancePolicyNo]</p>\n";

if ($row['comments']) {
	print "\n<p>Comments:<br />$row[comments]</p>";
}

$query = "SELECT client.cid, firstname, lastname FROM owners INNER JOIN client USING (cid) WHERE bid=$bid AND startdate <= DATE(NOW()) AND enddate > DATE(NOW())";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

print "<table>";
while ($row = mysql_fetch_assoc($result) ) {
	print "<tr><td><a href=\"client-view.php?cid=$row[cid]\">View current owner: $row[firstname] $row[lastname]</a></td><td><a href=\"building-removeowner.php?bid=$bid&amp;cid=$row[cid]\">Remove</a></td></tr>";
}
print "</table>";

?>
<p><a href="building-ownerhistory.php?bid=<?php print $bid; ?>">Building Owner History</a></p>
<p><a href="building-addowner.php?bid=<?php print $bid; ?>">Add another owner</a></p>
<p><a href="inspection-schedule.php?bid=<?php print $bid; ?>">Schedule an inspection for this building</a></p>

<?php
$query = "SELECT iid, type, DATE_FORMAT(scheddate, '%c/%e/%Y') AS scheddate FROM inspection WHERE bid=$bid ORDER BY iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) {
	print "This building has not yet been inspected.";
} else {
	print "<p><b>Past inspections:</b><br />\n";
	while ($row = mysql_fetch_assoc($result)) {
		print "<a href=\"inspection-view.php?iid=$row[iid]\">$row[type], $row[scheddate]</a><br />\n";
	}
	print "</p>";
}
?>

<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>