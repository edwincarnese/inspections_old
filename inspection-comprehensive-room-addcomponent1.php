<?php

require_once'session.php';
require_once'connect.php';

$crid = $_POST['crid'] or $crid = $_GET['crid'] or $crid = 0;

$query = "SELECT * FROM comprehensive_rooms INNER JOIN units USING (cuid) WHERE crid=$crid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}
$room = mysql_fetch_assoc($result);
$iid = $room['iid'];
$unitnumber = $room['number']; //units is second table and will overwrite room #

$query = "SELECT * FROM room_item_list WHERE type='Interior'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - $room[name] - Add Component";
require_once'header.php';
?>
<p>Choose the components to add.</p>
<form action="inspection-comprehensive-room-addcomponent2.php" method="post">
<input type="hidden" name="crid" value="<?php print $crid; ?>" />
<p>
<?php

while ($row = mysql_fetch_assoc($result)) {
	print "<input type=\"checkbox\" name=\"itemid[]\" value=\"$row[itemid]\" />$row[itemname]<br />\n";
}
?>
<input type="submit" value="Next" />
</p>
</form>

<p><a href="inspection-comprehensive-room.php?crid=<?php print $room[crid]; ?>">Room Main Menu</a></p>

<p><a href="inspection-comprehensive-unit.php?cuid=<?php print $room[cuid]; ?>"><?php print $room['unitdesc']; ?> Main Menu</a></p>

<p><a href="inspection-main.php?iid=<?php print $room[iid]; ?>">Inspection Main Menu</a></p>

<?php
require_once'footer.php';
?>