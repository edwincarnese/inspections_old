<?php
/*
 * Created on Jan 24, 2006
 *
 * Author: Jason Rankin
 */
?>
<?
require_once "session.php";
require_once "connect.php";


$SYNC_CONTROL_FILE = "sync-control.txt";
if ($_POST['submit']) {
	$SYNC_CONTROL_VALUE = $_POST['sync'];
	$SYNC_CONTROL_MESSAGE = $_POST['message'];
	
	if (strstr($SYNC_CONTROL_VALUE,"On")) {
		$fp = fopen($SYNC_CONTROL_FILE, "w") or die("couldn't open/create file");
		fwrite($fp, "On\r\n");
		fwrite($fp, $SYNC_CONTROL_MESSAGE);
		fclose($fp);
	} else {
		$fp = fopen($SYNC_CONTROL_FILE, "w");
		fwrite($fp, "Off\r\n");
		fwrite($fp, $SYNC_CONTROL_MESSAGE);
		fclose($fp);
	}
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/sync-control.php");
} else {
	require_once "header.php";
	$query = "SELECT userlevel FROM users WHERE uid=$sid";
	$result = mysql_query($query) or sql_crapout($query."<br>".mysql_error());
	$userlevel = mysql_result($result, 0);
	$fp = fopen($SYNC_CONTROL_FILE, "r");
	$val = fgets($fp);
	while (!feof($fp)) {
	  $printval .= fread($fp, 8192);
	}
	fclose($fp);
	
	if ($userlevel >= 5 ) {
		print "<form method='POST' action='$_SERVER[PHP_SELF]'>";
		print "<p>Sync: </p>";
		print "<p><input type='radio' name='sync' value='On'";
		if (strstr($val,"On")) {
			print " checked='checked'";
		}
		print " >On</input><br>";
		print "<input type='radio' name='sync' value='Off'";
		if (strstr($val,"Off")) {
			print " checked='checked'";
		}
		print " >Off</input><br>";
		print "Message: <br><textarea name='message' rows='10' cols='40'>$printval</textarea></p>";
		print "<input type='submit' name='submit' value='save'/></form>";
	}
}