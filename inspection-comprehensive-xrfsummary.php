<?php

require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;
$iid = $_POST['iid'] or $iid = $_GET['iid'] or $iid = 0;
$popup = $_GET['isPopup'];

$userlevelquery = "SELECT userlevel FROM users WHERE uid=$_SESSION[sid]";
$userlevelresult = mysql_query($userlevelquery) or sql_crapout($userlevelquery.'<br />'.mysql_error());
$userlevel = mysql_result($userlevelresult, 0);

$unitnumber = '*';
if ($cuid) {
	$query = "SELECT iid, number, unitdesc FROM units WHERE cuid=$cuid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	list($iid, $unitnumber, $unitdesc) = mysql_fetch_row($result);
}

$query = "SELECT DATE_FORMAT(timetaken, '%l:%i:%s %p') AS timetaken, manufacturer, serialnumber, standardtype, standard, temperature, reading1, reading2, reading3 FROM comprehensive_calibration WHERE iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);
if (!$popup){
	$title = "$iid-$unitnumber - $address - XRF summary";
	require_once'header.php';
} else {
		?><head>
	<link rel="stylesheet" type="text/css" href="style.css" />
	</head>
	
		<div id="content">
	<?
}

?>
<table class="info">
<tr><th>Time Taken</th><th>Manufacturer</th><th>Serial No.</th><th>Standard Type</th><th>Standard</th><th>Temp</th><th>#1</th><th>#2</th><th>#3</th><th>Avg</th><th>Spread</th></tr>
<?php
while($row = mysql_fetch_assoc($result)) {
	print '<tr>';
	foreach ($row as $val) {
		if ($userlevel > 4){
		print "<td><a href=\"inspection-xrfsummary-edit.php?cuid=$cuid&iid=$iid&timetaken=$row[timetaken]\">$val</a></td>";}
		else{print "<td>$val</td>";}
	}
	$avg = round(($row['reading1'] + $row['reading2'] + $row['reading3']) / 3, 2);
	print "<td>$avg</td>";
	$min = $max = $row['reading1'];
	if ($row['reading2'] < $min) {
		$min = $row['reading2'];
	} else {
		$max = $row['reading2'];
	}
	if ($row['reading3'] < $min) {
		$min = $row['reading3'];
	}
	if ($row['reading3'] > $max) {
		$max = $row['reading3'];
	}
	$spread = $max - $min;
	print "<td>$spread</td>";
	print '<tr>';
}
?>
</table>

<?php
if ($cuid) {
	$query = "SELECT comment FROM comprehensive_comments WHERE cuid=$cuid AND type='xrf'";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	if (mysql_num_rows($result)) {
		$comments = mysql_result($result,0);
	}
?>
<p>Unit / XRF comments: <a href="inspection-comprehensive-xrf-comments.php?cuid=<?php print $cuid; ?>">Edit</a><br />
<?php print $comments; ?>
</p>

<p><a href="inspection-comprehensive-unit.php?cuid=<?php print $cuid; ?>"><?php print $unitdesc; ?> Main Menu</a></p>
<?php
}
if (!$popup) {?>
<p><a href="inspection-main.php?iid=<?php print $iid; ?>">Inspection Main Menu</a></p>
<?
} else {
	print "<p><a href='javascript:self.close();'>Return to inspection</a></p>";
}

if (!$popup){
	require_once'footer.php';
} else {
?></div>
<?}?>
