<?php

$ml=0.625;
$w=7.25;

$pdf->AddPage('P');
//$pdf->Image('images/fixed/comprehensive-soil-test.png', 0, 0, 8.5, 11.0, 'PNG');
$pdf->SetFont('Arial','',14);
//ADDRESS, TOP
$query = "SELECT inspection.iid, streetnum, address, suffix, city, DATE_FORMAT(starttime, '%c/%e/%Y') AS start, unitdesc, number FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);
$pdf->SetLineWidth(0.012);
$y=.2;
//Inspection #
$pdf->SetFont('Times','B','10');
$pdf->SetXY(1.5, $y);
$pdf->Cell(0,0, "Inspection $row[iid]-$row[number]");
//PAGE NUMBERS
$pdf->SetXY(6.75,$y);
$pdf->Cell(0,0, 'Page '. $pdf->PageNo().' of  '. '{totalpages}');

//Interior Inspection Banner
$x=2.5 ;
$y=$y+.31;
$pdf->SetFont('Times','B','12');
$pdf->SetXY($ml, $y);
$pdf->Cell($w, 0.3,'Soil Inspection', 1, 0, 'C');
$pdf->SetFont('Times','','10');
$y=$y+.21;

$iid = $row['iid'];

//Unit Specifications
//ADDRESS, TOP
$query = "SELECT inspection.iid, streetnum, address, suffix, city, unitdesc, starttime, dustLab, number FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

$pdf->SetFont('Times','','10');
//Unit and address
$ld=0.07;
$x=$ml;
$y=$y+.35;
$pdf->SetXY($x, $y);
$pdf->Cell($x, 0, 'Floor / Apt. No.');
$pdf->SetXY($x, $y);
$pdf->Line($ml+1, $y+$ld, $ml+2.2, $y+$ld);
//$pdf->Rect($x+1.2, $y, 1, 0);
$pdf->SetXY($x+1.3, $y);
$pdf->Cell(0, 0, $row['unitdesc']);
$x=3;
$pdf->SetXY($x, $y);
$pdf->Cell($x, 0, 'Street /City:');
//$pdf->SetXY($x, $y);
$pdf->Line($ml+3.25, $y+$ld, $ml+7.25, $y+$ld);
$pdf->SetXY($x+1.3, $y);
$pdf->Cell(0,0, "$row[streetnum] $row[address] $row[suffix], $row[city]");

//Sampling Date line
$x=$ml;
$y=$y+.25;
$pdf->SetXY($x, $y);
$pdf->Cell($x, 0, 'Sampling Date:');
$pdf->Line($ml+1, $y+0.05, $ml+2.375, $y+0.05);
$pdf->SetXY($x+1.1, $y-0.05);
$pdf->Cell(1.275, 0, $row['starttime'], 0, 0, 'C');
$x=$x+2.875;
$pdf->SetXY($x, $y);
$pdf->Cell(0, 0, 'Laboratory Utilized:');
$pdf->Line($x+1.25, $y+0.05, $ml+$w, $y+0.05);
//$pdf->Cell(3.25,0, $row['dustLab'], 0, 0, 'C');
$pdf->SetXY($x+1.3, $y-0.05);
$pdf->Cell(3.0,0, 'Dust Lab', 0, 0, 'C');

//Table Headings 
$x=1.625;
$y=$y+.14;
$h=0.53;
$w=0.625;
$pdf->Rect($ml+$x, $y, $w, $h);
$pdf->SetFont('Times','','10');
$pdf->SetXY($ml+$x, $y+.1);
$pdf->Cell(0,0, ' Distance');
$pdf->SetXY($ml+$x, $y+.25);
$pdf->Cell(0,0, '   From');
$pdf->SetXY($ml+$x, $y+.4);
$pdf->Cell(0,0, ' Structure');

$x=$x+$w;
$h=0.53;
$w=0.6875;
$pdf->Rect($ml+$x, $y, $w, $h);
$pdf->SetXY($ml+$x, $y+.25);
$pdf->Cell(0,0, '    Depth');
$pdf->SetXY($ml+$x, $y+.4);
$pdf->Cell(0,0, '    ft / in');

$x=$x+$w;
$h=0.53;
$w=0.625;
$pdf->Rect($ml+$x, $y, $w, $h);
$pdf->SetXY($ml+$x, $y+.25);
$pdf->Cell(0,0, ' Exposed');
$pdf->SetXY($ml+$x, $y+.4);
$pdf->Cell(0,0, '   Y / N');

$x=$x+$w;
$h=0.53;
$w=0.9375;
$pdf->Rect($ml+$x, $y, $w, $h);
$pdf->SetXY($ml+$x, $y+.1);
$pdf->Cell(0,0, '      Ground');
$pdf->SetXY($ml+$x, $y+.25);
$pdf->Cell(0,0, '       Cover');
$pdf->SetXY($ml+$x, $y+.4);
$pdf->Cell(0,0, '     P / G / M');

$x=$x+$w;
$h=0.53;
$w=0.875;
$pdf->Rect($ml+$x, $y, $w, $h);
$pdf->SetXY($ml+$x, $y+.25);
$pdf->Cell(0,0, '  Lab Sample');
$pdf->SetXY($ml+$x, $y+.4);
$pdf->Cell(0,0, '    Number');

$x=$x+$w;
$h=0.53;
$w=0.875;
$pdf->Rect($ml+$x, $y, $w, $h);
$pdf->SetXY($ml+$x, $y+.25);
$pdf->Cell(0,0, '     Results');
$pdf->SetXY($ml+$x, $y+.4);
$pdf->Cell(0,0, '       (ppm)');

$x=$x+$w;
$h=0.53;
$w=1.0;
$pdf->Rect($ml+$x, $y, $w, $h);
$pdf->SetFont('Times','','10');
$pdf->SetXY($ml+$x+0.25, $y+.1);
$pdf->Cell(0,0, 'Hazard');
$pdf->SetXY($ml+$x+0.1, $y+.25);
$pdf->Cell(0,0, 'Assessment');
$pdf->SetXY($ml+$x+0.2, $y+.4);
$pdf->Cell(0,0, 'F / S / H');

//Build Table
$w=7.25;
$y=$y+$h;
$x=$ml;
$h=1;
$rowstable=5;

//Sides
$pdf->Line($x, $y, $x, $y+$h);
$x=$x+1.625;
$pdf->Line($x, $y, $x, $y+$h);
$x=$x+0.625;
$pdf->Line($x, $y, $x, $y+$h);
$x=$x+0.6875;
$pdf->Line($x, $y, $x, $y+$h);
$x=$x+0.625;
$pdf->Line($x, $y, $x, $y+$h);
$x=$x+0.9375;
$pdf->Line($x, $y, $x, $y+$h);
$x=$x+0.8750;
$pdf->Line($x, $y, $x, $y+$h);
$x=$x+0.8750;
$pdf->Line($x, $y, $x, $y+$h);
$x=$x+1.000;
$pdf->Line($x, $y, $x, $y+$h);


$hr=$h/$rowstable;
$c=0;

while ($c < ($hr*($rowstable+.9))) { 
	$pdf->Line($ml, $y+$c, $ml+$w, $y+$c);
	$c=$c+$hr;
}

$y=$y+0.1;
$pdf->SetXY($ml+1, $y);
$pdf->Cell(0, 0, 'Side 1');
$y=$y+0.2;
$pdf->SetXY($ml+1, $y);
$pdf->Cell(0, 0, 'Side 2');
$y=$y+0.2;
$pdf->SetXY($ml+1, $y);
$pdf->Cell(0, 0, 'Side 3');
$y=$y+0.2;
$pdf->SetXY($ml+1, $y);
$pdf->Cell(0, 0, 'Side 4');
$y=$y+0.2;
$pdf->SetXY($ml+0.75, $y);
$pdf->Cell(0, 0, 'Composite');


$pdf->SetFont('Times','B','12');
$pdf->SetXY($ml+2.35, $y+0.25);
$pdf->Cell(0, 0, 'Soil Around Other Exterior Objects');
$pdf->SetFont('Times','','10');
$pdf->SetXY($ml+2.15, $y+.45);
$pdf->Cell(0, 0, '( Fences, Garages, Swintg Sets, Sheds, Tables, etc. )');

$pdf->SetFont('Times','','10');

//Table Headings Other
$x=0;
$y=$y+.55;
$h=0.53;
$w=1.625;
$pdf->Rect($ml+$x, $y, $w, $h);
$pdf->SetFont('Times','','10');
$pdf->SetXY($ml+$x+0.475, $y+.25);
$pdf->Cell(0,0, 'Object / Side');

$x=$x+$w;
$h=0.53;
$w=0.625;
$pdf->Rect($ml+$x, $y, $w, $h);
$pdf->SetFont('Times','','10');
$pdf->SetXY($ml+$x, $y+.1);
$pdf->Cell(0,0, ' Distance');
$pdf->SetXY($ml+$x, $y+.25);
$pdf->Cell(0,0, '   From');
$pdf->SetXY($ml+$x, $y+.4);
$pdf->Cell(0,0, ' Structure');

$x=$x+$w;
$h=0.53;
$w=0.6875;
$pdf->Rect($ml+$x, $y, $w, $h);
$pdf->SetXY($ml+$x, $y+.25);
$pdf->Cell(0,0, '    Depth');
$pdf->SetXY($ml+$x, $y+.4);
$pdf->Cell(0,0, '    ft / in');

$x=$x+$w;
$h=0.53;
$w=0.625;
$pdf->Rect($ml+$x, $y, $w, $h);
$pdf->SetXY($ml+$x, $y+.25);
$pdf->Cell(0,0, ' Exposed');
$pdf->SetXY($ml+$x, $y+.4);
$pdf->Cell(0,0, '   Y / N');

$x=$x+$w;
$h=0.53;
$w=0.9375;
$pdf->Rect($ml+$x, $y, $w, $h);
$pdf->SetXY($ml+$x, $y+.1);
$pdf->Cell(0,0, '      Ground');
$pdf->SetXY($ml+$x, $y+.25);
$pdf->Cell(0,0, '       Cover');
$pdf->SetXY($ml+$x, $y+.4);
$pdf->Cell(0,0, '     P / G / M');

$x=$x+$w;
$h=0.53;
$w=0.875;
$pdf->Rect($ml+$x, $y, $w, $h);
$pdf->SetXY($ml+$x, $y+.25);
$pdf->Cell(0,0, '  Lab Sample');
$pdf->SetXY($ml+$x, $y+.4);
$pdf->Cell(0,0, '    Number');

$x=$x+$w;
$h=0.53;
$w=0.875;
$pdf->Rect($ml+$x, $y, $w, $h);
$pdf->SetXY($ml+$x, $y+.25);
$pdf->Cell(0,0, '     Results');
$pdf->SetXY($ml+$x, $y+.4);
$pdf->Cell(0,0, '       (ppm)');

$x=$x+$w;
$h=0.53;
$w=1.0;
$pdf->Rect($ml+$x, $y, $w, $h);
$pdf->SetFont('Times','','10');
$pdf->SetXY($ml+$x+0.25, $y+.1);
$pdf->Cell(0,0, 'Hazard');
$pdf->SetXY($ml+$x+0.1, $y+.25);
$pdf->Cell(0,0, 'Assessment');
$pdf->SetXY($ml+$x+0.2, $y+.4);
$pdf->Cell(0,0, 'F / S / H');

//Build Table Sides
$w=7.25;
$y=$y+$h;
$x=$ml;
$h=1.6;
$rowstable=8;

//Sides
$pdf->Line($x, $y, $x, $y+$h);
$x=$x+1.625;
$pdf->Line($x, $y, $x, $y+$h);
$x=$x+0.625;
$pdf->Line($x, $y, $x, $y+$h);
$x=$x+0.6875;
$pdf->Line($x, $y, $x, $y+$h);
$x=$x+0.625;
$pdf->Line($x, $y, $x, $y+$h);
$x=$x+0.9375;
$pdf->Line($x, $y, $x, $y+$h);
$x=$x+0.8750;
$pdf->Line($x, $y, $x, $y+$h);
$x=$x+0.8750;
$pdf->Line($x, $y, $x, $y+$h);
$x=$x+1.000;
$pdf->Line($x, $y, $x, $y+$h);


$hr=$h/$rowstable;
$c=0;

while ($c < ($hr*($rowstable+.9))) { 
	$pdf->Line($ml, $y+$c, $ml+$w, $y+$c);
	$c=$c+$hr;
}

$y=$y+$h;
$pdf->SetFont('Times','','12');
$pdf->SetXY($ml+1.5, $y+0.25);
$pdf->Cell(0, 0, 'Exterior soil sampling not performed on date of inspection due to:');
$pdf->SetFont('Times','','10');
$pdf->SetXY($ml+0.3, $y+.5);
$pdf->Cell(0, 0, 'Surfaces:  Wet / rain ______      Covered by debris ______      Covered by ice / snow ______      Other: _____________');

$pdf->SetFont('Times','B','12');
$pdf->SetXY($ml+2.9, $y+.75);
$pdf->Cell(0, 0, 'Exterior Debris');

$pdf->SetFont('Times','','10');

//Table Headings 
$x=0.5;
$y=$y+1.0;
$h=0.53;
$w=4.0;
$pdf->Rect($ml+$x, $y, $w, $h);
$pdf->SetFont('Times','','10');
$pdf->SetXY($ml+$x, $y+.1);
$pdf->Cell(0,0, '');
$pdf->SetXY($ml+$x+1.250, $y+.25);
$pdf->Cell(0,0, 'Location on Side 1, 2, 3 or 4');
$pdf->SetXY($ml+$x, $y+.4);
$pdf->Cell(0,0, '');

$x=$x+$w;
$h=0.53;
$w=0.875;
$pdf->Rect($ml+$x, $y, $w, $h);
$pdf->SetXY($ml+$x, $y+.25);
$pdf->Cell(0,0, '  Lab Sample');
$pdf->SetXY($ml+$x, $y+.4);
$pdf->Cell(0,0, '    Number');

$x=$x+$w;
$h=0.53;
$w=0.875;
$pdf->Rect($ml+$x, $y, $w, $h);
$pdf->SetXY($ml+$x, $y+.25);
$pdf->Cell(0,0, '     Results');
$pdf->SetXY($ml+$x, $y+.4);
$pdf->Cell(0,0, '       (ppm)');

$x=$x+$w;
$h=0.53;
$w=1.0;
$pdf->Rect($ml+$x, $y, $w, $h);
$pdf->SetFont('Times','','10');
$pdf->SetXY($ml+$x+0.25, $y+.1);
$pdf->Cell(0,0, 'Hazard');
$pdf->SetXY($ml+$x+0.1, $y+.25);
$pdf->Cell(0,0, 'Assessment');
$pdf->SetXY($ml+$x+0.2, $y+.4);
$pdf->Cell(0,0, 'F / S / H');

//Build Table
$w=7.25;
$y=$y+$h;
$x=$ml;
$h=0.8;
$rowstable=4;

//Sides
$pdf->Line($x, $y, $x, $y+$h);
$x=$x+0.5;
$pdf->Line($x, $y, $x, $y+$h);
$x=$x+4.0;
$pdf->Line($x, $y, $x, $y+$h);
$x=$x+0.8750;
$pdf->Line($x, $y, $x, $y+$h);
$x=$x+0.8750;
$pdf->Line($x, $y, $x, $y+$h);
$x=$x+1.000;
$pdf->Line($x, $y, $x, $y+$h);


$hr=$h/$rowstable;
$c=0;

while ($c < ($hr*($rowstable+.9))) { 
	$pdf->Line($ml, $y+$c, $ml+$w, $y+$c);
	$c=$c+$hr;
}

$y=$y+0.1;
$pdf->SetXY($ml+0.05, $y);
$pdf->Cell(0, 0, 'Side 1');
$y=$y+0.2;
$pdf->SetXY($ml+0.05, $y);
$pdf->Cell(0, 0, 'Side 2');
$y=$y+0.2;
$pdf->SetXY($ml+0.05, $y);
$pdf->Cell(0, 0, 'Side 3');
$y=$y+0.2;
$pdf->SetXY($ml+0.05, $y);
$pdf->Cell(0, 0, 'Side 4');

$pdf->SetFont('Times','B','12');
$pdf->SetXY($ml+2.9, $y+.25);
$pdf->Cell(0, 0, 'Key to Headings');
$pdf->SetFont('Times','','10');

$x=0.5;
$y=$y+.4;
$h=0.4;
$w=1.5;

$pdf->Line($ml+$x, $y, $ml+$x+(4*$w), $y);
$pdf->Line($ml+$x, $y+($h/2), $ml+$x+(4*$w), $y+($h/2));
$pdf->Line($ml+$x, $y+($h), $ml+$x+(4*$w), $y+($h));

$pdf->Line($ml+$x, $y, $ml+$x, $y+$h);
$x=$x+$w;
$pdf->Line($ml+$x, $y, $ml+$x, $y+$h);
$x=$x+$w;
$pdf->Line($ml+$x, $y, $ml+$x, $y+$h);
$x=$x+$w;
$pdf->Line($ml+$x, $y, $ml+$x, $y+$h);
$x=$x+$w;
$pdf->Line($ml+$x, $y, $ml+$x, $y+$h);


$pdf->SetFont('Times','','10');
$x=0.65;
$pdf->SetXY($ml+$x, $y+.1);
$pdf->Cell(0,0, '    Ground Cover');
$pdf->SetXY($ml+$x, $y+.3);
$pdf->Cell(0,0, 'Hazard Assessment');

$x=$x+$w;
$pdf->SetXY($ml+$x, $y+.1);
$pdf->Cell(0,0, '   P = Pavement');
$pdf->SetXY($ml+$x, $y+.3);
$pdf->Cell(0,0, '  F =  Lead-Free');

$x=$x+$w;
$pdf->SetXY($ml+$x, $y+.1);
$pdf->Cell(0,0, '     G = Grass');
$pdf->SetXY($ml+$x, $y+.3);
$pdf->Cell(0,0, '   S =  Lead-Safe');

$x=$x+$w;
$pdf->SetXY($ml+$x, $y+.1);
$pdf->Cell(0,0, '     M = Mulch');
$pdf->SetXY($ml+$x, $y+.3);
$pdf->Cell(0,0, '    H =  Hazard');

// Comments
$y=$y+.6;
$x=$ml;
$h=1;
$pdf->SetLineWidth(0.01);
$pdf->Line($ml, $y, $ml, $y+$h);
$pdf->Line($ml+7.25, $y, $ml+7.25, $y+$h);


$rowstable=5;
$hr=$h/$rowstable;
$c=0;

while ($c < ($hr*($rowstable+.9))) { 
	$pdf->Line($ml, $y+$c, $ml+7.25, $y+$c);
	$c=$c+$hr;
}

$y=$y+0.1;
$pdf->SetXY($ml, $y);
$pdf->Cell(0, 0, ' Comments:');


//eeeeeeeeeeeeeeeeeeeeeeeeeee
//SIDES
$query = "SELECT * FROM units INNER JOIN comprehensive_sides USING (cuid) WHERE iid=$iid ORDER BY comprehensive_sides.side";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$basex = $ml+1.625; $basey = 2.1; $mody = 0;

//$ml, 1.625, 0.625, 0.625, 0.6875, 0.625, 0.9375, 0.8750, 0.8750, 1.00

while ($row = mysql_fetch_assoc($result) ) {
	formfix($row);
	$pdf->SetXY($basex, $basey + $mody);
	if ($row['inaccessible'] != 'No') {
		$pdf->Cell(0,0, "Inaccessible: $row[inaccessible]");
	} else {
		$pdf->Cell(0.625, 0, $row['distance'], 0, 0, 'C');
		$pdf->SetXY($basex + 0.625, $basey + $mody);
		if ($row['cover'] != 'Pavement') {
			$pdf->Cell(0.6875, 0, $row['depth'], 0, 0, 'C');
		}
		$pdf->SetXY($basex + 1.3125, $basey + $mody);
		if ($row['cover'] == 'None') {
			$pdf->Cell(0.625, 0, 'Y', 0, 0, 'C');
		} else {
			$pdf->Cell(0.625, 0, 'N', 0, 0, 'C');
		}
		$pdf->SetXY($basex + 1.9375, $basey + $mody);
		if ($row['cover'] != 'None') {
			$pdf->Cell(0.9375, 0, substr($row['cover'],0,1), 0, 0, 'C');
		}
		$pdf->SetXY($basex + 2.875, $basey + $mody);
		if ($row['cover'] != 'Pavement') {
			$pdf->Cell(0.8750,0, "$iid-$unitnumber"."SS$row[side]", 0, 0, 'C');
		}
		$pdf->SetXY($basex + 3.75, $basey + $mody);
		$pdf->Cell(0.875,0, $row['results'], 0, 0, 'C');
		$pdf->SetXY($basex + 4.675, $basey + $mody);		
		if ($row['hazardassessment'] == 'Lead-free') {
			$pdf->Cell(0.6, 0,'F', 0, 0, 'L');
		} else if ($row['hazardassessment'] == 'Lead-safe') {
			$pdf->Cell(0.6,0,'S', 0, 0, 'L');
		} else if ($row['hazardassessment'] == 'Hazard') {
			$pdf->Cell(0.6,0,'H', 0, 0, 'R');
		}
	}
	$mody += 0.2;
}

//OBJECTS
$query = "SELECT * FROM units INNER JOIN comprehensive_soil_object USING (cuid) WHERE iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

$basex = $ml; $basey = 4.1; $mody = 0;

while ($row = mysql_fetch_assoc($result) ) {
	formfix($row);
	$pdf->SetXY($basex, $basey + $mody);
	$pdf->Cell(1.625, 0, $row['object'].' - '.$row['side'], 0, 0, 'C');
	$pdf->SetXY($basex + 1.625, $basey + $mody);	
	$pdf->Cell(0.625, 0, $row['distance'], 0, 0, 'C');
	$pdf->SetXY($basex + 2.25, $basey + $mody);
	$pdf->Cell(0.6875, 0, $row['depth'].'"', 0, 0, 'C');
	$pdf->SetXY($basex + 2.9375, $basey + $mody);
	if ($row['cover'] == 'None') {
		$pdf->Cell(0.625, 0, 'Y', 0, 0, 'C');
	} else {
		$pdf->Cell(0.625, 0, 'N', 0, 0, 'C');
	}
	$pdf->SetXY($basex + 3.5625, $basey + $mody);
	if ($row['cover'] != 'None') {
		$pdf->Cell(0.9375, 0, substr($row['cover'],0,1), 0, 0, 'C');
	}
	$pdf->SetXY($basex + 4.5, $basey + $mody);
	$pdf->Cell(0.875, 0, "$iid-$unitnumber"."SO$row[number]", 0, 0, 'C');
	$pdf->SetXY($basex + 5.375, $basey + $mody);
	$pdf->Cell(0.875,0, $row['results'], 0, 0, 'C');
	$pdf->SetXY($basex + 6.45, $basey + $mody);
	
	if ($row['hazardassessment'] == 'Lead-free') {
		$pdf->Cell(0.6,0,'F', 0, 0, 'L');
	} else if ($row['hazardassessment'] == 'Lead-safe') {
		$pdf->Cell(0.6,0,'S', 0, 0, 'L');
	} else if ($row['hazardassessment'] == 'Hazard') {
		$pdf->Cell(0.6,0,'H', 0, 0, 'R');
	}
	$mody += 0.2;
}

//DEBRIS
$query = "SELECT * FROM units INNER JOIN comprehensive_soil_debris USING (cuid) WHERE iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) > 4) {
	exit('Too many debris samples.');
}

$basex =$ml; $basey = 7.2; $mody = 0;

while ($row = mysql_fetch_assoc($result) ) {
	formfix($row);
	$pdf->SetXY($basex, $basey + $mody);
	$pdf->Cell(0,0, "Side $row[side]");
	$pdf->SetXY($basex + (1.42 - 0.44), $basey + $mody);	
	$pdf->Cell(0,0, $row['location']);
	$pdf->SetXY($basex + (2.50 - 0.44), $basey + $mody);	
	$pdf->Cell(0,0, $row['debris']);
	$pdf->SetXY($basex + (3.50 - 0.44), $basey + $mody);
	$pdf->Cell(0,0, $row['distance']."'");
	$pdf->SetXY($basex + (4.45 - 0.44), $basey + $mody);
	$pdf->Cell(0,0, $row['depth'].'"');
	$pdf->SetXY($basex + 4.5, $basey + $mody);
	$pdf->Cell(0.875,0, "$iid-$unitnumber"."SD$row[number]", 0, 0, 'C');
	$pdf->SetXY($basex + 5.375, $basey + $mody);
	$pdf->Cell(0.875, 0, $row['results'], 0, 0, 'C');
	$pdf->SetXY($basex + 6.45, $basey + $mody);
	
	if ($row['hazardassessment'] == 'Lead-free') {
		$pdf->Cell(0.6,0,'F', 0, 0, 'L');
	} else if ($row['hazardassessment'] == 'Lead-safe') {
		$pdf->Cell(0.6,0,'S', 0, 0, 'L');
	} else if ($row['hazardassessment'] == 'Hazard') {
		$pdf->Cell(0.6,0,'H', 0, 0, 'R');
	}
	$mody += 0.2;
}



//COMMENTS
$query = "SELECT comment FROM comprehensive_comments WHERE cuid=$cuid AND type='soil'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while ($row = mysql_fetch_row($result)) {
	formfix($row);
	$lh = 0.23;
	$pdf->SetXY(0.41, 8.82);
	$pdf->MultiCell(7.72, $lh, $row[0]);
}

$query = "SELECT DATE_FORMAT(starttime, '%c/%e/%Y') AS startdate, DATE_FORMAT(starttime, '%l:%i %p') AS starttime FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_row($result);


$pdf->SetXY(6, 10.10);
$pdf->Cell(0,0, '___________  /  ___________ ');
$pdf->SetXY(7.05, 10.10);
$pdf->Cell(0,0, $row[0]);
$pdf->SetXY(6, 10.30);
$pdf->Cell(0,0, '      Initials                 Date ');

$pdf->SetFont('Times','','9');
$pdf->SetXY($ml, 10.60);
$pdf->Cell(0,0, 'FORM PBLC-23-8 (9/97) Replaces OEHRA II (4/97) p.8, which may be used.');

?>