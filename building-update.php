<?

require_once'session.php';
?>
<script language=javascript>
function closePopUp() {
	popup = '<?print $_POST['popup'];?>';
	if (popup != '') {
		opener.location.reload(true);
		self.close();
	}
}
</script>
<body onLoad="javascript:closePopUp();"></body>
<?php


require_once'connect.php';

$bid = $_POST['bid'];
$popup = $_POST['popup'];

unset($_POST['bid']);
unset($_POST['popup']);
unset($_POST['description']);
unset($_POST['property_type']);
unset($_POST['date_performed']);

foreach ($_POST as $field => $value) {
  $fieldlist[] = $field;
  $valuelist[] = htmlspecialchars($value);
}
$valuelist[0] = (int) $valuelist[0];


$fieldlist[count($fieldlist)] = "image_name";
$valuelist['image_name'] = uploadImage();

foreach ($_POST as $field => $value) {
    $updates[] = $field.'='."'".htmlspecialchars($value)."'"; 
  
}

if($valuelist['image_name'] != "") {
    $updates[count($updates)] = 'image_name'.'='."'".htmlspecialchars($valuelist['image_name'])."'";
}


$query = "UPDATE building SET ".implode(', ', $updates)." WHERE bid=$bid";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

echo "Update successfull. Close this window to proceed";

function random_string($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}


function uploadImage() {
  if(!isset($_FILES["building_image"])){
    return "";
  }
  $target_dir = "images/buildings/";
  $target_file = $target_dir . random_string(4).basename($_FILES["building_image"]["name"]);

  $uploadOk = 1;
  $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
  // Check if image file is a actual image or fake image
  if(isset($_POST["submit"])) {
      $check = getimagesize($_FILES["building_image"]["tmp_name"]);
      if($check !== false) {
          echo "File is an image - " . $check["mime"] . ".";
          $uploadOk = 1;
      } else {
          echo "File is not an image.";
          $uploadOk = 0;
      }
  }
  // Check if file already exists
  if (file_exists($target_file)) {
      echo "Sorry, file already exists.";
      $uploadOk = 0;
  }



  // Check file size
  if ($_FILES["building_image"]["size"] > 500000) {
      echo "Sorry, your file is too large.";
      $uploadOk = 0;
  }
  // Allow certain file formats
  if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
  && $imageFileType != "gif" ) {
      echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
      $uploadOk = 0;
  }
  // Check if $uploadOk is set to 0 by an error
  if ($uploadOk == 0) {
      echo "Sorry, your file was not uploaded.";
  // if everything is ok, try to upload file
  } else {
      if (move_uploaded_file($_FILES["building_image"]["tmp_name"], $target_file)) {
          return $target_file;
      } else {
          echo "Sorry, there was an error uploading your file.";
          
      }
  }


  die;
  

}







unset($_POST['bid']);
unset($_POST['submit']);

//shortcut - all fields are text
foreach ($_POST as $field => $value) {
	if ($field != 'popup'){
		$updates[] = $field.'='."'".htmlspecialchars($value)."'"; //see note in building-save.php
	}
}

$query = "UPDATE building SET ".implode(', ', $updates)." WHERE bid=$bid";
mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if ($popup == '') {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/building-view.php?bid=$bid");
}











?>