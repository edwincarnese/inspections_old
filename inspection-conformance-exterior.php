<?php
require_once'session.php';
require_once'connect.php';

$cuid = $_POST['cuid'] or $cuid = $_GET['cuid'] or $cuid = 0;
$error = $_GET['error'];
$query = "SELECT iid, starttime, number FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/inspection-list.php");
	exit();
}

list($iid, $starttime, $unitnumber) = mysql_fetch_row($result);

$query = "SELECT count(*) FROM conformance_sides WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$sides = mysql_result($result, 0);

if ($starttime < '1000-01-01' || $sides < 4) {
	$query = "UPDATE units SET starttime=NOW() WHERE cuid=$cuid";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	
	//pre-fill exterior
	$query = "DELETE FROM conformance_sides WHERE cuid=$cuid";
	mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$sidearray = array('1' => 'Front', '2' => 'Left', '3' => 'Rear', '4' => 'Right');
	foreach ($sidearray as $number => $name) {
		$query = "INSERT INTO conformance_sides (cuid, number, name) VALUES ('$cuid', '$number', '$name')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	}
}

$addressquery = "SELECT CONCAT(streetnum, ' ', address, ' ', suffix) FROM inspection INNER JOIN building USING (bid) WHERE iid=$iid";
$addressresult = mysql_query($addressquery) or sql_crapout($addressquery.'<br />'.mysql_error());
$address = mysql_result($addressresult, 0);

$title = "$iid-$unitnumber - $address - Exterior";
require_once'header.php';

if ($error == 1) {
	print "<table width='550'><tr><td><font size='+2' color='red'>If you chose 'Yes' for hazards then you must enter a hazard area for that room or switch it to no.</font></td></tr></table>";
} else if ($error == 2) {
	print "<table width='550'><tr><td><font size='+2' color='red'>If you chose 'No' for hazards then you must deselect all hazard areas for that room.</font></td></tr></table>";
}

?>
<p><a href="inspection-conformance-defer.php?cuid=<? print $cuid; ?>&defer=soil">Defer Soil Inspection</a>
<form action="inspection-conformance-exterior-save.php" method="post">
<input type="hidden" name="cuid" value="<?php print $cuid; ?>" />
<table class="info">
<tr><th>Side #</th><th>Side Name</th><th>Paint Hazards</th><th>Hazard Areas</th><th>Soil Hazard</th></tr>
<?php

$hazards = array('Yes', 'No');
$soilhazard = array('Yes', 'No');
$hazardareas = array('Siding', 'Doors', 'Trim', 'Porch', 'Windows', 'Soffit', 'Railing', 'Stairs', 'Columns', 'Foundation', 'Garage', 'Fence', 'Outbuilding'); //Other is seperate

$query = "SELECT * FROM conformance_sides WHERE cuid=$cuid ORDER BY number";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while ($row = mysql_fetch_assoc($result)) {
	print "<tr><td>$row[number]</td><td><input type=\"text\" name=\"data[$row[csid]][name]\" value=\"$row[name]\" size=\"15\" maxlength=\"20\" /></td>";
	print "<td>";
	foreach ($hazards as $hazard) {
		if ($row['hazards'] == $hazard) {
			print "<input type=\"radio\" name=\"data[$row[csid]][hazards]\" value=\"$hazard\"  checked=\"checked\" />$hazard&nbsp;";
		} else {
			print "<input type=\"radio\" name=\"data[$row[csid]][hazards]\" value=\"$hazard\" />$hazard&nbsp;";
		}
	}
	print "</td>";
	print "<td>";
	print "<table>";
	$savedhazardareas = explode(',', $row['hazardareas']);
	$numitems = 0;
	$columns = 5;
	foreach ($hazardareas as $hazard) {
		if ($numitems % $columns == 0) {
			print "<tr>";
		}
		if ($hazard == 'Outbuilding') {
			$colspan = 2;
		} else {
			$colspan = 1;
		}
		if (in_array($hazard, $savedhazardareas)) {
			print "<td colspan=\"$colspan\"><input type=\"checkbox\" name=\"data[$row[csid]][hazardareas][]\" value=\"$hazard\" checked=\"checked\" />$hazard</td>";
		} else {
			print "<td colspan=\"$colspan\"><input type=\"checkbox\" name=\"data[$row[csid]][hazardareas][]\" value=\"$hazard\" />$hazard</td>";
		}
		$numitems++;
		if ($numitems % $columns == 0) {
			print "</tr>\n";
		}
	}
	if ($numitems % $columns) {
		while ($numitems % $columns) {
			print "<td> </td>";
			$numitems++;
		}
		print "</tr>";
	}
	print "<tr>";
	if (in_array('Other', $savedhazardareas)) {
		print "<td colspan=\"".($columns-1)."\"><input type=\"checkbox\" name=\"data[$row[csid]][hazardareas][]\" value=\"Other\" checked=\"checked\" />Other <input type=\"text\" name=\"data[$row[csid]][otherhazards]\" value=\"$row[otherhazards]\" maxlength=\"100\" /></td>";
	} else {
		print "<td colspan=\"".($columns-1)."\"><input type=\"checkbox\" name=\"data[$row[csid]][hazardareas][]\" value=\"Other\" />Other <input type=\"text\" name=\"data[$row[csid]][otherhazards]\" value=\"$row[otherhazards]\" maxlength=\"100\" /></td>";
	}

	print "</tr>";
	print "</table>";
	print "</td>";
	$hazard = 'Soil'; $colspan = 1;
	
	print "<td colspan=\"$colspan\" valign=top>";
	if ($row['soilhazard']=='Yes'){
		print "<input type=\"radio\" name=\"data[$row[csid]][soilhazard]\" value=\"Yes\" checked=\"checked\" />Yes ";}
	else{
	print "<input type=\"radio\" name=\"data[$row[csid]][soilhazard]\" value=\"Yes\" />Yes ";}
	if ($row['soilhazard'] == 'No'){
		print "<input type=\"radio\" name=\"data[$row[csid]][soilhazard]\" value=\"No\" checked=\"checked\" />No  ";}
	else{
	print "<input type=\"radio\" name=\"data[$row[csid]][soilhazard]\" value=\"No\" />No  ";}
	print "</td>";
	
	print "</td></tr>";
}
?>
</table>
<p><input type="submit" name="submit" value="Save" /> <input type="reset" value="Reset" /> <input type="submit" name="submit" value="Cancel" /></p>
</form>

<p>Unit Comments:&nbsp;&nbsp;&nbsp;<a href="inspection-conformance-unit-comments.php?cuid=<?php print $cuid; ?>">Edit</a><br />
<!--<textarea name="comment" rows="6" cols="50"> --><?php
$query = "SELECT comment FROM conformance_comments WHERE cuid=$cuid AND type='unit'";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) ) {
	$comment = mysql_result($result, 0);
}
print $comment;
?><!-- </textarea> --></p>

<p><a href="inspection-conformance-unit-notes.php?cuid=<?php print $cuid; ?>">Private Unit Inspection Notes</a></p>

<p><a href="inspection-view.php?iid=<?php print $iid; ?>">Inspection Main Menu</a></p>
<?php
require_once'footer.php';
?>