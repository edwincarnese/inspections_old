<?php

require_once'session.php';
require_once'connect.php';

if(isset($_SERVER['HTTP_USER_AGENT']) and strpos($_SERVER['HTTP_USER_AGENT'],'MSIE')) {
	header('Content-Type: application/force-download');
} else {
	header('Content-Type: application/octet-stream');
}
header('Content-disposition: attachment; filename=inspections.csv');

print '"Inspection #","Inspection Type","Inspection Date (Scheduled)","Inspection Time (Scheduled)","Inspection Started"'."\n";

$query = "SELECT iid, type, DATE_FORMAT(scheddate, '%c/%e/%Y'), schedstart, DATE_FORMAT((SELECT MIN(starttime) FROM units WHERE iid=inspection.iid), '%c/%e/%Y %h:%i%p') FROM inspection";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while ($row = mysql_fetch_assoc($result)) {
	print '"'.implode('","', $row).'"'."\n";
}

?>