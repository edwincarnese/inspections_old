<?php 
$title = "Remove Owner from Building";

require_once'session.php';
require_once'connect.php';

$cid = $_POST['cid'] or $cid = $_GET['cid'];

$cid += 0; // (force numbers)

$query = "SELECT * FROM client WHERE cid=$cid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/client-list.php");
	exit();
}

$row = mysql_fetch_assoc($result);
/*
print "<!--\n";
print_r($row);
print "\n-->";
*/

require_once'header.php';
?>
<p>Are you sure you want to remove this owner</p>
<?php
print "<p>$row[firstname] $row[lastname]<br />\n";
if ($row['company']) {print "$row[company]<br />\n";}
print "$row[streetnum] $row[address] $row[suffix]<br />\n";
if ($row['address2']) {print "$row[address2]<br />\n";}
print "$row[city], $row[state] $row[zip]<br />
$row[email]<br />\n";

$query = "SELECT * FROM client_phone WHERE cid=$cid";
$result2 = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while ($row2 = mysql_fetch_assoc($result2) ) {
	print "($row2[type]) $row2[number]&nbsp;&nbsp;&nbsp;";
	if ($row2['ext']) { print "Ext: $row2[ext]"; }
	print "<br />\n";
}
print "</p>\n";
?>
<hr />
<p>from this building?</p>
<?php
$bid = $_POST['bid'] or $bid = $_GET['bid'];

$bid += 0; // (force numbers)

$query = "SELECT * FROM building WHERE bid=$bid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/building-list.php");
	exit();
}

$row = mysql_fetch_assoc($result);
/*
print "<!--\n";
print_r($row);
print "\n-->";
*/

?>
<p>
<?php
print "$row[streetnum] $row[address] $row[suffix]<br />\n";
if ($row['address2']) {print "$row[address2]<br />\n";}
print "$row[city], $row[state] $row[zip]<br />";

print "Built in: $row[yearbuilt]<br />
Total Units: $row[numunits]<br />
Plat: $row[plat]<br />
Lot: $row[lot]<br />
Other: $row[other]<br />
Insurance Agent: $row[InsuranceAgent]<br />
Insurance Company: $row[InsuranceCo]<br />
Insurance Policy Number: $row[InsurancePolicyNo]</p>\n";

?>
<form action="building-removeowner-save.php" method="post">
<input type="hidden" name="bid" value="<?php print $bid; ?>" />
<input type="hidden" name="cid" value="<?php print $cid; ?>" />
<p>Date effective: <input type="text" name="endmonth" maxlength="2" size="3" /> / <input type="text" name="endday" maxlength="2" size="3" /> / <input type="text" name="endyear" maxlength="4" size="5" /> <span class="smaller">(Leave blank for current date.)</span><br />
<input type="submit" name="submit" value="Remove" /> <input type="submit" name="submit" value="Cancel" />
</p>
</form>

<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>