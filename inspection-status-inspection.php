<?php

require_once'connect.php';
require_once'session.php';

$title = "Inspection Status Inspection";
require_once'header.php';
?>
<table>
<table>
<tr><th><a href="inspection-status-inspection.php?order=iid">Insp </a></th><th>Address, <a href="inspection-status-inspection.php?order=city">City</a>, <a href="inspection-status-inspection.php?order=zip">Zip</a></th><th><a href="inspection-status-inspection.php?order=scheddate">Date</a></th><th>Type</th><th>Team</th></tr>
<?php
switch ($_GET['order']) {
	case 'city':
		$orderstring = 'ORDER BY city';
		break;
	case 'zip':
		$orderstring = 'ORDER BY zip';
		break;
	case 'scheddate':
		$orderstring = 'ORDER BY scheddate DESC';
		break;
	case 'iid':
	default:
		$orderstring = 'ORDER BY iid DESC';
		break;
}

mysql_query("DROP TABLE client_temp, inspbld_temp, comprehensive_temp, water_questions_temp, wipes_temp, final_temp");
$wherestring1 = "client.cid=''
OR client.certdate=''
OR client.certnumber=''
OR client.class_comp=''";
//OR client.class_with=''";

$query1 = "CREATE TEMPORARY TABLE client_temp (SELECT DISTINCT cid FROM client WHERE $wherestring1)";

$wherestring2 = "building.numunits=0
OR inspection.owneroccupied=''";

$query2 = "CREATE TEMPORARY TABLE inspbld_temp (SELECT DISTINCT iid FROM inspection INNER JOIN building USING (bid) WHERE $wherestring2)";

$wherestring3 = "units.insptype='Comprehensive'";

$query3 = "CREATE TEMPORARY TABLE comprehensive_temp (SELECT DISTINCT * FROM units WHERE $wherestring3)";

$wherestring4 = "waters.iid=inspection.iid
    OR comprehensive_temp.bottle=''
    OR comprehensive_temp.infant=''
    OR comprehensive_temp.testing=''
    OR comprehensive_temp.leadpipes=''";
    //This eventually needs to be able to skip deferred water samples
    
$query4 = "CREATE TEMPORARY TABLE water_questions_temp (SELECT DISTINCT inspection.iid FROM inspection INNER JOIN waters USING (iid) INNER JOIN comprehensive_temp USING (iid) where $wherestring4)";

$inspections = mysql_query("select iid from inspection") or sql_crapout("select iid from inspection".mysql_error());

mysql_query("create temporary table wipes_temp (iid mediumint(8))") or sql_crapout("create temporary table wipes_temp (iid mediumint(8))".mysql_error());

while ($val = mysql_fetch_array($inspections)) {
    $flag=0;
    
	$compquery = "SELECT type FROM inspection WHERE iid=$val[0]";
	$compresult = mysql_query($compquery) or sql_crapout($compquery.mysql_error());
	$compflag = mysql_fetch_array($compresult);
	if ($compflag[0]=="Comprehensive") {

        $under6query="SELECT count(iid) from units where iid=$val[0] and units.under6>0";
	    $under6result = mysql_query($under6query) or sql_crapout($under6query.'<br />'.mysql_error());
	    $foo = mysql_fetch_array($under6result);
		if ($foo[0]>0) {
			$numwipes = 5;
		}
		else {
		  $numwipes = 3;
  		}

		$wipequery="SELECT count(comprehensive_wipes.iid) from comprehensive_wipes where comprehensive_wipes.iid=$val[0]";
	    $wiperesult = mysql_query($wipequery) or sql_crapout($wipequery.'<br />'.mysql_error());
		$count =mysql_fetch_array($wiperesult);
		if ($count[0] <$numwipes) {
	        $flag=1;
	    }

	}
    
	if ($flag==0) {
        $confquery = "SELECT type FROM inspection WHERE iid=$val[0]";
		$confresult = mysql_query($confquery) or sql_crapout($confquery.mysql_error());
		$confflag = mysql_fetch_array($confresult);
		if ($confflag[0]=="Conformance") {
		    $wipequery1="SELECT count(conformance_wipes.iid) from conformance_wipes where conformance_wipes.iid=$val[0]";
		    $wiperesult1= mysql_query($wipequery1) or sql_crapout($wipequery1.'<br />'.mysql_error());
			$count1 =mysql_fetch_array($wiperesult1);
			if ($count1[0]<3) {
		        $flag=1;
		    }
   		}
    }
    if ($flag==1) {
      mysql_query("INSERT INTO wipes_temp VALUES($val[0])") or sql_crapout(mysql_error());
    }
}


$result1 = mysql_query($query1) or sql_crapout($query1.'<br />'.mysql_error());
$result2 = mysql_query($query2) or sql_crapout($query2.'<br />'.mysql_error());
$result3 = mysql_query($query3) or sql_crapout($query3.'<br />'.mysql_error());
$result4 = mysql_query($query4) or sql_crapout($query4.'<br />'.mysql_error());
//$result5 = mysql_query($query5) or sql_crapout($query5.'<br />'.mysql_error());

$query6 = "CREATE TEMPORARY TABLE temp (SELECT iid FROM inspection INNER JOIN owners USING (bid) INNER JOIN client_temp USING (cid))";

$result6 = mysql_query($query6) or sql_crapout($query6.'<br />'.mysql_error());

$query7 = "SELECT inspbld_temp.iid FROM inspbld_temp UNION SELECT wipes_temp.iid FROM wipes_temp UNION SELECT water_questions_temp.iid FROM water_questions_temp UNION SELECT temp.iid FROM temp";

$result7 = mysql_query($query7) or sql_crapout($query7.'<br />'.mysql_error());

mysql_query("CREATE TEMPORARY TABLE final_temp (iid mediumint(8))") or sql_crapout("create temporary table final_temp (iid mediumint(8))".mysql_error());

while ($val = mysql_fetch_array($result7)) {
    mysql_query("INSERT INTO final_temp VALUES($val[0])") or sql_crapout("INSERT INTO final_temp VALUES($val[0])".mysql_error());
}

$query = "SELECT distinct inspection.iid, building.address, building.address2, building.city, building.state, building.zip, DATE_FORMAT(inspection.scheddate, '%c/%e/%Y') AS inspdate, type FROM inspection, building, final_temp where inspection.iid=final_temp.iid and inspection.bid=building.bid and inspection.status!='Completed'$orderstring";

$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

while ($row = mysql_fetch_assoc($result)) {

    $tmptime = $row['inspdate'];
    $tmptimearray = explode('/', $tmptime);
    $tmptimestamp = mktime(0,0,0,$tmptimearray[0], $tmptimearray[1], $tmptimearray[2]);
    if ($tmptimestamp <= time() + 604800 and $tmptimestamp >= time() - 604800){
        print "<tr style=\"color:#DC143C\"><td>$row[iid]-*</td><td class=\"left\"><a style=\"color:#8B1A1A\" href=\"inspection-view.php?iid=$row[iid]\">$row[address], ";
        if ($row['address2']) {
		  print "$row[address2], ";
        }
        print "<br />$row[city], $row[state], $row[zip]</a></td><td>";
    } else {
        print "<tr><td>$row[iid]-*</td><td class=\"left\"><a href=\"inspection-view.php?iid=$row[iid]\">$row[address], ";
        if ($row['address2']) {
	       print "$row[address2], ";
        }
	   print "<br />$row[city], $row[state], $row[zip]</a></td><td>";
  }

    print "$tmptime</td><td>$row[type]</td><td>";
	$query = "SELECT firstname, lastname FROM insp_assigned INNER JOIN inspector USING (tid) WHERE iid=$row[iid]";
	$result2 = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row2 = mysql_fetch_row($result2)) {
		print "$row2[0] $row2[1]<br />";
	}
	print "</td></tr>\n";
}
?>
</table>
<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>
}


