<?php
/*
 * Created on Dec 22, 2005
 *
 * Author: Jason Rankin
 */
?>
<?php 
require_once'helper.php';


$iid = getPostIsset('iid');
$popup = $_GET['isPopup'];
$cuid = $_GET['cuid'];
$unitassign = $_GET['unitassign'] or $unitassign='';

require_once'session.php';
require_once'connect.php';

$userlevelquery = "SELECT userlevel FROM users WHERE uid=$_SESSION[sid]";
$userlevelresult = mysql_query($userlevelquery) or sql_crapout($userlevelquery.'<br />'.mysql_error());
$userlevel = mysql_result($userlevelresult, 0);


if (!$popup) {
	$title = "Schedule Inspector";
	require_once'header.php';
} else {
		?><head>
	<link rel="stylesheet" type="text/css" href="style.css" />
	</head>
	
		<div id="content" ><?php //style="width:700px; height:480px;">?>
	<?php
}
	


?>
<script language=javascript >
function openerHref() {
	document.update.popup.value = opener.location.href;
}

</script>
<?php if ($_GET['unitassign'] == 'true'){?>
<form name="update" action="inspection-inspector-unitassign.php" method="post">
<input type="hidden" name="iid" value="<?php print $iid;?>" />
<input type="hidden" name="cuid" value="<?php print $cuid;?>" />
<?php if ($popup != ''){ ?>
<input type="hidden" name="temp1" value="on">
<input type="hidden" name="popup" value="true">
<?php } }

else {?>
<p>Choose the inspectors to assign to this inspection.</p>
<form name="update" action="inspection-inspector-elt-eli-assign.php" method="post">
<input type="hidden" name="iid" value="<?php print $iid;?>">
<?php if ($popup != ''){ ?>
<input type="hidden" name="temp1" value="on">
<input type="hidden" name="popup" value="true">
<?php }}
?>
<table border="0"><tr><td valign="top"><table border="0"><tr><th>Lead</th><th>Assist</th><th>Name</th><th>Lic</th><th>Lic-Expire</th></tr>
<?php

//$query = "SELECT inspector.tid, lastname, firstname, job FROM inspector LEFT JOIN insp_assigned ON (inspector.tid=insp_assigned.tid AND insp_assigned.iid=$iid) ORDER BY eli DESC, lastname, firstname";

if ($_GET['unitassign'] == 'true'){
$query="SELECT distinct firstname, lastname, inspector.tid, inspector.eli,inspector.elt, Date_FORMAT(elidate, '%c/%y') AS elidate, DATE_FORMAT(eltdate, '%c/%y') AS eltdate,insp_assigned.assig_id FROM inspector LEFT JOIN insp_assigned ON iid=$iid WHERE insp_assigned.tid=inspector.tid";
}
else{
	$query = "SELECT inspector.tid, lastname, firstname, job, eli, elt, Date_FORMAT(elidate, '%c/%y') AS elidate, DATE_FORMAT(eltdate, '%c/%y') AS eltdate, lead_capable, insp_assigned.tid as IA_tid FROM inspector LEFT JOIN insp_assigned ON (inspector.tid=insp_assigned.tid AND insp_assigned.iid=$iid) WHERE inspector.comments <> 'inactive' ORDER BY lastname, firstname";
}



$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$count=0;
while ($count <round(mysql_num_rows($result)/2)) {
	$row = mysql_fetch_assoc($result);
	print '<tr><td>';
	if ($_GET['unitassign'] == 'true'){
	
		$jobquery = "SELECT job FROM unit_insp_assigned WHERE assig_id=".$row['assig_id'];
		$jobresult = mysql_query($jobquery) or sql_crapout($jobquery.'<br />'.mysql_error());
		if ($row['eli'] != NULL && $row['eli']!='') {
			$tmp = mysql_fetch_assoc($jobresult);
			if ($tmp['job'] =='Lead') {
				?>
				<input type="radio" value="<?php print $row['tid']; ?>" name="lead" checked="checked"/></td>
			<?php
			} else {
				?>
				<input type="radio" value="<?php print $row['tid']; ?>" name="lead"/></td>
				<?php
			}
		} 
	echo "<script> alert('$cuid');<script>";
	$cuidquery = "SELECT * FROM unit_insp_assigned WHERE assig_id=$row[assig_id] AND cuid='$cuid'";
		$cuidresult = mysql_query($cuidquery) or sql_crapout($cuidquery.'<br />'.mysql_error());

		if (mysql_num_rows($cuidresult) > 0) {
			?>
			<td><input type="checkbox" value="<?php print $row['tid']; ?>" name="assistant[]" checked="checked"/></td>
			<?php
		} else {
			?>
			<td><input type="checkbox" value="<?php print $row['tid']; ?>" name="assistant[]"/></td>
	<?php	}}	
	else{
 	if ($row['job'] =='Lead') 
 	{
 		print "<input type=\"radio\" name=\"lead\" value=\"$row[tid]\" checked=\"checked\" />";
		
 	}
 	elseif ($row['eli']) {
 			print "<input type=\"radio\" name=\"lead\" value=\"$row[tid]\" />";
			
 		} 
	elseif ($row['lead_capable']=='yes') {
 			print "<input type=\"radio\" name=\"lead\" value=\"$row[tid]\" />";
			
 		} 
	else {
 			print " ";
 		 		}
 	

	
	
 	print '</td><td>';
	if ($row['job'] == 'Assistant') {
		print "<input type=\"checkbox\" name=\"assist[]\" value=\"$row[tid]\" checked=\"checked\" />";
	} else {
		print "<input type=\"checkbox\" name=\"assist[]\" value=\"$row[tid]\" />";
	}}
//	print "</td><td>$row[lastname], $row[firstname]</td><td>elt</td></tr>\n";
	print "</td>";
	if ($userlevel >4){
		if ($row['tid']==$row['IA_tid']){print "<td><a href=\"removeinspector.php?tid=".$row['IA_tid']."&iid=".$iid."&cuid=".$cuid."\">$row[lastname], $row[firstname]</a></td>";}
		else {print "<td>$row[lastname], $row[firstname]</td>";}
	}
	else {print "<td>$row[lastname], $row[firstname]</td>";}
	
	print "<td>" ;
    if ($row['eli']){
    	print 'I';
    } else if ($row['elt']) {
    	print 'T';
    } else {
    	print 'None';
    }
    print "</td><td>";
    if ($row['eli']) {
    	print $row['elidate'];
    } else {
    	print $row['eltdate'];
    }
	print "</td></tr>\n";
	$count++;
}

?>
</table></td><td valign="top"><table border="0"><tr><th>Lead</th><th>Assist</th><th>Name</th><th>Lic</th><th>Lic-Expire</th></tr>
<?php
while ($row = mysql_fetch_assoc($result)) {
	print '<tr><td>';
 	if ($_GET['unitassign'] == 'true'){
	$jobquery = "SELECT job FROM unit_insp_assigned WHERE assig_id=".$row['assig_id'];
		$jobresult = mysql_query($jobquery) or sql_crapout($jobquery.'<br />'.mysql_error());
		if ($row['eli'] != NULL && $row['eli']!='') {
			$tmp = mysql_fetch_assoc($jobresult);
			if ($tmp['job'] =='Lead') {
				?>
				<input type="radio" value="<?php print $row['tid']; ?>" name="lead" checked="checked"/></td>
			<?php
			} else {
				?>
				<input type="radio" value="<?php print $row['tid']; ?>" name="lead"/></td>
				<?php
			}
		} 
	
	$cuidquery = "SELECT * FROM unit_insp_assigned WHERE assig_id=$row[assig_id] AND cuid='$cuid'";
		$cuidresult = mysql_query($cuidquery) or sql_crapout($cuidquery.'<br />'.mysql_error());

		if (mysql_num_rows($cuidresult) > 0) {
			?>
			<td><input type="checkbox" value="<?php print $row['tid']; ?>" name="assistant[]" checked="checked"/></td>
			<?php
		} else {
			?>
			<td><input type="checkbox" value="<?php print $row['tid']; ?>" name="assistant[]"/></td>
			<?php
		}}
		
	else{
 	if ($row['job'] =='Lead') 
 	{
 		print "<input type=\"radio\" name=\"lead\" value=\"$row[tid]\" checked=\"checked\" />";
		
 	}
 	elseif ($row['eli']) {
 			print "<input type=\"radio\" name=\"lead\" value=\"$row[tid]\" />";
			
 		} 
	elseif ($row['lead_capable']=='yes') {
 			print "<input type=\"radio\" name=\"lead\" value=\"$row[tid]\" />";
			
 		} 
	else {
 			print " ";
 		 		}
 	

	
	
 	print '</td><td>';
	if ($row['job'] == 'Assistant') {
		print "<input type=\"checkbox\" name=\"assist[]\" value=\"$row[tid]\" checked=\"checked\" />";
	} else {
		print "<input type=\"checkbox\" name=\"assist[]\" value=\"$row[tid]\" />";
	}}
	print "</td>";
	if ($userlevel >4){
		if ($row['tid']==$row['IA_tid']){print "<td><a href=\"removeinspector.php?tid=".$row['IA_tid']."&iid=".$iid."&cuid=".$cuid."\">$row[lastname], $row[firstname]</a></td>";}
		else {print "<td>$row[lastname], $row[firstname]</td>";}
	}
	else {print "<td>$row[lastname], $row[firstname]</td>";}
	print "<td>" ;
    if ($row['eli']){
    	print 'I';
    } else if ($row['elt']) {
    	print 'T';
    } else {
    	print 'None';
    }
    print "</td><td>";
    if ($row['eli']) {
    	print $row['elidate'];
    } else {
    	print $row['eltdate'];
    }
	print "</td></tr>\n";
}
?>
</table></td></tr>
</table>

<?php 
if ($_GET['unitassigned'] = ''){
if ($userlevel >4){print "Clicking on an inspector's name will remove him/her from this inspection";}}
?>
<p>
<input type="submit" name="submit" value="Save" onClick="javascript:openerHref();"/><input type="reset" value="Reset" /><input type="submit" name="submit" value="Cancel" onClick="javascript:self.close();"/>
</p>

<?php
if (!$popup){
	require_once'footer.php';
} else {

?>

</div>


<?php

}

?>