<?php 

require_once'session.php';
require_once'connect.php';

$ctid = $_SESSION['ctid'] or $_POST['ctid'] or $ctid = $_GET['ctid'] or $ctid = 0;
$part = $_SESSION['part'] or $_POST['part'] or $part = $_GET['part'] or $part = 1;

if (!isset ($_SESSION['ctid'])) {
	$_SESSION['ctid'] = $ctid;
}
if (!isset ($_SESSION['part'])) {
	$_SESSION['part'] = $part;
}

$title = "Schedule Class";
require_once'header.php';
?>
<?php

$query = "SELECT * FROM class_types WHERE ctid=$ctid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/class-schedule.php");
}
$row = mysql_fetch_assoc($result);

print "<p>Click a date's number to choose that day.<br />Schedule part $part of $row[parts] for $row[name].</p>";

// ********************** CALENDAR STUFF **********************
if ($_GET['year'] && $_GET['month']) {
	$year = $_GET['year'] + 0;
	if ($year < 10) {$year += 2000;}
	if ($year < 100) {$year += 1900;}
	if ($year < 1582) {$year = 1582;}
	if ($year > 9999) {$year = 9999;}
	$month = $_GET['month'] + 0;
	if ($month < 1) {$month = 1;}
	if ($month > 12) {$month = 12;}
	$date = "$year-$month-01";
} else {
	$date = date('Y-m-01');
}

$query = "SELECT DAYOFWEEK('$date') AS day, DATE_FORMAT('$date', '%M %Y') as title, MONTH('$date') AS month, YEAR('$date') AS year";
print "<!-- $query -->";
$result = mysql_query($query) or exit($query);
$day = mysql_result($result, 0, 'day');
$month = mysql_result($result, 0, 'month');
$year = mysql_result($result, 0, 'year');
$title = mysql_result($result, 0, 'title');
mysql_free_result($result);

switch ($month) {
	case 1:
	case 3:
	case 5:
	case 7:
	case 8:
	case 10:
	case 12:
		$days = 31;
		break;
	case 4:
	case 6:
	case 9:
	case 11:
		$days = 30;
		break;
	case 2:
		$days = 29;
		if ($year%4 || ($year % 100 == 0 && $year % 400)) {
			$days--;
		}
		break;
}

switch ($month) {
	case 1:
		$prevyear = $year - 1;
		$prevmonth = 12;
		$nextyear = $year;
		$nextmonth = $month + 1;
		break;
	case 12:
		$prevyear = $year;
		$prevmonth = $month - 1;
		$nextyear = $year + 1;
		$nextmonth = 1;
		break;
	default:
		$prevyear = $year;
		$prevmonth = $month - 1;
		$nextyear = $year;
		$nextmonth = $month + 1;
}	

?>
<table class="calendar">
<caption><a href="class-schedule2.php?year=<?php print $prevyear; ?>&amp;month=<?php print $prevmonth; ?>">&lt;&lt;</a> <?php print "$title"; ?> <a href="class-schedule2.php?year=<?php print $nextyear; ?>&amp;month=<?php print $nextmonth; ?>">&gt;&gt;</a> </caption>
<tr>
	<th>Sunday</th>
	<th>Monday</th>
	<th>Tuesday</th>
	<th>Wednesday</th>
	<th>Thursday</th>
	<th>Friday</th>
	<th>Saturday</th>
</tr>
<tr>
<?php

for ($x=1; $x<$day; $x++) {
	print '<td> </td>';
}

for ($x=1; $x<=$days; $x++, $day++) {
	if ($day > 7) {
		print "</tr>\n<tr>";
		$day -= 7;
	}
	print "<td><div class=\"date\"><a href=\"class-schedule-part.php?date=$year-$month-$x\">$x</a></div>";
	
	//day info and display
	$xx = str_pad($x, 2, '0', STR_PAD_LEFT); // db has leading zeros, dammit
	$query = "SELECT *, DATE_FORMAT(classtime, '%l:%i%p') AS starttime FROM class_parts INNER JOIN class USING (clid) INNER JOIN class_types USING (ctid) WHERE DATE(classtime) = '$year-$month-$xx'";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) {
		$query = "SELECT COUNT(*) FROM class_students WHERE cpid=$row[cpid]";
		$result2 = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		$students = mysql_result($result2, 0);
		
		print "<span style=\"font-size: 9pt\">$row[starttime] ($row[length] hrs)<br />&nbsp;$row[name]<br />&nbsp;Part $row[part] of $row[parts]<br /><u>&nbsp;<b>$students</b>/$row[recstudents]/$row[maxstudents]</span></u><br />";
	}
	print "</td>";
//	$day++;
}

while ($day <= 7) {
	print '<td> </td>';
	$day++;
}
?>
</tr>
</table>
<?php
require_once'footer.php';
?>