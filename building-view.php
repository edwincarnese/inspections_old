<?php


require_once'session.php';
require_once'connect.php';


$bid = isset($_POST['bid']) ? $_POST['bid'] : (isset($_GET['bid']) ? $_GET['bid'] : 0);
$popup = isset($_POST['isPopup']) ? $_POST['isPopup'] : (isset($_GET['isPopup']) ? $_GET['isPopup'] : 0);

$query = "SELECT * FROM building WHERE bid=$bid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

if (mysql_num_rows($result) == 0) {
	header("Location: http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/building-list.php");
	exit();
}

$row = mysql_fetch_assoc($result);
if (!$popup) {
	$title = "View building";
	require_once'header.php';
} else {
	?><head>
	<link rel="stylesheet" type="text/css" href="style.css" />
	</head>
	
	<div id="content">
	<?php
}
?>
<SCRIPT LANGUAGE="JavaScript">

function popUp(URL) {
day = new Date();
id = day.getTime();
eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=825,height=530,top=575');");
}
</script>
<div>	
</div>
<table frame="border" border="2" rules="none" cellspacing="1" cellpadding="1" width="350px">
	<tr><th align="left" colspan="3" class="box">Building Information <font size="-1"><a href="javascript:popUp('building-edit.php?bid=<?php print $row['bid'];?>&isPopup=true')" class="box2" style="border:solid gray 1px;display: inl">click to edit</font></a></th><th><img style="width: 100px;height: 100px" src="<?php echo $row['image_name']; ?>">
</th></tr>

	<tr>
		<td class="box2" colspan="2"> <?php print $row['streetnum']." ".$row['address']." ".$row['suffix']." ".$row['address2']; ?><br>
		<?php print $row['city'].", ".$row['state']." ".$row['zip']; ?></td>
		<td class="box2" colspan="2" valign="top">Number of Units: <?php print $row['numunits']; ?><br>
		Owner Occupied: <?php print isset($row['owneroccupied']) ? $row['owneroccupied'] : '';?></td>
	</tr>
	
	<tr>
		<td class="box2" colspan="2">Year Built: <?php print $row['yearbuilt'];?><br>
		Other: <?php print $row['other'];?><br>
		Plat: <?php print $row['plat']; ?><br>
		Lot: <?php print $row['lot']; ?></td>
		<td class="box2" valign="top" colspan="2">
		Insurance Agent: <?php print $row['InsuranceAgent']; ?><br>
		Insurance Company: <?php print $row['InsuranceCo']; ?><br>
		Policy #: <?php print $row['InsurancePolicyNo']; ?></td>
		
	</tr>

</table>
<?php

    
if (!$popup) {
?>
	<p><a href="inspection-schedule.php?bid=<?php print $bid; ?>">Schedule an inspection for this building</a></p>
<?php 

} else {
?>
	<p><a href="javascript:opener.window.parent.location.href='inspection-schedule.php?bid=<?php print $bid; ?>';self.close();">Schedule an inspection for this building</a></p>
<?php 

}

?>
	
<!--  ************************** PEOPLE ************************** -->

<?php
$query = "SELECT *, DATE_FORMAT(startdate, '%c/%e/%Y') AS fstartdate, DATE_FORMAT(enddate, '%c/%e/%Y') AS fenddate FROM owners INNER JOIN client USING (cid) WHERE bid=$bid AND enddate>NOW()";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
?>
<p><a href="building-addowner.php?bid=<? print $bid; ?>">Add a person </a></p>
<?php
if (mysql_num_rows($result)) {
?>
<table class="info">
<caption>
Associated People
</caption>
<tr><th>First Name</th><th>Last Name</th><th>Company</th><th>Rel</th><th>Start Date</th><th>End Date</th></tr>
<?php
	while ($row = mysql_fetch_assoc($result)) {
		if ($row['enddate'] == '9999-12-31') {
			$row['fenddate'] = '(current)';
		}
		if (!$popup) {
			print "<tr><td><a href=\"client-view.php?cid=$row[cid]\">$row[firstname]</a></td><td>$row[lastname]</td><td>$row[company]</td><td>$row[relationship]</td><td>$row[fstartdate]</td><td>$row[fenddate]</td><td><a href=\"building-removeowner.php?bid=$bid&cid=$row[cid]\">Remove</a></td></tr>\n";
		} else {
			print "<tr><td><a href=\"javascript:opener.window.parent.location.href='client-view.php?cid=$row[cid]';self.close();\">$row[firstname]</a></td><td>$row[lastname]</td><td>$row[company]</td><td>$row[relationship]</td><td>$row[fstartdate]</td><td>$row[fenddate]</td><td><a href=\"building-removeowner.php?bid=$bid&cid=$row[cid]\">Remove</a></td></tr>\n";
		}	
	}
?>

</table>
<?php

}

?>
<p>
<!--  ************************** Associate People ************************** -->

<br />
<br />


</p>
<!--  ************************** INSPECTIONS ************************** -->

<?php
$query = "SELECT inspection.iid, units.number, DATE_FORMAT(starttime, '%c/%e/%Y') AS startdate, unitdesc, insptype, status FROM inspection INNER JOIN units USING (iid) WHERE bid=$bid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result)) {
?>
<table class="info">
<caption>Past Inspections</caption>
<tr><th>Insp #</th><th>Date</th><th>Unit</th><th>Type</th><th>Status</th></tr>
<?php
while ($row = mysql_fetch_assoc($result)) {
	if (!$popup) {
			print "<tr><td><a href=\"inspection-view.php?iid=$row[iid]\">$row[iid]-$row[number]</a></td><td>$row[startdate]</td><td>$row[unitdesc]</td><td>$row[insptype]</td><td>$row[status]</td></tr>\n";
		} else {
			print "<tr><td><a href=\"javascript:opener.window.parent.location.href='inspection-view.php?iid=$row[iid]';self.close();\">$row[iid]-$row[number]</a></td><td>$row[startdate]</td><td>$row[unitdesc]</td><td>$row[insptype]</td><td>$row[status]</td></tr>\n";
		}	
	}
?>
</table>
<?php
}
?>

<?php


if (!$popup){
	require_once'footer.php';
} else {
?></div>
<?php 
}

?>