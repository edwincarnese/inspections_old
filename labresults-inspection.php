<?php

require_once'session.php';
require_once'connect.php';

$iid = $_POST['iid'] or $iid = $_GET['iid'] or $iid = 0;

$title = "Lab List";
require_once'header.php';
?>
<form action="labresults-save.php" method="post">
<input type="hidden" name="iid" value="<?php print $iid; ?>" />
<table>
<tr><th>Sample Number</th><th>Result</th></tr>
<?php
$query = "SELECT inspection.iid, building.streetnum, building.address, building.suffix, building.city FROM building INNER JOIN inspection USING (bid) WHERE iid=$iid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while($row = mysql_fetch_assoc($result)) {
	print "<p>$row[iid]-*,&nbsp;&nbsp;$row[streetnum] $row[address] $row[suffix], $row[city]</p>\n";
}

//WIPES
$x = 1;
$query = "(SELECT units.number AS unitnumber, conformance_wipes.number, 'conformance' FROM conformance_wipes INNER JOIN conformance_rooms USING (crid) INNER JOIN units USING (cuid) WHERE units.iid=$iid AND results = '')
UNION 
(SELECT units.number AS unitnumber, comprehensive_wipes.number, 'comprehensive' FROM comprehensive_wipes INNER JOIN comprehensive_rooms USING (crid) INNER JOIN units USING (cuid) WHERE units.iid=$iid AND results = '')
UNION
(SELECT 0 AS unitnumber, conformance_wipes.number, 'conformance' FROM conformance_wipes WHERE iid=$iid AND number=0 AND results = '') 
UNION 
(SELECT 0 AS unitnumber, comprehensive_wipes.number, 'comprehensive' FROM comprehensive_wipes WHERE iid=$iid AND number=0 AND results = '')
ORDER BY unitnumber, number"; //second half is field blank
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result)) {
	print "<tr><th colspan=\"2\">Dust Wipes</td></tr>\n";
	while ($row = mysql_fetch_row($result)) {
		print "<tr><td>$iid-$row[0]D$row[1]</td>
<input type=\"hidden\" name=\"results[$x][type]\" value=\"D\" />
<input type=\"hidden\" name=\"results[$x][insptype]\" value=\"$row[2]\" />
<input type=\"hidden\" name=\"results[$x][number]\" value=\"$row[1]\" />
<td><input type=\"text\" name=\"results[$x][result]\" size=\"10\" autocomplete=\"off\" /></td></tr>\n";
		$x++;
	}
}

//PAINT CHIPS
$query = "(SELECT units.number AS unitnumber, paintchips.number FROM paintchips INNER JOIN comprehensive_ext_components USING (ccid) INNER JOIN units USING (cuid) WHERE units.iid=$iid AND results = '')
UNION
(SELECT units.number AS unitnumber, paintchips.number FROM paintchips INNER JOIN comprehensive_components USING (ccid) INNER JOIN comprehensive_rooms USING (crid) INNER JOIN units USING (cuid) WHERE units.iid=$iid AND results = '')
ORDER BY unitnumber, number";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result)) {
	print "<tr><th colspan=\"2\">Paint Chips</td></tr>\n";
	while ($row = mysql_fetch_row($result)) {
		print "<tr><td>$iid-$row[0]P$row[1]</td>
<input type=\"hidden\" name=\"results[$x][type]\" value=\"P\" />
<input type=\"hidden\" name=\"results[$x][number]\" value=\"$row[1]\" />
<td><input type=\"text\" name=\"results[$x][result]\" size=\"10\" autocomplete=\"off\" /></td></tr>\n";
		$x++;
	}
}

//WATER
$query = "SELECT units.number AS unitnumber, waters.number FROM waters INNER JOIN units USING (cuid) WHERE units.iid=$iid AND results = '' ORDER BY unitnumber, number";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result)) {
	print "<tr><th colspan=\"2\">Water Samples</td></tr>\n";
	while ($row = mysql_fetch_row($result)) {
		print "<tr><td>$iid-$row[0]W$row[1]</td>
<input type=\"hidden\" name=\"results[$x][type]\" value=\"W\" />
<input type=\"hidden\" name=\"results[$x][number]\" value=\"$row[1]\" />
<td><input type=\"text\" name=\"results[$x][result]\" size=\"10\" autocomplete=\"off\" /></td></tr>\n";
		$x++;
	}
}

//SOIL
$query = "(SELECT units.number, comprehensive_sides.side AS samplenumber, 'SS' AS type, units.cuid FROM comprehensive_sides INNER JOIN units USING (cuid) WHERE units.iid=$iid AND results = '' AND cover != 'Pavement')
UNION
(SELECT units.number, comprehensive_soil_debris.samplenumber, 'SD' AS type, units.cuid FROM comprehensive_soil_debris INNER JOIN units USING (cuid) WHERE units.iid=$iid AND results = '')
UNION
(SELECT units.number, comprehensive_soil_object.samplenumber, 'SO' AS type, units.cuid FROM comprehensive_soil_object INNER JOIN units USING (cuid) WHERE units.iid=$iid AND results = '' AND cover != 'Pavement')
ORDER BY type DESC, samplenumber";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result)) {
	print "<tr><th colspan=\"2\">Soil Samples</td></tr>\n";
	while ($row = mysql_fetch_row($result)) {
		print "<tr><td>$iid-$row[0]$row[2]$row[1]</td>
<input type=\"hidden\" name=\"results[$x][type]\" value=\"$row[2]\" />
<input type=\"hidden\" name=\"results[$x][cuid]\" value=\"$row[3]\" />
<input type=\"hidden\" name=\"results[$x][number]\" value=\"$row[1]\" />
<td><input type=\"text\" name=\"results[$x][result]\" size=\"10\" autocomplete=\"off\" /></td></tr>\n";
		$x++;
	}
}

?></table>

<p><?php
//"Next" inspection
$query = "SELECT iid FROM waters WHERE results = '' AND iid>$iid UNION
SELECT iid FROM paintchips WHERE results = '' AND iid>$iid UNION
SELECT iid FROM comprehensive_wipes WHERE results = '' AND iid>$iid UNION
SELECT iid FROM conformance_wipes WHERE results = '' AND iid>$iid UNION
SELECT iid FROM comprehensive_soil_debris INNER JOIN units USING (cuid) WHERE results = '' AND iid>$iid UNION
SELECT iid FROM comprehensive_sides INNER JOIN units USING (cuid) WHERE results = '' AND iid>$iid UNION
SELECT iid FROM comprehensive_soil_object INNER JOIN units USING (cuid) WHERE results = '' AND iid>$iid ORDER BY iid LIMIT 1";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result)) {
	$next = mysql_result($result, 0);
} else {
	$next = 0;
}
$query = "SELECT iid FROM waters WHERE results = '' AND iid<$iid UNION
SELECT iid FROM paintchips WHERE results = '' AND iid<$iid UNION
SELECT iid FROM comprehensive_wipes WHERE results = '' AND iid<$iid UNION
SELECT iid FROM conformance_wipes WHERE results = '' AND iid<$iid UNION
SELECT iid FROM comprehensive_soil_debris INNER JOIN units USING (cuid) WHERE results = '' AND iid<$iid UNION
SELECT iid FROM comprehensive_sides INNER JOIN units USING (cuid) WHERE results = '' AND iid<$iid UNION
SELECT iid FROM comprehensive_soil_object INNER JOIN units USING (cuid) WHERE results = '' AND iid<$iid ORDER BY iid DESC LIMIT 1";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result)) {
	$prev = mysql_result($result, 0);
} else {
	$prev = 0;
}
?>
<input type="hidden" name="previous" value="<?php print $prev; ?>" />
<input type="hidden" name="next" value="<?php print $next; ?>" />
<?php
$query = "SELECT inspection.iid, building.streetnum, building.address, building.suffix, building.city FROM building INNER JOIN inspection USING (bid) WHERE iid=$prev";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while($row = mysql_fetch_assoc($result)) {
	print "Previous: $row[iid]-*,&nbsp;&nbsp;$row[streetnum] $row[address] $row[suffix], $row[city]<br />\n";
}


$query = "SELECT inspection.iid, building.streetnum, building.address, building.suffix, building.city FROM building INNER JOIN inspection USING (bid) WHERE iid=$next";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while($row = mysql_fetch_assoc($result)) {
	print "Next: $row[iid]-*,&nbsp;&nbsp;$row[streetnum] $row[address] $row[suffix], $row[city]\n";
}
?>
</p>
<p><input type="submit" name="submit" value="Save" /> 
<?php
if ($prev) { ?>
<input type="submit" name="submit" value="Save and Previous" />
<?php
}
if ($next) { ?>
<input type="submit" name="submit" value="Save and Next" />
<?php
}
?>
<input type="reset" value="Reset" /> <input type="submit" name="submit" value="Cancel" /></p>
</form>

<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>