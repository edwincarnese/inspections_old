<?php

require_once'session.php';
require_once'connect.php';

$iid = $_POST['iid'] or $iid = $_GET['iid'] or $iid = 0;
$old = $_POST['old'] or $old = $_GET['old'] or $old = 0;

foreach ($_POST['oldunits'] as $cuid) {
	copyunit($cuid);
}


function copyunit($cuid) {
	global $iid;
	$linkupdates = array();
	$query = "SELECT * FROM units WHERE cuid=$cuid order by designation";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		$cuid = array_shift($row); //get rid of cuid off array for implodes
		unset ($row['endtime']);
		//unset ($row['starttime']);
		$row['iid'] = $iid;
		$query = "INSERT INTO units (".implode(',', array_keys($row)).") VALUES ('".implode("','", $row)."')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		$query = "SELECT LAST_INSERT_ID()"; //get new cuid
		$resultid = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		$newid = mysql_result($resultid, 0);
		copy_conformance_comments($cuid, $newid);
		copy_conformance_sides($cuid, $newid);
		copy_conformance_rooms($cuid, $newid);
		copy_conformance_wipes($cuid, $newid);
		$unitmap["$cuid"] = $newid;
		$linkupdates[] = $cuid;
	}

/*
	//unit links; can't be a function; need to know whole unit map for non-commons
	foreach ($linkupdates as $common) {
		//deleting is done while updating the unit
		$newcommon = $unitmap[$common];
		$query = "SELECT unit FROM unit_links WHERE common=$common";
		$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		while ($row = mysql_fetch_assoc($result)) {
			$newunit = $unitmap[$row['unit']];
			$query = "INSERT IGNORE INTO unit_links (common, unit) VALUES ($newcommon, $newunit)";
			mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		}
		//flip half, unit could be non-common
		//deleting is done while updating the unit
		$newunit = $unitmap[$common];
		$query = "SELECT common FROM unit_links WHERE unit=$common";
		$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		while ($row = mysql_fetch_assoc($result)) {
			$newcommon = $unitmap[$row['common']];
			$query = "INSERT IGNORE INTO unit_links (common, unit) VALUES ($newcommon, $newunit)"; //ignore errors
			mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		}
	}
*/
}

function copy_conformance_comments($oldcuid, $newcuid) {
	global $iid;
	$query = "SELECT * FROM conformance_comments WHERE cuid=$oldcuid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		$query = "INSERT INTO conformance_comments (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		//print $query.'<br><br>';
	}
}

function copy_conformance_sides($oldcuid, $newcuid) {
	global $iid;
	$query = "SELECT * FROM conformance_sides WHERE cuid=$oldcuid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		unset($row['csid']);
		$query = "INSERT INTO conformance_sides (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		//print $query.'<br><br>';
	}
}

function copy_conformance_rooms($oldcuid, $newcuid) {
	global $iid;
	$query = "SELECT * FROM conformance_rooms WHERE cuid=$oldcuid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	while ($row = mysql_fetch_assoc($result)) { //for each row
		array_walk($row, 'slashadd'); //add slashes
		unset($row['cuid']); //get rid of cuid off array for implodes
		$oldcrid = $row['crid'];
		unset($row['crid']); //get rid of crid off array for implodes
		$query = "INSERT INTO conformance_rooms (cuid, ".implode(',', array_keys($row)).") VALUES ('$newcuid','".implode("','", $row)."')";
		mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
		//print $query.'<br><br>';
	}
}

function copy_conformance_wipes($oldcuid, $newcuid) {
	global $iid;
	$query = "SELECT * FROM conformance_wipes INNER JOIN conformance_rooms USING (crid) WHERE cuid=$oldcuid";
	$result = mysql_query($query) or sql_crapout($query."<br />".mysql_error());
	while ($row = mysql_fetch_assoc($result)) {
		$comment .= "Wipe:".$row['iid']."-".$row['number']."-".$row['number']."  Taken: ".$row['timetaken']."  Surface: ".$row['surface']."  Side: ".$row['side']."<br>Paintchips: ".$row['paintchipe']."  Length: ".$row['arealength']."  Width: ".$row['areawidth']."  Results: ".$row['results']."<br>Conversion: ".$row['conversion']."  Square Foot Results: ".$row['squarefootresults']."   Spot test: ".$row['spottest']."  Hazzard Assessment: ".$row['hazzardassessment']."<br /><br />";
	}
//$insQuery = "INSERT INTO conformance_comments (cuid, type, comment) VALUES($newcuid, 'prior_wipe', '$comment')";
//print $insQuery.'<br><br>';
	//$insResult= mysql_query($insQuery) or sql_crapout($insQuery."<br />".mysql_error());	
	
}

//**********************************************************************************//
//******************************* NON-COPY FUNCTIONS *******************************//
//**********************************************************************************//

function slashadd(&$item) {
	$item = addslashes($item);
}



$title = "Copy Inspection";
require_once'header.php';
?>
<?php
print '<script language=javascript>';
print 'window.location.href="inspection-view.php?iid='.$iid.'"';
print '</script>';

require_once'footer.php';
?>