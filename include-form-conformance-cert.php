<?php

$pdf->AddPage('P');

require_once'include-form-conformance-cert-form.php';
$pdf->SetFont('Arial','',12);

$query = "SELECT building.bid, streetnum, address, suffix, city, state, zip, numunits,designation, yearbuilt, plat, lot, inspection.iid, unitdesc, reason, number, units.cuid FROM building INNER JOIN inspection USING (bid) INNER JOIN units USING (iid) WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

$lh = 0.4;

$pdf->SetXY(1.75, 0.15);
$pdf->Cell(0,0, "Inspection $row[iid]-$row[number]");

$iid = $row['iid'];
$bid = $row['bid'];
$cuid = $row['cuid'];
$reason = $row['reason'];

$pdf->SetXY($ml+6.7, 1.7);
$pdf->Cell(0, 0, "8252");


//ADDRESS
$pdf->SetXY($ml+0.45, 2.68);
$pdf->Cell(0, 0, "$row[streetnum] $row[address] $row[suffix]");
$pdf->SetXY($ml+4.4, 2.68);
$pdf->Cell(0,0, $row['numunits']);
$pdf->SetXY($ml+5.85, 2.68);
$pdf->Cell(0,0, $row['plat'].' / '.$row['lot']);
$pdf->SetXY($ml+0.7, 3);
$pdf->Cell(0,0, $row['city']);
$pdf->SetXY($ml+2.7, 3);
$pdf->Cell(0,0, $row['zip']);
$pdf->SetXY($ml+5.2, 3);
//$pdf->Cell(0,0, $row['unitdesc']);
$pdf->Cell(0,0, $row['designation']);

//OWNER
$query = "SELECT client.cid, firstname, lastname, company, streetnum, address, suffix, city, state, zip FROM client INNER JOIN owners USING (cid) WHERE bid=$bid AND startdate <= DATE(NOW()) AND enddate >= DATE(NOW())";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
if (mysql_num_rows($result) == 0) {
	exit("Building requires owner. Form blows up without owner.");
}
$row = mysql_fetch_assoc($result);
formfix($row);
$cid = $row['cid'];

$pdf->SetXY($ml+0.5, 3.75);
if ($row['firstname']=='' or $row['lastname']=='') {
	$pdf->Cell(0,0,"$row[company]");
}
else {
	$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
}

$pdf->SetXY($ml+0.5, 4.05);
$pdf->Cell(0,0, "$row[streetnum] $row[address] $row[suffix]");
$pdf->SetXY($ml+3.4, 4.05);
$pdf->Cell(0,0, "$row[city]");
$pdf->SetXY($ml+5.4, 4.05);
$pdf->Cell(0,0, "$row[state]");
$pdf->SetXY($ml+6.35, 4.05);
$pdf->Cell(0,0, "$row[zip]");

$query = "SELECT type, number, ext FROM client_phone WHERE cid=$cid GROUP BY type";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
while ($row = mysql_fetch_assoc($result)) {
	formfix($row);
	if ($row['ext']) {
		$nums[$row['type']] = "$row[number] ext. $row[ext]";
	} else {
		$nums[$row['type']] = "$row[number]";
	}
}

$pdf->SetXY($ml+4.75, 3.7);
if ($nums['Home']) {
	$pdf->Cell(0,0, $nums['Home']);
} else if ($nums['Cell']) {
	$pdf->Cell(0,0, $nums['Cell'] . ' (Cell)');
	$cellused = 1;
}
/*
$pdf->SetXY(3.75, 4.7);
if ($nums['Work']) {
	$pdf->Cell(0,0, $nums['Work']);
} else if ($nums['Cell'] && $cellused < 1) {
	$pdf->Cell(0,0, $nums['Cell'] . ' (Cell)');
}
*/
//TENANT
$query = "SELECT tenant, tenantyears, under6 FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
formfix($row);

$pdf->SetXY($ml+0.6, 4.85);
$pdf->Cell(0,0, $row['tenant']);
$pdf->SetXY($ml+4.33, 5.7);
//$pdf->Cell(0,0, $row['tenantyears']);
//INSPECTORS
$query = "SELECT firstname, lastname, elt, eli FROM inspector INNER JOIN insp_assigned USING (tid) INNER JOIN unit_insp_assigned USING (assig_id) WHERE iid=$iid AND unit_insp_assigned.cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$x = $mody = 0;

	if ($row['eli']) {
		$pdf->SetXY($ml+3.1, 8.4);
		$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
		$pdf->SetXY($ml+4.5, 8.88);
		$pdf->Cell(0,0, "ELI-$row[eli]");
	} else {
		if ($x == 0) {
			$mody += 0.7;
			$x++;
		}
		$pdf->SetXY($ml+3.1, 8.4);
		$pdf->Cell(0,0, "$row[firstname] $row[lastname]");
		$pdf->SetXY($ml+4.5, 8.88);
		$pdf->Cell(0,0, "ELT-$row[elt]");
	}


$mody -= 0.07; //whatever
$query = "SELECT DATE_FORMAT(starttime, '%c/%e/%Y') AS inspdate, DATE_FORMAT(starttime, '%l:%i %p') AS insptime FROM units WHERE cuid=$cuid";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$row = mysql_fetch_assoc($result);
$pdf->SetXY($ml+1.1, 5.7);
$pdf->Cell(0,0, "Independent Clearance Inspection");
$pdf->SetXY($ml+5.5, 5.7);
$pdf->Cell(0,0, "$row[inspdate]");
$pdf->SetXY($ml+1.25, 8.88);
$pdf->Cell(0,0, "$row[inspdate]");



?>