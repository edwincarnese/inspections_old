<?php 

include_once('session.php');
include_once('connect.php');
include 'helper.php';


$cuid = getPostIsset('cuid');

$query = "SELECT 
		  units.* ,inspection.* ,insp_assigned.* , 
		  `inspector`.address as inspector_address,
		  inspector.*, 
		  building.*,
		  `client`.firstname as owner_fname,
		  `client`.lastname as owner_lname,
		  `client_phone`.number as owner_contact

		  FROM units,inspection ,insp_assigned , inspector , building , owners , client , client_phone
		  WHERE cuid=$cuid AND 
		  `units`.iid = `inspection`.iid AND
		  `inspection`.iid = `insp_assigned`.iid AND
		  `inspection`.bid = `building`.bid AND
		  `building`.bid = `owners`.bid AND
		  `owners`.cid = `client`.cid AND
		  `client_phone`.cid = `client`.cid AND
		  `inspector`.tid = `insp_assigned`.tid
		  ";
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
$data = mysql_fetch_array($result, MYSQL_ASSOC);

$default_top = "10px";
$prepared_by = issetBlank($data,'firstname') . " " . issetBlank($data,'lastname');
$prepared_for = "John Tonneson";
$inspector_pos = "NH Licensed Risk Assessor/Lead";
$inspector_no = "Inspector RA # 0064";
$client_address = "173 Rosecliff Lane, Manchester, NH 03013";
$client_contact = "603-203-4642";

$inspector_company_name = "K. Kirkwood Consulting, LLC";
$inspector_address = "25 Lowell St., Manchester, NH 0310123 Nute Rd, Madbury, NH";
$inspector_contact = "603-781-4304";
$inspector_email = "kate@kkirkwood.com";
$dateToday = date('Y-m-d');
$building_address = issetBlank($data,'address');



?>


<form name="update" action="report-nh.php" method="post" enctype="multipart/form-data">
<input type="hidden" name="iid" value="<?php echo issetBlank($data,'iid') ;?>" />
<input type="hidden" name="cuid" value="<?php echo $cuid ;?>" />

<table>
<tr><td>Upload Building Image:</td><td><input type="file" name="logo" id="fileToUpload" accept="image/*" required></td></tr>
<tr><td>LIRA:</td><td><input type='text' value='Lead Inspection / Risk Assessment' size='30' name='lira' required></td></tr>
<tr><td>Building Address:</td><td><input type='text' value='<?php echo issetBlank($data,'city') . ', ' . issetBlank($data,'state') . ' ' . issetBlank($data,'zip')?>' size='30' name='building_address' required></td></tr> 
<tr><td>Inspector Name:</td><td><input type='text' value='<?php echo issetBlank($data,'firstname') . ' ' . issetBlank($data,'lastname')?>' size='30' name='inspector_name' required></td></tr>
<tr><td>Inspector Address:</td><td><input type='text' value='<?php echo issetBlank($data,'inspector_address')?>' size='30' name='inspector_address' required></td></tr>
<tr><td>Inspector Contact:</td><td><input type='text' value='<?php echo issetBlank($data,'number')?>' size='30' name='inspector_contact' required></td></tr>
<tr><td>Inspector Email:</td><td><input type='text' value='<?php echo issetBlank($data,'email')?>' size='30' name='inspector_email' required></td></tr>
<tr><td>Inspector License:</td><td><input type='text' value='' size='30' name='inspector_license' required></td></tr>
<tr><td>Inspector RA #:</td><td><input type='text' value='' size='30' name='inspector_ra' required></td></tr>

<tr><td>Client Name:</td><td><input type='text' value='<?php echo issetBlank($data,'owner_fname') . ' ' . issetBlank($data,'owner_lname')?>' size='30' name='owner_name' required></td></tr>
<tr><td>Client Address:</td><td><input type='text' value='<?php echo issetBlank($data,'owner_address') ?>' size='30' name='owner_address' required></td></tr>
<tr><td>Client Contact:</td><td><input type='text' value='<?php echo issetBlank($data,'owner_contact') ?>' size='30' name='owner_contact' required></td></tr>
<tr><td>Property Type:</td><td><input type='text' value='' size='30' name='property_type' required></td></tr>
<tr><td>Date Performed:</td><td><input type='text' size='30' value="<?php echo $dateToday?>" name='date_performed' required></td></tr>
<tr><td>Generate PDF Report:</td><td><input type='submit' id='submit' name='submit' value='Submit'></td></tr>
</table>
</form>