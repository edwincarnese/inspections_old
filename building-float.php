<?php
/*
 * Created on Dec 29, 2005
 *
 * Author: Jason Rankin
 */
 require_once'session.php';
 require_once'connect.php';
 
?>
<head>
<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body style="background: #D5DFF5;"/>
<SCRIPT LANGUAGE="JavaScript">

function popUp(URL) {
day = new Date();
id = day.getTime();
eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=825,height=530,top=575');");
}
//document.searchForm.firstname.value='blah';
</script>
<?php


$cid = isset($_POST['cid']) ? $_POST['cid'] : (isset($_GET['cid']) ? $_GET['cid'] : 0);
$bid = isset($_POST['bid']) ? $_POST['bid'] : (isset($_GET['bid']) ? $_GET['bid'] : 0);

$showall = isset($_POST['showall']) ? $_POST['showall'] : (isset($_GET['showall']) ? $_GET['showall'] : 0);

if ($bid==0) {
	?>
	
	
	<form action="building-searchresults.php" method="post" name="searchForm" target="_parent">
	<?php if (isset($cid)) {
		print "<input type='hidden' name='cid' value='$cid'/>";
	}
	?>
	<table>
	<tr><td>Address:</td><td><input type="text" name="streetnum" maxlength="8" size="5" /><input type="text" name="address" maxlength="30" /><input type="text" name="suffix" maxlength="10" size="7" /></td></tr>
	<tr><td>Address 2:</td><td><input type="text" name="address2" maxlength="30" /></td></tr>
	<tr><td>City:</td><td><input type="text" name="city" maxlength="30" /></td></tr>
	<tr><td>State:</td><td><input type="text" name="state" maxlength="2" size="3" /></td></tr>
	<tr><td>Zip:</td><td><input type="text" name="zip" maxlength="5" size="6" /></td></tr>
	<tr><td>Year Built:</td><td><input type="text" name="yearbuilt" maxlength="6" size="7" /></td></tr>
	</table>
	<p><input type="submit" value="Search" /> <input type="reset" value="Reset" /></p>
	</form>
	<?php
}
else {
	$query = "SELECT * FROM building WHERE bid=$bid";
	$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());
	$row = mysql_fetch_assoc($result);
	$bid = $row['bid'];
	?>
	<table frame="border" border="2" rules="none" cellspacing="1" cellpadding="1" width="350px">
			<tr><th align="left" colspan="3" class="box">Building Information <font size="-1"><a href="javascript:popUp('building-view.php?bid=<?php print $row['bid'];?>&isPopup=true')" class="box2" style="border:solid gray 1px">click to view</a><?
			if (isset($_GET['cid'])) { ?>
				<a href="javascript:popUp('client-addbuilding-save.php?bid=<?php print $row['bid'];?>&cid=<?php print $_GET['cid'];?>&isPopup=true')" class="box2" style="border:solid gray 1px">add to client</a>
				<?
			}?>
			</font></th></tr>
			<tr>
				<td class="box2" colspan="2"> <?php print $row['streetnum']." ".$row['address']." ".$row['suffix']." ".$row['address2']; ?><br>
				<?php print $row['city'].", ".$row['state']." ".$row['zip']; ?></td>
				<td class="box2" valign="top">Number of Units: <?php print $row['numunits']; ?></td>
			</tr>
			
			<tr>
				<td class="box2" colspan="2">Year Built: <?php print $row['yearbuilt'];?><br>
				Other: <?php print $row['other'];?><br>
				Plat: <?php print $row['plat']; ?><br>
				Lot: <?php print $row['lot']; ?></td>
				<td class="box2" valign="top">
				Insurance Agent: <?php print $row['InsuranceAgent']; ?><br>
				Insurance Company: <?php print $row['InsuranceCo']; ?><br>
				Policy #: <?php print $row['InsurancePolicyNo']; ?></td>
			</tr>
			</table>

<?php
}

?>
<script type="text/javascript">
  //window.parent.fill_form()
</script>


