<?php


require_once 'session.php'; 
require_once 'connect.php'; 

$query = "SELECT lastname, firstname from users where uid=$sid";
$result = mysql_query($query) or sql_crapout($query."<br>", mysql_error());
$name = mysql_fetch_assoc($result);

$title = $name['lastname'].", ".$name['firstname'];
require_once 'header.php'; 
print "<table class='table table-bordered' frame='border' border='2' rules='none' cellspacing='1' cellpadding='1'><tr><th colspan='10' class='box' align='left'>Your Activities</th></tr><tr>";

$fields = array('date entered', 'date due', 'stat', 'description', 'client');
foreach ($fields as $field) {
	print "<th class='box'>$field</th>";
}

print "</tr>";

$query = "SELECT date_entered, date_due, status, description, concat(firstname , ' ' , lastname) as clientname FROM client_activities inner join client on client.cid=client_activities.cid WHERE assignedto=$sid and status not like 'done'";
$result = mysql_query($query) or sql_crapout($query."<br>", mysql_error());
while ($row = mysql_fetch_assoc($result)) {
	print "<tr>";
	foreach($row as $val) {
		print "<td class='box2' border='1px gray solid'>$val</td>";
	}
	print "</tr>";
}
print "</table>";

print "<table class='table table-bordered' frame='border' border='2' rules='none' cellspacing='1' cellpadding='1'><tr><th colspan='10' class='box' align='left'>Activities created by you</th></tr><tr>";

$fields = array('date entered', 'date due', 'stat', 'assigned to', 'description');
foreach ($fields as $field) {
	print "<th class='box'>$field</th>";
}

print "</tr>";

$query = "SELECT date_entered, date_due, status, assignedto, description FROM client_activities WHERE enteredby=$sid AND assignedto!=$sid";
$result = mysql_query($query) or sql_crapout($query."<br>", mysql_error());
while ($row = mysql_fetch_assoc($result)) {
	print "<tr>";
	foreach($row as $val) {
		print "<td class='box2' border='1px gray solid'>$val</td>";
	}
	print "</tr>";
}
print "</table>";
?>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<script language="javascript" src="md5.js"></script>
<link href='//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css' rel="stylesheet">

<script language="javascript">
function verifyNewPass() {
	var old_pass = calcMD5(document.passchange.old_pass.value);
	var new_pass = document.passchange.new_pass.value;
	var verify_pass = document.passchange.verify_pass.value;
	if (old_pass == <?php 
		$query = "SELECT password FROM users WHERE uid=$sid";
		$result = mysql_query($query) or sql_crapout($query."<br>".mysql_error());
		$old_pass = md5(mysql_result($result, 0));
		print "'".$old_pass."'"; 
		?>) {
		if (new_pass == verify_pass ) {
			return true;
		} else {
			alert('make sure the new password and the verify password fields match.');
			document.passchange.old_pass.value = '';
			document.passchange.new_pass.value = '';
			document.passchange.verify_pass.value = '';
			return false;
		}
	} else {
		alert('Sorry, the old password you entered does not match your current password.');
		document.passchange.old_pass.value = '';
		document.passchange.new_pass.value = '';
		document.passchange.verify_pass.value = '';
		return false;
	}
}
</script>

<form name="passchange" action="change-password.php" method="POST" onSubmit="return verifyNewPass()">
	<table class="table table-bordered" border="0">
		<tr>
			<th align="left" colspan="2">Change Password</th>
		</tr>
		<tr>
			<td colspan="2">Note: this is not required.  You are in the office version and this is just one of the options you have.  If you don't want to chage your password you don't have to.</td>
		</tr>
		<tr>
			<td>Old Password:</td>
			<td><input type="password" name="old_pass"/></td>
		</tr>
		<tr>
			<td>New Password:</td>
			<td><input type="password" name="new_pass"/></td>
		</tr>
		<tr>
			<td>Verify New Password:</td>
			<td><input type="password" name="verify_pass"/></td>
		</tr>
		<tr>
			<td>Submit</td>
			<td  ><input type="submit" name="save" class="btn btn-md btn-primary " value="save" /></td>
		</tr>
	</table>
</form>
	

<p><span style="color: gray">Code version: <?php require_once'datechanged.txt';?></span></p>

<script   src="http://code.jquery.com/jquery-3.1.1.min.js"  ></script>

<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>


<?php require_once'footer.php'; ?>


<script type="text/javascript">
	$(document).ready(function(){
	    $('.datatables').DataTable( {
        "paging":   false,
        "ordering": true,
        "info":     false
    } );
	});
</script>