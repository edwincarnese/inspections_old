<?php
$title = "Add Owner - Search Results";

require_once'session.php';
require_once'connect.php';

$_POST['number'] = "$_POST[phone1]-$_POST[phone2]-$_POST[phone3]";
unset($_POST['phone1']);
unset($_POST['phone2']);
unset($_POST['phone3']);
if (strlen($_POST['number']) != 12) {
	unset($_POST['number']);
}

$bid = $_POST['bid'];
unset($_POST['bid']);

$wheres = array();
foreach ($_POST as $field => $value) {
	if ($value) {
		switch ($field) {
			case 'firstname':
			case 'lastname':
			case 'company':
			case 'city':
				$value = htmlspecialchars($value);
				$wheres[] = "$field LIKE '%$value%'";
				break;
			case 'state':
			case 'zip':
			case 'number':
				$wheres[] = "$field = '$value'";
				break;
			default:
				exit("$field not coded");
		}
	}
}
$wherestring = implode(' AND ', $wheres);

if (!$wherestring) {
	$wherestring = '1';
}

$query = "SELECT DISTINCT client.cid, lastname, firstname, CONCAT(streetnum, ' ', address, ' ', suffix), city, state FROM client LEFT JOIN client_phone USING (cid) WHERE $wherestring AND client.cid NOT IN (SELECT cid FROM owners WHERE bid=$bid)";
//print $query;
$result = mysql_query($query) or sql_crapout($query.'<br />'.mysql_error());

require_once'header.php';

if (mysql_num_rows($result) == 0) {
	print "<p>No results found.</p>";
} else {
	print "<p>";
	while ($row = mysql_fetch_row($result) ) {
		$cid = array_shift($row);
		print "<a href=\"building-addowner-verify.php?bid=$bid&amp;cid=$cid\">".implode(', ', $row)."</a><br />\n";
	}
	print "</p>";
}
?>
<p><a href="index.php">Main Menu</a></p>
<?php
require_once'footer.php';
?>