<?php

require_once'connect.php';

require_once'fpdf152/fpdf.php';

$cuid = 3;

//subtract 0.06 inches from desired position of filst letter
// with 0.1 lineheight, measure from middle using Cell().

/*
Places to put X's using 6-point text:
ITEM		X		Y
Stardard:	0.87	2.88
------------------------
Paint:		2.155	3.51
Soil:		2.155	3.80
Water:		3.18	3.04	(Water, Drinking)
Wipe:		3.18	3.34
------------------------
Lead:		6.76	2.40
*/


$lh = 0.1; // line height

$pdf = new FPDF('L', 'in', 'Letter');
$pdf->SetLeftMargin(0.0);
$pdf->SetRightMargin(0.0);
$pdf->SetTopMargin(0.0);
$pdf->SetAutoPageBreak(true, 0.0);
$pdf->AddPage();

$pdf->SetDrawColor(255,0,0);

for ($x=0; $x<=22; $x++) {
	if ($x%2) {
		$pdf->SetLineWidth(0.2);
	} else {
		$pdf->SetLineWidth(0.4);
	}
	$pdf->Line($x/2, 0, $x/2, 8.5);
}
for ($y=0; $y<=13; $y++) {
	if ($x%2) {
		$pdf->SetLineWidth(0.2);
	} else {
		$pdf->SetLineWidth(0.4);
	}
	$pdf->Line(0, $y/2, 11, $y/2);
}

$pdf->Output();
?>